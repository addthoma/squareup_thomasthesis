.class public Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;
.super Landroid/widget/LinearLayout;
.source "CardReadersCardView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field private bleFooter:Landroid/view/View;

.field cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private connectReader:Landroid/widget/Button;

.field private header:Landroid/view/View;

.field private listView:Landroid/widget/ListView;

.field presenter:Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private readerStateAdapter:Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    const-class p2, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Component;->inject(Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 130
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 131
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->listView:Landroid/widget/ListView;

    return-void
.end method


# virtual methods
.method displayFooter(Z)V
    .locals 3

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->listView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->listView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->bleFooter:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 114
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->listView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->readerStateAdapter:Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method displayHeader(Z)V
    .locals 4

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->listView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    if-eqz p1, :cond_0

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->listView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->header:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    :cond_0
    if-nez p1, :cond_1

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->listView:Landroid/widget/ListView;

    invoke-virtual {p1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result p1

    if-lez p1, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->listView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->header:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->removeHeaderView(Landroid/view/View;)Z

    .line 105
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->listView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->readerStateAdapter:Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onAttachedToWindow$0$CardReadersCardView(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 78
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;

    invoke-virtual {p1, p3}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->onReaderClicked(I)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 73
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 74
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->bindViews()V

    .line 75
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->readerStateAdapter:Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->readerStateAdapter:Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->listView:Landroid/widget/ListView;

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReadersCardView$OZakP95EncBIv8rfpX8goxrq3xY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$CardReadersCardView$OZakP95EncBIv8rfpX8goxrq3xY;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 84
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 49
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 50
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/cardreader/ui/R$layout;->cardreaders_header_view:I

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->listView:Landroid/widget/ListView;

    const/4 v3, 0x0

    .line 51
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->header:Landroid/view/View;

    .line 52
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/cardreader/ui/R$layout;->cardreaders_ble_footer_view:I

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->listView:Landroid/widget/ListView;

    .line 53
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->bleFooter:Landroid/view/View;

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->bleFooter:Landroid/view/View;

    sget v1, Lcom/squareup/cardreader/ui/R$id;->device_contactless_help:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 56
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/cardreader/ui/R$string;->square_reader_help:I

    const-string v3, "square_shop"

    .line 57
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 58
    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getSupportSettings()Lcom/squareup/settings/server/SupportUrlSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/SupportUrlSettings;->getReorderHardwareUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(Ljava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/cardreader/ui/R$string;->square_shop:I

    .line 59
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 60
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asPhrase()Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 61
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/util/AppNameFormatterKt;->putAppName(Lcom/squareup/phrase/Phrase;Landroid/content/res/Resources;)Lcom/squareup/phrase/Phrase;

    .line 62
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->bleFooter:Landroid/view/View;

    sget v1, Lcom/squareup/cardreader/ui/R$id;->connect_reader_button:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->connectReader:Landroid/widget/Button;

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->connectReader:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView$1;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method setConnectReaderVisible(Z)V
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->connectReader:Landroid/widget/Button;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showBluetoothRequiredDialog()V
    .locals 2

    .line 118
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView$2;-><init>(Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;)V

    invoke-static {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/EnableBluetoothDialog;->show(Landroid/content/Context;Lcom/squareup/ui/settings/paymentdevices/EnableBluetoothDialog$EnableBluetoothDialogListener;)V

    return-void
.end method

.method updateCardReaderList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;)V"
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->readerStateAdapter:Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;->updateReaderList(Ljava/util/List;)V

    .line 93
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->readerStateAdapter:Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/ReaderStateAdapter;->notifyDataSetChanged()V

    return-void
.end method
