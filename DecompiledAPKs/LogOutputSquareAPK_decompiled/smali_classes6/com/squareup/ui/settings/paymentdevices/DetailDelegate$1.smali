.class synthetic Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;
.super Ljava/lang/Object;
.source "DetailDelegate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

.field static final synthetic $SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 42
    invoke-static {}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->values()[Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_CHECKING_SERVER_FOR_FWUP:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v2}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_IS_FWUP_REBOOTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v3}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_APPLYING_BLOCKING_AND_REBOOTING_FWUP_BUNDLE:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_KEY_INJECTION_STARTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v4

    const/4 v5, 0x4

    aput v5, v3, v4
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_APPLYING_BLOCKING_FWUP_BUNDLE:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v4

    const/4 v5, 0x5

    aput v5, v3, v4
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_TAMPERED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v4

    const/4 v5, 0x6

    aput v5, v3, v4
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_KEY_INJECTION_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v4

    const/4 v5, 0x7

    aput v5, v3, v4
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_FWUP_ERROR:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v4

    const/16 v5, 0x8

    aput v5, v3, v4
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_SERVER_DENIED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v4

    const/16 v5, 0x9

    aput v5, v3, v4
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    :try_start_9
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v4

    const/16 v5, 0xa

    aput v5, v3, v4
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    :try_start_a
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BATTERY_DEAD:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v4

    const/16 v5, 0xb

    aput v5, v3, v4
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    :try_start_b
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_STILL_BLUETOOTH_PAIRING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v4

    const/16 v5, 0xc

    aput v5, v3, v4
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    :catch_b
    :try_start_c
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_STILL_BLE_PAIRING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v4

    const/16 v5, 0xd

    aput v5, v3, v4
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_c

    :catch_c
    :try_start_d
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_ESTABLISHING_SESSION:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v4

    const/16 v5, 0xe

    aput v5, v3, v4
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_d

    :catch_d
    :try_start_e
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_RENEWING_SESSION:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v4

    const/16 v5, 0xf

    aput v5, v3, v4
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_e

    :catch_e
    :try_start_f
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_COMMS_CONNECTING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v4

    const/16 v5, 0x10

    aput v5, v3, v4
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_f

    :catch_f
    :try_start_10
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_OFFLINE_SWIPE_ONLY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v4

    const/16 v5, 0x11

    aput v5, v3, v4
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_10

    :catch_10
    :try_start_11
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BATTERY_LOW:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v4

    const/16 v5, 0x12

    aput v5, v3, v4
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_11

    :catch_11
    :try_start_12
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_READY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v4

    const/16 v5, 0x13

    aput v5, v3, v4
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_12

    :catch_12
    :try_start_13
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    sget-object v4, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_BLE_DISCONNECTED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v4}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v4

    const/16 v5, 0x14

    aput v5, v3, v4
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_13

    .line 110
    :catch_13
    invoke-static {}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->values()[Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    :try_start_14
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    sget-object v4, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v4}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_14

    :catch_14
    :try_start_15
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    sget-object v3, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v3

    aput v1, v0, v3
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_15

    :catch_15
    :try_start_16
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_16

    :catch_16
    return-void
.end method
