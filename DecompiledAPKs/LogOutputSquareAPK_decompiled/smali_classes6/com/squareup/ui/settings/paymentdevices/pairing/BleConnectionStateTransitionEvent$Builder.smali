.class public Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
.super Ljava/lang/Object;
.source "BleConnectionStateTransitionEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private additionalContext:Ljava/lang/String;

.field private batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

.field private bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

.field private bleRate:Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;

.field private bluetoothEnabled:Ljava/lang/Boolean;

.field private bondedDevicesCountExcludingCurrent:Ljava/lang/Integer;

.field private cardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field private connectedDevicesCountExcludingCurrent:Ljava/lang/Integer;

.field private connectionAttemptNumber:Ljava/lang/Integer;

.field private connectionError:Ljava/lang/String;

.field private currentBleState:Ljava/lang/String;

.field private disconnectRequestedBeforeDisconnection:Ljava/lang/Boolean;

.field private disconnectStatus:Ljava/lang/Integer;

.field private errorBeforeConnection:Ljava/lang/Boolean;

.field private firmwareVersion:Ljava/lang/String;

.field private hardwareSerialNumber:Ljava/lang/String;

.field private hoursMinutesAndSecondsSinceLastLCRCommunicationString:Ljava/lang/String;

.field public internetConnection:Lcom/squareup/internet/InternetConnection;

.field private macAddress:Ljava/lang/String;

.field private noReconnectReason:Ljava/lang/String;

.field private previousBleState:Ljava/lang/String;

.field private readerEventName:Ljava/lang/String;

.field private readerName:Ljava/lang/String;

.field private readerState:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field private receivedBleAction:Ljava/lang/String;

.field private rssiMax:Ljava/lang/Integer;

.field private rssiMean:Ljava/lang/Double;

.field private rssiMin:Ljava/lang/Integer;

.field private rssiSamples:Ljava/lang/String;

.field private rssiStdDev:Ljava/lang/Double;

.field private rssiVariance:Ljava/lang/Double;

.field private secondsSinceLastConnectionAttempt:Ljava/lang/Long;

.field private secondsSinceLastLCRCommunication:J

.field private secondsSinceLastSuccessfulConnection:Ljava/lang/Long;

.field private unlocalizedDescription:Ljava/lang/String;

.field private willReconnect:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->readerEventName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->readerName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Double;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->rssiMean:Ljava/lang/Double;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Double;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->rssiVariance:Ljava/lang/Double;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Double;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->rssiStdDev:Ljava/lang/Double;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->rssiSamples:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->hoursMinutesAndSecondsSinceLastLCRCommunicationString:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1500(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)J
    .locals 2

    .line 97
    iget-wide v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->secondsSinceLastLCRCommunication:J

    return-wide v0
.end method

.method static synthetic access$1600(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Long;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->secondsSinceLastSuccessfulConnection:Ljava/lang/Long;

    return-object p0
.end method

.method static synthetic access$1700(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Long;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->secondsSinceLastConnectionAttempt:Ljava/lang/Long;

    return-object p0
.end method

.method static synthetic access$1800(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->readerState:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-object p0
.end method

.method static synthetic access$1900(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Lcom/squareup/cardreader/CardReaderBatteryInfo;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->macAddress:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2000(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->bleRate:Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;

    return-object p0
.end method

.method static synthetic access$2100(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->firmwareVersion:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2200(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->hardwareSerialNumber:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2300(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Lcom/squareup/cardreader/ble/BleConnectType;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

    return-object p0
.end method

.method static synthetic access$2400(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Integer;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->bondedDevicesCountExcludingCurrent:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$2500(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Integer;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->connectedDevicesCountExcludingCurrent:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$2600(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->additionalContext:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2700(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Integer;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->connectionAttemptNumber:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$2800(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Integer;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->disconnectStatus:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$2900(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->connectionError:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Lcom/squareup/cardreader/CardReaderId;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-object p0
.end method

.method static synthetic access$3000(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Boolean;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->disconnectRequestedBeforeDisconnection:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$3100(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Boolean;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->errorBeforeConnection:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$3200(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Boolean;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->bluetoothEnabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$3300(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Boolean;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->willReconnect:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$3400(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->noReconnectReason:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->currentBleState:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->previousBleState:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->receivedBleAction:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->unlocalizedDescription:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Integer;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->rssiMin:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Integer;
    .locals 0

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->rssiMax:Ljava/lang/Integer;

    return-object p0
.end method


# virtual methods
.method public additionalContext(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 264
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->additionalContext:Ljava/lang/String;

    return-object p0
.end method

.method public batteryInfo(Lcom/squareup/cardreader/CardReaderBatteryInfo;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 224
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    return-object p0
.end method

.method public bleConnectType(Lcom/squareup/cardreader/ble/BleConnectType;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 244
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

    return-object p0
.end method

.method public bleRate(Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 229
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->bleRate:Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;

    return-object p0
.end method

.method public bluetoothEnabled(Z)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 295
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->bluetoothEnabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public bondedDevicesCount(I)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 249
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->bondedDevicesCountExcludingCurrent:Ljava/lang/Integer;

    return-object p0
.end method

.method public build()Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;
    .locals 1

    .line 220
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)V

    return-object v0
.end method

.method public cardReaderId(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-object p0
.end method

.method public connectedDevicesCount(I)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 254
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->connectedDevicesCountExcludingCurrent:Ljava/lang/Integer;

    return-object p0
.end method

.method public connectionAttemptNumber(Ljava/lang/Integer;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 269
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->connectionAttemptNumber:Ljava/lang/Integer;

    return-object p0
.end method

.method public connectionError(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 279
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->connectionError:Ljava/lang/String;

    return-object p0
.end method

.method public currentBleState(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->currentBleState:Ljava/lang/String;

    return-object p0
.end method

.method public disconnectRequestedBeforeDisconnection(Z)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 290
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->disconnectRequestedBeforeDisconnection:Ljava/lang/Boolean;

    return-object p0
.end method

.method public disconnectStatus(Ljava/lang/Integer;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 274
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->disconnectStatus:Ljava/lang/Integer;

    return-object p0
.end method

.method public errorBeforeConnection(Z)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 284
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->errorBeforeConnection:Ljava/lang/Boolean;

    return-object p0
.end method

.method public firmwareVersion(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 234
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->firmwareVersion:Ljava/lang/String;

    return-object p0
.end method

.method public hardwareSerialNumber(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 239
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->hardwareSerialNumber:Ljava/lang/String;

    return-object p0
.end method

.method public hoursMinutesAndSecondsSinceLastLCRCommunicationString(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->hoursMinutesAndSecondsSinceLastLCRCommunicationString:Ljava/lang/String;

    return-object p0
.end method

.method public internetConnection(Lcom/squareup/internet/InternetConnection;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 259
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->internetConnection:Lcom/squareup/internet/InternetConnection;

    return-object p0
.end method

.method public macAddress(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->macAddress:Ljava/lang/String;

    return-object p0
.end method

.method public noReconnectReason(Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 305
    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$NoReconnectReason;->name()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->noReconnectReason:Ljava/lang/String;

    return-object p0
.end method

.method public previousBleState(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->previousBleState:Ljava/lang/String;

    return-object p0
.end method

.method public readerEventName(Lcom/squareup/analytics/ReaderEventName;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 139
    iget-object p1, p1, Lcom/squareup/analytics/ReaderEventName;->value:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->readerEventName:Ljava/lang/String;

    return-object p0
.end method

.method public readerName(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->readerName:Ljava/lang/String;

    return-object p0
.end method

.method public readerState(Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->readerState:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    return-object p0
.end method

.method public receivedBleAction(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->receivedBleAction:Ljava/lang/String;

    return-object p0
.end method

.method public rssiSamples(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->rssiSamples:Ljava/lang/String;

    return-object p0
.end method

.method public rssiStatistics(Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 2

    .line 179
    invoke-virtual {p1}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->getMax()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->rssiMax:Ljava/lang/Integer;

    .line 180
    invoke-virtual {p1}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->getMin()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->rssiMin:Ljava/lang/Integer;

    .line 181
    invoke-virtual {p1}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->getMean()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->rssiMean:Ljava/lang/Double;

    .line 182
    invoke-virtual {p1}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->getVariance()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->rssiVariance:Ljava/lang/Double;

    .line 183
    invoke-virtual {p1}, Lcom/squareup/log/RssiLoggingHelper$RssiMeasurementStatistics;->getStdDev()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->rssiStdDev:Ljava/lang/Double;

    return-object p0
.end method

.method public secondsSinceLastConnectionAttempt(J)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 210
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->secondsSinceLastConnectionAttempt:Ljava/lang/Long;

    return-object p0
.end method

.method public secondsSinceLastLCRCommunication(J)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 200
    iput-wide p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->secondsSinceLastLCRCommunication:J

    return-object p0
.end method

.method public secondsSinceLastSuccessfulConnection(Ljava/lang/Long;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->secondsSinceLastSuccessfulConnection:Ljava/lang/Long;

    return-object p0
.end method

.method public unlocalizedDescription(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->unlocalizedDescription:Ljava/lang/String;

    return-object p0
.end method

.method public willReconnect(Z)Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    .locals 0

    .line 300
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->willReconnect:Ljava/lang/Boolean;

    return-object p0
.end method
