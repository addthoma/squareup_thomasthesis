.class Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter$1;
.super Ljava/lang/Object;
.source "CardReaderDetailCardScreen.java"

# interfaces
.implements Lrx/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->lambda$onLoad$3(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;)Lrx/Subscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observer<",
        "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;

.field final synthetic val$view:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;)V
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;

    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter$1;->val$view:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->access$100(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;)V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 140
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onNext(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;

    invoke-static {v0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->access$202(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->access$300(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;)Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter$1;->val$view:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardView;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->updateView(Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    .line 147
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 148
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isAudio()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;

    invoke-static {v0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;->access$400(Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 133
    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter$1;->onNext(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    return-void
.end method
