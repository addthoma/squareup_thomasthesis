.class Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "CardReadersScreenView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView$2;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView$2;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;

    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreenView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Presenter;->onConnectReaderClicked()V

    return-void
.end method
