.class public abstract Lcom/squareup/ui/settings/DefaultSettingsAppletServicesModule;
.super Ljava/lang/Object;
.source "DefaultSettingsAppletServicesModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract settingsAppletServices(Lcom/squareup/ui/settings/RealSettingsAppletServices;)Lcom/squareup/ui/settings/SettingsAppletServices;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
