.class public Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;
.super Lcom/squareup/ui/settings/opentickets/ticketgroups/InEditTicketGroupScope;
.source "TicketGroupLimitPopupScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$DialogBuilder;,
        Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$Factory;,
        Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final GROUP_LIMIT_REACHED:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;

.field public static final TEMPLATE_LIMIT_REACHED:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;


# instance fields
.field private final limitType:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 34
    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;

    sget-object v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;->TEMPLATE:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;)V

    sput-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;->TEMPLATE_LIMIT_REACHED:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;

    .line 40
    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;

    sget-object v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;->GROUP:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;)V

    sput-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;->GROUP_LIMIT_REACHED:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;

    .line 113
    sget-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$TicketGroupLimitPopupScreen$JYRejE8VTHl4F_fCH2d84iCtVTs;->INSTANCE:Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$TicketGroupLimitPopupScreen$JYRejE8VTHl4F_fCH2d84iCtVTs;

    .line 114
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;)V
    .locals 0

    .line 46
    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/InEditTicketGroupScope;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;->limitType:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;)Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;->limitType:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;
    .locals 1

    .line 115
    invoke-static {}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;->values()[Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;

    move-result-object v0

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    aget-object p0, v0, p0

    .line 116
    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 109
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/InEditTicketGroupScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 110
    iget-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen;->limitType:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;

    invoke-virtual {p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketGroupLimitPopupScreen$LimitType;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
