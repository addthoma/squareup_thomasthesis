.class Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "OpenTicketsSettingsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->onCreateTicketGroupButtonRowInflated(Lcom/squareup/marketfont/MarketButton;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;)V
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView$1;->this$0:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 117
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView$1;->this$0:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;

    iget-object p1, p1, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->onCreateTicketButtonClicked()V

    return-void
.end method
