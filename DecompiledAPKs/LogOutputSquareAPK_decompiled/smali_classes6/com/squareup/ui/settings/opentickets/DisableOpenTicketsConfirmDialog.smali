.class public Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "DisableOpenTicketsConfirmDialog.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final confirmation:Lcom/squareup/register/widgets/Confirmation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/ui/settings/opentickets/-$$Lambda$DisableOpenTicketsConfirmDialog$9EdCy4H0J8X1cIsp1sPORlt0Z3E;->INSTANCE:Lcom/squareup/ui/settings/opentickets/-$$Lambda$DisableOpenTicketsConfirmDialog$9EdCy4H0J8X1cIsp1sPORlt0Z3E;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/register/widgets/Confirmation;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog;->confirmation:Lcom/squareup/register/widgets/Confirmation;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog;)Lcom/squareup/register/widgets/Confirmation;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog;->confirmation:Lcom/squareup/register/widgets/Confirmation;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog;
    .locals 2

    .line 55
    new-instance v0, Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog;

    const-class v1, Lcom/squareup/register/widgets/ConfirmationStrings;

    .line 56
    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/register/widgets/Confirmation;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog;-><init>(Lcom/squareup/register/widgets/Confirmation;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 50
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/settings/InSettingsAppletScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog;->confirmation:Lcom/squareup/register/widgets/Confirmation;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
