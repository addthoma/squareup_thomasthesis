.class final Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState$1;
.super Ljava/lang/Object;
.source "EditTicketGroupState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;
    .locals 1

    .line 249
    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    invoke-direct {v0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 247
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;
    .locals 0

    .line 253
    new-array p1, p1, [Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 247
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState$1;->newArray(I)[Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;

    move-result-object p1

    return-object p1
.end method
