.class public Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsCardPresenter;
.source "PredefinedTicketsOptInScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsCardPresenter<",
        "Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInView;",
        ">;"
    }
.end annotation


# instance fields
.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 48
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/SettingsCardPresenter;-><init>(Lcom/squareup/util/Device;Lflow/Flow;)V

    .line 49
    iput-object p3, p0, Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen$Presenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 50
    iput-object p4, p0, Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen$Presenter;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->predefined_tickets_opt_in_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method onOptInClicked()V
    .locals 3

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScreen;-><init>(I)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onUpClicked()V
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen$Presenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/tickets/OpenTicketsSettings;->setPredefinedTicketsEnabled(Z)V

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 54
    const-class v0, Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen;

    return-object v0
.end method
