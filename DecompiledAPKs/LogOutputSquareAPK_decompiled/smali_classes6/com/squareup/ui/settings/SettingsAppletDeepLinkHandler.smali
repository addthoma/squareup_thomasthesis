.class public Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;
.super Ljava/lang/Object;
.source "SettingsAppletDeepLinkHandler.java"

# interfaces
.implements Lcom/squareup/deeplinks/DeepLinkHandler;


# instance fields
.field private final bankAccountSection:Lcom/squareup/ui/settings/bankaccount/BankAccountSection;

.field private final depositsSection:Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final quickAmountsSection:Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/ui/settings/instantdeposits/DepositsSection;Lcom/squareup/ui/settings/bankaccount/BankAccountSection;Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;->features:Lcom/squareup/settings/server/Features;

    .line 52
    iput-object p2, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;->depositsSection:Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

    .line 53
    iput-object p3, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;->bankAccountSection:Lcom/squareup/ui/settings/bankaccount/BankAccountSection;

    .line 54
    iput-object p4, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;->quickAmountsSection:Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;

    return-void
.end method


# virtual methods
.method public handleExternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 13

    .line 58
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x5

    const/4 v3, 0x6

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, -0x1

    const/4 v7, 0x2

    const/4 v8, 0x1

    const/4 v9, 0x0

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v1, "settings"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_1
    const-string v1, "square-alternative.app.link"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_2
    const-string v1, "root"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_3
    const-string v1, "squareup.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_4
    const-string v1, "square.test-app.link"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_5
    const-string v1, "bnc.lt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_6
    const-string v1, "square.app.link"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :cond_0
    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto/16 :goto_6

    .line 69
    :pswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 70
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 71
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    new-array v0, v9, [Lcom/squareup/permissions/Permission;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/SettingsHistoryFactory;->withPermissions(Ljava/util/List;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 73
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/16 v10, 0x8

    const/4 v11, 0x7

    const/16 v12, 0x9

    sparse-switch v1, :sswitch_data_1

    goto/16 :goto_2

    :sswitch_7
    const-string v1, "/dashboard/business/verify-bank-account"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x5

    goto :goto_3

    :sswitch_8
    const-string v1, "/instant-deposit"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_3

    :sswitch_9
    const-string v1, "/r12Pairing"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_3

    :sswitch_a
    const-string v1, "/instant-deposit-test"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    goto :goto_3

    :sswitch_b
    const-string v1, "/dashboard/business/verify-debit-card"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x6

    goto :goto_3

    :sswitch_c
    const-string v1, "/link-bank-account"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x8

    goto :goto_3

    :sswitch_d
    const-string v1, "/linkBankAccount"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x7

    goto :goto_3

    :sswitch_e
    const-string v1, "/navigate"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x9

    goto :goto_3

    :sswitch_f
    const-string v1, "/r12-pairing-test"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    goto :goto_3

    :sswitch_10
    const-string v1, "/r12-pairing"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_3

    :cond_2
    :goto_2
    const/4 v0, -0x1

    :goto_3
    const-string v1, "confirmation-token"

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_6

    :pswitch_1
    const-string v0, "navigationID"

    .line 117
    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 118
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    goto/16 :goto_4

    :sswitch_11
    const-string v0, "signatureSettings"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 v2, 0x2

    goto/16 :goto_5

    :sswitch_12
    const-string v0, "businessInformation"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/16 v2, 0x8

    goto/16 :goto_5

    :sswitch_13
    const-string v0, "instantDepositSettings"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 v2, 0x0

    goto :goto_5

    :sswitch_14
    const-string v0, "bankAccountSettings"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 v2, 0x1

    goto :goto_5

    :sswitch_15
    const-string v0, "openTicketSettings"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 v2, 0x4

    goto :goto_5

    :sswitch_16
    const-string v0, "storedPaymentSettings"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/16 v2, 0x9

    goto :goto_5

    :sswitch_17
    const-string v0, "quickAmountsSettings"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/16 v2, 0xb

    goto :goto_5

    :sswitch_18
    const-string v0, "tipSettings"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 v2, 0x6

    goto :goto_5

    :sswitch_19
    const-string v0, "taxSettings"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_5

    :sswitch_1a
    const-string v0, "customerManagementSettings"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 v2, 0x3

    goto :goto_5

    :sswitch_1b
    const-string v0, "paymentDeviceSettings"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 v2, 0x7

    goto :goto_5

    :sswitch_1c
    const-string v0, "cashManagementSettings"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/16 v2, 0xa

    goto :goto_5

    :cond_3
    :goto_4
    const/4 v2, -0x1

    :goto_5
    packed-switch v2, :pswitch_data_2

    goto/16 :goto_6

    .line 167
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;->quickAmountsSection:Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 168
    sget-object p1, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsBootstrapScreen;->INSTANCE:Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsBootstrapScreen;

    new-array v0, v8, [Lcom/squareup/permissions/Permission;

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_CHECKOUT_SETTINGS:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v9

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/SettingsHistoryFactory;->withPermissions(Lcom/squareup/container/ContainerTreeKey;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 172
    :cond_4
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->INACCESSIBLE:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1

    .line 162
    :pswitch_3
    new-instance p1, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen;

    sget-object v0, Lcom/squareup/ui/settings/SettingsAppletScope;->INSTANCE:Lcom/squareup/ui/settings/SettingsAppletScope;

    invoke-direct {p1, v0}, Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    new-array v0, v8, [Lcom/squareup/permissions/Permission;

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_CHECKOUT_SETTINGS:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v9

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/SettingsHistoryFactory;->withPermissions(Lcom/squareup/container/ContainerTreeKey;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 158
    :pswitch_4
    sget-object p1, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen;

    new-array v0, v7, [Lcom/squareup/permissions/Permission;

    sget-object v1, Lcom/squareup/permissions/Permission;->CHANGE_OFFLINE_MODE_SETTING:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_CHECKOUT_SETTINGS:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v8

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/SettingsHistoryFactory;->withPermissions(Lcom/squareup/container/ContainerTreeKey;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 155
    :pswitch_5
    sget-object p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileScreen;->INSTANCE:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileScreen;

    new-array v0, v8, [Lcom/squareup/permissions/Permission;

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_RECEIPT_INFORMATION:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v9

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/SettingsHistoryFactory;->withPermissions(Lcom/squareup/container/ContainerTreeKey;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 152
    :pswitch_6
    sget-object p1, Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen;

    new-array v0, v8, [Lcom/squareup/permissions/Permission;

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_HARDWARE_SETTINGS:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v9

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/SettingsHistoryFactory;->withPermissions(Lcom/squareup/container/ContainerTreeKey;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 149
    :pswitch_7
    sget-object p1, Lcom/squareup/ui/settings/tipping/TipSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/tipping/TipSettingsScreen;

    new-array v0, v8, [Lcom/squareup/permissions/Permission;

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_CHECKOUT_SETTINGS:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v9

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/SettingsHistoryFactory;->withPermissions(Lcom/squareup/container/ContainerTreeKey;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 146
    :pswitch_8
    sget-object p1, Lcom/squareup/ui/settings/taxes/TaxesListScreen;->INSTANCE:Lcom/squareup/ui/settings/taxes/TaxesListScreen;

    new-array v0, v8, [Lcom/squareup/permissions/Permission;

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_CHECKOUT_SETTINGS:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v9

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/SettingsHistoryFactory;->withPermissions(Lcom/squareup/container/ContainerTreeKey;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 142
    :pswitch_9
    sget-object p1, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen;

    new-array v0, v8, [Lcom/squareup/permissions/Permission;

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_CHECKOUT_SETTINGS:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v9

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/SettingsHistoryFactory;->withPermissions(Lcom/squareup/container/ContainerTreeKey;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 138
    :pswitch_a
    sget-object p1, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen;

    new-array v0, v7, [Lcom/squareup/permissions/Permission;

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_CUSTOMERS:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_CHECKOUT_SETTINGS:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v8

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/SettingsHistoryFactory;->withPermissions(Lcom/squareup/container/ContainerTreeKey;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 134
    :pswitch_b
    sget-object p1, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen;

    new-array v0, v8, [Lcom/squareup/permissions/Permission;

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_CHECKOUT_SETTINGS:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v9

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/SettingsHistoryFactory;->withPermissions(Lcom/squareup/container/ContainerTreeKey;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 127
    :pswitch_c
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;->bankAccountSection:Lcom/squareup/ui/settings/bankaccount/BankAccountSection;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 128
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;->bankAccountSection:Lcom/squareup/ui/settings/bankaccount/BankAccountSection;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSection;->getInitialScreen()Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;

    move-result-object p1

    new-array v0, v8, [Lcom/squareup/permissions/Permission;

    sget-object v1, Lcom/squareup/permissions/Permission;->VIEW_BANK_ACCOUNT_INFORMATION:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v9

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/SettingsHistoryFactory;->withPermissions(Lcom/squareup/container/ContainerTreeKey;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 131
    :cond_5
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->INACCESSIBLE:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1

    .line 120
    :pswitch_d
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;->depositsSection:Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositsSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 121
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;->depositsSection:Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositsSection;->getInitialScreen()Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;

    move-result-object p1

    new-array v0, v8, [Lcom/squareup/permissions/Permission;

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_BALANCE_SETTINGS:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v9

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/SettingsHistoryFactory;->withPermissions(Lcom/squareup/container/ContainerTreeKey;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 124
    :cond_6
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->INACCESSIBLE:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1

    .line 110
    :pswitch_e
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->BANK_ACCOUNT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 111
    new-instance p1, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;

    invoke-direct {p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;-><init>()V

    new-array v0, v7, [Lcom/squareup/permissions/Permission;

    sget-object v1, Lcom/squareup/permissions/Permission;->CREATE_BANK_ACCOUNT:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/permissions/Permission;->VIEW_BANK_ACCOUNT_INFORMATION:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v8

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/SettingsHistoryFactory;->withPermissions(Lcom/squareup/container/ContainerTreeKey;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 114
    :cond_7
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->INACCESSIBLE:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1

    .line 95
    :pswitch_f
    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "unit-token"

    .line 96
    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 97
    iget-object v1, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;->depositsSection:Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/instantdeposits/DepositsSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 98
    new-instance v1, Lcom/squareup/debitcard/VerifyCardChangeProps;

    invoke-direct {v1, v0, p1}, Lcom/squareup/debitcard/VerifyCardChangeProps;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-array p1, v7, [Lcom/squareup/container/ContainerTreeKey;

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;->depositsSection:Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

    .line 100
    invoke-virtual {v0}, Lcom/squareup/ui/settings/instantdeposits/DepositsSection;->getInitialScreen()Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;

    move-result-object v0

    aput-object v0, p1, v9

    new-instance v0, Lcom/squareup/ui/settings/instantdeposits/VerifyCardChangeBootstrapScreen;

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/instantdeposits/VerifyCardChangeBootstrapScreen;-><init>(Lcom/squareup/debitcard/VerifyCardChangeProps;)V

    aput-object v0, p1, v8

    .line 99
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-array v0, v8, [Lcom/squareup/permissions/Permission;

    .line 103
    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_BALANCE_SETTINGS:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v9

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/SettingsHistoryFactory;->withPermissions(Ljava/util/List;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 105
    :cond_8
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->INACCESSIBLE:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1

    .line 87
    :pswitch_10
    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BANK_ACCOUNT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 89
    new-instance v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;-><init>(Ljava/lang/String;)V

    new-array p1, v7, [Lcom/squareup/permissions/Permission;

    sget-object v1, Lcom/squareup/permissions/Permission;->CREATE_BANK_ACCOUNT:Lcom/squareup/permissions/Permission;

    aput-object v1, p1, v9

    sget-object v1, Lcom/squareup/permissions/Permission;->VIEW_BANK_ACCOUNT_INFORMATION:Lcom/squareup/permissions/Permission;

    aput-object v1, p1, v8

    invoke-static {v0, p1}, Lcom/squareup/ui/settings/SettingsHistoryFactory;->withPermissions(Lcom/squareup/container/ContainerTreeKey;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 92
    :cond_9
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->INACCESSIBLE:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1

    .line 81
    :pswitch_11
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;->depositsSection:Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositsSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result p1

    if-eqz p1, :cond_a

    .line 82
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;->depositsSection:Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/instantdeposits/DepositsSection;->getInitialScreen()Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;

    move-result-object p1

    new-array v0, v8, [Lcom/squareup/permissions/Permission;

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_BALANCE_SETTINGS:Lcom/squareup/permissions/Permission;

    aput-object v1, v0, v9

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/SettingsHistoryFactory;->withPermissions(Lcom/squareup/container/ContainerTreeKey;[Lcom/squareup/permissions/Permission;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    .line 84
    :cond_a
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->INACCESSIBLE:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1

    .line 77
    :pswitch_12
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    new-instance v0, Lcom/squareup/ui/settings/PairingHistoryFactory;

    invoke-direct {v0}, Lcom/squareup/ui/settings/PairingHistoryFactory;-><init>()V

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    return-object p1

    .line 176
    :goto_6
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6d54daa8 -> :sswitch_6
        -0x52893ec1 -> :sswitch_5
        -0x4348448f -> :sswitch_4
        -0xfbdfcf5 -> :sswitch_3
        0x3580e2 -> :sswitch_2
        0x1284b78 -> :sswitch_1
        0x5582bc23 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        -0x7e4cc281 -> :sswitch_10
        -0x76cd3fe0 -> :sswitch_f
        -0x4a1e1960 -> :sswitch_e
        -0x25c74c18 -> :sswitch_d
        -0x227c49c0 -> :sswitch_c
        -0x158ce260 -> :sswitch_b
        -0xc19d684 -> :sswitch_a
        -0x2c5255c -> :sswitch_9
        0x23e26aa3 -> :sswitch_8
        0x4292022b -> :sswitch_7
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_11
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_e
        :pswitch_1
    .end packed-switch

    :sswitch_data_2
    .sparse-switch
        -0x71a069e7 -> :sswitch_1c
        -0x52f07bc1 -> :sswitch_1b
        -0x3a1719fc -> :sswitch_1a
        -0x1f19fb12 -> :sswitch_19
        -0x1ef1ea22 -> :sswitch_18
        -0x1e67154f -> :sswitch_17
        0x2e366626 -> :sswitch_16
        0x2f8ce839 -> :sswitch_15
        0x70161f94 -> :sswitch_14
        0x7734acc0 -> :sswitch_13
        0x7e3a054c -> :sswitch_12
        0x7f7280fb -> :sswitch_11
    .end sparse-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
