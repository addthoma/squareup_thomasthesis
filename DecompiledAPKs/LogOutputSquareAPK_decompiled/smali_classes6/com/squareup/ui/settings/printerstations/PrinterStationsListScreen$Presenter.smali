.class public Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "PrinterStationsListScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final disabledStations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation
.end field

.field private final disposable:Lio/reactivex/disposables/SerialDisposable;

.field private final enabledStations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;

.field private final hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final res:Lcom/squareup/util/Res;

.field private final sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/settings/SidebarRefresher;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 76
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->enabledStations:Ljava/util/List;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->disabledStations:Ljava/util/List;

    .line 70
    new-instance v0, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->disposable:Lio/reactivex/disposables/SerialDisposable;

    .line 77
    iput-object p3, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 78
    iput-object p2, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 79
    iput-object p4, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    .line 80
    iput-object p5, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 81
    iput-object p6, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    .line 82
    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->getFlow()Lflow/Flow;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->flow:Lflow/Flow;

    return-void
.end method

.method private getDisabledPrinterStation(I)Lcom/squareup/print/PrinterStation;
    .locals 1

    add-int/lit8 p1, p1, -0x1

    .line 287
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getViewCountForEnabledStations()I

    move-result v0

    sub-int/2addr p1, v0

    add-int/lit8 p1, p1, -0x1

    .line 288
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->disabledStations:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/PrinterStation;

    return-object p1
.end method

.method private getDisabledReason(I)Ljava/lang/String;
    .locals 1

    .line 306
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getPrinterStation(I)Lcom/squareup/print/PrinterStation;

    move-result-object p1

    .line 307
    invoke-virtual {p1}, Lcom/squareup/print/PrinterStation;->hasHardwarePrinterSelected()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 308
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/settingsapplet/R$string;->printer_station_no_options_selected:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 310
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/settingsapplet/R$string;->printer_station_no_printer_selected:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getEnabledPrinterStation(I)Lcom/squareup/print/PrinterStation;
    .locals 1

    add-int/lit8 p1, p1, -0x1

    add-int/lit8 p1, p1, -0x1

    .line 280
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->enabledStations:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/PrinterStation;

    return-object p1
.end method

.method private getHardwarePrinterName(Lcom/squareup/print/PrinterStation;)Ljava/lang/String;
    .locals 1

    .line 221
    invoke-virtual {p1}, Lcom/squareup/print/PrinterStation;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object p1

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {v0, p1}, Lcom/squareup/print/HardwarePrinterTracker;->getCachedHardwareInfo(Ljava/lang/String;)Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object p1

    .line 223
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getDisplayableModelName()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getSelectedRoles(I)Ljava/lang/String;
    .locals 7

    .line 292
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getPrinterStation(I)Lcom/squareup/print/PrinterStation;

    move-result-object p1

    .line 293
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 294
    invoke-static {}, Lcom/squareup/print/PrinterStation$Role;->values()[Lcom/squareup/print/PrinterStation$Role;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_1

    aget-object v5, v1, v4

    const/4 v6, 0x1

    new-array v6, v6, [Lcom/squareup/print/PrinterStation$Role;

    aput-object v5, v6, v3

    .line 295
    invoke-virtual {p1, v6}, Lcom/squareup/print/PrinterStation;->hasAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 296
    iget-object v6, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->res:Lcom/squareup/util/Res;

    iget v5, v5, Lcom/squareup/print/PrinterStation$Role;->stringResIdPlural:I

    invoke-interface {v6, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 300
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/utilities/R$string;->list_pattern_short:I

    .line 301
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/ListPhrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/util/ListPhrase;

    move-result-object p1

    .line 302
    invoke-virtual {p1, v0}, Lcom/squareup/util/ListPhrase;->format(Ljava/lang/Iterable;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getViewCountForDisabledStations()I
    .locals 1

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->disabledStations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->disabledStations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private getViewCountForEnabledStations()I
    .locals 1

    .line 261
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->enabledStations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->enabledStations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private inEnabledStationRange(I)Z
    .locals 3

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->enabledStations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    .line 253
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getViewCountForEnabledStations()I

    move-result v0

    const/4 v2, 0x1

    add-int/2addr v0, v2

    if-ge p1, v0, :cond_1

    return v2

    :cond_1
    :goto_0
    return v1
.end method

.method public static synthetic lambda$mglXrb6otX6usR-sehGZIZulDYs(Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;Ljava/util/Collection;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->rebuildStationsListsAndRefresh(Ljava/util/Collection;)V

    return-void
.end method

.method private rebuildStationsListsAndRefresh(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;)V"
        }
    .end annotation

    .line 228
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->enabledStations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->disabledStations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 230
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/PrinterStation;

    .line 231
    invoke-virtual {v0}, Lcom/squareup/print/PrinterStation;->getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object v1

    iget-boolean v1, v1, Lcom/squareup/print/PrinterStationConfiguration;->internal:Z

    if-eqz v1, :cond_0

    goto :goto_0

    .line 235
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/print/PrinterStation;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 236
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->enabledStations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 238
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->disabledStations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 241
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 242
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->refresh()V

    .line 244
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    return-void
.end method


# virtual methods
.method public bridge synthetic dropView(Landroid/view/ViewGroup;)V
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->dropView(Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;)V

    return-void
.end method

.method public dropView(Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;)V
    .locals 2

    .line 93
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->disposable:Lio/reactivex/disposables/SerialDisposable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    .line 96
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->dropView(Landroid/view/ViewGroup;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->dropView(Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;)V

    return-void
.end method

.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;->TITLE_ID:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getHeaderText(I)Ljava/lang/String;
    .locals 3

    .line 129
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 133
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->inEnabledStationRange(I)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 134
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/settingsapplet/R$string;->printer_stations_enabled_uppercase:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 136
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/settingsapplet/R$string;->printer_stations_disabled_uppercase:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 130
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getHeaderText called for index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method getItemViewType(I)I
    .locals 3

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 191
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->inEnabledStationRange(I)Z

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    if-ne p1, v2, :cond_1

    return v2

    :cond_1
    return v1

    .line 202
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getViewCountForEnabledStations()I

    move-result v0

    add-int/2addr v0, v2

    if-ne p1, v0, :cond_3

    return v2

    :cond_3
    return v1
.end method

.method getPrinterStation(I)Lcom/squareup/print/PrinterStation;
    .locals 3

    .line 210
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 211
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->inEnabledStationRange(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getEnabledPrinterStation(I)Lcom/squareup/print/PrinterStation;

    move-result-object p1

    return-object p1

    .line 214
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getDisabledPrinterStation(I)Lcom/squareup/print/PrinterStation;

    move-result-object p1

    return-object p1

    .line 217
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getItem() called for index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method getPrinterStationSubtitleText(I)Ljava/lang/String;
    .locals 3

    .line 173
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 178
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getPrinterStation(I)Lcom/squareup/print/PrinterStation;

    move-result-object p1

    .line 179
    invoke-virtual {p1}, Lcom/squareup/print/PrinterStation;->hasHardwarePrinterSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getHardwarePrinterName(Lcom/squareup/print/PrinterStation;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1

    .line 174
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getPrinterStationSubtitleText called for index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method getPrinterStationTitleText(I)Ljava/lang/String;
    .locals 3

    .line 141
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 146
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getPrinterStation(I)Lcom/squareup/print/PrinterStation;

    move-result-object p1

    .line 148
    invoke-virtual {p1}, Lcom/squareup/print/PrinterStation;->hasHardwarePrinterSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getDevice()Lcom/squareup/util/Device;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->printer_stations_list_title_phrase:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 153
    invoke-virtual {p1}, Lcom/squareup/print/PrinterStation;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "printer_station_name"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 154
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getHardwarePrinterName(Lcom/squareup/print/PrinterStation;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "hardware_printer_name"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 155
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 156
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 149
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/print/PrinterStation;->getName()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 142
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getPrinterStationTitleText called for index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method getPrinterStationValueText(I)Ljava/lang/String;
    .locals 3

    .line 160
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 165
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->inEnabledStationRange(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getSelectedRoles(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 168
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getDisabledReason(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 161
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getPrinterStationValueText called for index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method getViewCount()I
    .locals 2

    .line 125
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getViewCountForEnabledStations()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getViewCountForDisabledStations()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method onCreatePrinterStationClicked()V
    .locals 3

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen;

    new-instance v2, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;

    invoke-direct {v2}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;-><init>()V

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PRINTER_STATION_CREATE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 86
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 87
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->initialize()V

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->disposable:Lio/reactivex/disposables/SerialDisposable;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-interface {v0}, Lcom/squareup/print/PrinterStations;->allStations()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/printerstations/-$$Lambda$PrinterStationsListScreen$Presenter$mglXrb6otX6usR-sehGZIZulDYs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/printerstations/-$$Lambda$PrinterStationsListScreen$Presenter$mglXrb6otX6usR-sehGZIZulDYs;-><init>(Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;)V

    .line 89
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 88
    invoke-virtual {p1, v0}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method onPrinterStationRowClicked(I)V
    .locals 2

    .line 117
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;

    .line 118
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getPrinterStation(I)Lcom/squareup/print/PrinterStation;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/print/PrinterStation;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;-><init>(Ljava/lang/String;)V

    .line 119
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen;

    invoke-direct {v1, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;)V

    invoke-virtual {p1, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 120
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->PRINTER_STATION:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method protected saveSettings()V
    .locals 0

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 100
    const-class v0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen;

    return-object v0
.end method
