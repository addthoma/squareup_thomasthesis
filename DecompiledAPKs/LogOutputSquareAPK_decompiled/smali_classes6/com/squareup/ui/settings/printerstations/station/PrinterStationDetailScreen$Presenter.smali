.class public Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "PrinterStationDetailScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;",
        ">;"
    }
.end annotation


# static fields
.field private static final NAME_ERROR_KEY:Ljava/lang/String; = "name"


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final appNameFormatter:Lcom/squareup/util/AppNameFormatter;

.field private autoNumberCanceled:Z

.field private categories:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final device:Lcom/squareup/util/Device;

.field private final errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

.field private final isNewPrinterStation:Z

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final orderTicketPrintingSettings:Lcom/squareup/print/OrderTicketPrintingSettings;

.field private final paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

.field private final printSettings:Lcom/squareup/print/PrintSettings;

.field private final printerRoleSupportChecker:Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;

.field private final printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

.field private final scopeRunner:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

.field private final selectedPrinterStation:Lcom/squareup/print/PrinterStation;

.field private selectedTicketNamingOption:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

.field private final testPrint:Lcom/squareup/ui/settings/printerstations/station/TestPrint;

.field private final ticketAutoNumberingEnabled:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/PrinterStations;Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/cogs/Cogs;Lcom/squareup/analytics/Analytics;ZLcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;Lcom/squareup/ui/settings/printerstations/station/TestPrint;Lcom/squareup/print/PrintSettings;Lcom/squareup/util/AppNameFormatter;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/print/OrderTicketPrintingSettings;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            "Lcom/squareup/print/PrinterStation;",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/analytics/Analytics;",
            "Z",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;",
            "Lcom/squareup/ui/settings/printerstations/station/TestPrint;",
            "Lcom/squareup/print/PrintSettings;",
            "Lcom/squareup/util/AppNameFormatter;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/print/OrderTicketPrintingSettings;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 136
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    move-object v1, p1

    .line 137
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    move-object v1, p5

    .line 138
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->scopeRunner:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

    move-object v1, p3

    .line 139
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    move-object v1, p6

    .line 140
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    move-object v1, p7

    .line 141
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->cogs:Lcom/squareup/cogs/Cogs;

    move-object v1, p2

    .line 142
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    move-object v1, p4

    .line 143
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStations:Lcom/squareup/print/PrinterStations;

    move-object v1, p8

    .line 144
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, p10

    .line 145
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->ticketAutoNumberingEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    move v1, p9

    .line 146
    iput-boolean v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->isNewPrinterStation:Z

    move-object v1, p11

    .line 147
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    move-object v1, p13

    .line 148
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    move-object v1, p12

    .line 149
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    move-object/from16 v1, p14

    .line 150
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerRoleSupportChecker:Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;

    move-object/from16 v1, p15

    .line 151
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->device:Lcom/squareup/util/Device;

    move-object/from16 v1, p16

    .line 152
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object/from16 v1, p17

    .line 153
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->runner:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

    move-object/from16 v1, p18

    .line 154
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->testPrint:Lcom/squareup/ui/settings/printerstations/station/TestPrint;

    move-object/from16 v1, p19

    .line 155
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printSettings:Lcom/squareup/print/PrintSettings;

    move-object/from16 v1, p20

    .line 156
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    move-object/from16 v1, p21

    .line 157
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    move-object/from16 v1, p22

    .line 158
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->orderTicketPrintingSettings:Lcom/squareup/print/OrderTicketPrintingSettings;

    const/4 v1, 0x0

    .line 160
    iput-boolean v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->autoNumberCanceled:Z

    return-void
.end method

.method private clearError()V
    .locals 2

    .line 647
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    const-string v1, "name"

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->removeError(Ljava/lang/String;)Z

    return-void
.end method

.method private getSelectedHardwareInfoOrNull()Lcom/squareup/print/HardwarePrinter$HardwareInfo;
    .locals 2

    .line 633
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->isHardwarePrinterSelected()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 637
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v1, v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    .line 638
    invoke-virtual {v1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object v1

    .line 637
    invoke-virtual {v0, v1}, Lcom/squareup/print/HardwarePrinterTracker;->getCachedHardwareInfo(Ljava/lang/String;)Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v0

    return-object v0
.end method

.method private hideCategories()V
    .locals 2

    .line 552
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setNoCategoriesMessageVisible(Z)V

    .line 553
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setCategoriesProgressVisible(Z)V

    .line 554
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setIncludeOnTicketVisible(Z)V

    return-void
.end method

.method private hidePrintCompactTicketsSwitch(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;)V
    .locals 0

    .line 349
    invoke-virtual {p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->hidePrintCompactTicketsSwitch()V

    return-void
.end method

.method private isAutoNumberingSelected()Z
    .locals 2

    .line 629
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedTicketNamingOption:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    sget-object v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->AUTO_NUMBER:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isHardwarePrinterSelected()Z
    .locals 2

    .line 642
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {v0}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/squareup/print/PrinterStationConfiguration;->NO_PRINTER_SELECTED_ID:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private isInternalPrinter()Z
    .locals 1

    .line 651
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    invoke-virtual {v0}, Lcom/squareup/print/PrinterStation;->getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/print/PrinterStationConfiguration;->internal:Z

    return v0
.end method

.method private static libraryCursorAsList(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;"
        }
    .end annotation

    .line 461
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getCount()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 464
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 465
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 468
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    .line 469
    throw v0
.end method

.method private logSettingsChangedBeforeCommitting()V
    .locals 9

    .line 568
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getSelectedHardwareInfoOrNull()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v0

    .line 571
    invoke-static {}, Lcom/squareup/print/PrinterStation$Role;->values()[Lcom/squareup/print/PrinterStation$Role;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    const/4 v5, 0x1

    if-ge v4, v2, :cond_1

    aget-object v6, v1, v4

    .line 572
    iget-object v7, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    new-array v5, v5, [Lcom/squareup/print/PrinterStation$Role;

    aput-object v6, v5, v3

    invoke-virtual {v7, v5}, Lcom/squareup/print/PrinterStation;->hasAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v5

    .line 573
    iget-object v7, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v7, v7, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {v7}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->getSelectedRoles()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eq v5, v7, :cond_0

    .line 575
    iget-object v7, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v8, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    .line 576
    invoke-static {v8, v0, v6, v5}, Lcom/squareup/print/PrinterStationEvent;->forPrinterRoleSettingsToggle(Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Lcom/squareup/print/PrinterStation$Role;Z)Lcom/squareup/print/PrinterStationEvent;

    move-result-object v5

    .line 575
    invoke-interface {v7, v5}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 581
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->isAutoNumberingSelected()Z

    move-result v1

    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->ticketAutoNumberingEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v2}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eq v1, v2, :cond_2

    .line 582
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    .line 583
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->isAutoNumberingSelected()Z

    move-result v4

    invoke-static {v2, v0, v4}, Lcom/squareup/print/PrinterStationEvent;->forTicketAutoNumberToggle(Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Z)Lcom/squareup/print/PrinterStationEvent;

    move-result-object v2

    .line 582
    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 585
    :cond_2
    iget-boolean v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->autoNumberCanceled:Z

    if-eqz v1, :cond_3

    .line 586
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    .line 587
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->isAutoNumberingSelected()Z

    move-result v4

    .line 586
    invoke-static {v2, v0, v4}, Lcom/squareup/print/PrinterStationEvent;->forTicketAutoNumberToggleCancel(Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Z)Lcom/squareup/print/PrinterStationEvent;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 591
    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    invoke-virtual {v1}, Lcom/squareup/print/PrinterStation;->getDisabledCategoryIds()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 592
    iget-object v4, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v4, v4, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {v4}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->getDisabledCategoryIds()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 593
    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v4, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    invoke-static {v4, v0, v5}, Lcom/squareup/print/PrinterStationEvent;->forPrinterCategoryToggle(Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Z)Lcom/squareup/print/PrinterStationEvent;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_1

    .line 596
    :cond_5
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v1, v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {v1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->getDisabledCategoryIds()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 597
    iget-object v4, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    invoke-virtual {v4, v2}, Lcom/squareup/print/PrinterStation;->isEnabledForCategoryId(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 598
    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v4, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    invoke-static {v4, v0, v3}, Lcom/squareup/print/PrinterStationEvent;->forPrinterCategoryToggle(Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Z)Lcom/squareup/print/PrinterStationEvent;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_2

    .line 601
    :cond_7
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v1, v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    .line 602
    invoke-virtual {v1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->isPrintsUncategorizedItemsEnabled()Z

    move-result v1

    .line 603
    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    invoke-virtual {v2}, Lcom/squareup/print/PrinterStation;->printsUncategorizedItems()Z

    move-result v2

    if-eq v1, v2, :cond_8

    .line 605
    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v3, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    invoke-static {v3, v0, v1}, Lcom/squareup/print/PrinterStationEvent;->forPrinterUncategorizedItemsToggle(Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Z)Lcom/squareup/print/PrinterStationEvent;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 609
    :cond_8
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v1, v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    .line 610
    invoke-virtual {v1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->isAutoPrintItemizedReceiptsEnabled()Z

    move-result v1

    .line 611
    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    .line 612
    invoke-virtual {v2}, Lcom/squareup/print/PrinterStation;->isAutoPrintItemizedReceiptsEnabled()Z

    move-result v2

    if-eq v1, v2, :cond_9

    .line 614
    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v3, Lcom/squareup/analytics/event/v1/BooleanToggleEvent;

    sget-object v4, Lcom/squareup/analytics/RegisterActionName;->AUTOMATICALLY_PRINT_ITEMIZED_RECEIPT_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v3, v4, v1}, Lcom/squareup/analytics/event/v1/BooleanToggleEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Z)V

    invoke-interface {v2, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 618
    :cond_9
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v1, v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    .line 619
    invoke-virtual {v1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->isPrintATicketForEachItemEnabled()Z

    move-result v1

    .line 620
    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    .line 621
    invoke-virtual {v2}, Lcom/squareup/print/PrinterStation;->isPrintATicketForEachItemEnabled()Z

    move-result v2

    if-eq v1, v2, :cond_a

    .line 623
    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v3, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    invoke-static {v3, v0, v1}, Lcom/squareup/print/PrinterStationEvent;->forPrintATicketForEachItemToggle(Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Z)Lcom/squareup/print/PrinterStationEvent;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_a
    return-void
.end method

.method private maybeShowPrintCompactTicketsSwitch(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;)V
    .locals 2

    .line 340
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printSettings:Lcom/squareup/print/PrintSettings;

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    invoke-interface {v0, v1}, Lcom/squareup/print/PrintSettings;->canPrintCompactTickets(Lcom/squareup/print/PrinterStation;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {v0}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->isPrintCompactTickets()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->showPrintCompactTicketsSwitch(Z)V

    :cond_0
    return-void
.end method

.method private showCategoriesEmpty()V
    .locals 2

    .line 540
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setNoCategoriesMessageVisible(Z)V

    .line 541
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setCategoriesProgressVisible(Z)V

    .line 542
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setIncludeOnTicketVisible(Z)V

    return-void
.end method

.method private showCategoriesLoading()V
    .locals 3

    .line 534
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setNoCategoriesMessageVisible(Z)V

    .line 535
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setCategoriesProgressVisible(Z)V

    .line 536
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setIncludeOnTicketVisible(Z)V

    return-void
.end method

.method private showCategoriesNonEmpty()V
    .locals 2

    .line 546
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setNoCategoriesMessageVisible(Z)V

    .line 547
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setCategoriesProgressVisible(Z)V

    .line 548
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setIncludeOnTicketVisible(Z)V

    return-void
.end method

.method private updateCategoriesOptions(Z)V
    .locals 4

    .line 501
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    .line 503
    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setLayoutTransitionEnabled(Z)V

    .line 505
    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->getCheckedRoles()Ljava/util/Set;

    move-result-object p1

    .line 506
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerRoleSupportChecker:Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;

    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getSelectedHardwareInfoOrNull()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;->canPrintReceipts(Lcom/squareup/print/HardwarePrinter$HardwareInfo;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 507
    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-interface {p1, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 509
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerRoleSupportChecker:Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;

    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getSelectedHardwareInfoOrNull()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;->canPrintTicketStubs(Lcom/squareup/print/HardwarePrinter$HardwareInfo;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 510
    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    invoke-interface {p1, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 513
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->updateSelectedPrinterStationWithRoles(Ljava/util/Set;)V

    .line 515
    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-interface {p1, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setPaperSigOptionsVisible(Z)V

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/squareup/print/PrinterStation$Role;

    const/4 v2, 0x0

    .line 516
    sget-object v3, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    aput-object v3, v1, v2

    invoke-static {p1, v1}, Lcom/squareup/util/SquareCollections;->containsAny(Ljava/util/Collection;[Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setTicketNamingOptionsVisible(Z)V

    .line 518
    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    invoke-interface {p1, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 519
    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->maybeShowPrintCompactTicketsSwitch(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;)V

    .line 520
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->categories:Ljava/util/List;

    if-nez p1, :cond_2

    .line 521
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->showCategoriesLoading()V

    goto :goto_0

    .line 522
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-nez p1, :cond_3

    .line 523
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->showCategoriesEmpty()V

    goto :goto_0

    .line 525
    :cond_3
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->showCategoriesNonEmpty()V

    goto :goto_0

    .line 528
    :cond_4
    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->hidePrintCompactTicketsSwitch(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;)V

    .line 529
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->hideCategories()V

    :goto_0
    return-void
.end method

.method private updateRoles()V
    .locals 4

    .line 382
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->isInternalPrinter()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 383
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->getCheckedRoles()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 384
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v3}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->getCheckedRoles()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 385
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v3, v2, v2, v2}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setCheckedRoles(ZZZ)V

    .line 386
    invoke-direct {p0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->updateCategoriesOptions(Z)V

    .line 388
    :cond_0
    iget-object v3, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->orderTicketPrintingSettings:Lcom/squareup/print/OrderTicketPrintingSettings;

    invoke-interface {v3}, Lcom/squareup/print/OrderTicketPrintingSettings;->enabled()Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 389
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v2, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setTicketStubSwitchEnabled(Z)V

    .line 390
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v2, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setTicketSwitchEnabled(Z)V

    .line 391
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v2, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setShowOrderTicketsMessage(Z)V

    .line 392
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setShowTestPrintButton(Z)V

    :cond_2
    return-void
.end method

.method private updateSelectedPrinterStationWithRoles(Ljava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/print/PrinterStation$Role;",
            ">;)V"
        }
    .end annotation

    .line 558
    invoke-static {}, Lcom/squareup/print/PrinterStation$Role;->values()[Lcom/squareup/print/PrinterStation$Role;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 559
    invoke-interface {p1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 560
    iget-object v4, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v4, v4, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {v4, v3}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->addRole(Lcom/squareup/print/PrinterStation$Role;)Lcom/squareup/print/PrinterStationConfiguration$Builder;

    goto :goto_1

    .line 562
    :cond_0
    iget-object v4, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v4, v4, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {v4, v3}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->removeRole(Lcom/squareup/print/PrinterStation$Role;)Lcom/squareup/print/PrinterStationConfiguration$Builder;

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private warnIfCannotSignOnPrintedReceipt()V
    .locals 4

    .line 475
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v0}, Lcom/squareup/papersignature/PaperSignatureSettings;->isSignOnPrintedReceiptEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 479
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {v0}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->getSelectedRoles()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 480
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->getCheckedRoles()Ljava/util/Set;

    move-result-object v1

    sget-object v2, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v0, :cond_3

    if-eqz v1, :cond_1

    goto :goto_0

    .line 487
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v0}, Lcom/squareup/papersignature/PaperSignatureSettings;->getNumberOfReceiptPrinters()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    return-void

    .line 492
    :cond_2
    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings;

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$string;->printer_stations_cannot_sign_on_printed_receipt_title:I

    .line 493
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v3, Lcom/squareup/settingsapplet/R$string;->printer_stations_cannot_sign_on_printed_receipt_content:I

    .line 494
    invoke-interface {v2, v3}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 495
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->scopeRunner:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->showWarning(Lcom/squareup/widgets/warning/WarningStrings;)V

    :cond_3
    :goto_0
    return-void
.end method


# virtual methods
.method commit()V
    .locals 2

    .line 429
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->hideKeyboard()V

    .line 430
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->setName(Ljava/lang/String;)Lcom/squareup/print/PrinterStationConfiguration$Builder;

    .line 432
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerRoleSupportChecker:Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;

    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getSelectedHardwareInfoOrNull()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;->canPrintReceipts(Lcom/squareup/print/HardwarePrinter$HardwareInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-virtual {v0, v1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->removeRole(Lcom/squareup/print/PrinterStation$Role;)Lcom/squareup/print/PrinterStationConfiguration$Builder;

    .line 435
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerRoleSupportChecker:Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;

    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getSelectedHardwareInfoOrNull()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;->canPrintTicketStubs(Lcom/squareup/print/HardwarePrinter$HardwareInfo;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 436
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    invoke-virtual {v0, v1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->removeRole(Lcom/squareup/print/PrinterStation$Role;)Lcom/squareup/print/PrinterStationConfiguration$Builder;

    .line 439
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->logSettingsChangedBeforeCommitting()V

    .line 441
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v1, v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {v1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->build()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/print/PrinterStation;->commit(Lcom/squareup/print/PrinterStationConfiguration;)V

    .line 443
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStations:Lcom/squareup/print/PrinterStations;

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->addNewPrinterStationByUuid(Lcom/squareup/print/PrinterStation;)V

    .line 445
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedTicketNamingOption:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    sget-object v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->AUTO_NUMBER:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 446
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->ticketAutoNumberingEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    .line 447
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->finish()V

    return-void
.end method

.method finish()V
    .locals 1

    .line 455
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->hideKeyboard()V

    .line 456
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->clearError()V

    .line 457
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->scopeRunner:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->goBack()V

    return-void
.end method

.method getActionBarConfig(I)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 1

    const/4 v0, 0x0

    .line 260
    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getActionBarConfig(IZ)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method getActionBarConfig(IZ)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2

    .line 264
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 265
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    if-eqz p2, :cond_0

    .line 267
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, p2, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    const/4 p2, 0x1

    .line 268
    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailScreen$Presenter$ZZ_wJersKlmHvu40Sids8h6qfW8;

    invoke-direct {p2, p0}, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailScreen$Presenter$ZZ_wJersKlmHvu40Sids8h6qfW8;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;)V

    .line 269
    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_0

    .line 271
    :cond_0
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, p2, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$IHuY0KUeOjleSXot9R7K7HpjED0;

    invoke-direct {p2, p0}, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$IHuY0KUeOjleSXot9R7K7HpjED0;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;)V

    .line 272
    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->save:I

    .line 273
    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailScreen$Presenter$8lzZ2vm6EBEWmf3ZzPzrnzeVvmQ;

    invoke-direct {p2, p0}, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailScreen$Presenter$8lzZ2vm6EBEWmf3ZzPzrnzeVvmQ;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;)V

    .line 274
    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 283
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method getCategories()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;"
        }
    .end annotation

    .line 402
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->categories:Ljava/util/List;

    return-object v0
.end method

.method getHardwarePrinterDisplayName()Ljava/lang/String;
    .locals 2

    .line 366
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getSelectedHardwareInfoOrNull()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 368
    invoke-virtual {v0}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getDisplayableModelName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->printer_stations_no_hardware_printer_selected:I

    .line 369
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method getPrintsUncategorizedItems()Z
    .locals 1

    .line 287
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {v0}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->isPrintsUncategorizedItemsEnabled()Z

    move-result v0

    return v0
.end method

.method hardwarePrinterSelectClicked()V
    .locals 1

    .line 360
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->clearError()V

    .line 361
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->hideKeyboard()V

    .line 362
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->scopeRunner:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->goToHardwarePrinterSelect()V

    return-void
.end method

.method isNewPrinterStation()Z
    .locals 1

    .line 425
    iget-boolean v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->isNewPrinterStation:Z

    return v0
.end method

.method isPrintCategoryIdEnabled(Ljava/lang/String;)Z
    .locals 1

    .line 295
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {v0}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->getDisabledCategoryIds()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public synthetic lambda$getActionBarConfig$4$PrinterStationDetailScreen$Presenter()V
    .locals 0

    .line 269
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->commit()V

    return-void
.end method

.method public synthetic lambda$getActionBarConfig$5$PrinterStationDetailScreen$Presenter()V
    .locals 3

    .line 275
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$string;->printer_stations_name_required_error_message:I

    .line 277
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "name"

    .line 276
    invoke-virtual {v0, v2, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 279
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->commit()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$2$PrinterStationDetailScreen$Presenter(Ljava/lang/Boolean;)V
    .locals 2

    .line 242
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 243
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->toggleTicketNamingOption()V

    .line 248
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedTicketNamingOption:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    sget-object v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->AUTO_NUMBER:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 249
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->ticketAutoNumberingEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne p1, v0, :cond_2

    .line 250
    iput-boolean v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->autoNumberCanceled:Z

    :cond_2
    return-void
.end method

.method public synthetic lambda$onLoad$0$PrinterStationDetailScreen$Presenter(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 4

    .line 229
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 230
    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/api/items/Item$Type;

    sget-object v2, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 232
    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v0

    .line 231
    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllNonEmptyCategories(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$1$PrinterStationDetailScreen$Presenter(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 234
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-static {p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->libraryCursorAsList(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->categories:Ljava/util/List;

    .line 235
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 236
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->updateCategoriesList()V

    const/4 p1, 0x1

    .line 237
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->updateCategoriesOptions(Z)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onLoad$3$PrinterStationDetailScreen$Presenter()Lrx/Subscription;
    .locals 2

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->runner:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->getAutoNumberingInEditRelay()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailScreen$Presenter$-So5Z5MplMCbTpPpJ9jCzC1xXKY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailScreen$Presenter$-So5Z5MplMCbTpPpJ9jCzC1xXKY;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method onAutomaticItemizedReceiptChanged(Z)V
    .locals 1

    .line 451
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->setAutoPrintItemizedReceipts(Z)Lcom/squareup/print/PrinterStationConfiguration$Builder;

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 397
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->finish()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 164
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 165
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    iget-object v0, v0, Lcom/squareup/print/PrinterStationConfiguration$Builder;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setName(Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->printer_stations_name_hint:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setNameHint(Ljava/lang/String;)V

    .line 169
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->isInternalPrinter()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/squareup/settingsapplet/R$string;->printer_title:I

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->isNewPrinterStation:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/squareup/settingsapplet/R$string;->printer_station_create_printer_station:I

    goto :goto_0

    :cond_1
    sget v0, Lcom/squareup/settingsapplet/R$string;->printer_station_edit_printer_station:I

    .line 173
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->isInternalPrinter()Z

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getActionBarConfig(IZ)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {v0}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->getSelectedRoles()Ljava/util/Set;

    move-result-object v0

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v1, v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    .line 176
    invoke-virtual {v1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->getSelectedRoles()Ljava/util/Set;

    move-result-object v1

    sget-object v2, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v2, v2, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    .line 177
    invoke-virtual {v2}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->getSelectedRoles()Ljava/util/Set;

    move-result-object v2

    sget-object v3, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 175
    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setCheckedRoles(ZZZ)V

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerRoleSupportChecker:Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;

    .line 180
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getSelectedHardwareInfoOrNull()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;->canPrintReceipts(Lcom/squareup/print/HardwarePrinter$HardwareInfo;)Z

    move-result v0

    .line 179
    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setReceiptSwitchSupported(Z)V

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerRoleSupportChecker:Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;

    .line 182
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getSelectedHardwareInfoOrNull()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;->canPrintTicketStubs(Lcom/squareup/print/HardwarePrinter$HardwareInfo;)Z

    move-result v0

    .line 181
    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setTicketStubSwitchSupported(Z)V

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAllowed()Z

    move-result v0

    .line 186
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->REPORTS_APPLET_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v1, 0x1

    .line 187
    :goto_2
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->isInternalPrinter()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 188
    sget v0, Lcom/squareup/settingsapplet/R$string;->print_allow_printing:I

    .line 189
    sget v1, Lcom/squareup/settingsapplet/R$string;->print_allow_printing_hint:I

    invoke-virtual {p1, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setPrintReceiptHint(I)V

    .line 190
    sget v1, Lcom/squareup/settingsapplet/R$string;->paper_signature_always_print_receipt:I

    invoke-virtual {p1, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setAutomaticItemizedReceiptSwitchLabel(I)V

    goto :goto_3

    :cond_4
    if-eqz v0, :cond_5

    if-eqz v1, :cond_5

    .line 192
    sget v0, Lcom/squareup/settingsapplet/R$string;->print_receipts_value_reports_open_tickets:I

    goto :goto_3

    :cond_5
    if-eqz v0, :cond_6

    .line 194
    sget v0, Lcom/squareup/settingsapplet/R$string;->print_receipts_value_open_tickets:I

    goto :goto_3

    :cond_6
    if-eqz v1, :cond_7

    .line 196
    sget v0, Lcom/squareup/settingsapplet/R$string;->print_receipts_value_reports:I

    goto :goto_3

    .line 198
    :cond_7
    sget v0, Lcom/squareup/settingsapplet/R$string;->print_receipts_value:I

    .line 200
    :goto_3
    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setPrintReceiptText(I)V

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->ticketAutoNumberingEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->AUTO_NUMBER:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    goto :goto_4

    :cond_8
    sget-object v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->CUSTOM_NAME:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    :goto_4
    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedTicketNamingOption:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedTicketNamingOption:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setSelectedNamingOption(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;)V

    .line 207
    invoke-direct {p0, v2}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->updateCategoriesOptions(Z)V

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printSettings:Lcom/squareup/print/PrintSettings;

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    .line 210
    invoke-interface {v0, v1}, Lcom/squareup/print/PrintSettings;->canPrintATicketForEachItem(Lcom/squareup/print/PrinterStation;)Z

    move-result v0

    .line 209
    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setPrintATicketForEachItemVisible(Z)V

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    .line 219
    invoke-virtual {v0}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->isPrintATicketForEachItemEnabled()Z

    move-result v0

    .line 218
    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setPrintATicketForEachItemSwitchChecked(Z)V

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    invoke-virtual {v0}, Lcom/squareup/print/PrinterStation;->isPrintCompactTicketsEnabled()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->onPrintCompactTicketsChanged(Z)V

    .line 223
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->maybeShowPrintCompactTicketsSwitch(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    .line 226
    invoke-virtual {v0}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->isAutoPrintItemizedReceiptsEnabled()Z

    move-result v0

    .line 225
    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setAutomaticItemizedReceiptChecked(Z)V

    .line 228
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailScreen$Presenter$cawyG3khqeFoO1TuPEmIGLYhYxQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailScreen$Presenter$cawyG3khqeFoO1TuPEmIGLYhYxQ;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;)V

    new-instance v2, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailScreen$Presenter$sVYg21n-cbql1aCQlSv5cw_BV2s;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailScreen$Presenter$sVYg21n-cbql1aCQlSv5cw_BV2s;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    .line 241
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailScreen$Presenter$KcofZMNYBw9NEoau5sCJfv5evBM;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationDetailScreen$Presenter$KcofZMNYBw9NEoau5sCJfv5evBM;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 254
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->isInternalPrinter()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setIsInternalPrinter(Z)V

    .line 255
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->isNewPrinterStation()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setIsNewPrinterStation(Z)V

    .line 256
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->updateRoles()V

    return-void
.end method

.method onNamingOptionSelected(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;)V
    .locals 5

    if-eqz p1, :cond_1

    .line 312
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedTicketNamingOption:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    if-ne p1, v0, :cond_0

    return-void

    .line 314
    :cond_0
    new-instance p1, Lcom/squareup/register/widgets/ConfirmationStrings;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->printer_stations_change_settings:I

    .line 315
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$string;->printer_stations_confirm_change_settings:I

    .line 316
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/settingsapplet/R$string;->print_order_tickets_confirm_naming_change:I

    .line 317
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/strings/R$string;->cancel:I

    .line 318
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/squareup/register/widgets/ConfirmationStrings;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->scopeRunner:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->confirmNameOption(Lcom/squareup/register/widgets/ConfirmationStrings;)V

    return-void

    .line 310
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No ticket naming option selected"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method onPrintATicketForEachItemChanged(Z)V
    .locals 1

    .line 331
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->setPrintATicketForEachItem(Z)Lcom/squareup/print/PrinterStationConfiguration$Builder;

    .line 332
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setPrintATicketForEachItemSwitchChecked(Z)V

    return-void
.end method

.method onPrintCompactTicketsChanged(Z)V
    .locals 1

    .line 356
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->setPrintCompactTickets(Z)Lcom/squareup/print/PrinterStationConfiguration$Builder;

    return-void
.end method

.method onRemoveButtonClicked()V
    .locals 2

    .line 407
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->isNewPrinterStation()Z

    move-result v0

    if-nez v0, :cond_1

    .line 409
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->isInternalPrinter()Z

    move-result v0

    if-nez v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStations:Lcom/squareup/print/PrinterStations;

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedPrinterStation:Lcom/squareup/print/PrinterStation;

    invoke-virtual {v1}, Lcom/squareup/print/PrinterStation;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->deletePrinterStation(Ljava/lang/String;)V

    .line 415
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PRINTER_STATION_DELETE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 417
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->finish()V

    return-void

    .line 410
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempting to delete an internal printerStation."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 408
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempting to delete a new printerStation."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method onRolesChanged()V
    .locals 1

    .line 374
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->warnIfCannotSignOnPrintedReceipt()V

    const/4 v0, 0x1

    .line 375
    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->updateCategoriesOptions(Z)V

    .line 376
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->updateRoles()V

    return-void
.end method

.method onTestPrintClicked()V
    .locals 2

    .line 421
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->testPrint:Lcom/squareup/ui/settings/printerstations/station/TestPrint;

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v1, v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {v1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->performTestPrint(Ljava/lang/String;)V

    return-void
.end method

.method setPrintCategoryIdEnabled(Ljava/lang/String;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 300
    iget-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object p2, p2, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {p2, p1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->removeDisabledCategoryId(Ljava/lang/String;)Lcom/squareup/print/PrinterStationConfiguration$Builder;

    goto :goto_0

    .line 302
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object p2, p2, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {p2, p1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->addDisabledCategoryId(Ljava/lang/String;)Lcom/squareup/print/PrinterStationConfiguration$Builder;

    :goto_0
    return-void
.end method

.method setPrintsUncategorizedItems(Z)V
    .locals 1

    .line 291
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->setPrintsUncategorizedItems(Z)Lcom/squareup/print/PrinterStationConfiguration$Builder;

    return-void
.end method

.method toggleTicketNamingOption()V
    .locals 2

    .line 324
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedTicketNamingOption:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    sget-object v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->CUSTOM_NAME:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->AUTO_NUMBER:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->CUSTOM_NAME:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    :goto_0
    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedTicketNamingOption:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    .line 327
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;->selectedTicketNamingOption:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;->setSelectedNamingOption(Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;)V

    return-void
.end method
