.class public Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "PrinterStationScope.java"

# interfaces
.implements Lcom/squareup/container/layer/InSection;
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Component;,
        Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$ComponentFactory;,
        Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;,
        Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;",
            ">;"
        }
    .end annotation
.end field

.field private static final NEW_PRINTER_STATION_UUID:Ljava/lang/String;


# instance fields
.field private final printerStationUUID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 154
    sget-object v0, Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationScope$7o6kQN7IKpLioxD7BueCu1eCT7c;->INSTANCE:Lcom/squareup/ui/settings/printerstations/station/-$$Lambda$PrinterStationScope$7o6kQN7IKpLioxD7BueCu1eCT7c;

    .line 155
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 83
    sget-object v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;->NEW_PRINTER_STATION_UUID:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 78
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;->printerStationUUID:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;)Ljava/lang/String;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;->printerStationUUID:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .line 31
    sget-object v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;->NEW_PRINTER_STATION_UUID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;
    .locals 1

    .line 156
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 157
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 150
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/settings/InSettingsAppletScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 151
    iget-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;->printerStationUUID:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 35
    const-class v0, Lcom/squareup/ui/settings/printerstations/PrinterStationsSection;

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 87
    const-class v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Component;

    .line 88
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Component;

    .line 89
    invoke-interface {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Component;->printerStationScopeRunner()Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

    move-result-object v0

    .line 90
    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
