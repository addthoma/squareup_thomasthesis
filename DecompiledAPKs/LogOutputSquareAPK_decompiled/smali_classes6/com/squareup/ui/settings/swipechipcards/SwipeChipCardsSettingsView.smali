.class public Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;
.super Landroid/widget/LinearLayout;
.source "SwipeChipCardsSettingsView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field presenter:Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private swipeChipCardsEnabledSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const-class p2, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Component;->inject(Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 56
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onAttachedToWindow$0$SwipeChipCardsSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 33
    iget-object p1, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;->presenter:Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;

    invoke-virtual {p1, p0, p2}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;->onEnabledToggled(Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;Z)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .line 30
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 31
    sget v0, Lcom/squareup/settingsapplet/R$id;->enable_swipe_chip_cards:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;->swipeChipCardsEnabledSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;->swipeChipCardsEnabledSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/swipechipcards/-$$Lambda$SwipeChipCardsSettingsView$TiB6gd2VgqCBeoMh10NTM4sAw0I;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/swipechipcards/-$$Lambda$SwipeChipCardsSettingsView$TiB6gd2VgqCBeoMh10NTM4sAw0I;-><init>(Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 36
    sget v0, Lcom/squareup/settingsapplet/R$id;->emv_liability_hint:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 37
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/settingsapplet/R$string;->swipe_chip_cards_enable_hint:I

    const-string v3, "support_center"

    .line 38
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/cardreader/ui/R$string;->swipe_chip_cards_url:I

    .line 39
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/cardreader/ui/R$string;->support_center:I

    .line 40
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 41
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 37
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;->presenter:Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;->presenter:Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Presenter;->dropView(Landroid/view/ViewGroup;)V

    .line 48
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method setSwipeChipCardsEnabled(ZZ)V
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsView;->swipeChipCardsEnabledSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(ZZ)V

    return-void
.end method
