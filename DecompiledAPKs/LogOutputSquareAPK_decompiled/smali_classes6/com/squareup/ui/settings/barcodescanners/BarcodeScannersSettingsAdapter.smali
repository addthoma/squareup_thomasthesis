.class Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;
.super Lcom/squareup/ui/account/HardwarePeripheralListAdapter;
.source "BarcodeScannersSettingsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/account/HardwarePeripheralListAdapter<",
        "Lcom/squareup/barcodescanners/BarcodeScanner;",
        ">;"
    }
.end annotation


# static fields
.field private static final STRINGS:Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;


# instance fields
.field private availableBarcodeScanners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/barcodescanners/BarcodeScanner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 20
    new-instance v0, Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;

    sget v1, Lcom/squareup/settingsapplet/R$string;->barcode_scanners_uppercase_available:I

    sget v2, Lcom/squareup/settingsapplet/R$string;->barcode_scanners_none_found:I

    sget v3, Lcom/squareup/settingsapplet/R$string;->barcode_scanners_help_message:I

    const/4 v4, 0x0

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;-><init>(IIII)V

    sput-object v0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;->STRINGS:Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 27
    invoke-direct {p0}, Lcom/squareup/ui/account/HardwarePeripheralListAdapter;-><init>()V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;->availableBarcodeScanners:Ljava/util/List;

    return-void
.end method


# virtual methods
.method protected buildAndBindItemRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/barcodescanners/BarcodeScanner;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    if-nez p1, :cond_0

    .line 61
    invoke-static {p3}, Lcom/squareup/ui/account/view/SmartLineRow;->inflateForListView(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object p1

    .line 64
    :cond_0
    invoke-interface {p2}, Lcom/squareup/barcodescanners/BarcodeScanner;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    return-object p1
.end method

.method protected bridge synthetic buildAndBindItemRow(Lcom/squareup/ui/account/view/SmartLineRow;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    .line 18
    check-cast p2, Lcom/squareup/barcodescanners/BarcodeScanner;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;->buildAndBindItemRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/barcodescanners/BarcodeScanner;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method protected buildAndBindUnsupportedItemRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/barcodescanners/BarcodeScanner;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    .line 70
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unable to show unsupported barcode scanners."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected bridge synthetic buildAndBindUnsupportedItemRow(Lcom/squareup/ui/account/view/SmartLineRow;Ljava/lang/Object;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    .line 18
    check-cast p2, Lcom/squareup/barcodescanners/BarcodeScanner;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;->buildAndBindUnsupportedItemRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/barcodescanners/BarcodeScanner;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getAvailableCount()I
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;->availableBarcodeScanners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/squareup/barcodescanners/BarcodeScanner;
    .locals 2

    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;->availableBarcodeScanners:Ljava/util/List;

    sub-int/2addr p1, v1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/barcodescanners/BarcodeScanner;

    return-object p1

    .line 49
    :cond_0
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Attempting to get a position that is not an ITEM_ROW."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 18
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;->getItem(I)Lcom/squareup/barcodescanners/BarcodeScanner;

    move-result-object p1

    return-object p1
.end method

.method protected getStrings()Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;
    .locals 1

    .line 74
    sget-object v0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;->STRINGS:Lcom/squareup/ui/account/HardwarePeripheralListAdapter$StringHolder;

    return-object v0
.end method

.method public getUnsupportedCount()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method setBarcodeScannersList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/barcodescanners/BarcodeScanner;",
            ">;)V"
        }
    .end annotation

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;->availableBarcodeScanners:Ljava/util/List;

    .line 33
    invoke-virtual {p0}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsAdapter;->notifyDataSetChanged()V

    return-void
.end method
