.class Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$PaymentTypeViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "PaymentTypesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PaymentTypeViewHolder"
.end annotation


# instance fields
.field row:Lcom/squareup/ui/library/PaymentTypeListRow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/library/PaymentTypeListRow;)V
    .locals 0

    .line 384
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 385
    iput-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$PaymentTypeViewHolder;->row:Lcom/squareup/ui/library/PaymentTypeListRow;

    return-void
.end method


# virtual methods
.method public setEnabled(Z)V
    .locals 1

    .line 389
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$PaymentTypeViewHolder;->row:Lcom/squareup/ui/library/PaymentTypeListRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/library/PaymentTypeListRow;->setChecked(Z)V

    return-void
.end method
