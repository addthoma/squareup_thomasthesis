.class Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$InfoViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "PaymentTypesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InfoViewHolder"
.end annotation


# instance fields
.field private textView:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1

    .line 398
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 399
    sget v0, Lcom/squareup/settingsapplet/R$id;->payment_types_settings_info_message:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$InfoViewHolder;->textView:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method static synthetic access$600(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$InfoViewHolder;)Lcom/squareup/marketfont/MarketTextView;
    .locals 0

    .line 393
    iget-object p0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$InfoViewHolder;->textView:Lcom/squareup/marketfont/MarketTextView;

    return-object p0
.end method
