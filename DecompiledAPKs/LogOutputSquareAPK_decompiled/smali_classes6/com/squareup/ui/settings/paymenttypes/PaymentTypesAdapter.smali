.class public Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "PaymentTypesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;,
        Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Info;,
        Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;,
        Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$InfoViewHolder;,
        Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$PaymentTypeViewHolder;,
        Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$HeaderViewHolder;,
        Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final AT_LEAST_ONE_PRIMARY_MSG_VIEW_TYPE:I = 0x2

.field private static final HEADER_VIEW_TYPE:I = 0x0

.field private static final PAYMENT_TYPE_VIEW_TYPE:I = 0x1

.field private static final legacyTenderIdToNameStringResMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final tenderTypeToNameStringResMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final atLeastOnePrimary:Z

.field private final context:Landroid/content/Context;

.field itemTouchHelper:Landroidx/recyclerview/widget/ItemTouchHelper;

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final legacyTenderNamesMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private listener:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;

.field private settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->tenderTypeToNameStringResMap:Ljava/util/Map;

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->legacyTenderIdToNameStringResMap:Ljava/util/Map;

    .line 52
    sget-object v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->tenderTypeToNameStringResMap:Ljava/util/Map;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    sget v2, Lcom/squareup/settingsapplet/R$string;->payment_types_settings_label_card:I

    .line 53
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 52
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->tenderTypeToNameStringResMap:Ljava/util/Map;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->INSTALLMENTS:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    sget v2, Lcom/squareup/settingsapplet/R$string;->payment_types_settings_label_installments:I

    .line 55
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 54
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->tenderTypeToNameStringResMap:Ljava/util/Map;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->INVOICE:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    sget v2, Lcom/squareup/settingsapplet/R$string;->payment_types_settings_label_invoice:I

    .line 57
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 56
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->tenderTypeToNameStringResMap:Ljava/util/Map;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CHECKOUT_LINK:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    sget v2, Lcom/squareup/settingsapplet/R$string;->payment_types_settings_label_online_checkout:I

    .line 59
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 58
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->tenderTypeToNameStringResMap:Ljava/util/Map;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->GIFT_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    sget v2, Lcom/squareup/settingsapplet/R$string;->payment_types_settings_label_gift_card:I

    .line 61
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 60
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->tenderTypeToNameStringResMap:Ljava/util/Map;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CARD_ON_FILE:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    sget v2, Lcom/squareup/settingsapplet/R$string;->payment_types_settings_label_card_on_file:I

    .line 63
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 62
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->tenderTypeToNameStringResMap:Ljava/util/Map;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->CASH:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    sget v2, Lcom/squareup/settingsapplet/R$string;->payment_types_settings_label_cash:I

    .line 65
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 64
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->tenderTypeToNameStringResMap:Ljava/util/Map;

    sget-object v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->E_MONEY:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    sget v2, Lcom/squareup/settingsapplet/R$string;->payment_types_settings_label_emoney:I

    .line 67
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 66
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->legacyTenderIdToNameStringResMap:Ljava/util/Map;

    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager;->THIRD_PARTY_CARD:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    iget-object v1, v1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    sget v2, Lcom/squareup/settingsapplet/R$string;->payment_types_settings_label_record_card_payment:I

    .line 69
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 68
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/Map;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .line 133
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 72
    new-instance v0, Landroidx/recyclerview/widget/ItemTouchHelper;

    new-instance v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;-><init>(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;II)V

    invoke-direct {v0, v1}, Landroidx/recyclerview/widget/ItemTouchHelper;-><init>(Landroidx/recyclerview/widget/ItemTouchHelper$Callback;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->itemTouchHelper:Landroidx/recyclerview/widget/ItemTouchHelper;

    .line 125
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    .line 134
    iput-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->context:Landroid/content/Context;

    .line 135
    iput-object p2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->legacyTenderNamesMap:Ljava/util/Map;

    .line 136
    iput-boolean p3, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->atLeastOnePrimary:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;)Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;)Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->listener:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;

    return-object p0
.end method

.method private getBeginningOfSection(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/protos/client/devicesettings/TenderSettings;)I
    .locals 2

    .line 440
    sget-object v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$2;->$SwitchMap$com$squareup$tenderpayment$TenderSettingsManager$TenderSettingsCategory:[I

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    .line 447
    sget-object p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->SECONDARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->getEndOfSection(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/protos/client/devicesettings/TenderSettings;)I

    move-result p1

    :goto_0
    add-int/2addr p1, v0

    return p1

    .line 449
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Invalid category."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 444
    :cond_1
    sget-object p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->PRIMARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->getEndOfSection(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/protos/client/devicesettings/TenderSettings;)I

    move-result p1

    iget-boolean p2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->atLeastOnePrimary:Z

    add-int/2addr p1, p2

    goto :goto_0

    :cond_2
    return v0
.end method

.method private getEndOfSection(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/protos/client/devicesettings/TenderSettings;)I
    .locals 2

    .line 456
    sget-object v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$2;->$SwitchMap$com$squareup$tenderpayment$TenderSettingsManager$TenderSettingsCategory:[I

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    .line 464
    sget-object p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->DISABLED:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->getBeginningOfSection(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/protos/client/devicesettings/TenderSettings;)I

    move-result p1

    iget-object p2, p2, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    .line 465
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    .line 464
    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result p2

    :goto_0
    add-int/2addr p1, p2

    sub-int/2addr p1, v0

    return p1

    .line 467
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Invalid category."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 461
    :cond_1
    sget-object p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->SECONDARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->getBeginningOfSection(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/protos/client/devicesettings/TenderSettings;)I

    move-result p1

    iget-object p2, p2, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    .line 462
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    .line 461
    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result p2

    goto :goto_0

    .line 458
    :cond_2
    sget-object p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->PRIMARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->getBeginningOfSection(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/protos/client/devicesettings/TenderSettings;)I

    move-result p1

    iget-object p2, p2, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    .line 459
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    .line 458
    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result p2

    goto :goto_0
.end method


# virtual methods
.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 263
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemCount()I
    .locals 1

    .line 354
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 358
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    .line 359
    instance-of v0, p1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 361
    :cond_0
    instance-of v0, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    if-nez v0, :cond_3

    instance-of v0, p1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;

    if-eqz v0, :cond_1

    goto :goto_0

    .line 363
    :cond_1
    instance-of p1, p1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Info;

    if-eqz p1, :cond_2

    const/4 p1, 0x2

    return p1

    .line 366
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "View type not found."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public synthetic lambda$onBindViewHolder$0$PaymentTypesAdapter(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Landroid/widget/CompoundButton;Z)V
    .locals 1

    .line 319
    invoke-virtual {p2}, Landroid/widget/CompoundButton;->toggle()V

    .line 320
    iget-object p2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->listener:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;

    if-eqz p3, :cond_0

    sget-object p3, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->PRIMARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    goto :goto_0

    :cond_0
    sget-object p3, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->DISABLED:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    .line 322
    invoke-static {p1, v0}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    move-result-object v0

    .line 320
    invoke-interface {p2, p1, p3, v0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;->onTenderMovedToCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)V

    return-void
.end method

.method public synthetic lambda$onBindViewHolder$1$PaymentTypesAdapter(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 328
    iget-object p3, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    invoke-static {p1, p3}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    move-result-object p1

    sget-object p3, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->PRIMARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    if-ne p1, p3, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    .line 329
    invoke-static {p1}, Lcom/squareup/tenderpayment/TenderSettingsManager;->moreThanOnePrimaryPaymentMethodEnabled(Lcom/squareup/protos/client/devicesettings/TenderSettings;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-boolean p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->atLeastOnePrimary:Z

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    .line 330
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->itemTouchHelper:Landroidx/recyclerview/widget/ItemTouchHelper;

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/ItemTouchHelper;->startDrag(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public moveTender(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;)V
    .locals 8

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 189
    iget-object v1, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->destCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    invoke-direct {p0, v1, v2}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->getBeginningOfSection(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/protos/client/devicesettings/TenderSettings;)I

    move-result v1

    .line 191
    sget-object v2, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$2;->$SwitchMap$com$squareup$tenderpayment$TenderSettingsManager$TenderSettingsCategory:[I

    iget-object v3, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->destCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-virtual {v3}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eq v2, v4, :cond_2

    const/4 v5, 0x2

    if-eq v2, v5, :cond_1

    const/4 v5, 0x3

    if-eq v2, v5, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    .line 199
    :cond_0
    iget-object v2, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    iget-object v2, v2, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    iget-object v5, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-interface {v2, v5}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    goto :goto_0

    .line 196
    :cond_1
    iget-object v2, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    iget-object v2, v2, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    iget-object v5, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-interface {v2, v5}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    goto :goto_0

    .line 193
    :cond_2
    iget-object v2, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    iget-object v2, v2, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    iget-object v5, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-interface {v2, v5}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    :goto_0
    if-ltz v2, :cond_3

    add-int/2addr v1, v2

    goto :goto_1

    :cond_3
    add-int/lit8 v1, v1, -0x1

    .line 208
    :goto_1
    iget-boolean v2, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->isDragUpdate:Z

    if-eqz v2, :cond_5

    .line 209
    iget-object v2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 210
    iget-object v2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v2, v4

    if-ne v1, v2, :cond_4

    add-int/lit8 v1, v1, -0x1

    .line 215
    :cond_4
    iget-object v2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    iget-object v5, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-interface {v2, v1, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 216
    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->notifyItemMoved(II)V

    goto :goto_2

    .line 218
    :cond_5
    iget-object v2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 219
    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->notifyItemRemoved(I)V

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, v4

    if-ne v1, v0, :cond_6

    add-int/lit8 v1, v1, -0x1

    .line 225
    :cond_6
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    iget-object v2, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 226
    invoke-virtual {p0, v1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->notifyItemInserted(I)V

    .line 229
    :goto_2
    iget-object v0, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    iget-object v2, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->srcCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-static {v0, v2}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderForCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_8

    .line 231
    iget-object v0, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->srcCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    sget-object v5, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->DISABLED:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    if-ne v0, v5, :cond_7

    .line 232
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    new-instance v5, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;

    iget-object v6, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->srcCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-direct {v5, v6, v2}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;-><init>(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v0, v4

    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->notifyItemInserted(I)V

    goto :goto_3

    .line 235
    :cond_7
    iget-object v0, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->srcCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    iget-object v5, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    invoke-direct {p0, v0, v5}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->getBeginningOfSection(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/protos/client/devicesettings/TenderSettings;)I

    move-result v0

    .line 236
    iget-object v5, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    new-instance v6, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;

    iget-object v7, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->srcCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-direct {v6, v7, v2}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;-><init>(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;)V

    invoke-interface {v5, v0, v6}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 237
    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->notifyItemInserted(I)V

    .line 240
    :cond_8
    :goto_3
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    iget-object v5, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->destCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-static {v0, v5}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderForCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    new-instance v5, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;

    iget-object v6, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->destCat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-direct {v5, v6, v2}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;-><init>(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;)V

    invoke-interface {v0, v5}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 243
    iget-object v2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 244
    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->notifyItemRemoved(I)V

    if-le v1, v0, :cond_9

    add-int/lit8 v1, v1, -0x1

    .line 250
    :cond_9
    iget-object v0, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    .line 251
    iget-boolean v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->atLeastOnePrimary:Z

    if-eqz v0, :cond_a

    .line 253
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    iget-object v0, v0, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, v4

    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->notifyItemChanged(I)V

    .line 256
    :cond_a
    iget-boolean v0, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->isDragUpdate:Z

    if-eqz v0, :cond_c

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->listener:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;

    iget-object p1, p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    .line 258
    invoke-static {p1, v2}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    move-result-object p1

    sget-object v2, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->DISABLED:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    if-eq p1, v2, :cond_b

    const/4 v3, 0x1

    .line 257
    :cond_b
    invoke-interface {v0, v1, v3}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;->onTenderDragFinished(IZ)V

    :cond_c
    return-void
.end method

.method public onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 7

    .line 286
    invoke-virtual {p0, p2}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_8

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    goto/16 :goto_3

    .line 346
    :cond_0
    check-cast p1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$InfoViewHolder;

    .line 347
    invoke-static {p1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$InfoViewHolder;->access$600(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$InfoViewHolder;)Lcom/squareup/marketfont/MarketTextView;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    invoke-interface {v3, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Info;

    iget-object p2, p2, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Info;->message:Ljava/lang/String;

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 348
    invoke-static {p1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$InfoViewHolder;->access$600(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$InfoViewHolder;)Lcom/squareup/marketfont/MarketTextView;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    iget-object p2, p2, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    if-ne p2, v2, :cond_1

    goto :goto_0

    :cond_1
    const/16 v1, 0x8

    :goto_0
    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 292
    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$PaymentTypeViewHolder;

    .line 293
    iget-object v3, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    invoke-interface {v3, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    if-eqz v3, :cond_6

    .line 294
    iget-object v3, v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$PaymentTypeViewHolder;->row:Lcom/squareup/ui/library/PaymentTypeListRow;

    .line 295
    iget-object v4, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    invoke-interface {v4, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 296
    invoke-virtual {v3}, Lcom/squareup/ui/library/PaymentTypeListRow;->resetRow()V

    .line 299
    iget-object v4, p2, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    if-eqz v4, :cond_3

    .line 300
    iget-object v4, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget-object v5, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->tenderTypeToNameStringResMap:Ljava/util/Map;

    iget-object v6, p2, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    .line 301
    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 300
    invoke-virtual {v3, v4}, Lcom/squareup/ui/library/PaymentTypeListRow;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 302
    :cond_3
    sget-object v4, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->legacyTenderIdToNameStringResMap:Ljava/util/Map;

    iget-object v5, p2, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 304
    iget-object v4, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget-object v5, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->legacyTenderIdToNameStringResMap:Ljava/util/Map;

    iget-object v6, p2, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    .line 306
    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 305
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 304
    invoke-virtual {v3, v4}, Lcom/squareup/ui/library/PaymentTypeListRow;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 308
    :cond_4
    iget-object v4, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->legacyTenderNamesMap:Ljava/util/Map;

    iget-object v5, p2, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Lcom/squareup/ui/library/PaymentTypeListRow;->setText(Ljava/lang/CharSequence;)V

    .line 313
    :goto_1
    iget-object v4, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    .line 314
    invoke-static {p2, v4}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    move-result-object v4

    sget-object v5, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->DISABLED:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    if-eq v4, v5, :cond_5

    const/4 v1, 0x1

    .line 315
    :cond_5
    invoke-virtual {v3, v1}, Lcom/squareup/ui/library/PaymentTypeListRow;->setChecked(Z)V

    .line 318
    new-instance v1, Lcom/squareup/ui/settings/paymenttypes/-$$Lambda$PaymentTypesAdapter$rskvDrpycrzfSERxqzYZFAW_P8o;

    invoke-direct {v1, p0, p2}, Lcom/squareup/ui/settings/paymenttypes/-$$Lambda$PaymentTypesAdapter$rskvDrpycrzfSERxqzYZFAW_P8o;-><init>(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;)V

    invoke-virtual {v3, v1}, Lcom/squareup/ui/library/PaymentTypeListRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 326
    invoke-virtual {v3}, Lcom/squareup/ui/library/PaymentTypeListRow;->getDragHandle()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/settings/paymenttypes/-$$Lambda$PaymentTypesAdapter$GJc-XT9lqWGTKR2WuN0lDbXUacs;

    invoke-direct {v2, p0, p2, p1}, Lcom/squareup/ui/settings/paymenttypes/-$$Lambda$PaymentTypesAdapter$GJc-XT9lqWGTKR2WuN0lDbXUacs;-><init>(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    invoke-virtual {v1, v2}, Lcom/squareup/glyph/SquareGlyphView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_2

    .line 336
    :cond_6
    iget-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    sub-int/2addr p1, v2

    if-ne p2, p1, :cond_7

    .line 337
    iget-object p1, v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$PaymentTypeViewHolder;->row:Lcom/squareup/ui/library/PaymentTypeListRow;

    invoke-virtual {p1}, Lcom/squareup/ui/library/PaymentTypeListRow;->setEmptyDisabledRow()V

    goto :goto_2

    .line 339
    :cond_7
    iget-object p1, v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$PaymentTypeViewHolder;->row:Lcom/squareup/ui/library/PaymentTypeListRow;

    invoke-virtual {p1}, Lcom/squareup/ui/library/PaymentTypeListRow;->setEmptyRow()V

    .line 342
    :goto_2
    iget-object p1, v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$PaymentTypeViewHolder;->row:Lcom/squareup/ui/library/PaymentTypeListRow;

    iget-object p2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->context:Landroid/content/Context;

    .line 343
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/marin/R$color;->marin_window_background:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    .line 342
    invoke-virtual {p1, p2}, Lcom/squareup/ui/library/PaymentTypeListRow;->setBackgroundColor(I)V

    goto :goto_3

    .line 288
    :cond_8
    check-cast p1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$HeaderViewHolder;

    .line 289
    invoke-static {p1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$HeaderViewHolder;->access$500(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$HeaderViewHolder;)Lcom/squareup/marketfont/MarketTextView;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;

    iget-object p2, p2, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;->title:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 2

    const/4 v0, 0x0

    if-eqz p2, :cond_2

    const/4 v1, 0x1

    if-eq p2, v1, :cond_1

    const/4 v1, 0x2

    if-ne p2, v1, :cond_0

    .line 278
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    sget v1, Lcom/squareup/settingsapplet/R$layout;->payment_types_settings_info_row:I

    .line 279
    invoke-virtual {p2, v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 280
    new-instance p2, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$InfoViewHolder;

    invoke-direct {p2, p1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$InfoViewHolder;-><init>(Landroid/view/View;)V

    return-object p2

    .line 282
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "View type invalid."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 273
    :cond_1
    new-instance p2, Lcom/squareup/ui/library/PaymentTypeListRow;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/ui/library/PaymentTypeListRow;-><init>(Landroid/content/Context;)V

    .line 274
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-direct {p1, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p2, p1}, Lcom/squareup/ui/library/PaymentTypeListRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 276
    new-instance p1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$PaymentTypeViewHolder;

    invoke-direct {p1, p2}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$PaymentTypeViewHolder;-><init>(Lcom/squareup/ui/library/PaymentTypeListRow;)V

    return-object p1

    .line 269
    :cond_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    sget v1, Lcom/squareup/settingsapplet/R$layout;->payment_types_settings_header_row:I

    .line 270
    invoke-virtual {p2, v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 271
    new-instance p2, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$HeaderViewHolder;

    invoke-direct {p2, p1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$HeaderViewHolder;-><init>(Landroid/view/View;)V

    return-object p2
.end method

.method public setListener(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;)V
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->listener:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;

    return-void
.end method

.method public setTenderSettings(Lcom/squareup/protos/client/devicesettings/TenderSettings;)V
    .locals 6

    .line 144
    iput-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    new-instance v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->context:Landroid/content/Context;

    .line 149
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/settingsapplet/R$string;->uppercase_payment_types_settings_primary:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->PRIMARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4, v3, v4}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;-><init>(Ljava/lang/String;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;)V

    .line 148
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    iget-object v0, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    new-instance v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;

    sget-object v2, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->PRIMARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-direct {v1, v2, v4}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;-><init>(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    :goto_0
    iget-boolean v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->atLeastOnePrimary:Z

    if-eqz v0, :cond_1

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    new-instance v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Info;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/registerlib/R$string;->payment_types_settings_minimum_1_payment_type_required:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v4}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Info;-><init>(Ljava/lang/String;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    new-instance v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->context:Landroid/content/Context;

    .line 166
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/settingsapplet/R$string;->uppercase_payment_types_settings_secondary:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->PRIMARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    sget-object v5, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->SECONDARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-direct {v1, v2, v3, v5, v4}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;-><init>(Ljava/lang/String;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;)V

    .line 165
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    iget-object v0, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 171
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    new-instance v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;

    sget-object v2, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->SECONDARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-direct {v1, v2, v4}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;-><init>(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    new-instance v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->context:Landroid/content/Context;

    .line 176
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/settingsapplet/R$string;->uppercase_payment_types_settings_disabled:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->SECONDARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    sget-object v5, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->DISABLED:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-direct {v1, v2, v3, v5, v4}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Header;-><init>(Ljava/lang/String;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;)V

    .line 175
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    iget-object v0, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->disabled_tender:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 181
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->items:Ljava/util/List;

    new-instance v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;

    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->DISABLED:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-direct {v0, v1, v4}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;-><init>(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    :goto_2
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;->notifyDataSetChanged()V

    return-void
.end method
