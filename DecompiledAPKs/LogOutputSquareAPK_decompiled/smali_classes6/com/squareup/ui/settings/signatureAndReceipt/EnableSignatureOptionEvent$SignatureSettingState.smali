.class public final enum Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;
.super Ljava/lang/Enum;
.source "EnableSignatureOptionEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SignatureSettingState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

.field public static final enum ALWAYS_SKIP_SIGNATURE:Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

.field public static final enum NEVER_SKIP_SIGNATURE:Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

.field public static final enum UNDER_AMOUNT_SKIP_SIGNATURE:Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 15
    new-instance v0, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    const/4 v1, 0x0

    const-string v2, "ALWAYS_SKIP_SIGNATURE"

    const-string v3, "never collect"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;->ALWAYS_SKIP_SIGNATURE:Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    .line 16
    new-instance v0, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    const/4 v2, 0x1

    const-string v3, "UNDER_AMOUNT_SKIP_SIGNATURE"

    const-string v4, "skip under threshold"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;->UNDER_AMOUNT_SKIP_SIGNATURE:Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    .line 17
    new-instance v0, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    const/4 v3, 0x2

    const-string v4, "NEVER_SKIP_SIGNATURE"

    const-string v5, "always collect"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;->NEVER_SKIP_SIGNATURE:Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    .line 14
    sget-object v4, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;->ALWAYS_SKIP_SIGNATURE:Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;->UNDER_AMOUNT_SKIP_SIGNATURE:Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;->NEVER_SKIP_SIGNATURE:Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;->$VALUES:[Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput-object p3, p0, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;
    .locals 1

    .line 14
    const-class v0, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;->$VALUES:[Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    invoke-virtual {v0}, [Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;->value:Ljava/lang/String;

    return-object v0
.end method
