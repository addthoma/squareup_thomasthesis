.class public final Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "SignatureAndReceiptSettingsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Component;,
        Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 52
    new-instance v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen;

    .line 399
    sget-object v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen;

    .line 400
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 55
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 63
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_SIGNATURE:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 59
    const-class v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 403
    sget v0, Lcom/squareup/settingsapplet/R$layout;->signature_settings_view:I

    return v0
.end method
