.class public final Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "BankAccountSettingsScope.kt"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope$Component;,
        Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBankAccountSettingsScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BankAccountSettingsScope.kt\ncom/squareup/ui/settings/bankaccount/BankAccountSettingsScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,41:1\n35#2:42\n24#3,4:43\n*E\n*S KotlinDebug\n*F\n+ 1 BankAccountSettingsScope.kt\ncom/squareup/ui/settings/bankaccount/BankAccountSettingsScope\n*L\n19#1:42\n39#1,4:43\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c7\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0002\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016R\u0016\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope;",
        "Lcom/squareup/ui/settings/InSettingsAppletScope;",
        "Lcom/squareup/container/RegistersInScope;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "register",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "Component",
        "Module",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope;

    invoke-direct {v0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope;->INSTANCE:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope;

    .line 43
    new-instance v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 46
    sput-object v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    const-class v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 19
    check-cast v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope$Component;

    .line 20
    invoke-interface {v0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope$Component;->scopeRunner()Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

    move-result-object v0

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
