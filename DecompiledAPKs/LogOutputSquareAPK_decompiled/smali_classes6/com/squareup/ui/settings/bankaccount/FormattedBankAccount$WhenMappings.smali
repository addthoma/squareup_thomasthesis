.class public final synthetic Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I

.field public static final synthetic $EnumSwitchMapping$3:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 8

    invoke-static {}, Lcom/squareup/protos/client/bankaccount/BankAccountType;->values()[Lcom/squareup/protos/client/bankaccount/BankAccountType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountType;->CHECKING:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bankaccount/BankAccountType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountType;->BUSINESS_CHECKING:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bankaccount/BankAccountType;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountType;->SAVINGS:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bankaccount/BankAccountType;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->values()[Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->AWAITING_DEBIT_AUTHORIZATION:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->AWAITING_EMAIL_CONFIRMATION:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->VERIFIED:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->VERIFICATION_CANCELED:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->VERIFICATION_IN_PROGRESS:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->ordinal()I

    move-result v1

    const/4 v6, 0x5

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->FAILED:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->ordinal()I

    move-result v1

    const/4 v7, 0x6

    aput v7, v0, v1

    invoke-static {}, Lcom/squareup/CountryCode;->values()[Lcom/squareup/CountryCode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/CountryCode;->AU:Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/CountryCode;->CA:Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/CountryCode;->FR:Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/CountryCode;->JP:Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/CountryCode;->GB:Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v1

    aput v6, v0, v1

    invoke-static {}, Lcom/squareup/CountryCode;->values()[Lcom/squareup/CountryCode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/CountryCode;->CA:Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/CountryCode;->JP:Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v1

    aput v3, v0, v1

    return-void
.end method
