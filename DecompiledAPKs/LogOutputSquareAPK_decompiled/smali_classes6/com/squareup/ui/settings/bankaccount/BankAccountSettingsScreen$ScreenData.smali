.class public final Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;
.super Ljava/lang/Object;
.source "BankAccountSettingsScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0011\u0018\u00002\u00020\u0001BS\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u0012\u0008\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010\u000c\u001a\u00020\u0008\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u0010R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0012R\u0011\u0010\u000f\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\u000c\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0012R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0019R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u0015\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;",
        "",
        "firstBankAccount",
        "Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;",
        "secondBankAccount",
        "state",
        "Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;",
        "canBeCanceled",
        "",
        "canResendEmail",
        "failureMessage",
        "Lcom/squareup/receiving/FailureMessage;",
        "hasUpButton",
        "verificationHintText",
        "",
        "depositOptionsHintText",
        "(Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;ZZLcom/squareup/receiving/FailureMessage;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V",
        "getCanBeCanceled",
        "()Z",
        "getCanResendEmail",
        "getDepositOptionsHintText",
        "()Ljava/lang/CharSequence;",
        "getFailureMessage",
        "()Lcom/squareup/receiving/FailureMessage;",
        "getFirstBankAccount",
        "()Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;",
        "getHasUpButton",
        "getSecondBankAccount",
        "getState",
        "()Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;",
        "getVerificationHintText",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final canBeCanceled:Z

.field private final canResendEmail:Z

.field private final depositOptionsHintText:Ljava/lang/CharSequence;

.field private final failureMessage:Lcom/squareup/receiving/FailureMessage;

.field private final firstBankAccount:Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;

.field private final hasUpButton:Z

.field private final secondBankAccount:Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;

.field private final state:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

.field private final verificationHintText:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;ZZLcom/squareup/receiving/FailureMessage;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "verificationHintText"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "depositOptionsHintText"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->firstBankAccount:Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;

    iput-object p2, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->secondBankAccount:Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;

    iput-object p3, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->state:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    iput-boolean p4, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->canBeCanceled:Z

    iput-boolean p5, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->canResendEmail:Z

    iput-object p6, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    iput-boolean p7, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->hasUpButton:Z

    iput-object p8, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->verificationHintText:Ljava/lang/CharSequence;

    iput-object p9, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->depositOptionsHintText:Ljava/lang/CharSequence;

    return-void
.end method


# virtual methods
.method public final getCanBeCanceled()Z
    .locals 1

    .line 45
    iget-boolean v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->canBeCanceled:Z

    return v0
.end method

.method public final getCanResendEmail()Z
    .locals 1

    .line 46
    iget-boolean v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->canResendEmail:Z

    return v0
.end method

.method public final getDepositOptionsHintText()Ljava/lang/CharSequence;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->depositOptionsHintText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getFailureMessage()Lcom/squareup/receiving/FailureMessage;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->failureMessage:Lcom/squareup/receiving/FailureMessage;

    return-object v0
.end method

.method public final getFirstBankAccount()Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->firstBankAccount:Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;

    return-object v0
.end method

.method public final getHasUpButton()Z
    .locals 1

    .line 48
    iget-boolean v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->hasUpButton:Z

    return v0
.end method

.method public final getSecondBankAccount()Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->secondBankAccount:Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;

    return-object v0
.end method

.method public final getState()Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->state:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    return-object v0
.end method

.method public final getVerificationHintText()Ljava/lang/CharSequence;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;->verificationHintText:Ljava/lang/CharSequence;

    return-object v0
.end method
