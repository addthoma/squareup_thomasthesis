.class public interface abstract Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Component;
.super Ljava/lang/Object;
.source "TaxDetailScreen.java"

# interfaces
.implements Lcom/squareup/ui/items/AppliedLocationsBanner$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/settings/taxes/tax/TaxDetailView;)V
.end method
