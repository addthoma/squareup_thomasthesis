.class public Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;
.super Landroid/widget/LinearLayout;
.source "TaxApplicableView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;
    }
.end annotation


# instance fields
.field private adapter:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;

.field analytics:Lcom/squareup/analytics/Analytics;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private listView:Landroid/widget/ListView;

.field presenter:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Lcom/squareup/ui/DelayedLoadingProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    const-class p2, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Component;->inject(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 90
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method hideProgressBar()V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->progressBar:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->hide()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$TaxApplicableView()V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->onProgressHidden()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$TaxApplicableView(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 54
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    .line 55
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/Related;

    .line 54
    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->onRowClicked(Lcom/squareup/shared/catalog/Related;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 62
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 47
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 49
    sget v0, Lcom/squareup/settingsapplet/R$id;->tax_applicable_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/DelayedLoadingProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->progressBar:Lcom/squareup/ui/DelayedLoadingProgressBar;

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->progressBar:Lcom/squareup/ui/DelayedLoadingProgressBar;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxApplicableView$-hfLId85t9I2KtZMEy94bEZpTYk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxApplicableView$-hfLId85t9I2KtZMEy94bEZpTYk;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/DelayedLoadingProgressBar;->setCallback(Lcom/squareup/ui/DelayedLoadingProgressBar$DelayedLoadingProgressBarCallback;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->progressBar:Lcom/squareup/ui/DelayedLoadingProgressBar;

    invoke-virtual {v0}, Lcom/squareup/ui/DelayedLoadingProgressBar;->show()V

    .line 53
    sget v0, Lcom/squareup/settingsapplet/R$id;->tax_applicable_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->listView:Landroid/widget/ListView;

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->listView:Landroid/widget/ListView;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxApplicableView$VoLdLXtuSpRYgBAd8-jazPq-Wyo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxApplicableView$VoLdLXtuSpRYgBAd8-jazPq-Wyo;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method refreshView()V
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->adapter:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method setItems(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;>;)V"
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->adapter:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$1;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->adapter:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->adapter:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->adapter:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->setItems(Ljava/util/List;)V

    return-void
.end method

.method showContent()V
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->listView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    return-void
.end method
