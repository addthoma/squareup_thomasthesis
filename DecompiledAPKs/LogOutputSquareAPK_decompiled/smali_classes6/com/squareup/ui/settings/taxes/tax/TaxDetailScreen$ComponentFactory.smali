.class public Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$ComponentFactory;
.super Ljava/lang/Object;
.source "TaxDetailScreen.java"

# interfaces
.implements Lcom/squareup/ui/WithComponent$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComponentFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 1

    .line 51
    const-class v0, Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;

    .line 52
    check-cast p2, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;

    .line 53
    new-instance v0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, p2}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;)V

    .line 54
    invoke-interface {p1, v0}, Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;->taxDetail(Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;)Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Component;

    move-result-object p1

    return-object p1
.end method
