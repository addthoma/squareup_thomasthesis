.class public Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;
.super Landroid/widget/LinearLayout;
.source "TaxItemPricingView.java"


# instance fields
.field private excludeRow:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private includeRow:Lcom/squareup/widgets/list/ToggleButtonRow;

.field presenter:Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const-class p2, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Component;->inject(Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 71
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 59
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 32
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 33
    sget v0, Lcom/squareup/settingsapplet/R$id;->exclude_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;->excludeRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;->excludeRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView$1;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    sget v0, Lcom/squareup/settingsapplet/R$id;->include_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;->includeRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;->includeRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView$2;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 48
    new-instance v1, Lcom/squareup/register/widgets/list/HelpRow;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/squareup/settingsapplet/R$string;->tax_inclusion_helper:I

    invoke-direct {v1, v2, v3}, Lcom/squareup/register/widgets/list/HelpRow;-><init>(Landroid/content/Context;I)V

    const/4 v2, 0x0

    .line 49
    invoke-virtual {v1, v0, v2, v0, v2}, Lcom/squareup/register/widgets/list/HelpRow;->setPadding(IIII)V

    .line 51
    sget v0, Lcom/squareup/settingsapplet/R$id;->container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 52
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method setExcludeRowChecked(Z)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;->excludeRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method setIncludeRowChecked(Z)V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;->includeRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method
