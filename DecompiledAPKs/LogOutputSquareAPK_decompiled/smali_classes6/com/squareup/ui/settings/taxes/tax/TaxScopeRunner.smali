.class public Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;
.super Ljava/lang/Object;
.source "TaxScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private applicableDisplayForService:Z

.field private final appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final flow:Lflow/Flow;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

.field private final taxStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/ui/settings/taxes/tax/TaxState;Lcom/squareup/settings/server/AccountStatusSettings;Lflow/Flow;Lcom/squareup/util/Res;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            "Lcom/squareup/ui/settings/taxes/tax/TaxState;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lflow/Flow;",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 42
    iput-boolean v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->applicableDisplayForService:Z

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    .line 48
    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    .line 49
    iput-object p3, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    .line 50
    iput-object p4, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 51
    iput-object p5, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->flow:Lflow/Flow;

    .line 52
    iput-object p6, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->res:Lcom/squareup/util/Res;

    .line 53
    iput-object p7, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->localeProvider:Ljavax/inject/Provider;

    .line 54
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-void
.end method


# virtual methods
.method buildTaxPercentageScrubber()Lcom/squareup/text/PercentageScrubber;
    .locals 3

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$integer;->tax_precision:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getInteger(I)I

    move-result v0

    .line 96
    new-instance v1, Lcom/squareup/text/PercentageScrubber;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->localeProvider:Ljavax/inject/Provider;

    invoke-direct {v1, v0, v2}, Lcom/squareup/text/PercentageScrubber;-><init>(ILjavax/inject/Provider;)V

    return-object v1
.end method

.method discardTaxChanges()V
    .locals 4

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->clear()V

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/settings/taxes/tax/InTaxScope;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public isApplicableDisplayForService()Z
    .locals 1

    .line 87
    iget-boolean v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->applicableDisplayForService:Z

    return v0
.end method

.method public synthetic lambda$onEnterScope$0$TaxScopeRunner()V
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->saveCopyOfTaxState()V

    .line 72
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->updateTaxState()V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 58
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 61
    new-instance v0, Lcom/squareup/cogs/CogsLockScoped;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-direct {v0, v1}, Lcom/squareup/cogs/CogsLockScoped;-><init>(Lcom/squareup/cogs/Cogs;)V

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 63
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/taxes/tax/TaxScope;

    .line 64
    iget-object v0, v0, Lcom/squareup/ui/settings/taxes/tax/TaxScope;->cogsTaxId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 65
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    invoke-interface {v1}, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;->getActiveLocationCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 66
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 67
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    invoke-interface {p1, v0}, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;->fetchAppliedLocationCount(Ljava/lang/String;)V

    .line 70
    :cond_0
    new-instance p1, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxScopeRunner$yBSMnoS_INZ5pRcHtQTiN_2SC5U;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxScopeRunner$yBSMnoS_INZ5pRcHtQTiN_2SC5U;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;)V

    .line 76
    sget-object v1, Lcom/squareup/ui/settings/taxes/tax/TaxScope;->NEW_TAX:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->newTax(Lcom/squareup/cogs/Cogs;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 79
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-virtual {v1, v0, v2, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->setTax(Ljava/lang/String;Lcom/squareup/cogs/Cogs;Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public setApplicableDisplayForService(Z)V
    .locals 0

    .line 91
    iput-boolean p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->applicableDisplayForService:Z

    return-void
.end method

.method taxState()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxState;",
            ">;"
        }
    .end annotation

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->asObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method updateTaxName(Ljava/lang/String;)V
    .locals 1

    .line 105
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    .line 107
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method updateTaxPercentage(Ljava/lang/String;)V
    .locals 4

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getFeeTypes()Lcom/squareup/server/account/FeeTypes;

    move-result-object v0

    .line 113
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz v0, :cond_2

    .line 114
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getFeeTypeId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/FeeTypes;->withId(Ljava/lang/String;)Lcom/squareup/server/account/protos/FeeType;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, v0, Lcom/squareup/server/account/protos/FeeType;->percentage:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/squareup/util/Percentage;->fromDouble(D)Lcom/squareup/util/Percentage;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 116
    :goto_0
    invoke-static {p1, v0}, Lcom/squareup/money/TaxRateStrings;->parse(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object p1

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/util/Percentage;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->setPercentage(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    .line 118
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_2
    return-void
.end method

.method updateTaxState()V
    .locals 5

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getFeeTypes()Lcom/squareup/server/account/FeeTypes;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Lcom/squareup/server/account/FeeTypes;->getTypes()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 129
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v1

    .line 130
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getFeeTypeId()Ljava/lang/String;

    move-result-object v2

    .line 131
    invoke-virtual {v0, v2}, Lcom/squareup/server/account/FeeTypes;->withId(Ljava/lang/String;)Lcom/squareup/server/account/protos/FeeType;

    move-result-object v2

    if-nez v2, :cond_0

    .line 134
    invoke-virtual {v0}, Lcom/squareup/server/account/FeeTypes;->getDefault()Lcom/squareup/server/account/protos/FeeType;

    move-result-object v2

    .line 135
    iget-object v0, v2, Lcom/squareup/server/account/protos/FeeType;->id:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->setFeeTypeId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    iget-object v3, v2, Lcom/squareup/server/account/protos/FeeType;->name:Ljava/lang/String;

    .line 136
    invoke-virtual {v0, v3}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->setFeeTypeName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    iget-object v3, v2, Lcom/squareup/server/account/protos/FeeType;->calculation_phase:Ljava/lang/Integer;

    .line 137
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->setCalculationPhase(I)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    .line 140
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/billhistoryui/R$string;->tax_name_default:I

    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    .line 144
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getPercentage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 145
    iget-object v0, v2, Lcom/squareup/server/account/protos/FeeType;->percentage:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/squareup/util/Percentage;->fromDouble(D)Lcom/squareup/util/Percentage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/util/Percentage;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->setPercentage(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    .line 148
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->hasInclusionType()Z

    move-result v0

    if-nez v0, :cond_3

    .line 149
    iget-object v0, v2, Lcom/squareup/server/account/protos/FeeType;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-virtual {v1, v0}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->setInclusionType(Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    :cond_3
    return-void
.end method
