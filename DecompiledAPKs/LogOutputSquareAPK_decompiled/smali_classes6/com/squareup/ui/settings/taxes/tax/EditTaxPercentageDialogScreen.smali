.class public Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen;
.super Lcom/squareup/ui/settings/taxes/tax/InTaxScope;
.source "EditTaxPercentageDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final editTextDialogFactory:Lcom/squareup/register/widgets/EditTextDialogFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 43
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$EditTaxPercentageDialogScreen$cLVBVyQfmsW3Oi_dA0tg2sSnUGw;->INSTANCE:Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$EditTaxPercentageDialogScreen$cLVBVyQfmsW3Oi_dA0tg2sSnUGw;

    .line 44
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/squareup/register/widgets/EditTextDialogFactory;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/InTaxScope;-><init>(Ljava/lang/String;)V

    .line 22
    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen;->editTextDialogFactory:Lcom/squareup/register/widgets/EditTextDialogFactory;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen;)Lcom/squareup/register/widgets/EditTextDialogFactory;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen;->editTextDialogFactory:Lcom/squareup/register/widgets/EditTextDialogFactory;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen;
    .locals 2

    .line 45
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 46
    const-class v1, Lcom/squareup/register/widgets/EditTextDialogFactory;

    .line 47
    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/register/widgets/EditTextDialogFactory;

    .line 48
    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen;

    invoke-direct {v1, v0, p0}, Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen;-><init>(Ljava/lang/String;Lcom/squareup/register/widgets/EditTextDialogFactory;)V

    return-object v1
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen;->cogsTaxId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/EditTaxPercentageDialogScreen;->editTextDialogFactory:Lcom/squareup/register/widgets/EditTextDialogFactory;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
