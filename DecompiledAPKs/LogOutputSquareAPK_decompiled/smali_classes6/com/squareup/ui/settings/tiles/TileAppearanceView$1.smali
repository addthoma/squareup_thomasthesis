.class Lcom/squareup/ui/settings/tiles/TileAppearanceView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "TileAppearanceView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/tiles/TileAppearanceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/tiles/TileAppearanceView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/tiles/TileAppearanceView;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView$1;->this$0:Lcom/squareup/ui/settings/tiles/TileAppearanceView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 31
    iget-object p1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceView$1;->this$0:Lcom/squareup/ui/settings/tiles/TileAppearanceView;

    iget-object p1, p1, Lcom/squareup/ui/settings/tiles/TileAppearanceView;->presenter:Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;

    sget-object v0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;->IMAGE:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Presenter;->maybeConfirmChange(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;)V

    return-void
.end method
