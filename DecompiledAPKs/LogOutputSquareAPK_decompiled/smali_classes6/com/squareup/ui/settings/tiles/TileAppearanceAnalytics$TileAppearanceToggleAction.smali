.class Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics$TileAppearanceToggleAction;
.super Lcom/squareup/analytics/event/v1/ToggleEvent;
.source "TileAppearanceAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TileAppearanceToggleAction"
.end annotation


# direct methods
.method constructor <init>(Z)V
    .locals 1

    .line 42
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->TILE_APPEARANCE_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0, p1}, Lcom/squareup/analytics/event/v1/ToggleEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Z)V

    return-void
.end method
