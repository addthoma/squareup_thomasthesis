.class public Lcom/squareup/ui/settings/scales/ScalesSettingsSection$ListEntry;
.super Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;
.source "ScalesSettingsSection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/scales/ScalesSettingsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListEntry"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0016\u0018\u00002\u00020\u0001B\u001f\u0008\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008B\'\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/settings/scales/ScalesSettingsSection$ListEntry;",
        "Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;",
        "section",
        "Lcom/squareup/ui/settings/scales/ScalesSettingsSection;",
        "res",
        "Lcom/squareup/util/Res;",
        "device",
        "Lcom/squareup/util/Device;",
        "(Lcom/squareup/ui/settings/scales/ScalesSettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;)V",
        "grouping",
        "Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;",
        "(Lcom/squareup/ui/settings/scales/ScalesSettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/scales/ScalesSettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "section"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    sget-object v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->HARDWARE:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    check-cast v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/ui/settings/scales/ScalesSettingsSection$ListEntry;-><init>(Lcom/squareup/ui/settings/scales/ScalesSettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/settings/scales/ScalesSettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V
    .locals 7

    const-string v0, "section"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "grouping"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    move-object v2, p1

    check-cast v2, Lcom/squareup/applet/AppletSection;

    sget v4, Lcom/squareup/ui/settings/scales/ScalesSettingsSection;->TITLE_ID:I

    move-object v1, p0

    move-object v3, p4

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;ILcom/squareup/util/Res;Lcom/squareup/util/Device;)V

    return-void
.end method
