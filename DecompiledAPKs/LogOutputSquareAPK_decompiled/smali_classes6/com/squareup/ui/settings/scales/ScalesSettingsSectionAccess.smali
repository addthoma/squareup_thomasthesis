.class public final Lcom/squareup/ui/settings/scales/ScalesSettingsSectionAccess;
.super Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess;
.source "ScalesSettingsSectionAccess.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/settings/scales/ScalesSettingsSectionAccess;",
        "Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/settings/server/Features;)V",
        "determineVisibility",
        "",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 1

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/squareup/permissions/Permission;

    .line 13
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/hardware/HardwareSettingsSectionAccess;-><init>(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/scales/ScalesSettingsSectionAccess;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 2

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/settings/scales/ScalesSettingsSectionAccess;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_SCALES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method
