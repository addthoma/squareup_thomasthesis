.class public final Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "CashDrawerSettingsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Component;,
        Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen;

    .line 129
    sget-object v0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen;

    .line 130
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 42
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_CASH_DRAWERS:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 38
    const-class v0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 133
    sget v0, Lcom/squareup/settingsapplet/R$layout;->cash_drawer_settings_view:I

    return v0
.end method
