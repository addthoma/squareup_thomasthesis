.class public Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "CashDrawerSettingsScreen.java"

# interfaces
.implements Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsView;",
        ">;",
        "Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/permissions/PermissionGatekeeper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 56
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 57
    iput-object p2, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 58
    iput-object p3, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 59
    iput-object p4, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    .line 60
    iput-object p5, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;)Lcom/squareup/cashdrawer/CashDrawerTracker;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method private onCashDrawerConnectionChanged()V
    .locals 1

    .line 113
    invoke-virtual {p0}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    invoke-direct {p0}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->refreshView()V

    :cond_0
    return-void
.end method

.method private refreshView()V
    .locals 2

    .line 119
    invoke-virtual {p0}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsView;

    iget-object v1, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-virtual {v1}, Lcom/squareup/cashdrawer/CashDrawerTracker;->getAvailableCashDrawers()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsView;->updateView(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public cashDrawerConnected(Lcom/squareup/cashdrawer/CashDrawer;)V
    .locals 0

    .line 109
    invoke-direct {p0}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->onCashDrawerConnectionChanged()V

    return-void
.end method

.method public cashDrawerDisconnected(Lcom/squareup/cashdrawer/CashDrawer;Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)V
    .locals 0

    .line 105
    invoke-direct {p0}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->onCashDrawerConnectionChanged()V

    return-void
.end method

.method public cashDrawersOpened()V
    .locals 0

    return-void
.end method

.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection;->TITLE_ID:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 64
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 65
    iget-object p1, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-virtual {p1, p0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->addListener(Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 69
    invoke-super {p0}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onExitScope()V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-virtual {v0, p0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->removeListener(Lcom/squareup/cashdrawer/CashDrawerTracker$Listener;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 74
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 75
    invoke-direct {p0}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->refreshView()V

    return-void
.end method

.method protected saveSettings()V
    .locals 0

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 79
    const-class v0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen;

    return-object v0
.end method

.method public testCashDrawersClicked()V
    .locals 3

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_CASH_DRAWER:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter$1;-><init>(Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Presenter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method
