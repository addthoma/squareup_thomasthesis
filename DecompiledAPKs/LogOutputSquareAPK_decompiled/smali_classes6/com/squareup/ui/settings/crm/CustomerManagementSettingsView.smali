.class public Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;
.super Landroid/widget/LinearLayout;
.source "CustomerManagementSettingsView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field private inCartToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private postTransactionLabel:Lcom/squareup/widgets/MessageView;

.field private postTransactionToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field presenter:Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private saveCardDivider:Landroid/view/View;

.field private saveCardLabel:Lcom/squareup/widgets/MessageView;

.field private saveCardPostTransactionContainer:Landroid/view/View;

.field private saveCardPostTransactionToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private saveCardToggle:Lcom/squareup/widgets/list/ToggleButtonRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    const-class p2, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Component;->inject(Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 110
    sget v0, Lcom/squareup/settingsapplet/R$id;->crm_customer_management_in_cart_toggle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->inCartToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 111
    sget v0, Lcom/squareup/settingsapplet/R$id;->crm_customer_management_post_transaction_toggle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->postTransactionToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 112
    sget v0, Lcom/squareup/settingsapplet/R$id;->crm_customer_management_post_transaction_label:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->postTransactionLabel:Lcom/squareup/widgets/MessageView;

    .line 113
    sget v0, Lcom/squareup/settingsapplet/R$id;->crm_customer_management_save_card_divider:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->saveCardDivider:Landroid/view/View;

    .line 114
    sget v0, Lcom/squareup/settingsapplet/R$id;->crm_customer_management_save_card_toggle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->saveCardToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 115
    sget v0, Lcom/squareup/settingsapplet/R$id;->crm_customer_management_save_card_label:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->saveCardLabel:Lcom/squareup/widgets/MessageView;

    .line 116
    sget v0, Lcom/squareup/settingsapplet/R$id;->crm_customer_management_save_card_post_transaction_container:I

    .line 117
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->saveCardPostTransactionContainer:Landroid/view/View;

    .line 118
    sget v0, Lcom/squareup/settingsapplet/R$id;->crm_customer_management_post_transaction_save_card_toggle:I

    .line 119
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->saveCardPostTransactionToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 123
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method isInCartEnabled()Z
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->inCartToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    return v0
.end method

.method isPostTransactionEnabled()Z
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->postTransactionToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    return v0
.end method

.method isSaveCardEnabled()Z
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->saveCardToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    return v0
.end method

.method isSaveCardPostTransactionEnabled()Z
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->saveCardPostTransactionToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$onFinishInflate$0$CustomerManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->presenter:Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->onInCartToggled()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$CustomerManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 46
    iget-object p1, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->presenter:Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->onPostTransactionToggled()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$2$CustomerManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 49
    iget-object p1, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->presenter:Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->onSaveCardToggled()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$3$CustomerManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 52
    iget-object p1, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->presenter:Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->onSaveCardPostTransactionToggled()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->presenter:Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->dropView(Landroid/view/ViewGroup;)V

    .line 59
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 38
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 40
    invoke-direct {p0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->bindViews()V

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->inCartToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/crm/-$$Lambda$CustomerManagementSettingsView$QmDENtIt0yqL0JGrC2noxLuFuJM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/crm/-$$Lambda$CustomerManagementSettingsView$QmDENtIt0yqL0JGrC2noxLuFuJM;-><init>(Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->postTransactionToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/crm/-$$Lambda$CustomerManagementSettingsView$PU4EFlYKpnRffMffROj-7JH6LA0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/crm/-$$Lambda$CustomerManagementSettingsView$PU4EFlYKpnRffMffROj-7JH6LA0;-><init>(Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->saveCardToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/crm/-$$Lambda$CustomerManagementSettingsView$iDHeSG3wxGFul1oSw_m5Ap1Z0Oo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/crm/-$$Lambda$CustomerManagementSettingsView$iDHeSG3wxGFul1oSw_m5Ap1Z0Oo;-><init>(Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->saveCardPostTransactionToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/crm/-$$Lambda$CustomerManagementSettingsView$G2xhu5wa5ir0fx60XKrngomcJ-E;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/crm/-$$Lambda$CustomerManagementSettingsView$G2xhu5wa5ir0fx60XKrngomcJ-E;-><init>(Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->presenter:Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method setInCartEnabled(Z)V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->inCartToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method setPostTransactionEnabled(Z)V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->postTransactionToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method setPostTransactionVisibility(Z)V
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->postTransactionToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->postTransactionLabel:Lcom/squareup/widgets/MessageView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setSaveCardEnabled(Z)V
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->saveCardToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method public setSaveCardPostTransactionEnabled(Z)V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->saveCardPostTransactionToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method setSaveCardPostTransactionVisibility(Z)V
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->saveCardPostTransactionContainer:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setSaveCardVisibility(Z)V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->saveCardToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->saveCardLabel:Lcom/squareup/widgets/MessageView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->saveCardDivider:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
