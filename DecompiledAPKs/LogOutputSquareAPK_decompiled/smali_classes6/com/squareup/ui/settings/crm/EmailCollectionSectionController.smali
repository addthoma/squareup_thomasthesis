.class public final Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;
.super Ljava/lang/Object;
.source "EmailCollectionSectionController.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Controller;
.implements Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Controller;


# static fields
.field private static final EMAIL_COLLECTION_TOGGLE_STATE:Ljava/lang/String; = "emailCollectionToggleState"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private emailCollectionToggleState:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;

.field private final settings:Lcom/squareup/crm/EmailCollectionSettings;

.field private final sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/crm/EmailCollectionSettings;Lflow/Flow;Lcom/squareup/ui/settings/SidebarRefresher;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->emailCollectionToggleState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->analytics:Lcom/squareup/analytics/Analytics;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->settings:Lcom/squareup/crm/EmailCollectionSettings;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->flow:Lflow/Flow;

    .line 39
    iput-object p4, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    return-void
.end method


# virtual methods
.method public closeEmailCollectionConfirmationDialog()V
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->emailCollectionToggleState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public closeEmailCollectionConfirmationDialog(Z)V
    .locals 4

    if-eqz p1, :cond_0

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/v1/ToggleEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->CRM_EMAIL_COLLECTION_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/squareup/analytics/event/v1/ToggleEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Z)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/crm/events/CrmToggleEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->CRM_EMAIL_COLLECTION_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v1, v2, v3}, Lcom/squareup/crm/events/CrmToggleEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Z)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->settings:Lcom/squareup/crm/EmailCollectionSettings;

    invoke-interface {v0, v3}, Lcom/squareup/crm/EmailCollectionSettings;->setEmailCollectionEnabled(Z)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->emailCollectionToggleState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public emailCollectionEnabled()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->emailCollectionToggleState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public enableEmailCollectionToggleClicked()V
    .locals 4

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->emailCollectionToggleState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 81
    iget-object v1, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->emailCollectionToggleState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog;

    invoke-direct {v1}, Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog;-><init>()V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/v1/ToggleEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->CRM_EMAIL_COLLECTION_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/squareup/analytics/event/v1/ToggleEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Z)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/crm/events/CrmToggleEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->CRM_EMAIL_COLLECTION_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v1, v2, v3}, Lcom/squareup/crm/events/CrmToggleEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Z)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->settings:Lcom/squareup/crm/EmailCollectionSettings;

    invoke-interface {v0, v3}, Lcom/squareup/crm/EmailCollectionSettings;->setEmailCollectionEnabled(Z)V

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    :goto_0
    return-void
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 47
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->emailCollectionToggleState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->settings:Lcom/squareup/crm/EmailCollectionSettings;

    invoke-interface {v0}, Lcom/squareup/crm/EmailCollectionSettings;->isEmailCollectionEnabled()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->emailCollectionToggleState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "emailCollectionToggleState"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;->emailCollectionToggleState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const-string v1, "emailCollectionToggleState"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
