.class public Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;
.super Landroid/widget/LinearLayout;
.source "StoreAndForwardSettingsView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field private limitHint:Landroid/widget/TextView;

.field presenter:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private storeAndForwardEnabledSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private transactionLimit:Lcom/squareup/widgets/SelectableEditText;

.field private transactionLimitSection:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    const-class p2, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Component;->inject(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 97
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method getTransactionLimit()Ljava/lang/CharSequence;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->transactionLimit:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method initializeFocusListener(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->transactionLimit:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/settings/offline/-$$Lambda$StoreAndForwardSettingsView$W9KpCfYadZYEtjiki9LLHkjLQd8;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ui/settings/offline/-$$Lambda$StoreAndForwardSettingsView$W9KpCfYadZYEtjiki9LLHkjLQd8;-><init>(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method initializeMaxAmountScrubber(Lcom/squareup/text/Scrubber;)V
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v1, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->transactionLimit:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/Scrubber;)V

    return-void
.end method

.method public synthetic lambda$initializeFocusListener$1$StoreAndForwardSettingsView(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;Z)V
    .locals 0

    .line 67
    iget-object p3, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->transactionLimit:Lcom/squareup/widgets/SelectableEditText;

    if-eqz p4, :cond_0

    goto :goto_0

    :cond_0
    move-object p1, p2

    :goto_0
    invoke-virtual {p3, p1}, Lcom/squareup/widgets/SelectableEditText;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$StoreAndForwardSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->presenter:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;

    invoke-virtual {p1, p0, p2}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->onEnabledToggled(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;Z)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->presenter:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->dropView(Landroid/view/ViewGroup;)V

    .line 62
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 40
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 41
    sget v0, Lcom/squareup/settingsapplet/R$id;->enable_store_and_forward:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->storeAndForwardEnabledSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->storeAndForwardEnabledSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/offline/-$$Lambda$StoreAndForwardSettingsView$1qiDDMCw51V6UE1hs2PrjizS-SU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/offline/-$$Lambda$StoreAndForwardSettingsView$1qiDDMCw51V6UE1hs2PrjizS-SU;-><init>(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 46
    sget v0, Lcom/squareup/settingsapplet/R$id;->offline_payments_hint:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 47
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/settingsapplet/R$string;->offline_mode_enable_hint:I

    const-string v3, "support_center"

    .line 48
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/billhistoryui/R$string;->offline_mode_url:I

    .line 49
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/cardreader/ui/R$string;->support_center:I

    .line 50
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 51
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 47
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    sget v0, Lcom/squareup/settingsapplet/R$id;->transaction_limit_section:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->transactionLimitSection:Landroid/view/View;

    .line 54
    sget v0, Lcom/squareup/settingsapplet/R$id;->transaction_limit:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->transactionLimit:Lcom/squareup/widgets/SelectableEditText;

    .line 55
    sget v0, Lcom/squareup/settingsapplet/R$id;->transaction_limit_hint:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->limitHint:Landroid/widget/TextView;

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->presenter:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method setLimitHint(I)V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->limitHint:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method setStoreAndForwardEnabled(ZZ)V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->storeAndForwardEnabledSwitch:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(ZZ)V

    return-void
.end method

.method setTransactionLimit(Ljava/lang/String;)V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->transactionLimit:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setTransactionLimitSectionVisible(Z)V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->transactionLimitSection:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
