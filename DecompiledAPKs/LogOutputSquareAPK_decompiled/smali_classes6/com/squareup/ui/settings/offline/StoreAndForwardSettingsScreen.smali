.class public final Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "StoreAndForwardSettingsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Component;,
        Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen;

    .line 166
    sget-object v0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen;

    .line 167
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 50
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_OFFLINE:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 46
    const-class v0, Lcom/squareup/ui/settings/offline/OfflineSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 170
    sget v0, Lcom/squareup/settingsapplet/R$layout;->store_and_forward_settings_view:I

    return v0
.end method
