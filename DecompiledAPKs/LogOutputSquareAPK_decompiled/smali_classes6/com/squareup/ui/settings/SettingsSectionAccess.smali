.class public abstract Lcom/squareup/ui/settings/SettingsSectionAccess;
.super Lcom/squareup/applet/SectionAccess;
.source "SettingsSectionAccess.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/SettingsSectionAccess$Unrestricted;,
        Lcom/squareup/ui/settings/SettingsSectionAccess$Restricted;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008&\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/ui/settings/SettingsSectionAccess;",
        "Lcom/squareup/applet/SectionAccess;",
        "()V",
        "Restricted",
        "Unrestricted",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/squareup/applet/SectionAccess;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/squareup/ui/settings/SettingsSectionAccess;-><init>()V

    return-void
.end method
