.class final Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupHourPicker$2;
.super Ljava/lang/Object;
.source "CloseOfDayDialogBuilder.kt"

# interfaces
.implements Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->setupHourPicker()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/noho/NohoNumberPicker;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "",
        "newHour",
        "onValueChange"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupHourPicker$2;->this$0:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onValueChange(Lcom/squareup/noho/NohoNumberPicker;II)V
    .locals 1

    .line 168
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupHourPicker$2;->this$0:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;

    invoke-static {p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->access$getDayOfWeekPicker$p(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;)Lcom/squareup/noho/NohoNumberPicker;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result p2

    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupHourPicker$2;->this$0:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;

    invoke-static {v0}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->access$getAmpmPicker$p(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;)Lcom/squareup/noho/NohoNumberPicker;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v0

    invoke-static {p1, p2, p3, v0}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->access$invalidCloseOfDay(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;III)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 170
    iget-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupHourPicker$2;->this$0:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;

    invoke-static {p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->access$getAmpmPicker$p(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;)Lcom/squareup/noho/NohoNumberPicker;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoNumberPicker;->setValue(I)V

    :cond_0
    return-void
.end method
