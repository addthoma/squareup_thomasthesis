.class public final Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory;
.super Ljava/lang/Object;
.source "CloseOfDayDialog.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCloseOfDayDialog.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CloseOfDayDialog.kt\ncom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,86:1\n52#2:87\n*E\n*S KotlinDebug\n*F\n+ 1 CloseOfDayDialog.kt\ncom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory\n*L\n42#1:87\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0018\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\tH\u0002J\u0010\u0010\u000c\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\tH\u0002\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "()V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "currentOrNextDay",
        "",
        "cutoffDay",
        "dayOfWeek",
        "zeroBasedCutoffDay",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$currentOrNextDay(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory;II)I
    .locals 0

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory;->currentOrNextDay(II)I

    move-result p0

    return p0
.end method

.method private final currentOrNextDay(II)I
    .locals 0

    .line 74
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory;->zeroBasedCutoffDay(I)I

    move-result p1

    sub-int/2addr p1, p2

    add-int/lit8 p1, p1, 0x7

    rem-int/lit8 p1, p1, 0x7

    return p1
.end method

.method private final zeroBasedCutoffDay(I)I
    .locals 0

    add-int/lit8 p1, p1, -0x2

    if-gez p1, :cond_0

    const/4 p1, 0x6

    :cond_0
    return p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/settingsapplet/R$array;->day_of_week:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.resources.getStr\u2026plet.R.array.day_of_week)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    const-class v1, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope$Component;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope$Component;

    .line 43
    invoke-interface {v1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope$Component;->scopeRunner()Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    move-result-object v1

    .line 45
    invoke-virtual {v1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->closeOfDayDialogScreenData()Lio/reactivex/Observable;

    move-result-object v2

    .line 46
    invoke-virtual {v2}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v2

    .line 47
    new-instance v3, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory$create$1;

    invoke-direct {v3, p0, v0, p1, v1}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory$create$1;-><init>(Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory;[Ljava/lang/String;Landroid/content/Context;Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;)V

    check-cast v3, Lio/reactivex/functions/Function;

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "scopeRunner.closeOfDayDi\u2026     .build()\n          }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
