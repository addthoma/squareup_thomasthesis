.class public final Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge_Factory;
.super Ljava/lang/Object;
.source "MerchantProfileBadge_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;",
        ">;"
    }
.end annotation


# instance fields
.field private final monitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final publicProfileSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge_Factory;->monitorProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge_Factory;->publicProfileSectionProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;",
            ">;)",
            "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;)Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;-><init>(Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge_Factory;->monitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge_Factory;->publicProfileSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;

    invoke-static {v0, v1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge_Factory;->newInstance(Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;)Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge_Factory;->get()Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBadge;

    move-result-object v0

    return-object v0
.end method
