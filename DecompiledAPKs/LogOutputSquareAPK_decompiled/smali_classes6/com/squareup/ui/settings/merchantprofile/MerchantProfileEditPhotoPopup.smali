.class Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup;
.super Lcom/squareup/flowlegacy/DialogPopup;
.source "MerchantProfileEditPhotoPopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;,
        Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;,
        Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/flowlegacy/DialogPopup<",
        "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;",
        "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/flowlegacy/DialogPopup;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic lambda$createDialog$0(Lcom/squareup/mortar/PopupPresenter;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;Landroid/content/DialogInterface;I)V
    .locals 0

    if-nez p3, :cond_0

    .line 44
    sget-object p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;->TAKE_PHOTO:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/4 p2, 0x1

    if-ne p3, p2, :cond_1

    .line 46
    sget-object p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;->CHOOSE_PHOTO:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    const/4 p2, 0x2

    if-ne p3, p2, :cond_2

    .line 48
    sget-object p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;->CLEAR_PHOTO:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    :goto_0
    return-void

    .line 50
    :cond_2
    new-instance p0, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Unknown photo option. "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method static synthetic lambda$createDialog$1(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 54
    sget-object p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;->CANCEL:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected createDialog(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/AlertDialog;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;",
            "Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;",
            "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;",
            ">;)",
            "Landroid/app/AlertDialog;"
        }
    .end annotation

    .line 28
    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;->access$000(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;)Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    move-result-object p1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 31
    sget v0, Lcom/squareup/common/strings/R$array;->photo_sources:I

    .line 32
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 33
    sget-object v1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;->EDITING_WITH_CLEAR:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    if-ne p1, v1, :cond_0

    .line 34
    array-length v1, v0

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    .line 35
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    sget v2, Lcom/squareup/settingsapplet/R$string;->clear_photo:I

    invoke-virtual {p2, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    aput-object p2, v0, v1

    .line 38
    :cond_0
    new-instance p2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p2, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;->ADDING:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    if-ne p1, v1, :cond_1

    sget v1, Lcom/squareup/registerlib/R$string;->add_photo:I

    goto :goto_0

    :cond_1
    sget v1, Lcom/squareup/registerlib/R$string;->edit_photo:I

    .line 39
    :goto_0
    invoke-virtual {p2, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p2

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileEditPhotoPopup$e1xUT5g2h3PFGcySewM1o4tz9GA;

    invoke-direct {v1, p3, p1}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileEditPhotoPopup$e1xUT5g2h3PFGcySewM1o4tz9GA;-><init>(Lcom/squareup/mortar/PopupPresenter;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;)V

    .line 42
    invoke-virtual {p2, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget p2, Lcom/squareup/common/strings/R$string;->cancel:I

    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileEditPhotoPopup$Oq3V6LFtazSwjr9TaUVrxrs8zIk;

    invoke-direct {v0, p3}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileEditPhotoPopup$Oq3V6LFtazSwjr9TaUVrxrs8zIk;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 53
    invoke-virtual {p1, p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 55
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic createDialog(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup;->createDialog(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
