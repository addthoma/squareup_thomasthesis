.class public final Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "MerchantProfileEditLogoScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Component;,
        Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final uri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 172
    sget-object v0, Lcom/squareup/ui/settings/merchantprofile/logo/-$$Lambda$MerchantProfileEditLogoScreen$ens7sGzRDJTsEcFrAXkf6tcBSxM;->INSTANCE:Lcom/squareup/ui/settings/merchantprofile/logo/-$$Lambda$MerchantProfileEditLogoScreen$ens7sGzRDJTsEcFrAXkf6tcBSxM;

    .line 173
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 0

    .line 43
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen;->uri:Landroid/net/Uri;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen;)Landroid/net/Uri;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen;->uri:Landroid/net/Uri;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen;
    .locals 1

    .line 174
    const-class v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 175
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Landroid/net/Uri;

    .line 176
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen;-><init>(Landroid/net/Uri;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 163
    iget-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen;->uri:Landroid/net/Uri;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 48
    const-class v0, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 180
    sget v0, Lcom/squareup/settingsapplet/R$layout;->merchant_profile_edit_logo_view:I

    return v0
.end method
