.class final enum Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;
.super Ljava/lang/Enum;
.source "MerchantProfileEditPhotoPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

.field public static final enum ADDING:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

.field public static final enum EDITING:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

.field public static final enum EDITING_WITH_CLEAR:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 97
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    const/4 v1, 0x0

    const-string v2, "ADDING"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;->ADDING:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    .line 98
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    const/4 v2, 0x1

    const-string v3, "EDITING"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;->EDITING:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    .line 99
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    const/4 v3, 0x2

    const-string v4, "EDITING_WITH_CLEAR"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;->EDITING_WITH_CLEAR:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    .line 96
    sget-object v4, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;->ADDING:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;->EDITING:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;->EDITING_WITH_CLEAR:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;->$VALUES:[Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 96
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;
    .locals 1

    .line 96
    const-class v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;
    .locals 1

    .line 96
    sget-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;->$VALUES:[Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    invoke-virtual {v0}, [Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    return-object v0
.end method
