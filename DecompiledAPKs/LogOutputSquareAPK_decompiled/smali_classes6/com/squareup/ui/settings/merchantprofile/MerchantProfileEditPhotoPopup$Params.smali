.class Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;
.super Ljava/lang/Object;
.source "MerchantProfileEditPhotoPopup.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Params"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mode:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 78
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params$1;

    invoke-direct {v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 1

    .line 67
    invoke-static {}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;->values()[Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    move-result-object v0

    aget-object p1, v0, p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;)V

    return-void
.end method

.method synthetic constructor <init>(ILcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$1;)V
    .locals 0

    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;-><init>(I)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;)V
    .locals 0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;->mode:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;)Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;
    .locals 0

    .line 58
    iget-object p0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;->mode:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    return-object p0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 75
    iget-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;->mode:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;

    invoke-virtual {p2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Mode;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
