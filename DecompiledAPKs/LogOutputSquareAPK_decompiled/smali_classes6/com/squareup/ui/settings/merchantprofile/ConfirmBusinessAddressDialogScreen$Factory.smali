.class public final Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory;
.super Ljava/lang/Object;
.source "ConfirmBusinessAddressDialogScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nConfirmBusinessAddressDialogScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ConfirmBusinessAddressDialogScreen.kt\ncom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory\n+ 2 MarketSpan.kt\ncom/squareup/marketfont/MarketSpanKt\n*L\n1#1,142:1\n17#2,2:143\n*E\n*S KotlinDebug\n*F\n+ 1 ConfirmBusinessAddressDialogScreen.kt\ncom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory\n*L\n76#1,2:143\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J(\u0010\u0008\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0002J \u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "()V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "runner",
        "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "address",
        "",
        "createDialogMobileCase",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;Lcom/squareup/analytics/Analytics;Ljava/lang/String;)Landroid/app/Dialog;
    .locals 4

    .line 73
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 74
    sget v1, Lcom/squareup/settingsapplet/R$string;->merchant_profile_confirm_business_address_dialog_message_address:I

    .line 72
    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 76
    check-cast p4, Ljava/lang/CharSequence;

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 144
    new-instance v2, Lcom/squareup/fonts/FontSpan;

    const/4 v3, 0x0

    invoke-static {v1, v3}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result v1

    invoke-direct {v2, p1, v1}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    check-cast v2, Landroid/text/style/CharacterStyle;

    .line 76
    invoke-static {p4, v2}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p4

    check-cast p4, Ljava/lang/CharSequence;

    const-string v1, "address"

    invoke-virtual {v0, v1, p4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p4

    .line 77
    invoke-virtual {p4}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p4

    .line 79
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 80
    sget p1, Lcom/squareup/settingsapplet/R$string;->merchant_profile_confirm_business_address_dialog_title:I

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 81
    invoke-virtual {p1, p4}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 83
    sget p4, Lcom/squareup/settingsapplet/R$string;->merchant_profile_confirm_business_address_dialog_button_positive:I

    .line 84
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory$createDialog$1;

    invoke-direct {v0, p2, p3}, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory$createDialog$1;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;Lcom/squareup/analytics/Analytics;)V

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    .line 82
    invoke-virtual {p1, p4, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 89
    sget p2, Lcom/squareup/marin/R$drawable;->marin_selector_blue:I

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 90
    sget p2, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 92
    sget p2, Lcom/squareup/settingsapplet/R$string;->merchant_profile_confirm_business_address_dialog_button_negative:I

    .line 93
    new-instance p4, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory$createDialog$2;

    invoke-direct {p4, p3}, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory$createDialog$2;-><init>(Lcom/squareup/analytics/Analytics;)V

    check-cast p4, Landroid/content/DialogInterface$OnClickListener;

    .line 91
    invoke-virtual {p1, p2, p4}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 97
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "ThemedAlertDialog.Builde\u2026    }\n          .create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/app/Dialog;

    return-object p1
.end method

.method private final createDialogMobileCase(Landroid/content/Context;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;Lcom/squareup/analytics/Analytics;)Landroid/app/Dialog;
    .locals 2

    .line 105
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 106
    sget p1, Lcom/squareup/settingsapplet/R$string;->merchant_profile_confirm_business_address_dialog_title:I

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 107
    sget v0, Lcom/squareup/settingsapplet/R$string;->merchant_profile_confirm_business_address_dialog_message_mobile:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 109
    sget v0, Lcom/squareup/settingsapplet/R$string;->merchant_profile_confirm_business_address_dialog_button_positive:I

    .line 110
    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory$createDialogMobileCase$1;

    invoke-direct {v1, p2, p3}, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory$createDialogMobileCase$1;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;Lcom/squareup/analytics/Analytics;)V

    check-cast v1, Landroid/content/DialogInterface$OnClickListener;

    .line 108
    invoke-virtual {p1, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 115
    sget p2, Lcom/squareup/marin/R$drawable;->marin_selector_blue:I

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 116
    sget p2, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 118
    sget p2, Lcom/squareup/settingsapplet/R$string;->merchant_profile_confirm_business_address_dialog_button_negative:I

    .line 119
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory$createDialogMobileCase$2;

    invoke-direct {v0, p3}, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory$createDialogMobileCase$2;-><init>(Lcom/squareup/analytics/Analytics;)V

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    .line 117
    invoke-virtual {p1, p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 123
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "ThemedAlertDialog.Builde\u2026    }\n          .create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/app/Dialog;

    return-object p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-static {p1}, Lcom/squareup/ui/settings/InSettingsAppletScope;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    const-string v1, "get(context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen;

    .line 54
    const-class v1, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Component;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Component;

    .line 56
    invoke-interface {v1}, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Component;->merchantProfileRunner()Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;

    move-result-object v2

    .line 57
    invoke-interface {v1}, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Component;->analytics()Lcom/squareup/analytics/Analytics;

    move-result-object v1

    .line 58
    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen;->getData()Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;->getConfirmMobileBusiness()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 60
    invoke-direct {p0, p1, v2, v1}, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory;->createDialogMobileCase(Landroid/content/Context;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;Lcom/squareup/analytics/Analytics;)Landroid/app/Dialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(createDialog\u2026text, runner, analytics))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v2, v1, v0}, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory;->createDialog(Landroid/content/Context;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;Lcom/squareup/analytics/Analytics;Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(createDialog\u2026analytics, data.address))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method
