.class Lcom/squareup/ui/permissions/SystemPermissionDialog$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "SystemPermissionDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/permissions/SystemPermissionDialog;-><init>(Landroid/content/Context;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/systempermissions/SystemPermission;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/permissions/SystemPermissionDialog;

.field final synthetic val$permission:Lcom/squareup/systempermissions/SystemPermission;

.field final synthetic val$permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/permissions/SystemPermissionDialog;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/systempermissions/SystemPermission;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/permissions/SystemPermissionDialog$1;->this$0:Lcom/squareup/ui/permissions/SystemPermissionDialog;

    iput-object p2, p0, Lcom/squareup/ui/permissions/SystemPermissionDialog$1;->val$permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    iput-object p3, p0, Lcom/squareup/ui/permissions/SystemPermissionDialog$1;->val$permission:Lcom/squareup/systempermissions/SystemPermission;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/permissions/SystemPermissionDialog$1;->this$0:Lcom/squareup/ui/permissions/SystemPermissionDialog;

    iget-object v0, p0, Lcom/squareup/ui/permissions/SystemPermissionDialog$1;->val$permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    iget-object v1, p0, Lcom/squareup/ui/permissions/SystemPermissionDialog$1;->val$permission:Lcom/squareup/systempermissions/SystemPermission;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/permissions/SystemPermissionDialog;->onRequestPermission(Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/systempermissions/SystemPermission;)V

    return-void
.end method
