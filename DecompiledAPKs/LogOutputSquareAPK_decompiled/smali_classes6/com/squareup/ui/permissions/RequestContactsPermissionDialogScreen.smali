.class public final Lcom/squareup/ui/permissions/RequestContactsPermissionDialogScreen;
.super Lcom/squareup/ui/crm/flow/InUpdateCustomerScope;
.source "RequestContactsPermissionDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/permissions/RequestContactsPermissionDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/permissions/RequestContactsPermissionDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/permissions/RequestContactsPermissionDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 40
    sget-object v0, Lcom/squareup/ui/permissions/-$$Lambda$RequestContactsPermissionDialogScreen$0UDIbYLHd-7Qo1owRj2KCp8rndM;->INSTANCE:Lcom/squareup/ui/permissions/-$$Lambda$RequestContactsPermissionDialogScreen$0UDIbYLHd-7Qo1owRj2KCp8rndM;

    .line 41
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/permissions/RequestContactsPermissionDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InUpdateCustomerScope;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/permissions/RequestContactsPermissionDialogScreen;
    .locals 1

    .line 42
    const-class v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    .line 43
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    .line 44
    new-instance v0, Lcom/squareup/ui/permissions/RequestContactsPermissionDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/permissions/RequestContactsPermissionDialogScreen;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 36
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/flow/InUpdateCustomerScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/permissions/RequestContactsPermissionDialogScreen;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
