.class public final Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "EnterPasscodeToUnlockScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Component;,
        Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$ParentComponent;,
        Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final DISMISSED:Ljava/lang/String; = "Dismissed EnterPasscodeToUnlockScreen"

.field public static final INSTANCE:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;

.field public static final SHOWN:Ljava/lang/String; = "Shown EnterPasscodeToUnlockScreen"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;

    invoke-direct {v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;->INSTANCE:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;

    .line 224
    sget-object v0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;->INSTANCE:Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;

    .line 225
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 58
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 55
    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 51
    sget v0, Lcom/squareup/ui/permissions/R$layout;->enter_passcode_to_unlock_view:I

    return v0
.end method
