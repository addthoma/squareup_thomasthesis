.class public Lcom/squareup/ui/permissions/PermissionDeniedView;
.super Lcom/squareup/ui/DialogCardView;
.source "PermissionDeniedView.java"


# instance fields
.field private accessDeniedGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private passcodeEnabled:Z

.field private passcodePad:Lcom/squareup/padlock/Padlock;

.field presenter:Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private starGroup:Lcom/squareup/ui/StarGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/DialogCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x1

    .line 29
    iput-boolean p2, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->passcodeEnabled:Z

    .line 33
    const-class p2, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Component;->inject(Lcom/squareup/ui/permissions/PermissionDeniedView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/permissions/PermissionDeniedView;)Z
    .locals 0

    .line 20
    iget-boolean p0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->passcodeEnabled:Z

    return p0
.end method

.method private bindViews()V
    .locals 1

    .line 126
    sget v0, Lcom/squareup/ui/permissions/R$id;->permission_actionbar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 127
    sget v0, Lcom/squareup/ui/permissions/R$id;->permission_passcode_pad:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock;

    iput-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->passcodePad:Lcom/squareup/padlock/Padlock;

    .line 128
    sget v0, Lcom/squareup/ui/permissions/R$id;->star_group:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/StarGroup;

    iput-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->starGroup:Lcom/squareup/ui/StarGroup;

    .line 129
    sget v0, Lcom/squareup/ui/permissions/R$id;->access_denied_glyph_message:I

    .line 130
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->accessDeniedGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    return-void
.end method

.method private showAccessDenied()V
    .locals 3

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/permissions/-$$Lambda$PermissionDeniedView$lIf2sX_3DtPoz2gnGOUn2OrzTwg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/permissions/-$$Lambda$PermissionDeniedView$lIf2sX_3DtPoz2gnGOUn2OrzTwg;-><init>(Lcom/squareup/ui/permissions/PermissionDeniedView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->accessDeniedGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    new-instance v1, Lcom/squareup/ui/permissions/PermissionDeniedView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/permissions/PermissionDeniedView$1;-><init>(Lcom/squareup/ui/permissions/PermissionDeniedView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->accessDeniedGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    return-void
.end method

.method private showPasscodePad()V
    .locals 4

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->presenter:Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->allowAccountOwnerPasscodeOnly()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    sget v0, Lcom/squareup/ui/permissions/R$string;->enter_passcode_account_owner:I

    goto :goto_0

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->presenter:Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->allowAccountOwnerOrAdminPasscodeOnly()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    sget v0, Lcom/squareup/ui/permissions/R$string;->enter_passcode_account_owner_or_admin:I

    goto :goto_0

    .line 76
    :cond_1
    sget v0, Lcom/squareup/ui/permissions/R$string;->enter_passcode:I

    .line 78
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 79
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/PermissionDeniedView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/permissions/-$$Lambda$PermissionDeniedView$jI_S-Ya3qqAe0e7HT4yEV2x7d1M;

    invoke-direct {v1, p0}, Lcom/squareup/ui/permissions/-$$Lambda$PermissionDeniedView$jI_S-Ya3qqAe0e7HT4yEV2x7d1M;-><init>(Lcom/squareup/ui/permissions/PermissionDeniedView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->passcodePad:Lcom/squareup/padlock/Padlock;

    invoke-virtual {p0}, Lcom/squareup/ui/permissions/PermissionDeniedView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->LIGHT:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3, v3}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;ZZ)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/padlock/Padlock;->setTypeface(Landroid/graphics/Typeface;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->passcodePad:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v0, v3}, Lcom/squareup/padlock/Padlock;->setClearEnabled(Z)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->passcodePad:Lcom/squareup/padlock/Padlock;

    new-instance v1, Lcom/squareup/ui/permissions/PermissionDeniedView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/permissions/PermissionDeniedView$2;-><init>(Lcom/squareup/ui/permissions/PermissionDeniedView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/padlock/Padlock;->setOnKeyPressListener(Lcom/squareup/padlock/Padlock$OnKeyPressListener;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->passcodePad:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v0, v3}, Lcom/squareup/padlock/Padlock;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->starGroup:Lcom/squareup/ui/StarGroup;

    invoke-virtual {v0, v3}, Lcom/squareup/ui/StarGroup;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method incorrectPasscode()V
    .locals 1

    const/4 v0, 0x1

    .line 104
    invoke-virtual {p0, v0}, Lcom/squareup/ui/permissions/PermissionDeniedView;->setPasscodePadEnabled(Z)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->starGroup:Lcom/squareup/ui/StarGroup;

    invoke-virtual {v0}, Lcom/squareup/ui/StarGroup;->wiggleClear()V

    return-void
.end method

.method public synthetic lambda$showAccessDenied$0$PermissionDeniedView()V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->presenter:Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->cancel()V

    return-void
.end method

.method public synthetic lambda$showPasscodePad$1$PermissionDeniedView()V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->presenter:Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->cancel()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 37
    invoke-super {p0}, Lcom/squareup/ui/DialogCardView;->onAttachedToWindow()V

    .line 38
    invoke-direct {p0}, Lcom/squareup/ui/permissions/PermissionDeniedView;->bindViews()V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->presenter:Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->presenter:Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->shouldAskForPasscode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    invoke-direct {p0}, Lcom/squareup/ui/permissions/PermissionDeniedView;->showPasscodePad()V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->presenter:Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->initPasscodePad()V

    goto :goto_0

    .line 48
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/permissions/PermissionDeniedView;->showAccessDenied()V

    :goto_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->presenter:Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 54
    invoke-super {p0}, Lcom/squareup/ui/DialogCardView;->onDetachedFromWindow()V

    return-void
.end method

.method setClearEnabled(Z)V
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->passcodePad:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock;->setClearEnabled(Z)V

    return-void
.end method

.method setExpectedStars(I)V
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->starGroup:Lcom/squareup/ui/StarGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/StarGroup;->setExpectedStarCount(I)V

    return-void
.end method

.method setPasscodePadEnabled(Z)V
    .locals 1

    .line 109
    iput-boolean p1, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->passcodeEnabled:Z

    .line 110
    iget-object p1, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->passcodePad:Lcom/squareup/padlock/Padlock;

    iget-boolean v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->passcodeEnabled:Z

    invoke-virtual {p1, v0}, Lcom/squareup/padlock/Padlock;->setDigitsEnabled(Z)V

    return-void
.end method

.method setStarCount(I)V
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView;->starGroup:Lcom/squareup/ui/StarGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/StarGroup;->setStarCount(I)V

    return-void
.end method
