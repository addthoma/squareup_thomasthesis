.class public Lcom/squareup/ui/main/SessionExpiredDialog$Factory;
.super Ljava/lang/Object;
.source "SessionExpiredDialog.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/SessionExpiredDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/main/SessionExpiredDialog$Runner;Landroid/content/Context;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 51
    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/SessionExpiredDialog$Runner;->dismissed(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 42
    const-class v0, Lcom/squareup/ui/main/SessionExpiredDialog$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/SessionExpiredDialog$Component;

    .line 43
    invoke-interface {v0}, Lcom/squareup/ui/main/SessionExpiredDialog$Component;->getSessionExpiredDialogRunner()Lcom/squareup/ui/main/SessionExpiredDialog$Runner;

    move-result-object v1

    .line 44
    invoke-interface {v0}, Lcom/squareup/ui/main/SessionExpiredDialog$Component;->getAppNameFormatter()Lcom/squareup/util/AppNameFormatter;

    move-result-object v0

    .line 46
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/squareup/common/strings/R$string;->session_expired_title:I

    .line 47
    invoke-virtual {v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v2

    sget v3, Lcom/squareup/common/strings/R$string;->session_expired_message:I

    .line 48
    invoke-interface {v0, v3}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    sget v2, Lcom/squareup/common/strings/R$string;->ok:I

    new-instance v3, Lcom/squareup/ui/main/-$$Lambda$SessionExpiredDialog$Factory$dIa4Y62YlE57TrpGjEFfR8W8a3M;

    invoke-direct {v3, v1, p1}, Lcom/squareup/ui/main/-$$Lambda$SessionExpiredDialog$Factory$dIa4Y62YlE57TrpGjEFfR8W8a3M;-><init>(Lcom/squareup/ui/main/SessionExpiredDialog$Runner;Landroid/content/Context;)V

    .line 50
    invoke-virtual {v0, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 54
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 55
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 46
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
