.class public final Lcom/squareup/ui/main/R6FirstTimeVideoScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "R6FirstTimeVideoScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Component;,
        Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;,
        Lcom/squareup/ui/main/R6FirstTimeVideoScreen$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/main/R6FirstTimeVideoScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/main/R6FirstTimeVideoScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/ui/main/R6FirstTimeVideoScreen;

    invoke-direct {v0}, Lcom/squareup/ui/main/R6FirstTimeVideoScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/R6FirstTimeVideoScreen;->INSTANCE:Lcom/squareup/ui/main/R6FirstTimeVideoScreen;

    .line 124
    sget-object v0, Lcom/squareup/ui/main/R6FirstTimeVideoScreen;->INSTANCE:Lcom/squareup/ui/main/R6FirstTimeVideoScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/R6FirstTimeVideoScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 39
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 35
    sget v0, Lcom/squareup/readertutorial/R$layout;->r6_first_time_video_view:I

    return v0
.end method
