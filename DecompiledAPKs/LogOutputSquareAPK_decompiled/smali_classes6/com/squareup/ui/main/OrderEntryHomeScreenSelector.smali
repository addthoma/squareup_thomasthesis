.class public Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;
.super Ljava/lang/Object;
.source "OrderEntryHomeScreenSelector.java"

# interfaces
.implements Lcom/squareup/ui/main/HomeScreenSelector;


# instance fields
.field private final openTicketsSelector:Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;

.field private final tenderStarter:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final timecardsHomeScreenRedirector:Lcom/squareup/ui/timecards/api/TimecardsHomeScreenRedirector;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;Ldagger/Lazy;Lcom/squareup/ui/timecards/api/TimecardsHomeScreenRedirector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Lcom/squareup/ui/timecards/api/TimecardsHomeScreenRedirector;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;->openTicketsSelector:Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;

    .line 40
    iput-object p2, p0, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;->tenderStarter:Ldagger/Lazy;

    .line 41
    iput-object p3, p0, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;->timecardsHomeScreenRedirector:Lcom/squareup/ui/timecards/api/TimecardsHomeScreenRedirector;

    return-void
.end method

.method private getOrderEntryHomeScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 103
    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreen;->LAST_SELECTED:Lcom/squareup/orderentry/OrderEntryScreen;

    return-object v0
.end method


# virtual methods
.method public redirectForOpenTicketsHomeScreen(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 6

    .line 48
    invoke-direct {p0}, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;->getOrderEntryHomeScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/container/Traversals;->leavingScreen(Lflow/Traversal;Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    .line 49
    const-class v1, Lcom/squareup/ui/seller/SellerScope;

    invoke-static {p1, v1}, Lcom/squareup/container/Traversals;->leavingScope(Lflow/Traversal;Ljava/lang/Class;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;->tenderStarter:Ldagger/Lazy;

    .line 50
    invoke-interface {v1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/tender/TenderStarter;

    iget-object v4, p1, Lflow/Traversal;->origin:Lflow/History;

    invoke-static {v4}, Lcom/squareup/container/Traversals;->topNonDialog(Lflow/History;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v4

    invoke-interface {v1, v4}, Lcom/squareup/ui/tender/TenderStarter;->inTenderFlow(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    .line 51
    :goto_1
    const-class v4, Lcom/squareup/ui/seller/SellerScope;

    invoke-static {p1, v4}, Lcom/squareup/container/Traversals;->enteringScope(Lflow/Traversal;Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;->tenderStarter:Ldagger/Lazy;

    .line 52
    invoke-interface {v4}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/tender/TenderStarter;

    iget-object v5, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-static {v5}, Lcom/squareup/container/Traversals;->topNonDialog(Lflow/History;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/squareup/ui/tender/TenderStarter;->inTenderFlow(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v4, 0x1

    :goto_3
    if-eqz v1, :cond_4

    if-eqz v4, :cond_4

    const/4 v2, 0x1

    :cond_4
    const/4 v1, 0x0

    if-eqz v2, :cond_5

    .line 55
    iget-object v2, p0, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;->openTicketsSelector:Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;

    invoke-interface {v2, p1}, Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;->leavingSplitTicketScreen(Lflow/Traversal;)Z

    move-result v2

    if-nez v2, :cond_5

    return-object v1

    .line 60
    :cond_5
    invoke-static {p1}, Lcom/squareup/cart/util/PartOfCartScopeKt;->reachableFromCart(Lflow/Traversal;)Z

    move-result v2

    if-eqz v2, :cond_6

    return-object v1

    .line 66
    :cond_6
    sget-object v2, Lcom/squareup/ui/permissions/PermissionDeniedScreen;->INSTANCE:Lcom/squareup/ui/permissions/PermissionDeniedScreen;

    invoke-static {p1, v2}, Lcom/squareup/container/Traversals;->enteringScreen(Lflow/Traversal;Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v2

    if-nez v2, :cond_b

    sget-object v2, Lcom/squareup/ui/permissions/PermissionDeniedScreen;->INSTANCE:Lcom/squareup/ui/permissions/PermissionDeniedScreen;

    .line 67
    invoke-static {p1, v2}, Lcom/squareup/container/Traversals;->leavingScreen(Lflow/Traversal;Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v2

    if-eqz v2, :cond_7

    goto :goto_4

    .line 76
    :cond_7
    iget-object v2, p0, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;->timecardsHomeScreenRedirector:Lcom/squareup/ui/timecards/api/TimecardsHomeScreenRedirector;

    invoke-interface {v2, p1, v0}, Lcom/squareup/ui/timecards/api/TimecardsHomeScreenRedirector;->shouldNotRedirectForOpenTicketsHomeScreen(Lflow/Traversal;Z)Z

    move-result v2

    if-eqz v2, :cond_8

    return-object v1

    .line 81
    :cond_8
    iget-object v2, p0, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;->openTicketsSelector:Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;

    iget-object v3, p1, Lflow/Traversal;->destination:Lflow/History;

    .line 82
    invoke-direct {p0}, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;->getOrderEntryHomeScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v4

    .line 81
    invoke-interface {v2, v3, v4}, Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;->updateHomeForOpenTickets(Lflow/History;Lcom/squareup/container/ContainerTreeKey;)Lflow/History;

    move-result-object v2

    .line 85
    iget-object v3, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v2, v3}, Lflow/History;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    return-object v1

    .line 89
    :cond_9
    iget-object p1, p1, Lflow/Traversal;->direction:Lflow/Direction;

    if-eqz v0, :cond_a

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/main/OrderEntryHomeScreenSelector;->openTicketsSelector:Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;

    .line 94
    invoke-static {v2}, Lcom/squareup/container/Traversals;->topNonDialog(Lflow/History;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;->isOpenTicketsHomeScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-eqz v0, :cond_a

    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    if-ne p1, v0, :cond_a

    .line 96
    sget-object p1, Lflow/Direction;->FORWARD:Lflow/Direction;

    .line 99
    :cond_a
    new-instance v0, Lcom/squareup/container/RedirectStep$Result;

    invoke-static {v2, p1}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    const-string v1, "OpenTicketsHomeScreen"

    invoke-direct {v0, v1, p1}, Lcom/squareup/container/RedirectStep$Result;-><init>(Ljava/lang/String;Lcom/squareup/container/Command;)V

    return-object v0

    :cond_b
    :goto_4
    return-object v1
.end method
