.class public final Lcom/squareup/ui/main/LibraryCreateNewItemDialog$Factory;
.super Ljava/lang/Object;
.source "LibraryCreateNewItemDialog.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/LibraryCreateNewItemDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLibraryCreateNewItemDialog.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LibraryCreateNewItemDialog.kt\ncom/squareup/ui/main/LibraryCreateNewItemDialog$Factory\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,57:1\n52#2:58\n*E\n*S KotlinDebug\n*F\n+ 1 LibraryCreateNewItemDialog.kt\ncom/squareup/ui/main/LibraryCreateNewItemDialog$Factory\n*L\n43#1:58\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0018\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u0005H\u0002\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/main/LibraryCreateNewItemDialog$Factory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "()V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "setupView",
        "",
        "view",
        "Landroid/view/View;",
        "dialog",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final setupView(Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3

    .line 43
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "view.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    const-class v1, Lcom/squareup/ui/main/LibraryCreateNewItemDialog$ParentComponent;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 43
    check-cast v0, Lcom/squareup/ui/main/LibraryCreateNewItemDialog$ParentComponent;

    .line 44
    invoke-interface {v0}, Lcom/squareup/ui/main/LibraryCreateNewItemDialog$ParentComponent;->editItemGateway()Lcom/squareup/ui/items/EditItemGateway;

    move-result-object v0

    .line 46
    sget v1, Lcom/squareup/orderentry/R$id;->create_item_button:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 47
    new-instance v2, Lcom/squareup/ui/main/LibraryCreateNewItemDialog$Factory$setupView$1;

    invoke-direct {v2, v0}, Lcom/squareup/ui/main/LibraryCreateNewItemDialog$Factory$setupView$1;-><init>(Lcom/squareup/ui/items/EditItemGateway;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    sget v1, Lcom/squareup/orderentry/R$id;->create_discount_button:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 49
    new-instance v2, Lcom/squareup/ui/main/LibraryCreateNewItemDialog$Factory$setupView$2;

    invoke-direct {v2, v0}, Lcom/squareup/ui/main/LibraryCreateNewItemDialog$Factory$setupView$2;-><init>(Lcom/squareup/ui/items/EditItemGateway;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    sget v0, Lcom/squareup/orderentry/R$id;->dismiss_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    .line 51
    new-instance v0, Lcom/squareup/ui/main/LibraryCreateNewItemDialog$Factory$setupView$3;

    invoke-direct {v0, p2}, Lcom/squareup/ui/main/LibraryCreateNewItemDialog$Factory$setupView$3;-><init>(Landroid/app/Dialog;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 31
    sget v1, Lcom/squareup/orderentry/R$layout;->library_create_new_item_dialog_view:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 32
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 33
    invoke-virtual {v1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 34
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string v1, "view"

    .line 35
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "dialog"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Landroid/app/Dialog;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/main/LibraryCreateNewItemDialog$Factory;->setupView(Landroid/view/View;Landroid/app/Dialog;)V

    .line 36
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "just(dialog)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
