.class public final Lcom/squareup/ui/main/MainActivityContainer$setStartHistoryFactoryToDefault$1;
.super Ljava/lang/Object;
.source "MainActivityContainer.kt"

# interfaces
.implements Lcom/squareup/ui/main/HistoryFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/MainActivityContainer;->setStartHistoryFactoryToDefault()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000#\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u001a\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0003H\u0016J\u0010\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u0008H\u0016\u00a8\u0006\n"
    }
    d2 = {
        "com/squareup/ui/main/MainActivityContainer$setStartHistoryFactoryToDefault$1",
        "Lcom/squareup/ui/main/HistoryFactory;",
        "createHistory",
        "Lflow/History;",
        "home",
        "Lcom/squareup/ui/main/Home;",
        "currentHistory",
        "getPermissions",
        "",
        "Lcom/squareup/permissions/Permission;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
    .locals 1

    const-string v0, "home"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 484
    invoke-static {p1, p2}, Lcom/squareup/ui/main/HomeKt;->getHomeHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;

    move-result-object p1

    return-object p1
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method
