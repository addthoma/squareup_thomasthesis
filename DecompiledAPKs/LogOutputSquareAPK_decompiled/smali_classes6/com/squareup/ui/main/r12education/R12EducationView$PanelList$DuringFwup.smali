.class final enum Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;
.super Ljava/lang/Enum;
.source "R12EducationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "DuringFwup"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

.field public static final enum DURING_BLOCKING_FWUP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

.field public static final enum NO_FWUP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 294
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

    const/4 v1, 0x0

    const-string v2, "DURING_BLOCKING_FWUP"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;->DURING_BLOCKING_FWUP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

    .line 295
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

    const/4 v2, 0x1

    const-string v3, "NO_FWUP"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;->NO_FWUP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

    .line 293
    sget-object v3, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;->DURING_BLOCKING_FWUP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;->NO_FWUP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;->$VALUES:[Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 293
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;
    .locals 1

    .line 293
    const-class v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;
    .locals 1

    .line 293
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;->$VALUES:[Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

    invoke-virtual {v0}, [Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

    return-object v0
.end method
