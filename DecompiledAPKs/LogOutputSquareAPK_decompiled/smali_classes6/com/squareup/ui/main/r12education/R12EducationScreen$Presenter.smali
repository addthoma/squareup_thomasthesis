.class public Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "R12EducationScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/r12education/R12EducationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/main/r12education/R12EducationView;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

.field private final flow:Lflow/Flow;

.field private final r12BlockingUpdateScreenLauncher:Lcom/squareup/ui/main/ContentLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/ui/main/ContentLauncher<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final r12EducationDoneRedirector:Lcom/squareup/ui/help/R12EducationDoneRedirector;

.field private final res:Lcom/squareup/util/Res;

.field private final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

.field variant:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/ui/main/ContentLauncher;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lflow/Flow;Lcom/squareup/ui/help/R12EducationDoneRedirector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/tender/TenderStarter;",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            "Lcom/squareup/ui/main/ContentLauncher<",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/Features;",
            "Lflow/Flow;",
            "Lcom/squareup/ui/help/R12EducationDoneRedirector;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 86
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 87
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 88
    iput-object p2, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    .line 89
    iput-object p3, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    .line 90
    iput-object p4, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->r12BlockingUpdateScreenLauncher:Lcom/squareup/ui/main/ContentLauncher;

    .line 91
    iput-object p5, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 92
    iput-object p6, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 93
    iput-object p7, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    .line 94
    iput-object p8, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->flow:Lflow/Flow;

    .line 95
    iput-object p9, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->r12EducationDoneRedirector:Lcom/squareup/ui/help/R12EducationDoneRedirector;

    return-void
.end method

.method private exitToWarning()Z
    .locals 2

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->isProcessingBlockingR12FirmwareUpdate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->r12BlockingUpdateScreenLauncher:Lcom/squareup/ui/main/ContentLauncher;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/ui/main/ContentLauncher;->attemptToShowContent(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {v0}, Lcom/squareup/ui/tender/TenderStarter;->warnIfNfcEnabled()Z

    move-result v0

    return v0
.end method

.method private getContactlessPreference()Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->COUNTRY_PREFERS_CONTACTLESS_CARDS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->COUNTRY_PREFERS_CONTACTLESS_CARDS:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    return-object v0

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->COUNTRY_PREFERS_NO_CONTACTLESS_CARDS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->COUNTRY_PREFERS_NO_CONTACTLESS_CARDS:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    return-object v0

    .line 143
    :cond_1
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->NO_CL_PREFERENCE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    return-object v0
.end method


# virtual methods
.method logPanelPresented(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;)V
    .locals 3

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterViewName;->R12_MULTIPAGE_WALKTHROUGH_PAGE:Lcom/squareup/analytics/RegisterViewName;

    .line 156
    invoke-virtual {p1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->getLogDetailFromPanel()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;-><init>(Lcom/squareup/analytics/RegisterViewName;Ljava/lang/String;)V

    .line 155
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 99
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 101
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/r12education/R12EducationScreen;

    .line 102
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;

    .line 103
    invoke-static {p1}, Lcom/squareup/ui/main/r12education/R12EducationScreen;->access$000(Lcom/squareup/ui/main/r12education/R12EducationScreen;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;->DURING_BLOCKING_FWUP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;->NO_FWUP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;

    .line 104
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->getContactlessPreference()Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 105
    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getVideoUrlSettings()Lcom/squareup/settings/server/VideoUrlSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/VideoUrlSettings;->hasR12GettingStartedVideoInfo()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$VideoAvailable;->R12_EDUCATION_VIDEO_AVAILABLE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$VideoAvailable;

    goto :goto_1

    :cond_1
    sget-object v2, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$VideoAvailable;->NO_VIDEO_URL:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$VideoAvailable;

    :goto_1
    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;-><init>(Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$DuringFwup;Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$VideoAvailable;)V

    iput-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->variant:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;

    return-void
.end method

.method onNextClicked()V
    .locals 3

    .line 119
    invoke-virtual {p0}, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-virtual {v0}, Lcom/squareup/ui/main/r12education/R12EducationView;->advancePager()Z

    move-result v0

    if-nez v0, :cond_1

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    .line 121
    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->exitToWarning()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->R12_MULTIPAGE_WALKTHROUGH_COMPLETED_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;-><init>(Lcom/squareup/analytics/RegisterTapName;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->r12EducationDoneRedirector:Lcom/squareup/ui/help/R12EducationDoneRedirector;

    invoke-interface {v0}, Lcom/squareup/ui/help/R12EducationDoneRedirector;->redirect()V

    :cond_1
    return-void
.end method

.method onXClicked(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;)V
    .locals 3

    .line 110
    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->exitToWarning()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->R12_MULTIPAGE_WALKTHROUGH_DISMISSED:Lcom/squareup/analytics/RegisterActionName;

    .line 114
    invoke-virtual {p1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->getLogDetailFromPanel()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;)V

    .line 112
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 115
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method showIntroVideo()V
    .locals 3

    .line 130
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/MovieLauncher;

    invoke-virtual {p0}, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-virtual {v1}, Lcom/squareup/ui/main/r12education/R12EducationView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/settings/paymentdevices/MovieLauncher;-><init>(Landroid/content/Context;Lcom/squareup/util/Res;)V

    .line 131
    iget-object v1, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 132
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getVideoUrlSettings()Lcom/squareup/settings/server/VideoUrlSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/VideoUrlSettings;->getR12GettingStartedVideoYoutubeId()Ljava/lang/String;

    move-result-object v1

    .line 131
    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/MovieLauncher;->launchYouTubeMovie(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->R12_MULTIPAGE_WALKTHROUGH_INTRO_VIDEO:Lcom/squareup/analytics/RegisterTapName;

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/v1/R12FeatureTourEvent;-><init>(Lcom/squareup/analytics/RegisterTapName;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
