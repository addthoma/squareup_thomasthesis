.class final enum Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;
.super Ljava/lang/Enum;
.source "R12EducationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "PreferContactless"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

.field public static final enum COUNTRY_PREFERS_CONTACTLESS_CARDS:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

.field public static final enum COUNTRY_PREFERS_NO_CONTACTLESS_CARDS:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

.field public static final enum NO_CL_PREFERENCE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 299
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    const/4 v1, 0x0

    const-string v2, "COUNTRY_PREFERS_CONTACTLESS_CARDS"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->COUNTRY_PREFERS_CONTACTLESS_CARDS:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    .line 300
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    const/4 v2, 0x1

    const-string v3, "COUNTRY_PREFERS_NO_CONTACTLESS_CARDS"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->COUNTRY_PREFERS_NO_CONTACTLESS_CARDS:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    .line 301
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    const/4 v3, 0x2

    const-string v4, "NO_CL_PREFERENCE"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->NO_CL_PREFERENCE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    .line 298
    sget-object v4, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->COUNTRY_PREFERS_CONTACTLESS_CARDS:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->COUNTRY_PREFERS_NO_CONTACTLESS_CARDS:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->NO_CL_PREFERENCE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->$VALUES:[Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 298
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;
    .locals 1

    .line 298
    const-class v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;
    .locals 1

    .line 298
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->$VALUES:[Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    invoke-virtual {v0}, [Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/main/r12education/R12EducationView$PanelList$PreferContactless;

    return-object v0
.end method
