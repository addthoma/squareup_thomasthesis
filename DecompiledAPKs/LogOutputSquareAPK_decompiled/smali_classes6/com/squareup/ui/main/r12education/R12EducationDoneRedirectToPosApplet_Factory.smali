.class public final Lcom/squareup/ui/main/r12education/R12EducationDoneRedirectToPosApplet_Factory;
.super Ljava/lang/Object;
.source "R12EducationDoneRedirectToPosApplet_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/r12education/R12EducationDoneRedirectToPosApplet;",
        ">;"
    }
.end annotation


# instance fields
.field private final orderEntryAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationDoneRedirectToPosApplet_Factory;->orderEntryAppletProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/main/r12education/R12EducationDoneRedirectToPosApplet_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;)",
            "Lcom/squareup/ui/main/r12education/R12EducationDoneRedirectToPosApplet_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationDoneRedirectToPosApplet_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/r12education/R12EducationDoneRedirectToPosApplet_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/orderentry/OrderEntryAppletGateway;)Lcom/squareup/ui/main/r12education/R12EducationDoneRedirectToPosApplet;
    .locals 1

    .line 36
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationDoneRedirectToPosApplet;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/r12education/R12EducationDoneRedirectToPosApplet;-><init>(Lcom/squareup/orderentry/OrderEntryAppletGateway;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/r12education/R12EducationDoneRedirectToPosApplet;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationDoneRedirectToPosApplet_Factory;->orderEntryAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationDoneRedirectToPosApplet_Factory;->newInstance(Lcom/squareup/orderentry/OrderEntryAppletGateway;)Lcom/squareup/ui/main/r12education/R12EducationDoneRedirectToPosApplet;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/main/r12education/R12EducationDoneRedirectToPosApplet_Factory;->get()Lcom/squareup/ui/main/r12education/R12EducationDoneRedirectToPosApplet;

    move-result-object v0

    return-object v0
.end method
