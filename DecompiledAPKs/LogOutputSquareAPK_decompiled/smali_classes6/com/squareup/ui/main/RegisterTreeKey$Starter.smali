.class Lcom/squareup/ui/main/RegisterTreeKey$Starter;
.super Ljava/lang/Object;
.source "RegisterTreeKey.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/RegisterTreeKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Starter"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey$1;)V
    .locals 0

    .line 69
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey$Starter;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "watch-for-leaks"

    .line 71
    invoke-virtual {p1, v0}, Lmortar/MortarScope;->getService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    const-class v0, Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 73
    invoke-static {p1, v0}, Lcom/squareup/leakcanary/ScopedObjectWatcher;->watchForLeaks(Lmortar/MortarScope;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
