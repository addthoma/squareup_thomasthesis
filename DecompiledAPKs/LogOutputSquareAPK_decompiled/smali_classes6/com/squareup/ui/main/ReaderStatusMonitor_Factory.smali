.class public final Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;
.super Ljava/lang/Object;
.source "ReaderStatusMonitor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/ReaderStatusMonitor;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final apiReaderSettingsControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;"
        }
    .end annotation
.end field

.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderOracleProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final posContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p2, p0, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->apiReaderSettingsControllerProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p3, p0, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->badBusProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p4, p0, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p5, p0, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p6, p0, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p7, p0, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p8, p0, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->posContainerProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p9, p0, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->applicationProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;"
        }
    .end annotation

    .line 72
    new-instance v10, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/badbus/BadBus;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/settings/server/Features;Ldagger/Lazy;Lcom/squareup/ui/main/PosContainer;Landroid/app/Application;)Lcom/squareup/ui/main/ReaderStatusMonitor;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            "Lcom/squareup/settings/server/Features;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Landroid/app/Application;",
            ")",
            "Lcom/squareup/ui/main/ReaderStatusMonitor;"
        }
    .end annotation

    .line 79
    new-instance v10, Lcom/squareup/ui/main/ReaderStatusMonitor;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/main/ReaderStatusMonitor;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/badbus/BadBus;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/settings/server/Features;Ldagger/Lazy;Lcom/squareup/ui/main/PosContainer;Landroid/app/Application;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/ReaderStatusMonitor;
    .locals 10

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->apiReaderSettingsControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/api/ApiReaderSettingsController;

    iget-object v0, p0, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/badbus/BadBus;

    iget-object v0, p0, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/cardreader/CardReaderHubUtils;

    iget-object v0, p0, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    iget-object v0, p0, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v7

    iget-object v0, p0, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->posContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/main/PosContainer;

    iget-object v0, p0, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/app/Application;

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/badbus/BadBus;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/settings/server/Features;Ldagger/Lazy;Lcom/squareup/ui/main/PosContainer;Landroid/app/Application;)Lcom/squareup/ui/main/ReaderStatusMonitor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/main/ReaderStatusMonitor_Factory;->get()Lcom/squareup/ui/main/ReaderStatusMonitor;

    move-result-object v0

    return-object v0
.end method
