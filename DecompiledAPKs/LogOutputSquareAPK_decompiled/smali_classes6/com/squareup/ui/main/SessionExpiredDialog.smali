.class public Lcom/squareup/ui/main/SessionExpiredDialog;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "SessionExpiredDialog.java"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/main/SessionExpiredDialog$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/SessionExpiredDialog$Runner;,
        Lcom/squareup/ui/main/SessionExpiredDialog$Component;,
        Lcom/squareup/ui/main/SessionExpiredDialog$Factory;
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/main/SessionExpiredDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/ui/main/SessionExpiredDialog;

    invoke-direct {v0}, Lcom/squareup/ui/main/SessionExpiredDialog;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/SessionExpiredDialog;->INSTANCE:Lcom/squareup/ui/main/SessionExpiredDialog;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
