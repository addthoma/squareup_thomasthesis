.class public Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher;
.super Lcom/squareup/ui/main/DailyContentLauncher;
.source "R12BlockingUpdateScreenLauncher.java"


# instance fields
.field private final readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 23
    new-instance v0, Lcom/squareup/ui/main/-$$Lambda$R12BlockingUpdateScreenLauncher$ZxRKTKpnswAsijmPlKQW7YbMxgI;

    invoke-direct {v0, p3}, Lcom/squareup/ui/main/-$$Lambda$R12BlockingUpdateScreenLauncher$ZxRKTKpnswAsijmPlKQW7YbMxgI;-><init>(Lcom/squareup/ui/buyer/BuyerFlowStarter;)V

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/ui/main/DailyContentLauncher;-><init>(Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V

    .line 25
    iput-object p4, p0, Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    return-void
.end method

.method static synthetic lambda$new$0(Lcom/squareup/ui/buyer/BuyerFlowStarter;)Ljava/lang/Boolean;
    .locals 0

    .line 23
    invoke-interface {p0}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->isShowing()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method protected showContent()V
    .locals 3

    .line 29
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->R12_UPDATE_BLOCKING:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 31
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v2, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {v2, v0}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void
.end method
