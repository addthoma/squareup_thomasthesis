.class public final Lcom/squareup/ui/main/RealPaymentCompletionMonitor;
.super Ljava/lang/Object;
.source "PaymentCompletionMonitor.kt"

# interfaces
.implements Lcom/squareup/ui/main/PaymentCompletionMonitor;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B7\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0008\u0010\u0011\u001a\u00020\u0012H\u0016J\u0008\u0010\u0013\u001a\u00020\u0014H\u0002J\u0010\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0008\u0010\u0018\u001a\u00020\u0014H\u0016J\u0012\u0010\u0019\u001a\u00020\u00142\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0016J\u0008\u0010\u001c\u001a\u00020\u0014H\u0002J\u0008\u0010\u001d\u001a\u00020\u0014H\u0002J\u0010\u0010\u001e\u001a\u00020\u00142\u0006\u0010\u001f\u001a\u00020\u001bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/ui/main/RealPaymentCompletionMonitor;",
        "Lcom/squareup/ui/main/PaymentCompletionMonitor;",
        "autoVoid",
        "Lcom/squareup/payment/AutoVoid;",
        "badBus",
        "Lcom/squareup/badbus/BadBus;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "pauseAndResumeRegistrar",
        "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
        "paymentIncompleteNotifier",
        "Lcom/squareup/notifications/PaymentIncompleteNotifier;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "(Lcom/squareup/payment/AutoVoid;Lcom/squareup/badbus/BadBus;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/notifications/PaymentIncompleteNotifier;Lcom/squareup/payment/Transaction;)V",
        "showTransactionIncompleteNotification",
        "Ljava/lang/Runnable;",
        "getMortarBundleKey",
        "",
        "maybeHidePaymentIncompleteNotification",
        "",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onPause",
        "onResume",
        "onSave",
        "outState",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final autoVoid:Lcom/squareup/payment/AutoVoid;

.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

.field private final paymentIncompleteNotifier:Lcom/squareup/notifications/PaymentIncompleteNotifier;

.field private final showTransactionIncompleteNotification:Ljava/lang/Runnable;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/AutoVoid;Lcom/squareup/badbus/BadBus;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/notifications/PaymentIncompleteNotifier;Lcom/squareup/payment/Transaction;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "autoVoid"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "badBus"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pauseAndResumeRegistrar"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentIncompleteNotifier"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transaction"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->autoVoid:Lcom/squareup/payment/AutoVoid;

    iput-object p2, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->badBus:Lcom/squareup/badbus/BadBus;

    iput-object p3, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iput-object p4, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    iput-object p5, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->paymentIncompleteNotifier:Lcom/squareup/notifications/PaymentIncompleteNotifier;

    iput-object p6, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->transaction:Lcom/squareup/payment/Transaction;

    .line 38
    new-instance p1, Lcom/squareup/ui/main/RealPaymentCompletionMonitor$showTransactionIncompleteNotification$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/main/RealPaymentCompletionMonitor$showTransactionIncompleteNotification$1;-><init>(Lcom/squareup/ui/main/RealPaymentCompletionMonitor;)V

    check-cast p1, Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->showTransactionIncompleteNotification:Ljava/lang/Runnable;

    return-void
.end method

.method public static final synthetic access$getPaymentIncompleteNotifier$p(Lcom/squareup/ui/main/RealPaymentCompletionMonitor;)Lcom/squareup/notifications/PaymentIncompleteNotifier;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->paymentIncompleteNotifier:Lcom/squareup/notifications/PaymentIncompleteNotifier;

    return-object p0
.end method

.method public static final synthetic access$maybeHidePaymentIncompleteNotification(Lcom/squareup/ui/main/RealPaymentCompletionMonitor;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->maybeHidePaymentIncompleteNotification()V

    return-void
.end method

.method public static final synthetic access$onPause(Lcom/squareup/ui/main/RealPaymentCompletionMonitor;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->onPause()V

    return-void
.end method

.method public static final synthetic access$onResume(Lcom/squareup/ui/main/RealPaymentCompletionMonitor;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->onResume()V

    return-void
.end method

.method private final maybeHidePaymentIncompleteNotification()V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasUncapturedAuth()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->paymentIncompleteNotifier:Lcom/squareup/notifications/PaymentIncompleteNotifier;

    invoke-interface {v0}, Lcom/squareup/notifications/PaymentIncompleteNotifier;->hideNotification()V

    :cond_0
    return-void
.end method

.method private final onPause()V
    .locals 2

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasUncapturedAuth()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->showTransactionIncompleteNotification:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->post(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method private final onResume()V
    .locals 2

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->showTransactionIncompleteNotification:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->paymentIncompleteNotifier:Lcom/squareup/notifications/PaymentIncompleteNotifier;

    invoke-interface {v0}, Lcom/squareup/notifications/PaymentIncompleteNotifier;->hideNotification()V

    return-void
.end method


# virtual methods
.method public getMortarBundleKey()Ljava/lang/String;
    .locals 2

    .line 60
    const-class v0, Lcom/squareup/ui/main/PaymentCompletionMonitor;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PaymentCompletionMonitor::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "badBus.events(OrderEntry\u2026.CartChanged::class.java)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    new-instance v1, Lcom/squareup/ui/main/RealPaymentCompletionMonitor$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/RealPaymentCompletionMonitor$onEnterScope$1;-><init>(Lcom/squareup/ui/main/RealPaymentCompletionMonitor;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    invoke-interface {v0}, Lcom/squareup/pauses/PauseAndResumeRegistrar;->isRunningState()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "pauseAndResumeRegistrar.isRunningState"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/squareup/ui/main/RealPaymentCompletionMonitor$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/RealPaymentCompletionMonitor$onEnterScope$2;-><init>(Lcom/squareup/ui/main/RealPaymentCompletionMonitor;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    if-nez p1, :cond_0

    .line 56
    iget-object p1, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->autoVoid:Lcom/squareup/payment/AutoVoid;

    const-string v0, "New process started"

    invoke-virtual {p1, v0}, Lcom/squareup/payment/AutoVoid;->voidDanglingAuthAfterCrash(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "outState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
