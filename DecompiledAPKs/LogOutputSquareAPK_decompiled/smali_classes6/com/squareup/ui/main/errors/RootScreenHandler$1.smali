.class Lcom/squareup/ui/main/errors/RootScreenHandler$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "RootScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/errors/RootScreenHandler;->openBrowserHelpPage(ILjava/lang/String;Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/errors/RootScreenHandler;

.field final synthetic val$cardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field final synthetic val$url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/errors/RootScreenHandler;Lcom/squareup/cardreader/CardReaderId;Ljava/lang/String;)V
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$1;->this$0:Lcom/squareup/ui/main/errors/RootScreenHandler;

    iput-object p2, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$1;->val$cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iput-object p3, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$1;->val$url:Ljava/lang/String;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$1;->this$0:Lcom/squareup/ui/main/errors/RootScreenHandler;

    iget-object p1, p1, Lcom/squareup/ui/main/errors/RootScreenHandler;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$1;->val$cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object p1

    if-nez p1, :cond_0

    .line 102
    iget-object p1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$1;->this$0:Lcom/squareup/ui/main/errors/RootScreenHandler;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/RootScreenHandler;->goHome()V

    .line 106
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$1;->this$0:Lcom/squareup/ui/main/errors/RootScreenHandler;

    iget-object p1, p1, Lcom/squareup/ui/main/errors/RootScreenHandler;->application:Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$1;->val$url:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/squareup/util/RegisterIntents;->launchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method
