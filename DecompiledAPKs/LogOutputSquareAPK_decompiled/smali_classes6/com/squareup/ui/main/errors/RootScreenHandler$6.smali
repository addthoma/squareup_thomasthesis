.class Lcom/squareup/ui/main/errors/RootScreenHandler$6;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "RootScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/errors/RootScreenHandler;->goToPlayStore(I)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/errors/RootScreenHandler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/errors/RootScreenHandler;)V
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$6;->this$0:Lcom/squareup/ui/main/errors/RootScreenHandler;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 166
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 167
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 168
    sget v1, Lcom/squareup/cardreader/ui/R$string;->play_store_intent_uri:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 169
    invoke-static {v0, p1}, Lcom/squareup/util/Intents;->isIntentAvailable(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
