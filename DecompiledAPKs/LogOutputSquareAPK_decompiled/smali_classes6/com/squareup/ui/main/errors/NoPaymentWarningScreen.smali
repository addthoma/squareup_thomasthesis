.class public abstract Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "NoPaymentWarningScreen.java"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/ModalBodyScreen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$Component;,
        Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$ParentComponent;,
        Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$Module;,
        Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$ComponentFactory;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getHideMaster()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected abstract getInitialViewData(Lcom/squareup/util/Res;)Lcom/squareup/ui/main/errors/WarningScreenData;
.end method

.method public bridge synthetic provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 0

    .line 27
    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;->provideCoordinator(Landroid/view/View;)Lcom/squareup/ui/main/errors/WarningCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/ui/main/errors/WarningCoordinator;
    .locals 2

    .line 35
    const-class v0, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$Component;

    .line 36
    new-instance v0, Lcom/squareup/ui/main/errors/WarningCoordinator;

    invoke-interface {p1}, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$Component;->workflow()Lcom/squareup/ui/main/errors/WarningWorkflow;

    move-result-object v1

    invoke-interface {p1}, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$Component;->mainScheduler()Lrx/Scheduler;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/main/errors/WarningCoordinator;-><init>(Lcom/squareup/ui/main/errors/WarningWorkflow;Lrx/Scheduler;)V

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 31
    sget v0, Lcom/squareup/cardreader/ui/R$layout;->warning_view:I

    return v0
.end method
