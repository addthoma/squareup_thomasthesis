.class public Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;
.super Ljava/lang/Object;
.source "SwipeDipTapWarningScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Module"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;->this$0:Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideInitialViewData(Lcom/squareup/util/Res;)Lcom/squareup/ui/main/errors/WarningScreenData;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;->this$0:Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen;->getInitialScreenData(Lcom/squareup/util/Res;)Lcom/squareup/ui/main/errors/WarningScreenData;

    move-result-object p1

    return-object p1
.end method

.method providePaymentTakingWorkflow(Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/ui/main/errors/WarningScreenData;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/ui/main/SmartPaymentFlowStarter;)Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;
    .locals 7
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 70
    new-instance v6, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;

    move-object v0, v6

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;-><init>(Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/ui/main/errors/WarningScreenData;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/ui/main/SmartPaymentFlowStarter;)V

    return-object v6
.end method
