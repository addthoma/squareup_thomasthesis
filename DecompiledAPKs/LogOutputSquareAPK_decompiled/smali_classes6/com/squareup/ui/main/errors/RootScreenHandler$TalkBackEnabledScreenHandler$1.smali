.class Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "RootScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler;->goToHomeScreenIfTalkbackDisabled(I)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler;)V
    .locals 0

    .line 669
    iput-object p1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler$1;->this$0:Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 671
    iget-object p1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler$1;->this$0:Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler;

    iget-object p1, p1, Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler;->accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 672
    iget-object p1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler$1;->this$0:Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler;->toastMustCancelTalkback()V

    goto :goto_0

    .line 674
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler$1;->this$0:Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler;->goHome()V

    :goto_0
    return-void
.end method
