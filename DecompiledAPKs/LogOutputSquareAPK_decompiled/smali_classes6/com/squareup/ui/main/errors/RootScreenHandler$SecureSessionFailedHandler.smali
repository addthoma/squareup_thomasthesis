.class public Lcom/squareup/ui/main/errors/RootScreenHandler$SecureSessionFailedHandler;
.super Lcom/squareup/ui/main/errors/RootScreenHandler;
.source "RootScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/RootScreenHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SecureSessionFailedHandler"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/AndroidNfcState;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 284
    invoke-direct/range {p0 .. p12}, Lcom/squareup/ui/main/errors/RootScreenHandler;-><init>(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/AndroidNfcState;)V

    return-void
.end method


# virtual methods
.method public from(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;
    .locals 2

    .line 290
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 291
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget v1, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->titleId:I

    if-lez v1, :cond_0

    iget v1, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->titleId:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/cardreader/ui/R$string;->emv_securesession_failed_title:I

    .line 292
    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget v1, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->messageId:I

    if-lez v1, :cond_1

    iget v1, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->messageId:I

    goto :goto_1

    :cond_1
    sget v1, Lcom/squareup/cardreader/ui/R$string;->emv_securesession_failed_msg:I

    .line 294
    :goto_1
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/common/strings/R$string;->ok:I

    .line 296
    invoke-virtual {p0, v1}, Lcom/squareup/ui/main/errors/RootScreenHandler$SecureSessionFailedHandler;->goHomeButton(I)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->defaultButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/cardreader/ui/R$string;->try_again:I

    iget-object p1, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    .line 297
    invoke-virtual {p0, v1, p1}, Lcom/squareup/ui/main/errors/RootScreenHandler$SecureSessionFailedHandler;->goHomeAndRestartSecureSessionButton(ILcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->secondButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 299
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    return-object p1
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    .line 303
    invoke-virtual {p0}, Lcom/squareup/ui/main/errors/RootScreenHandler$SecureSessionFailedHandler;->goHome()V

    return-void
.end method
