.class public final Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;
.super Ljava/lang/Object;
.source "ButtonFlowStarter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/errors/ButtonFlowStarter;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiReaderSettingsControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;"
        }
    .end annotation
.end field

.field private final apiTransactionStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;"
        }
    .end annotation
.end field

.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessorInterface;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingDiverterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentProcessingEventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final posAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final readerMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ReaderStatusMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessorInterface;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ReaderStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;)V"
        }
    .end annotation

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->apiReaderSettingsControllerProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p2, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p3, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p4, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p5, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->posAppletProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p6, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p7, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->readerMonitorProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p8, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p9, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p10, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p11, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p12, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->onboardingDiverterProvider:Ljavax/inject/Provider;

    .line 82
    iput-object p13, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->paymentProcessingEventSinkProvider:Ljavax/inject/Provider;

    .line 83
    iput-object p14, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessorInterface;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ReaderStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;)",
            "Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;"
        }
    .end annotation

    .line 103
    new-instance v15, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v15
.end method

.method public static newInstance(Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/ui/NfcProcessorInterface;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lflow/Flow;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Landroid/app/Application;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;Lcom/squareup/util/BrowserLauncher;)Lcom/squareup/ui/main/errors/ButtonFlowStarter;
    .locals 16

    .line 114
    new-instance v15, Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/ui/main/errors/ButtonFlowStarter;-><init>(Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/ui/NfcProcessorInterface;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lflow/Flow;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Landroid/app/Application;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;Lcom/squareup/util/BrowserLauncher;)V

    return-object v15
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/errors/ButtonFlowStarter;
    .locals 15

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->apiReaderSettingsControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/api/ApiReaderSettingsController;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/api/ApiTransactionState;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/NfcProcessorInterface;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->posAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->readerMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/main/ReaderStatusMonitor;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/tender/TenderStarter;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->onboardingDiverterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/onboarding/OnboardingDiverter;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->paymentProcessingEventSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Lcom/squareup/util/BrowserLauncher;

    invoke-static/range {v1 .. v14}, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->newInstance(Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/ui/NfcProcessorInterface;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lflow/Flow;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Landroid/app/Application;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;Lcom/squareup/util/BrowserLauncher;)Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/ui/main/errors/ButtonFlowStarter_Factory;->get()Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    move-result-object v0

    return-object v0
.end method
