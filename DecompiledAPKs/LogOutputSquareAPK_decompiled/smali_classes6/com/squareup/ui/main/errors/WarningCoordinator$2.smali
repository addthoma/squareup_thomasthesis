.class Lcom/squareup/ui/main/errors/WarningCoordinator$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "WarningCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/errors/WarningCoordinator;->updateTopAlternativeButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/errors/WarningCoordinator;

.field final synthetic val$buttonConfig:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/errors/WarningCoordinator;Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;)V
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/squareup/ui/main/errors/WarningCoordinator$2;->this$0:Lcom/squareup/ui/main/errors/WarningCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/main/errors/WarningCoordinator$2;->val$buttonConfig:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 126
    iget-object p1, p0, Lcom/squareup/ui/main/errors/WarningCoordinator$2;->this$0:Lcom/squareup/ui/main/errors/WarningCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/main/errors/WarningCoordinator;->access$000(Lcom/squareup/ui/main/errors/WarningCoordinator;)Lcom/squareup/ui/main/errors/WarningWorkflow;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator$2;->val$buttonConfig:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    iget-object v0, v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->buttonBehaviorType:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningWorkflow;->onTopAlternativeButtonPressed(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V

    return-void
.end method
