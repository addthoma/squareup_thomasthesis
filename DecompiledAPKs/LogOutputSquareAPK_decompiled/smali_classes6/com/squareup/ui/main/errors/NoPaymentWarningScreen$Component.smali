.class public interface abstract Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$Component;
.super Ljava/lang/Object;
.source "NoPaymentWarningScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract mainScheduler()Lrx/Scheduler;
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation
.end method

.method public abstract workflow()Lcom/squareup/ui/main/errors/WarningWorkflow;
.end method
