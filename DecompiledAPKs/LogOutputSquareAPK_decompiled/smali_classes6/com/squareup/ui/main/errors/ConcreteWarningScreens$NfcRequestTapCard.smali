.class public final Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcRequestTapCard;
.super Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;
.source "ConcreteWarningScreens.java"


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/ConcreteWarningScreens;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NfcRequestTapCard"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcRequestTapCard;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcRequestTapCard;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 272
    new-instance v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcRequestTapCard;

    invoke-direct {v0}, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcRequestTapCard;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcRequestTapCard;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcRequestTapCard;

    .line 278
    sget-object v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcRequestTapCard;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcRequestTapCard;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcRequestTapCard;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 275
    invoke-direct {p0}, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;-><init>()V

    return-void
.end method


# virtual methods
.method protected getInitialViewData(Lcom/squareup/util/Res;)Lcom/squareup/ui/main/errors/WarningScreenData;
    .locals 2

    .line 281
    new-instance p1, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    invoke-direct {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;-><init>()V

    sget-object v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->KILL_TENDER_INITIATED_BY_READER:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    sget v1, Lcom/squareup/common/strings/R$string;->ok:I

    .line 282
    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->defaultButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 283
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->emv_request_tap_payment_flow_card_message:I

    .line 284
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->messageId(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->emv_request_tap_payment_flow_card_title:I

    .line 285
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->titleId(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    .line 286
    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->build()Lcom/squareup/ui/main/errors/WarningScreenData;

    move-result-object p1

    return-object p1
.end method
