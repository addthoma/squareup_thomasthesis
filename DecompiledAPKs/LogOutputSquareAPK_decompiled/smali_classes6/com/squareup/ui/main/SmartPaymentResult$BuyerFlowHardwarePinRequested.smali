.class public final Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowHardwarePinRequested;
.super Ljava/lang/Object;
.source "SmartPaymentResult.java"

# interfaces
.implements Lcom/squareup/ui/main/SmartPaymentResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/SmartPaymentResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BuyerFlowHardwarePinRequested"
.end annotation


# instance fields
.field public final cardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field public final secureTouchPinRequestData:Lcom/squareup/securetouch/SecureTouchPinRequestData;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
    .locals 0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowHardwarePinRequested;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    .line 60
    iput-object p2, p0, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowHardwarePinRequested;->secureTouchPinRequestData:Lcom/squareup/securetouch/SecureTouchPinRequestData;

    return-void
.end method


# virtual methods
.method public dispatch(Lcom/squareup/ui/main/SmartPaymentResult$SmartPaymentResultHandler;)V
    .locals 0

    .line 64
    invoke-interface {p1, p0}, Lcom/squareup/ui/main/SmartPaymentResult$SmartPaymentResultHandler;->onResult(Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowHardwarePinRequested;)V

    return-void
.end method
