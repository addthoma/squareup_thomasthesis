.class public abstract Lcom/squareup/ui/main/ReaderTutorialLauncher;
.super Ljava/lang/Object;
.source "ReaderTutorialLauncher.java"

# interfaces
.implements Lcom/squareup/ui/main/ForceableContentLauncher;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/ui/main/ForceableContentLauncher<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final contentViewed:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/squareup/ui/main/ReaderTutorialLauncher;->contentViewed:Lcom/squareup/settings/LocalSetting;

    return-void
.end method


# virtual methods
.method public attemptToShowContent(Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/main/ReaderTutorialLauncher;->contentViewed:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/main/ReaderTutorialLauncher;->canShowTutorial()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/ReaderTutorialLauncher;->contentViewed:Lcom/squareup/settings/LocalSetting;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 18
    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/ReaderTutorialLauncher;->showContent(Ljava/lang/Object;)V

    return v1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method protected abstract canShowTutorial()Z
.end method
