.class public interface abstract Lcom/squareup/ui/main/MainActivityScope$View;
.super Ljava/lang/Object;
.source "MainActivityScope.java"

# interfaces
.implements Lcom/squareup/container/ContainerView;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/MainActivityScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "View"
.end annotation


# virtual methods
.method public abstract disableLayoutTransitions()V
.end method

.method public abstract enableLayoutTransitions()V
.end method

.method public abstract finishEnterFullScreen()V
.end method

.method public abstract startEnterFullScreen()V
.end method
