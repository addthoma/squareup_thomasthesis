.class public abstract Lcom/squareup/ui/main/NoReaderTutorialModule;
.super Ljava/lang/Object;
.source "NoReaderTutorialModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/NoReaderTutorialModule$NoOpContentLauncher;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideR12TutorialLauncher()Lcom/squareup/ui/main/R12ForceableContentLauncher;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/ui/main/NoReaderTutorialModule$2NoOp;

    invoke-direct {v0}, Lcom/squareup/ui/main/NoReaderTutorialModule$2NoOp;-><init>()V

    return-object v0
.end method

.method static provideR6FirstTimeVideoLauncher()Lcom/squareup/ui/main/R6ForceableContentLauncher;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 24
    new-instance v0, Lcom/squareup/ui/main/NoReaderTutorialModule$1NoOp;

    invoke-direct {v0}, Lcom/squareup/ui/main/NoReaderTutorialModule$1NoOp;-><init>()V

    return-object v0
.end method
