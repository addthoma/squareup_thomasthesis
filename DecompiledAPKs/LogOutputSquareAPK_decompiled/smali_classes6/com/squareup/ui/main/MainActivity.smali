.class public Lcom/squareup/ui/main/MainActivity;
.super Lcom/squareup/ui/SquareActivity;
.source "MainActivity.java"

# interfaces
.implements Lcom/squareup/pauses/PauseAndResumeActivity;
.implements Lcom/squareup/camerahelper/CameraHelper$View;
.implements Lcom/squareup/container/ContainerActivity;
.implements Lcom/squareup/visibilitypresenter/VisibleActivity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/MainActivity$Component;
    }
.end annotation


# instance fields
.field activityVisibilityPresenter:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field additionalActivityDelegates:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/ui/ActivityDelegate;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field apiRequestController:Lcom/squareup/api/ApiRequestController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field badKeyboardHider:Lcom/squareup/ui/main/BadKeyboardHider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field browserLauncher:Lcom/squareup/util/BrowserLauncher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field cameraHelper:Lcom/squareup/camerahelper/CameraHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field containerActivityDelegate:Lcom/squareup/container/ContainerActivityDelegate;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field deepLinkHelper:Lcom/squareup/analytics/DeepLinkHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field invoiceShareUrlLauncher:Lcom/squareup/url/InvoiceShareUrlLauncher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field locationPresenter:Lcom/squareup/ui/main/LocationPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field mainContainer:Lcom/squareup/ui/main/PosContainer;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field nfcState:Lcom/squareup/ui/AndroidNfcState;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field pauseNarcPresenter:Lcom/squareup/pauses/PauseAndResumePresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private rootView:Landroid/view/View;

.field rootViewSetup:Lcom/squareup/rootview/RootViewSetup;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field secureScopeManager:Lcom/squareup/secure/SecureScopeManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field statusBarHelper:Lcom/squareup/ui/StatusBarHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field toastFactory:Lcom/squareup/util/ToastFactory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 113
    sget-object v0, Lcom/squareup/ui/SquareActivity$Preconditions;->LOGGED_IN:Lcom/squareup/ui/SquareActivity$Preconditions;

    invoke-direct {p0, v0}, Lcom/squareup/ui/SquareActivity;-><init>(Lcom/squareup/ui/SquareActivity$Preconditions;)V

    return-void
.end method

.method public static createDeepLinkIntent(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 1

    .line 61
    invoke-static {p0}, Lcom/squareup/ui/main/MainActivity;->createMainActivityIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object p0

    const-string v0, "android.intent.action.VIEW"

    .line 62
    invoke-virtual {p0, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 63
    invoke-virtual {p0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    return-object p0
.end method

.method public static createMainActivityIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .line 72
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/squareup/ui/main/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 p0, 0x14000000

    .line 73
    invoke-virtual {v0, p0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method protected buildActivityScope(Ljava/lang/Object;Lmortar/MortarScope$Builder;)Lmortar/MortarScope$Builder;
    .locals 1

    .line 126
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/SquareActivity;->buildActivityScope(Ljava/lang/Object;Lmortar/MortarScope$Builder;)Lmortar/MortarScope$Builder;

    move-result-object p2

    .line 142
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/main/CheckoutWorkflowRunner$ParentComponent;

    .line 143
    invoke-interface {v0}, Lcom/squareup/ui/main/CheckoutWorkflowRunner$ParentComponent;->checkoutWorkflowRunner()Lcom/squareup/ui/main/CheckoutWorkflowRunner;

    move-result-object v0

    .line 144
    invoke-interface {v0, p2}, Lcom/squareup/ui/main/CheckoutWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    .line 146
    move-object v0, p1

    check-cast v0, Lcom/squareup/onboarding/OnboardingWorkflowRunner$ParentComponent;

    .line 147
    invoke-interface {v0}, Lcom/squareup/onboarding/OnboardingWorkflowRunner$ParentComponent;->onboardingWorkflowRunner()Lcom/squareup/onboarding/OnboardingWorkflowRunner;

    move-result-object v0

    .line 148
    invoke-interface {v0, p2}, Lcom/squareup/onboarding/OnboardingWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    .line 150
    check-cast p1, Lcom/squareup/x2/X2MonitorWorkflowRunner$ParentComponent;

    .line 151
    invoke-interface {p1}, Lcom/squareup/x2/X2MonitorWorkflowRunner$ParentComponent;->x2MonitorWorkflowRunner()Lcom/squareup/x2/X2MonitorWorkflowRunner;

    move-result-object p1

    .line 152
    invoke-interface {p1, p2}, Lcom/squareup/x2/X2MonitorWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    return-object p2
.end method

.method protected doBackPressed()V
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->onBackPressed()V

    return-void
.end method

.method protected doCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 158
    invoke-super {p0, p1}, Lcom/squareup/ui/SquareActivity;->doCreate(Landroid/os/Bundle;)V

    .line 160
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    invoke-interface {p1, p0}, Lcom/squareup/camerahelper/CameraHelper;->takeView(Lcom/squareup/camerahelper/CameraHelper$View;)V

    .line 161
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->locationPresenter:Lcom/squareup/ui/main/LocationPresenter;

    invoke-virtual {p1, p0}, Lcom/squareup/ui/main/LocationPresenter;->takeView(Ljava/lang/Object;)V

    .line 162
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->containerActivityDelegate:Lcom/squareup/container/ContainerActivityDelegate;

    invoke-virtual {p1, p0}, Lcom/squareup/container/ContainerActivityDelegate;->takeActivity(Lcom/squareup/container/ContainerActivity;)V

    .line 163
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->pauseNarcPresenter:Lcom/squareup/pauses/PauseAndResumePresenter;

    invoke-virtual {p1, p0}, Lcom/squareup/pauses/PauseAndResumePresenter;->takeView(Ljava/lang/Object;)V

    .line 164
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    invoke-virtual {p1, p0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->takeView(Ljava/lang/Object;)V

    .line 165
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->activityVisibilityPresenter:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;

    invoke-virtual {p1, p0}, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->takeView(Ljava/lang/Object;)V

    .line 166
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    invoke-virtual {p1, p0}, Lcom/squareup/ui/SoftInputPresenter;->takeView(Ljava/lang/Object;)V

    .line 167
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->statusBarHelper:Lcom/squareup/ui/StatusBarHelper;

    invoke-virtual {p1, p0}, Lcom/squareup/ui/StatusBarHelper;->takeView(Ljava/lang/Object;)V

    .line 168
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {p1, p0}, Lcom/squareup/api/ApiRequestController;->onActivityCreated(Landroid/app/Activity;)V

    .line 169
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->deepLinkHelper:Lcom/squareup/analytics/DeepLinkHelper;

    invoke-interface {p1, p0}, Lcom/squareup/analytics/DeepLinkHelper;->onActivityCreate(Landroid/app/Activity;)V

    .line 170
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    invoke-interface {p1, p0}, Lcom/squareup/util/BrowserLauncher;->takeActivity(Landroid/app/Activity;)V

    .line 171
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->invoiceShareUrlLauncher:Lcom/squareup/url/InvoiceShareUrlLauncher;

    invoke-interface {p1, p0}, Lcom/squareup/url/InvoiceShareUrlLauncher;->takeActivity(Landroid/app/Activity;)V

    .line 172
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->nfcState:Lcom/squareup/ui/AndroidNfcState;

    invoke-virtual {p1, p0}, Lcom/squareup/ui/AndroidNfcState;->takeView(Ljava/lang/Object;)V

    .line 173
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->secureScopeManager:Lcom/squareup/secure/SecureScopeManager;

    invoke-virtual {p1, p0}, Lcom/squareup/secure/SecureScopeManager;->takeView(Ljava/lang/Object;)V

    .line 175
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->additionalActivityDelegates:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ActivityDelegate;

    .line 176
    invoke-interface {v0, p0}, Lcom/squareup/ui/ActivityDelegate;->onCreate(Landroid/app/Activity;)V

    goto :goto_0

    .line 181
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/ui/main/PosContainer;->onActivityCreate(Landroid/content/Intent;)V

    .line 182
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->containerActivityDelegate:Lcom/squareup/container/ContainerActivityDelegate;

    sget-object v0, Lcom/squareup/ui/main/MainActivityScope;->INSTANCE:Lcom/squareup/ui/main/MainActivityScope;

    iget-object v1, p0, Lcom/squareup/ui/main/MainActivity;->rootViewSetup:Lcom/squareup/rootview/RootViewSetup;

    .line 183
    invoke-interface {v1}, Lcom/squareup/rootview/RootViewSetup;->getRootViewLayout()I

    move-result v1

    .line 182
    invoke-virtual {p1, v0, p0, v1}, Lcom/squareup/container/ContainerActivityDelegate;->inflateRootScreen(Lflow/path/Path;Landroid/content/Context;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/main/MainActivity;->rootView:Landroid/view/View;

    .line 184
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->badKeyboardHider:Lcom/squareup/ui/main/BadKeyboardHider;

    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->rootView:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/BadKeyboardHider;->takeView(Landroid/view/View;)V

    .line 185
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivity;->rootView:Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/MainActivity;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public finish()V
    .locals 1

    .line 282
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    if-eqz v0, :cond_0

    .line 283
    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->onActivityFinish()V

    .line 285
    :cond_0
    invoke-super {p0}, Lcom/squareup/ui/SquareActivity;->finish()V

    return-void
.end method

.method public getBundleService()Lmortar/bundler/BundleService;
    .locals 1

    .line 121
    invoke-static {p0}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object v0

    return-object v0
.end method

.method protected inject(Lmortar/MortarScope;)V
    .locals 1

    .line 117
    const-class v0, Lcom/squareup/ui/main/MainActivity$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/MainActivity$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/main/MainActivity$Component;->inject(Lcom/squareup/ui/main/MainActivity;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .line 216
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    if-eqz v0, :cond_0

    .line 218
    invoke-interface {v0, p0}, Lcom/squareup/camerahelper/CameraHelper;->dropView(Lcom/squareup/camerahelper/CameraHelper$View;)V

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->locationPresenter:Lcom/squareup/ui/main/LocationPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/main/LocationPresenter;->dropView(Ljava/lang/Object;)V

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->containerActivityDelegate:Lcom/squareup/container/ContainerActivityDelegate;

    invoke-virtual {v0, p0}, Lcom/squareup/container/ContainerActivityDelegate;->dropActivity(Lcom/squareup/container/ContainerActivity;)V

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->pauseNarcPresenter:Lcom/squareup/pauses/PauseAndResumePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/pauses/PauseAndResumePresenter;->dropView(Ljava/lang/Object;)V

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->dropView(Ljava/lang/Object;)V

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->activityVisibilityPresenter:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->dropView(Ljava/lang/Object;)V

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->softInputPresenter:Lcom/squareup/ui/SoftInputPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/SoftInputPresenter;->dropView(Ljava/lang/Object;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->statusBarHelper:Lcom/squareup/ui/StatusBarHelper;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/StatusBarHelper;->dropView(Ljava/lang/Object;)V

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0, p0}, Lcom/squareup/api/ApiRequestController;->onActivityDestroyed(Landroid/app/Activity;)V

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    invoke-interface {v0, p0}, Lcom/squareup/util/BrowserLauncher;->dropActivity(Landroid/app/Activity;)V

    .line 228
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->invoiceShareUrlLauncher:Lcom/squareup/url/InvoiceShareUrlLauncher;

    invoke-interface {v0, p0}, Lcom/squareup/url/InvoiceShareUrlLauncher;->dropActivity(Landroid/app/Activity;)V

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->nfcState:Lcom/squareup/ui/AndroidNfcState;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/AndroidNfcState;->dropView(Ljava/lang/Object;)V

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->secureScopeManager:Lcom/squareup/secure/SecureScopeManager;

    invoke-virtual {v0, p0}, Lcom/squareup/secure/SecureScopeManager;->dropView(Ljava/lang/Object;)V

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->badKeyboardHider:Lcom/squareup/ui/main/BadKeyboardHider;

    iget-object v1, p0, Lcom/squareup/ui/main/MainActivity;->rootView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/BadKeyboardHider;->dropView(Landroid/view/View;)V

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->additionalActivityDelegates:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/ActivityDelegate;

    .line 234
    invoke-interface {v1, p0}, Lcom/squareup/ui/ActivityDelegate;->onDestroy(Landroid/app/Activity;)V

    goto :goto_0

    .line 237
    :cond_0
    invoke-super {p0}, Lcom/squareup/ui/SquareActivity;->onDestroy()V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v0, p1}, Lcom/squareup/ui/main/PosContainer;->onActivityNewIntent(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 200
    invoke-super {p0}, Lcom/squareup/ui/SquareActivity;->onPause()V

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->pauseNarcPresenter:Lcom/squareup/pauses/PauseAndResumePresenter;

    invoke-virtual {v0}, Lcom/squareup/pauses/PauseAndResumePresenter;->activityPaused()V

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    invoke-virtual {v0}, Lcom/squareup/usb/UsbDiscoverer;->stopDelayed()V

    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 1

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 193
    invoke-super {p0}, Lcom/squareup/ui/SquareActivity;->onResume()V

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->pauseNarcPresenter:Lcom/squareup/pauses/PauseAndResumePresenter;

    invoke-virtual {v0}, Lcom/squareup/pauses/PauseAndResumePresenter;->activityResumed()V

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->offlineModeMonitor:Lcom/squareup/payment/OfflineModeMonitor;

    invoke-interface {v0}, Lcom/squareup/payment/OfflineModeMonitor;->activityResumed()V

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->usbDiscoverer:Lcom/squareup/usb/UsbDiscoverer;

    invoke-virtual {v0}, Lcom/squareup/usb/UsbDiscoverer;->start()V

    return-void
.end method

.method protected onStart()V
    .locals 1

    .line 254
    invoke-super {p0}, Lcom/squareup/ui/SquareActivity;->onStart()V

    .line 255
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->activityVisibilityPresenter:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;

    invoke-virtual {v0}, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->activityStart()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    .line 259
    invoke-super {p0}, Lcom/squareup/ui/SquareActivity;->onStop()V

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->activityVisibilityPresenter:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;

    invoke-virtual {v0}, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->activityStop()V

    return-void
.end method

.method public onUserInteraction()V
    .locals 1

    .line 264
    invoke-super {p0}, Lcom/squareup/ui/SquareActivity;->onUserInteraction()V

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->onUserInteraction()V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .line 211
    invoke-super {p0, p1}, Lcom/squareup/ui/SquareActivity;->onWindowFocusChanged(Z)V

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->locationPresenter:Lcom/squareup/ui/main/LocationPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/LocationPresenter;->check(Z)V

    return-void
.end method

.method protected requiresLocation()Z
    .locals 2

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENFORCE_LOCATION_FIX:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ELIGIBLE_FOR_SQUARE_CARD_PROCESSING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public toast(I)V
    .locals 2

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivity;->toastFactory:Lcom/squareup/util/ToastFactory;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/squareup/util/ToastFactory;->showText(II)V

    return-void
.end method
