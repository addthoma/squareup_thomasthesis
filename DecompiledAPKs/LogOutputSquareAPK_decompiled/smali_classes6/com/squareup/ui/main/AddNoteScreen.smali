.class public final Lcom/squareup/ui/main/AddNoteScreen;
.super Lcom/squareup/ui/seller/InSellerScope;
.source "AddNoteScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/main/AddNoteScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/AddNoteScreen$Component;,
        Lcom/squareup/ui/main/AddNoteScreen$Module;,
        Lcom/squareup/ui/main/AddNoteScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/main/AddNoteScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/main/AddNoteScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/ui/main/AddNoteScreen;

    invoke-direct {v0}, Lcom/squareup/ui/main/AddNoteScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/AddNoteScreen;->INSTANCE:Lcom/squareup/ui/main/AddNoteScreen;

    .line 120
    sget-object v0, Lcom/squareup/ui/main/AddNoteScreen;->INSTANCE:Lcom/squareup/ui/main/AddNoteScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/AddNoteScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/squareup/ui/seller/InSellerScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 123
    sget v0, Lcom/squareup/orderentry/R$layout;->add_note_view:I

    return v0
.end method
