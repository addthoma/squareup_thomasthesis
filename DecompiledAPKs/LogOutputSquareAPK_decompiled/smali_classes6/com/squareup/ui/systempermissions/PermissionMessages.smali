.class public Lcom/squareup/ui/systempermissions/PermissionMessages;
.super Ljava/lang/Object;
.source "PermissionMessages.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buttonText(Lcom/squareup/systempermissions/SystemPermission;)I
    .locals 3

    .line 46
    sget-object v0, Lcom/squareup/ui/systempermissions/PermissionMessages$1;->$SwitchMap$com$squareup$systempermissions$SystemPermission:[I

    invoke-virtual {p0}, Lcom/squareup/systempermissions/SystemPermission;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 56
    sget p0, Lcom/squareup/ui/systempermissions/R$string;->system_permission_storage_button:I

    return p0

    .line 58
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown permission: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/systempermissions/SystemPermission;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_1
    sget p0, Lcom/squareup/ui/systempermissions/R$string;->system_permission_phone_button:I

    return p0

    .line 52
    :cond_2
    sget p0, Lcom/squareup/ui/systempermissions/R$string;->system_permission_mic_button:I

    return p0

    .line 50
    :cond_3
    sget p0, Lcom/squareup/ui/systempermissions/R$string;->system_permission_location_button:I

    return p0

    .line 48
    :cond_4
    sget p0, Lcom/squareup/ui/systempermissions/R$string;->system_permission_contacts_button:I

    return p0
.end method

.method public static getExplanationBody(Lcom/squareup/systempermissions/SystemPermission;)I
    .locals 3

    .line 29
    sget-object v0, Lcom/squareup/ui/systempermissions/PermissionMessages$1;->$SwitchMap$com$squareup$systempermissions$SystemPermission:[I

    invoke-virtual {p0}, Lcom/squareup/systempermissions/SystemPermission;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 39
    sget p0, Lcom/squareup/ui/systempermissions/R$string;->system_permission_storage_body:I

    return p0

    .line 41
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown permission: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/systempermissions/SystemPermission;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_1
    sget p0, Lcom/squareup/ui/systempermissions/R$string;->system_permission_phone_body:I

    return p0

    .line 35
    :cond_2
    sget p0, Lcom/squareup/ui/systempermissions/R$string;->system_permission_mic_body:I

    return p0

    .line 33
    :cond_3
    sget p0, Lcom/squareup/ui/systempermissions/R$string;->system_permission_location_body:I

    return p0

    .line 31
    :cond_4
    sget p0, Lcom/squareup/ui/systempermissions/R$string;->system_permission_contacts_body:I

    return p0
.end method

.method public static getExplanationTitle(Lcom/squareup/systempermissions/SystemPermission;)I
    .locals 3

    .line 12
    sget-object v0, Lcom/squareup/ui/systempermissions/PermissionMessages$1;->$SwitchMap$com$squareup$systempermissions$SystemPermission:[I

    invoke-virtual {p0}, Lcom/squareup/systempermissions/SystemPermission;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 22
    sget p0, Lcom/squareup/ui/systempermissions/R$string;->system_permission_storage_title:I

    return p0

    .line 24
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown permission: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/systempermissions/SystemPermission;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_1
    sget p0, Lcom/squareup/ui/systempermissions/R$string;->system_permission_phone_title:I

    return p0

    .line 18
    :cond_2
    sget p0, Lcom/squareup/ui/systempermissions/R$string;->system_permission_mic_title:I

    return p0

    .line 16
    :cond_3
    sget p0, Lcom/squareup/ui/systempermissions/R$string;->system_permission_location_title:I

    return p0

    .line 14
    :cond_4
    sget p0, Lcom/squareup/ui/systempermissions/R$string;->system_permission_contacts_title:I

    return p0
.end method
