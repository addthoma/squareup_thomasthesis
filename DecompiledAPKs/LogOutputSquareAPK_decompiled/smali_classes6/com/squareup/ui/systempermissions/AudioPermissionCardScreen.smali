.class public Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "AudioPermissionCardScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Component;,
        Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$ParentComponent;,
        Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;",
            ">;"
        }
    .end annotation
.end field

.field private static final DISPLAY_TITLE_AND_ARROW:Z = true

.field private static final NO_TITLE_AND_X:Z


# instance fields
.field private final showTitleTextAndArrow:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 178
    sget-object v0, Lcom/squareup/ui/systempermissions/-$$Lambda$AudioPermissionCardScreen$nrjk2ZbCnUhJ-LijgeCduqrIwVg;->INSTANCE:Lcom/squareup/ui/systempermissions/-$$Lambda$AudioPermissionCardScreen$nrjk2ZbCnUhJ-LijgeCduqrIwVg;

    .line 179
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    .line 56
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    .line 57
    iput-boolean p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;->showTitleTextAndArrow:Z

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;)Z
    .locals 0

    .line 41
    iget-boolean p0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;->showTitleTextAndArrow:Z

    return p0
.end method

.method public static createAudioPermissionNoTitleText()Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;
    .locals 2

    .line 53
    new-instance v0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;-><init>(Z)V

    return-object v0
.end method

.method public static createAudioPermissionWithTitleText()Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;
    .locals 2

    .line 49
    new-instance v0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;-><init>(Z)V

    return-object v0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;
    .locals 1

    .line 180
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 181
    :goto_0
    new-instance p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;

    invoke-direct {p0, v0}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;-><init>(Z)V

    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 174
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/InMainActivityScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 175
    iget-boolean p2, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;->showTitleTextAndArrow:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 185
    sget v0, Lcom/squareup/cardreader/ui/R$layout;->audio_permission_card_view:I

    return v0
.end method
