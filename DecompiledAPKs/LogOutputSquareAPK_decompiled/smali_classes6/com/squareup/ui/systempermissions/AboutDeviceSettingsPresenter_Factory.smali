.class public final Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter_Factory;
.super Ljava/lang/Object;
.source "AboutDeviceSettingsPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinCardActionBar;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinCardActionBar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter_Factory;->actionBarProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinCardActionBar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Device;Lcom/squareup/marin/widgets/MarinCardActionBar;Lflow/Flow;)Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;-><init>(Lcom/squareup/util/Device;Lcom/squareup/marin/widgets/MarinCardActionBar;Lflow/Flow;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    iget-object v1, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter_Factory;->actionBarProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/marin/widgets/MarinCardActionBar;

    iget-object v2, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflow/Flow;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter_Factory;->newInstance(Lcom/squareup/util/Device;Lcom/squareup/marin/widgets/MarinCardActionBar;Lflow/Flow;)Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter_Factory;->get()Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;

    move-result-object v0

    return-object v0
.end method
