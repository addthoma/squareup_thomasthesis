.class public Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;
.super Landroid/widget/LinearLayout;
.source "AboutDeviceSettingsView.java"


# instance fields
.field private aboutLocation:Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

.field private aboutPhone:Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

.field private aboutStorage:Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

.field appNameFormatter:Lcom/squareup/util/AppNameFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const-class p2, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsScreen$Component;->inject(Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 65
    sget v0, Lcom/squareup/common/bootstrap/R$id;->about_device_settings_location:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->aboutLocation:Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

    .line 66
    sget v0, Lcom/squareup/common/bootstrap/R$id;->about_device_settings_storage:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->aboutStorage:Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

    .line 67
    sget v0, Lcom/squareup/common/bootstrap/R$id;->about_device_settings_phone:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->aboutPhone:Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->presenter:Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;->dropView(Ljava/lang/Object;)V

    .line 61
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 30
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 31
    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->bindViews()V

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->aboutLocation:Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOCATION_CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->aboutLocation:Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

    sget v1, Lcom/squareup/common/bootstrap/R$string;->about_device_settings_location_services:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->setTitle(I)V

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->aboutLocation:Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

    iget-object v1, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v2, Lcom/squareup/ui/systempermissions/R$string;->system_permission_location_body:I

    .line 36
    invoke-interface {v1, v2}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 35
    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->aboutStorage:Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STORAGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->aboutStorage:Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

    sget v1, Lcom/squareup/common/bootstrap/R$string;->about_device_settings_device_storage:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->setTitle(I)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->aboutStorage:Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

    iget-object v1, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v2, Lcom/squareup/ui/systempermissions/R$string;->system_permission_storage_body:I

    .line 42
    invoke-interface {v1, v2}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 41
    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->aboutPhone:Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PHONE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->aboutPhone:Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

    sget v1, Lcom/squareup/common/bootstrap/R$string;->about_device_settings_phone_access:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->setTitle(I)V

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->aboutPhone:Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

    iget-object v1, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v2, Lcom/squareup/ui/systempermissions/R$string;->system_permission_phone_body:I

    .line 48
    invoke-interface {v1, v2}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 47
    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->presenter:Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;->isPortrait()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->presenter:Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->aboutLocation:Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->setAllGravities(I)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->aboutStorage:Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->setAllGravities(I)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->aboutPhone:Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->setAllGravities(I)V

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;->presenter:Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingsPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
