.class final Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EnrollGoogleAuthQrCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEnrollGoogleAuthQrCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EnrollGoogleAuthQrCoordinator.kt\ncom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,133:1\n1103#2,7:134\n*E\n*S KotlinDebug\n*F\n+ 1 EnrollGoogleAuthQrCoordinator.kt\ncom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1\n*L\n64#1,7:134\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;->$view:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component2()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 48
    iget-object v1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->access$getActionBar$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    .line 49
    iget-object v2, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "view.context"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v4, "actionBarPresenter"

    .line 55
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    new-instance v4, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 51
    sget v5, Lcom/squareup/common/strings/R$string;->next:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v4

    .line 52
    new-instance v5, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1$1;

    invoke-direct {v5, p1}, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v5, Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v4

    .line 53
    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v6, Lcom/squareup/common/authenticatorviews/R$string;->back:I

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 54
    new-instance v4, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1$2;

    invoke-direct {v4, p1}, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v4, Ljava/lang/Runnable;

    invoke-virtual {v2, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 55
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 57
    iget-object v1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->access$getTitle$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;)Lcom/squareup/widgets/MessageView;

    move-result-object v1

    sget v2, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_enroll_title:I

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 58
    iget-object v1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->access$getSubtitle$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;)Lcom/squareup/widgets/MessageView;

    move-result-object v1

    sget v2, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_enroll_google_auth_qr_subtitle:I

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 64
    iget-object v1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->access$getCantScanBarcode$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;)Landroid/widget/TextView;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 134
    new-instance v2, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1$$special$$inlined$onClickDebounced$1;

    invoke-direct {v2, p1}, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1$$special$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    iget-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->access$getCantScanBarcode$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;)Landroid/widget/TextView;

    move-result-object p1

    .line 66
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_google_auth_cant_scan_barcode:I

    .line 65
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(I)V

    .line 69
    new-instance p1, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 70
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_enroll_google_auth_learn_more:I

    const-string v2, "learn_more"

    invoke-virtual {p1, v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 71
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_auth_url:I

    invoke-virtual {p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 72
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_enroll_google_auth_learn_more_text:I

    invoke-virtual {p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 73
    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->linkColor(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 74
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    .line 76
    iget-object v1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->access$getLearnMore$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;)Lcom/squareup/widgets/MessageView;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;->getTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->access$setGoogleAuthTwoFactorDetails$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;Lcom/squareup/protos/multipass/common/TwoFactorDetails;)V

    .line 81
    iget-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;

    iget-object v0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->access$getGoogleAuthTwoFactorDetails$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;)Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    iget-object v1, v1, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->authenticator_key_uri:Ljava/lang/String;

    const-string v2, "googleAuthTwoFactorDetai\u2026uth.authenticator_key_uri"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->access$encodeAsBitmap(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;->access$getQrCode$p(Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method
