.class public final Lcom/squareup/ui/login/RealAuthenticatorKt;
.super Ljava/lang/Object;
.source "RealAuthenticator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAuthenticator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAuthenticator.kt\ncom/squareup/ui/login/RealAuthenticatorKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,1721:1\n1588#1,15:1722\n1588#1,15:1737\n704#2:1752\n777#2,2:1753\n*E\n*S KotlinDebug\n*F\n+ 1 RealAuthenticator.kt\ncom/squareup/ui/login/RealAuthenticatorKt\n*L\n1172#1,15:1722\n1184#1,15:1737\n1437#1:1752\n1437#1,2:1753\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00b2\u0001\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u001a4\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00060\u0005j\u0002`\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u00012\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0006H\u0000\u001a \u0010\u000c\u001a\u0012\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00060\u0005j\u0002`\u00072\u0006\u0010\r\u001a\u00020\u000eH\u0002\u001aF\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00060\u0005j\u0002`\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00102\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00062\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00010\u0012H\u0000\u001a@\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00060\u0005j\u0002`\u00072\u0006\u0010\u0008\u001a\u00020\t2\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00062\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00010\u0012H\u0000\u001a\u0010\u0010\u0013\u001a\u00020\u00012\u0006\u0010\n\u001a\u00020\u0001H\u0000\u001a\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0015H\u0002\u001a\u000c\u0010\u0017\u001a\u00020\u0018*\u00020\u0001H\u0002\u001a\u000c\u0010\u0019\u001a\u00020\u0001*\u00020\u0001H\u0002\u001a\u0014\u0010\u001a\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u001b\u001a\u00020\tH\u0002\u001a\u0014\u0010\u001c\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u001d\u001a\u00020\u001eH\u0002\u001a<\u0010\u001f\u001a\u0012\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00060\u0005j\u0002`\u0007\"\u0008\u0008\u0000\u0010 *\u00020!*\u00020\u00012\u0006\u0010\"\u001a\u00020#2\u000c\u0010$\u001a\u0008\u0012\u0004\u0012\u0002H 0%H\u0002\u001a\u000c\u0010&\u001a\u00020\'*\u00020\u0001H\u0002\u001a\u0014\u0010(\u001a\u00020\u0001*\u00020\u00012\u0006\u0010)\u001a\u00020\tH\u0000\u001a\u0014\u0010*\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u000b\u001a\u00020+H\u0000\u001a\"\u0010,\u001a\u00020\u0001*\u00020\u00012\u000c\u0010-\u001a\u0008\u0012\u0004\u0012\u00020/0.2\u0006\u00100\u001a\u00020\'H\u0000\u001a\u000c\u00101\u001a\u00020\u0001*\u00020\u0001H\u0002\u001a\u0014\u00101\u001a\u00020\u0001*\u00020\u00012\u0006\u00100\u001a\u00020\'H\u0002\u001a\u001a\u00102\u001a\u00020\u0001*\u00020\u00012\u000c\u00103\u001a\u0008\u0012\u0004\u0012\u00020\u001e0.H\u0000\u001a\u000c\u00104\u001a\u00020\u0001*\u00020\u0001H\u0000\u001a\u0014\u00105\u001a\u00020\u0001*\u00020\u00012\u0006\u00106\u001a\u000207H\u0002\u001a\u0014\u00108\u001a\u00020\u0001*\u00020\u00012\u0006\u0010$\u001a\u00020!H\u0000\u001a$\u00109\u001a\u00020\u0001*\u00020\u00012\u000e\u0010:\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020!0;2\u0006\u0010<\u001a\u00020!H\u0002\u001a\u0014\u0010=\u001a\u00020\u0001*\u00020\u00012\u0006\u0010>\u001a\u00020\tH\u0002\u001a,\u0010?\u001a\u0012\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00060\u0005j\u0002`\u0007*\u00020\u00012\u0006\u0010\"\u001a\u00020#2\u0006\u0010@\u001a\u00020\tH\u0000\u001a\u0014\u0010A\u001a\u00020\u0001*\u00020\u00012\u0006\u0010B\u001a\u00020\tH\u0000\u001a\u0014\u0010C\u001a\u00020\u0001*\u00020\u00012\u0006\u0010D\u001a\u00020EH\u0002\u001a\u000c\u0010F\u001a\u00020\u0001*\u00020\u0001H\u0002\u001a$\u0010G\u001a\u00020\u0001*\u00020\u00012\u0006\u0010H\u001a\u00020\'2\u0006\u0010I\u001a\u00020\'2\u0006\u00100\u001a\u00020\'H\u0002\u001a\u001a\u0010J\u001a\u00020K*\u00020!2\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020M0LH\u0002\u001a\u000c\u0010N\u001a\u00020\'*\u00020#H\u0002\u001a-\u0010O\u001a\u00020\u0001\"\n\u0008\u0000\u0010P\u0018\u0001*\u00020!*\u00020\u00012\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u0002HP\u0012\u0004\u0012\u0002HP0\u0012H\u0082\u0008\u001a\u001c\u0010Q\u001a\u00020\u0001*\u00020\u00012\u0006\u0010R\u001a\u00020\t2\u0006\u0010S\u001a\u00020\'H\u0002\u001a\u001c\u0010T\u001a\u00020\u0001*\u00020\u00012\u0006\u0010R\u001a\u00020\t2\u0006\u0010S\u001a\u00020\'H\u0002\u001a\u001c\u0010U\u001a\u00020\u0001*\u00020\u00012\u0006\u0010V\u001a\u00020W2\u0006\u0010X\u001a\u00020\u001eH\u0002\u001a5\u0010Y\u001a\u00020\u0001*\u00020\u00012\n\u0008\u0002\u0010Z\u001a\u0004\u0018\u00010\t2\n\u0008\u0002\u0010[\u001a\u0004\u0018\u00010\\2\n\u0008\u0002\u0010]\u001a\u0004\u0018\u00010\'H\u0000\u00a2\u0006\u0002\u0010^\"\u0014\u0010\u0000\u001a\u00020\u0001X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0003*$\u0008\u0000\u0010_\"\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00060\u00052\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00060\u0005\u00a8\u0006`"
    }
    d2 = {
        "START_STATE",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "getSTART_STATE",
        "()Lcom/squareup/ui/login/AuthenticatorState;",
        "enterState",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/login/AuthenticatorOutput;",
        "Lcom/squareup/ui/login/AuthUpdate;",
        "name",
        "",
        "state",
        "result",
        "handleSessionExpired",
        "event",
        "Lcom/squareup/account/AccountEvents$SessionExpired;",
        "modifyState",
        "Lkotlin/Function0;",
        "block",
        "Lkotlin/Function1;",
        "populateScreenPropertiesFromState",
        "scrubPasswordWhenLeavingScreen",
        "Lcom/squareup/ui/login/AuthenticatorStack;",
        "stack",
        "checkNoLoadInProgress",
        "",
        "enrollInGoogleAuth",
        "enrollInSms",
        "phoneNumber",
        "enrollInTwoFactor",
        "details",
        "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
        "goBackFrom",
        "S",
        "Lcom/squareup/ui/login/AuthenticatorScreen;",
        "input",
        "Lcom/squareup/ui/login/AuthenticatorInput;",
        "screen",
        "Ljava/lang/Class;",
        "isLoadInProgress",
        "",
        "loginWithDeviceCode",
        "deviceCode",
        "onCountryGuessedCallback",
        "Lcom/squareup/location/CountryGuesser$Result;",
        "pickTwoFactorMethod",
        "methods",
        "",
        "Lcom/squareup/ui/login/TwoFactorDetailsMethod;",
        "canSkipEnroll",
        "promptToEnrollSms",
        "promptToPickSmsNumber",
        "allTwoFactorDetails",
        "promptToVerifyGoogleAuthCode",
        "pushLoading",
        "loader",
        "Lcom/squareup/ui/login/Operation;",
        "pushScreen",
        "replaceScreen",
        "oldScreen",
        "Lkotlin/reflect/KClass;",
        "newScreen",
        "resetPassword",
        "email",
        "selectMerchant",
        "merchantToken",
        "selectUnit",
        "unitToken",
        "showFailure",
        "authFailure",
        "Lcom/squareup/ui/login/AuthenticationCallResult$Failure;",
        "showGoogleAuthCodeInsteadOfQr",
        "startTwoFactorEnrollment",
        "canUseGoogleAuth",
        "canUseSms",
        "toScreen",
        "Lcom/squareup/ui/login/AuthenticatorScreenRendering;",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "unitSelectionWasConfiguredToPause",
        "updateScreen",
        "T",
        "verifyGoogleAuthCode",
        "code",
        "rememberDevice",
        "verifySmsCode",
        "verifyTwoFactorCode",
        "endpoint",
        "Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;",
        "twoFactorDetails",
        "withTemporarySessionData",
        "temporarySessionToken",
        "unitsAndMerchants",
        "Lcom/squareup/ui/login/UnitsAndMerchants;",
        "isWorldEnabled",
        "(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Lcom/squareup/ui/login/UnitsAndMerchants;Ljava/lang/Boolean;)Lcom/squareup/ui/login/AuthenticatorState;",
        "AuthUpdate",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final START_STATE:Lcom/squareup/ui/login/AuthenticatorState;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    .line 153
    new-instance v8, Lcom/squareup/ui/login/AuthenticatorState;

    .line 154
    new-instance v4, Lcom/squareup/ui/login/AuthenticatorStack;

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1f

    const/16 v16, 0x0

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;-><init>(ZZLjava/lang/String;ZLcom/squareup/account/SecretString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/squareup/ui/login/AuthenticatorStack;-><init>(Ljava/util/List;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x17

    const/4 v7, 0x0

    move-object v0, v8

    .line 153
    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorState;-><init>(ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v8, Lcom/squareup/ui/login/RealAuthenticatorKt;->START_STATE:Lcom/squareup/ui/login/AuthenticatorState;

    return-void
.end method

.method public static final synthetic access$checkNoLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->checkNoLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)V

    return-void
.end method

.method public static final synthetic access$enrollInGoogleAuth(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enrollInGoogleAuth(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$enrollInSms(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enrollInSms(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$goBackFrom(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Ljava/lang/Class;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->goBackFrom(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Ljava/lang/Class;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$handleSessionExpired(Lcom/squareup/account/AccountEvents$SessionExpired;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->handleSessionExpired(Lcom/squareup/account/AccountEvents$SessionExpired;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$isLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->isLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$promptToEnrollSms(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->promptToEnrollSms(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$pushLoading(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/Operation;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushLoading(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/Operation;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$replaceScreen(Lcom/squareup/ui/login/AuthenticatorState;Lkotlin/reflect/KClass;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->replaceScreen(Lcom/squareup/ui/login/AuthenticatorState;Lkotlin/reflect/KClass;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$resetPassword(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->resetPassword(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$scrubPasswordWhenLeavingScreen(Lcom/squareup/ui/login/AuthenticatorStack;)Lcom/squareup/ui/login/AuthenticatorStack;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->scrubPasswordWhenLeavingScreen(Lcom/squareup/ui/login/AuthenticatorStack;)Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$showFailure(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticationCallResult$Failure;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->showFailure(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticationCallResult$Failure;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$showGoogleAuthCodeInsteadOfQr(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->showGoogleAuthCodeInsteadOfQr(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$startTwoFactorEnrollment(Lcom/squareup/ui/login/AuthenticatorState;ZZZ)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/ui/login/RealAuthenticatorKt;->startTwoFactorEnrollment(Lcom/squareup/ui/login/AuthenticatorState;ZZZ)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toScreen(Lcom/squareup/ui/login/AuthenticatorScreen;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/ui/login/AuthenticatorScreenRendering;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->toScreen(Lcom/squareup/ui/login/AuthenticatorScreen;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateScreen(Lcom/squareup/ui/login/AuthenticatorState;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->updateScreen(Lcom/squareup/ui/login/AuthenticatorState;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$verifyGoogleAuthCode(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Z)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->verifyGoogleAuthCode(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Z)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$verifySmsCode(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Z)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->verifySmsCode(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Z)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method private static final checkNoLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)V
    .locals 2

    .line 1691
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getOperation()Lcom/squareup/ui/login/Operation;

    move-result-object v0

    .line 1692
    invoke-static {p0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->isLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    if-eqz p0, :cond_0

    return-void

    .line 1693
    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Expected operation to be NONE, was "

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1692
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private static final enrollInGoogleAuth(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 2

    .line 1222
    new-instance v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;-><init>()V

    .line 1223
    new-instance v1, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->build()Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->googleauth(Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;)Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    move-result-object v0

    .line 1224
    invoke-virtual {v0}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->build()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object v0

    const-string v1, "TwoFactorDetails.Builder\u2026ild())\n          .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1221
    invoke-static {p0, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enrollInTwoFactor(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method private static final enrollInSms(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 2

    .line 1230
    new-instance v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;-><init>()V

    .line 1232
    new-instance v1, Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;-><init>()V

    .line 1233
    invoke-virtual {v1, p1}, Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;->phone(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;

    move-result-object p1

    .line 1234
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;->build()Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    move-result-object p1

    .line 1231
    invoke-virtual {v0, p1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->sms(Lcom/squareup/protos/multipass/common/SmsTwoFactor;)Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    move-result-object p1

    .line 1236
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->build()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object p1

    const-string v0, "TwoFactorDetails.Builder\u2026     )\n          .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1229
    invoke-static {p0, p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enrollInTwoFactor(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method private static final enrollInTwoFactor(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1457
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1460
    new-instance v0, Lcom/squareup/ui/login/Operation;

    sget v1, Lcom/squareup/common/authenticator/impl/R$string;->two_factor_spinner_title_enrolling:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    new-instance v1, Lcom/squareup/ui/login/OperationType$EnrollTwoFactor;

    invoke-direct {v1, p1}, Lcom/squareup/ui/login/OperationType$EnrollTwoFactor;-><init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)V

    move-object v4, v1

    check-cast v4, Lcom/squareup/ui/login/OperationType;

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/login/Operation;-><init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 1459
    invoke-static {p0, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushLoading(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/Operation;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method public static final enterState(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "name"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1514
    new-instance v0, Lcom/squareup/ui/login/RealAuthenticatorKt$enterState$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt$enterState$1;-><init>(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    new-instance p0, Lcom/squareup/ui/login/RealAuthenticatorKt$enterState$2;

    invoke-direct {p0, p1}, Lcom/squareup/ui/login/RealAuthenticatorKt$enterState$2;-><init>(Lcom/squareup/ui/login/AuthenticatorState;)V

    check-cast p0, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p2, p0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->modifyState(Lkotlin/jvm/functions/Function0;Lcom/squareup/ui/login/AuthenticatorOutput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    and-int/lit8 p3, p3, 0x4

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 1513
    check-cast p2, Lcom/squareup/ui/login/AuthenticatorOutput;

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final getSTART_STATE()Lcom/squareup/ui/login/AuthenticatorState;
    .locals 1

    .line 153
    sget-object v0, Lcom/squareup/ui/login/RealAuthenticatorKt;->START_STATE:Lcom/squareup/ui/login/AuthenticatorState;

    return-object v0
.end method

.method private static final goBackFrom(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Ljava/lang/Class;)Lcom/squareup/workflow/WorkflowAction;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Lcom/squareup/ui/login/AuthenticatorScreen;",
            ">(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorInput;",
            "Ljava/lang/Class<",
            "TS;>;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1652
    invoke-static {v1, v0, v1}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const-string p2, "Authenticator popping screen: %s"

    .line 1654
    invoke-static {p2, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1657
    invoke-static {p0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->checkNoLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)V

    .line 1661
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorStack;->size()I

    move-result p2

    if-gt p2, v0, :cond_0

    sget-object p0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object p1, Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$Canceled;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorOutput$FinalResult$Canceled;

    const-string p2, "back past start of authenticator"

    invoke-virtual {p0, p2, p1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    goto/16 :goto_1

    .line 1665
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object p2

    instance-of p2, p2, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;

    if-eqz p2, :cond_2

    invoke-static {p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->unitSelectionWasConfiguredToPause(Lcom/squareup/ui/login/AuthenticatorInput;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 1666
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getUnitsAndMerchants()Lcom/squareup/ui/login/UnitsAndMerchants;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/login/UnitsAndMerchants;->getUnits()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ui/login/AuthenticationServiceEndpointKt;->allSameMerchant(Ljava/util/List;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1667
    new-instance p1, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken$WithMerchant;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getTemporarySessionToken()Lcom/squareup/account/SecretString;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken$WithMerchant;-><init>(Lcom/squareup/account/SecretString;)V

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;

    goto :goto_0

    .line 1669
    :cond_1
    new-instance p1, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken$WithoutMerchant;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getTemporarySessionToken()Lcom/squareup/account/SecretString;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken$WithoutMerchant;-><init>(Lcom/squareup/account/SecretString;)V

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;

    :goto_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1673
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->scrubPasswordWhenLeavingScreen(Lcom/squareup/ui/login/AuthenticatorStack;)Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorStack;->pop()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0x17

    const/4 v7, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p2

    .line 1674
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;

    invoke-direct {v0, p1, p0}, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;-><init>(Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;Lcom/squareup/ui/login/AuthenticatorState;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorOutput;

    const-string p0, "back to unit selection ready"

    .line 1671
    invoke-static {p0, p2, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1679
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorStack;->pop()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x17

    const/4 v9, 0x0

    move-object v2, p0

    invoke-static/range {v2 .. v9}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    const/4 p1, 0x4

    const-string p2, "goBackFrom"

    invoke-static {p2, p0, v1, p1, v1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    :goto_1
    return-object p0
.end method

.method private static final handleSessionExpired(Lcom/squareup/account/AccountEvents$SessionExpired;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/account/AccountEvents$SessionExpired;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    .line 1409
    sget-object p0, Lcom/squareup/ui/login/RealAuthenticatorKt$handleSessionExpired$1;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticatorKt$handleSessionExpired$1;

    check-cast p0, Lkotlin/jvm/functions/Function1;

    const/4 v0, 0x0

    const-string v1, "sessionExpired"

    const/4 v2, 0x2

    invoke-static {v1, v0, p0, v2, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->modifyState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorOutput;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private static final isLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)Z
    .locals 2

    .line 1699
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getOperation()Lcom/squareup/ui/login/Operation;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/login/Operation;->Companion:Lcom/squareup/ui/login/Operation$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/login/Operation$Companion;->getNONE()Lcom/squareup/ui/login/Operation;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getOperation()Lcom/squareup/ui/login/Operation;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/ui/login/Operation;->getType()Lcom/squareup/ui/login/OperationType;

    move-result-object p0

    instance-of p0, p0, Lcom/squareup/ui/login/OperationType$VerifyLoginByCaptcha;

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public static final loginWithDeviceCode(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 12

    const-string v0, "$this$loginWithDeviceCode"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1180
    invoke-static {v1, v0, v1}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1181
    invoke-static {p0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->checkNoLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)V

    .line 1183
    move-object v2, p1

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1737
    invoke-static {v1, v0, v1}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1740
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p1

    .line 1742
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v2

    instance-of v3, v2, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;

    if-nez v3, :cond_0

    move-object v2, v1

    :cond_0
    check-cast v2, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;

    check-cast v2, Lcom/squareup/ui/login/AuthenticatorScreen;

    if-eqz v2, :cond_1

    .line 1747
    move-object v1, v2

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;

    .line 1184
    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;->withErrorIndicated()Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorScreen;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    aput-object v1, v3, v0

    const-string v0, "Authenticator updating screen: %s -> %s"

    .line 1749
    invoke-static {v0, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 1751
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorStack;->pop()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/login/AuthenticatorStack;->plus(Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v8

    const/4 v9, 0x0

    const/16 v10, 0x17

    const/4 v11, 0x0

    move-object v4, p0

    invoke-static/range {v4 .. v11}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0

    .line 1743
    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    .line 1744
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected to be on screen "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class v2, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", was "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1745
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    :cond_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1743
    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 1186
    :cond_3
    invoke-static {}, Lcom/squareup/ui/login/AuthenticatorKt;->getDEVICE_CODE_SUPPORTED_LENGTH()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1187
    new-instance p1, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;

    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$DeviceCodeLoginFailedText;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$DeviceCodeLoginFailedText;

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;

    invoke-direct {p1, v0}, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;)V

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorScreen;

    invoke-static {p0, p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0

    .line 1190
    :cond_4
    new-instance v0, Lcom/squareup/protos/register/api/LoginRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/register/api/LoginRequest$Builder;-><init>()V

    .line 1191
    invoke-virtual {v0, p1}, Lcom/squareup/protos/register/api/LoginRequest$Builder;->device_code(Ljava/lang/String;)Lcom/squareup/protos/register/api/LoginRequest$Builder;

    move-result-object p1

    .line 1192
    invoke-virtual {p1}, Lcom/squareup/protos/register/api/LoginRequest$Builder;->build()Lcom/squareup/protos/register/api/LoginRequest;

    move-result-object p1

    .line 1194
    new-instance v6, Lcom/squareup/ui/login/Operation;

    sget v0, Lcom/squareup/common/authenticator/impl/R$string;->sign_in_signing_in:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v0, Lcom/squareup/ui/login/OperationType$LoginWithDeviceCode;

    const-string v2, "loginRequest"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/ui/login/OperationType$LoginWithDeviceCode;-><init>(Lcom/squareup/protos/register/api/LoginRequest;)V

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/login/OperationType;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/login/Operation;-><init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-static {p0, v6}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushLoading(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/Operation;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method public static final modifyState(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorOutput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "name"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1537
    new-instance v0, Lcom/squareup/ui/login/RealAuthenticatorKt$modifyState$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/login/RealAuthenticatorKt$modifyState$2;-><init>(Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->modifyState(Lkotlin/jvm/functions/Function0;Lcom/squareup/ui/login/AuthenticatorOutput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final modifyState(Lkotlin/jvm/functions/Function0;Lcom/squareup/ui/login/AuthenticatorOutput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "name"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1524
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v1, Lcom/squareup/ui/login/RealAuthenticatorKt$modifyState$1;

    invoke-direct {v1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt$modifyState$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p0, p1, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->modifyState(Lkotlin/jvm/functions/Function0;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic modifyState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorOutput;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p1, 0x0

    .line 1535
    check-cast p1, Lcom/squareup/ui/login/AuthenticatorOutput;

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->modifyState(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorOutput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic modifyState$default(Lkotlin/jvm/functions/Function0;Lcom/squareup/ui/login/AuthenticatorOutput;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p1, 0x0

    .line 1522
    check-cast p1, Lcom/squareup/ui/login/AuthenticatorOutput;

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->modifyState(Lkotlin/jvm/functions/Function0;Lcom/squareup/ui/login/AuthenticatorOutput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final onCountryGuessedCallback(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/location/CountryGuesser$Result;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 7

    const-string v0, "$this$onCountryGuessedCallback"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1426
    instance-of v0, p1, Lcom/squareup/location/CountryGuesser$Result$Country;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1427
    invoke-virtual {p1}, Lcom/squareup/location/CountryGuesser$Result;->getSupportsPayments()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x3

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/login/RealAuthenticatorKt;->withTemporarySessionData$default(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Lcom/squareup/ui/login/UnitsAndMerchants;Ljava/lang/Boolean;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static final pickTwoFactorMethod(Lcom/squareup/ui/login/AuthenticatorState;Ljava/util/List;Z)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/ui/login/TwoFactorDetailsMethod;",
            ">;Z)",
            "Lcom/squareup/ui/login/AuthenticatorState;"
        }
    .end annotation

    const-string v0, "$this$pickTwoFactorMethod"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "methods"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1210
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1211
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;-><init>(Ljava/util/List;Z)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen;

    invoke-static {p0, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method public static final populateScreenPropertiesFromState(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 11

    const-string v0, "state"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1566
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1568
    instance-of v1, v0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->isWorldEnabled()Z

    move-result v7

    const/4 v8, 0x0

    const/16 v9, 0x17

    const/4 v10, 0x0

    invoke-static/range {v3 .. v10}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->copy$default(Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;ZZLjava/lang/String;ZLcom/squareup/account/SecretString;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto :goto_0

    .line 1569
    :cond_0
    instance-of v1, v0, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->isWorldEnabled()Z

    move-result v4

    invoke-static {v1, v3, v4, v2, v3}, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;->copy$default(Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto :goto_0

    .line 1570
    :cond_1
    instance-of v1, v0, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;

    if-eqz v1, :cond_2

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getUnitsAndMerchants()Lcom/squareup/ui/login/UnitsAndMerchants;

    move-result-object v4

    invoke-static {v1, v3, v4, v2, v3}, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;->copy$default(Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;Ljava/lang/String;Lcom/squareup/ui/login/UnitsAndMerchants;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto :goto_0

    .line 1571
    :cond_2
    instance-of v1, v0, Lcom/squareup/ui/login/AuthenticatorScreen$PickMerchant;

    if-eqz v1, :cond_3

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorScreen$PickMerchant;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getUnitsAndMerchants()Lcom/squareup/ui/login/UnitsAndMerchants;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/ui/login/AuthenticatorScreen$PickMerchant;->copy(Lcom/squareup/ui/login/UnitsAndMerchants;)Lcom/squareup/ui/login/AuthenticatorScreen$PickMerchant;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto :goto_0

    :cond_3
    move-object v1, v0

    :goto_0
    if-ne v1, v0, :cond_4

    return-object p0

    .line 1578
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorStack;->getScreens()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->dropLast(Ljava/util/List;I)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1579
    new-instance v5, Lcom/squareup/ui/login/AuthenticatorStack;

    invoke-direct {v5, v0}, Lcom/squareup/ui/login/AuthenticatorStack;-><init>(Ljava/util/List;)V

    const/4 v6, 0x0

    const/16 v7, 0x17

    const/4 v8, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    :cond_5
    return-object p0
.end method

.method private static final promptToEnrollSms(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1251
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1252
    invoke-static {p0}, Lcom/squareup/ui/login/AuthenticatorStateKt;->getCanSkipEnroll(Lcom/squareup/ui/login/AuthenticatorState;)Z

    move-result v0

    invoke-static {p0, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->promptToEnrollSms(Lcom/squareup/ui/login/AuthenticatorState;Z)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method private static final promptToEnrollSms(Lcom/squareup/ui/login/AuthenticatorState;Z)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 1

    .line 1256
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;

    invoke-direct {v0, p1}, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;-><init>(Z)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen;

    invoke-static {p0, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method public static final promptToPickSmsNumber(Lcom/squareup/ui/login/AuthenticatorState;Ljava/util/List;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            ">;)",
            "Lcom/squareup/ui/login/AuthenticatorState;"
        }
    .end annotation

    const-string v0, "$this$promptToPickSmsNumber"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "allTwoFactorDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1437
    check-cast p1, Ljava/lang/Iterable;

    .line 1752
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 1753
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    .line 1437
    iget-object v4, v4, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    if-eqz v4, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1754
    :cond_2
    check-cast v0, Ljava/util/List;

    .line 1438
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    const/4 v1, 0x2

    if-lt p1, v1, :cond_3

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_4

    .line 1439
    new-instance p1, Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;

    invoke-direct {p1, v0}, Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;-><init>(Ljava/util/List;)V

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorScreen;

    invoke-static {p0, p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0

    .line 1438
    :cond_4
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Found fewer than 2 SMS numbers."

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final promptToVerifyGoogleAuthCode(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 4

    const-string v0, "$this$promptToVerifyGoogleAuthCode"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1260
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1262
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v1

    instance-of v2, v1, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuth;

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuth;

    if-eqz v0, :cond_1

    .line 1268
    new-instance v1, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    .line 1269
    invoke-interface {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuth;->getTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object v2

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 1270
    invoke-interface {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuth;->getTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object v0

    .line 1271
    sget-object v3, Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;->ENROLL:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    .line 1268
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;-><init>(Ljava/util/List;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;)V

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorScreen;

    .line 1267
    invoke-static {p0, v1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0

    .line 1263
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 1264
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected to be in EnrollGoogleAuthCode state, but is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1263
    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private static final pushLoading(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/Operation;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 10

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1619
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1622
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Authenticator pushing loading from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x2026

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0xf

    const/4 v9, 0x0

    move-object v2, p0

    move-object v7, p1

    .line 1624
    invoke-static/range {v2 .. v9}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method public static final pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 9

    const-string v0, "$this$pushScreen"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1606
    invoke-static {v1, v0, v1}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Authenticator pushing screen: %s"

    .line 1608
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1611
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v0

    .line 1612
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1615
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/login/AuthenticatorStack;->plus(Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v5

    sget-object p1, Lcom/squareup/ui/login/Operation;->Companion:Lcom/squareup/ui/login/Operation$Companion;

    invoke-virtual {p1}, Lcom/squareup/ui/login/Operation$Companion;->getNONE()Lcom/squareup/ui/login/Operation;

    move-result-object v6

    const/4 v7, 0x7

    const/4 v8, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method private static final replaceScreen(Lcom/squareup/ui/login/AuthenticatorState;Lkotlin/reflect/KClass;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lkotlin/reflect/KClass<",
            "+",
            "Lcom/squareup/ui/login/AuthenticatorScreen;",
            ">;",
            "Lcom/squareup/ui/login/AuthenticatorScreen;",
            ")",
            "Lcom/squareup/ui/login/AuthenticatorState;"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1631
    invoke-static {v1, v0, v1}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1632
    invoke-static {p0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->checkNoLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)V

    .line 1635
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v1

    .line 1636
    invoke-static {p1}, Lkotlin/jvm/JvmClassMappingKt;->getJavaClass(Lkotlin/reflect/KClass;)Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1637
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorStack;->pop()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object p1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const-string v1, "Authenticator pushing screen: %s"

    .line 1639
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1642
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v0

    .line 1643
    invoke-static {v0, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1645
    invoke-virtual {p1, p2}, Lcom/squareup/ui/login/AuthenticatorStack;->plus(Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v5

    const/4 v6, 0x0

    const/16 v7, 0x17

    const/4 v8, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0

    .line 1636
    :cond_1
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Expected top screen to be "

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ", but was "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private static final resetPassword(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 13

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1168
    invoke-static {v1, v0, v1}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1169
    invoke-static {p0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->checkNoLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)V

    .line 1722
    invoke-static {v1, v0, v1}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1725
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v2

    .line 1727
    invoke-virtual {v2}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v3

    instance-of v4, v3, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;

    if-nez v4, :cond_0

    move-object v3, v1

    :cond_0
    check-cast v3, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;

    check-cast v3, Lcom/squareup/ui/login/AuthenticatorScreen;

    if-eqz v3, :cond_1

    .line 1732
    move-object v4, v3

    check-cast v4, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;

    const/4 v5, 0x2

    const/4 v6, 0x0

    .line 1172
    invoke-static {v4, p1, v6, v5, v1}, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;->copy$default(Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorScreen;

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v3, v4, v6

    aput-object v1, v4, v0

    const-string v0, "Authenticator updating screen: %s -> %s"

    .line 1734
    invoke-static {v0, v4}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 1736
    invoke-virtual {v2}, Lcom/squareup/ui/login/AuthenticatorStack;->pop()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/login/AuthenticatorStack;->plus(Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v9

    const/4 v10, 0x0

    const/16 v11, 0x17

    const/4 v12, 0x0

    move-object v5, p0

    invoke-static/range {v5 .. v12}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    .line 1175
    new-instance v6, Lcom/squareup/ui/login/Operation;

    sget v0, Lcom/squareup/common/authenticator/impl/R$string;->forgot_password_sending:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v0, Lcom/squareup/ui/login/OperationType$ResetPassword;

    new-instance v2, Lcom/squareup/server/account/ForgotPasswordBody;

    invoke-direct {v2, p1}, Lcom/squareup/server/account/ForgotPasswordBody;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v2}, Lcom/squareup/ui/login/OperationType$ResetPassword;-><init>(Lcom/squareup/server/account/ForgotPasswordBody;)V

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/login/OperationType;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/login/Operation;-><init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 1174
    invoke-static {p0, v6}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushLoading(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/Operation;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0

    .line 1728
    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    .line 1729
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Expected to be on screen "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class v0, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", was "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1730
    invoke-virtual {v2}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    :cond_2
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1728
    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method private static final scrubPasswordWhenLeavingScreen(Lcom/squareup/ui/login/AuthenticatorStack;)Lcom/squareup/ui/login/AuthenticatorStack;
    .locals 10

    .line 1544
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorStack;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    return-object p0

    .line 1546
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorStack;->getScreens()Ljava/util/List;

    move-result-object v2

    sub-int/2addr v0, v1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorScreen;

    .line 1548
    instance-of v2, v1, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    if-eqz v2, :cond_1

    .line 1549
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorStack;->getScreens()Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/util/Collection;

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p0

    .line 1550
    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v1, Lcom/squareup/account/SecretString;->Companion:Lcom/squareup/account/SecretString$Companion;

    invoke-virtual {v1}, Lcom/squareup/account/SecretString$Companion;->getEMPTY()Lcom/squareup/account/SecretString;

    move-result-object v7

    const/16 v8, 0xf

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;->copy$default(Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;ZZLjava/lang/String;ZLcom/squareup/account/SecretString;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    move-result-object v1

    .line 1551
    invoke-interface {p0, v0, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1552
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorStack;

    invoke-direct {v0, p0}, Lcom/squareup/ui/login/AuthenticatorStack;-><init>(Ljava/util/List;)V

    return-object v0

    :cond_1
    return-object p0
.end method

.method public static final selectMerchant(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorInput;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$selectMerchant"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "merchantToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1340
    invoke-static {v1, v0, v1}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1342
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getUnitsAndMerchants()Lcom/squareup/ui/login/UnitsAndMerchants;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/login/UnitsAndMerchants;->getMerchantTokenToUnitList()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3

    check-cast v2, Ljava/util/List;

    .line 1348
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v0, :cond_0

    const/4 p2, 0x0

    .line 1349
    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/register/api/Unit;

    iget-object p2, p2, Lcom/squareup/protos/register/api/Unit;->unit_token:Ljava/lang/String;

    const-string v0, "unitList[0].unit_token"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p2}, Lcom/squareup/ui/login/RealAuthenticatorKt;->selectUnit(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p2

    goto :goto_0

    .line 1351
    :cond_0
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;

    const/4 v2, 0x2

    invoke-direct {v0, p2, v1, v2, v1}, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;-><init>(Ljava/lang/String;Lcom/squareup/ui/login/UnitsAndMerchants;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen;

    invoke-static {p0, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p2

    .line 1354
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorInput;->getUnitSelectionFlow()Lcom/squareup/ui/login/UnitSelectionFlow;

    move-result-object p1

    .line 1361
    instance-of v0, p1, Lcom/squareup/ui/login/UnitSelectionFlow$Pause;

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;

    new-instance v1, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken$WithoutMerchant;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getTemporarySessionToken()Lcom/squareup/account/SecretString;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken$WithoutMerchant;-><init>(Lcom/squareup/account/SecretString;)V

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;

    invoke-direct {v0, v1, p2}, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;-><init>(Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;Lcom/squareup/ui/login/AuthenticatorState;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    goto :goto_1

    .line 1362
    :cond_1
    instance-of p0, p1, Lcom/squareup/ui/login/UnitSelectionFlow$Go;

    if-eqz p0, :cond_2

    const/4 p0, 0x4

    const-string p1, "selectMerchant"

    invoke-static {p1, p2, v1, p0, v1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    :goto_1
    return-object p0

    :cond_2
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 1342
    :cond_3
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Expected merchantToken to be present."

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final selectUnit(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 7

    const-string v0, "$this$selectUnit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unitToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1367
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1368
    invoke-static {p0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->checkNoLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)V

    .line 1370
    new-instance v0, Lcom/squareup/protos/register/api/SelectUnitRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/register/api/SelectUnitRequest$Builder;-><init>()V

    .line 1371
    invoke-virtual {v0, p1}, Lcom/squareup/protos/register/api/SelectUnitRequest$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/register/api/SelectUnitRequest$Builder;

    move-result-object p1

    .line 1372
    invoke-virtual {p1}, Lcom/squareup/protos/register/api/SelectUnitRequest$Builder;->build()Lcom/squareup/protos/register/api/SelectUnitRequest;

    move-result-object p1

    .line 1375
    new-instance v6, Lcom/squareup/ui/login/Operation;

    .line 1376
    sget v0, Lcom/squareup/common/authenticator/impl/R$string;->sign_in_signing_in:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1377
    new-instance v0, Lcom/squareup/ui/login/OperationType$SelectUnit;

    const-string v2, "selectUnitRequest"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/ui/login/OperationType$SelectUnit;-><init>(Lcom/squareup/protos/register/api/SelectUnitRequest;)V

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/login/OperationType;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, v6

    .line 1375
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/login/Operation;-><init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 1374
    invoke-static {p0, v6}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushLoading(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/Operation;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method

.method private static final showFailure(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticationCallResult$Failure;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 1

    .line 1494
    instance-of v0, p1, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithAlert;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;

    check-cast p1, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithAlert;

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithAlert;->getAlert()Lcom/squareup/protos/multipass/mobile/Alert;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;-><init>(Lcom/squareup/protos/multipass/mobile/Alert;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen;

    goto :goto_1

    .line 1495
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;

    if-eqz v0, :cond_4

    .line 1496
    check-cast p1, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;->getWarning()Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$CustomWarningText;

    if-eqz v0, :cond_3

    .line 1497
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;->getWarning()Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$CustomWarningText;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$CustomWarningText;->getTitle()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 1498
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;->getWarning()Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$CustomWarningText;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText$CustomWarningText;->getMessage()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Expected message not to be blank."

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 1497
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Expected title not to be blank."

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 1500
    :cond_3
    :goto_0
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticationCallResult$Failure$FailureWithWarning;->getWarning()Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen;

    .line 1503
    :goto_1
    invoke-static {p0, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0

    .line 1500
    :cond_4
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method private static final showGoogleAuthCodeInsteadOfQr(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1240
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1242
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v1

    instance-of v2, v1, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    if-eqz v0, :cond_1

    .line 1247
    new-instance v1, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;->getTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;-><init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)V

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorScreen;

    invoke-static {p0, v1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0

    .line 1243
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 1244
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected to be in EnrollGoogleAuthQr state, but is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1243
    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private static final startTwoFactorEnrollment(Lcom/squareup/ui/login/AuthenticatorState;ZZZ)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    const/4 p1, 0x2

    new-array p1, p1, [Lcom/squareup/ui/login/TwoFactorDetailsMethod;

    const/4 p2, 0x0

    .line 1449
    sget-object v0, Lcom/squareup/ui/login/TwoFactorDetailsMethod;->GOOGLE_AUTH:Lcom/squareup/ui/login/TwoFactorDetailsMethod;

    aput-object v0, p1, p2

    const/4 p2, 0x1

    sget-object v0, Lcom/squareup/ui/login/TwoFactorDetailsMethod;->SMS:Lcom/squareup/ui/login/TwoFactorDetailsMethod;

    aput-object v0, p1, p2

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-static {p0, p1, p3}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pickTwoFactorMethod(Lcom/squareup/ui/login/AuthenticatorState;Ljava/util/List;Z)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    .line 1451
    invoke-static {p0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enrollInGoogleAuth(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    .line 1452
    invoke-static {p0, p3}, Lcom/squareup/ui/login/RealAuthenticatorKt;->promptToEnrollSms(Lcom/squareup/ui/login/AuthenticatorState;Z)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    :goto_0
    return-object p0

    .line 1453
    :cond_2
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "2FA required but Google Auth and SMS are not allowed."

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method private static final toScreen(Lcom/squareup/ui/login/AuthenticatorScreen;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/ui/login/AuthenticatorScreenRendering;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorScreen;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)",
            "Lcom/squareup/ui/login/AuthenticatorScreenRendering;"
        }
    .end annotation

    .line 1704
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EmailPasswordRendering;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EmailPasswordRendering;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    goto/16 :goto_0

    .line 1705
    :cond_0
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ForgotPasswordRendering;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ForgotPasswordRendering;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    goto/16 :goto_0

    .line 1706
    :cond_1
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ForgotPasswordFailedRendering;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ForgotPasswordFailedRendering;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    goto/16 :goto_0

    .line 1707
    :cond_2
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$DeviceCodeRendering;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$DeviceCodeRendering;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    goto/16 :goto_0

    .line 1708
    :cond_3
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$ProvidedDeviceCodeAuth;

    if-eqz v0, :cond_4

    new-instance p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;

    invoke-direct {p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    goto/16 :goto_0

    .line 1709
    :cond_4
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickUnitRendering;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickUnitRendering;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    goto/16 :goto_0

    .line 1710
    :cond_5
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$PickMerchant;

    if-eqz v0, :cond_6

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickMerchantRendering;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$PickMerchant;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickMerchantRendering;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$PickMerchant;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    goto/16 :goto_0

    .line 1711
    :cond_6
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;

    if-eqz v0, :cond_7

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ShowWarningRendering;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ShowWarningRendering;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    goto/16 :goto_0

    .line 1712
    :cond_7
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;

    if-eqz v0, :cond_8

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ShowLoginAlertRendering;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ShowLoginAlertRendering;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    goto :goto_0

    .line 1713
    :cond_8
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;

    if-eqz v0, :cond_9

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickTwoFactorMethodRendering;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickTwoFactorMethodRendering;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    goto :goto_0

    .line 1714
    :cond_9
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;

    if-eqz v0, :cond_a

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickSmsRendering;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickSmsRendering;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    goto :goto_0

    .line 1715
    :cond_a
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;

    if-eqz v0, :cond_b

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollSmsRendering;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollSmsRendering;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    goto :goto_0

    .line 1716
    :cond_b
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    if-eqz v0, :cond_c

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthQr;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    goto :goto_0

    .line 1717
    :cond_c
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;

    if-eqz v0, :cond_d

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthCodeRendering;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthCodeRendering;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    goto :goto_0

    .line 1718
    :cond_d
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    if-eqz v0, :cond_e

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$VerifyCodeSmsRendering;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$VerifyCodeSmsRendering;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    goto :goto_0

    .line 1719
    :cond_e
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    if-eqz v0, :cond_f

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$VerifyCodeGoogleAuthRendering;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$VerifyCodeGoogleAuthRendering;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    :goto_0
    return-object v0

    :cond_f
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method private static final unitSelectionWasConfiguredToPause(Lcom/squareup/ui/login/AuthenticatorInput;)Z
    .locals 3

    .line 1684
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorInput;->getUnitSelectionFlow()Lcom/squareup/ui/login/UnitSelectionFlow;

    move-result-object v0

    .line 1685
    instance-of v1, v0, Lcom/squareup/ui/login/UnitSelectionFlow$Pause;

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 1686
    :cond_0
    instance-of v0, v0, Lcom/squareup/ui/login/UnitSelectionFlow$Go;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorInput;->getUnitSelectionFlow()Lcom/squareup/ui/login/UnitSelectionFlow;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/login/UnitSelectionFlow$Go;

    invoke-virtual {p0}, Lcom/squareup/ui/login/UnitSelectionFlow$Go;->getResumeState()Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    if-eqz p0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_2
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method private static final synthetic updateScreen(Lcom/squareup/ui/login/AuthenticatorState;Lkotlin/jvm/functions/Function1;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/ui/login/AuthenticatorScreen;",
            ">(",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+TT;>;)",
            "Lcom/squareup/ui/login/AuthenticatorState;"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1588
    invoke-static {v1, v0, v1}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1591
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v2

    .line 1593
    invoke-virtual {v2}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v3

    const-string v4, "T"

    const/4 v5, 0x2

    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    check-cast v3, Lcom/squareup/ui/login/AuthenticatorScreen;

    if-eqz v3, :cond_0

    .line 1598
    invoke-interface {p1, v3}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorScreen;

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v3, v1, v4

    aput-object p1, v1, v0

    const-string v0, "Authenticator updating screen: %s -> %s"

    .line 1600
    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 1602
    invoke-virtual {v2}, Lcom/squareup/ui/login/AuthenticatorStack;->pop()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/login/AuthenticatorStack;->plus(Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x17

    const/4 v10, 0x0

    move-object v3, p0

    invoke-static/range {v3 .. v10}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0

    .line 1594
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    .line 1595
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Expected to be on screen "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x4

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Lcom/squareup/ui/login/AuthenticatorScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", was "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1596
    invoke-virtual {v2}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1594
    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method private static final verifyGoogleAuthCode(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Z)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1280
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1281
    invoke-static {p0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->checkNoLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)V

    .line 1283
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v1

    instance-of v2, v1, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;

    if-eqz v0, :cond_1

    .line 1289
    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->getGoogleAuthTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object v1

    .line 1290
    invoke-virtual {v1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->newBuilder()Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    move-result-object v2

    .line 1292
    iget-object v1, v1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->newBuilder()Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;

    move-result-object v1

    .line 1293
    invoke-virtual {v1, p1}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->verification_code(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;

    move-result-object p1

    .line 1294
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor$Builder;->build()Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    move-result-object p1

    .line 1291
    invoke-virtual {v2, p1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->googleauth(Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;)Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    move-result-object p1

    .line 1296
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->set_trusted_device(Ljava/lang/Boolean;)Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    move-result-object p1

    .line 1297
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->build()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object p1

    .line 1300
    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeGoogleAuth;->getLogin()Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    move-result-object p2

    const-string v0, "twoFactorDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p2, p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->verifyTwoFactorCode(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0

    .line 1284
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 1285
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Expected to be in VerifyCodeGoogleAuth state, but is "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1284
    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private static final verifySmsCode(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Z)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1307
    invoke-static {v0, v1, v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 1308
    invoke-static {p0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->checkNoLoadInProgress(Lcom/squareup/ui/login/AuthenticatorState;)V

    .line 1310
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorState;->getScreens()Lcom/squareup/ui/login/AuthenticatorStack;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticatorStack;->getTop()Lcom/squareup/ui/login/AuthenticatorScreen;

    move-result-object v1

    instance-of v2, v1, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;

    if-eqz v0, :cond_1

    .line 1316
    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->getSmsTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object v1

    .line 1317
    invoke-virtual {v1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->newBuilder()Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    move-result-object v2

    .line 1319
    iget-object v1, v1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->newBuilder()Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;

    move-result-object v1

    .line 1320
    invoke-virtual {v1, p1}, Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;->verification_code(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;

    move-result-object p1

    .line 1321
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;->build()Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    move-result-object p1

    .line 1318
    invoke-virtual {v2, p1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->sms(Lcom/squareup/protos/multipass/common/SmsTwoFactor;)Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    move-result-object p1

    .line 1323
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->set_trusted_device(Ljava/lang/Boolean;)Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    move-result-object p1

    .line 1324
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->build()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object p1

    .line 1327
    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$VerifyCodeSms;->getLogin()Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    move-result-object p2

    const-string v0, "twoFactorDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p2, p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->verifyTwoFactorCode(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0

    .line 1311
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 1312
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Expected to be in VerifyCodeSms state, but is "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1311
    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private static final verifyTwoFactorCode(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 7

    .line 1392
    sget-object v0, Lcom/squareup/ui/login/RealAuthenticatorKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1400
    new-instance p1, Lcom/squareup/ui/login/Operation;

    .line 1401
    sget v0, Lcom/squareup/common/authenticator/impl/R$string;->two_factor_spinner_title_verifying_code:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1402
    new-instance v0, Lcom/squareup/ui/login/OperationType$VerifyTwoFactorByUpgradeSession;

    invoke-direct {v0, p2}, Lcom/squareup/ui/login/OperationType$VerifyTwoFactorByUpgradeSession;-><init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)V

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/login/OperationType;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p1

    .line 1400
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/login/Operation;-><init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 1399
    invoke-static {p0, p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushLoading(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/Operation;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 1394
    :cond_1
    new-instance p1, Lcom/squareup/ui/login/Operation;

    .line 1395
    sget v0, Lcom/squareup/common/authenticator/impl/R$string;->two_factor_spinner_title_verifying_code:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1396
    new-instance v0, Lcom/squareup/ui/login/OperationType$VerifyTwoFactorByEnroll;

    invoke-direct {v0, p2}, Lcom/squareup/ui/login/OperationType$VerifyTwoFactorByEnroll;-><init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)V

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/login/OperationType;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p1

    .line 1394
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/login/Operation;-><init>(Ljava/lang/Integer;Lcom/squareup/ui/login/OperationType;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 1393
    invoke-static {p0, p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushLoading(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/Operation;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static final withTemporarySessionData(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Lcom/squareup/ui/login/UnitsAndMerchants;Ljava/lang/Boolean;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 9

    const-string v0, "$this$withTemporarySessionData"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 1474
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    .line 1475
    new-instance v3, Lcom/squareup/account/SecretString;

    invoke-direct {v3, p1}, Lcom/squareup/account/SecretString;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1d

    const/4 v8, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    :cond_0
    move-object v0, p0

    if-eqz p2, :cond_1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1b

    const/4 v7, 0x0

    move-object v3, p2

    .line 1482
    invoke-static/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v0

    :cond_1
    move-object v1, v0

    if-eqz p3, :cond_2

    .line 1486
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/login/AuthenticatorState;->copy$default(Lcom/squareup/ui/login/AuthenticatorState;ZLcom/squareup/account/SecretString;Lcom/squareup/ui/login/UnitsAndMerchants;Lcom/squareup/ui/login/AuthenticatorStack;Lcom/squareup/ui/login/Operation;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object v1

    :cond_2
    return-object v1
.end method

.method public static synthetic withTemporarySessionData$default(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Lcom/squareup/ui/login/UnitsAndMerchants;Ljava/lang/Boolean;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 1

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    .line 1468
    move-object p1, v0

    check-cast p1, Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    .line 1469
    move-object p2, v0

    check-cast p2, Lcom/squareup/ui/login/UnitsAndMerchants;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    .line 1470
    move-object p3, v0

    check-cast p3, Ljava/lang/Boolean;

    :cond_2
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/ui/login/RealAuthenticatorKt;->withTemporarySessionData(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Lcom/squareup/ui/login/UnitsAndMerchants;Ljava/lang/Boolean;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p0

    return-object p0
.end method
