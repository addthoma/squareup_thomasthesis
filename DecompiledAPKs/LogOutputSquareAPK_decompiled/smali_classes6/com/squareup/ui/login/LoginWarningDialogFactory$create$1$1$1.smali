.class final Lcom/squareup/ui/login/LoginWarningDialogFactory$create$1$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "LoginWarningDialogFactory.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/LoginWarningDialogFactory$create$1$1;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/login/LoginWarningDialogFactory$create$1$1;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/LoginWarningDialogFactory$create$1$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/LoginWarningDialogFactory$create$1$1$1;->this$0:Lcom/squareup/ui/login/LoginWarningDialogFactory$create$1$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/LoginWarningDialogFactory$create$1$1$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component2()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 64
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorEvent$WarningDialogDismissed;

    iget-object v1, p0, Lcom/squareup/ui/login/LoginWarningDialogFactory$create$1$1$1;->this$0:Lcom/squareup/ui/login/LoginWarningDialogFactory$create$1$1;

    iget-object v1, v1, Lcom/squareup/ui/login/LoginWarningDialogFactory$create$1$1;->$screen:Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;

    invoke-virtual {v1}, Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning;->getWarning()Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/login/AuthenticatorEvent$WarningDialogDismissed;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$ShowWarning$WarningText;)V

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
