.class final Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$3;
.super Ljava/lang/Object;
.source "CreateAccountHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/CreateAccountHelper;->attemptCreateAccount(Lcom/squareup/server/account/CreateBody;)Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "nextStepResponse",
        "Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/login/CreateAccountHelper;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/CreateAccountHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$3;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;)V
    .locals 3

    .line 96
    instance-of v0, p1, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$Success;

    if-eqz v0, :cond_1

    .line 97
    check-cast p1, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$Success;

    invoke-virtual {p1}, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$Success;->getNextStep()Lcom/squareup/ui/login/CreateAccountHelper$NextStep;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/login/CreateAccountHelper$NextStep;->ACTIVATE:Lcom/squareup/ui/login/CreateAccountHelper$NextStep;

    if-ne p1, v0, :cond_0

    .line 98
    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$3;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-static {p1}, Lcom/squareup/ui/login/CreateAccountHelper;->access$getRunner$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->goToActivation()V

    goto :goto_0

    .line 100
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$3;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-static {p1}, Lcom/squareup/ui/login/CreateAccountHelper;->access$getRunner$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->goToPaymentActivity()V

    .line 102
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$3;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-virtual {p1}, Lcom/squareup/ui/login/CreateAccountHelper;->getProgressPresenter()Lcom/squareup/caller/ProgressAndFailurePresenter;

    move-result-object p1

    new-instance v0, Lcom/squareup/server/SimpleResponse;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/caller/ProgressAndFailurePresenter;->onSuccess(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 104
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$FailureMessage;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$3;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/login/CreateAccountHelper;->getProgressPresenter()Lcom/squareup/caller/ProgressAndFailurePresenter;

    move-result-object v0

    .line 105
    check-cast p1, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$FailureMessage;

    invoke-virtual {p1}, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$FailureMessage;->getMessage()Ljava/lang/String;

    move-result-object p1

    .line 104
    invoke-virtual {v0, v1, p1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->onFailure(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 107
    :cond_2
    instance-of v0, p1, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$FailureResponse;

    if-eqz v0, :cond_7

    .line 108
    check-cast p1, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$FailureResponse;

    invoke-virtual {p1}, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$FailureResponse;->getFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 110
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_3

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$3;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/ui/login/CreateAccountHelper;->access$parseClientError(Lcom/squareup/ui/login/CreateAccountHelper;Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$3;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/login/CreateAccountHelper;->getProgressPresenter()Lcom/squareup/caller/ProgressAndFailurePresenter;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/server/SimpleResponse;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/SimpleResponse;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->onFailure(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 114
    :cond_3
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$3;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/login/CreateAccountHelper;->getProgressPresenter()Lcom/squareup/caller/ProgressAndFailurePresenter;

    move-result-object v0

    .line 115
    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$3;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getResponse()Ljava/lang/Object;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/squareup/ui/login/CreateAccountHelper;->access$parseClientError(Lcom/squareup/ui/login/CreateAccountHelper;Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    .line 114
    invoke-virtual {v0, p1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->onClientError(Ljava/lang/Object;)V

    goto :goto_1

    .line 117
    :cond_4
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz v0, :cond_5

    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$3;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-virtual {p1}, Lcom/squareup/ui/login/CreateAccountHelper;->getProgressPresenter()Lcom/squareup/caller/ProgressAndFailurePresenter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->onServerError()V

    goto :goto_1

    .line 118
    :cond_5
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz v0, :cond_6

    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$3;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-virtual {p1}, Lcom/squareup/ui/login/CreateAccountHelper;->getProgressPresenter()Lcom/squareup/caller/ProgressAndFailurePresenter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->onNetworkError()V

    goto :goto_1

    .line 119
    :cond_6
    new-instance v0, Lkotlin/NotImplementedError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected response "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_7
    :goto_1
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/CreateAccountHelper$attemptCreateAccount$3;->accept(Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;)V

    return-void
.end method
