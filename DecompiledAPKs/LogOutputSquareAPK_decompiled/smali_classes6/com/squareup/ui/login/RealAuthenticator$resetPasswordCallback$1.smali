.class final Lcom/squareup/ui/login/RealAuthenticator$resetPasswordCallback$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAuthenticator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/RealAuthenticator;->resetPasswordCallback$impl_release(Ljava/lang/String;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "state",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $failureMessage:Lcom/squareup/receiving/FailureMessage;


# direct methods
.method constructor <init>(Lcom/squareup/receiving/FailureMessage;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/RealAuthenticator$resetPasswordCallback$1;->$failureMessage:Lcom/squareup/receiving/FailureMessage;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 3

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 889
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;

    iget-object v1, p0, Lcom/squareup/ui/login/RealAuthenticator$resetPasswordCallback$1;->$failureMessage:Lcom/squareup/receiving/FailureMessage;

    invoke-virtual {v1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/login/RealAuthenticator$resetPasswordCallback$1;->$failureMessage:Lcom/squareup/receiving/FailureMessage;

    invoke-virtual {v2}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen;

    .line 888
    invoke-static {p1, v0}, Lcom/squareup/ui/login/RealAuthenticatorKt;->pushScreen(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorScreen;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 157
    check-cast p1, Lcom/squareup/ui/login/AuthenticatorState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator$resetPasswordCallback$1;->invoke(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1
.end method
