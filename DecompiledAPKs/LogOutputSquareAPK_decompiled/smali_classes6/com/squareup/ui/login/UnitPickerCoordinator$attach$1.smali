.class final Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;
.super Lkotlin/jvm/internal/Lambda;
.source "UnitPickerCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/UnitPickerCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUnitPickerCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UnitPickerCoordinator.kt\ncom/squareup/ui/login/UnitPickerCoordinator$attach$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,91:1\n950#2:92\n*E\n*S KotlinDebug\n*F\n+ 1 UnitPickerCoordinator.kt\ncom/squareup/ui/login/UnitPickerCoordinator$attach$1\n*L\n58#1:92\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/login/UnitPickerCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/UnitPickerCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/UnitPickerCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;->$view:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component2()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 41
    iget-object v1, p0, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/UnitPickerCoordinator;

    .line 40
    iget-object v2, p0, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;->$view:Landroid/view/View;

    sget v3, Lcom/squareup/common/authenticatorviews/R$id;->row_container:I

    invoke-static {v2, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    .line 41
    check-cast v2, Landroid/widget/ListView;

    .line 42
    iget-object v3, p0, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/UnitPickerCoordinator;

    invoke-virtual {v3}, Lcom/squareup/ui/login/UnitPickerCoordinator;->getAdapter$authenticator_views_release()Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;

    move-result-object v3

    check-cast v3, Landroid/widget/ListAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 43
    new-instance v3, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1$$special$$inlined$also$lambda$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1$$special$$inlined$also$lambda$1;-><init>(Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v3, Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 41
    invoke-static {v1, v2}, Lcom/squareup/ui/login/UnitPickerCoordinator;->access$setContainer$p(Lcom/squareup/ui/login/UnitPickerCoordinator;Landroid/widget/ListView;)V

    .line 48
    iget-object v1, p0, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;->$view:Landroid/view/View;

    sget v2, Lcom/squareup/common/authenticatorviews/R$id;->stable_action_bar:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    const-string v2, "actionBar"

    .line 52
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    new-instance v2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 51
    new-instance v3, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1$2;

    invoke-direct {v3, p1}, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 52
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 54
    iget-object v1, p0, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;->$view:Landroid/view/View;

    new-instance v2, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1$3;

    invoke-direct {v2, p1}, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1$3;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-static {v1, v2}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 58
    iget-object p1, p0, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/UnitPickerCoordinator;

    invoke-virtual {p1}, Lcom/squareup/ui/login/UnitPickerCoordinator;->getAdapter$authenticator_views_release()Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;

    move-result-object p1

    .line 56
    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;->getUnitsAndMerchants()Lcom/squareup/ui/login/UnitsAndMerchants;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/login/UnitsAndMerchants;->getMerchantTokenToUnitList()Ljava/util/Map;

    move-result-object v1

    .line 57
    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;->getMerchantToken()Ljava/lang/String;

    move-result-object v0

    .line 56
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 57
    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    check-cast v0, Ljava/lang/Iterable;

    .line 92
    new-instance v1, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1$$special$$inlined$sortedBy$1;

    invoke-direct {v1}, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1$$special$$inlined$sortedBy$1;-><init>()V

    check-cast v1, Ljava/util/Comparator;

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;->setUnits(Ljava/util/List;)V

    return-void
.end method
