.class Lcom/squareup/ui/login/CreateAccountView$2;
.super Lcom/squareup/debounce/DebouncedChangeOnceTextWatcher;
.source "CreateAccountView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/CreateAccountView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/login/CreateAccountView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/CreateAccountView;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountView$2;->this$0:Lcom/squareup/ui/login/CreateAccountView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedChangeOnceTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnFirstTextChange(Landroid/text/Editable;)V
    .locals 0

    .line 83
    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountView$2;->this$0:Lcom/squareup/ui/login/CreateAccountView;

    iget-object p1, p1, Lcom/squareup/ui/login/CreateAccountView;->presenter:Lcom/squareup/ui/login/CreateAccountPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/login/CreateAccountPresenter;->onEmailAddressSelected()V

    return-void
.end method
