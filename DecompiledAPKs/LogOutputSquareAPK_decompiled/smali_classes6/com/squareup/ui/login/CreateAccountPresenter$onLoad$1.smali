.class final Lcom/squareup/ui/login/CreateAccountPresenter$onLoad$1;
.super Lkotlin/jvm/internal/Lambda;
.source "CreateAccountPresenter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/CreateAccountPresenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lio/reactivex/disposables/Disposable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/disposables/Disposable;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/login/CreateAccountPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/CreateAccountPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountPresenter$onLoad$1;->this$0:Lcom/squareup/ui/login/CreateAccountPresenter;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter$onLoad$1;->this$0:Lcom/squareup/ui/login/CreateAccountPresenter;

    invoke-static {v0}, Lcom/squareup/ui/login/CreateAccountPresenter;->access$getCountryGuesser$p(Lcom/squareup/ui/login/CreateAccountPresenter;)Lcom/squareup/location/CountryCodeGuesser;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/location/CountryCodeGuesser;->getCountryCodeGuess()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/login/CreateAccountPresenter$onLoad$1$1;

    iget-object v2, p0, Lcom/squareup/ui/login/CreateAccountPresenter$onLoad$1;->this$0:Lcom/squareup/ui/login/CreateAccountPresenter;

    invoke-direct {v1, v2}, Lcom/squareup/ui/login/CreateAccountPresenter$onLoad$1$1;-><init>(Lcom/squareup/ui/login/CreateAccountPresenter;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/login/CreateAccountPresenter$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/login/CreateAccountPresenter$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "countryGuesser.countryCo\u2026ribe(::setGuessedCountry)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 55
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountPresenter$onLoad$1;->invoke()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method
