.class public final Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1$$special$$inlined$onClickDebounced$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 EnrollGoogleAuthQrCoordinator.kt\ncom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1\n*L\n1#1,1322:1\n64#2:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1$$special$$inlined$onClickDebounced$1;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Landroid/widget/TextView;

    .line 1323
    iget-object p1, p0, Lcom/squareup/ui/login/EnrollGoogleAuthQrCoordinator$attach$1$$special$$inlined$onClickDebounced$1;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v0, Lcom/squareup/ui/login/AuthenticatorEvent$ShowGoogleAuthCodeInsteadOfQr;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorEvent$ShowGoogleAuthCodeInsteadOfQr;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
