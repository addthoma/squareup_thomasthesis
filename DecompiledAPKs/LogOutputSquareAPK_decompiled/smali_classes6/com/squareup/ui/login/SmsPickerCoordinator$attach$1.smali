.class final Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SmsPickerCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/SmsPickerCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/login/SmsPickerCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/SmsPickerCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/SmsPickerCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1;->$view:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component2()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 52
    iget-object v1, p0, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/SmsPickerCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/login/SmsPickerCoordinator;->access$getActionBar$p(Lcom/squareup/ui/login/SmsPickerCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    const-string v2, "actionBar.presenter"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 50
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v4, p0, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/squareup/common/authenticatorviews/R$string;->back:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 51
    new-instance v3, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1$1;

    invoke-direct {v3, p1}, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 52
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 54
    iget-object v1, p0, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/SmsPickerCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/login/SmsPickerCoordinator;->access$getSmsTwoFactorDetailsContainer$p(Lcom/squareup/ui/login/SmsPickerCoordinator;)Landroid/widget/ListView;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1$2;

    invoke-direct {v2, v0, p1}, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1$2;-><init>(Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 59
    iget-object v1, p0, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/SmsPickerCoordinator;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;->getSmsTwoFactorDetails()Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/squareup/ui/login/SmsPickerCoordinator;->access$setSmsTwoFactorDetails$p(Lcom/squareup/ui/login/SmsPickerCoordinator;Ljava/util/List;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1;->$view:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1$3;

    invoke-direct {v1, p1}, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1$3;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
