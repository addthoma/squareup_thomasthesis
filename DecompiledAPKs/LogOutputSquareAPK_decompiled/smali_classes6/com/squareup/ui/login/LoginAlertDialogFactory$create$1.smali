.class final Lcom/squareup/ui/login/LoginAlertDialogFactory$create$1;
.super Ljava/lang/Object;
.source "LoginAlertDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/LoginAlertDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/AlertDialog;",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;

.field final synthetic $inputs:Lio/reactivex/Observable;

.field final synthetic this$0:Lcom/squareup/ui/login/LoginAlertDialogFactory;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/LoginAlertDialogFactory;Landroid/content/Context;Lio/reactivex/Observable;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory$create$1;->this$0:Lcom/squareup/ui/login/LoginAlertDialogFactory;

    iput-object p2, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory$create$1;->$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory$create$1;->$inputs:Lio/reactivex/Observable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)",
            "Landroid/app/AlertDialog;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component1()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory$create$1;->this$0:Lcom/squareup/ui/login/LoginAlertDialogFactory;

    iget-object v1, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory$create$1;->$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory$create$1;->$inputs:Lio/reactivex/Observable;

    const-string v3, "inputs"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, p1, v2}, Lcom/squareup/ui/login/LoginAlertDialogFactory;->access$create(Lcom/squareup/ui/login/LoginAlertDialogFactory;Landroid/content/Context;Lcom/squareup/ui/login/AuthenticatorScreen$ShowLoginAlert;Lio/reactivex/Observable;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 54
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/LoginAlertDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
