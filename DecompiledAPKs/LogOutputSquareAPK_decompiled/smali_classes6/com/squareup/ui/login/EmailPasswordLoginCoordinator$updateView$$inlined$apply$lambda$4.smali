.class final Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateView$$inlined$apply$lambda$4;
.super Ljava/lang/Object;
.source "EmailPasswordLoginCoordinator.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->updateView(Landroid/view/View;Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;Lcom/squareup/workflow/legacy/WorkflowInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "run",
        "com/squareup/ui/login/EmailPasswordLoginCoordinator$updateView$1$4"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen$inlined:Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

.field final synthetic $workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

.field final synthetic this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateView$$inlined$apply$lambda$4;->this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateView$$inlined$apply$lambda$4;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    iput-object p3, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateView$$inlined$apply$lambda$4;->$screen$inlined:Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$updateView$$inlined$apply$lambda$4;->$workflow$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v1, Lcom/squareup/ui/login/AuthenticatorEvent$SwitchLoginPromptToDeviceCode;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorEvent$SwitchLoginPromptToDeviceCode;

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
