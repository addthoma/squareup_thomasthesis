.class final Lcom/squareup/ui/login/CreateAccountPresenter$EmailCountEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "CreateAccountPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/CreateAccountPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "EmailCountEvent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/ui/login/CreateAccountPresenter$EmailCountEvent;",
        "Lcom/squareup/analytics/event/v1/ActionEvent;",
        "()V",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 280
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->AVAILABLE_EMAILS:Lcom/squareup/analytics/RegisterActionName;

    check-cast v0, Lcom/squareup/analytics/EventNamedAction;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    return-void
.end method
