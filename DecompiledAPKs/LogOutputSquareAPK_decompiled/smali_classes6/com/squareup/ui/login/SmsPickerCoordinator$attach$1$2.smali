.class final Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1$2;
.super Ljava/lang/Object;
.source "SmsPickerCoordinator.kt"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\t\n\u0000\u0010\u0000\u001a\u00020\u00012\u0016\u0010\u0002\u001a\u0012\u0012\u0002\u0008\u0003 \u0004*\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00030\u00032\u000e\u0010\u0005\u001a\n \u0004*\u0004\u0018\u00010\u00060\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\n\u00a2\u0006\u0002\u0008\u000b"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/widget/AdapterView;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "Landroid/view/View;",
        "position",
        "",
        "<anonymous parameter 3>",
        "",
        "onItemClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;

.field final synthetic $workflow:Lcom/squareup/workflow/legacy/WorkflowInput;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1$2;->$screen:Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;

    iput-object p2, p0, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1$2;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 55
    iget-object p1, p0, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1$2;->$screen:Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;->getSmsTwoFactorDetails()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    .line 56
    iget-object p2, p0, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1$2;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance p3, Lcom/squareup/ui/login/AuthenticatorEvent$PromptToVerifySmsCode;

    sget-object p4, Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;->LOGIN:Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;

    invoke-direct {p3, p1, p4}, Lcom/squareup/ui/login/AuthenticatorEvent$PromptToVerifySmsCode;-><init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;Lcom/squareup/ui/login/TwoFactorVerificationEndpoint;)V

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
