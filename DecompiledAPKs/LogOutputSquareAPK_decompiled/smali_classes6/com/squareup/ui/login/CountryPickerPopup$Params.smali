.class public Lcom/squareup/ui/login/CountryPickerPopup$Params;
.super Ljava/lang/Object;
.source "CountryPickerPopup.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/CountryPickerPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Params"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/login/CountryPickerPopup$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final initialValue:Lcom/squareup/CountryCode;

.field private final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 70
    new-instance v0, Lcom/squareup/ui/login/CountryPickerPopup$Params$1;

    invoke-direct {v0}, Lcom/squareup/ui/login/CountryPickerPopup$Params$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/login/CountryPickerPopup$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/squareup/CountryCode;)V
    .locals 0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/login/CountryPickerPopup$Params;->title:Ljava/lang/String;

    .line 58
    iput-object p2, p0, Lcom/squareup/ui/login/CountryPickerPopup$Params;->initialValue:Lcom/squareup/CountryCode;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/login/CountryPickerPopup$Params;)Lcom/squareup/CountryCode;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/ui/login/CountryPickerPopup$Params;->initialValue:Lcom/squareup/CountryCode;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/login/CountryPickerPopup$Params;)Ljava/lang/String;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/ui/login/CountryPickerPopup$Params;->title:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 66
    iget-object p2, p0, Lcom/squareup/ui/login/CountryPickerPopup$Params;->title:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 67
    iget-object p2, p0, Lcom/squareup/ui/login/CountryPickerPopup$Params;->initialValue:Lcom/squareup/CountryCode;

    invoke-virtual {p2}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
