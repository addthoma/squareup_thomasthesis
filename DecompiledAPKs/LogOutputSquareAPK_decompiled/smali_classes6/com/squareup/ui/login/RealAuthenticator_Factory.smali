.class public final Lcom/squareup/ui/login/RealAuthenticator_Factory;
.super Ljava/lang/Object;
.source "RealAuthenticator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/login/RealAuthenticator;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/TrustedDeviceDetailsStore;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/AuthenticationServiceEndpoint;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/location/CountryGuesser;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/SupportsDeviceCodeLogin;",
            ">;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/accountservice/AppAccountCache;",
            ">;"
        }
    .end annotation
.end field

.field private final arg9Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/safetynetrecaptcha/SafetyNetRecaptchaVerifier;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/TrustedDeviceDetailsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/AuthenticationServiceEndpoint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/location/CountryGuesser;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/SupportsDeviceCodeLogin;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/accountservice/AppAccountCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/safetynetrecaptcha/SafetyNetRecaptchaVerifier;",
            ">;)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 46
    iput-object p2, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 47
    iput-object p3, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 48
    iput-object p4, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 49
    iput-object p5, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 50
    iput-object p6, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 51
    iput-object p7, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 52
    iput-object p8, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg7Provider:Ljavax/inject/Provider;

    .line 53
    iput-object p9, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg8Provider:Ljavax/inject/Provider;

    .line 54
    iput-object p10, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg9Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/login/RealAuthenticator_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/TrustedDeviceDetailsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/AuthenticationServiceEndpoint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/location/CountryGuesser;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/SupportsDeviceCodeLogin;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/accountservice/AppAccountCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/safetynetrecaptcha/SafetyNetRecaptchaVerifier;",
            ">;)",
            "Lcom/squareup/ui/login/RealAuthenticator_Factory;"
        }
    .end annotation

    .line 68
    new-instance v11, Lcom/squareup/ui/login/RealAuthenticator_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/login/RealAuthenticator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/ui/login/TrustedDeviceDetailsStore;Lcom/squareup/ui/login/AuthenticationServiceEndpoint;Lio/reactivex/Scheduler;Lcom/squareup/badbus/BadBus;Lcom/squareup/location/CountryGuesser;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/util/ToastFactory;Lcom/squareup/ui/login/SupportsDeviceCodeLogin;Lcom/squareup/account/accountservice/AppAccountCache;Lcom/squareup/safetynetrecaptcha/SafetyNetRecaptchaVerifier;)Lcom/squareup/ui/login/RealAuthenticator;
    .locals 12

    .line 75
    new-instance v11, Lcom/squareup/ui/login/RealAuthenticator;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/login/RealAuthenticator;-><init>(Lcom/squareup/ui/login/TrustedDeviceDetailsStore;Lcom/squareup/ui/login/AuthenticationServiceEndpoint;Lio/reactivex/Scheduler;Lcom/squareup/badbus/BadBus;Lcom/squareup/location/CountryGuesser;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/util/ToastFactory;Lcom/squareup/ui/login/SupportsDeviceCodeLogin;Lcom/squareup/account/accountservice/AppAccountCache;Lcom/squareup/safetynetrecaptcha/SafetyNetRecaptchaVerifier;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/ui/login/RealAuthenticator;
    .locals 11

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/login/TrustedDeviceDetailsStore;

    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/login/AuthenticationServiceEndpoint;

    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/badbus/BadBus;

    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/location/CountryGuesser;

    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/receiving/FailureMessageFactory;

    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/ToastFactory;

    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/login/SupportsDeviceCodeLogin;

    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg8Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/account/accountservice/AppAccountCache;

    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator_Factory;->arg9Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/safetynetrecaptcha/SafetyNetRecaptchaVerifier;

    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/login/RealAuthenticator_Factory;->newInstance(Lcom/squareup/ui/login/TrustedDeviceDetailsStore;Lcom/squareup/ui/login/AuthenticationServiceEndpoint;Lio/reactivex/Scheduler;Lcom/squareup/badbus/BadBus;Lcom/squareup/location/CountryGuesser;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/util/ToastFactory;Lcom/squareup/ui/login/SupportsDeviceCodeLogin;Lcom/squareup/account/accountservice/AppAccountCache;Lcom/squareup/safetynetrecaptcha/SafetyNetRecaptchaVerifier;)Lcom/squareup/ui/login/RealAuthenticator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/login/RealAuthenticator_Factory;->get()Lcom/squareup/ui/login/RealAuthenticator;

    move-result-object v0

    return-object v0
.end method
