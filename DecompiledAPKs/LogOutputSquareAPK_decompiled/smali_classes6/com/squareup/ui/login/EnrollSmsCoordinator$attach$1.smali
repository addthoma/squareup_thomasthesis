.class final Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EnrollSmsCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/EnrollSmsCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEnrollSmsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EnrollSmsCoordinator.kt\ncom/squareup/ui/login/EnrollSmsCoordinator$attach$1\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,142:1\n1103#2,7:143\n*E\n*S KotlinDebug\n*F\n+ 1 EnrollSmsCoordinator.kt\ncom/squareup/ui/login/EnrollSmsCoordinator$attach$1\n*L\n85#1,7:143\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/login/EnrollSmsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/EnrollSmsCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollSmsCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;->$view:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component2()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 68
    new-instance v1, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$1;-><init>(Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 83
    iget-object v2, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollSmsCoordinator;

    invoke-static {v2}, Lcom/squareup/ui/login/EnrollSmsCoordinator;->access$getActionBar$p(Lcom/squareup/ui/login/EnrollSmsCoordinator;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v2

    .line 78
    new-instance v3, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 79
    iget-object v4, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollSmsCoordinator;

    invoke-static {v4}, Lcom/squareup/ui/login/EnrollSmsCoordinator;->access$getRes$p(Lcom/squareup/ui/login/EnrollSmsCoordinator;)Lcom/squareup/util/Res;

    move-result-object v4

    sget v5, Lcom/squareup/common/strings/R$string;->next:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v3

    .line 80
    new-instance v4, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$2;

    invoke-direct {v4, v1}, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$2;-><init>(Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$1;)V

    check-cast v4, Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v3

    .line 81
    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v5, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollSmsCoordinator;

    invoke-static {v5}, Lcom/squareup/ui/login/EnrollSmsCoordinator;->access$getRes$p(Lcom/squareup/ui/login/EnrollSmsCoordinator;)Lcom/squareup/util/Res;

    move-result-object v5

    sget v6, Lcom/squareup/common/authenticatorviews/R$string;->back:I

    invoke-interface {v5, v6}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v3, v4, v5}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v3

    .line 82
    new-instance v4, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$3;

    invoke-direct {v4, p1}, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$3;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v4, Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v3

    .line 83
    invoke-virtual {v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 85
    iget-object v2, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollSmsCoordinator;

    invoke-static {v2}, Lcom/squareup/ui/login/EnrollSmsCoordinator;->access$getSkipEnrollmentLink$p(Lcom/squareup/ui/login/EnrollSmsCoordinator;)Landroid/widget/TextView;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 143
    new-instance v3, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$$special$$inlined$onClickDebounced$1;

    invoke-direct {v3, p1}, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$$special$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    iget-object v2, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollSmsCoordinator;

    invoke-static {v2}, Lcom/squareup/ui/login/EnrollSmsCoordinator;->access$getPhoneNumberField$p(Lcom/squareup/ui/login/EnrollSmsCoordinator;)Lcom/squareup/ui/XableEditText;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$5;

    invoke-direct {v3, v1}, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$5;-><init>(Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$1;)V

    check-cast v3, Lcom/squareup/debounce/DebouncedOnEditorActionListener;

    invoke-virtual {v2, v3}, Lcom/squareup/ui/XableEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 95
    iget-object v1, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;->$view:Landroid/view/View;

    new-instance v2, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$6;

    invoke-direct {v2, p1}, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$6;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-static {v1, v2}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 97
    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;->getCanSkipEnroll()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollSmsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/login/EnrollSmsCoordinator;->access$getSkipEnrollmentLink$p(Lcom/squareup/ui/login/EnrollSmsCoordinator;)Landroid/widget/TextView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void
.end method
