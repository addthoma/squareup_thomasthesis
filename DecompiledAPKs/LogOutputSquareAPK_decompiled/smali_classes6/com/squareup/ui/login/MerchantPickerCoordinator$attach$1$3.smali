.class final Lcom/squareup/ui/login/MerchantPickerCoordinator$attach$1$3;
.super Lkotlin/jvm/internal/Lambda;
.source "MerchantPickerCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/MerchantPickerCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMerchantPickerCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MerchantPickerCoordinator.kt\ncom/squareup/ui/login/MerchantPickerCoordinator$attach$1$3\n+ 2 AuthenticatorEvent.kt\ncom/squareup/ui/login/AuthenticatorEventKt\n*L\n1#1,86:1\n200#2:87\n*E\n*S KotlinDebug\n*F\n+ 1 MerchantPickerCoordinator.kt\ncom/squareup/ui/login/MerchantPickerCoordinator$attach$1$3\n*L\n60#1:87\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflow:Lcom/squareup/workflow/legacy/WorkflowInput;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/MerchantPickerCoordinator$attach$1$3;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/ui/login/MerchantPickerCoordinator$attach$1$3;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/login/MerchantPickerCoordinator$attach$1$3;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 87
    new-instance v1, Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;

    const-class v2, Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;

    invoke-direct {v1, v2}, Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;-><init>(Ljava/lang/Class;)V

    .line 60
    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
