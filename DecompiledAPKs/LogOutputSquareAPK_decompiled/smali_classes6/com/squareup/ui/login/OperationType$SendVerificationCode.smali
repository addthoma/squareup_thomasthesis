.class public final Lcom/squareup/ui/login/OperationType$SendVerificationCode;
.super Lcom/squareup/ui/login/OperationType;
.source "Operation.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/OperationType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SendVerificationCode"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\r\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00072\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/ui/login/OperationType$SendVerificationCode;",
        "Lcom/squareup/ui/login/OperationType;",
        "sessionToken",
        "Lcom/squareup/account/SecretString;",
        "twoFactorDetails",
        "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
        "throttle",
        "",
        "(Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Z)V",
        "getSessionToken",
        "()Lcom/squareup/account/SecretString;",
        "getThrottle",
        "()Z",
        "getTwoFactorDetails",
        "()Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final sessionToken:Lcom/squareup/account/SecretString;

.field private final throttle:Z

.field private final twoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;


# direct methods
.method public constructor <init>(Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Z)V
    .locals 1

    const-string v0, "sessionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "twoFactorDetails"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 52
    invoke-direct {p0, v0}, Lcom/squareup/ui/login/OperationType;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->sessionToken:Lcom/squareup/account/SecretString;

    iput-object p2, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->twoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    iput-boolean p3, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->throttle:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/login/OperationType$SendVerificationCode;Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;ZILjava/lang/Object;)Lcom/squareup/ui/login/OperationType$SendVerificationCode;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->sessionToken:Lcom/squareup/account/SecretString;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->twoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->throttle:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->copy(Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Z)Lcom/squareup/ui/login/OperationType$SendVerificationCode;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/account/SecretString;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->sessionToken:Lcom/squareup/account/SecretString;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/multipass/common/TwoFactorDetails;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->twoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->throttle:Z

    return v0
.end method

.method public final copy(Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Z)Lcom/squareup/ui/login/OperationType$SendVerificationCode;
    .locals 1

    const-string v0, "sessionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "twoFactorDetails"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/login/OperationType$SendVerificationCode;-><init>(Lcom/squareup/account/SecretString;Lcom/squareup/protos/multipass/common/TwoFactorDetails;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/login/OperationType$SendVerificationCode;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/login/OperationType$SendVerificationCode;

    iget-object v0, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->sessionToken:Lcom/squareup/account/SecretString;

    iget-object v1, p1, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->sessionToken:Lcom/squareup/account/SecretString;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->twoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    iget-object v1, p1, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->twoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->throttle:Z

    iget-boolean p1, p1, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->throttle:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getSessionToken()Lcom/squareup/account/SecretString;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->sessionToken:Lcom/squareup/account/SecretString;

    return-object v0
.end method

.method public final getThrottle()Z
    .locals 1

    .line 51
    iget-boolean v0, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->throttle:Z

    return v0
.end method

.method public final getTwoFactorDetails()Lcom/squareup/protos/multipass/common/TwoFactorDetails;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->twoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->sessionToken:Lcom/squareup/account/SecretString;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->twoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->throttle:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SendVerificationCode(sessionToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->sessionToken:Lcom/squareup/account/SecretString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", twoFactorDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->twoFactorDetails:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", throttle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/login/OperationType$SendVerificationCode;->throttle:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
