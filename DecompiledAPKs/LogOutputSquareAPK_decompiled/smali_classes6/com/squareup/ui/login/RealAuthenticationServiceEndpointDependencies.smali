.class public interface abstract Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;
.super Ljava/lang/Object;
.source "AuthenticationServiceEndpoint.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001:\u0001\u0012R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u0012\u0010\n\u001a\u00020\u000bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u0012\u0010\u000e\u001a\u00020\u000fX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies;",
        "",
        "authenticationService",
        "Lcom/squareup/server/account/AuthenticationService;",
        "getAuthenticationService",
        "()Lcom/squareup/server/account/AuthenticationService;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "getMainScheduler",
        "()Lio/reactivex/Scheduler;",
        "passwordService",
        "Lcom/squareup/server/account/PasswordService;",
        "getPasswordService",
        "()Lcom/squareup/server/account/PasswordService;",
        "res",
        "Lcom/squareup/util/Res;",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "Implementation",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getAuthenticationService()Lcom/squareup/server/account/AuthenticationService;
.end method

.method public abstract getMainScheduler()Lio/reactivex/Scheduler;
.end method

.method public abstract getPasswordService()Lcom/squareup/server/account/PasswordService;
.end method

.method public abstract getRes()Lcom/squareup/util/Res;
.end method
