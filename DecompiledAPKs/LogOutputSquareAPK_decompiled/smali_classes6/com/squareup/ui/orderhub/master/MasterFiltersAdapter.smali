.class public final Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "MasterFiltersAdapter.kt"

# interfaces
.implements Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;,
        Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$HeaderViewHolder;,
        Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$ViewHolder;,
        Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$ViewHolder;",
        ">;",
        "Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter<",
        "Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$HeaderViewHolder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMasterFiltersAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MasterFiltersAdapter.kt\ncom/squareup/ui/orderhub/master/MasterFiltersAdapter\n+ 2 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,132:1\n151#2,2:133\n*E\n*S KotlinDebug\n*F\n+ 1 MasterFiltersAdapter.kt\ncom/squareup/ui/orderhub/master/MasterFiltersAdapter\n*L\n32#1,2:133\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0008\u0000\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0008\u0012\u0004\u0012\u00020\u00040\u0003:\u0004\u001e\u001f !B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0008\u0010\u0012\u001a\u00020\u0011H\u0016J\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0018\u0010\u0016\u001a\u00020\u00142\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0018\u0010\u001b\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u0011H\u0016J\u000e\u0010\u001d\u001a\u00020\u00142\u0006\u0010\u000c\u001a\u00020\rR\u001e\u0010\u0008\u001a\u0012\u0012\u0004\u0012\u00020\n0\tj\u0008\u0012\u0004\u0012\u00020\n`\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter;",
        "Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$ViewHolder;",
        "Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;",
        "Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$HeaderViewHolder;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;)V",
        "filterList",
        "Ljava/util/ArrayList;",
        "Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;",
        "Lkotlin/collections/ArrayList;",
        "screen",
        "Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;",
        "getHeaderId",
        "",
        "position",
        "",
        "getItemCount",
        "onBindHeaderViewHolder",
        "",
        "viewHolder",
        "onBindViewHolder",
        "holder",
        "onCreateHeaderViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "onCreateViewHolder",
        "viewType",
        "setData",
        "FilterRow",
        "FilterViewHolder",
        "HeaderViewHolder",
        "ViewHolder",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final filterList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private screen:Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->res:Lcom/squareup/util/Res;

    .line 24
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->filterList:Ljava/util/ArrayList;

    return-void
.end method

.method public static final synthetic access$getFilterList$p(Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;)Ljava/util/ArrayList;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->filterList:Ljava/util/ArrayList;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;)Lcom/squareup/util/Res;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getScreen$p(Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;)Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;
    .locals 1

    .line 20
    iget-object p0, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->screen:Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;

    if-nez p0, :cond_0

    const-string v0, "screen"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setScreen$p(Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;)V
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->screen:Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;

    return-void
.end method


# virtual methods
.method public getHeaderId(I)J
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->filterList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;->getGroupId()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemCount()I
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->filterList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindHeaderViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$HeaderViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->onBindHeaderViewHolder(Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$HeaderViewHolder;I)V

    return-void
.end method

.method public onBindHeaderViewHolder(Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$HeaderViewHolder;I)V
    .locals 1

    const-string v0, "viewHolder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->filterList:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;->getFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object p2

    .line 83
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$HeaderViewHolder;->getTextView()Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->res:Lcom/squareup/util/Res;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/orderhub/master/Filter;->getGroupName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->onBindViewHolder(Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$ViewHolder;I)V
    .locals 1

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    instance-of v0, p1, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;->bind(I)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onCreateHeaderViewHolder(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 20
    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->onCreateHeaderViewHolder(Landroid/view/ViewGroup;)Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$HeaderViewHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateHeaderViewHolder(Landroid/view/ViewGroup;)Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$HeaderViewHolder;
    .locals 3

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 74
    sget v1, Lcom/squareup/pos/container/R$layout;->applet_header_sidebar:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 75
    new-instance v0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$HeaderViewHolder;

    const-string v1, "inflate"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$HeaderViewHolder;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 20
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$ViewHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$ViewHolder;
    .locals 2

    const-string p2, "parent"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    .line 55
    sget v0, Lcom/squareup/orderhub/applet/R$layout;->orderhub_master_row:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 56
    new-instance p2, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;

    const-string v0, "nohoRow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterViewHolder;-><init>(Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;Landroid/view/View;)V

    check-cast p2, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$ViewHolder;

    return-object p2
.end method

.method public final setData(Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;)V
    .locals 7

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->screen:Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->filterList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 32
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;->getData()Lcom/squareup/ui/orderhub/MasterState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/MasterState;->getFiltersAndOpenOrderCounts()Ljava/util/Map;

    move-result-object v0

    .line 133
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/orderhub/master/Filter;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    .line 33
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;->getData()Lcom/squareup/ui/orderhub/MasterState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/MasterState;->getSelectedFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    .line 34
    iget-object v4, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->filterList:Ljava/util/ArrayList;

    .line 35
    new-instance v5, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;

    .line 36
    invoke-static {v2}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapterKt;->access$getGroupPosition$p(Lcom/squareup/ui/orderhub/master/Filter;)I

    move-result v6

    .line 35
    invoke-direct {v5, v6, v2, v1, v3}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;-><init>(ILcom/squareup/ui/orderhub/master/Filter;IZ)V

    .line 34
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 43
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;->notifyDataSetChanged()V

    return-void
.end method
