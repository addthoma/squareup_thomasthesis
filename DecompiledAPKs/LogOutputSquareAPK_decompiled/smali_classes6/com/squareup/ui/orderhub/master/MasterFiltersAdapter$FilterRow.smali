.class final Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;
.super Ljava/lang/Object;
.source "MasterFiltersAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "FilterRow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0008\u0008\u0002\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\r\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;",
        "",
        "groupId",
        "",
        "filter",
        "Lcom/squareup/ui/orderhub/master/Filter;",
        "orderCount",
        "isSelected",
        "",
        "(ILcom/squareup/ui/orderhub/master/Filter;IZ)V",
        "getFilter",
        "()Lcom/squareup/ui/orderhub/master/Filter;",
        "getGroupId",
        "()I",
        "()Z",
        "getOrderCount",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final filter:Lcom/squareup/ui/orderhub/master/Filter;

.field private final groupId:I

.field private final isSelected:Z

.field private final orderCount:I


# direct methods
.method public constructor <init>(ILcom/squareup/ui/orderhub/master/Filter;IZ)V
    .locals 1

    const-string v0, "filter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;->groupId:I

    iput-object p2, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;->filter:Lcom/squareup/ui/orderhub/master/Filter;

    iput p3, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;->orderCount:I

    iput-boolean p4, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;->isSelected:Z

    return-void
.end method


# virtual methods
.method public final getFilter()Lcom/squareup/ui/orderhub/master/Filter;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;->filter:Lcom/squareup/ui/orderhub/master/Filter;

    return-object v0
.end method

.method public final getGroupId()I
    .locals 1

    .line 115
    iget v0, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;->groupId:I

    return v0
.end method

.method public final getOrderCount()I
    .locals 1

    .line 117
    iget v0, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;->orderCount:I

    return v0
.end method

.method public final isSelected()Z
    .locals 1

    .line 118
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapter$FilterRow;->isSelected:Z

    return v0
.end method
