.class public final Lcom/squareup/ui/orderhub/master/FilterKt;
.super Ljava/lang/Object;
.source "Filter.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFilter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Filter.kt\ncom/squareup/ui/orderhub/master/FilterKt\n*L\n1#1,283:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\u001a\u001a\u0010\u0015\u001a\u00020\u0016*\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028CX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00058CX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0006\"\u0018\u0010\u0007\u001a\u00020\u0001*\u00020\u00058CX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\u0006\"\u001e\u0010\t\u001a\u00020\n*\u00020\u000b8FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u000c\u0010\r\u001a\u0004\u0008\u000e\u0010\u000f\"\u001e\u0010\u0010\u001a\u00020\u0011*\u00020\u000b8FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u0012\u0010\r\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u0006\u001c"
    }
    d2 = {
        "nameRes",
        "",
        "Lcom/squareup/orders/model/Order$FulfillmentType;",
        "getNameRes",
        "(Lcom/squareup/orders/model/Order$FulfillmentType;)I",
        "Lcom/squareup/protos/client/orders/OrderGroup;",
        "(Lcom/squareup/protos/client/orders/OrderGroup;)I",
        "shortNameRes",
        "getShortNameRes",
        "sourceFilter",
        "Lcom/squareup/ui/orderhub/master/Filter$Source;",
        "Lcom/squareup/orders/model/Order;",
        "sourceFilter$annotations",
        "(Lcom/squareup/orders/model/Order;)V",
        "getSourceFilter",
        "(Lcom/squareup/orders/model/Order;)Lcom/squareup/ui/orderhub/master/Filter$Source;",
        "typeFilter",
        "Lcom/squareup/ui/orderhub/master/Filter$Type;",
        "typeFilter$annotations",
        "getTypeFilter",
        "(Lcom/squareup/orders/model/Order;)Lcom/squareup/ui/orderhub/master/Filter$Type;",
        "getSearchQuery",
        "Lcom/squareup/ordermanagerdata/SearchQuery;",
        "Lcom/squareup/ui/orderhub/master/Filter;",
        "searchTerm",
        "",
        "searchRangeMillis",
        "",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getNameRes$p(Lcom/squareup/orders/model/Order$FulfillmentType;)I
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/orderhub/master/FilterKt;->getNameRes(Lcom/squareup/orders/model/Order$FulfillmentType;)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getNameRes$p(Lcom/squareup/protos/client/orders/OrderGroup;)I
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/orderhub/master/FilterKt;->getNameRes(Lcom/squareup/protos/client/orders/OrderGroup;)I

    move-result p0

    return p0
.end method

.method public static final synthetic access$getShortNameRes$p(Lcom/squareup/protos/client/orders/OrderGroup;)I
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/orderhub/master/FilterKt;->getShortNameRes(Lcom/squareup/protos/client/orders/OrderGroup;)I

    move-result p0

    return p0
.end method

.method private static final getNameRes(Lcom/squareup/orders/model/Order$FulfillmentType;)I
    .locals 2

    .line 245
    sget-object v0, Lcom/squareup/ui/orderhub/master/FilterKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 252
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unsupported fulfillment type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 251
    :pswitch_1
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_filter_value_type_shipment:I

    goto :goto_0

    .line 250
    :pswitch_2
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_filter_value_type_pickup:I

    goto :goto_0

    .line 249
    :pswitch_3
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_filter_value_type_managed_delivery:I

    goto :goto_0

    .line 248
    :pswitch_4
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_filter_value_type_delivery:I

    goto :goto_0

    .line 247
    :pswitch_5
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_filter_value_type_digital:I

    goto :goto_0

    .line 246
    :pswitch_6
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_filter_value_type_custom:I

    :goto_0
    return p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static final getNameRes(Lcom/squareup/protos/client/orders/OrderGroup;)I
    .locals 2

    .line 262
    sget-object v0, Lcom/squareup/ui/orderhub/master/FilterKt$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/OrderGroup;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 265
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_filter_value_status_upcoming:I

    goto :goto_0

    .line 266
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected order group "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 264
    :cond_1
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_filter_value_status_completed:I

    goto :goto_0

    .line 263
    :cond_2
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_filter_value_status_active:I

    :goto_0
    return p0
.end method

.method public static final getSearchQuery(Lcom/squareup/ui/orderhub/master/Filter;Ljava/lang/String;J)Lcom/squareup/ordermanagerdata/SearchQuery;
    .locals 8

    const-string v0, "$this$getSearchQuery"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchTerm"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    new-instance v0, Lcom/squareup/ordermanagerdata/SearchQuery;

    .line 218
    instance-of v1, p0, Lcom/squareup/ui/orderhub/master/Filter$Status;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v1, v2

    goto :goto_0

    :cond_0
    move-object v1, p0

    :goto_0
    check-cast v1, Lcom/squareup/ui/orderhub/master/Filter$Status;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/master/Filter$Status;->getOrderGroup()Lcom/squareup/protos/client/orders/OrderGroup;

    move-result-object v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    sget-object v1, Lcom/squareup/protos/client/orders/OrderGroup;->ACTIVE:Lcom/squareup/protos/client/orders/OrderGroup;

    :goto_1
    move-object v5, v1

    .line 219
    instance-of v1, p0, Lcom/squareup/ui/orderhub/master/Filter$Type;

    if-nez v1, :cond_2

    move-object v1, v2

    goto :goto_2

    :cond_2
    move-object v1, p0

    :goto_2
    check-cast v1, Lcom/squareup/ui/orderhub/master/Filter$Type;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/master/Filter$Type;->getType()Lcom/squareup/orders/model/Order$FulfillmentType;

    move-result-object v1

    move-object v6, v1

    goto :goto_3

    :cond_3
    move-object v6, v2

    .line 220
    :goto_3
    instance-of v1, p0, Lcom/squareup/ui/orderhub/master/Filter$Source;

    if-nez v1, :cond_4

    move-object p0, v2

    :cond_4
    check-cast p0, Lcom/squareup/ui/orderhub/master/Filter$Source;

    if-eqz p0, :cond_5

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/master/Filter$Source;->getChannel()Lcom/squareup/protos/client/orders/Channel;

    move-result-object p0

    move-object v7, p0

    goto :goto_4

    :cond_5
    move-object v7, v2

    :goto_4
    move-object v1, v0

    move-object v2, p1

    move-wide v3, p2

    .line 215
    invoke-direct/range {v1 .. v7}, Lcom/squareup/ordermanagerdata/SearchQuery;-><init>(Ljava/lang/String;JLcom/squareup/protos/client/orders/OrderGroup;Lcom/squareup/orders/model/Order$FulfillmentType;Lcom/squareup/protos/client/orders/Channel;)V

    return-object v0
.end method

.method private static final getShortNameRes(Lcom/squareup/protos/client/orders/OrderGroup;)I
    .locals 2

    .line 276
    sget-object v0, Lcom/squareup/ui/orderhub/master/FilterKt$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/OrderGroup;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 279
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_filter_value_status_upcoming:I

    goto :goto_0

    .line 280
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected order group "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 278
    :cond_1
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_filter_value_status_completed:I

    goto :goto_0

    .line 277
    :cond_2
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_filter_value_status_active_shortened:I

    :goto_0
    return p0
.end method

.method public static final getSourceFilter(Lcom/squareup/orders/model/Order;)Lcom/squareup/ui/orderhub/master/Filter$Source;
    .locals 1

    const-string v0, "$this$sourceFilter"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 237
    new-instance v0, Lcom/squareup/ui/orderhub/master/Filter$Source;

    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getChannel(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Channel;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/ui/orderhub/master/Filter$Source;-><init>(Lcom/squareup/protos/client/orders/Channel;)V

    return-object v0
.end method

.method public static final getTypeFilter(Lcom/squareup/orders/model/Order;)Lcom/squareup/ui/orderhub/master/Filter$Type;
    .locals 2

    const-string v0, "$this$typeFilter"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    new-instance v0, Lcom/squareup/ui/orderhub/master/Filter$Type;

    invoke-static {p0}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const-string v1, "primaryFulfillment.type"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0}, Lcom/squareup/ui/orderhub/master/Filter$Type;-><init>(Lcom/squareup/orders/model/Order$FulfillmentType;)V

    return-object v0
.end method

.method public static synthetic sourceFilter$annotations(Lcom/squareup/orders/model/Order;)V
    .locals 0

    return-void
.end method

.method public static synthetic typeFilter$annotations(Lcom/squareup/orders/model/Order;)V
    .locals 0

    return-void
.end method
