.class public final Lcom/squareup/ui/orderhub/master/MasterFiltersAdapterKt;
.super Ljava/lang/Object;
.source "MasterFiltersAdapter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "groupPosition",
        "",
        "Lcom/squareup/ui/orderhub/master/Filter;",
        "getGroupPosition",
        "(Lcom/squareup/ui/orderhub/master/Filter;)I",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getGroupPosition$p(Lcom/squareup/ui/orderhub/master/Filter;)I
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/orderhub/master/MasterFiltersAdapterKt;->getGroupPosition(Lcom/squareup/ui/orderhub/master/Filter;)I

    move-result p0

    return p0
.end method

.method private static final getGroupPosition(Lcom/squareup/ui/orderhub/master/Filter;)I
    .locals 1

    .line 128
    instance-of v0, p0, Lcom/squareup/ui/orderhub/master/Filter$Status;

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 129
    :cond_0
    instance-of v0, p0, Lcom/squareup/ui/orderhub/master/Filter$Type;

    if-eqz v0, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    .line 130
    :cond_1
    instance-of p0, p0, Lcom/squareup/ui/orderhub/master/Filter$Source;

    if-eqz p0, :cond_2

    const/4 p0, 0x2

    :goto_0
    return p0

    :cond_2
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
