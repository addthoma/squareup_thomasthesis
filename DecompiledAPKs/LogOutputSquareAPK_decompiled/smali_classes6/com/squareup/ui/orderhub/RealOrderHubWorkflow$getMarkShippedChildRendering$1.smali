.class final Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderHubWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->getMarkShippedChildRendering(Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/orderhub/OrderHubState;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/OrderHubState;",
        "+",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderHubWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderHubWorkflow.kt\ncom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1\n*L\n1#1,713:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/OrderHubState;",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        "result",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $order:Lcom/squareup/orders/model/Order;

.field final synthetic $state:Lcom/squareup/ui/orderhub/OrderHubState;

.field final synthetic $updatedOrderStatusMap:Ljava/util/Map;


# direct methods
.method constructor <init>(Ljava/util/Map;Lcom/squareup/orders/model/Order;Lcom/squareup/ui/orderhub/OrderHubState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;->$updatedOrderStatusMap:Ljava/util/Map;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;->$order:Lcom/squareup/orders/model/Order;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/OrderHubState;",
            "Lcom/squareup/ui/orderhub/OrderHubResult;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "result"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 473
    instance-of v2, v1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$MarkShippedComplete;

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$GoBackFromMarkShipped;->INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$GoBackFromMarkShipped;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 474
    :goto_0
    iget-object v1, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;->$updatedOrderStatusMap:Ljava/util/Map;

    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;->$order:Lcom/squareup/orders/model/Order;

    iget-object v2, v2, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 476
    iget-object v5, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 477
    sget-object v2, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderNoWorkflowAction;->INSTANCE:Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderNoWorkflowAction;

    move-object v11, v2

    check-cast v11, Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    .line 478
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    .line 479
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;->$updatedOrderStatusMap:Ljava/util/Map;

    const/16 v20, 0x3f

    const/16 v21, 0x0

    move-object/from16 v19, v2

    .line 478
    invoke-static/range {v12 .. v21}, Lcom/squareup/ui/orderhub/DetailState;->copy$default(Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ordermanagerdata/ResultState$Failure;Lorg/threeten/bp/ZonedDateTime;Ljava/util/Map;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v10

    const/4 v12, 0x0

    const/16 v13, 0x4f

    .line 476
    invoke-static/range {v5 .. v14}, Lcom/squareup/ui/orderhub/OrderHubState;->copy$default(Lcom/squareup/ui/orderhub/OrderHubState;ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v2

    .line 475
    invoke-static {v1, v2, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_1

    .line 484
    :cond_1
    instance-of v1, v1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$MarkShippedFailed;

    if-eqz v1, :cond_3

    .line 485
    iget-object v1, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;->$updatedOrderStatusMap:Ljava/util/Map;

    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;->$order:Lcom/squareup/orders/model/Order;

    iget-object v2, v2, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    const-string v5, "order.id"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 486
    iget-object v5, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/DetailState;->getOrderQuickActionStatusMap()Ljava/util/Map;

    move-result-object v5

    iget-object v6, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;->$order:Lcom/squareup/orders/model/Order;

    iget-object v6, v6, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_2

    move-object v6, v5

    check-cast v6, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-static/range {v6 .. v11}, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->copy$default(Lcom/squareup/ui/orderhub/OrderQuickActionStatus;Lcom/squareup/orders/model/Order;ZZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderQuickActionStatus;

    move-result-object v5

    .line 485
    invoke-interface {v1, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 489
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 490
    iget-object v5, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 491
    sget-object v2, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderNoWorkflowAction;->INSTANCE:Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderNoWorkflowAction;

    move-object v11, v2

    check-cast v11, Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    .line 492
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    .line 493
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;->$updatedOrderStatusMap:Ljava/util/Map;

    const/16 v20, 0x3f

    const/16 v21, 0x0

    move-object/from16 v19, v2

    .line 492
    invoke-static/range {v12 .. v21}, Lcom/squareup/ui/orderhub/DetailState;->copy$default(Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ordermanagerdata/ResultState$Failure;Lorg/threeten/bp/ZonedDateTime;Ljava/util/Map;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v10

    const/4 v12, 0x0

    const/16 v13, 0x4f

    .line 490
    invoke-static/range {v5 .. v14}, Lcom/squareup/ui/orderhub/OrderHubState;->copy$default(Lcom/squareup/ui/orderhub/OrderHubState;ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v2

    .line 489
    invoke-static {v1, v2, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    :goto_1
    return-object v1

    .line 486
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Required value was null."

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 489
    :cond_3
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 77
    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$getMarkShippedChildRendering$1;->invoke(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
