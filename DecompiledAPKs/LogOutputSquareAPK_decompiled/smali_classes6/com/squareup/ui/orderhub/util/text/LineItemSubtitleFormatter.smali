.class public final Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;
.super Ljava/lang/Object;
.source "LineItemSubtitleFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLineItemSubtitleFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LineItemSubtitleFormatter.kt\ncom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter\n*L\n1#1,66:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000cJ\u0018\u0010\r\u001a\u00020\u00082\u0006\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "perUnitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)V",
        "createOrderLineItemSubtitle",
        "",
        "lineItem",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "includeQuantityInBasePrice",
        "",
        "quantityWithPrecision",
        "quantity",
        "Ljava/math/BigDecimal;",
        "quantityUnit",
        "Lcom/squareup/orders/model/Order$QuantityUnit;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    return-void
.end method

.method public static synthetic createOrderLineItemSubtitle$default(Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;Lcom/squareup/orders/model/Order$LineItem;ZILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 36
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;->createOrderLineItemSubtitle(Lcom/squareup/orders/model/Order$LineItem;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final createOrderLineItemSubtitle(Lcom/squareup/orders/model/Order$LineItem;Z)Ljava/lang/String;
    .locals 9

    const-string v0, "lineItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {p1, v0, v1, p2}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->basePricePerQuantityUnit(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Z)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->listOfNotNull(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/util/Collection;

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->variationAndPriceString(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOfNotNull(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 40
    invoke-static {p2, v0}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/util/Collection;

    .line 42
    iget-object v0, p1, Lcom/squareup/orders/model/Order$LineItem;->sku:Ljava/lang/String;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOfNotNull(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 41
    invoke-static {p2, v0}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/util/Collection;

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->modifierStrings(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 42
    invoke-static {p2, v0}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/util/Collection;

    .line 44
    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem;->note:Ljava/lang/String;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOfNotNull(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 43
    invoke-static {p2, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    const-string p1, ", "

    .line 45
    move-object v1, p1

    check-cast v1, Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3e

    const/4 v8, 0x0

    invoke-static/range {v0 .. v8}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final quantityWithPrecision(Ljava/math/BigDecimal;Lcom/squareup/orders/model/Order$QuantityUnit;)Ljava/lang/String;
    .locals 2

    const-string v0, "quantity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    sget-object v0, Lcom/squareup/quantity/UnitDisplayData;->Companion:Lcom/squareup/quantity/UnitDisplayData$Companion;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v1, p2}, Lcom/squareup/quantity/UnitDisplayData$Companion;->fromQuantityUnit(Lcom/squareup/util/Res;Lcom/squareup/orders/model/Order$QuantityUnit;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object p2

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 60
    invoke-virtual {p2}, Lcom/squareup/quantity/UnitDisplayData;->getQuantityPrecision()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/quantity/PerUnitFormatter;->quantityAndPrecision(Ljava/math/BigDecimal;I)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 61
    invoke-virtual {p2}, Lcom/squareup/quantity/UnitDisplayData;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 62
    invoke-virtual {p1}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 63
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
