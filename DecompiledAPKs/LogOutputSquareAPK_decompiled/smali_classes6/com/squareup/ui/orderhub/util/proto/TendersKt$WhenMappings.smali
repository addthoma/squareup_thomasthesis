.class public final synthetic Lcom/squareup/ui/orderhub/util/proto/TendersKt$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->values()[Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/orderhub/util/proto/TendersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/TendersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/TendersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->CASH:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/TendersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->THIRD_PARTY_CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/TendersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->SQUARE_GIFT_CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/TendersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->OTHER:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/TendersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->NO_SALE:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/TendersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->CHECK:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/TendersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->MERCHANT_GIFT_CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/TendersKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->THIRD_PARTY_E_MONEY:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1

    return-void
.end method
