.class public final Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealOrderSearchWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final activityAppletGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final debounceSearchMsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final orderRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Long;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow_Factory;->orderRepositoryProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow_Factory;->activityAppletGatewayProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow_Factory;->debounceSearchMsProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Long;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)",
            "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow_Factory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/ui/activity/ActivityAppletGateway;JLcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;
    .locals 7

    .line 53
    new-instance v6, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;-><init>(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/ui/activity/ActivityAppletGateway;JLcom/squareup/settings/server/AccountStatusSettings;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;
    .locals 5

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow_Factory;->orderRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ordermanagerdata/OrderRepository;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow_Factory;->activityAppletGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/activity/ActivityAppletGateway;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow_Factory;->debounceSearchMsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow_Factory;->newInstance(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/ui/activity/ActivityAppletGateway;JLcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow_Factory;->get()Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;

    move-result-object v0

    return-object v0
.end method
