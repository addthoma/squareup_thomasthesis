.class public final Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;
.super Ljava/lang/Object;
.source "SearchWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SearchRendering"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;,
        Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSearchWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SearchWorkflow.kt\ncom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering\n*L\n1#1,272:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008 \n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 32\u00020\u0001:\u000234Bs\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u0012\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000f0\u000e\u0012\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0011\u0012\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0011\u0012\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0011\u00a2\u0006\u0002\u0010\u0014J\t\u0010$\u001a\u00020\u0003H\u00c6\u0003J\t\u0010%\u001a\u00020\u0005H\u00c6\u0003J\t\u0010&\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010\'\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\u000f\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH\u00c6\u0003J\u0015\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000f0\u000eH\u00c6\u0003J\u000f\u0010*\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0011H\u00c6\u0003J\u000f\u0010+\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0011H\u00c6\u0003J\u000f\u0010,\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0011H\u00c6\u0003J\u0089\u0001\u0010-\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\u000e\u0008\u0002\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b2\u0014\u0008\u0002\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000f0\u000e2\u000e\u0008\u0002\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u00112\u000e\u0008\u0002\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u00112\u000e\u0008\u0002\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0011H\u00c6\u0001J\u0013\u0010.\u001a\u00020\u00032\u0008\u0010/\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00100\u001a\u00020\u0007H\u00d6\u0001J\t\u00101\u001a\u000202H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0015R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u001d\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000f0\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0017\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0017\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001bR\u0017\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001fR\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010!R\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010#\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;",
        "",
        "isSearchEnabled",
        "",
        "searchDisplayState",
        "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;",
        "numberOfDaysSearched",
        "",
        "searchEditText",
        "Lcom/squareup/workflow/text/WorkflowEditableText;",
        "searchResults",
        "",
        "Lcom/squareup/orders/model/Order;",
        "onChangedSearchFocus",
        "Lkotlin/Function1;",
        "",
        "onPressedEnter",
        "Lkotlin/Function0;",
        "onClickedTransactionsLink",
        "onClickedRetrySearch",
        "(ZLcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;ILcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V",
        "()Z",
        "getNumberOfDaysSearched",
        "()I",
        "getOnChangedSearchFocus",
        "()Lkotlin/jvm/functions/Function1;",
        "getOnClickedRetrySearch",
        "()Lkotlin/jvm/functions/Function0;",
        "getOnClickedTransactionsLink",
        "getOnPressedEnter",
        "getSearchDisplayState",
        "()Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;",
        "getSearchEditText",
        "()Lcom/squareup/workflow/text/WorkflowEditableText;",
        "getSearchResults",
        "()Ljava/util/List;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "hashCode",
        "toString",
        "",
        "Companion",
        "SearchDisplayState",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$Companion;

.field private static final SEARCH_NOT_ALLOWED:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;


# instance fields
.field private final isSearchEnabled:Z

.field private final numberOfDaysSearched:I

.field private final onChangedSearchFocus:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onClickedRetrySearch:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onClickedTransactionsLink:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onPressedEnter:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final searchDisplayState:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

.field private final searchEditText:Lcom/squareup/workflow/text/WorkflowEditableText;

.field private final searchResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 12

    new-instance v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->Companion:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$Companion;

    .line 72
    sget-object v4, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->NOT_SEARCHING:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v7

    sget-object v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$Companion$SEARCH_NOT_ALLOWED$1;->INSTANCE:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$Companion$SEARCH_NOT_ALLOWED$1;

    move-object v8, v0

    check-cast v8, Lkotlin/jvm/functions/Function1;

    sget-object v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$Companion$SEARCH_NOT_ALLOWED$2;->INSTANCE:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$Companion$SEARCH_NOT_ALLOWED$2;

    move-object v9, v0

    check-cast v9, Lkotlin/jvm/functions/Function0;

    sget-object v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$Companion$SEARCH_NOT_ALLOWED$3;->INSTANCE:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$Companion$SEARCH_NOT_ALLOWED$3;

    move-object v10, v0

    check-cast v10, Lkotlin/jvm/functions/Function0;

    sget-object v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$Companion$SEARCH_NOT_ALLOWED$4;->INSTANCE:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$Companion$SEARCH_NOT_ALLOWED$4;

    move-object v11, v0

    check-cast v11, Lkotlin/jvm/functions/Function0;

    new-instance v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v11}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;-><init>(ZLcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;ILcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    sput-object v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->SEARCH_NOT_ALLOWED:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;

    return-void
.end method

.method public constructor <init>(ZLcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;ILcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;",
            "I",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "searchDisplayState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchResults"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onChangedSearchFocus"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onPressedEnter"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClickedTransactionsLink"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClickedRetrySearch"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->isSearchEnabled:Z

    iput-object p2, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchDisplayState:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    iput p3, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->numberOfDaysSearched:I

    iput-object p4, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchEditText:Lcom/squareup/workflow/text/WorkflowEditableText;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchResults:Ljava/util/List;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onChangedSearchFocus:Lkotlin/jvm/functions/Function1;

    iput-object p7, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onPressedEnter:Lkotlin/jvm/functions/Function0;

    iput-object p8, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onClickedTransactionsLink:Lkotlin/jvm/functions/Function0;

    iput-object p9, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onClickedRetrySearch:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public static final synthetic access$getSEARCH_NOT_ALLOWED$cp()Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;
    .locals 1

    .line 49
    sget-object v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->SEARCH_NOT_ALLOWED:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;ZLcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;ILcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;
    .locals 10

    move-object v0, p0

    move/from16 v1, p10

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->isSearchEnabled:Z

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchDisplayState:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget v4, v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->numberOfDaysSearched:I

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchEditText:Lcom/squareup/workflow/text/WorkflowEditableText;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchResults:Ljava/util/List;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onChangedSearchFocus:Lkotlin/jvm/functions/Function1;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onPressedEnter:Lkotlin/jvm/functions/Function0;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onClickedTransactionsLink:Lkotlin/jvm/functions/Function0;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onClickedRetrySearch:Lkotlin/jvm/functions/Function0;

    goto :goto_8

    :cond_8
    move-object/from16 v1, p9

    :goto_8
    move p1, v2

    move-object p2, v3

    move p3, v4

    move-object p4, v5

    move-object p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v1

    invoke-virtual/range {p0 .. p9}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->copy(ZLcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;ILcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->isSearchEnabled:Z

    return v0
.end method

.method public final component2()Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchDisplayState:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->numberOfDaysSearched:I

    return v0
.end method

.method public final component4()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchEditText:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final component5()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchResults:Ljava/util/List;

    return-object v0
.end method

.method public final component6()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onChangedSearchFocus:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component7()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onPressedEnter:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component8()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onClickedTransactionsLink:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component9()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onClickedRetrySearch:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final copy(ZLcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;ILcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;",
            "I",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;"
        }
    .end annotation

    const-string v0, "searchDisplayState"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchResults"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onChangedSearchFocus"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onPressedEnter"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClickedTransactionsLink"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClickedRetrySearch"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;

    move-object v1, v0

    move v2, p1

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v10}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;-><init>(ZLcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;ILcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->isSearchEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->isSearchEnabled:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchDisplayState:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchDisplayState:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->numberOfDaysSearched:I

    iget v1, p1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->numberOfDaysSearched:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchEditText:Lcom/squareup/workflow/text/WorkflowEditableText;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchEditText:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchResults:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchResults:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onChangedSearchFocus:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onChangedSearchFocus:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onPressedEnter:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onPressedEnter:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onClickedTransactionsLink:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onClickedTransactionsLink:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onClickedRetrySearch:Lkotlin/jvm/functions/Function0;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onClickedRetrySearch:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getNumberOfDaysSearched()I
    .locals 1

    .line 52
    iget v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->numberOfDaysSearched:I

    return v0
.end method

.method public final getOnChangedSearchFocus()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onChangedSearchFocus:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnClickedRetrySearch()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onClickedRetrySearch:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnClickedTransactionsLink()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onClickedTransactionsLink:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnPressedEnter()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onPressedEnter:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getSearchDisplayState()Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchDisplayState:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    return-object v0
.end method

.method public final getSearchEditText()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchEditText:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final getSearchResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchResults:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->isSearchEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchDisplayState:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->numberOfDaysSearched:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchEditText:Lcom/squareup/workflow/text/WorkflowEditableText;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchResults:Ljava/util/List;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onChangedSearchFocus:Lkotlin/jvm/functions/Function1;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onPressedEnter:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_5
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onClickedTransactionsLink:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_6
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onClickedRetrySearch:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    return v0
.end method

.method public final isSearchEnabled()Z
    .locals 1

    .line 50
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->isSearchEnabled:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SearchRendering(isSearchEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->isSearchEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", searchDisplayState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchDisplayState:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", numberOfDaysSearched="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->numberOfDaysSearched:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", searchEditText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchEditText:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", searchResults="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->searchResults:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onChangedSearchFocus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onChangedSearchFocus:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onPressedEnter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onPressedEnter:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onClickedTransactionsLink="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onClickedTransactionsLink:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onClickedRetrySearch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->onClickedRetrySearch:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
