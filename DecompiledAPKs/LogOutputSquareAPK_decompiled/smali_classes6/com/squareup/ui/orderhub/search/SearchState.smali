.class public final Lcom/squareup/ui/orderhub/search/SearchState;
.super Ljava/lang/Object;
.source "SearchState.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0015\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001BC\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u0012\u0006\u0010\u000c\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\rJ\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0003H\u00c6\u0003JU\u0010\u001d\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00032\u000e\u0008\u0002\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u001e\u001a\u00020\u00032\u0008\u0010\u001f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010 \u001a\u00020!H\u00d6\u0001J\t\u0010\"\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0010R\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0010R\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0010R\u0011\u0010\u000c\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0010R\u0017\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u000f\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/search/SearchState;",
        "",
        "isInSearchContext",
        "",
        "searchText",
        "",
        "dispatchedSearchText",
        "redoSearchSwitch",
        "isSearching",
        "searchResults",
        "",
        "Lcom/squareup/orders/model/Order;",
        "searchError",
        "(ZLjava/lang/String;Ljava/lang/String;ZZLjava/util/List;Z)V",
        "getDispatchedSearchText",
        "()Ljava/lang/String;",
        "()Z",
        "getRedoSearchSwitch",
        "getSearchError",
        "getSearchResults",
        "()Ljava/util/List;",
        "getSearchText",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dispatchedSearchText:Ljava/lang/String;

.field private final isInSearchContext:Z

.field private final isSearching:Z

.field private final redoSearchSwitch:Z

.field private final searchError:Z

.field private final searchResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation
.end field

.field private final searchText:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;ZZLjava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "searchText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatchedSearchText"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchResults"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/ui/orderhub/search/SearchState;->isInSearchContext:Z

    iput-object p2, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchText:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/search/SearchState;->dispatchedSearchText:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/squareup/ui/orderhub/search/SearchState;->redoSearchSwitch:Z

    iput-boolean p5, p0, Lcom/squareup/ui/orderhub/search/SearchState;->isSearching:Z

    iput-object p6, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchResults:Ljava/util/List;

    iput-boolean p7, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchError:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/search/SearchState;ZLjava/lang/String;Ljava/lang/String;ZZLjava/util/List;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/search/SearchState;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-boolean p1, p0, Lcom/squareup/ui/orderhub/search/SearchState;->isInSearchContext:Z

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchText:Ljava/lang/String;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/orderhub/search/SearchState;->dispatchedSearchText:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/ui/orderhub/search/SearchState;->redoSearchSwitch:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/ui/orderhub/search/SearchState;->isSearching:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchResults:Ljava/util/List;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-boolean p7, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchError:Z

    :cond_6
    move v4, p7

    move-object p2, p0

    move p3, p1

    move-object p4, p9

    move-object p5, v0

    move p6, v1

    move p7, v2

    move-object p8, v3

    move p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/ui/orderhub/search/SearchState;->copy(ZLjava/lang/String;Ljava/lang/String;ZZLjava/util/List;Z)Lcom/squareup/ui/orderhub/search/SearchState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->isInSearchContext:Z

    return v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchText:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->dispatchedSearchText:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->redoSearchSwitch:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->isSearching:Z

    return v0
.end method

.method public final component6()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchResults:Ljava/util/List;

    return-object v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchError:Z

    return v0
.end method

.method public final copy(ZLjava/lang/String;Ljava/lang/String;ZZLjava/util/List;Z)Lcom/squareup/ui/orderhub/search/SearchState;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;Z)",
            "Lcom/squareup/ui/orderhub/search/SearchState;"
        }
    .end annotation

    const-string v0, "searchText"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dispatchedSearchText"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchResults"

    move-object v7, p6

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/search/SearchState;

    move-object v1, v0

    move v2, p1

    move v5, p4

    move v6, p5

    move/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/orderhub/search/SearchState;-><init>(ZLjava/lang/String;Ljava/lang/String;ZZLjava/util/List;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/search/SearchState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/search/SearchState;

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->isInSearchContext:Z

    iget-boolean v1, p1, Lcom/squareup/ui/orderhub/search/SearchState;->isInSearchContext:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchText:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/search/SearchState;->searchText:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->dispatchedSearchText:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/search/SearchState;->dispatchedSearchText:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->redoSearchSwitch:Z

    iget-boolean v1, p1, Lcom/squareup/ui/orderhub/search/SearchState;->redoSearchSwitch:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->isSearching:Z

    iget-boolean v1, p1, Lcom/squareup/ui/orderhub/search/SearchState;->isSearching:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchResults:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/search/SearchState;->searchResults:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchError:Z

    iget-boolean p1, p1, Lcom/squareup/ui/orderhub/search/SearchState;->searchError:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDispatchedSearchText()Ljava/lang/String;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->dispatchedSearchText:Ljava/lang/String;

    return-object v0
.end method

.method public final getRedoSearchSwitch()Z
    .locals 1

    .line 11
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->redoSearchSwitch:Z

    return v0
.end method

.method public final getSearchError()Z
    .locals 1

    .line 15
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchError:Z

    return v0
.end method

.method public final getSearchResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchResults:Ljava/util/List;

    return-object v0
.end method

.method public final getSearchText()Ljava/lang/String;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchText:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->isInSearchContext:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchText:Ljava/lang/String;

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/search/SearchState;->dispatchedSearchText:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/orderhub/search/SearchState;->redoSearchSwitch:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/orderhub/search/SearchState;->isSearching:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchResults:Ljava/util/List;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :cond_5
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchError:Z

    if-eqz v2, :cond_6

    goto :goto_2

    :cond_6
    move v1, v2

    :goto_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final isInSearchContext()Z
    .locals 1

    .line 6
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->isInSearchContext:Z

    return v0
.end method

.method public final isSearching()Z
    .locals 1

    .line 13
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/search/SearchState;->isSearching:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SearchState(isInSearchContext="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/search/SearchState;->isInSearchContext:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", searchText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", dispatchedSearchText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/SearchState;->dispatchedSearchText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", redoSearchSwitch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/search/SearchState;->redoSearchSwitch:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isSearching="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/search/SearchState;->isSearching:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", searchResults="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchResults:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", searchError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/search/SearchState;->searchError:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
