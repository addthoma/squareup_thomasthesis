.class public final Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Companion;
.super Ljava/lang/Object;
.source "SearchWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u000e\u0010\n\u001a\u00020\u000bX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u000bX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000bX\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Companion;",
        "",
        "()V",
        "FALLBACK_SEARCH_MILLIS",
        "",
        "MILLIS_PER_DAY",
        "SEARCH_INITIAL_STATE",
        "Lcom/squareup/ui/orderhub/search/SearchState;",
        "getSEARCH_INITIAL_STATE",
        "()Lcom/squareup/ui/orderhub/search/SearchState;",
        "SEARCH_ORDERS_DEBOUNCE_KEY",
        "",
        "SEARCH_ORDERS_WORKER_KEY_A",
        "SEARCH_ORDERS_WORKER_KEY_B",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 254
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getSEARCH_INITIAL_STATE()Lcom/squareup/ui/orderhub/search/SearchState;
    .locals 1

    .line 261
    invoke-static {}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->access$getSEARCH_INITIAL_STATE$cp()Lcom/squareup/ui/orderhub/search/SearchState;

    move-result-object v0

    return-object v0
.end method
