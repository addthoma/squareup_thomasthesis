.class public final Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;
.super Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action;
.source "SearchWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EnterSearchText"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0003H\u00d6\u0001J\u0018\u0010\u0010\u001a\u00020\u0011*\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u0012H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;",
        "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action;",
        "newSearchText",
        "",
        "(Ljava/lang/String;)V",
        "getNewSearchText",
        "()Ljava/lang/String;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/ui/orderhub/search/SearchState;",
        "",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final newSearchText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string v0, "newSearchText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 193
    invoke-direct {p0, v0}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;->newSearchText:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;->newSearchText:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;->copy(Ljava/lang/String;)Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/ui/orderhub/search/SearchState;",
            "*>;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 195
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/orderhub/search/SearchState;

    .line 196
    iget-object v3, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;->newSearchText:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x7d

    const/4 v10, 0x0

    .line 195
    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/orderhub/search/SearchState;->copy$default(Lcom/squareup/ui/orderhub/search/SearchState;ZLjava/lang/String;Ljava/lang/String;ZZLjava/util/List;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/search/SearchState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;->newSearchText:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;)Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;
    .locals 1

    const-string v0, "newSearchText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;

    invoke-direct {v0, p1}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;->newSearchText:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;->newSearchText:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getNewSearchText()Ljava/lang/String;
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;->newSearchText:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;->newSearchText:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EnterSearchText(newSearchText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action$EnterSearchText;->newSearchText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
