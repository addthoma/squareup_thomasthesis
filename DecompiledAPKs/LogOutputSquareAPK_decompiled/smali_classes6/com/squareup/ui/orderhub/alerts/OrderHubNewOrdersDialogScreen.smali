.class public final Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "OrderHubNewOrdersDialogScreen.kt"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Factory;,
        Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$ParentComponent;,
        Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen$Component;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\t\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002:\u0003\n\u000b\u000cB\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0008\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "Lcom/squareup/container/MaybePersistent;",
        "newOrderCount",
        "",
        "upcomingOrderCount",
        "(II)V",
        "getNewOrderCount",
        "()I",
        "getUpcomingOrderCount",
        "Component",
        "Factory",
        "ParentComponent",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final newOrderCount:I

.field private final upcomingOrderCount:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput p1, p0, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen;->newOrderCount:I

    iput p2, p0, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen;->upcomingOrderCount:I

    return-void
.end method


# virtual methods
.method public final getNewOrderCount()I
    .locals 1

    .line 28
    iget v0, p0, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen;->newOrderCount:I

    return v0
.end method

.method public final getUpcomingOrderCount()I
    .locals 1

    .line 29
    iget v0, p0, Lcom/squareup/ui/orderhub/alerts/OrderHubNewOrdersDialogScreen;->upcomingOrderCount:I

    return v0
.end method

.method public isPersistent()Z
    .locals 1

    .line 27
    invoke-static {p0}, Lcom/squareup/container/MaybePersistent$DefaultImpls;->isPersistent(Lcom/squareup/container/MaybePersistent;)Z

    move-result v0

    return v0
.end method
