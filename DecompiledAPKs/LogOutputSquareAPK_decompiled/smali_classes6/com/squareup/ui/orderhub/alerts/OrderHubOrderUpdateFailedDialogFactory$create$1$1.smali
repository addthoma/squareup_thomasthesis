.class final Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogFactory$create$1$1;
.super Ljava/lang/Object;
.source "OrderHubOrderUpdateFailedDialogFactory.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/content/DialogInterface;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $data:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

.field final synthetic $screen:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogFactory$create$1$1;->$screen:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogFactory$create$1$1;->$data:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogFactory$create$1$1;->$screen:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogFactory$create$1$1;->$data:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;->getNegativeButtonEvent()Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    move-result-object p2

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
