.class final Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderHubWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->render(Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/OrderHubState;",
        "+",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderHubWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderHubWorkflow.kt\ncom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n*L\n1#1,713:1\n250#2,2:714\n501#3:716\n486#3,6:717\n*E\n*S KotlinDebug\n*F\n+ 1 RealOrderHubWorkflow.kt\ncom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1\n*L\n303#1,2:714\n308#1:716\n308#1,6:717\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/OrderHubState;",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        "event",
        "Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/OrderHubState;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;->this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/OrderHubState;",
            "Lcom/squareup/ui/orderhub/OrderHubResult;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "event"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 292
    sget-object v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$LoadMoreCompletedOrders;->INSTANCE:Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$LoadMoreCompletedOrders;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    .line 294
    iget-object v1, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/DetailState;->getCanLoadMoreOrders()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 295
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;->this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    iget-object v5, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    invoke-static {v2, v5}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->access$getStateAfterPaginationRequested(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v2

    invoke-static {v1, v2, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_5

    .line 297
    :cond_0
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_5

    .line 300
    :cond_1
    instance-of v2, v1, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$ViewOrder;

    if-eqz v2, :cond_7

    .line 301
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;->this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    invoke-static {v2}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->access$getOrderHubAnalytics$p(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    move-result-object v2

    check-cast v1, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$ViewOrder;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$ViewOrder;->getViewedOrder()Lcom/squareup/orders/model/Order;

    move-result-object v5

    iget-object v5, v5, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    const-string v6, "event.viewedOrder.id"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v6, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_TAP_ORDER_DETAIL:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-virtual {v2, v5, v6}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logEvent$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    .line 303
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/OrderHubState;->getSyncedOrders()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 714
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/squareup/orders/model/Order;

    .line 303
    iget-object v6, v6, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$ViewOrder;->getViewedOrder()Lcom/squareup/orders/model/Order;

    move-result-object v7

    iget-object v7, v7, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    goto :goto_0

    :cond_3
    move-object v5, v4

    .line 302
    :goto_0
    check-cast v5, Lcom/squareup/orders/model/Order;

    .line 308
    iget-object v1, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/DetailState;->getOrderQuickActionStatusMap()Ljava/util/Map;

    move-result-object v1

    .line 716
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v2, Ljava/util/Map;

    .line 717
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 308
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;

    invoke-virtual {v7}, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->isLoading()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 719
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v2, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 308
    :cond_5
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    if-eqz v5, :cond_6

    .line 311
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 312
    iget-object v6, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 313
    new-instance v2, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;

    .line 314
    new-instance v12, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrder;

    invoke-direct {v12, v5}, Lcom/squareup/ui/orderhub/ViewedOrderInput$ViewedOrder;-><init>(Lcom/squareup/orders/model/Order;)V

    check-cast v12, Lcom/squareup/ui/orderhub/ViewedOrderInput;

    .line 313
    invoke-direct {v2, v12}, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderDetailWorkflowAction;-><init>(Lcom/squareup/ui/orderhub/ViewedOrderInput;)V

    move-object v12, v2

    check-cast v12, Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    const/4 v13, 0x0

    const/16 v14, 0x5f

    const/4 v15, 0x0

    .line 312
    invoke-static/range {v6 .. v15}, Lcom/squareup/ui/orderhub/OrderHubState;->copy$default(Lcom/squareup/ui/orderhub/OrderHubState;ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v2

    .line 311
    invoke-static {v1, v2, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_5

    .line 324
    :cond_6
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_5

    .line 327
    :cond_7
    instance-of v2, v1, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$FilterSelected;

    if-eqz v2, :cond_8

    .line 328
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;->this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    invoke-static {v2}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->access$getOrderHubAnalytics$p(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    move-result-object v2

    check-cast v1, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$FilterSelected;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$FilterSelected;->getFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logFilterSelection$orderhub_applet_release(Lcom/squareup/ui/orderhub/master/Filter;)V

    .line 329
    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v5, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;->this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    iget-object v6, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$FilterSelected;->getFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object v1

    invoke-static {v5, v6, v1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->access$getStateAfterFilterSelected(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/ui/orderhub/master/Filter;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v1

    invoke-static {v2, v1, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_5

    .line 331
    :cond_8
    sget-object v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$GoBack;->INSTANCE:Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$GoBack;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 332
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v2, Lcom/squareup/ui/orderhub/OrderHubResult$ExitedOrder;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubResult$ExitedOrder;

    invoke-virtual {v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_5

    .line 334
    :cond_9
    instance-of v2, v1, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$QuickActionSelected;

    if-eqz v2, :cond_10

    .line 335
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/DetailState;->getOrderQuickActionStatusMap()Ljava/util/Map;

    move-result-object v2

    invoke-static {v2}, Lkotlin/collections/MapsKt;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v12

    .line 336
    check-cast v1, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$QuickActionSelected;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$QuickActionSelected;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-interface {v12, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;

    const/4 v5, 0x1

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->isLoading()Z

    move-result v2

    if-eq v2, v5, :cond_b

    .line 337
    :cond_a
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/OrderHubState;->getNextWorkflowAction()Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    move-result-object v2

    instance-of v2, v2, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderMarkShippedWorkflowAction;

    if-eqz v2, :cond_c

    .line 339
    :cond_b
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_5

    .line 341
    :cond_c
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;->this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    invoke-static {v2}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->access$getOrderHubAnalytics$p(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$QuickActionSelected;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v6

    iget-object v6, v6, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    const-string v7, "event.order.id"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v8, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_QUICK_ACTIONS:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-virtual {v2, v6, v8}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logEvent$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    .line 342
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$QuickActionSelected;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getPrimaryAction(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Action;

    move-result-object v2

    if-eqz v2, :cond_d

    iget-object v2, v2, Lcom/squareup/protos/client/orders/Action;->type:Lcom/squareup/protos/client/orders/Action$Type;

    goto :goto_2

    :cond_d
    move-object v2, v4

    :goto_2
    sget-object v6, Lcom/squareup/protos/client/orders/Action$Type;->MARK_SHIPPED:Lcom/squareup/protos/client/orders/Action$Type;

    const/4 v8, 0x0

    if-ne v2, v6, :cond_e

    const/4 v2, 0x1

    goto :goto_3

    :cond_e
    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_f

    .line 344
    new-instance v2, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderMarkShippedWorkflowAction;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$QuickActionSelected;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v6

    invoke-direct {v2, v6}, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderMarkShippedWorkflowAction;-><init>(Lcom/squareup/orders/model/Order;)V

    check-cast v2, Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    goto :goto_4

    .line 346
    :cond_f
    sget-object v2, Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderNoWorkflowAction;->INSTANCE:Lcom/squareup/ui/orderhub/OrderWorkflowAction$OrderNoWorkflowAction;

    check-cast v2, Lcom/squareup/ui/orderhub/OrderWorkflowAction;

    :goto_4
    move-object/from16 v19, v2

    .line 348
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$QuickActionSelected;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v6, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;

    .line 349
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event$QuickActionSelected;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v1

    invoke-direct {v6, v1, v5, v8}, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;-><init>(Lcom/squareup/orders/model/Order;ZZ)V

    .line 348
    invoke-interface {v12, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 351
    iget-object v2, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    .line 353
    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v13, 0x3f

    const/4 v14, 0x0

    invoke-static/range {v5 .. v14}, Lcom/squareup/ui/orderhub/DetailState;->copy$default(Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ordermanagerdata/ResultState$Failure;Lorg/threeten/bp/ZonedDateTime;Ljava/util/Map;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v5

    const/16 v20, 0x0

    const/16 v21, 0x4f

    const/16 v22, 0x0

    move-object v13, v2

    move v14, v15

    move/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v5

    .line 351
    invoke-static/range {v13 .. v22}, Lcom/squareup/ui/orderhub/OrderHubState;->copy$default(Lcom/squareup/ui/orderhub/OrderHubState;ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v2

    .line 350
    invoke-static {v1, v2, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    :goto_5
    return-object v1

    .line 336
    :cond_10
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 77
    check-cast p1, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$detailEventHandler$1;->invoke(Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
