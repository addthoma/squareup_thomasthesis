.class final Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$masterEventHandler$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderHubWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->render(Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/OrderHubState;",
        "+",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/OrderHubState;",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        "event",
        "Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/OrderHubState;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$masterEventHandler$1;->this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$masterEventHandler$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/OrderHubState;",
            "Lcom/squareup/ui/orderhub/OrderHubResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 280
    instance-of v0, p1, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen$Event$FilterSelected;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$masterEventHandler$1;->this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->access$getOrderHubAnalytics$p(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    move-result-object v0

    check-cast p1, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen$Event$FilterSelected;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen$Event$FilterSelected;->getFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logFilterSelection$orderhub_applet_release(Lcom/squareup/ui/orderhub/master/Filter;)V

    .line 282
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$masterEventHandler$1;->this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$masterEventHandler$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen$Event$FilterSelected;->getFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object p1

    invoke-static {v1, v2, p1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->access$getStateAfterFilterSelected(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/ui/orderhub/master/Filter;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p1

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 284
    :cond_0
    sget-object v0, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen$Event$GoBack;->INSTANCE:Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen$Event$GoBack;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 285
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/ui/orderhub/OrderHubResult$ExitedOrder;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubResult$ExitedOrder;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 77
    check-cast p1, Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$masterEventHandler$1;->invoke(Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
