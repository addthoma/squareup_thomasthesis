.class final Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$1;
.super Ljava/lang/Object;
.source "OrderHubPrintingMonitor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->printOrders()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubPrintingMonitor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubPrintingMonitor.kt\ncom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,142:1\n1642#2,2:143\n1360#2:145\n1429#2,3:146\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubPrintingMonitor.kt\ncom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$1\n*L\n85#1,2:143\n104#1:145\n104#1,3:146\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012<\u0010\u0004\u001a8\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0007 \u0003*\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00060\u0006\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00080\u0008\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00080\u00080\u0005H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "",
        "kotlin.jvm.PlatformType",
        "<name for destructuring parameter 0>",
        "Lkotlin/Triple;",
        "",
        "Lcom/squareup/orders/model/Order;",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 44
    check-cast p1, Lkotlin/Triple;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$1;->apply(Lkotlin/Triple;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lkotlin/Triple;)Ljava/util/Set;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Triple<",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Triple;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p1}, Lkotlin/Triple;->component2()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Lkotlin/Triple;->component3()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    const-string v2, "isPrintingEnabled"

    .line 83
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v2, 0x1

    const-string v3, "orders"

    if-eqz v1, :cond_1

    const-string v1, "printedBefore"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 84
    iget-object v1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;

    invoke-static {v1}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->access$getCurrentTime$p(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lcom/squareup/time/CurrentTime;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/time/CurrentTime;->zonedDateTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v1

    .line 85
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, v0

    check-cast v4, Ljava/lang/Iterable;

    .line 143
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/orders/model/Order;

    .line 86
    iget-object v6, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;

    invoke-static {v6, v5, v1}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->access$shouldAutoPrint(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;Lcom/squareup/orders/model/Order;Lorg/threeten/bp/ZonedDateTime;)Z

    move-result v6

    const-string v7, "order.id"

    if-eqz v6, :cond_0

    .line 87
    iget-object v6, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;

    invoke-static {v6}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->access$getOrderHubAnalytics$p(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    move-result-object v6

    iget-object v8, v5, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {v8, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6, v8, v2}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logPrintingEvent$orderhub_applet_release(Ljava/lang/String;Z)V

    .line 88
    iget-object v6, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;

    invoke-static {v6}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->access$getOrderPrintingDispatcher$p(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lcom/squareup/print/OrderPrintingDispatcher;

    move-result-object v6

    .line 90
    iget-object v7, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;

    invoke-static {v7}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->access$getRes$p(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lcom/squareup/util/Res;

    move-result-object v7

    .line 91
    iget-object v8, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;

    invoke-static {v8}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->access$getDateAndTimeFormatter$p(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    move-result-object v8

    .line 92
    iget-object v9, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;

    invoke-static {v9}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->access$getCurrentTime$p(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lcom/squareup/time/CurrentTime;

    move-result-object v9

    .line 88
    invoke-static {v6, v5, v7, v8, v9}, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderKt;->printOrder(Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/orders/model/Order;Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/time/CurrentTime;)V

    goto :goto_0

    .line 95
    :cond_0
    iget-object v6, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;

    invoke-static {v6}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->access$getOrderHubAnalytics$p(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    move-result-object v6

    iget-object v5, v5, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logSkippedAutomaticPrinting$orderhub_applet_release(Ljava/lang/String;)V

    goto :goto_0

    .line 99
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_2

    .line 100
    iget-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->access$getOrderHubAnalytics$p(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logSkippedAutomaticPrintingInitialAttempt$orderhub_applet_release()V

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$1;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->access$getOrdersPrintedBeforePreference$p(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    .line 104
    :cond_2
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 145
    new-instance p1, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 146
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 147
    check-cast v1, Lcom/squareup/orders/model/Order;

    .line 104
    iget-object v1, v1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 148
    :cond_3
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 105
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method
