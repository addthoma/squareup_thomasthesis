.class public final Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;
.super Ljava/lang/Object;
.source "OrderEditTrackingState.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u001a\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001BS\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\r\u0012\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\r\u00a2\u0006\u0002\u0010\u000fJ\t\u0010\u001c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0005H\u00c6\u0003J\u000b\u0010 \u001a\u0004\u0018\u00010\tH\u00c6\u0003J\u000b\u0010!\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003J\u000b\u0010\"\u001a\u0004\u0018\u00010\rH\u00c6\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\rH\u00c6\u0003Ja\u0010$\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00052\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\r2\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\rH\u00c6\u0001J\u0013\u0010%\u001a\u00020\u00052\u0008\u0010&\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\'\u001a\u00020(H\u00d6\u0001J\t\u0010)\u001a\u00020\rH\u00d6\u0001R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0012R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0013\u0010\u000c\u001a\u0004\u0018\u00010\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001a\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;",
        "",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "isReadOnly",
        "",
        "showCustomCarrierOnly",
        "shouldShowRemoveTracking",
        "orderUpdateFailureState",
        "Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;",
        "displayTrackingInfo",
        "Lcom/squareup/ordermanagerdata/TrackingInfo;",
        "unsubmittedTrackingNumber",
        "",
        "unsubmittedCarrierName",
        "(Lcom/squareup/orders/model/Order;ZZZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ordermanagerdata/TrackingInfo;Ljava/lang/String;Ljava/lang/String;)V",
        "getDisplayTrackingInfo",
        "()Lcom/squareup/ordermanagerdata/TrackingInfo;",
        "()Z",
        "getOrder",
        "()Lcom/squareup/orders/model/Order;",
        "getOrderUpdateFailureState",
        "()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;",
        "getShouldShowRemoveTracking",
        "getShowCustomCarrierOnly",
        "getUnsubmittedCarrierName",
        "()Ljava/lang/String;",
        "getUnsubmittedTrackingNumber",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final displayTrackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

.field private final isReadOnly:Z

.field private final order:Lcom/squareup/orders/model/Order;

.field private final orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

.field private final shouldShowRemoveTracking:Z

.field private final showCustomCarrierOnly:Z

.field private final unsubmittedCarrierName:Ljava/lang/String;

.field private final unsubmittedTrackingNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/orders/model/Order;ZZZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ordermanagerdata/TrackingInfo;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->order:Lcom/squareup/orders/model/Order;

    iput-boolean p2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->isReadOnly:Z

    iput-boolean p3, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->showCustomCarrierOnly:Z

    iput-boolean p4, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->shouldShowRemoveTracking:Z

    iput-object p5, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->displayTrackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

    iput-object p7, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->unsubmittedTrackingNumber:Ljava/lang/String;

    iput-object p8, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->unsubmittedCarrierName:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/orders/model/Order;ZZZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ordermanagerdata/TrackingInfo;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 12

    move/from16 v0, p9

    and-int/lit8 v1, v0, 0x20

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 14
    move-object v1, v2

    check-cast v1, Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-object v9, v1

    goto :goto_0

    :cond_0
    move-object/from16 v9, p6

    :goto_0
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_1

    .line 15
    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    move-object v10, v1

    goto :goto_1

    :cond_1
    move-object/from16 v10, p7

    :goto_1
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_2

    .line 16
    move-object v0, v2

    check-cast v0, Ljava/lang/String;

    move-object v11, v0

    goto :goto_2

    :cond_2
    move-object/from16 v11, p8

    :goto_2
    move-object v3, p0

    move-object v4, p1

    move v5, p2

    move v6, p3

    move/from16 v7, p4

    move-object/from16 v8, p5

    invoke-direct/range {v3 .. v11}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;-><init>(Lcom/squareup/orders/model/Order;ZZZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ordermanagerdata/TrackingInfo;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;Lcom/squareup/orders/model/Order;ZZZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ordermanagerdata/TrackingInfo;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->order:Lcom/squareup/orders/model/Order;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-boolean v3, v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->isReadOnly:Z

    goto :goto_1

    :cond_1
    move v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->showCustomCarrierOnly:Z

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-boolean v5, v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->shouldShowRemoveTracking:Z

    goto :goto_3

    :cond_3
    move v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->displayTrackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

    goto :goto_5

    :cond_5
    move-object v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->unsubmittedTrackingNumber:Ljava/lang/String;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->unsubmittedCarrierName:Ljava/lang/String;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p8

    :goto_7
    move-object p1, v2

    move p2, v3

    move p3, v4

    move p4, v5

    move-object p5, v6

    move-object p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->copy(Lcom/squareup/orders/model/Order;ZZZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ordermanagerdata/TrackingInfo;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/orders/model/Order;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->order:Lcom/squareup/orders/model/Order;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->isReadOnly:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->showCustomCarrierOnly:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->shouldShowRemoveTracking:Z

    return v0
.end method

.method public final component5()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    return-object v0
.end method

.method public final component6()Lcom/squareup/ordermanagerdata/TrackingInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->displayTrackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

    return-object v0
.end method

.method public final component7()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->unsubmittedTrackingNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final component8()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->unsubmittedCarrierName:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Lcom/squareup/orders/model/Order;ZZZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ordermanagerdata/TrackingInfo;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;
    .locals 10

    const-string v0, "order"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    move-object v1, v0

    move v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;-><init>(Lcom/squareup/orders/model/Order;ZZZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ordermanagerdata/TrackingInfo;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->order:Lcom/squareup/orders/model/Order;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->order:Lcom/squareup/orders/model/Order;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->isReadOnly:Z

    iget-boolean v1, p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->isReadOnly:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->showCustomCarrierOnly:Z

    iget-boolean v1, p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->showCustomCarrierOnly:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->shouldShowRemoveTracking:Z

    iget-boolean v1, p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->shouldShowRemoveTracking:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->displayTrackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->displayTrackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->unsubmittedTrackingNumber:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->unsubmittedTrackingNumber:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->unsubmittedCarrierName:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->unsubmittedCarrierName:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDisplayTrackingInfo()Lcom/squareup/ordermanagerdata/TrackingInfo;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->displayTrackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

    return-object v0
.end method

.method public final getOrder()Lcom/squareup/orders/model/Order;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->order:Lcom/squareup/orders/model/Order;

    return-object v0
.end method

.method public final getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    return-object v0
.end method

.method public final getShouldShowRemoveTracking()Z
    .locals 1

    .line 12
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->shouldShowRemoveTracking:Z

    return v0
.end method

.method public final getShowCustomCarrierOnly()Z
    .locals 1

    .line 11
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->showCustomCarrierOnly:Z

    return v0
.end method

.method public final getUnsubmittedCarrierName()Ljava/lang/String;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->unsubmittedCarrierName:Ljava/lang/String;

    return-object v0
.end method

.method public final getUnsubmittedTrackingNumber()Ljava/lang/String;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->unsubmittedTrackingNumber:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->order:Lcom/squareup/orders/model/Order;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->isReadOnly:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->showCustomCarrierOnly:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->shouldShowRemoveTracking:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->displayTrackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_5
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->unsubmittedTrackingNumber:Ljava/lang/String;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_6
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->unsubmittedCarrierName:Ljava/lang/String;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_7
    add-int/2addr v0, v1

    return v0
.end method

.method public final isReadOnly()Z
    .locals 1

    .line 10
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->isReadOnly:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OrderEditTrackingState(order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->order:Lcom/squareup/orders/model/Order;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isReadOnly="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->isReadOnly:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showCustomCarrierOnly="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->showCustomCarrierOnly:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", shouldShowRemoveTracking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->shouldShowRemoveTracking:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", orderUpdateFailureState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", displayTrackingInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->displayTrackingInfo:Lcom/squareup/ordermanagerdata/TrackingInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", unsubmittedTrackingNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->unsubmittedTrackingNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", unsubmittedCarrierName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->unsubmittedCarrierName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
