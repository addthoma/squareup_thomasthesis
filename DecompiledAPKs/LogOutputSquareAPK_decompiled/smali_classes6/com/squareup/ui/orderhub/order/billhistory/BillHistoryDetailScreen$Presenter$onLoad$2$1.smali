.class final Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$2$1;
.super Ljava/lang/Object;
.source "BillHistoryDetailScreen.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$2;->invoke()Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "billLoadedState",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$2;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$2$1;->this$0:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;)V
    .locals 3

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$2$1;->this$0:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$2;

    iget-object v0, v0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$2;->this$0:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->access$getView(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;)Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const-string v2, "FALSE"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->showProgress$orderhub_applet_release(Z)V

    .line 121
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$2$1;->this$0:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$2;

    iget-object v0, v0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$2;->this$0:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$2$1;->this$0:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$2;

    iget-object v1, v1, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$2;->this$0:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;

    invoke-static {v1}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->access$getView(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;)Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;

    move-result-object v1

    const-string v2, "view"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;

    invoke-static {v0, v1, p1}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->access$handleBillLoadedSuccess(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;)V

    return-void

    .line 124
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Bill must be loaded"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 88
    check-cast p1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter$onLoad$2$1;->accept(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;)V

    return-void
.end method
