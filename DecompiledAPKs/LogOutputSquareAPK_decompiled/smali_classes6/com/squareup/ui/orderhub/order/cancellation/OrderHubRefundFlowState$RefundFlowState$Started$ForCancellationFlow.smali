.class public final Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;
.super Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started;
.source "OrderHubRefundFlowState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ForCancellationFlow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0013\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001BI\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012*\u0010\u0008\u001a&\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r0\tj\u0002`\u000e\u00a2\u0006\u0002\u0010\u000fJ\u000e\u0010\u0018\u001a\u00020\u0003H\u00c0\u0003\u00a2\u0006\u0002\u0008\u0019J\u000e\u0010\u001a\u001a\u00020\u0005H\u00c0\u0003\u00a2\u0006\u0002\u0008\u001bJ\u000e\u0010\u001c\u001a\u00020\u0007H\u00c0\u0003\u00a2\u0006\u0002\u0008\u001dJ2\u0010\u001e\u001a&\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r0\tj\u0002`\u000eH\u00c0\u0003\u00a2\u0006\u0002\u0008\u001fJU\u0010 \u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072,\u0008\u0002\u0010\u0008\u001a&\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r0\tj\u0002`\u000eH\u00c6\u0001J\u0013\u0010!\u001a\u00020\"2\u0008\u0010#\u001a\u0004\u0018\u00010$H\u00d6\u0003J\t\u0010%\u001a\u00020&H\u00d6\u0001J\t\u0010\'\u001a\u00020\nH\u00d6\u0001R\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0014\u0010\u0006\u001a\u00020\u0007X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R8\u0010\u0008\u001a&\u0012\u0004\u0012\u00020\n\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\nj\u0002`\u000b\u0012\u0004\u0012\u00020\u000c0\tj\u0002`\r0\tj\u0002`\u000eX\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started;",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "bill",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "cancelReason",
        "Lcom/squareup/ordermanagerdata/CancellationReason;",
        "selectedLineItems",
        "",
        "",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemRowIdentifier;",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemWithQuantityByIdentifier;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemSelectionsByUid;",
        "(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;)V",
        "getBill$orderhub_applet_release",
        "()Lcom/squareup/billhistory/model/BillHistory;",
        "getCancelReason$orderhub_applet_release",
        "()Lcom/squareup/ordermanagerdata/CancellationReason;",
        "getOrder$orderhub_applet_release",
        "()Lcom/squareup/orders/model/Order;",
        "getSelectedLineItems$orderhub_applet_release",
        "()Ljava/util/Map;",
        "component1",
        "component1$orderhub_applet_release",
        "component2",
        "component2$orderhub_applet_release",
        "component3",
        "component3$orderhub_applet_release",
        "component4",
        "component4$orderhub_applet_release",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bill:Lcom/squareup/billhistory/model/BillHistory;

.field private final cancelReason:Lcom/squareup/ordermanagerdata/CancellationReason;

.field private final order:Lcom/squareup/orders/model/Order;

.field private final selectedLineItems:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Lcom/squareup/ordermanagerdata/CancellationReason;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bill"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelReason"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedLineItems"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 75
    invoke-direct {p0, v0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->order:Lcom/squareup/orders/model/Order;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->cancelReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->selectedLineItems:Ljava/util/Map;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->order:Lcom/squareup/orders/model/Order;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->cancelReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->selectedLineItems:Ljava/util/Map;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->copy(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;)Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1$orderhub_applet_release()Lcom/squareup/orders/model/Order;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->order:Lcom/squareup/orders/model/Order;

    return-object v0
.end method

.method public final component2$orderhub_applet_release()Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    return-object v0
.end method

.method public final component3$orderhub_applet_release()Lcom/squareup/ordermanagerdata/CancellationReason;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->cancelReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    return-object v0
.end method

.method public final component4$orderhub_applet_release()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->selectedLineItems:Ljava/util/Map;

    return-object v0
.end method

.method public final copy(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;)Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Lcom/squareup/ordermanagerdata/CancellationReason;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;)",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bill"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelReason"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedLineItems"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->order:Lcom/squareup/orders/model/Order;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->order:Lcom/squareup/orders/model/Order;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->cancelReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->cancelReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->selectedLineItems:Ljava/util/Map;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->selectedLineItems:Ljava/util/Map;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBill$orderhub_applet_release()Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    return-object v0
.end method

.method public final getCancelReason$orderhub_applet_release()Lcom/squareup/ordermanagerdata/CancellationReason;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->cancelReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    return-object v0
.end method

.method public final getOrder$orderhub_applet_release()Lcom/squareup/orders/model/Order;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->order:Lcom/squareup/orders/model/Order;

    return-object v0
.end method

.method public final getSelectedLineItems$orderhub_applet_release()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->selectedLineItems:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->order:Lcom/squareup/orders/model/Order;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->cancelReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->selectedLineItems:Ljava/util/Map;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ForCancellationFlow(order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->order:Lcom/squareup/orders/model/Order;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", bill="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->bill:Lcom/squareup/billhistory/model/BillHistory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cancelReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->cancelReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedLineItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->selectedLineItems:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
