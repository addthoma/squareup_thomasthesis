.class final Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderMarkShippedWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;->render(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedInput;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ordermanagerdata/ResultState<",
        "+",
        "Lcom/squareup/orders/model/Order;",
        ">;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;",
        "result",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "Lcom/squareup/orders/model/Order;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$render$1;->$state:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ordermanagerdata/ResultState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lcom/squareup/orders/model/Order;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    instance-of v0, p1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$MarkShippedComplete;

    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/ResultState$Success;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orders/model/Order;

    invoke-direct {v1, p1}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$MarkShippedComplete;-><init>(Lcom/squareup/orders/model/Order;)V

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 124
    :cond_0
    instance-of v0, p1, Lcom/squareup/ordermanagerdata/ResultState$Failure$VersionMismatchError;

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 125
    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$MarkShippedFailed;

    sget-object v1, Lcom/squareup/ordermanagerdata/ResultState$Failure$VersionMismatchError;->INSTANCE:Lcom/squareup/ordermanagerdata/ResultState$Failure$VersionMismatchError;

    check-cast v1, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    invoke-static {v1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflowKt;->asOrderUpdateFailure(Lcom/squareup/ordermanagerdata/ResultState$Failure;)Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$MarkShippedFailed;-><init>(Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;)V

    .line 124
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 127
    :cond_1
    instance-of v0, p1, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    if-eqz v0, :cond_3

    .line 128
    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflowKt;->asOrderUpdateFailure(Lcom/squareup/ordermanagerdata/ResultState$Failure;)Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;

    move-result-object p1

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$render$1;->$state:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->getCanShowErrorDialog()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 130
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 131
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$render$1;->$state:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 132
    sget-object v7, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;->EDIT_TRACKING:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 133
    new-instance v4, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    .line 135
    new-instance v8, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryFulfillmentAction;

    iget-object v9, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$render$1;->$state:Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    invoke-virtual {v9}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->getMarkShippedAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryFulfillmentAction;-><init>(Lcom/squareup/protos/client/orders/Action;)V

    check-cast v8, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    .line 136
    sget-object v9, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CancelSaveTracking;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CancelSaveTracking;

    check-cast v9, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    .line 133
    invoke-direct {v4, p1, v8, v9}, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;-><init>(Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;)V

    const/4 v8, 0x0

    const/16 v9, 0x5b

    const/4 v10, 0x0

    .line 131
    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;->copy$default(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;Lcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Ljava/util/Map;Lcom/squareup/ordermanagerdata/TrackingInfo;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState$Companion$ShipmentAction;Lcom/squareup/protos/client/orders/Action;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedState;

    move-result-object p1

    const/4 v1, 0x2

    .line 130
    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 141
    :cond_2
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$MarkShippedFailed;

    invoke-direct {v1, p1}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$MarkShippedFailed;-><init>(Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;)V

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    .line 129
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow$render$1;->invoke(Lcom/squareup/ordermanagerdata/ResultState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
