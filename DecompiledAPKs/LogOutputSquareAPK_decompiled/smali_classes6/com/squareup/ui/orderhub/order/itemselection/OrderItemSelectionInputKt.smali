.class public final Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInputKt;
.super Ljava/lang/Object;
.source "OrderItemSelectionInput.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "initialState",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;",
        "getInitialState",
        "(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getInitialState(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;)Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;
    .locals 7

    const-string v0, "$this$initialState"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    new-instance v0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->getAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v4

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelectionsKt;->getAvailableLineItemsToRowIdentifiers(Lcom/squareup/orders/model/Order;)Ljava/util/Map;

    move-result-object v5

    .line 19
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionInput;->getPreSelectedLineItems()Ljava/util/Map;

    move-result-object v6

    const/4 v3, 0x0

    move-object v1, v0

    .line 14
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;-><init>(Lcom/squareup/orders/model/Order;ZLcom/squareup/protos/client/orders/Action;Ljava/util/Map;Ljava/util/Map;)V

    return-object v0
.end method
