.class public interface abstract Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope$Component;
.super Ljava/lang/Object;
.source "OrderHubBillHistoryScope.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope$BillHistoryModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0006\u001a\u00020\u0007H&J\u0008\u0010\u0008\u001a\u00020\tH&J\u0008\u0010\n\u001a\u00020\u000bH&\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope$Component;",
        "",
        "billHistoryDetailForOrderHubScreen",
        "Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Component;",
        "issueReceiptScreen",
        "Lcom/squareup/ui/activity/IssueReceiptScreen$Component;",
        "orderHubBillHistoryScopeRunner",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;",
        "selectGiftReceiptTenderScreen",
        "Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen$Component;",
        "selectReceiptTenderScreen",
        "Lcom/squareup/ui/activity/SelectReceiptTenderScreen$Component;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract billHistoryDetailForOrderHubScreen()Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Component;
.end method

.method public abstract issueReceiptScreen()Lcom/squareup/ui/activity/IssueReceiptScreen$Component;
.end method

.method public abstract orderHubBillHistoryScopeRunner()Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;
.end method

.method public abstract selectGiftReceiptTenderScreen()Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen$Component;
.end method

.method public abstract selectReceiptTenderScreen()Lcom/squareup/ui/activity/SelectReceiptTenderScreen$Component;
.end method
