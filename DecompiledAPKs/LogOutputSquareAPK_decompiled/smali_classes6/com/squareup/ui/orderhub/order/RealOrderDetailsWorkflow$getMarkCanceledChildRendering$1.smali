.class final Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkCanceledChildRendering$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderDetailsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->getMarkCanceledChildRendering(Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderDetailsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderDetailsWorkflow.kt\ncom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkCanceledChildRendering$1\n*L\n1#1,885:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        "result",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkCanceledChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "result"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 623
    instance-of v2, v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledSkipRefundComplete;

    if-eqz v2, :cond_0

    .line 624
    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 625
    new-instance v13, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    .line 626
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkCanceledChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->isReadOnly()Z

    move-result v4

    .line 627
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkCanceledChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->getShowOrderIdInActionBar()Z

    move-result v5

    .line 628
    check-cast v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledSkipRefundComplete;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledSkipRefundComplete;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x78

    const/4 v12, 0x0

    move-object v3, v13

    .line 625
    invoke-direct/range {v3 .. v12}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 630
    new-instance v3, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$IssuedInventoryAdjustment;

    .line 631
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledSkipRefundComplete;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v1

    .line 630
    invoke-direct {v3, v1}, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$IssuedInventoryAdjustment;-><init>(Lcom/squareup/orders/model/Order;)V

    .line 624
    invoke-virtual {v2, v13, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState(Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_0

    .line 634
    :cond_0
    instance-of v2, v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledRefundComplete;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 635
    new-instance v3, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$CancelOrder;

    .line 636
    check-cast v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledRefundComplete;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledRefundComplete;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v4

    .line 637
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledRefundComplete;->getBillHistory()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v5

    .line 638
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledRefundComplete;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v6

    .line 639
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledRefundComplete;->getCancellationReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v1

    .line 635
    invoke-direct {v3, v4, v5, v6, v1}, Lcom/squareup/ui/orderhub/order/OrderDetailsResult$CancelOrder;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;)V

    .line 634
    invoke-virtual {v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_0

    .line 642
    :cond_1
    instance-of v2, v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledInventoryAdjustmentFailed;

    const/4 v4, 0x0

    if-eqz v2, :cond_2

    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 643
    new-instance v15, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    .line 644
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkCanceledChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->getShowOrderIdInActionBar()Z

    move-result v7

    .line 645
    check-cast v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledInventoryAdjustmentFailed;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledInventoryAdjustmentFailed;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v8

    const/4 v9, 0x0

    .line 647
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkCanceledChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->isReadOnly()Z

    move-result v6

    .line 648
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledInventoryAdjustmentFailed;->getInventoryAdjustmentRetry()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;

    move-result-object v12

    .line 649
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledInventoryAdjustmentFailed;->getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object v11

    .line 650
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledInventoryAdjustmentFailed;->getInventoryAdjustmentRetry()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;->getCancelAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v10

    const/4 v13, 0x0

    const/16 v14, 0x80

    const/4 v1, 0x0

    move-object v5, v15

    move-object v3, v15

    move-object v15, v1

    .line 643
    invoke-direct/range {v5 .. v15}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v1, 0x2

    .line 642
    invoke-static {v2, v3, v4, v1, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_0

    .line 653
    :cond_2
    instance-of v2, v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledBillRetrievalFailed;

    if-eqz v2, :cond_5

    .line 654
    check-cast v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledBillRetrievalFailed;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledBillRetrievalFailed;->isBillFullyRefunded()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 655
    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 656
    new-instance v3, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    .line 657
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkCanceledChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->isReadOnly()Z

    move-result v6

    .line 658
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkCanceledChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->getShowOrderIdInActionBar()Z

    move-result v7

    .line 659
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledBillRetrievalFailed;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v8

    const/4 v9, 0x0

    .line 660
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledBillRetrievalFailed;->getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x68

    const/4 v14, 0x0

    move-object v5, v3

    .line 656
    invoke-direct/range {v5 .. v14}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v1, 0x2

    .line 655
    invoke-static {v2, v3, v4, v1, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto/16 :goto_0

    .line 664
    :cond_3
    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    const/4 v9, 0x0

    .line 667
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkCanceledChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->isReadOnly()Z

    move-result v6

    .line 668
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkCanceledChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->getShowOrderIdInActionBar()Z

    move-result v7

    .line 669
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledBillRetrievalFailed;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v8

    .line 670
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledBillRetrievalFailed;->getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    move-result-object v12

    .line 671
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledBillRetrievalFailed;->getBillRetrievalRetry()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->getCancellationReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v10

    .line 672
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledBillRetrievalFailed;->getBillRetrievalRetry()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v11

    const/4 v13, 0x1

    .line 665
    new-instance v1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    move-object v5, v1

    invoke-direct/range {v5 .. v13}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Z)V

    const/4 v3, 0x2

    .line 664
    invoke-static {v2, v1, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_0

    .line 671
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Required value was null."

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 677
    :cond_5
    instance-of v2, v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledVersionMismatch;

    if-eqz v2, :cond_6

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v2, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkCanceledChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    check-cast v2, Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-static {v2}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflowKt;->access$getVersionMismatchState(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v1, v2, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_0

    .line 678
    :cond_6
    instance-of v1, v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$GoBackFromMarkCanceled;

    if-eqz v1, :cond_7

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 679
    new-instance v2, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    .line 680
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkCanceledChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->isReadOnly()Z

    move-result v6

    .line 681
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkCanceledChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->getShowOrderIdInActionBar()Z

    move-result v7

    .line 682
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkCanceledChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkCanceledState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x78

    const/4 v14, 0x0

    move-object v5, v2

    .line 679
    invoke-direct/range {v5 .. v14}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v3, 0x2

    .line 678
    invoke-static {v1, v2, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_7
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkCanceledChildRendering$1;->invoke(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
