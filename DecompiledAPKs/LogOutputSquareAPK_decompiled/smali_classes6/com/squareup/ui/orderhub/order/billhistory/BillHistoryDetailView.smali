.class public final Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;
.super Landroid/widget/LinearLayout;
.source "BillHistoryDetailView.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\u0018\u00002\u00020\u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\r\u0010\u0016\u001a\u00020\u0017H\u0000\u00a2\u0006\u0002\u0008\u0018J\u0008\u0010\u0019\u001a\u00020\u001aH\u0014J\u0008\u0010\u001b\u001a\u00020\u001cH\u0016J\u0008\u0010\u001d\u001a\u00020\u001aH\u0014J\u0008\u0010\u001e\u001a\u00020\u001aH\u0014J\u0013\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u001a0 H\u0000\u00a2\u0006\u0002\u0008!J\u0013\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u001a0 H\u0000\u00a2\u0006\u0002\u0008#J\u0013\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u001a0 H\u0000\u00a2\u0006\u0002\u0008%J\u0013\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\u001a0 H\u0000\u00a2\u0006\u0002\u0008\'J\n\u0010(\u001a\u0004\u0018\u00010)H\u0015J\u0015\u0010*\u001a\u00020\u001a2\u0006\u0010+\u001a\u00020,H\u0000\u00a2\u0006\u0002\u0008-J\u0015\u0010.\u001a\u00020\u001a2\u0006\u0010/\u001a\u00020\u001cH\u0000\u00a2\u0006\u0002\u00080J\r\u00101\u001a\u00020\u001aH\u0000\u00a2\u0006\u0002\u00082J\r\u00103\u001a\u00020\u001aH\u0000\u00a2\u0006\u0002\u00084R\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000c\u001a\u00020\r8\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;",
        "Landroid/widget/LinearLayout;",
        "Lcom/squareup/workflow/ui/HandlesBack;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "billHistoryView",
        "Lcom/squareup/ui/activity/billhistory/BillHistoryView;",
        "presenter",
        "Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;",
        "getPresenter$orderhub_applet_release",
        "()Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;",
        "setPresenter$orderhub_applet_release",
        "(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;)V",
        "progressBar",
        "Landroid/widget/ProgressBar;",
        "shortAnimTimeMs",
        "",
        "getActionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "getActionBar$orderhub_applet_release",
        "onAttachedToWindow",
        "",
        "onBackPressed",
        "",
        "onDetachedFromWindow",
        "onFinishInflate",
        "onPrintGiftReceiptButtonClicked",
        "Lrx/Observable;",
        "onPrintGiftReceiptButtonClicked$orderhub_applet_release",
        "onReceiptButtonClicked",
        "onReceiptButtonClicked$orderhub_applet_release",
        "onRefundButtonClicked",
        "onRefundButtonClicked$orderhub_applet_release",
        "onReprintButtonClicked",
        "onReprintButtonClicked$orderhub_applet_release",
        "onSaveInstanceState",
        "Landroid/os/Parcelable;",
        "show",
        "bill",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "show$orderhub_applet_release",
        "showProgress",
        "visible",
        "showProgress$orderhub_applet_release",
        "temporarilySetPrintGiftReceiptButtonDisabled",
        "temporarilySetPrintGiftReceiptButtonDisabled$orderhub_applet_release",
        "temporarilySetReprintTicketButtonDisabled",
        "temporarilySetReprintTicketButtonDisabled$orderhub_applet_release",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

.field public presenter:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Landroid/widget/ProgressBar;

.field private final shortAnimTimeMs:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    const-class p2, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Component;

    .line 41
    invoke-interface {p1, p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Component;->inject(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;)V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->shortAnimTimeMs:I

    return-void
.end method


# virtual methods
.method public final getActionBar$orderhub_applet_release()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "actionBar.presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getPresenter$orderhub_applet_release()Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->presenter:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 55
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->presenter:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->presenter:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->onBackPressed$orderhub_applet_release()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->presenter:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 61
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 47
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 49
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 50
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_bill_history_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    .line 51
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_bill_history_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->progressBar:Landroid/widget/ProgressBar;

    return-void
.end method

.method public final onPrintGiftReceiptButtonClicked$orderhub_applet_release()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    if-nez v0, :cond_0

    const-string v1, "billHistoryView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->onPrintGiftReceiptButtonClicked()Lrx/Observable;

    move-result-object v0

    const-string v1, "billHistoryView.onPrintGiftReceiptButtonClicked()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onReceiptButtonClicked$orderhub_applet_release()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    if-nez v0, :cond_0

    const-string v1, "billHistoryView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->onReceiptButtonClicked()Lrx/Observable;

    move-result-object v0

    const-string v1, "billHistoryView.onReceiptButtonClicked()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onRefundButtonClicked$orderhub_applet_release()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    if-nez v0, :cond_0

    const-string v1, "billHistoryView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->onRefundButtonClicked()Lrx/Observable;

    move-result-object v0

    const-string v1, "billHistoryView.onRefundButtonClicked()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onReprintButtonClicked$orderhub_applet_release()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    if-nez v0, :cond_0

    const-string v1, "billHistoryView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->onReprintTicketButtonClicked()Lrx/Observable;

    move-result-object v0

    const-string v1, "billHistoryView.onReprintTicketButtonClicked()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 66
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented. Set saveEnabled to false."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final setPresenter$orderhub_applet_release(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->presenter:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;

    return-void
.end method

.method public final show$orderhub_applet_release(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 2

    const-string v0, "bill"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    if-nez v0, :cond_0

    const-string v1, "billHistoryView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->show(Lcom/squareup/billhistory/model/BillHistory;)V

    return-void
.end method

.method public final showProgress$orderhub_applet_release(Z)V
    .locals 1

    const-string v0, "progressBar"

    if-eqz p1, :cond_1

    .line 107
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->progressBar:Landroid/widget/ProgressBar;

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 109
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->progressBar:Landroid/widget/ProgressBar;

    if-nez p1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public final temporarilySetPrintGiftReceiptButtonDisabled$orderhub_applet_release()V
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    if-nez v0, :cond_0

    const-string v1, "billHistoryView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->temporarilyDisablePrintGiftReceiptButton()V

    return-void
.end method

.method public final temporarilySetReprintTicketButtonDisabled$orderhub_applet_release()V
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    if-nez v0, :cond_0

    const-string v1, "billHistoryView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->temporarilyDisableReprintTicketButton()V

    return-void
.end method
