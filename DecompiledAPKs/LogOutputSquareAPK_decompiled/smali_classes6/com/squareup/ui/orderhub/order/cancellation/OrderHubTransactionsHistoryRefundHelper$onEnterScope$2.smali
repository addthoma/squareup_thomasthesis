.class final Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper$onEnterScope$2;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderHubTransactionsHistoryRefundHelper.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Triple<",
        "+",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Finished;",
        "+",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundResult;",
        "+",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012(\u0010\u0002\u001a$\u0012\u0004\u0012\u00020\u0004\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00070\u00070\u0003H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lkotlin/Triple;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Finished;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundResult;",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflowRunner:Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper$onEnterScope$2;->$workflowRunner:Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lkotlin/Triple;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper$onEnterScope$2;->invoke(Lkotlin/Triple;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Triple;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Triple<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Finished;",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundResult;",
            "+",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started;",
            ">;)V"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-virtual {p1}, Lkotlin/Triple;->getThird()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started;

    .line 63
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;

    if-eqz v1, :cond_0

    .line 64
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper$onEnterScope$2;->$workflowRunner:Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;

    .line 65
    check-cast v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->getOrder$orderhub_applet_release()Lcom/squareup/orders/model/Order;

    move-result-object v2

    .line 66
    invoke-virtual {p1}, Lkotlin/Triple;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundResult;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundResult;->getBillHistory$orderhub_applet_release()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    .line 67
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->getCancelReason$orderhub_applet_release()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v3

    .line 68
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started$ForCancellationFlow;->getSelectedLineItems$orderhub_applet_release()Ljava/util/Map;

    move-result-object v0

    .line 64
    invoke-virtual {v1, v2, p1, v3, v0}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->restartWorkflowAfterCanceledOrder$orderhub_applet_release(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;)V

    :cond_0
    return-void
.end method
