.class public abstract Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;
.super Ljava/lang/Object;
.source "OrderHubCancellationProcessingScreen.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData$ShowingCancellationSpinner;,
        Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData$CancellationCompleted;,
        Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData$CancellationFailed;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0006\u0007\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;",
        "",
        "()V",
        "CancellationCompleted",
        "CancellationFailed",
        "ShowingCancellationSpinner",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData$ShowingCancellationSpinner;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData$CancellationCompleted;",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData$CancellationFailed;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/CancellationProcessingScreenData;-><init>()V

    return-void
.end method
