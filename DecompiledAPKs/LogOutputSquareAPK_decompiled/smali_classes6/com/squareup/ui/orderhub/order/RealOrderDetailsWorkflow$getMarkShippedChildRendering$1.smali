.class final Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkShippedChildRendering$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderDetailsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->getMarkShippedChildRendering(Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        "result",
        "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkShippedChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 740
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$MarkShippedComplete;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 741
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 742
    new-instance v3, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;

    .line 743
    iget-object v4, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkShippedChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    invoke-virtual {v4}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->isReadOnly()Z

    move-result v4

    .line 744
    iget-object v5, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkShippedChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getShowOrderIdInActionBar()Z

    move-result v5

    .line 745
    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$MarkShippedComplete;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$MarkShippedComplete;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p1

    .line 742
    invoke-direct {v3, v4, v5, p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RefreshingOrderState;-><init>(ZZLcom/squareup/orders/model/Order;)V

    .line 741
    invoke-static {v0, v3, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 748
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$MarkShippedFailed;

    if-eqz v0, :cond_2

    .line 749
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$MarkShippedFailed;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$MarkShippedFailed;->getOrderUpdateFailure()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;

    move-result-object v0

    sget-object v3, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$VersionMismatchError;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$VersionMismatchError;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_1

    .line 754
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 755
    new-instance v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    .line 756
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkShippedChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->isReadOnly()Z

    move-result v4

    .line 757
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkShippedChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getShowOrderIdInActionBar()Z

    move-result v5

    .line 758
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkShippedChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v9, 0x0

    .line 760
    new-instance v8, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    .line 761
    sget-object v3, Lcom/squareup/ordermanagerdata/ResultState$Failure$VersionMismatchError;->INSTANCE:Lcom/squareup/ordermanagerdata/ResultState$Failure$VersionMismatchError;

    check-cast v3, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    invoke-static {v3}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflowKt;->asOrderUpdateFailure(Lcom/squareup/ordermanagerdata/ResultState$Failure;)Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;

    move-result-object v3

    .line 762
    sget-object v10, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CurrentOrderUpdated;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CurrentOrderUpdated;

    check-cast v10, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    .line 760
    invoke-direct {v8, v3, v10, v2}, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;-><init>(Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;)V

    const/4 v10, 0x0

    const/16 v11, 0x48

    const/4 v12, 0x0

    move-object v3, v0

    .line 755
    invoke-direct/range {v3 .. v12}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 754
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 750
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 751
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected failure from mark shipped in detail workflow "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 750
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 768
    :cond_2
    instance-of p1, p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult$GoBackFromMarkShipped;

    if-eqz p1, :cond_3

    .line 769
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 770
    new-instance v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    .line 771
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkShippedChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->isReadOnly()Z

    move-result v4

    .line 772
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkShippedChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getShowOrderIdInActionBar()Z

    move-result v5

    .line 773
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkShippedChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v6

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x50

    const/4 v12, 0x0

    move-object v3, v0

    .line 770
    invoke-direct/range {v3 .. v12}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 769
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getMarkShippedChildRendering$1;->invoke(Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
