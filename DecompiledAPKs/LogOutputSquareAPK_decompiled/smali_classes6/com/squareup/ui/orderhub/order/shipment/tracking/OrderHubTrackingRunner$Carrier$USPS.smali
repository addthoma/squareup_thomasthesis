.class public final Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$USPS;
.super Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;
.source "OrderHubTrackingRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "USPS"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$USPS;",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;",
        "()V",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$USPS;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 309
    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$USPS;

    invoke-direct {v0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$USPS;-><init>()V

    sput-object v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$USPS;->INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$USPS;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const-string v0, "U.S. Postal Service"

    const/4 v1, 0x0

    .line 309
    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;-><init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
