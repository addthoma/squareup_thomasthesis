.class public final Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;
.super Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;
.source "OrderHubTransactionsHistoryRefundHelper.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/orderhub/OrderHubAppletScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubTransactionsHistoryRefundHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubTransactionsHistoryRefundHelper.kt\ncom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper\n+ 2 Rx2.kt\ncom/squareup/util/rx2/Rx2Kt\n*L\n1#1,116:1\n19#2:117\n19#2:118\n19#2:119\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubTransactionsHistoryRefundHelper.kt\ncom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper\n*L\n42#1:117\n54#1:118\n58#1:119\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002B\'\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0008\u0010\u000e\u001a\u00020\u000fH\u0016J\u0008\u0010\u0010\u001a\u00020\u000fH\u0016J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0015H\u0016JL\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\u0017\u001a\u00020\u001220\u0008\u0002\u0010\u0018\u001a*\u0012\u0004\u0012\u00020\u001a\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u001aj\u0002`\u001b\u0012\u0004\u0012\u00020\u001c0\u0019j\u0002`\u001d\u0018\u00010\u0019j\u0004\u0018\u0001`\u001e2\u0008\u0008\u0002\u0010\u001f\u001a\u00020 H\u0002J\u0010\u0010!\u001a\u00020\u000f2\u0006\u0010\"\u001a\u00020#H\u0016J\u0008\u0010$\u001a\u00020\u000fH\u0016R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;",
        "Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;",
        "Lmortar/Scoped;",
        "orderHubRefundFlowState",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;",
        "orderHubRefundFlowStarter",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;",
        "orderHubBillHistoryCreator",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;",
        "orderHubBillLoader",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;",
        "(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;)V",
        "compositeDisposable",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "canceledRefundFlow",
        "",
        "finishedRefundFlow",
        "ingestRefundsResponse",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "billHistory",
        "issueRefundsResponse",
        "Lcom/squareup/protos/client/bills/IssueRefundsResponse;",
        "initiateRefundFlow",
        "bill",
        "selectedLineItems",
        "",
        "",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemRowIdentifier;",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemWithQuantityByIdentifier;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemSelectionsByUid;",
        "skipRestock",
        "",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

.field private final orderHubBillHistoryCreator:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;

.field private final orderHubBillLoader:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

.field private final orderHubRefundFlowStarter:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;

.field private final orderHubRefundFlowState:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "orderHubRefundFlowState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubRefundFlowStarter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubBillHistoryCreator"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubBillLoader"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->orderHubRefundFlowState:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->orderHubRefundFlowStarter:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->orderHubBillHistoryCreator:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->orderHubBillLoader:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    .line 34
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    return-void
.end method

.method private final initiateRefundFlow(Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;Z)V"
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->orderHubRefundFlowStarter:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;->initiateRefundFlow$orderhub_applet_release(Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;Z)V

    return-void
.end method

.method static synthetic initiateRefundFlow$default(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 110
    check-cast p2, Ljava/util/Map;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x1

    .line 111
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->initiateRefundFlow(Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;Z)V

    return-void
.end method


# virtual methods
.method public canceledRefundFlow()V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->orderHubRefundFlowState:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->cancelRefundFlow()V

    return-void
.end method

.method public finishedRefundFlow()V
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->orderHubRefundFlowState:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->leaveRefundFlow()V

    return-void
.end method

.method public ingestRefundsResponse(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/IssueRefundsResponse;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 2

    const-string v0, "billHistory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "issueRefundsResponse"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->orderHubBillHistoryCreator:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/IssueRefundsResponse;->bill:Lcom/squareup/protos/client/bills/Bill;

    const-string v1, "issueRefundsResponse.bill"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;->toRefundBill$orderhub_applet_release(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    .line 89
    iget-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->orderHubRefundFlowState:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->refundedOrder(Lcom/squareup/billhistory/model/BillHistory;)V

    .line 90
    iget-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->orderHubBillLoader:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->loadFromBill$orderhub_applet_release(Lcom/squareup/billhistory/model/BillHistory;)V

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 6

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    sget-object v0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;->Companion:Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$Companion;->getRunner(Lmortar/MortarScope;)Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->orderHubRefundFlowState:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    .line 41
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->refundFlowState()Lio/reactivex/Observable;

    move-result-object v1

    .line 117
    const-class v2, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "ofType(T::class.java)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    new-instance v3, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper$onEnterScope$1;

    invoke-direct {v3, p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper$onEnterScope$1;-><init>(Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, p1, v3}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 52
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->orderHubRefundFlowState:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    .line 53
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->refundFlowState()Lio/reactivex/Observable;

    move-result-object v1

    .line 118
    const-class v3, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Finished;

    invoke-virtual {v1, v3}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->orderHubRefundFlowState:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->refundResult()Lio/reactivex/Observable;

    move-result-object v3

    check-cast v3, Lio/reactivex/ObservableSource;

    .line 57
    iget-object v4, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->orderHubRefundFlowState:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    invoke-virtual {v4}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->refundFlowState()Lio/reactivex/Observable;

    move-result-object v4

    .line 119
    const-class v5, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState$RefundFlowState$Started;

    invoke-virtual {v4, v5}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v4

    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lio/reactivex/ObservableSource;

    .line 55
    invoke-static {v1, v3, v4}, Lcom/squareup/util/rx2/RxKotlinKt;->withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v1

    .line 60
    new-instance v2, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper$onEnterScope$2;

    invoke-direct {v2, v0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper$onEnterScope$2;-><init>(Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, p1, v2}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubTransactionsHistoryRefundHelper;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method
