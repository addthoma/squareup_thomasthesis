.class public final Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;
.super Ljava/lang/Object;
.source "OrderHubOrderDetailsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000t\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001Bq\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0008\u0008\u0001\u0010\u0017\u001a\u00020\u0018\u0012\u0008\u0008\u0001\u0010\u0019\u001a\u00020\u001a\u00a2\u0006\u0002\u0010\u001bJ*\u0010\u001c\u001a\u00020\u001d2\"\u0010\u001e\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020!\u0012\u0004\u0012\u00020\"0 j\u0008\u0012\u0004\u0012\u00020!`#0\u001fR\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "dateTimeFormatter",
        "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
        "lineItemSubtitleFormatter",
        "Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;",
        "orderPrintingDispatcher",
        "Lcom/squareup/print/OrderPrintingDispatcher;",
        "printerStations",
        "Lcom/squareup/print/PrinterStations;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "badMaybeSquareDeviceCheck",
        "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
        "orderHubAnalytics",
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
        "emailAppAvailable",
        "",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/print/PrinterStations;Lcom/squareup/time/CurrentTime;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;ZLio/reactivex/Scheduler;)V",
        "create",
        "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final dateTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

.field private final emailAppAvailable:Z

.field private final lineItemSubtitleFormatter:Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/print/PrinterStations;Lcom/squareup/time/CurrentTime;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;ZLio/reactivex/Scheduler;)V
    .locals 1
    .param p11    # Z
        .annotation runtime Lcom/squareup/ui/orderhub/EmailAppAvailable;
        .end annotation
    .end param
    .param p12    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
            "Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/util/BrowserLauncher;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            "Z",
            "Lio/reactivex/Scheduler;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateTimeFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lineItemSubtitleFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderPrintingDispatcher"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "printerStations"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "browserLauncher"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "badMaybeSquareDeviceCheck"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubAnalytics"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->dateTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->lineItemSubtitleFormatter:Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->printerStations:Lcom/squareup/print/PrinterStations;

    iput-object p7, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object p8, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iput-object p9, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    iput-object p10, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    iput-boolean p11, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->emailAppAvailable:Z

    iput-object p12, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->mainScheduler:Lio/reactivex/Scheduler;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;"
        }
    .end annotation

    move-object/from16 v0, p0

    const-string v1, "screens"

    move-object/from16 v7, p1

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    new-instance v1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;

    .line 151
    iget-object v3, v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->res:Lcom/squareup/util/Res;

    .line 152
    iget-object v4, v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 153
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->dateTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    .line 154
    iget-object v6, v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->lineItemSubtitleFormatter:Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;

    .line 156
    iget-object v8, v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    .line 157
    iget-object v9, v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 158
    iget-object v10, v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 159
    iget-object v11, v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    .line 160
    iget-object v12, v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    .line 161
    iget-object v13, v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    .line 162
    iget-boolean v14, v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->emailAppAvailable:Z

    .line 163
    iget-object v15, v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;->mainScheduler:Lio/reactivex/Scheduler;

    move-object v2, v1

    .line 150
    invoke-direct/range {v2 .. v15}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;Lio/reactivex/Observable;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/print/PrinterStations;Lcom/squareup/time/CurrentTime;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;ZLio/reactivex/Scheduler;)V

    return-object v1
.end method
