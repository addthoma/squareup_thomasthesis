.class public final Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItemModifier;
.super Ljava/lang/Object;
.source "PrintableOrderHubOrderItemModifier.kt"

# interfaces
.implements Lcom/squareup/print/PrintableOrderItemModifier;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\t\u001a\u00020\nX\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItemModifier;",
        "Lcom/squareup/print/PrintableOrderItemModifier;",
        "modifier",
        "Lcom/squareup/orders/model/Order$LineItem$Modifier;",
        "(Lcom/squareup/orders/model/Order$LineItem$Modifier;)V",
        "basePriceTimesModifierQuantity",
        "Lcom/squareup/protos/common/Money;",
        "getBasePriceTimesModifierQuantity",
        "()Lcom/squareup/protos/common/Money;",
        "hideFromCustomer",
        "",
        "getHideFromCustomer",
        "()Z",
        "getDisplayName",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final basePriceTimesModifierQuantity:Lcom/squareup/protos/common/Money;

.field private final hideFromCustomer:Z

.field private final modifier:Lcom/squareup/orders/model/Order$LineItem$Modifier;


# direct methods
.method public constructor <init>(Lcom/squareup/orders/model/Order$LineItem$Modifier;)V
    .locals 1

    const-string v0, "modifier"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItemModifier;->modifier:Lcom/squareup/orders/model/Order$LineItem$Modifier;

    .line 14
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItemModifier;->modifier:Lcom/squareup/orders/model/Order$LineItem$Modifier;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem$Modifier;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    const-string v0, "modifier.total_price_money"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItemModifier;->basePriceTimesModifierQuantity:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public getBasePriceTimesModifierQuantity()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItemModifier;->basePriceTimesModifierQuantity:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getDisplayName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    iget-object p1, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItemModifier;->modifier:Lcom/squareup/orders/model/Order$LineItem$Modifier;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem$Modifier;->name:Ljava/lang/String;

    const-string v0, "modifier.name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public getHideFromCustomer()Z
    .locals 1

    .line 12
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubOrderItemModifier;->hideFromCustomer:Z

    return v0
.end method
