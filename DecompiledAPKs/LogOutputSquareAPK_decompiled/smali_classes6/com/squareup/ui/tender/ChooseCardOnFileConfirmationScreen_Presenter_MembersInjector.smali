.class public final Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;
.super Ljava/lang/Object;
.source "ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final customerSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderCompleterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->customerSettingsProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->transactionProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p3, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->tenderCompleterProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p4, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p5, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->settingsProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p6, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->tenderScopeRunnerProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p7, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->tenderInEditProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;",
            ">;"
        }
    .end annotation

    .line 59
    new-instance v8, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static injectAnalytics(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;Lcom/squareup/analytics/Analytics;)V
    .locals 0

    .line 93
    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static injectCustomerSettings(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;Lcom/squareup/crm/CustomerManagementSettings;)V
    .locals 0

    .line 75
    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;->customerSettings:Lcom/squareup/crm/CustomerManagementSettings;

    return-void
.end method

.method public static injectSettings(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method public static injectTenderCompleter(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;Lcom/squareup/tenderpayment/TenderCompleter;)V
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    return-void
.end method

.method public static injectTenderInEdit(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;Lcom/squareup/payment/TenderInEdit;)V
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    return-void
.end method

.method public static injectTenderScopeRunner(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;Lcom/squareup/tenderpayment/TenderScopeRunner;)V
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;->tenderScopeRunner:Lcom/squareup/tenderpayment/TenderScopeRunner;

    return-void
.end method

.method public static injectTransaction(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;Lcom/squareup/payment/Transaction;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->customerSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/crm/CustomerManagementSettings;

    invoke-static {p1, v0}, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->injectCustomerSettings(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;Lcom/squareup/crm/CustomerManagementSettings;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/Transaction;

    invoke-static {p1, v0}, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->injectTransaction(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;Lcom/squareup/payment/Transaction;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->tenderCompleterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/TenderCompleter;

    invoke-static {p1, v0}, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->injectTenderCompleter(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;Lcom/squareup/tenderpayment/TenderCompleter;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->injectAnalytics(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;Lcom/squareup/analytics/Analytics;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {p1, v0}, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->injectSettings(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;Lcom/squareup/settings/server/AccountStatusSettings;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->tenderScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/TenderScopeRunner;

    invoke-static {p1, v0}, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->injectTenderScopeRunner(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;Lcom/squareup/tenderpayment/TenderScopeRunner;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/TenderInEdit;

    invoke-static {p1, v0}, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->injectTenderInEdit(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;Lcom/squareup/payment/TenderInEdit;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen_Presenter_MembersInjector;->injectMembers(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;)V

    return-void
.end method
