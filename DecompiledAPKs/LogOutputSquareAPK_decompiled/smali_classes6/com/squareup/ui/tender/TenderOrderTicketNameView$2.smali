.class Lcom/squareup/ui/tender/TenderOrderTicketNameView$2;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "TenderOrderTicketNameView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/tender/TenderOrderTicketNameView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/tender/TenderOrderTicketNameView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/tender/TenderOrderTicketNameView;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView$2;->this$0:Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x6

    if-ne p2, p1, :cond_0

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNameView$2;->this$0:Lcom/squareup/ui/tender/TenderOrderTicketNameView;

    iget-object p1, p1, Lcom/squareup/ui/tender/TenderOrderTicketNameView;->presenter:Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->onCommitOrderName()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
