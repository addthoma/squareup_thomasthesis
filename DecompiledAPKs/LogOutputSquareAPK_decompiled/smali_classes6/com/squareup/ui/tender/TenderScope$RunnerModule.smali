.class abstract Lcom/squareup/ui/tender/TenderScope$RunnerModule;
.super Ljava/lang/Object;
.source "TenderScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/TenderScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "RunnerModule"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/tender/TenderScope;


# direct methods
.method constructor <init>(Lcom/squareup/ui/tender/TenderScope;)V
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/squareup/ui/tender/TenderScope$RunnerModule;->this$0:Lcom/squareup/ui/tender/TenderScope;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideTenderScopeRunner(Lcom/squareup/ui/tender/RealTenderScopeRunner;)Lcom/squareup/tenderpayment/TenderScopeRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
