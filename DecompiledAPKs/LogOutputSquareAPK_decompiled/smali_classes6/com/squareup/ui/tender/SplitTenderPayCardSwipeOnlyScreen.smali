.class public final Lcom/squareup/ui/tender/SplitTenderPayCardSwipeOnlyScreen;
.super Lcom/squareup/ui/tender/InTenderScope;
.source "SplitTenderPayCardSwipeOnlyScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/tender/PayCardSwipeOnlyScreen$Component;
.end annotation

.annotation runtime Lcom/squareup/ui/tender/RequiresPayment;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/tender/SplitTenderPayCardSwipeOnlyScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/tender/SplitTenderPayCardSwipeOnlyScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/ui/tender/SplitTenderPayCardSwipeOnlyScreen;

    invoke-direct {v0}, Lcom/squareup/ui/tender/SplitTenderPayCardSwipeOnlyScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/tender/SplitTenderPayCardSwipeOnlyScreen;->INSTANCE:Lcom/squareup/ui/tender/SplitTenderPayCardSwipeOnlyScreen;

    .line 23
    sget-object v0, Lcom/squareup/ui/tender/SplitTenderPayCardSwipeOnlyScreen;->INSTANCE:Lcom/squareup/ui/tender/SplitTenderPayCardSwipeOnlyScreen;

    .line 24
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/tender/SplitTenderPayCardSwipeOnlyScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 31
    sget-object v0, Lcom/squareup/ui/tender/TenderScope;->INSTANCE:Lcom/squareup/ui/tender/TenderScope;

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/InTenderScope;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 27
    sget v0, Lcom/squareup/ui/tender/legacy/R$layout;->pay_card_swipe_only_view:I

    return v0
.end method
