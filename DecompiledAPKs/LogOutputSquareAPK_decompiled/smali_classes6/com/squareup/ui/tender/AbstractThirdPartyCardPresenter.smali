.class public abstract Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;
.super Lmortar/ViewPresenter;
.source "AbstractThirdPartyCardPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/tender/PayThirdPartyCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 26
    iput-object p3, p0, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private getCustomAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 81
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/PayThirdPartyCardView;

    invoke-virtual {v0}, Lcom/squareup/ui/tender/PayThirdPartyCardView;->getInputText()Ljava/lang/String;

    .line 82
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/PayThirdPartyCardView;

    invoke-virtual {v0}, Lcom/squareup/ui/tender/PayThirdPartyCardView;->getInputText()Ljava/lang/String;

    move-result-object v0

    .line 83
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0

    .line 86
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    invoke-virtual {v1, v0}, Lcom/squareup/money/MoneyLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-nez v0, :cond_1

    .line 88
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method private getTipAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 5

    .line 94
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 95
    iget-object v0, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private isAmountEnough(Lcom/squareup/protos/common/Money;)Z
    .locals 1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 100
    invoke-static {p1, v0}, Lcom/squareup/money/MoneyMath;->greaterThanOrEqualTo(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    return p1
.end method


# virtual methods
.method abstract getAmountDue()Lcom/squareup/protos/common/Money;
.end method

.method onBackPressed()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 34
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 35
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/PayThirdPartyCardView;

    .line 36
    invoke-virtual {v0}, Lcom/squareup/ui/tender/PayThirdPartyCardView;->showButton()V

    .line 38
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 39
    iget-object v2, p0, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/tender/PayThirdPartyCardView;->configureAmount(Lcom/squareup/money/MoneyLocaleHelper;)V

    .line 40
    iget-object v2, p0, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v2, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/tender/PayThirdPartyCardView;->updateAmountDue(Ljava/lang/CharSequence;)V

    if-nez p1, :cond_0

    .line 43
    invoke-virtual {v0}, Lcom/squareup/ui/tender/PayThirdPartyCardView;->requestInitialFocus()V

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->update()V

    return-void
.end method

.method onRecordButtonClicked()V
    .locals 1

    .line 49
    invoke-direct {p0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->getCustomAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 50
    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->isAmountEnough(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 51
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/PayThirdPartyCardView;

    invoke-virtual {v0}, Lcom/squareup/ui/tender/PayThirdPartyCardView;->hideSoftKeyboard()V

    .line 52
    invoke-direct {p0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->getCustomAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->getTipAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 53
    invoke-virtual {p0, v0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->record(Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method abstract record(Lcom/squareup/protos/common/Money;)V
.end method

.method update()V
    .locals 6

    .line 57
    invoke-direct {p0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->getCustomAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 58
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 59
    iget-object v2, p0, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v2, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 60
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/tender/PayThirdPartyCardView;

    .line 61
    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->isAmountEnough(Lcom/squareup/protos/common/Money;)Z

    move-result v3

    .line 62
    invoke-virtual {v2, v3}, Lcom/squareup/ui/tender/PayThirdPartyCardView;->enableRecordPayment(Z)V

    if-eqz v3, :cond_0

    .line 64
    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->getTipAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 66
    iget-object v3, p0, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/checkout/R$string;->buyer_amount_tip_action_bar:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 67
    invoke-virtual {v2}, Lcom/squareup/ui/tender/PayThirdPartyCardView;->getContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v4, v5}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v1

    const-string v4, "amount"

    invoke-virtual {v3, v4, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v3, p0, Lcom/squareup/ui/tender/AbstractThirdPartyCardPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 68
    invoke-interface {v3, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v3, "tip"

    invoke-virtual {v1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 66
    invoke-virtual {v2, v0}, Lcom/squareup/ui/tender/PayThirdPartyCardView;->setUpTitle(Ljava/lang/CharSequence;)V

    return-void

    .line 73
    :cond_0
    invoke-virtual {v2, v1}, Lcom/squareup/ui/tender/PayThirdPartyCardView;->setUpTitle(Ljava/lang/CharSequence;)V

    return-void
.end method
