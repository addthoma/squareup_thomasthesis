.class Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$CardholderNameListener;
.super Ljava/lang/Object;
.source "TenderOrderTicketNamePresenter.java"

# interfaces
.implements Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CardholderNameListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;)V
    .locals 0

    .line 477
    iput-object p1, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$CardholderNameListener;->this$0:Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$1;)V
    .locals 0

    .line 477
    invoke-direct {p0, p1}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$CardholderNameListener;-><init>(Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;)V

    return-void
.end method


# virtual methods
.method public onFailure()V
    .locals 1

    .line 484
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$CardholderNameListener;->this$0:Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->handleCardholderError()V

    return-void
.end method

.method public onNameReceived(Ljava/lang/String;)V
    .locals 1

    .line 480
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$CardholderNameListener;->this$0:Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->processNameReceived(Ljava/lang/String;)V

    return-void
.end method

.method public onTerminated(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
    .locals 2

    .line 488
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->PAYMENT_CANCELED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 490
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 491
    invoke-static {p1}, Lcom/squareup/cardreader/StandardMessageResources;->forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object v1

    iget v1, v1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->messageId:I

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 492
    invoke-static {p1}, Lcom/squareup/cardreader/StandardMessageResources;->forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->localizedMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedMessage(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 493
    invoke-static {p1}, Lcom/squareup/cardreader/StandardMessageResources;->forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object v1

    iget v1, v1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->titleId:I

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 494
    invoke-static {p1}, Lcom/squareup/cardreader/StandardMessageResources;->forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->localizedTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedTitle(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 495
    invoke-static {p1}, Lcom/squareup/cardreader/StandardMessageResources;->forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 496
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    .line 497
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter$CardholderNameListener;->this$0:Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;

    invoke-static {v0}, Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;->access$200(Lcom/squareup/ui/tender/TenderOrderTicketNamePresenter;)Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void
.end method
