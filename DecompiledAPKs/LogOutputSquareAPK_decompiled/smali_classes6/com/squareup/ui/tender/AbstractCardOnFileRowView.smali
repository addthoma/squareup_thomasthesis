.class public abstract Lcom/squareup/ui/tender/AbstractCardOnFileRowView;
.super Lcom/squareup/noho/NohoLinearLayout;
.source "AbstractCardOnFileRowView.java"


# instance fields
.field currencyCode:Lcom/squareup/protos/common/CurrencyCode;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/squareup/noho/NohoLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x1

    .line 35
    invoke-virtual {p0, p1}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->setOrientation(I)V

    return-void
.end method

.method private newCustomerRow()Lcom/squareup/marin/widgets/MarinGlyphTextView;
    .locals 1

    .line 117
    sget v0, Lcom/squareup/ui/tender/legacy/R$layout;->customer_card_on_file_row:I

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 118
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->removeAllViews()V

    .line 119
    invoke-virtual {p0, v0}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->addView(Landroid/view/View;)V

    return-object v0
.end method


# virtual methods
.method addCardRow(IZLcom/squareup/Card$Brand;Ljava/lang/String;Z)V
    .locals 4

    .line 51
    sget v0, Lcom/squareup/ui/tender/legacy/R$layout;->pay_card_on_file_card_row:I

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    if-gtz p1, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->removeAllViews()V

    .line 55
    :cond_0
    invoke-virtual {p0, v0}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->addView(Landroid/view/View;)V

    .line 57
    sget v1, Lcom/squareup/ui/tender/legacy/R$id;->card_on_file_card_name_number:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 58
    sget v2, Lcom/squareup/ui/tender/legacy/R$id;->card_on_file_charge_button:I

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    .line 59
    sget v3, Lcom/squareup/ui/tender/legacy/R$id;->card_on_file_card_expired_text:I

    invoke-static {v0, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 61
    iget-object v3, p0, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p3, v3}, Lcom/squareup/util/ProtoGlyphs;->card(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p3

    const/4 v3, 0x0

    invoke-virtual {v1, p3, v3}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;I)V

    .line 63
    invoke-static {v0, p5}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    xor-int/lit8 p3, p5, 0x1

    .line 64
    invoke-static {v2, p3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-nez p2, :cond_1

    .line 67
    invoke-virtual {v1, p4}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 68
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setEnabled(Z)V

    .line 69
    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    return-void

    .line 73
    :cond_1
    invoke-virtual {v1, p4}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    new-instance p2, Lcom/squareup/ui/tender/AbstractCardOnFileRowView$1;

    invoke-direct {p2, p0, p1, p4}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView$1;-><init>(Lcom/squareup/ui/tender/AbstractCardOnFileRowView;ILjava/lang/String;)V

    invoke-virtual {v2, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method addCustomerRow(I)Lcom/squareup/marin/widgets/MarinGlyphTextView;
    .locals 1

    .line 111
    invoke-direct {p0}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->newCustomerRow()Lcom/squareup/marin/widgets/MarinGlyphTextView;

    move-result-object v0

    .line 112
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(I)V

    return-object v0
.end method

.method addCustomerRow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinGlyphTextView;
    .locals 2

    .line 101
    invoke-direct {p0}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->newCustomerRow()Lcom/squareup/marin/widgets/MarinGlyphTextView;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->removeAllGlyphs()V

    .line 103
    sget-object v1, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 104
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setHint(Ljava/lang/CharSequence;)V

    const/4 p1, 0x0

    .line 105
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x0

    .line 106
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setEnabled(Z)V

    return-object v0
.end method

.method addNoCardsRow()V
    .locals 2

    .line 84
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->removeAllViews()V

    .line 85
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/ui/tender/legacy/R$layout;->customer_no_cards_on_file_row:I

    invoke-static {v0, v1, p0}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method addNoGiftCardsRow()V
    .locals 2

    .line 89
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->removeAllViews()V

    .line 90
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/ui/tender/legacy/R$layout;->customer_no_cards_on_file_row:I

    invoke-static {v0, v1, p0}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 91
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->customer_cards_on_file:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    .line 92
    sget v1, Lcom/squareup/ui/tender/legacy/R$string;->customer_no_gift_cards_on_file:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    return-void
.end method

.method addOfflineRow()V
    .locals 2

    .line 96
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->removeAllViews()V

    .line 97
    invoke-virtual {p0}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/ui/tender/legacy/R$layout;->card_on_file_offline_not_available:I

    invoke-static {v0, v1, p0}, Lcom/squareup/ui/tender/AbstractCardOnFileRowView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method protected abstract onChargeCardOnFileClicked(ILjava/lang/String;)V
.end method
