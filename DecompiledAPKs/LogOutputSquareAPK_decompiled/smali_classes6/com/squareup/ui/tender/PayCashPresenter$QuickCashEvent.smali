.class Lcom/squareup/ui/tender/PayCashPresenter$QuickCashEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "PayCashPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/PayCashPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QuickCashEvent"
.end annotation


# instance fields
.field final index:I

.field final valueCents:J


# direct methods
.method constructor <init>(IJ)V
    .locals 1

    .line 210
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PAYMENT_FLOW_QUICK_CASH_OPTION:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 211
    iput p1, p0, Lcom/squareup/ui/tender/PayCashPresenter$QuickCashEvent;->index:I

    .line 212
    iput-wide p2, p0, Lcom/squareup/ui/tender/PayCashPresenter$QuickCashEvent;->valueCents:J

    return-void
.end method
