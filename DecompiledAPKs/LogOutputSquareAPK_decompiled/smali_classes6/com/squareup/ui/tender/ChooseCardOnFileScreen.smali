.class public Lcom/squareup/ui/tender/ChooseCardOnFileScreen;
.super Lcom/squareup/ui/tender/InTenderScope;
.source "ChooseCardOnFileScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Component;,
        Lcom/squareup/ui/tender/ChooseCardOnFileScreen$Presenter;,
        Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;
    }
.end annotation


# instance fields
.field private final cardTypeToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

.field private final forCustomerInTransaction:Z


# direct methods
.method private constructor <init>(ZLcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;)V
    .locals 1

    .line 70
    sget-object v0, Lcom/squareup/ui/tender/TenderScope;->INSTANCE:Lcom/squareup/ui/tender/TenderScope;

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/InTenderScope;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V

    .line 71
    iput-boolean p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen;->forCustomerInTransaction:Z

    .line 72
    iput-object p2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen;->cardTypeToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/tender/ChooseCardOnFileScreen;)Z
    .locals 0

    .line 43
    iget-boolean p0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen;->forCustomerInTransaction:Z

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/tender/ChooseCardOnFileScreen;)Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen;->cardTypeToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    return-object p0
.end method

.method public static forCustomerInTransaction(Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;)Lcom/squareup/ui/tender/ChooseCardOnFileScreen;
    .locals 2

    .line 63
    new-instance v0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen;-><init>(ZLcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;)V

    return-object v0
.end method

.method public static forSelectedCustomer(Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;)Lcom/squareup/ui/tender/ChooseCardOnFileScreen;
    .locals 2

    .line 56
    new-instance v0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen;-><init>(ZLcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;)V

    return-object v0
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public screenLayout()I
    .locals 1

    .line 252
    sget v0, Lcom/squareup/ui/tender/legacy/R$layout;->crm_choose_card_on_file_view:I

    return v0
.end method
