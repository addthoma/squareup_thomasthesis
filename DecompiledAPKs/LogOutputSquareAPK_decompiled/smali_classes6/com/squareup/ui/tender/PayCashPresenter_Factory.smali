.class public final Lcom/squareup/ui/tender/PayCashPresenter_Factory;
.super Ljava/lang/Object;
.source "PayCashPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/tender/PayCashPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final quickCashCalculatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/QuickCashCalculator;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final softInputPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderCompleterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/QuickCashCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;)V"
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p2, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p3, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p4, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->quickCashCalculatorProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p5, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p6, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->softInputPresenterProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p7, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p8, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->tenderCompleterProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p9, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p10, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/tender/PayCashPresenter_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/QuickCashCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;)",
            "Lcom/squareup/ui/tender/PayCashPresenter_Factory;"
        }
    .end annotation

    .line 79
    new-instance v11, Lcom/squareup/ui/tender/PayCashPresenter_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/tender/PayCashPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/money/QuickCashCalculator;Lcom/squareup/util/Device;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tenderpayment/TenderScopeRunner;)Lcom/squareup/ui/tender/PayCashPresenter;
    .locals 12

    .line 86
    new-instance v11, Lcom/squareup/ui/tender/PayCashPresenter;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/tender/PayCashPresenter;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/money/QuickCashCalculator;Lcom/squareup/util/Device;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tenderpayment/TenderScopeRunner;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/ui/tender/PayCashPresenter;
    .locals 11

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->quickCashCalculatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/money/QuickCashCalculator;

    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->softInputPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/SoftInputPresenter;

    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->tenderCompleterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/tenderpayment/TenderCompleter;

    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/payment/TenderInEdit;

    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/tenderpayment/TenderScopeRunner;

    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/money/QuickCashCalculator;Lcom/squareup/util/Device;Lcom/squareup/ui/SoftInputPresenter;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/tenderpayment/TenderScopeRunner;)Lcom/squareup/ui/tender/PayCashPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/tender/PayCashPresenter_Factory;->get()Lcom/squareup/ui/tender/PayCashPresenter;

    move-result-object v0

    return-object v0
.end method
