.class public Lcom/squareup/ui/timecards/ClockOutCoordinator;
.super Lcom/squareup/ui/timecards/TimecardsCoordinator;
.source "ClockOutCoordinator.java"


# static fields
.field private static final TABLE_ROW_PARAMS:Landroid/widget/LinearLayout$LayoutParams;


# instance fields
.field private clockOutSummaryTable:Landroid/widget/TableLayout;

.field private clockOutSummaryView:Landroid/view/View;

.field private featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

.field private layoutInflater:Landroid/view/LayoutInflater;

.field private locale:Ljava/util/Locale;

.field private placeholderView:Landroid/view/View;

.field private successButtonsView:Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;

.field private successContainer:Landroid/view/View;

.field private successMessage:Lcom/squareup/marketfont/MarketTextView;

.field private successTitle:Lcom/squareup/marketfont/MarketTextView;

.field private successView:Landroid/view/View;

.field private viewNotesButton:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 56
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    sput-object v0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->TABLE_ROW_PARAMS:Landroid/widget/LinearLayout$LayoutParams;

    return-void
.end method

.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/timecards/FeatureReleaseHelper;Ljava/util/Locale;)V
    .locals 0
    .param p4    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 63
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/timecards/TimecardsCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V

    .line 64
    iput-object p6, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    .line 65
    iput-object p7, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->locale:Ljava/util/Locale;

    return-void
.end method

.method private createTableDataCell(Landroid/widget/TableRow;Ljava/lang/String;IZZ)V
    .locals 7

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move v5, p4

    move v6, p5

    .line 291
    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->createTableDataCell(Landroid/widget/TableRow;Ljava/lang/String;Ljava/lang/String;IZZ)V

    return-void
.end method

.method private createTableDataCell(Landroid/widget/TableRow;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 4

    .line 297
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->layoutInflater:Landroid/view/LayoutInflater;

    sget v1, Lcom/squareup/ui/timecards/R$layout;->timecards_clockout_summary_data_cell:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    if-eqz p3, :cond_0

    .line 300
    new-instance v1, Landroid/text/SpannableStringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 302
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    .line 303
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p3

    add-int/2addr p3, p2

    .line 304
    new-instance v2, Landroid/text/style/SuperscriptSpan;

    invoke-direct {v2}, Landroid/text/style/SuperscriptSpan;-><init>()V

    const/16 v3, 0x11

    invoke-virtual {v1, v2, p2, p3, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 306
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 308
    :cond_0
    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    if-nez p4, :cond_1

    const p2, 0x800013

    .line 311
    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setGravity(I)V

    :cond_1
    if-eqz p5, :cond_2

    const/4 p2, 0x2

    if-eq p4, p2, :cond_3

    :cond_2
    if-nez p5, :cond_4

    const/4 p2, 0x3

    if-ne p4, p2, :cond_4

    :cond_3
    const p2, 0x800015

    .line 314
    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setGravity(I)V

    :cond_4
    if-eqz p6, :cond_5

    .line 317
    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 319
    :cond_5
    invoke-virtual {p1, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    return-void
.end method

.method private createTableHeaderCell(Landroid/view/LayoutInflater;Landroid/widget/TableRow;Ljava/lang/String;IZ)V
    .locals 2

    .line 221
    sget v0, Lcom/squareup/ui/timecards/R$layout;->timecards_clockout_summary_header_cell:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    .line 223
    invoke-virtual {p1, p3}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    if-nez p4, :cond_0

    const p3, 0x800013

    .line 225
    invoke-virtual {p1, p3}, Lcom/squareup/marketfont/MarketTextView;->setGravity(I)V

    :cond_0
    if-eqz p5, :cond_1

    const/4 p3, 0x2

    if-eq p4, p3, :cond_2

    :cond_1
    if-nez p5, :cond_3

    const/4 p3, 0x3

    if-ne p4, p3, :cond_3

    :cond_2
    const p3, 0x800015

    .line 228
    invoke-virtual {p1, p3}, Lcom/squareup/marketfont/MarketTextView;->setGravity(I)V

    .line 230
    :cond_3
    invoke-virtual {p2, p1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    return-void
.end method

.method private getClockOutSummaryTableLineSeparator(I)Landroid/view/View;
    .locals 4

    .line 341
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->layoutInflater:Landroid/view/LayoutInflater;

    sget v1, Lcom/squareup/ui/timecards/R$layout;->timecards_clockout_summary_line_separator:I

    iget-object v2, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->clockOutSummaryTable:Landroid/widget/TableLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    .line 343
    sget v1, Lcom/squareup/ui/timecards/R$id;->timecards_clockout_summary_line_separator:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 344
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/TableRow$LayoutParams;

    .line 345
    iput p1, v2, Landroid/widget/TableRow$LayoutParams;->span:I

    .line 346
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method private getDayDifference(Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;Lorg/threeten/bp/ZoneId;Lorg/threeten/bp/ZoneId;)J
    .locals 5

    .line 269
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 270
    invoke-virtual {p2}, Lorg/threeten/bp/ZoneId;->getId()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 271
    new-instance p2, Ljava/util/Date;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->start_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object v2, v2, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    .line 272
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    .line 271
    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v1

    invoke-direct {p2, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, p2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 273
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p2

    invoke-static {p2}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object p2

    .line 274
    invoke-virtual {p3}, Lorg/threeten/bp/ZoneId;->getId()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 275
    new-instance p3, Ljava/util/Date;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->stop_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object p1, p1, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    .line 276
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object p1, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    .line 275
    invoke-virtual {v1, v2, v3, p1}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v1

    invoke-direct {p3, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p2, p3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 277
    invoke-direct {p0, v0}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->removeTimeComponent(Ljava/util/Calendar;)V

    .line 278
    invoke-direct {p0, p2}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->removeTimeComponent(Ljava/util/Calendar;)V

    .line 279
    sget-object p1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide p2

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    sub-long/2addr p2, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, p2, p3, v0}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide p1

    return-wide p1
.end method

.method private getShiftSummary(Lcom/squareup/ui/timecards/HoursMinutes;)Ljava/lang/String;
    .locals 2

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_clock_out_success_shift_summary:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->res:Lcom/squareup/util/Res;

    .line 152
    invoke-static {v1, p1}, Lcom/squareup/ui/timecards/TimeFormatter;->getFormattedTimePeriod(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/HoursMinutes;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "hrs_mins"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 153
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getTableDataRow(ZLcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;)Landroid/widget/TableRow;
    .locals 13

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->res:Lcom/squareup/util/Res;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->paid_seconds:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/timecards/TimeFormatter;->getFormattedHoursFraction(Lcom/squareup/util/Res;J)Ljava/lang/String;

    move-result-object v5

    .line 236
    new-instance v0, Landroid/widget/TableRow;

    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 237
    sget-object v1, Lcom/squareup/ui/timecards/ClockOutCoordinator;->TABLE_ROW_PARAMS:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x0

    if-nez p1, :cond_2

    .line 240
    iget-object v2, p2, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iget-object v2, v2, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->job_title:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 241
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 242
    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/ui/timecards/R$string;->timecard_no_job_title:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_1
    move-object v8, v2

    const/4 v11, 0x1

    const/4 v9, 0x0

    move-object v6, p0

    move-object v7, v0

    move v10, p1

    .line 244
    invoke-direct/range {v6 .. v11}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->createTableDataCell(Landroid/widget/TableRow;Ljava/lang/String;IZZ)V

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    .line 247
    :goto_0
    iget-object v3, p2, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->start_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, v3, Lcom/squareup/protos/common/time/DateTime;->tz_name:Ljava/util/List;

    .line 248
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Lorg/threeten/bp/ZoneId;->of(Ljava/lang/String;)Lorg/threeten/bp/ZoneId;

    move-result-object v3

    .line 249
    iget-object v4, p2, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->stop_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object v4, v4, Lcom/squareup/protos/common/time/DateTime;->tz_name:Ljava/util/List;

    .line 250
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lorg/threeten/bp/ZoneId;->of(Ljava/lang/String;)Lorg/threeten/bp/ZoneId;

    move-result-object v1

    .line 251
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, p2, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->start_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object v6, v6, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    .line 252
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    .line 251
    invoke-static {v3, v6, v7}, Lcom/squareup/ui/timecards/TimeFormatter;->getTitleTime(Lorg/threeten/bp/ZoneId;J)Ljava/lang/String;

    move-result-object v8

    .line 253
    invoke-direct {p0, p2, v3, v1}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->getDayDifference(Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;Lorg/threeten/bp/ZoneId;Lorg/threeten/bp/ZoneId;)J

    move-result-wide v3

    const-wide/16 v6, 0x0

    cmp-long v9, v3, v6

    if-lez v9, :cond_3

    .line 255
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "(-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v3, ")"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v3, v2, 0x1

    const/4 v12, 0x0

    move-object v6, p0

    move-object v7, v0

    move v10, v2

    move v11, p1

    .line 256
    invoke-direct/range {v6 .. v12}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->createTableDataCell(Landroid/widget/TableRow;Ljava/lang/String;Ljava/lang/String;IZZ)V

    goto :goto_1

    :cond_3
    add-int/lit8 v3, v2, 0x1

    const/4 v11, 0x0

    move-object v6, p0

    move-object v7, v0

    move v9, v2

    move v10, p1

    .line 258
    invoke-direct/range {v6 .. v11}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->createTableDataCell(Landroid/widget/TableRow;Ljava/lang/String;IZZ)V

    :goto_1
    move v9, v3

    .line 260
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object p2, p2, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->stop_zoned_date_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object p2, p2, Lcom/squareup/protos/common/time/DateTime;->instant_usec:Ljava/lang/Long;

    .line 261
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 260
    invoke-static {v1, v2, v3}, Lcom/squareup/ui/timecards/TimeFormatter;->getTitleTime(Lorg/threeten/bp/ZoneId;J)Ljava/lang/String;

    move-result-object v8

    add-int/lit8 p2, v9, 0x1

    const/4 v11, 0x0

    move-object v6, p0

    move-object v7, v0

    move v10, p1

    invoke-direct/range {v6 .. v11}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->createTableDataCell(Landroid/widget/TableRow;Ljava/lang/String;IZZ)V

    const/4 v8, 0x0

    move-object v3, p0

    move-object v4, v0

    move v6, p2

    move v7, p1

    .line 263
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->createTableDataCell(Landroid/widget/TableRow;Ljava/lang/String;IZZ)V

    return-object v0
.end method

.method private getTableFooterRow(ZJ)Landroid/widget/TableRow;
    .locals 8

    .line 323
    new-instance v0, Landroid/widget/TableRow;

    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 324
    sget-object v1, Lcom/squareup/ui/timecards/ClockOutCoordinator;->TABLE_ROW_PARAMS:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 325
    new-instance v0, Landroid/widget/TableRow;

    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 327
    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {v1, p2, p3}, Lcom/squareup/ui/timecards/TimeFormatter;->getFormattedHoursFraction(Lcom/squareup/util/Res;J)Ljava/lang/String;

    move-result-object p2

    .line 328
    iget-object p3, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->timecard_clock_out_summary_footer_total:I

    .line 329
    invoke-interface {p3, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x1

    const/4 v5, 0x0

    move-object v2, p0

    move-object v3, v0

    move v6, p1

    .line 328
    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->createTableDataCell(Landroid/widget/TableRow;Ljava/lang/String;IZZ)V

    const/4 v5, 0x1

    if-nez p1, :cond_0

    const/4 p3, 0x2

    const/4 v7, 0x0

    const-string v4, ""

    move-object v2, p0

    move-object v3, v0

    move v6, p1

    .line 333
    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->createTableDataCell(Landroid/widget/TableRow;Ljava/lang/String;IZZ)V

    const/4 v5, 0x2

    :cond_0
    add-int/lit8 p3, v5, 0x1

    const/4 v7, 0x0

    const-string v4, ""

    move-object v2, p0

    move-object v3, v0

    move v6, p1

    .line 335
    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->createTableDataCell(Landroid/widget/TableRow;Ljava/lang/String;IZZ)V

    const/4 v7, 0x1

    move-object v4, p2

    move v5, p3

    .line 336
    invoke-direct/range {v2 .. v7}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->createTableDataCell(Landroid/widget/TableRow;Ljava/lang/String;IZZ)V

    return-object v0
.end method

.method private getTableHeaderRow(Z)Landroid/widget/TableRow;
    .locals 9

    .line 199
    new-instance v6, Landroid/widget/TableRow;

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v6, v0}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 200
    sget-object v0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->TABLE_ROW_PARAMS:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v6, v0}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    if-nez p1, :cond_0

    .line 203
    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->layoutInflater:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/timecards/R$string;->timecard_clock_out_summary_mw_header_0_shift:I

    .line 204
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object v0, p0

    move-object v2, v6

    move v5, p1

    .line 203
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->createTableHeaderCell(Landroid/view/LayoutInflater;Landroid/widget/TableRow;Ljava/lang/String;IZ)V

    const/4 v0, 0x1

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 207
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->layoutInflater:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/timecards/R$string;->timecard_clock_out_summary_mw_header_1_clocked_in:I

    .line 208
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v7, v4, 0x1

    move-object v0, p0

    move-object v2, v6

    move v5, p1

    .line 207
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->createTableHeaderCell(Landroid/view/LayoutInflater;Landroid/widget/TableRow;Ljava/lang/String;IZ)V

    .line 210
    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->layoutInflater:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/timecards/R$string;->timecard_clock_out_summary_mw_header_2_clocked_out:I

    .line 211
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v8, v7, 0x1

    move-object v0, p0

    move-object v2, v6

    move v4, v7

    .line 210
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->createTableHeaderCell(Landroid/view/LayoutInflater;Landroid/widget/TableRow;Ljava/lang/String;IZ)V

    .line 213
    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->layoutInflater:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/timecards/R$string;->timecard_clock_out_summary_mw_header_3_paid_hours:I

    .line 214
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v2, v6

    move v4, v8

    .line 213
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->createTableHeaderCell(Landroid/view/LayoutInflater;Landroid/widget/TableRow;Ljava/lang/String;IZ)V

    return-object v6
.end method

.method private hideShiftColumn(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;",
            ">;)Z"
        }
    .end annotation

    .line 189
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;

    .line 190
    iget-object v0, v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iget-object v0, v0, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->job_title:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 191
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method private isReportPresent(Lcom/squareup/ui/timecards/ClockOutScreen$Data;)Z
    .locals 1

    .line 142
    iget-object v0, p1, Lcom/squareup/ui/timecards/ClockOutScreen$Data;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/ui/timecards/ClockOutScreen$Data;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    iget-object v0, v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->job_summaries:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/squareup/ui/timecards/ClockOutScreen$Data;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->job_summaries:Ljava/util/List;

    .line 144
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private removeTimeComponent(Ljava/util/Calendar;)V
    .locals 2

    const/4 v0, 0x0

    const/16 v1, 0xb

    .line 283
    invoke-virtual {p1, v1, v0}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xc

    .line 284
    invoke-virtual {p1, v1, v0}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xd

    .line 285
    invoke-virtual {p1, v1, v0}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xe

    .line 286
    invoke-virtual {p1, v1, v0}, Ljava/util/Calendar;->set(II)V

    return-void
.end method

.method private showClockOutSummaryTable(Lcom/squareup/ui/timecards/ClockOutScreen$Data;)V
    .locals 8

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->clockOutSummaryTable:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->removeAllViews()V

    .line 158
    iget-object p1, p1, Lcom/squareup/ui/timecards/ClockOutScreen$Data;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->job_summaries:Ljava/util/List;

    .line 160
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 161
    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->hideShiftColumn(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v2, 0x3

    goto :goto_0

    :cond_0
    const/4 v2, 0x4

    .line 164
    :goto_0
    iget-object v3, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->clockOutSummaryTable:Landroid/widget/TableLayout;

    invoke-direct {p0, v1}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->getTableHeaderRow(Z)Landroid/widget/TableRow;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 165
    iget-object v3, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->clockOutSummaryTable:Landroid/widget/TableLayout;

    invoke-direct {p0, v2}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->getClockOutSummaryTableLineSeparator(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    const-wide/16 v3, 0x0

    .line 170
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;

    .line 171
    iget-object v6, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->clockOutSummaryTable:Landroid/widget/TableLayout;

    invoke-direct {p0, v1, v5}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->getTableDataRow(ZLcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;)Landroid/widget/TableRow;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 172
    iget-object v6, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->clockOutSummaryTable:Landroid/widget/TableLayout;

    invoke-direct {p0, v2}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->getClockOutSummaryTableLineSeparator(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 173
    iget-object v5, v5, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;->paid_seconds:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    add-long/2addr v3, v5

    goto :goto_1

    :cond_1
    const/4 p1, 0x1

    if-le v0, p1, :cond_2

    .line 178
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->clockOutSummaryTable:Landroid/widget/TableLayout;

    invoke-direct {p0, v1, v3, v4}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->getTableFooterRow(ZJ)Landroid/widget/TableRow;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 179
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->clockOutSummaryTable:Landroid/widget/TableLayout;

    invoke-direct {p0, v2}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->getClockOutSummaryTableLineSeparator(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    :cond_2
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    .line 69
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->attach(Landroid/view/View;)V

    .line 71
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->layoutInflater:Landroid/view/LayoutInflater;

    .line 72
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockOutCoordinator$7t4vNIVww-3Cn5V4uPn_f0_UzW0;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockOutCoordinator$7t4vNIVww-3Cn5V4uPn_f0_UzW0;-><init>(Lcom/squareup/ui/timecards/ClockOutCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected bindViews(Landroid/view/View;)V
    .locals 1

    .line 351
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->bindViews(Landroid/view/View;)V

    .line 352
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->successView:Landroid/view/View;

    .line 353
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->successContainer:Landroid/view/View;

    .line 354
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_clockout_summary:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->clockOutSummaryView:Landroid/view/View;

    .line 355
    sget v0, Lcom/squareup/ui/timecards/R$id;->clockout_summary_view_notes_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->viewNotesButton:Lcom/squareup/marketfont/MarketTextView;

    .line 356
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->successTitle:Lcom/squareup/marketfont/MarketTextView;

    .line 357
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->successMessage:Lcom/squareup/marketfont/MarketTextView;

    .line 358
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_clockout_summary_table:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->clockOutSummaryTable:Landroid/widget/TableLayout;

    .line 359
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_clockout_summary_placeholder:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->placeholderView:Landroid/view/View;

    .line 360
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success_buttons_view_summary:I

    .line 361
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;

    iput-object p1, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->successButtonsView:Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;

    return-void
.end method

.method protected getErrorMessage()Ljava/lang/String;
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorTitle()Ljava/lang/String;
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getProgressBarTitle()Ljava/lang/String;
    .locals 2

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_clock_out_progress_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSuccessViewEvent()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 83
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SUCCESS_CLOCK_OUT:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public synthetic lambda$attach$0$ClockOutCoordinator()Lrx/Subscription;
    .locals 3

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->clockOutScreenData()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->mainScheduler:Lrx/Scheduler;

    .line 73
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$J568aP7Vi9eGix_6oT9sMHbypwc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$J568aP7Vi9eGix_6oT9sMHbypwc;-><init>(Lcom/squareup/ui/timecards/ClockOutCoordinator;)V

    .line 74
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$EohElE0v-AcUX5ALFtIDAJPPgwc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$EohElE0v-AcUX5ALFtIDAJPPgwc;-><init>(Lcom/squareup/ui/timecards/ClockOutCoordinator;)V

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$dOdh1cBT6C__SSZcn1snGYNguwo;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$dOdh1cBT6C__SSZcn1snGYNguwo;-><init>(Lcom/squareup/ui/timecards/ClockOutCoordinator;)V

    .line 75
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$showSuccess$1$ClockOutCoordinator(Landroid/view/View;)V
    .locals 0

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onViewNotesClicked()V

    return-void
.end method

.method public synthetic lambda$showSuccess$2$ClockOutCoordinator(Landroid/view/View;)V
    .locals 0

    .line 137
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->printReceiptClicked()V

    return-void
.end method

.method protected showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 6

    .line 95
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    .line 96
    check-cast p1, Lcom/squareup/ui/timecards/ClockOutScreen$Data;

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$7XwxV_VzV14tlOleK0NWbjzouFM;

    invoke-direct {v2, v1}, Lcom/squareup/ui/timecards/-$$Lambda$7XwxV_VzV14tlOleK0NWbjzouFM;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    invoke-virtual {v0, v2}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->updateConfigForSuccessScreen(Ljava/lang/Runnable;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->featureReleaseHelper:Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    sget-object v1, Lcom/squareup/ui/timecards/Feature;->MULTIPLE_WAGES_CLOCK_OUT_SUMMARY:Lcom/squareup/ui/timecards/Feature;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/FeatureReleaseHelper;->isFeatureEnabled(Lcom/squareup/ui/timecards/Feature;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 100
    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->isReportPresent(Lcom/squareup/ui/timecards/ClockOutScreen$Data;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v2, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SUCCESS_CLOCK_OUT_SUMMARY:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v2}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    iget-object v2, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/ui/timecards/R$string;->employee_management_clock_out_success_title_multiple_wages:I

    .line 103
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 102
    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->timecardSubHeader:Lcom/squareup/marketfont/MarketTextView;

    iget-object v2, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 106
    invoke-interface {v2}, Lcom/squareup/time/CurrentTime;->zonedDateTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/ZonedDateTime;->getZone()Lorg/threeten/bp/ZoneId;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->locale:Ljava/util/Locale;

    iget-wide v4, p1, Lcom/squareup/ui/timecards/ClockOutScreen$Data;->currentTimeMillis:J

    .line 105
    invoke-static {v2, v3, v4, v5}, Lcom/squareup/ui/timecards/TimeFormatter;->getCurrentWorkDay(Lorg/threeten/bp/ZoneId;Ljava/util/Locale;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->timecardSubHeader:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 109
    iget-boolean v0, p1, Lcom/squareup/ui/timecards/ClockOutScreen$Data;->showViewNotesButton:Z

    const/16 v2, 0x8

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->viewNotesButton:Lcom/squareup/marketfont/MarketTextView;

    new-instance v3, Lcom/squareup/ui/timecards/-$$Lambda$ClockOutCoordinator$HydlrhEUBLEZ8_aibUXHXBY0MVQ;

    invoke-direct {v3, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockOutCoordinator$HydlrhEUBLEZ8_aibUXHXBY0MVQ;-><init>(Lcom/squareup/ui/timecards/ClockOutCoordinator;)V

    .line 111
    invoke-static {v3}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v3

    .line 110
    invoke-virtual {v0, v3}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->viewNotesButton:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    goto :goto_0

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->viewNotesButton:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 117
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->showClockOutSummaryTable(Lcom/squareup/ui/timecards/ClockOutScreen$Data;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->successView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->clockOutSummaryView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 120
    iget-object v0, p1, Lcom/squareup/ui/timecards/ClockOutScreen$Data;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->placeholderView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->successTitle:Lcom/squareup/marketfont/MarketTextView;

    sget v2, Lcom/squareup/ui/timecards/R$string;->employee_management_clock_out_success_title:I

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    .line 125
    iget-object v0, p1, Lcom/squareup/ui/timecards/ClockOutScreen$Data;->hoursMinutesWorked:Lcom/squareup/ui/timecards/HoursMinutes;

    invoke-direct {p0, v0}, Lcom/squareup/ui/timecards/ClockOutCoordinator;->getShiftSummary(Lcom/squareup/ui/timecards/HoursMinutes;)Ljava/lang/String;

    move-result-object v0

    .line 126
    iget-object v2, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->successMessage:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v2, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->successMessage:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->successView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 129
    iget-object v0, p1, Lcom/squareup/ui/timecards/ClockOutScreen$Data;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->successContainer:Landroid/view/View;

    sget-object v1, Lcom/squareup/ui/timecards/ClockOutCoordinator;->MOBILE_LAYOUT_PARAMS:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 134
    :cond_2
    :goto_1
    iget-boolean p1, p1, Lcom/squareup/ui/timecards/ClockOutScreen$Data;->printerConnected:Z

    if-eqz p1, :cond_3

    .line 135
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->successButtonsView:Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockOutCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->timecard_print_receipt_summary:I

    .line 136
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$ClockOutCoordinator$t9ZuE_FJ2GVeSA6A4wSrKujaMRM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockOutCoordinator$t9ZuE_FJ2GVeSA6A4wSrKujaMRM;-><init>(Lcom/squareup/ui/timecards/ClockOutCoordinator;)V

    .line 137
    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    .line 135
    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;->showSecondaryButton(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    :cond_3
    return-void
.end method
