.class public Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EndBreakAfterLoginCoordinator.java"


# instance fields
.field private analytics:Lcom/squareup/analytics/Analytics;

.field private clockGlyph:Lcom/squareup/glyph/SquareGlyphView;

.field private currentTime:Lcom/squareup/time/CurrentTime;

.field private device:Lcom/squareup/util/Device;

.field private failureGlyph:Lcom/squareup/glyph/SquareGlyphView;

.field private final features:Lcom/squareup/settings/server/Features;

.field private mainScheduler:Lrx/Scheduler;

.field private message:Lcom/squareup/marketfont/MarketTextView;

.field private passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private primaryButton:Lcom/squareup/marketfont/MarketButton;

.field private progressBar:Landroid/widget/ProgressBar;

.field private res:Lcom/squareup/util/Res;

.field private secondaryButton:Lcom/squareup/marketfont/MarketButton;

.field private successGlyph:Lcom/squareup/glyph/SquareGlyphView;

.field private timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

.field private timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

.field private title:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lrx/Scheduler;Lcom/squareup/time/CurrentTime;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .param p3    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 53
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->res:Lcom/squareup/util/Res;

    .line 55
    iput-object p2, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    .line 56
    iput-object p3, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->mainScheduler:Lrx/Scheduler;

    .line 57
    iput-object p4, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 58
    iput-object p5, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    .line 59
    iput-object p6, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 60
    iput-object p7, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->device:Lcom/squareup/util/Device;

    .line 61
    iput-object p8, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 166
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/TimecardsActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    .line 167
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_modal_progress_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->progressBar:Landroid/widget/ProgressBar;

    .line 168
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_modal_success:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->successGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 169
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_modal_clock_glyph:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->clockGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 170
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_modal_failure:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->failureGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 171
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_modal_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    .line 172
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_modal_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->message:Lcom/squareup/marketfont/MarketTextView;

    .line 173
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_modal_primary_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    .line 174
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_modal_secondary_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method private static breakEndingLate(Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;)Z
    .locals 4

    .line 238
    iget-wide v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;->currentTimeMillis:J

    iget-object p0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;->expectedBreakEndTimeMillis:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long p0, v0, v2

    if-lez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private getEmployeeName()Ljava/lang/String;
    .locals 2

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 234
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    .line 233
    invoke-static {v0}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeInfo(Lcom/squareup/permissions/Employee;)Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->res:Lcom/squareup/util/Res;

    .line 234
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/EmployeeInfo;->getFullName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getMessage(Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;)Ljava/lang/String;
    .locals 4

    .line 206
    invoke-static {p1}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->breakEndingLate(Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_modal_message_break_over:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 208
    invoke-interface {v1}, Lcom/squareup/time/CurrentTime;->zonedDateTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/ZonedDateTime;->getZone()Lorg/threeten/bp/ZoneId;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;->expectedBreakEndTimeMillis:Ljava/lang/Long;

    .line 209
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 208
    invoke-static {v1, v2, v3}, Lcom/squareup/ui/timecards/TimeFormatter;->getSentenceTime(Lorg/threeten/bp/ZoneId;J)Ljava/lang/String;

    move-result-object p1

    const-string v1, "break_end_time"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 210
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 211
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 214
    :cond_0
    new-instance v0, Ljava/util/Date;

    iget-wide v1, p1, Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;->currentTimeMillis:J

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    new-instance v1, Ljava/util/Date;

    iget-object p1, p1, Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;->expectedBreakEndTimeMillis:Ljava/lang/Long;

    .line 215
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 214
    invoke-static {v0, v1}, Lcom/squareup/ui/timecards/HoursMinutes;->getDiff(Ljava/util/Date;Ljava/util/Date;)Lcom/squareup/ui/timecards/HoursMinutes;

    move-result-object p1

    .line 216
    iget v0, p1, Lcom/squareup/ui/timecards/HoursMinutes;->minutes:I

    iget p1, p1, Lcom/squareup/ui/timecards/HoursMinutes;->hours:I

    mul-int/lit8 p1, p1, 0x3c

    add-int/2addr v0, p1

    const/4 p1, 0x1

    if-ge v0, p1, :cond_1

    .line 219
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_modal_message_sub_one_min:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    if-ne v0, p1, :cond_2

    .line 223
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_modal_message_one_min:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 226
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_modal_message:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v1, "mins_left"

    .line 227
    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 228
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 229
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getTitle(Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;)Ljava/lang/String;
    .locals 1

    .line 200
    invoke-static {p1}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->breakEndingLate(Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_modal_title_over:I

    .line 201
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_modal_title_not_over:I

    .line 202
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public static synthetic lambda$AGLU688pT--ujrN8HXlNjX6d30Y(Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->showData(Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;)V

    return-void
.end method

.method public static synthetic lambda$gh6e4Sf-scQLNZRrD7suS04ERRk(Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->showProgressBar()V

    return-void
.end method

.method private setErrorVisibility()V
    .locals 4

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    iget-object v2, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/timecards/-$$Lambda$cePlQqc8brG5yNhhmgl4PpRKLXE;

    invoke-direct {v3, v2}, Lcom/squareup/ui/timecards/-$$Lambda$cePlQqc8brG5yNhhmgl4PpRKLXE;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    .line 140
    invoke-direct {p0}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->getEmployeeName()Ljava/lang/String;

    move-result-object v2

    .line 139
    invoke-virtual {v0, v3, v2}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setConfigForReminderLandingScreen(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->clockGlyph:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->failureGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 146
    invoke-direct {p0}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->showOrHideSecondaryButton()V

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->successGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v2}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->message:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    return-void
.end method

.method private showActionBar(Z)V
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginCoordinator$1iQxn0AnPf3ALL7Bpp2KMGb5z8E;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginCoordinator$1iQxn0AnPf3ALL7Bpp2KMGb5z8E;-><init>(Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;Z)V

    .line 116
    invoke-direct {p0}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->getEmployeeName()Ljava/lang/String;

    move-result-object p1

    .line 114
    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setConfigForReminderLandingScreen(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 117
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setVisibility(I)V

    return-void
.end method

.method private showData(Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;)V
    .locals 2

    .line 78
    iget-object v0, p1, Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;->internetState:Lcom/squareup/connectivity/InternetState;

    sget-object v1, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-eq v0, v1, :cond_0

    .line 79
    invoke-direct {p0}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->showNoInternet()V

    return-void

    .line 82
    :cond_0
    sget-object v0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator$1;->$SwitchMap$com$squareup$ui$timecards$Timecards$TimecardRequestState:[I

    iget-object v1, p1, Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    invoke-virtual {v1}, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 90
    invoke-direct {p0}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->showError()V

    goto :goto_0

    .line 87
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->showSuccess(Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;)V

    goto :goto_0

    .line 84
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->showProgressBar()V

    :goto_0
    return-void
.end method

.method private showError()V
    .locals 2

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 160
    invoke-direct {p0}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->setErrorVisibility()V

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_no_internet_error_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->message:Lcom/squareup/marketfont/MarketTextView;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_no_internet_error_message:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    return-void
.end method

.method private showNoInternet()V
    .locals 2

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_NO_INTERNET:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 153
    invoke-direct {p0}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->setErrorVisibility()V

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_no_internet_error_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->message:Lcom/squareup/marketfont/MarketTextView;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_no_internet_error_message:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    return-void
.end method

.method private showOrHideSecondaryButton()V
    .locals 2

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    goto :goto_0

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_modal_log_out:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private showPrimaryButton(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 122
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    sget v0, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_end_break_early_button_text:I

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 124
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginCoordinator$Daua8kvletwOzI7IW46MEnl_Kkk;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginCoordinator$Daua8kvletwOzI7IW46MEnl_Kkk;-><init>(Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;)V

    .line 125
    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    .line 124
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 127
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    sget v0, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_end_break_button_text:I

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 128
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginCoordinator$TrRP4C7qdrcEbdZvI39gZVY71cw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginCoordinator$TrRP4C7qdrcEbdZvI39gZVY71cw;-><init>(Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;)V

    .line 129
    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    .line 128
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    return-void
.end method

.method private showProgressBar()V
    .locals 3

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_LOADING:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setVisibility(I)V

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->progressBar:Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->clockGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->successGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->failureGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->message:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    return-void
.end method

.method private showSuccess(Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;)V
    .locals 3

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_REMINDER_SUCCESS_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 97
    iget-boolean v0, p1, Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;->endAuthorizationNeeded:Z

    invoke-direct {p0, v0}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->showActionBar(Z)V

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->clockGlyph:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->successGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->failureGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->getTitle(Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->message:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->message:Lcom/squareup/marketfont/MarketTextView;

    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->getMessage(Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    iget-boolean p1, p1, Lcom/squareup/ui/timecards/EndBreakAfterLoginScreen$Data;->endAuthorizationNeeded:Z

    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->showPrimaryButton(Z)V

    .line 110
    invoke-direct {p0}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->showOrHideSecondaryButton()V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 65
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 66
    invoke-direct {p0, p1}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->bindViews(Landroid/view/View;)V

    .line 68
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginCoordinator$ATSfN_VP7T2q9FIZWyQDQjjrraE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginCoordinator$ATSfN_VP7T2q9FIZWyQDQjjrraE;-><init>(Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 73
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginCoordinator$l7bgZu74Nlp_lscfSArFo-67P4w;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginCoordinator$l7bgZu74Nlp_lscfSArFo-67P4w;-><init>(Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;)V

    .line 74
    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    .line 73
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public synthetic lambda$attach$0$EndBreakAfterLoginCoordinator()Lrx/Subscription;
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->endBreakAfterLoginScreenData()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->mainScheduler:Lrx/Scheduler;

    .line 69
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginCoordinator$gh6e4Sf-scQLNZRrD7suS04ERRk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginCoordinator$gh6e4Sf-scQLNZRrD7suS04ERRk;-><init>(Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;)V

    .line 70
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginCoordinator$AGLU688pT--ujrN8HXlNjX6d30Y;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakAfterLoginCoordinator$AGLU688pT--ujrN8HXlNjX6d30Y;-><init>(Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;)V

    .line 71
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$1$EndBreakAfterLoginCoordinator(Landroid/view/View;)V
    .locals 0

    .line 74
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->finishAndLogOut()V

    return-void
.end method

.method public synthetic lambda$showActionBar$2$EndBreakAfterLoginCoordinator(Z)V
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onEndBreakAfterLoginCloseClicked(Z)V

    return-void
.end method

.method public synthetic lambda$showPrimaryButton$3$EndBreakAfterLoginCoordinator(Landroid/view/View;)V
    .locals 0

    .line 125
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onEndBreakEarlyAfterLoginClicked()V

    return-void
.end method

.method public synthetic lambda$showPrimaryButton$4$EndBreakAfterLoginCoordinator(Landroid/view/View;)V
    .locals 0

    .line 129
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onEndBreakAfterLoginClicked()V

    return-void
.end method
