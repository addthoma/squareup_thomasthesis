.class public Lcom/squareup/ui/timecards/EmployeeJobsListModalScreen;
.super Lcom/squareup/ui/timecards/InTimecardsScope;
.source "EmployeeJobsListModalScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/timecards/EmployeeJobsListModalScreen$Data;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/timecards/EmployeeJobsListModalScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/timecards/EmployeeJobsListModalScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 19
    new-instance v0, Lcom/squareup/ui/timecards/EmployeeJobsListModalScreen;

    invoke-direct {v0}, Lcom/squareup/ui/timecards/EmployeeJobsListModalScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/timecards/EmployeeJobsListModalScreen;->INSTANCE:Lcom/squareup/ui/timecards/EmployeeJobsListModalScreen;

    .line 47
    sget-object v0, Lcom/squareup/ui/timecards/EmployeeJobsListModalScreen;->INSTANCE:Lcom/squareup/ui/timecards/EmployeeJobsListModalScreen;

    .line 48
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/timecards/EmployeeJobsListModalScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/squareup/ui/timecards/InTimecardsScope;-><init>()V

    return-void
.end method


# virtual methods
.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 43
    const-class v0, Lcom/squareup/ui/timecards/TimecardsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/timecards/TimecardsScope$Component;

    .line 44
    invoke-interface {p1}, Lcom/squareup/ui/timecards/TimecardsScope$Component;->employeeJobsListModalCoordinator()Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 39
    sget v0, Lcom/squareup/ui/timecards/R$layout;->timecards:I

    return v0
.end method
