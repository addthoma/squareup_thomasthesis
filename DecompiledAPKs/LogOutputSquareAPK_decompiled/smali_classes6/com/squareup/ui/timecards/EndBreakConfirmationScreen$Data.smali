.class public final Lcom/squareup/ui/timecards/EndBreakConfirmationScreen$Data;
.super Lcom/squareup/ui/timecards/TimecardsScreenData;
.source "EndBreakConfirmationScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/EndBreakConfirmationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation


# instance fields
.field public final endAuthorizationNeeded:Z

.field public final expectedBreakEndTimeMillis:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;Ljava/lang/Long;Ljava/lang/Boolean;)V
    .locals 0

    .line 27
    invoke-direct/range {p0 .. p7}, Lcom/squareup/ui/timecards/TimecardsScreenData;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;)V

    .line 29
    iput-object p8, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationScreen$Data;->expectedBreakEndTimeMillis:Ljava/lang/Long;

    .line 30
    invoke-virtual {p9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationScreen$Data;->endAuthorizationNeeded:Z

    return-void
.end method
