.class Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakDefinitions;
.super Lcom/squareup/ui/timecards/Timecards$Event;
.source "Timecards.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/Timecards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SetTimecardBreakDefinitions"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/timecards/Timecards;

.field public final timecardBreakDefinitions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/ui/timecards/Timecards;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;",
            ">;)V"
        }
    .end annotation

    .line 618
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakDefinitions;->this$0:Lcom/squareup/ui/timecards/Timecards;

    invoke-direct {p0}, Lcom/squareup/ui/timecards/Timecards$Event;-><init>()V

    .line 619
    iput-object p2, p0, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakDefinitions;->timecardBreakDefinitions:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/timecards/Timecards;Ljava/util/List;Lcom/squareup/ui/timecards/Timecards$1;)V
    .locals 0

    .line 615
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/timecards/Timecards$SetTimecardBreakDefinitions;-><init>(Lcom/squareup/ui/timecards/Timecards;Ljava/util/List;)V

    return-void
.end method
