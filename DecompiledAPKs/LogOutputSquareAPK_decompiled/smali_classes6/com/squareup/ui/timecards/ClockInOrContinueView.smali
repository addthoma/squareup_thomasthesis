.class public Lcom/squareup/ui/timecards/ClockInOrContinueView;
.super Lcom/squareup/widgets/PairLayout;
.source "ClockInOrContinueView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field appNameFormatter:Lcom/squareup/util/AppNameFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private clockGlyph:Lcom/squareup/glyph/SquareGlyphView;

.field private clockInButton:Landroid/view/View;

.field private clockInSuccessGlyph:Lcom/squareup/glyph/SquareGlyphView;

.field private continueButton:Landroid/view/View;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private message:Landroid/widget/TextView;

.field private noInternetGlyph:Lcom/squareup/glyph/SquareGlyphView;

.field presenter:Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Landroid/widget/ProgressBar;

.field private successContinueButton:Lcom/squareup/marketfont/MarketButton;

.field private timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

.field private title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/PairLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    const-class p2, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Component;->inject(Lcom/squareup/ui/timecards/ClockInOrContinueView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 120
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/TimecardsActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    .line 121
    sget v0, Lcom/squareup/ui/timecards/R$id;->clock_in_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->clockInButton:Landroid/view/View;

    .line 122
    sget v0, Lcom/squareup/ui/timecards/R$id;->clock_in_sheet_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->progressBar:Landroid/widget/ProgressBar;

    .line 123
    sget v0, Lcom/squareup/ui/timecards/R$id;->clock_in_sheet_success:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->clockInSuccessGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 124
    sget v0, Lcom/squareup/ui/timecards/R$id;->continue_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->continueButton:Landroid/view/View;

    .line 125
    sget v0, Lcom/squareup/ui/timecards/R$id;->success_continue_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->successContinueButton:Lcom/squareup/marketfont/MarketButton;

    .line 126
    sget v0, Lcom/squareup/ui/timecards/R$id;->clock_in_clock_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->clockGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 127
    sget v0, Lcom/squareup/ui/timecards/R$id;->no_internet_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->noInternetGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 128
    sget v0, Lcom/squareup/ui/timecards/R$id;->clock_in_sheet_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->title:Landroid/widget/TextView;

    .line 129
    sget v0, Lcom/squareup/ui/timecards/R$id;->clock_in_sheet_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->message:Landroid/widget/TextView;

    return-void
.end method

.method private showOrHideContinueButton()V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscape()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->continueButton:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->continueButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public synthetic lambda$onAttachedToWindow$0$ClockInOrContinueView(Landroid/view/View;)V
    .locals 0

    .line 49
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->presenter:Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->clockIn()V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$1$ClockInOrContinueView(Landroid/view/View;)V
    .locals 0

    .line 50
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->presenter:Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->finish()Z

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$2$ClockInOrContinueView(Landroid/view/View;)V
    .locals 0

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->presenter:Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->finish()Z

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .line 43
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onAttachedToWindow()V

    .line 44
    invoke-direct {p0}, Lcom/squareup/ui/timecards/ClockInOrContinueView;->bindViews()V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->successContinueButton:Lcom/squareup/marketfont/MarketButton;

    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v2, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_success_continue_button_text:I

    invoke-interface {v1, v2}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->clockInButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOrContinueView$7Sfs6EcdMTjeYAnvqMlZNuZ_bqg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOrContinueView$7Sfs6EcdMTjeYAnvqMlZNuZ_bqg;-><init>(Lcom/squareup/ui/timecards/ClockInOrContinueView;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->continueButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOrContinueView$CARDgSmyPoXmvzS2g7Hi49UpcOA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOrContinueView$CARDgSmyPoXmvzS2g7Hi49UpcOA;-><init>(Lcom/squareup/ui/timecards/ClockInOrContinueView;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->successContinueButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOrContinueView$dLYJR4xooaBOWN_tpoKiRd2eW18;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOrContinueView$dLYJR4xooaBOWN_tpoKiRd2eW18;-><init>(Lcom/squareup/ui/timecards/ClockInOrContinueView;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->presenter:Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->presenter:Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->finish()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->presenter:Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 58
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onDetachedFromWindow()V

    return-void
.end method

.method setActionBarConfig(Ljava/lang/Runnable;Ljava/lang/String;)V
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->setConfigForReminderLandingScreen(Ljava/lang/Runnable;Ljava/lang/String;)V

    return-void
.end method

.method public showClockInPrompt()V
    .locals 4

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->presenter:Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_REMINDER_SELECT_ACTION_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->clockGlyph:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->progressBar:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->clockInSuccessGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v2}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->noInternetGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v2}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->title:Landroid/widget/TextView;

    sget v3, Lcom/squareup/ui/timecards/R$string;->timecard_clock_in_and_continue_glyph_title:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->title:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->clockInButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 70
    invoke-direct {p0}, Lcom/squareup/ui/timecards/ClockInOrContinueView;->showOrHideContinueButton()V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->successContinueButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->message:Landroid/widget/TextView;

    sget v2, Lcom/squareup/ui/timecards/R$string;->timecard_message:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->message:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public showNoInternet()V
    .locals 3

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->presenter:Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_ERROR_NO_INTERNET:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->clockGlyph:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->title:Landroid/widget/TextView;

    sget v2, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_no_internet_error_title:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->title:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->noInternetGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v2}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->clockInButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 97
    invoke-direct {p0}, Lcom/squareup/ui/timecards/ClockInOrContinueView;->showOrHideContinueButton()V

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->successContinueButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->message:Landroid/widget/TextView;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_no_internet_error_message:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->message:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public showProgressBar()V
    .locals 3

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->presenter:Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_LOADING:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->clockGlyph:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->clockInSuccessGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v2}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->noInternetGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v2}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->title:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->message:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->clockInButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->continueButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 86
    invoke-direct {p0}, Lcom/squareup/ui/timecards/ClockInOrContinueView;->showOrHideContinueButton()V

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->successContinueButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    return-void
.end method
