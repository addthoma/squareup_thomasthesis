.class public Lcom/squareup/ui/timecards/ListBreaksCoordinator;
.super Lcom/squareup/ui/timecards/TimecardsCoordinator;
.source "ListBreaksCoordinator.java"


# instance fields
.field private listBreaksLayout:Landroid/widget/LinearLayout;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .param p4    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 33
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/timecards/TimecardsCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V

    return-void
.end method

.method private createButton(Landroid/view/LayoutInflater;Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;ZZZ)V
    .locals 4

    .line 112
    sget v0, Lcom/squareup/ui/timecards/R$layout;->timecards_list_breaks_button:I

    iget-object v1, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->listBreaksLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    .line 113
    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    .line 115
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_list_breaks_button_break_duration:I

    .line 116
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    .line 117
    invoke-direct {p0, p2}, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->getBreakDuration(Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    sget v1, Lcom/squareup/ui/timecards/R$id;->timecards_list_breaks_button_break_name:I

    .line 120
    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/marketfont/MarketTextView;

    .line 121
    iget-object v3, p2, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->break_name:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    invoke-direct {p0, p4, p5, p3}, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->getButtonLayoutParams(ZZZ)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object p3

    .line 125
    invoke-virtual {p1, p3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    if-eqz p4, :cond_0

    if-nez p5, :cond_0

    .line 127
    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 128
    iget-object p3, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->res:Lcom/squareup/util/Res;

    sget p4, Lcom/squareup/noho/R$dimen;->noho_text_size_button:I

    .line 129
    invoke-interface {p3, p4}, Lcom/squareup/util/Res;->getDimension(I)F

    move-result p3

    .line 128
    invoke-virtual {v0, v2, p3}, Lcom/squareup/marketfont/MarketTextView;->setTextSize(IF)V

    .line 130
    sget-object p3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, p3}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 131
    new-instance p3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 p4, -0x2

    invoke-direct {p3, p4, p4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 134
    iget-object p4, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->res:Lcom/squareup/util/Res;

    sget p5, Lcom/squareup/marin/R$dimen;->marin_gap_mini:I

    .line 135
    invoke-interface {p4, p5}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result p4

    .line 134
    invoke-virtual {p3, p4, v2, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 138
    invoke-virtual {v1, p3}, Lcom/squareup/marketfont/MarketTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 141
    :cond_0
    new-instance p3, Lcom/squareup/ui/timecards/-$$Lambda$ListBreaksCoordinator$cXBWXG2rnjEWklwOcr-LeI8Ekc0;

    invoke-direct {p3, p0, p2}, Lcom/squareup/ui/timecards/-$$Lambda$ListBreaksCoordinator$cXBWXG2rnjEWklwOcr-LeI8Ekc0;-><init>(Lcom/squareup/ui/timecards/ListBreaksCoordinator;Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;)V

    invoke-static {p3}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object p2, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->listBreaksLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p2, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private getBreakDuration(Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;)Ljava/lang/String;
    .locals 3

    .line 186
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->expected_duration_seconds:Ljava/lang/Integer;

    .line 187
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v0

    long-to-int p1, v0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 189
    iget-object p1, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_list_button_text_one_min:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_list_button_text:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "num_minutes"

    .line 192
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getButtonLayoutParams(ZZZ)Landroid/widget/LinearLayout$LayoutParams;
    .locals 3

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    const/4 p1, -0x1

    if-eqz p2, :cond_0

    .line 161
    new-instance p2, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v0, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/timecards/R$dimen;->employee_management_timecards_button_height:I

    .line 163
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v0

    invoke-direct {p2, p1, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    goto :goto_0

    .line 165
    :cond_0
    new-instance p2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {p2, p1, v1, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    :goto_0
    if-nez p3, :cond_2

    .line 169
    iget-object p1, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/noho/R$dimen;->noho_spacing_small:I

    .line 170
    invoke-interface {p1, p3}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result p1

    .line 169
    invoke-virtual {p2, v1, p1, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_1

    .line 174
    :cond_1
    new-instance p2, Landroid/widget/LinearLayout$LayoutParams;

    iget-object p1, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/timecards/R$dimen;->employee_management_timecards_button_height:I

    .line 175
    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result p1

    invoke-direct {p2, v1, p1, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    if-nez p3, :cond_2

    .line 177
    iget-object p1, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/noho/R$dimen;->noho_spacing_small:I

    .line 178
    invoke-interface {p1, p3}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result p1

    .line 177
    invoke-virtual {p2, p1, v1, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    :cond_2
    :goto_1
    return-object p2
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 37
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->attach(Landroid/view/View;)V

    .line 38
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ListBreaksCoordinator$9CWKul58XgFiMachZgakSjX7uyk;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ListBreaksCoordinator$9CWKul58XgFiMachZgakSjX7uyk;-><init>(Lcom/squareup/ui/timecards/ListBreaksCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected bindViews(Landroid/view/View;)V
    .locals 1

    .line 196
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->bindViews(Landroid/view/View;)V

    .line 197
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_list_breaks:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->listBreaksLayout:Landroid/widget/LinearLayout;

    .line 198
    iget-object p1, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->listBreaksLayout:Landroid/widget/LinearLayout;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

.method protected getErrorMessage()Ljava/lang/String;
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorTitle()Ljava/lang/String;
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSuccessViewEvent()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 49
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_LIST_BREAKS:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public synthetic lambda$attach$0$ListBreaksCoordinator()Lrx/Subscription;
    .locals 3

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->listBreaksScreenData()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->mainScheduler:Lrx/Scheduler;

    .line 39
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$tgB-ce9pFq8CTW7WwPGy3-0VyR4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$tgB-ce9pFq8CTW7WwPGy3-0VyR4;-><init>(Lcom/squareup/ui/timecards/ListBreaksCoordinator;)V

    .line 40
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$7r9wXJR-HskPKN_QKD9Q9Oj_Ql0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$7r9wXJR-HskPKN_QKD9Q9Oj_Ql0;-><init>(Lcom/squareup/ui/timecards/ListBreaksCoordinator;)V

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$4VKKN3hC8bDXX_gatCujFQh9oHg;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$4VKKN3hC8bDXX_gatCujFQh9oHg;-><init>(Lcom/squareup/ui/timecards/ListBreaksCoordinator;)V

    .line 41
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$createButton$1$ListBreaksCoordinator(Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;Landroid/view/View;)V
    .locals 3

    .line 142
    iget-object p2, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object v0, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->updated_at_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    iget-object p1, p1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    .line 143
    invoke-static {p1}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    .line 144
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    .line 143
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    .line 142
    invoke-virtual {p2, v0, p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onBreakSelected(Ljava/lang/String;Ljava/lang/Long;)V

    return-void
.end method

.method protected showBackArrowOnSuccess()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 12

    .line 61
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/timecards/ListBreaksScreen$Data;

    .line 62
    iget-object v1, v0, Lcom/squareup/ui/timecards/ListBreaksScreen$Data;->timecardBreakDefinitions:Ljava/util/List;

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/squareup/ui/timecards/ListBreaksScreen$Data;->timecardBreakDefinitions:Ljava/util/List;

    .line 63
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_6

    .line 67
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    .line 68
    iget-object p1, v0, Lcom/squareup/ui/timecards/ListBreaksScreen$Data;->timecardBreakDefinitions:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    iget-object v2, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_list_title_one_break_def:I

    .line 70
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 69
    invoke-virtual {p1, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 72
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    iget-object v2, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_list_title:I

    .line 73
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 72
    invoke-virtual {p1, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->view:Landroid/view/View;

    .line 77
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v3, "layout_inflater"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    .line 78
    iget-object v3, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->listBreaksLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 80
    iget-object v3, v0, Lcom/squareup/ui/timecards/ListBreaksScreen$Data;->device:Lcom/squareup/util/Device;

    .line 84
    invoke-interface {v3}, Lcom/squareup/util/Device;->isPortrait()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-interface {v3}, Lcom/squareup/util/Device;->isTabletLessThan10Inches()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 85
    :cond_2
    iget-object v3, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->listBreaksLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const/4 v9, 0x1

    goto :goto_1

    .line 87
    :cond_3
    iget-object v4, v0, Lcom/squareup/ui/timecards/ListBreaksScreen$Data;->timecardBreakDefinitions:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x2

    if-gt v4, v5, :cond_5

    .line 88
    invoke-interface {v3}, Lcom/squareup/util/Device;->isLandscape()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v3

    if-eqz v3, :cond_4

    goto :goto_2

    :cond_4
    const/4 v9, 0x0

    :goto_1
    const/4 v10, 0x1

    goto :goto_3

    .line 89
    :cond_5
    :goto_2
    iget-object v3, p0, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->listBreaksLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const/4 v9, 0x1

    const/4 v10, 0x0

    :goto_3
    const/4 v11, 0x0

    .line 94
    :goto_4
    iget-object v3, v0, Lcom/squareup/ui/timecards/ListBreaksScreen$Data;->timecardBreakDefinitions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v11, v3, :cond_7

    .line 95
    iget-object v3, v0, Lcom/squareup/ui/timecards/ListBreaksScreen$Data;->timecardBreakDefinitions:Ljava/util/List;

    .line 96
    invoke-interface {v3, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;

    if-nez v11, :cond_6

    const/4 v6, 0x1

    goto :goto_5

    :cond_6
    const/4 v6, 0x0

    :goto_5
    move-object v3, p0

    move-object v4, p1

    move v7, v9

    move v8, v10

    .line 97
    invoke-direct/range {v3 .. v8}, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->createButton(Landroid/view/LayoutInflater;Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;ZZZ)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    :cond_7
    return-void

    .line 64
    :cond_8
    :goto_6
    invoke-virtual {p0, v0}, Lcom/squareup/ui/timecards/ListBreaksCoordinator;->showError(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    return-void
.end method
