.class Lcom/squareup/ui/timecards/TimecardsScopeRunner$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "TimecardsScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onEndBreakEarlyConfirmationClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/timecards/TimecardsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V
    .locals 0

    .line 611
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner$1;->this$0:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 4

    .line 613
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner$1;->this$0:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner$1;->getAuthorizedEmployeeToken()Ljava/lang/String;

    move-result-object v1

    .line 614
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner$1;->getAuthorizedEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v2

    sget-object v3, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->GUEST_EMPLOYEE:Lcom/squareup/permissions/Employee;

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 613
    invoke-static {v0, v1, v2}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->access$000(Lcom/squareup/ui/timecards/TimecardsScopeRunner;Ljava/lang/String;Ljava/lang/Boolean;)V

    return-void
.end method
