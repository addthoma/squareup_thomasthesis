.class public final Lcom/squareup/ui/timecards/ClockInOrContinueScreen;
.super Lcom/squareup/ui/timecards/InTimecardsScope;
.source "ClockInOrContinueScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Component;,
        Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/timecards/ClockInOrContinueScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/timecards/ClockInOrContinueScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen;

    invoke-direct {v0}, Lcom/squareup/ui/timecards/ClockInOrContinueScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen;->INSTANCE:Lcom/squareup/ui/timecards/ClockInOrContinueScreen;

    .line 115
    sget-object v0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen;->INSTANCE:Lcom/squareup/ui/timecards/ClockInOrContinueScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/ui/timecards/InTimecardsScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 118
    sget v0, Lcom/squareup/ui/timecards/R$layout;->clock_in_or_continue_view:I

    return v0
.end method
