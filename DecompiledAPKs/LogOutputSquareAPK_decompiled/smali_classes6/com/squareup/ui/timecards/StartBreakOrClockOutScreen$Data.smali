.class public final Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen$Data;
.super Lcom/squareup/ui/timecards/TimecardsScreenData;
.source "StartBreakOrClockOutScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation


# instance fields
.field public final hoursMinutesWorkedToday:Lcom/squareup/ui/timecards/HoursMinutes;

.field public final jobTitle:Ljava/lang/String;

.field public final showStartBreakButton:Z

.field public final showSwitchJobsButton:Z


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;Lcom/squareup/ui/timecards/HoursMinutes;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;Ljava/lang/String;ZZ)V
    .locals 9

    move-object v8, p0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p5

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    .line 31
    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/timecards/TimecardsScreenData;-><init>(Lcom/squareup/util/Device;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/String;JLcom/squareup/connectivity/InternetState;Lcom/squareup/ui/timecards/ADD_OR_EDIT_NOTES_BUTTON_CONFIG;)V

    move-object v0, p4

    .line 33
    iput-object v0, v8, Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen$Data;->hoursMinutesWorkedToday:Lcom/squareup/ui/timecards/HoursMinutes;

    move-object/from16 v0, p9

    .line 34
    iput-object v0, v8, Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen$Data;->jobTitle:Ljava/lang/String;

    move/from16 v0, p10

    .line 35
    iput-boolean v0, v8, Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen$Data;->showStartBreakButton:Z

    move/from16 v0, p11

    .line 36
    iput-boolean v0, v8, Lcom/squareup/ui/timecards/StartBreakOrClockOutScreen$Data;->showSwitchJobsButton:Z

    return-void
.end method
