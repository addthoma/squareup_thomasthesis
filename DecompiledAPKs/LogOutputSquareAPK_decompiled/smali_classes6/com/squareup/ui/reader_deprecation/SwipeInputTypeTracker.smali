.class public Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;
.super Ljava/lang/Object;
.source "SwipeInputTypeTracker.java"


# instance fields
.field inputType:Lcom/squareup/Card$InputType;


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLastInputType()Lcom/squareup/Card$InputType;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;->inputType:Lcom/squareup/Card$InputType;

    return-object v0
.end method

.method public noteInputType(Lcom/squareup/Card$InputType;)V
    .locals 0

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/reader_deprecation/SwipeInputTypeTracker;->inputType:Lcom/squareup/Card$InputType;

    return-void
.end method
