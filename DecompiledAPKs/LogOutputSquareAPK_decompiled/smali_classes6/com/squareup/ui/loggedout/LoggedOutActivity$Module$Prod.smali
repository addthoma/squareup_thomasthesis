.class public Lcom/squareup/ui/loggedout/LoggedOutActivity$Module$Prod;
.super Ljava/lang/Object;
.source "LoggedOutActivity.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/loggedout/LoggedOutActivity$Module;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Prod"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideImpersonationHelper()Lcom/squareup/account/ImpersonationHelper;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 196
    new-instance v0, Lcom/squareup/account/ImpersonationHelper$NoImpersonationHelper;

    invoke-direct {v0}, Lcom/squareup/account/ImpersonationHelper$NoImpersonationHelper;-><init>()V

    return-object v0
.end method
