.class final Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$3;
.super Ljava/lang/Object;
.source "TextAboveImageSplashCoordinator.kt"

# interfaces
.implements Landroidx/viewpager/widget/ViewPager$PageTransformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "pageView",
        "Landroid/view/View;",
        "position",
        "",
        "transformPage"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$3;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$3;

    invoke-direct {v0}, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$3;-><init>()V

    sput-object v0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$3;->INSTANCE:Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$3;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final transformPage(Landroid/view/View;F)V
    .locals 1

    const-string v0, "pageView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    sget v0, Lcom/squareup/loggedout/R$id;->splash_page_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 66
    invoke-static {v0, p2}, Lcom/squareup/ui/loggedout/SplashScreenAnimationsKt;->fadeAndParallax(Landroid/view/View;F)V

    .line 68
    sget v0, Lcom/squareup/loggedout/R$id;->splash_page_icon:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 69
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    invoke-static {v0, p2, p1}, Lcom/squareup/ui/loggedout/SplashScreenAnimationsKt;->fadeInPlace(Landroid/view/View;FI)V

    return-void
.end method
