.class public final Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;
.super Ljava/lang/Object;
.source "LoggedOutScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;"
        }
    .end annotation
.end field

.field private final adAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final additionalServicesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;>;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;",
            ">;"
        }
    .end annotation
.end field

.field private final createAccountCanceledListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/CreateAccountCanceledListener;",
            ">;"
        }
    .end annotation
.end field

.field private final finalLogInCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/FinalLogInCheck;",
            ">;"
        }
    .end annotation
.end field

.field private final finalizeLoginFailureDialogFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final landingCanceledListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LandingCanceledListener;",
            ">;"
        }
    .end annotation
.end field

.field private final landingScreenSelectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LandingScreenSelector;",
            ">;"
        }
    .end annotation
.end field

.field private final legacyAuthenticatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final postInstallEncryptedEmailSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;>;"
        }
    .end annotation
.end field

.field private final singleSignOnProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/singlesignon/SingleSignOn;",
            ">;"
        }
    .end annotation
.end field

.field private final tourPagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tour/LearnMoreTourPager;",
            ">;"
        }
    .end annotation
.end field

.field private final tourPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LandingScreenSelector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tour/LearnMoreTourPager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/singlesignon/SingleSignOn;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LandingCanceledListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/CreateAccountCanceledListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/FinalLogInCheck;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 85
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 86
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 87
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->busProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 88
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->legacyAuthenticatorProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->accountStatusServiceProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 90
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->postInstallEncryptedEmailSettingProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 91
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->onboardingStarterProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 92
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->landingScreenSelectorProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 93
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->tourPresenterProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 94
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->tourPagerProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 95
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 96
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->singleSignOnProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 97
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->landingCanceledListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 98
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->createAccountCanceledListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 99
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->adAnalyticsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 100
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->finalizeLoginFailureDialogFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 101
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->additionalServicesProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 102
    iput-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->finalLogInCheckProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LandingScreenSelector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tour/LearnMoreTourPager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/singlesignon/SingleSignOn;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LandingCanceledListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/CreateAccountCanceledListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/FinalLogInCheck;",
            ">;)",
            "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    .line 126
    new-instance v19, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;

    move-object/from16 v0, v19

    invoke-direct/range {v0 .. v18}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v19
.end method

.method public static newInstance(Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/badbus/BadBus;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/settings/LocalSetting;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/ui/loggedout/LandingScreenSelector;Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;Lcom/squareup/ui/tour/LearnMoreTourPager;Lcom/squareup/analytics/Analytics;Lcom/squareup/singlesignon/SingleSignOn;Lcom/squareup/ui/loggedout/LandingCanceledListener;Lcom/squareup/ui/login/CreateAccountCanceledListener;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;Ljava/util/Set;Lcom/squareup/ui/loggedout/FinalLogInCheck;)Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;",
            ">;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/account/LegacyAuthenticator;",
            "Lcom/squareup/accountstatus/PersistentAccountStatusService;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            "Lcom/squareup/ui/loggedout/LandingScreenSelector;",
            "Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;",
            "Lcom/squareup/ui/tour/LearnMoreTourPager;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/singlesignon/SingleSignOn;",
            "Lcom/squareup/ui/loggedout/LandingCanceledListener;",
            "Lcom/squareup/ui/login/CreateAccountCanceledListener;",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            "Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;",
            "Lcom/squareup/ui/loggedout/FinalLogInCheck;",
            ")",
            "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    .line 139
    new-instance v19, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    move-object/from16 v0, v19

    invoke-direct/range {v0 .. v18}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;-><init>(Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/badbus/BadBus;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/settings/LocalSetting;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/ui/loggedout/LandingScreenSelector;Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;Lcom/squareup/ui/tour/LearnMoreTourPager;Lcom/squareup/analytics/Analytics;Lcom/squareup/singlesignon/SingleSignOn;Lcom/squareup/ui/loggedout/LandingCanceledListener;Lcom/squareup/ui/login/CreateAccountCanceledListener;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;Ljava/util/Set;Lcom/squareup/ui/loggedout/FinalLogInCheck;)V

    return-object v19
.end method


# virtual methods
.method public get()Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;
    .locals 20

    move-object/from16 v0, p0

    .line 107
    iget-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v2

    iget-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v3

    iget-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/badbus/BadBus;

    iget-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->legacyAuthenticatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/account/LegacyAuthenticator;

    iget-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->accountStatusServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/accountstatus/PersistentAccountStatusService;

    iget-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->postInstallEncryptedEmailSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/settings/LocalSetting;

    iget-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->onboardingStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/onboarding/OnboardingStarter;

    iget-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->landingScreenSelectorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/ui/loggedout/LandingScreenSelector;

    iget-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->tourPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    iget-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->tourPagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/ui/tour/LearnMoreTourPager;

    iget-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->singleSignOnProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/singlesignon/SingleSignOn;

    iget-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->landingCanceledListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/ui/loggedout/LandingCanceledListener;

    iget-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->createAccountCanceledListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/ui/login/CreateAccountCanceledListener;

    iget-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->adAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/adanalytics/AdAnalytics;

    iget-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->finalizeLoginFailureDialogFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;

    iget-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->additionalServicesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Ljava/util/Set;

    iget-object v1, v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->finalLogInCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/ui/loggedout/FinalLogInCheck;

    invoke-static/range {v2 .. v19}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->newInstance(Ldagger/Lazy;Ldagger/Lazy;Lcom/squareup/badbus/BadBus;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/accountstatus/PersistentAccountStatusService;Lcom/squareup/settings/LocalSetting;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/ui/loggedout/LandingScreenSelector;Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;Lcom/squareup/ui/tour/LearnMoreTourPager;Lcom/squareup/analytics/Analytics;Lcom/squareup/singlesignon/SingleSignOn;Lcom/squareup/ui/loggedout/LandingCanceledListener;Lcom/squareup/ui/login/CreateAccountCanceledListener;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/ui/loggedout/FinalizeLoginFailureDialogFactory;Ljava/util/Set;Lcom/squareup/ui/loggedout/FinalLogInCheck;)Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->get()Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    move-result-object v0

    return-object v0
.end method
