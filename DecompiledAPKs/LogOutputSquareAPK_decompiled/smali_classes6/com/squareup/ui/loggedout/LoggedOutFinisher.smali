.class public final Lcom/squareup/ui/loggedout/LoggedOutFinisher;
.super Ljava/lang/Object;
.source "LoggedOutFinisher.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/loggedout/LoggedOutActivity;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoggedOutFinisher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoggedOutFinisher.kt\ncom/squareup/ui/loggedout/LoggedOutFinisher\n*L\n1#1,40:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u0004J\u0006\u0010\u0007\u001a\u00020\u0006J\u000e\u0010\u0008\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u0004R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/loggedout/LoggedOutFinisher;",
        "",
        "()V",
        "activity",
        "Lcom/squareup/ui/loggedout/LoggedOutActivity;",
        "dropActivity",
        "",
        "finish",
        "takeActivity",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private activity:Lcom/squareup/ui/loggedout/LoggedOutActivity;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final dropActivity(Lcom/squareup/ui/loggedout/LoggedOutActivity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutFinisher;->activity:Lcom/squareup/ui/loggedout/LoggedOutActivity;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 26
    check-cast p1, Lcom/squareup/ui/loggedout/LoggedOutActivity;

    iput-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutFinisher;->activity:Lcom/squareup/ui/loggedout/LoggedOutActivity;

    :cond_0
    return-void
.end method

.method public final finish()V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LoggedOutFinisher;->activity:Lcom/squareup/ui/loggedout/LoggedOutActivity;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 35
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity;->useDefaultExitAnimation()V

    .line 36
    invoke-virtual {v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity;->finish()V

    return-void
.end method

.method public final takeActivity(Lcom/squareup/ui/loggedout/LoggedOutActivity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iput-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutFinisher;->activity:Lcom/squareup/ui/loggedout/LoggedOutActivity;

    return-void
.end method
