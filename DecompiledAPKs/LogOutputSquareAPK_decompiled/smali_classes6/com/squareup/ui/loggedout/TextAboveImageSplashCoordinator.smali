.class public final Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "TextAboveImageSplashCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTextAboveImageSplashCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TextAboveImageSplashCoordinator.kt\ncom/squareup/ui/loggedout/TextAboveImageSplashCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,91:1\n1103#2,7:92\n1103#2,7:99\n*E\n*S KotlinDebug\n*F\n+ 1 TextAboveImageSplashCoordinator.kt\ncom/squareup/ui/loggedout/TextAboveImageSplashCoordinator\n*L\n30#1,7:92\n39#1,7:99\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0014\u0010\r\u001a\u00020\n*\u00020\u000e2\u0006\u0010\u0006\u001a\u00020\u000fH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "scopeRunner",
        "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "config",
        "Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;",
        "(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;)V",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "setButtonConfiguration",
        "Lcom/squareup/noho/NohoButton;",
        "Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final config:Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;

.field private final scopeRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "scopeRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "config"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->scopeRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p3, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->config:Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getConfig$p(Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;)Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->config:Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;

    return-object p0
.end method

.method public static final synthetic access$getScopeRunner$p(Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;)Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->scopeRunner:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    return-object p0
.end method

.method private final setButtonConfiguration(Lcom/squareup/noho/NohoButton;Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;)V
    .locals 2

    .line 86
    invoke-virtual {p1}, Lcom/squareup/noho/NohoButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->getTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 87
    invoke-virtual {p2}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->getBackground()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setBackgroundResource(I)V

    .line 88
    invoke-virtual {p2}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->getText()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->setText(I)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->config:Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;

    invoke-virtual {v0}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;->getStyling()Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$StylingConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$StylingConfig;->getBackgroundColor()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 29
    sget v0, Lcom/squareup/loggedout/R$id;->sign_in:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 30
    check-cast v0, Lcom/squareup/noho/NohoButton;

    .line 31
    iget-object v1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->config:Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;

    invoke-virtual {v1}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;->getStyling()Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$StylingConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$StylingConfig;->getSignInButtonConfig()Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->setButtonConfiguration(Lcom/squareup/noho/NohoButton;Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;)V

    .line 32
    check-cast v0, Landroid/view/View;

    .line 92
    new-instance v1, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$$inlined$apply$lambda$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$$inlined$apply$lambda$1;-><init>(Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    sget v0, Lcom/squareup/loggedout/R$id;->create_account:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 39
    check-cast v0, Lcom/squareup/noho/NohoButton;

    .line 40
    iget-object v1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->config:Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;

    invoke-virtual {v1}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;->getStyling()Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$StylingConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$StylingConfig;->getCreateAccountButtonConfig()Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->setButtonConfiguration(Lcom/squareup/noho/NohoButton;Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;)V

    .line 41
    check-cast v0, Landroid/view/View;

    .line 99
    new-instance v1, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$$inlined$apply$lambda$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$$inlined$apply$lambda$2;-><init>(Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    sget v0, Lcom/squareup/loggedout/R$id;->splash_screen_gradient:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->config:Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;

    invoke-virtual {v1}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;->getStyling()Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$StylingConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$StylingConfig;->getShouldShowGradient()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 50
    sget v0, Lcom/squareup/loggedout/R$id;->splash_pager:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/viewpager/widget/ViewPager;

    .line 51
    sget v1, Lcom/squareup/loggedout/R$id;->page_indicator:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 52
    check-cast v1, Lcom/squareup/marin/widgets/MarinPageIndicator;

    .line 53
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinPageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->config:Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;

    invoke-virtual {v3}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;->getStyling()Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$StylingConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$StylingConfig;->getPageIndicatorConfig()Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$PageIndicatorConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$PageIndicatorConfig;->getHighlightColor()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setHighlightColor(I)V

    .line 54
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinPageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->config:Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;

    invoke-virtual {v3}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;->getStyling()Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$StylingConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$StylingConfig;->getPageIndicatorConfig()Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$PageIndicatorConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$PageIndicatorConfig;->getMatteColor()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setMatteColor(I)V

    .line 57
    sget v2, Lcom/squareup/loggedout/R$layout;->text_above_image_splash_page:I

    .line 60
    new-instance v3, Lcom/squareup/ui/loggedout/TextAboveImageSplashPagerAdapter;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v4, "view.context"

    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;->config:Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;

    invoke-virtual {v4}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;->getPages()Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, p1, v4, v2}, Lcom/squareup/ui/loggedout/TextAboveImageSplashPagerAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I)V

    .line 61
    check-cast v3, Landroidx/viewpager/widget/PagerAdapter;

    invoke-virtual {v0, v3}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    const/4 p1, 0x0

    .line 62
    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    .line 64
    sget-object v2, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$3;->INSTANCE:Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$3;

    check-cast v2, Landroidx/viewpager/widget/ViewPager$PageTransformer;

    invoke-virtual {v0, p1, v2}, Landroidx/viewpager/widget/ViewPager;->setPageTransformer(ZLandroidx/viewpager/widget/ViewPager$PageTransformer;)V

    .line 72
    new-instance p1, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$4;

    invoke-direct {p1, p0}, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator$attach$4;-><init>(Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;)V

    check-cast p1, Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/ViewPager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 82
    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setViewPager(Landroidx/viewpager/widget/ViewPager;)V

    return-void
.end method
