.class public final Lcom/squareup/vectoricons/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/vectoricons/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final brands_caviar_24:I = 0x7f0800b6

.field public static final brands_deliveroo_24:I = 0x7f0800b7

.field public static final brands_doordash_24:I = 0x7f0800b8

.field public static final brands_instagram_24:I = 0x7f0800b9

.field public static final brands_just_eat_24:I = 0x7f0800ba

.field public static final brands_postmates_24:I = 0x7f0800bb

.field public static final brands_square_24:I = 0x7f0800bc

.field public static final brands_uber_eats_24:I = 0x7f0800bd

.field public static final brands_weebly_24:I = 0x7f0800be

.field public static final buildings_building_columns_80:I = 0x7f0800c7

.field public static final cart_link:I = 0x7f0800d6

.field public static final chip_card_2_120:I = 0x7f0800d8

.field public static final circle_alert_24:I = 0x7f0800d9

.field public static final circle_alert_96:I = 0x7f0800da

.field public static final circle_arrow_left_24:I = 0x7f0800db

.field public static final circle_check_24:I = 0x7f0800dd

.field public static final circle_check_96:I = 0x7f0800de

.field public static final circle_check_white_96:I = 0x7f0800df

.field public static final circle_circle_arrows_left_right_40:I = 0x7f0800e0

.field public static final circle_circle_check_80:I = 0x7f0800e1

.field public static final circle_filled_check_24:I = 0x7f0800e2

.field public static final circle_minus_24:I = 0x7f0800e4

.field public static final circle_plus_24:I = 0x7f0800e5

.field public static final circle_question_24:I = 0x7f0800e6

.field public static final device_cash_drawer_80:I = 0x7f080120

.field public static final device_laptop_items_40:I = 0x7f080121

.field public static final diagram_box_arrow_cycle_40:I = 0x7f080122

.field public static final diagram_box_copy_40:I = 0x7f080123

.field public static final diagram_box_graph_bar_40:I = 0x7f080124

.field public static final diagram_box_graph_bar_callout_40:I = 0x7f080125

.field public static final diagram_box_stacked_plus_40:I = 0x7f080126

.field public static final documents_envelope_80:I = 0x7f080129

.field public static final documents_time_card_40:I = 0x7f08012a

.field public static final favorites_96:I = 0x7f080137

.field public static final hardware_sq_hardware_reader_magstripe_card_swiping_40:I = 0x7f0801f1

.field public static final icon_arrow_left:I = 0x7f080236

.field public static final icon_arrows_up_down:I = 0x7f080238

.field public static final icon_check:I = 0x7f080244

.field public static final icon_check_thick:I = 0x7f080245

.field public static final icon_chevron_down:I = 0x7f080247

.field public static final icon_chevron_left:I = 0x7f080248

.field public static final icon_chevron_right:I = 0x7f080249

.field public static final icon_chevron_up:I = 0x7f08024a

.field public static final icon_circle_solid_24:I = 0x7f08024b

.field public static final icon_copy:I = 0x7f08024e

.field public static final icon_delete:I = 0x7f08024f

.field public static final icon_delete_24:I = 0x7f080250

.field public static final icon_dotted_cloud_120:I = 0x7f080252

.field public static final icon_magnifying_glass:I = 0x7f080259

.field public static final icon_marketing_shopping_cart_24:I = 0x7f08025a

.field public static final icon_menu_3:I = 0x7f08025b

.field public static final icon_menu_4:I = 0x7f08025d

.field public static final icon_settings:I = 0x7f08026a

.field public static final icon_x:I = 0x7f08026e

.field public static final icon_x_thick:I = 0x7f080270

.field public static final marketing_bag_purse_24:I = 0x7f080389

.field public static final marketing_device_landline_handset_24:I = 0x7f08038a

.field public static final marketing_triangle_warning_24:I = 0x7f08038b

.field public static final misc_address_book_40:I = 0x7f08038c

.field public static final misc_bag_purse_80:I = 0x7f08038d

.field public static final misc_bell_service_24:I = 0x7f08038e

.field public static final misc_logo_square_80:I = 0x7f08038f

.field public static final misc_shopping_cart_40:I = 0x7f080390

.field public static final misc_tag_40:I = 0x7f080391

.field public static final misc_tag_percent_28:I = 0x7f080392

.field public static final payment_card_amex_24:I = 0x7f08042a

.field public static final payment_card_chip_24:I = 0x7f08042b

.field public static final payment_card_diners_club_24:I = 0x7f08042c

.field public static final payment_card_discover_24:I = 0x7f08042d

.field public static final payment_card_gift_card_24:I = 0x7f08042e

.field public static final payment_card_gift_ribbon_40:I = 0x7f08042f

.field public static final payment_card_ic_24:I = 0x7f080430

.field public static final payment_card_id_24:I = 0x7f080431

.field public static final payment_card_interac_24:I = 0x7f080432

.field public static final payment_card_jcb_24:I = 0x7f080433

.field public static final payment_card_mastercard_24:I = 0x7f080434

.field public static final payment_card_quicpay_24:I = 0x7f080435

.field public static final payment_card_swipe_24:I = 0x7f080436

.field public static final payment_card_union_pay_24:I = 0x7f080437

.field public static final payment_card_visa_24:I = 0x7f080438

.field public static final reader_magstripe_card_swiping_96:I = 0x7f08047a

.field public static final sq_product_capital_24:I = 0x7f0804a7

.field public static final sq_product_deposits_24:I = 0x7f0804a8

.field public static final sq_product_invoices_24:I = 0x7f0804a9

.field public static final sq_product_item_services_24:I = 0x7f0804aa

.field public static final sq_product_orders_24:I = 0x7f0804ab

.field public static final sq_product_reports_24:I = 0x7f0804ac

.field public static final sq_product_settings_24:I = 0x7f0804ad

.field public static final sq_product_transactions_24:I = 0x7f0804ae

.field public static final ui_divide_24:I = 0x7f0804de

.field public static final ui_equal_24:I = 0x7f0804df

.field public static final ui_eye_24:I = 0x7f0804e0

.field public static final ui_info_stroked_16:I = 0x7f0804e1

.field public static final ui_items_80:I = 0x7f0804e2

.field public static final ui_link_16:I = 0x7f0804e3

.field public static final ui_lock_24:I = 0x7f0804e4

.field public static final ui_magnifying_glass_24:I = 0x7f0804e6

.field public static final ui_minus_24:I = 0x7f0804e7

.field public static final ui_overflow_24:I = 0x7f0804e8

.field public static final ui_plus_24:I = 0x7f0804e9

.field public static final ui_settings_24:I = 0x7f0804ea

.field public static final ui_settings_80:I = 0x7f0804eb

.field public static final ui_share_24:I = 0x7f0804ec

.field public static final ui_tag_24:I = 0x7f0804ed

.field public static final ui_triangle_warning_80:I = 0x7f0804ee

.field public static final ui_user_40:I = 0x7f0804ef

.field public static final ui_x_24:I = 0x7f0804f0

.field public static final ui_x_filled_16:I = 0x7f0804f1

.field public static final ui_x_stroked_16:I = 0x7f0804f2


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
