.class public final Lcom/squareup/usb/UsbDiscoverer_Factory;
.super Ljava/lang/Object;
.source "UsbDiscoverer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/usb/UsbDiscoverer;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final usbManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/usb/UsbDiscoverer_Factory;->contextProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/usb/UsbDiscoverer_Factory;->usbManagerProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/usb/UsbDiscoverer_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/usb/UsbDiscoverer_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)",
            "Lcom/squareup/usb/UsbDiscoverer_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/usb/UsbDiscoverer_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/usb/UsbDiscoverer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/usb/UsbDiscoverer;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/usb/UsbDiscoverer;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/usb/UsbDiscoverer;-><init>(Landroid/app/Application;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/thread/executor/MainThread;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/usb/UsbDiscoverer;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/usb/UsbDiscoverer_Factory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/usb/UsbDiscoverer_Factory;->usbManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/hardware/usb/UsbManager;

    iget-object v2, p0, Lcom/squareup/usb/UsbDiscoverer_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/thread/executor/MainThread;

    invoke-static {v0, v1, v2}, Lcom/squareup/usb/UsbDiscoverer_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/usb/UsbDiscoverer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/usb/UsbDiscoverer_Factory;->get()Lcom/squareup/usb/UsbDiscoverer;

    move-result-object v0

    return-object v0
.end method
