.class public Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;
.super Lmortar/Presenter;
.source "ActivityVisibilityPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;,
        Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Registration;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/visibilitypresenter/VisibleActivity;",
        ">;"
    }
.end annotation


# instance fields
.field private expectingConfigurationChange:Z

.field private final registrations:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Registration;",
            ">;"
        }
    .end annotation
.end field

.field private started:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 21
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 16
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->registrations:Ljava/util/Set;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;)Ljava/util/Set;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->registrations:Ljava/util/Set;

    return-object p0
.end method


# virtual methods
.method public activityStart()V
    .locals 2

    const/4 v0, 0x1

    .line 55
    iput-boolean v0, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->started:Z

    .line 56
    iget-boolean v0, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->expectingConfigurationChange:Z

    if-nez v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->registrations:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Registration;

    .line 58
    iget-object v1, v1, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Registration;->registrant:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;

    invoke-interface {v1}, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;->activityVisible()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 61
    iput-boolean v0, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->expectingConfigurationChange:Z

    return-void
.end method

.method public activityStop()V
    .locals 2

    const/4 v0, 0x0

    .line 65
    iput-boolean v0, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->started:Z

    .line 66
    invoke-virtual {p0}, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/visibilitypresenter/VisibleActivity;

    invoke-interface {v0}, Lcom/squareup/visibilitypresenter/VisibleActivity;->isChangingConfigurations()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->expectingConfigurationChange:Z

    .line 67
    iget-boolean v0, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->expectingConfigurationChange:Z

    if-nez v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->registrations:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Registration;

    .line 69
    iget-object v1, v1, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Registration;->registrant:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;

    invoke-interface {v1}, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;->activityNoLongerVisible()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected extractBundleService(Lcom/squareup/visibilitypresenter/VisibleActivity;)Lmortar/bundler/BundleService;
    .locals 0

    .line 51
    invoke-interface {p1}, Lcom/squareup/visibilitypresenter/VisibleActivity;->getBundleService()Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/visibilitypresenter/VisibleActivity;

    invoke-virtual {p0, p1}, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->extractBundleService(Lcom/squareup/visibilitypresenter/VisibleActivity;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public isActivityVisible()Z
    .locals 1

    .line 43
    iget-boolean v0, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->started:Z

    return v0
.end method

.method protected onExitScope()V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->registrations:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method public register(Lmortar/MortarScope;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;)V
    .locals 2

    .line 33
    new-instance v0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Registration;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p2, v1}, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Registration;-><init>(Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$1;)V

    .line 34
    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 36
    iget-object p1, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->registrations:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 37
    iget-boolean p1, p0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->started:Z

    if-eqz p1, :cond_0

    .line 38
    invoke-interface {p2}, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;->activityVisible()V

    :cond_0
    return-void
.end method
