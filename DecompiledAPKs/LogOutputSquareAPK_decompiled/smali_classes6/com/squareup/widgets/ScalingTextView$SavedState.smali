.class Lcom/squareup/widgets/ScalingTextView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "ScalingTextView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/ScalingTextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/widgets/ScalingTextView$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final maxSize:F

.field private final minSize:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 146
    new-instance v0, Lcom/squareup/widgets/ScalingTextView$SavedState$1;

    invoke-direct {v0}, Lcom/squareup/widgets/ScalingTextView$SavedState$1;-><init>()V

    sput-object v0, Lcom/squareup/widgets/ScalingTextView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 166
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 167
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/squareup/widgets/ScalingTextView$SavedState;->minSize:F

    .line 168
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result p1

    iput p1, p0, Lcom/squareup/widgets/ScalingTextView$SavedState;->maxSize:F

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/squareup/widgets/ScalingTextView$1;)V
    .locals 0

    .line 145
    invoke-direct {p0, p1}, Lcom/squareup/widgets/ScalingTextView$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcelable;FF)V
    .locals 0

    .line 160
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 161
    iput p2, p0, Lcom/squareup/widgets/ScalingTextView$SavedState;->minSize:F

    .line 162
    iput p3, p0, Lcom/squareup/widgets/ScalingTextView$SavedState;->maxSize:F

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcelable;FFLcom/squareup/widgets/ScalingTextView$1;)V
    .locals 0

    .line 145
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/widgets/ScalingTextView$SavedState;-><init>(Landroid/os/Parcelable;FF)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/widgets/ScalingTextView$SavedState;)F
    .locals 0

    .line 145
    iget p0, p0, Lcom/squareup/widgets/ScalingTextView$SavedState;->minSize:F

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/widgets/ScalingTextView$SavedState;)F
    .locals 0

    .line 145
    iget p0, p0, Lcom/squareup/widgets/ScalingTextView$SavedState;->maxSize:F

    return p0
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 172
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 173
    iget p2, p0, Lcom/squareup/widgets/ScalingTextView$SavedState;->minSize:F

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 174
    iget p2, p0, Lcom/squareup/widgets/ScalingTextView$SavedState;->maxSize:F

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    return-void
.end method
