.class public Lcom/squareup/widgets/OnScreenRectangleEditText;
.super Lcom/squareup/widgets/SelectableEditText;
.source "OnScreenRectangleEditText.java"


# static fields
.field private static final SCROLL_MARGIN_BOTTOM:I = 0x3


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/widgets/SelectableEditText;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/SelectableEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/widgets/SelectableEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private adjustRect(Landroid/graphics/Rect;)V
    .locals 1

    .line 50
    invoke-virtual {p0}, Lcom/squareup/widgets/OnScreenRectangleEditText;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x3

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    return-void
.end method


# virtual methods
.method public getFocusedRect(Landroid/graphics/Rect;)V
    .locals 0

    .line 44
    invoke-super {p0, p1}, Lcom/squareup/widgets/SelectableEditText;->getFocusedRect(Landroid/graphics/Rect;)V

    .line 45
    invoke-direct {p0, p1}, Lcom/squareup/widgets/OnScreenRectangleEditText;->adjustRect(Landroid/graphics/Rect;)V

    return-void
.end method

.method public getOffsetForPosition(FF)I
    .locals 0

    .line 34
    invoke-super {p0, p1, p2}, Lcom/squareup/widgets/SelectableEditText;->getOffsetForPosition(FF)I

    move-result p1

    const/4 p2, -0x1

    if-eq p1, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public requestRectangleOnScreen(Landroid/graphics/Rect;Z)Z
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/widgets/OnScreenRectangleEditText;->adjustRect(Landroid/graphics/Rect;)V

    .line 40
    invoke-super {p0, p1, p2}, Lcom/squareup/widgets/SelectableEditText;->requestRectangleOnScreen(Landroid/graphics/Rect;Z)Z

    move-result p1

    return p1
.end method
