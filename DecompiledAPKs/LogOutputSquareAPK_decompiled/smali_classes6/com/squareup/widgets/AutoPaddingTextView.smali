.class public Lcom/squareup/widgets/AutoPaddingTextView;
.super Lcom/squareup/marketfont/MarketTextView;
.source "AutoPaddingTextView.java"


# instance fields
.field private maxlines:I

.field private final verticalPadding:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 17
    invoke-direct {p0, p1, v0}, Lcom/squareup/widgets/AutoPaddingTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 21
    sget v0, Lcom/squareup/widgets/R$attr;->autoPaddingTextViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/widgets/AutoPaddingTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const v0, 0x7fffffff

    .line 14
    iput v0, p0, Lcom/squareup/widgets/AutoPaddingTextView;->maxlines:I

    .line 26
    sget-object v0, Lcom/squareup/widgets/R$styleable;->AutoPaddingTextView:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 27
    sget p2, Lcom/squareup/widgets/R$styleable;->AutoPaddingTextView_autoPadding:I

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/AutoPaddingTextView;->verticalPadding:I

    .line 28
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 4

    .line 37
    invoke-virtual {p0}, Lcom/squareup/widgets/AutoPaddingTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/squareup/widgets/AutoPaddingTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lcom/squareup/widgets/AutoPaddingTextView;->maxlines:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 43
    invoke-virtual {p0}, Lcom/squareup/widgets/AutoPaddingTextView;->getPaddingLeft()I

    move-result v0

    iget v1, p0, Lcom/squareup/widgets/AutoPaddingTextView;->verticalPadding:I

    invoke-virtual {p0}, Lcom/squareup/widgets/AutoPaddingTextView;->getPaddingLeft()I

    move-result v2

    iget v3, p0, Lcom/squareup/widgets/AutoPaddingTextView;->verticalPadding:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/squareup/widgets/AutoPaddingTextView;->setPadding(IIII)V

    .line 44
    invoke-virtual {p0}, Lcom/squareup/widgets/AutoPaddingTextView;->requestLayout()V

    .line 47
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/squareup/marketfont/MarketTextView;->onMeasure(II)V

    return-void
.end method

.method public setMaxLines(I)V
    .locals 0

    .line 32
    iput p1, p0, Lcom/squareup/widgets/AutoPaddingTextView;->maxlines:I

    .line 33
    invoke-super {p0, p1}, Lcom/squareup/marketfont/MarketTextView;->setMaxLines(I)V

    return-void
.end method
