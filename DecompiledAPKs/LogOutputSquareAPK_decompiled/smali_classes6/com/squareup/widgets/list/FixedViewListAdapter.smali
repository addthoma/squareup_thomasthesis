.class public Lcom/squareup/widgets/list/FixedViewListAdapter;
.super Landroid/widget/BaseAdapter;
.source "FixedViewListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/list/FixedViewListAdapter$Row;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Landroid/widget/BaseAdapter;"
    }
.end annotation


# instance fields
.field private final rows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/widgets/list/FixedViewListAdapter$Row<",
            "TT;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 15
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/widgets/list/FixedViewListAdapter;->rows:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/widgets/list/FixedViewListAdapter$Row<",
            "TT;TV;>;>;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/widgets/list/FixedViewListAdapter;->rows:Ljava/util/List;

    .line 19
    iget-object v0, p0, Lcom/squareup/widgets/list/FixedViewListAdapter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method


# virtual methods
.method public add(ILcom/squareup/widgets/list/FixedViewListAdapter$Row;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/widgets/list/FixedViewListAdapter$Row<",
            "TT;TV;>;)V"
        }
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/squareup/widgets/list/FixedViewListAdapter;->rows:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 53
    invoke-virtual {p0}, Lcom/squareup/widgets/list/FixedViewListAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public clear()V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/widgets/list/FixedViewListAdapter;->rows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 58
    invoke-virtual {p0}, Lcom/squareup/widgets/list/FixedViewListAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public getCount()I
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/widgets/list/FixedViewListAdapter;->rows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/squareup/widgets/list/FixedViewListAdapter$Row;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/squareup/widgets/list/FixedViewListAdapter$Row<",
            "TT;TV;>;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/squareup/widgets/list/FixedViewListAdapter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/list/FixedViewListAdapter$Row;

    return-object p1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 11
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/list/FixedViewListAdapter;->getItem(I)Lcom/squareup/widgets/list/FixedViewListAdapter$Row;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 0

    const/4 p1, -0x1

    return p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    .line 48
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/list/FixedViewListAdapter;->getItem(I)Lcom/squareup/widgets/list/FixedViewListAdapter$Row;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/widgets/list/FixedViewListAdapter$Row;->view:Landroid/view/View;

    return-object p1
.end method

.method public isEnabled(I)Z
    .locals 0

    .line 31
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/list/FixedViewListAdapter;->getItem(I)Lcom/squareup/widgets/list/FixedViewListAdapter$Row;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/widgets/list/FixedViewListAdapter$Row;->access$000(Lcom/squareup/widgets/list/FixedViewListAdapter$Row;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public remove(I)Lcom/squareup/widgets/list/FixedViewListAdapter$Row;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/squareup/widgets/list/FixedViewListAdapter$Row<",
            "TT;TV;>;"
        }
    .end annotation

    .line 68
    iget-object v0, p0, Lcom/squareup/widgets/list/FixedViewListAdapter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/list/FixedViewListAdapter$Row;

    .line 69
    invoke-virtual {p0}, Lcom/squareup/widgets/list/FixedViewListAdapter;->notifyDataSetChanged()V

    return-object p1
.end method

.method public replaceAll(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/widgets/list/FixedViewListAdapter$Row<",
            "TT;TV;>;>;)V"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/widgets/list/FixedViewListAdapter;->rows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 63
    iget-object v0, p0, Lcom/squareup/widgets/list/FixedViewListAdapter;->rows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 64
    invoke-virtual {p0}, Lcom/squareup/widgets/list/FixedViewListAdapter;->notifyDataSetChanged()V

    return-void
.end method
