.class public Lcom/squareup/widgets/SelectableEditText;
.super Lcom/squareup/marketfont/MarketEditText;
.source "SelectableEditText.java"

# interfaces
.implements Lcom/squareup/text/HasSelectableText;


# instance fields
.field private scrubbingTextWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

.field private final selectionWatchers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/text/SelectionWatcher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/squareup/marketfont/MarketEditText;-><init>(Landroid/content/Context;)V

    .line 18
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/widgets/SelectableEditText;->selectionWatchers:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/squareup/marketfont/MarketEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/widgets/SelectableEditText;->selectionWatchers:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/marketfont/MarketEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 18
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/widgets/SelectableEditText;->selectionWatchers:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/widgets/SelectableEditText;->selectionWatchers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .line 34
    instance-of v0, p1, Lcom/squareup/text/ScrubbingTextWatcher;

    if-eqz v0, :cond_1

    .line 35
    iget-object v0, p0, Lcom/squareup/widgets/SelectableEditText;->scrubbingTextWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    if-nez v0, :cond_0

    .line 39
    move-object v0, p1

    check-cast v0, Lcom/squareup/text/ScrubbingTextWatcher;

    iput-object v0, p0, Lcom/squareup/widgets/SelectableEditText;->scrubbingTextWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    goto :goto_0

    .line 36
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "One ScrubbingTextWatcher per TextView please. Use ScrubbingTextWatcher#addScrubber() to combine additional scrubbers."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 41
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Lcom/squareup/marketfont/MarketEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public synthetic lambda$postRestartInput$0$SelectableEditText()V
    .locals 0

    .line 72
    invoke-static {p0}, Lcom/squareup/util/Views;->restartInput(Landroid/view/View;)V

    return-void
.end method

.method protected onSelectionChanged(II)V
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/widgets/SelectableEditText;->selectionWatchers:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 60
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/SelectionWatcher;

    .line 61
    invoke-interface {v1, p1, p2}, Lcom/squareup/text/SelectionWatcher;->onSelectionChanged(II)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public postRestartInput()V
    .locals 1

    .line 72
    new-instance v0, Lcom/squareup/widgets/-$$Lambda$SelectableEditText$9tqYxJ-GKK5pb394JWvJc3jCRI4;

    invoke-direct {v0, p0}, Lcom/squareup/widgets/-$$Lambda$SelectableEditText$9tqYxJ-GKK5pb394JWvJc3jCRI4;-><init>(Lcom/squareup/widgets/SelectableEditText;)V

    invoke-virtual {p0, v0}, Lcom/squareup/widgets/SelectableEditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public removeSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/widgets/SelectableEditText;->selectionWatchers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public removeTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .line 45
    invoke-super {p0, p1}, Lcom/squareup/marketfont/MarketEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/widgets/SelectableEditText;->scrubbingTextWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    if-ne v0, p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/squareup/widgets/SelectableEditText;->scrubbingTextWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    :cond_0
    return-void
.end method

.method public setCharacterLimit(I)V
    .locals 4

    .line 76
    invoke-virtual {p0}, Lcom/squareup/widgets/SelectableEditText;->getFilters()[Landroid/text/InputFilter;

    move-result-object v0

    .line 77
    array-length v1, v0

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    .line 78
    :goto_0
    array-length v3, v0

    if-ge v2, v3, :cond_0

    .line 79
    aget-object v3, v0, v2

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 81
    :cond_0
    array-length v0, v0

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v2, p1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v0

    .line 82
    invoke-virtual {p0, v1}, Lcom/squareup/widgets/SelectableEditText;->setFilters([Landroid/text/InputFilter;)V

    return-void
.end method
