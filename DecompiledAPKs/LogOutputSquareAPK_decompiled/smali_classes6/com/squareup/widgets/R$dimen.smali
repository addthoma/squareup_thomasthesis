.class public final Lcom/squareup/widgets/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final badge_margin_bottom:I = 0x7f070071

.field public static final default_digit_input_text_size:I = 0x7f0700f0

.field public static final default_title_indicator_clip_padding:I = 0x7f0700fb

.field public static final default_title_indicator_footer_indicator_height:I = 0x7f0700fc

.field public static final default_title_indicator_footer_indicator_underline_padding:I = 0x7f0700fd

.field public static final default_title_indicator_footer_line_height:I = 0x7f0700fe

.field public static final default_title_indicator_footer_padding:I = 0x7f0700ff

.field public static final default_title_indicator_text_size:I = 0x7f070100

.field public static final default_title_indicator_title_padding:I = 0x7f070101

.field public static final default_title_indicator_top_padding:I = 0x7f070102

.field public static final holo_gap:I = 0x7f070190

.field public static final holo_gap_double:I = 0x7f070191

.field public static final holo_gap_half:I = 0x7f070192

.field public static final holo_gap_one_and_half:I = 0x7f070193

.field public static final holo_gap_six:I = 0x7f070194

.field public static final holo_gap_triple:I = 0x7f070195

.field public static final holo_gap_two_and_half:I = 0x7f070196

.field public static final holo_list_row_height:I = 0x7f070197

.field public static final holo_list_row_height_small:I = 0x7f070198

.field public static final marin_gap_medium:I = 0x7f07022e

.field public static final marin_gap_medium_lollipop:I = 0x7f07022f

.field public static final marin_gap_medium_negated:I = 0x7f070230

.field public static final marin_gap_small:I = 0x7f070243

.field public static final marin_gap_tiny:I = 0x7f070247

.field public static final max_drag_scroll_per_frame:I = 0x7f0702bb

.field public static final scaling_text_view_minimum_font_size:I = 0x7f0704aa

.field public static final text_android_large:I = 0x7f070526

.field public static final text_android_medium:I = 0x7f070527

.field public static final text_android_micro:I = 0x7f070528

.field public static final text_android_mini:I = 0x7f070529

.field public static final text_android_small:I = 0x7f07052a

.field public static final text_android_xlarge:I = 0x7f07052b

.field public static final transactions_history_badge_margin_right:I = 0x7f07054b

.field public static final wrapping_name_value_row_minimum_font_size:I = 0x7f070573


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
