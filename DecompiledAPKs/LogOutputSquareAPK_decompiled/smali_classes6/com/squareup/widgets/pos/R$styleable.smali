.class public final Lcom/squareup/widgets/pos/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/pos/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AppletSectionsListView:[I

.field public static final AppletSectionsListView_sq_valueColor:I = 0x0

.field public static final AspectRatioImageView:[I

.field public static final AspectRatioImageView_dominantMeasurement:I = 0x0

.field public static final AspectRatioImageView_sq_aspectRatio:I = 0x1

.field public static final AspectRatioImageView_sq_aspectRatioEnabled:I = 0x2

.field public static final AspectRatioLayout:[I

.field public static final AspectRatioLayout_sq_aspectRatio:I = 0x0

.field public static final AspectRatioLayout_sq_aspectRatioEnabled:I = 0x1

.field public static final CardEditor:[I

.field public static final CardEditor_android_textSize:I = 0x0

.field public static final CardEditor_compact:I = 0x1

.field public static final CardEditor_hideGlyph:I = 0x2

.field public static final CardEditor_sqInnerShadowColor:I = 0x3

.field public static final CardEditor_sqInnerShadowHeight:I = 0x4

.field public static final DialogCardView:[I

.field public static final DialogCardView_landscapePhoneAsCard:I = 0x0

.field public static final ErrorsBarView:[I

.field public static final ErrorsBarView_android_divider:I = 0x3

.field public static final ErrorsBarView_android_paddingLeft:I = 0x1

.field public static final ErrorsBarView_android_paddingRight:I = 0x2

.field public static final ErrorsBarView_android_showDividers:I = 0x4

.field public static final ErrorsBarView_android_textColor:I = 0x0

.field public static final ErrorsBarView_errorRowHeight:I = 0x5

.field public static final GlyphButtonEditText:[I

.field public static final GlyphButtonEditText_android_hint:I = 0x1

.field public static final GlyphButtonEditText_android_imeOptions:I = 0x3

.field public static final GlyphButtonEditText_android_inputType:I = 0x2

.field public static final GlyphButtonEditText_android_text:I = 0x0

.field public static final GlyphButtonEditText_buttonGlyph:I = 0x4

.field public static final GlyphButtonEditText_buttonGlyphBackground:I = 0x5

.field public static final GlyphButtonEditText_buttonGlyphColor:I = 0x6

.field public static final GlyphButtonEditText_buttonGlyphHint:I = 0x7

.field public static final GlyphButtonEditText_glyph:I = 0x8

.field public static final GlyphButtonEditText_sq_glyphColor:I = 0x9

.field public static final GlyphRadioRow:[I

.field public static final GlyphRadioRow_android_text:I = 0x0

.field public static final GlyphRadioRow_shortText:I = 0x1

.field public static final GlyphSelectableEditText:[I

.field public static final GlyphSelectableEditText_glyph:I = 0x0

.field public static final LineRow:[I

.field public static final LineRow_chevronVisibility:I = 0x0

.field public static final LineRow_displayValueAsPercent:I = 0x1

.field public static final LineRow_glyph:I = 0x2

.field public static final LineRow_lineRowPadding:I = 0x3

.field public static final LineRow_sq_labelText:I = 0x4

.field public static final LineRow_sq_noteText:I = 0x5

.field public static final LineRow_sq_valueColor:I = 0x6

.field public static final LineRow_sq_valueText:I = 0x7

.field public static final LineRow_titleText:I = 0x8

.field public static final LineRow_titleTextSize:I = 0x9

.field public static final LineRow_valueWeight:I = 0xa

.field public static final LineRow_weight:I = 0xb

.field public static final RectangularGridLayout:[I

.field public static final RectangularGridLayout_columnCount:I = 0x0

.field public static final RectangularGridLayout_gap:I = 0x1

.field public static final RectangularGridLayout_minCellWidth:I = 0x2

.field public static final RectangularGridLayout_rowHeight:I = 0x3

.field public static final SmartLineRow:[I

.field public static final SmartLineRow_chevronVisibility:I = 0x0

.field public static final SmartLineRow_preserveValueText:I = 0x1

.field public static final SmartLineRow_sq_subtitleTextAppearance:I = 0x2

.field public static final SmartLineRow_sq_titleTextAppearance:I = 0x3

.field public static final SmartLineRow_sq_valueColor:I = 0x4

.field public static final SmartLineRow_sq_valueTextAppearance:I = 0x5

.field public static final SmartLineRow_subtitleText:I = 0x6

.field public static final SmartLineRow_titleText:I = 0x7

.field public static final SmartLineRow_titleTextColor:I = 0x8

.field public static final SmartLineRow_weight:I = 0x9

.field public static final StarGroup:[I

.field public static final StarGroup_starFailColor:I = 0x0

.field public static final StarGroup_starGap:I = 0x1

.field public static final StarGroup_starRadius:I = 0x2

.field public static final StarGroup_starSuccessColor:I = 0x3

.field public static final TokenView:[I

.field public static final TokenView_android_saveEnabled:I = 0x1

.field public static final TokenView_android_textColor:I = 0x0

.field public static final XableEditText:[I

.field public static final XableEditText_android_hint:I = 0x1

.field public static final XableEditText_android_imeOptions:I = 0x4

.field public static final XableEditText_android_inputType:I = 0x3

.field public static final XableEditText_android_maxLength:I = 0x2

.field public static final XableEditText_android_text:I = 0x0

.field public static final XableEditText_dismissOnClick:I = 0x5

.field public static final XableEditText_glyph:I = 0x6

.field public static final XableEditText_glyphSpacing:I = 0x7

.field public static final XableEditText_hideBackground:I = 0x8

.field public static final XableEditText_showXOnFocus:I = 0x9

.field public static final XableEditText_showXOnNonEmptyText:I = 0xa

.field public static final XableEditText_sq_glyphColor:I = 0xb

.field public static final XableEditText_xSelector:I = 0xc


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v2, 0x0

    const v3, 0x7f0403ca

    aput v3, v1, v2

    .line 343
    sput-object v1, Lcom/squareup/widgets/pos/R$styleable;->AppletSectionsListView:[I

    const/4 v1, 0x3

    new-array v1, v1, [I

    .line 345
    fill-array-data v1, :array_0

    sput-object v1, Lcom/squareup/widgets/pos/R$styleable;->AspectRatioImageView:[I

    const/4 v1, 0x2

    new-array v3, v1, [I

    .line 349
    fill-array-data v3, :array_1

    sput-object v3, Lcom/squareup/widgets/pos/R$styleable;->AspectRatioLayout:[I

    const/4 v3, 0x5

    new-array v3, v3, [I

    .line 352
    fill-array-data v3, :array_2

    sput-object v3, Lcom/squareup/widgets/pos/R$styleable;->CardEditor:[I

    new-array v3, v0, [I

    const v4, 0x7f040236

    aput v4, v3, v2

    .line 358
    sput-object v3, Lcom/squareup/widgets/pos/R$styleable;->DialogCardView:[I

    const/4 v3, 0x6

    new-array v3, v3, [I

    .line 360
    fill-array-data v3, :array_3

    sput-object v3, Lcom/squareup/widgets/pos/R$styleable;->ErrorsBarView:[I

    const/16 v3, 0xa

    new-array v4, v3, [I

    .line 367
    fill-array-data v4, :array_4

    sput-object v4, Lcom/squareup/widgets/pos/R$styleable;->GlyphButtonEditText:[I

    new-array v4, v1, [I

    .line 378
    fill-array-data v4, :array_5

    sput-object v4, Lcom/squareup/widgets/pos/R$styleable;->GlyphRadioRow:[I

    new-array v0, v0, [I

    const v4, 0x7f0401a0

    aput v4, v0, v2

    .line 381
    sput-object v0, Lcom/squareup/widgets/pos/R$styleable;->GlyphSelectableEditText:[I

    const/16 v0, 0xc

    new-array v0, v0, [I

    .line 383
    fill-array-data v0, :array_6

    sput-object v0, Lcom/squareup/widgets/pos/R$styleable;->LineRow:[I

    const/4 v0, 0x4

    new-array v2, v0, [I

    .line 396
    fill-array-data v2, :array_7

    sput-object v2, Lcom/squareup/widgets/pos/R$styleable;->RectangularGridLayout:[I

    new-array v2, v3, [I

    .line 401
    fill-array-data v2, :array_8

    sput-object v2, Lcom/squareup/widgets/pos/R$styleable;->SmartLineRow:[I

    new-array v0, v0, [I

    .line 412
    fill-array-data v0, :array_9

    sput-object v0, Lcom/squareup/widgets/pos/R$styleable;->StarGroup:[I

    new-array v0, v1, [I

    .line 417
    fill-array-data v0, :array_a

    sput-object v0, Lcom/squareup/widgets/pos/R$styleable;->TokenView:[I

    const/16 v0, 0xd

    new-array v0, v0, [I

    .line 420
    fill-array-data v0, :array_b

    sput-object v0, Lcom/squareup/widgets/pos/R$styleable;->XableEditText:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f040132
        0x7f0403c2
        0x7f0403c3
    .end array-data

    :array_1
    .array-data 4
        0x7f0403c2
        0x7f0403c3
    .end array-data

    :array_2
    .array-data 4
        0x1010095
        0x7f0400eb
        0x7f0401b9
        0x7f040388
        0x7f040389
    .end array-data

    :array_3
    .array-data 4
        0x1010098
        0x10100d6
        0x10100d8
        0x1010129
        0x1010329
        0x7f040164
    .end array-data

    :array_4
    .array-data 4
        0x101014f
        0x1010150
        0x1010220
        0x1010264
        0x7f04007e
        0x7f04007f
        0x7f040080
        0x7f040081
        0x7f0401a0
        0x7f0403c4
    .end array-data

    :array_5
    .array-data 4
        0x101014f
        0x7f040347
    .end array-data

    :array_6
    .array-data 4
        0x7f0400a9
        0x7f04012b
        0x7f0401a0
        0x7f04027f
        0x7f0403c5
        0x7f0403c7
        0x7f0403ca
        0x7f0403cb
        0x7f040466
        0x7f040469
        0x7f040492
        0x7f040497
    .end array-data

    :array_7
    .array-data 4
        0x7f0400e9
        0x7f04019d
        0x7f0402c1
        0x7f040331
    .end array-data

    :array_8
    .array-data 4
        0x7f0400a9
        0x7f04031b
        0x7f0403c8
        0x7f0403c9
        0x7f0403ca
        0x7f0403cc
        0x7f040403
        0x7f040466
        0x7f040468
        0x7f040497
    .end array-data

    :array_9
    .array-data 4
        0x7f0403da
        0x7f0403db
        0x7f0403dc
        0x7f0403dd
    .end array-data

    :array_a
    .array-data 4
        0x1010098
        0x10100e7
    .end array-data

    :array_b
    .array-data 4
        0x101014f
        0x1010150
        0x1010160
        0x1010220
        0x1010264
        0x7f040129
        0x7f0401a0
        0x7f0401a9
        0x7f0401b7
        0x7f040351
        0x7f040352
        0x7f0403c4
        0x7f0404a8
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
