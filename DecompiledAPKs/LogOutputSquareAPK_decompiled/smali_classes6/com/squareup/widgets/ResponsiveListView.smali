.class public Lcom/squareup/widgets/ResponsiveListView;
.super Landroid/widget/ListView;
.source "ResponsiveListView.java"


# instance fields
.field protected final controller:Lcom/squareup/widgets/ResponsiveViewController;

.field private deferredSelector:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x1010074

    .line 18
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/widgets/ResponsiveListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    new-instance p3, Lcom/squareup/widgets/ResponsiveViewController;

    invoke-direct {p3, p1, p2, p0}, Lcom/squareup/widgets/ResponsiveViewController;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    iput-object p3, p0, Lcom/squareup/widgets/ResponsiveListView;->controller:Lcom/squareup/widgets/ResponsiveViewController;

    const/high16 p1, 0x2000000

    .line 25
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/ResponsiveListView;->setScrollBarStyle(I)V

    .line 28
    iget-object p1, p0, Lcom/squareup/widgets/ResponsiveListView;->deferredSelector:Landroid/graphics/drawable/Drawable;

    if-eqz p1, :cond_0

    .line 29
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/ResponsiveListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    const/4 p1, 0x0

    .line 30
    iput-object p1, p0, Lcom/squareup/widgets/ResponsiveListView;->deferredSelector:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/widgets/ResponsiveListView;->controller:Lcom/squareup/widgets/ResponsiveViewController;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/widgets/ResponsiveViewController;->measure(II)V

    .line 36
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onMeasure(II)V

    return-void
.end method

.method public setSelector(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/widgets/ResponsiveListView;->controller:Lcom/squareup/widgets/ResponsiveViewController;

    if-nez v0, :cond_0

    .line 43
    iput-object p1, p0, Lcom/squareup/widgets/ResponsiveListView;->deferredSelector:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 45
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void
.end method
