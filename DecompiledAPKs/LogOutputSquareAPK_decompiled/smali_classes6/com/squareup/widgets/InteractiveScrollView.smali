.class public Lcom/squareup/widgets/InteractiveScrollView;
.super Landroid/widget/ScrollView;
.source "InteractiveScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/InteractiveScrollView$ScrollViewListener;
    }
.end annotation


# instance fields
.field private listener:Lcom/squareup/widgets/InteractiveScrollView$ScrollViewListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private updateListener()V
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/widgets/-$$Lambda$InteractiveScrollView$oJn-yBk3YIK5whHM5oyNKJU35U8;

    invoke-direct {v0, p0}, Lcom/squareup/widgets/-$$Lambda$InteractiveScrollView$oJn-yBk3YIK5whHM5oyNKJU35U8;-><init>(Lcom/squareup/widgets/InteractiveScrollView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/widgets/InteractiveScrollView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public getListener()Lcom/squareup/widgets/InteractiveScrollView$ScrollViewListener;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/widgets/InteractiveScrollView;->listener:Lcom/squareup/widgets/InteractiveScrollView$ScrollViewListener;

    return-object v0
.end method

.method public synthetic lambda$updateListener$0$InteractiveScrollView()V
    .locals 2

    .line 40
    invoke-virtual {p0}, Lcom/squareup/widgets/InteractiveScrollView;->computeVerticalScrollOffset()I

    move-result v0

    .line 42
    iget-object v1, p0, Lcom/squareup/widgets/InteractiveScrollView;->listener:Lcom/squareup/widgets/InteractiveScrollView$ScrollViewListener;

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    .line 44
    invoke-interface {v1}, Lcom/squareup/widgets/InteractiveScrollView$ScrollViewListener;->onScrollTopReached()V

    goto :goto_0

    .line 46
    :cond_0
    invoke-interface {v1}, Lcom/squareup/widgets/InteractiveScrollView$ScrollViewListener;->onScrollTopNotReached()V

    :cond_1
    :goto_0
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 0

    .line 24
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 25
    invoke-direct {p0}, Lcom/squareup/widgets/InteractiveScrollView;->updateListener()V

    return-void
.end method

.method public scrollTo(II)V
    .locals 0

    .line 18
    invoke-super {p0, p1, p2}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 19
    invoke-direct {p0}, Lcom/squareup/widgets/InteractiveScrollView;->updateListener()V

    return-void
.end method

.method public setListener(Lcom/squareup/widgets/InteractiveScrollView$ScrollViewListener;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/squareup/widgets/InteractiveScrollView;->listener:Lcom/squareup/widgets/InteractiveScrollView$ScrollViewListener;

    .line 35
    invoke-direct {p0}, Lcom/squareup/widgets/InteractiveScrollView;->updateListener()V

    return-void
.end method
