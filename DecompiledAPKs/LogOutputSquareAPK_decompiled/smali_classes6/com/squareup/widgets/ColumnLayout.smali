.class public Lcom/squareup/widgets/ColumnLayout;
.super Landroid/widget/LinearLayout;
.source "ColumnLayout.java"


# instance fields
.field private addingColumns:Z

.field private leftColumnLayout:Landroid/widget/LinearLayout;

.field private marginBetweenColumns:I

.field private rightColumnLayout:Landroid/widget/LinearLayout;

.field private twoColumns:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    .line 20
    iput-boolean v0, p0, Lcom/squareup/widgets/ColumnLayout;->addingColumns:Z

    .line 27
    sget-object v1, Lcom/squareup/widgets/R$styleable;->ColumnLayout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 28
    sget v1, Lcom/squareup/widgets/R$styleable;->ColumnLayout_twoColumns:I

    const/4 v2, 0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/squareup/widgets/ColumnLayout;->twoColumns:Z

    .line 29
    sget v1, Lcom/squareup/widgets/R$styleable;->ColumnLayout_sq_marginBetweenColumns:I

    .line 30
    invoke-virtual {p2, v1, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/squareup/widgets/ColumnLayout;->marginBetweenColumns:I

    .line 32
    iget-boolean v1, p0, Lcom/squareup/widgets/ColumnLayout;->twoColumns:Z

    if-eqz v1, :cond_1

    .line 33
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/ColumnLayout;->setOrientation(I)V

    .line 35
    sget v1, Lcom/squareup/widgets/R$styleable;->CheckableGroup_android_divider:I

    invoke-virtual {p2, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 37
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 40
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/widgets/ColumnLayout;->createColumn(Landroid/content/Context;)Landroid/widget/LinearLayout;

    move-result-object v3

    iput-object v3, p0, Lcom/squareup/widgets/ColumnLayout;->leftColumnLayout:Landroid/widget/LinearLayout;

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/widgets/ColumnLayout;->createColumn(Landroid/content/Context;)Landroid/widget/LinearLayout;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/widgets/ColumnLayout;->rightColumnLayout:Landroid/widget/LinearLayout;

    .line 43
    invoke-virtual {p0, p2, p2}, Lcom/squareup/widgets/ColumnLayout;->setColumnDividers(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 45
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 p2, 0x3f800000    # 1.0f

    const/4 v3, -0x2

    invoke-direct {p1, v0, v3, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 46
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v0, v3, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 47
    iget p2, p0, Lcom/squareup/widgets/ColumnLayout;->marginBetweenColumns:I

    add-int/2addr v1, p2

    invoke-virtual {v4, v1, v0, v0, v0}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 49
    iput-boolean v2, p0, Lcom/squareup/widgets/ColumnLayout;->addingColumns:Z

    .line 50
    iget-object p2, p0, Lcom/squareup/widgets/ColumnLayout;->leftColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, p2, p1}, Lcom/squareup/widgets/ColumnLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 51
    iget-object p1, p0, Lcom/squareup/widgets/ColumnLayout;->rightColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, p1, v4}, Lcom/squareup/widgets/ColumnLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 52
    iput-boolean v0, p0, Lcom/squareup/widgets/ColumnLayout;->addingColumns:Z

    goto :goto_1

    .line 54
    :cond_1
    invoke-virtual {p0, v2}, Lcom/squareup/widgets/ColumnLayout;->setOrientation(I)V

    :goto_1
    return-void
.end method

.method private createColumn(Landroid/content/Context;)Landroid/widget/LinearLayout;
    .locals 2

    .line 108
    new-instance v0, Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x1

    .line 109
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 110
    invoke-virtual {p0}, Lcom/squareup/widgets/ColumnLayout;->getShowDividers()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    return-object v0
.end method


# virtual methods
.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .line 72
    iget-boolean v0, p0, Lcom/squareup/widgets/ColumnLayout;->addingColumns:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/squareup/widgets/ColumnLayout;->twoColumns:Z

    if-nez v0, :cond_0

    goto :goto_1

    .line 77
    :cond_0
    iget-object p2, p0, Lcom/squareup/widgets/ColumnLayout;->leftColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result p2

    iget-object v0, p0, Lcom/squareup/widgets/ColumnLayout;->rightColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 78
    iget-object p2, p0, Lcom/squareup/widgets/ColumnLayout;->leftColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p2, p1, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 80
    :cond_1
    iget-object p2, p0, Lcom/squareup/widgets/ColumnLayout;->rightColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p2, p1, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    return-void

    .line 73
    :cond_2
    :goto_1
    invoke-super {p0, p1, p2, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public isTwoColumn()Z
    .locals 1

    .line 68
    iget-boolean v0, p0, Lcom/squareup/widgets/ColumnLayout;->twoColumns:Z

    return v0
.end method

.method public removeAllViews()V
    .locals 1

    .line 85
    iget-boolean v0, p0, Lcom/squareup/widgets/ColumnLayout;->addingColumns:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/widgets/ColumnLayout;->twoColumns:Z

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/squareup/widgets/ColumnLayout;->leftColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 87
    iget-object v0, p0, Lcom/squareup/widgets/ColumnLayout;->rightColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 88
    invoke-virtual {p0}, Lcom/squareup/widgets/ColumnLayout;->requestLayout()V

    .line 89
    invoke-virtual {p0}, Lcom/squareup/widgets/ColumnLayout;->invalidate()V

    goto :goto_0

    .line 91
    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->removeAllViews()V

    :goto_0
    return-void
.end method

.method public removeViewAt(I)V
    .locals 1

    .line 96
    iget-boolean v0, p0, Lcom/squareup/widgets/ColumnLayout;->addingColumns:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/widgets/ColumnLayout;->twoColumns:Z

    if-nez v0, :cond_1

    .line 97
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 100
    :cond_1
    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_2

    .line 101
    iget-object v0, p0, Lcom/squareup/widgets/ColumnLayout;->leftColumnLayout:Landroid/widget/LinearLayout;

    div-int/lit8 p1, p1, 0x2

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    goto :goto_0

    .line 103
    :cond_2
    iget-object v0, p0, Lcom/squareup/widgets/ColumnLayout;->rightColumnLayout:Landroid/widget/LinearLayout;

    div-int/lit8 p1, p1, 0x2

    add-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    :goto_0
    return-void
.end method

.method public setColumnDividers(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/widgets/ColumnLayout;->leftColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 64
    iget-object p1, p0, Lcom/squareup/widgets/ColumnLayout;->rightColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
