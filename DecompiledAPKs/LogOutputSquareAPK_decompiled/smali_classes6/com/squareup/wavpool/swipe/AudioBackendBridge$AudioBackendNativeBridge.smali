.class public Lcom/squareup/wavpool/swipe/AudioBackendBridge$AudioBackendNativeBridge;
.super Ljava/lang/Object;
.source "AudioBackendBridge.java"

# interfaces
.implements Lcom/squareup/wavpool/swipe/AudioBackendBridge;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/wavpool/swipe/AudioBackendBridge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AudioBackendNativeBridge"
.end annotation


# instance fields
.field private final audioBackendNative:Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AudioBackendBridge$AudioBackendNativeBridge;->audioBackendNative:Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;

    return-void
.end method


# virtual methods
.method public cr_comms_backend_audio_alloc()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendBridge$AudioBackendNativeBridge;->audioBackendNative:Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;

    invoke-interface {v0}, Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;->cr_comms_backend_audio_alloc()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    move-result-object v0

    return-object v0
.end method

.method public cr_comms_backend_audio_enable_tx_for_connection(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendBridge$AudioBackendNativeBridge;->audioBackendNative:Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;->cr_comms_backend_audio_enable_tx_for_connection(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V

    return-void
.end method

.method public cr_comms_backend_audio_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendBridge$AudioBackendNativeBridge;->audioBackendNative:Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;->cr_comms_backend_audio_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    move-result-object p1

    return-object p1
.end method

.method public cr_comms_backend_audio_notify_phy_tx_complete(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendBridge$AudioBackendNativeBridge;->audioBackendNative:Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;->cr_comms_backend_audio_notify_phy_tx_complete(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V

    return-void
.end method

.method public cr_comms_backend_audio_shutdown(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendBridge$AudioBackendNativeBridge;->audioBackendNative:Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;->cr_comms_backend_audio_shutdown(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V

    return-void
.end method

.method public decode_r4_packet(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;I[S)Ljava/lang/Object;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendBridge$AudioBackendNativeBridge;->audioBackendNative:Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;->decode_r4_packet(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;I[S)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public feed_audio_samples(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;Ljava/lang/Object;II)V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendBridge$AudioBackendNativeBridge;->audioBackendNative:Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;->feed_audio_samples(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;Ljava/lang/Object;II)V

    return-void
.end method

.method public initialize_backend_audio(IILcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;
    .locals 6

    .line 69
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendBridge$AudioBackendNativeBridge;->audioBackendNative:Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;->initialize_backend_audio(IILcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;

    move-result-object p1

    return-object p1
.end method

.method public set_legacy_reader_type(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;I)V
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendBridge$AudioBackendNativeBridge;->audioBackendNative:Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;->set_legacy_reader_type(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;I)V

    return-void
.end method
