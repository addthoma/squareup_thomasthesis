.class public abstract Lcom/squareup/wavpool/swipe/AsyncDecoderModule;
.super Ljava/lang/Object;
.source "AsyncDecoderModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/wavpool/swipe/DecoderModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$provideDecoderExecutor$0(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 2

    .line 17
    new-instance v0, Ljava/lang/Thread;

    const-string v1, "Sq-decoder"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideDecoderExecutor()Ljava/util/concurrent/ExecutorService;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 16
    sget-object v0, Lcom/squareup/wavpool/swipe/-$$Lambda$AsyncDecoderModule$HrJRADiUZLlArCNPtmkHMxE_rxU;->INSTANCE:Lcom/squareup/wavpool/swipe/-$$Lambda$AsyncDecoderModule$HrJRADiUZLlArCNPtmkHMxE_rxU;

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method abstract provideDecoder(Lcom/squareup/squarewave/ClassifyingDecoder;)Lcom/squareup/squarewave/SignalDecoder;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
