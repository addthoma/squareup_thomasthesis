.class public final Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;
.super Ljava/lang/Object;
.source "DecoderModule_ProvideClassifyingDecoderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/squarewave/ClassifyingDecoder;",
        ">;"
    }
.end annotation


# instance fields
.field private final gen2SignalDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private final o1SignalDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/o1/O1SignalDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private final r4FastSignalDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/SignalDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private final r4SlowSignalDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/SignalDecoder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/o1/O1SignalDecoder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/SignalDecoder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/SignalDecoder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;->o1SignalDecoderProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;->r4FastSignalDecoderProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;->r4SlowSignalDecoderProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;->gen2SignalDecoderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/o1/O1SignalDecoder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/SignalDecoder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/SignalDecoder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;"
        }
    .end annotation

    .line 50
    new-instance v0, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideClassifyingDecoder(Lcom/squareup/squarewave/o1/O1SignalDecoder;Lcom/squareup/squarewave/SignalDecoder;Lcom/squareup/squarewave/SignalDecoder;Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;)Lcom/squareup/squarewave/ClassifyingDecoder;
    .locals 0

    .line 56
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/wavpool/swipe/DecoderModule;->provideClassifyingDecoder(Lcom/squareup/squarewave/o1/O1SignalDecoder;Lcom/squareup/squarewave/SignalDecoder;Lcom/squareup/squarewave/SignalDecoder;Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;)Lcom/squareup/squarewave/ClassifyingDecoder;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/squarewave/ClassifyingDecoder;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/squarewave/ClassifyingDecoder;
    .locals 4

    .line 42
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;->o1SignalDecoderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/squarewave/o1/O1SignalDecoder;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;->r4FastSignalDecoderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/squarewave/SignalDecoder;

    iget-object v2, p0, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;->r4SlowSignalDecoderProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/squarewave/SignalDecoder;

    iget-object v3, p0, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;->gen2SignalDecoderProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;->provideClassifyingDecoder(Lcom/squareup/squarewave/o1/O1SignalDecoder;Lcom/squareup/squarewave/SignalDecoder;Lcom/squareup/squarewave/SignalDecoder;Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;)Lcom/squareup/squarewave/ClassifyingDecoder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;->get()Lcom/squareup/squarewave/ClassifyingDecoder;

    move-result-object v0

    return-object v0
.end method
