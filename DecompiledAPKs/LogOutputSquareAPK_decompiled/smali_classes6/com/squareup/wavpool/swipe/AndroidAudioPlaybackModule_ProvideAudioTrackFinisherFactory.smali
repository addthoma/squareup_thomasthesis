.class public final Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioTrackFinisherFactory;
.super Ljava/lang/Object;
.source "AndroidAudioPlaybackModule_ProvideAudioTrackFinisherFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/wavpool/swipe/AudioTrackFinisher;",
        ">;"
    }
.end annotation


# instance fields
.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final sampleRateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioTrackFinisherFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioTrackFinisherFactory;->sampleRateProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioTrackFinisherFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioTrackFinisherFactory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioTrackFinisherFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioTrackFinisherFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideAudioTrackFinisher(Lcom/squareup/thread/executor/MainThread;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AudioTrackFinisher;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/AudioTrackFinisher;"
        }
    .end annotation

    .line 40
    invoke-static {p0, p1}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule;->provideAudioTrackFinisher(Lcom/squareup/thread/executor/MainThread;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AudioTrackFinisher;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/wavpool/swipe/AudioTrackFinisher;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioTrackFinisherFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioTrackFinisherFactory;->sampleRateProvider:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioTrackFinisherFactory;->provideAudioTrackFinisher(Lcom/squareup/thread/executor/MainThread;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AudioTrackFinisher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioTrackFinisherFactory;->get()Lcom/squareup/wavpool/swipe/AudioTrackFinisher;

    move-result-object v0

    return-object v0
.end method
