.class final Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2;
.super Lkotlin/jvm/internal/Lambda;
.source "AndroidHeadsetConnectionListener.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;-><init>(Landroid/app/Application;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lio/reactivex/Observable<",
        "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;


# direct methods
.method constructor <init>(Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2;->this$0:Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;"
        }
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2;->this$0:Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;->access$getApplication$p(Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;)Landroid/app/Application;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-string v1, "android.intent.action.HEADSET_PLUG"

    const-string v2, "android.media.AUDIO_BECOMING_NOISY"

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2ContextsKt;->registerForIntents(Landroid/content/Context;[Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v0

    .line 17
    new-instance v1, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2$1;

    invoke-direct {v1, p0}, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2$1;-><init>(Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 27
    new-instance v1, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v2}, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;-><init>(ZZ)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const/4 v1, 0x1

    .line 29
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lio/reactivex/observables/ConnectableObservable;->autoConnect()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener$onHeadsetStateChangedSource$2;->invoke()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
