.class public Lcom/squareup/wavpool/swipe/MicRecorder;
.super Ljava/lang/Object;
.source "MicRecorder.java"

# interfaces
.implements Lcom/squareup/wavpool/swipe/Recorder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;
    }
.end annotation


# static fields
.field private static final AUDIO_INIT_THRESHOLD_MS:J = 0x3e8L

.field private static final AUDIO_RECORD_BUFFER_MILLIS:I = 0x7530

.field private static final AUDIO_RECORD_BUFFER_SIZE_BYTES:I = 0xea60

.field private static final EMPTY_BUFFER_DELAY_MS:J = 0xfaL

.field public static final PROCESS_BUFFER_MILLIS:I = 0x64

.field public static final PROCESS_BUFFER_SIZE_BYTES:I = 0xc8

.field private static final REPORTING_RATE_LIMIT_MS:J = 0xdbba0L

.field private static final RESTART:Z = true

.field private static final STOP_THREAD:Z


# instance fields
.field private final audioFilterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/AudioFilter;",
            ">;"
        }
    .end annotation
.end field

.field private backgroundRecordingTimestamp:J

.field private final backgroundWork:Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;

.field private final bus:Lcom/squareup/badbus/BadEventSink;

.field private final crashnado:Lcom/squareup/crashnado/Crashnado;

.field private final deviceParams:Lcom/squareup/wavpool/swipe/AndroidDeviceParams;

.field private headsetConnectionState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

.field private headsetDisposable:Lio/reactivex/disposables/CompositeDisposable;

.field private final headsetListener:Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private recorderThread:Landroid/os/HandlerThread;

.field private volatile running:Z

.field private final sampleRateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private volatile state:Lcom/squareup/wavpool/swipe/Recorder$State;

.field private final telephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Lcom/squareup/crashnado/Crashnado;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/wavpool/swipe/AndroidDeviceParams;Lcom/squareup/thread/executor/MainThread;Landroid/telephony/TelephonyManager;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crashnado/Crashnado;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/AudioFilter;",
            ">;",
            "Lcom/squareup/wavpool/swipe/AndroidDeviceParams;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Landroid/telephony/TelephonyManager;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ")V"
        }
    .end annotation

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    .line 63
    iput-wide v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->backgroundRecordingTimestamp:J

    .line 66
    sget-object v0, Lcom/squareup/wavpool/swipe/Recorder$State;->STOPPED:Lcom/squareup/wavpool/swipe/Recorder$State;

    iput-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->state:Lcom/squareup/wavpool/swipe/Recorder$State;

    .line 68
    new-instance v0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;-><init>(ZZ)V

    iput-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->headsetConnectionState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    .line 69
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->headsetDisposable:Lio/reactivex/disposables/CompositeDisposable;

    .line 75
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->crashnado:Lcom/squareup/crashnado/Crashnado;

    .line 76
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->sampleRateProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p4, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->deviceParams:Lcom/squareup/wavpool/swipe/AndroidDeviceParams;

    .line 78
    iput-object p5, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 79
    iput-object p6, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 80
    iput-object p3, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->audioFilterProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p7, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->bus:Lcom/squareup/badbus/BadEventSink;

    .line 82
    iput-object p8, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->headsetListener:Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    .line 84
    new-instance p1, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;-><init>(Lcom/squareup/wavpool/swipe/MicRecorder;Lcom/squareup/wavpool/swipe/MicRecorder$1;)V

    iput-object p1, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->backgroundWork:Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/wavpool/swipe/MicRecorder;)Lcom/squareup/crashnado/Crashnado;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->crashnado:Lcom/squareup/crashnado/Crashnado;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/squareup/wavpool/swipe/MicRecorder;)Lcom/squareup/badbus/BadEventSink;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->bus:Lcom/squareup/badbus/BadEventSink;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/wavpool/swipe/MicRecorder;)Landroid/telephony/TelephonyManager;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->telephonyManager:Landroid/telephony/TelephonyManager;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/wavpool/swipe/MicRecorder;Lcom/squareup/wavpool/swipe/Recorder$State;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/wavpool/swipe/MicRecorder;->setState(Lcom/squareup/wavpool/swipe/Recorder$State;)V

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/wavpool/swipe/MicRecorder;)Ljavax/inject/Provider;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->sampleRateProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/wavpool/swipe/MicRecorder;)Z
    .locals 0

    .line 25
    iget-boolean p0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->running:Z

    return p0
.end method

.method static synthetic access$600(Lcom/squareup/wavpool/swipe/MicRecorder;)Ljavax/inject/Provider;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->audioFilterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/wavpool/swipe/MicRecorder;)Lcom/squareup/wavpool/swipe/AndroidDeviceParams;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->deviceParams:Lcom/squareup/wavpool/swipe/AndroidDeviceParams;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/wavpool/swipe/MicRecorder;)J
    .locals 2

    .line 25
    iget-wide v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->backgroundRecordingTimestamp:J

    return-wide v0
.end method

.method static synthetic access$802(Lcom/squareup/wavpool/swipe/MicRecorder;J)J
    .locals 0

    .line 25
    iput-wide p1, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->backgroundRecordingTimestamp:J

    return-wide p1
.end method

.method static synthetic access$900(Lcom/squareup/wavpool/swipe/MicRecorder;)Lcom/squareup/thread/executor/MainThread;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-object p0
.end method

.method private setState(Lcom/squareup/wavpool/swipe/Recorder$State;)V
    .locals 2

    .line 134
    sget-object v0, Lcom/squareup/wavpool/swipe/Recorder$State;->BLOCKED:Lcom/squareup/wavpool/swipe/Recorder$State;

    if-ne p1, v0, :cond_1

    .line 135
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->headsetConnectionState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    iget-boolean v0, v0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->connected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->headsetConnectionState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    iget-boolean v0, v0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->hasMicInput:Z

    if-nez v0, :cond_1

    .line 136
    :cond_0
    sget-object p1, Lcom/squareup/wavpool/swipe/Recorder$State;->BLOCKED_NO_READER:Lcom/squareup/wavpool/swipe/Recorder$State;

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->state:Lcom/squareup/wavpool/swipe/Recorder$State;

    if-ne v0, p1, :cond_2

    return-void

    .line 143
    :cond_2
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->state:Lcom/squareup/wavpool/swipe/Recorder$State;

    .line 144
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/wavpool/swipe/-$$Lambda$MicRecorder$Y1wic6hY6HYOcu9HJtoRmpnZtRA;

    invoke-direct {v1, p0, p1}, Lcom/squareup/wavpool/swipe/-$$Lambda$MicRecorder$Y1wic6hY6HYOcu9HJtoRmpnZtRA;-><init>(Lcom/squareup/wavpool/swipe/MicRecorder;Lcom/squareup/wavpool/swipe/Recorder$State;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public getRecordingState()Lcom/squareup/wavpool/swipe/Recorder$State;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    sget-object v0, Lcom/squareup/wavpool/swipe/Recorder$State;->BLOCKED:Lcom/squareup/wavpool/swipe/Recorder$State;

    return-object v0

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->headsetConnectionState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    iget-boolean v0, v0, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->hasMicInput:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/wavpool/swipe/Recorder$State;->READY:Lcom/squareup/wavpool/swipe/Recorder$State;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/squareup/wavpool/swipe/Recorder$State;->MISROUTED_AUDIO:Lcom/squareup/wavpool/swipe/Recorder$State;

    :goto_0
    return-object v0
.end method

.method public synthetic lambda$setState$1$MicRecorder(Lcom/squareup/wavpool/swipe/Recorder$State;)V
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->bus:Lcom/squareup/badbus/BadEventSink;

    invoke-interface {v0, p1}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$start$0$MicRecorder(Lcom/squareup/wavpool/swipe/HeadsetConnectionState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 92
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->headsetConnectionState:Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    return-void
.end method

.method public declared-synchronized start()V
    .locals 4

    monitor-enter p0

    :try_start_0
    const-string v0, "MicRecorder#start()"

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    .line 88
    invoke-static {v0, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    iget-boolean v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->running:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    .line 91
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->headsetDisposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->headsetListener:Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    invoke-interface {v2}, Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;->onHeadsetStateChanged()Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/wavpool/swipe/-$$Lambda$MicRecorder$thIJXZxlATJ_Jr-f65H8nDDg8uQ;

    invoke-direct {v3, p0}, Lcom/squareup/wavpool/swipe/-$$Lambda$MicRecorder$thIJXZxlATJ_Jr-f65H8nDDg8uQ;-><init>(Lcom/squareup/wavpool/swipe/MicRecorder;)V

    .line 92
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    .line 91
    invoke-virtual {v0, v2}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 95
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->recorderThread:Landroid/os/HandlerThread;

    if-nez v0, :cond_1

    .line 98
    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "Sq-MicRecorder"

    const/16 v3, -0x13

    invoke-direct {v0, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->recorderThread:Landroid/os/HandlerThread;

    .line 100
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->recorderThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 101
    new-instance v0, Landroid/os/Handler;

    iget-object v2, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->recorderThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    const-string v2, "Posting work to the recording thread..."

    new-array v3, v1, [Ljava/lang/Object;

    .line 103
    invoke-static {v2, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v2, 0x1

    .line 104
    iput-boolean v2, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->running:Z

    .line 105
    iget-object v2, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->backgroundWork:Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 106
    iget-object v2, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->backgroundWork:Lcom/squareup/wavpool/swipe/MicRecorder$BackgroundWork;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 109
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/MicRecorder;->getRecordingState()Lcom/squareup/wavpool/swipe/Recorder$State;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/wavpool/swipe/MicRecorder;->setState(Lcom/squareup/wavpool/swipe/Recorder$State;)V

    const-string v0, "Recording started."

    new-array v1, v1, [Ljava/lang/Object;

    .line 110
    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    monitor-exit p0

    return-void

    .line 96
    :cond_1
    :try_start_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Improper disposal of recorderThread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public stop()V
    .locals 3

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "MicRecorder#stop()"

    .line 114
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 116
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->headsetDisposable:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v1}, Lio/reactivex/disposables/CompositeDisposable;->dispose()V

    .line 117
    iput-boolean v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->running:Z

    .line 118
    sget-object v0, Lcom/squareup/wavpool/swipe/Recorder$State;->STOPPED:Lcom/squareup/wavpool/swipe/Recorder$State;

    invoke-direct {p0, v0}, Lcom/squareup/wavpool/swipe/MicRecorder;->setState(Lcom/squareup/wavpool/swipe/Recorder$State;)V

    .line 119
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->recorderThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    .line 120
    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    const/4 v0, 0x0

    .line 121
    iput-object v0, p0, Lcom/squareup/wavpool/swipe/MicRecorder;->recorderThread:Landroid/os/HandlerThread;

    :cond_0
    return-void
.end method
