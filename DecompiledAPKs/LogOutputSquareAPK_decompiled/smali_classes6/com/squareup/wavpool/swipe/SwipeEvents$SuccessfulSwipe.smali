.class public Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;
.super Lcom/squareup/wavpool/swipe/SwipeEvents;
.source "SwipeEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/wavpool/swipe/SwipeEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SuccessfulSwipe"
.end annotation


# instance fields
.field public final card:Lcom/squareup/Card;

.field public final cardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field public final hasTrack1Data:Z

.field public final hasTrack2Data:Z


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/Card;ZZ)V
    .locals 1

    const/4 v0, 0x0

    .line 17
    invoke-direct {p0, v0}, Lcom/squareup/wavpool/swipe/SwipeEvents;-><init>(Lcom/squareup/wavpool/swipe/SwipeEvents$1;)V

    .line 18
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    .line 19
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    .line 20
    iput-boolean p3, p0, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->hasTrack1Data:Z

    .line 21
    iput-boolean p4, p0, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->hasTrack2Data:Z

    return-void
.end method
