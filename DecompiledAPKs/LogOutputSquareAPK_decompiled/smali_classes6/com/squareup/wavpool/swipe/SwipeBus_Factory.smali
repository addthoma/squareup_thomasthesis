.class public final Lcom/squareup/wavpool/swipe/SwipeBus_Factory;
.super Ljava/lang/Object;
.source "SwipeBus_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/wavpool/swipe/SwipeBus_Factory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/wavpool/swipe/SwipeBus;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/wavpool/swipe/SwipeBus_Factory;
    .locals 1

    .line 21
    invoke-static {}, Lcom/squareup/wavpool/swipe/SwipeBus_Factory$InstanceHolder;->access$000()Lcom/squareup/wavpool/swipe/SwipeBus_Factory;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance()Lcom/squareup/wavpool/swipe/SwipeBus;
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/wavpool/swipe/SwipeBus;

    invoke-direct {v0}, Lcom/squareup/wavpool/swipe/SwipeBus;-><init>()V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/wavpool/swipe/SwipeBus;
    .locals 1

    .line 17
    invoke-static {}, Lcom/squareup/wavpool/swipe/SwipeBus_Factory;->newInstance()Lcom/squareup/wavpool/swipe/SwipeBus;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/SwipeBus_Factory;->get()Lcom/squareup/wavpool/swipe/SwipeBus;

    move-result-object v0

    return-object v0
.end method
