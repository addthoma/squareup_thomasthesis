.class public final Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioGraphInitiailizerFactory;
.super Ljava/lang/Object;
.source "AndroidAudioPlaybackModule_ProvideAudioGraphInitiailizerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/wavpool/swipe/AudioGraphInitializer;",
        ">;"
    }
.end annotation


# instance fields
.field private final audioStartAndStopperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioStartAndStopper;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioStartAndStopper;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioGraphInitiailizerFactory;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioGraphInitiailizerFactory;->audioStartAndStopperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioGraphInitiailizerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioStartAndStopper;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioGraphInitiailizerFactory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioGraphInitiailizerFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioGraphInitiailizerFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideAudioGraphInitiailizer(Lcom/squareup/cardreader/CardReaderPauseAndResumer;Lcom/squareup/wavpool/swipe/AudioStartAndStopper;)Lcom/squareup/wavpool/swipe/AudioGraphInitializer;
    .locals 0

    .line 43
    invoke-static {p0, p1}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule;->provideAudioGraphInitiailizer(Lcom/squareup/cardreader/CardReaderPauseAndResumer;Lcom/squareup/wavpool/swipe/AudioStartAndStopper;)Lcom/squareup/wavpool/swipe/AudioGraphInitializer;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wavpool/swipe/AudioGraphInitializer;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/wavpool/swipe/AudioGraphInitializer;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioGraphInitiailizerFactory;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioGraphInitiailizerFactory;->audioStartAndStopperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;

    invoke-static {v0, v1}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioGraphInitiailizerFactory;->provideAudioGraphInitiailizer(Lcom/squareup/cardreader/CardReaderPauseAndResumer;Lcom/squareup/wavpool/swipe/AudioStartAndStopper;)Lcom/squareup/wavpool/swipe/AudioGraphInitializer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioGraphInitiailizerFactory;->get()Lcom/squareup/wavpool/swipe/AudioGraphInitializer;

    move-result-object v0

    return-object v0
.end method
