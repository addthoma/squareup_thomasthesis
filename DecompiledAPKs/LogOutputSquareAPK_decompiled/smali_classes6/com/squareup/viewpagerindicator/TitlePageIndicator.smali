.class public Lcom/squareup/viewpagerindicator/TitlePageIndicator;
.super Landroid/view/View;
.source "TitlePageIndicator.java"

# interfaces
.implements Lcom/squareup/viewpagerindicator/PageIndicator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/viewpagerindicator/TitlePageIndicator$SavedState;,
        Lcom/squareup/viewpagerindicator/TitlePageIndicator$IndicatorStyle;,
        Lcom/squareup/viewpagerindicator/TitlePageIndicator$OnCenterItemClickListener;
    }
.end annotation


# static fields
.field private static final BOLD_FADE_PERCENTAGE:F = 0.05f

.field private static final EMPTY_TITLE:Ljava/lang/String; = ""

.field private static final INVALID_POINTER:I = -0x1

.field private static final SELECTION_FADE_PERCENTAGE:F = 0.25f


# instance fields
.field private mActivePointerId:I

.field private mBoldText:Z

.field private mCenterItemClickListener:Lcom/squareup/viewpagerindicator/TitlePageIndicator$OnCenterItemClickListener;

.field private mClipPadding:F

.field private mColorSelected:I

.field private mColorText:I

.field private mCurrentOffset:I

.field private mCurrentPage:I

.field private mFooterIndicatorHeight:F

.field private mFooterIndicatorStyle:Lcom/squareup/viewpagerindicator/TitlePageIndicator$IndicatorStyle;

.field private mFooterIndicatorUnderlinePadding:F

.field private mFooterLineHeight:F

.field private mFooterPadding:F

.field private mIsDragging:Z

.field private mLastMotionX:F

.field private mListener:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

.field private mPagerAdapter:Landroidx/viewpager/widget/PagerAdapter;

.field private final mPaintFooterIndicator:Landroid/graphics/Paint;

.field private final mPaintFooterLine:Landroid/graphics/Paint;

.field private final mPaintText:Landroid/graphics/Paint;

.field private final mPath:Landroid/graphics/Path;

.field private mScrollState:I

.field private mTitlePadding:F

.field private mTopPadding:F

.field private mTouchSlop:I

.field private mViewPager:Landroidx/viewpager/widget/ViewPager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 127
    invoke-direct {p0, p1, v0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 131
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 19

    move-object/from16 v0, p0

    .line 135
    invoke-direct/range {p0 .. p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v1, -0x1

    .line 97
    iput v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    .line 100
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    .line 104
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    iput-object v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    .line 105
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintFooterLine:Landroid/graphics/Paint;

    .line 107
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintFooterIndicator:Landroid/graphics/Paint;

    const/high16 v2, -0x40800000    # -1.0f

    .line 120
    iput v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mLastMotionX:F

    .line 121
    iput v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mActivePointerId:I

    .line 138
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 139
    sget v2, Lcom/squareup/widgets/R$color;->default_title_indicator_footer_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 140
    sget v3, Lcom/squareup/widgets/R$dimen;->default_title_indicator_footer_line_height:I

    .line 141
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 142
    sget v4, Lcom/squareup/widgets/R$integer;->default_title_indicator_footer_indicator_style:I

    .line 143
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 144
    sget v5, Lcom/squareup/widgets/R$dimen;->default_title_indicator_footer_indicator_height:I

    .line 145
    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 146
    sget v6, Lcom/squareup/widgets/R$dimen;->default_title_indicator_footer_indicator_underline_padding:I

    .line 147
    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    .line 148
    sget v7, Lcom/squareup/widgets/R$dimen;->default_title_indicator_footer_padding:I

    .line 149
    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    .line 150
    sget v8, Lcom/squareup/widgets/R$color;->default_title_indicator_selected_color:I

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 151
    sget v9, Lcom/squareup/widgets/R$bool;->default_title_indicator_selected_bold:I

    .line 152
    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v9

    .line 153
    sget v10, Lcom/squareup/widgets/R$color;->default_title_indicator_text_color:I

    invoke-virtual {v1, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    .line 154
    sget v11, Lcom/squareup/widgets/R$dimen;->default_title_indicator_text_size:I

    invoke-virtual {v1, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    .line 155
    sget v12, Lcom/squareup/widgets/R$dimen;->default_title_indicator_title_padding:I

    .line 156
    invoke-virtual {v1, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    .line 157
    sget v13, Lcom/squareup/widgets/R$dimen;->default_title_indicator_clip_padding:I

    invoke-virtual {v1, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v13

    .line 158
    sget v14, Lcom/squareup/widgets/R$dimen;->default_title_indicator_top_padding:I

    invoke-virtual {v1, v14}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 161
    sget-object v14, Lcom/squareup/widgets/R$styleable;->TitlePageIndicator:[I

    const/4 v15, 0x0

    move/from16 v16, v2

    move/from16 v18, v9

    move/from16 v17, v11

    move-object/from16 v2, p1

    move-object/from16 v11, p2

    move/from16 v9, p3

    .line 162
    invoke-virtual {v2, v11, v14, v9, v15}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v9

    .line 165
    sget v11, Lcom/squareup/widgets/R$styleable;->TitlePageIndicator_footerLineHeight:I

    .line 166
    invoke-virtual {v9, v11, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    .line 167
    sget v3, Lcom/squareup/widgets/R$styleable;->TitlePageIndicator_footerIndicatorStyle:I

    .line 168
    invoke-virtual {v9, v3, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    .line 167
    invoke-static {v3}, Lcom/squareup/viewpagerindicator/TitlePageIndicator$IndicatorStyle;->fromValue(I)Lcom/squareup/viewpagerindicator/TitlePageIndicator$IndicatorStyle;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorStyle:Lcom/squareup/viewpagerindicator/TitlePageIndicator$IndicatorStyle;

    .line 170
    sget v3, Lcom/squareup/widgets/R$styleable;->TitlePageIndicator_footerIndicatorHeight:I

    invoke-virtual {v9, v3, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorHeight:F

    .line 172
    sget v3, Lcom/squareup/widgets/R$styleable;->TitlePageIndicator_footerIndicatorUnderlinePadding:I

    .line 173
    invoke-virtual {v9, v3, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorUnderlinePadding:F

    .line 175
    sget v3, Lcom/squareup/widgets/R$styleable;->TitlePageIndicator_footerPadding:I

    .line 176
    invoke-virtual {v9, v3, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterPadding:F

    .line 177
    sget v3, Lcom/squareup/widgets/R$styleable;->TitlePageIndicator_topPadding:I

    invoke-virtual {v9, v3, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mTopPadding:F

    .line 178
    sget v1, Lcom/squareup/widgets/R$styleable;->TitlePageIndicator_titlePadding:I

    .line 179
    invoke-virtual {v9, v1, v12}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mTitlePadding:F

    .line 180
    sget v1, Lcom/squareup/widgets/R$styleable;->TitlePageIndicator_clipPadding:I

    invoke-virtual {v9, v1, v13}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mClipPadding:F

    .line 181
    sget v1, Lcom/squareup/widgets/R$styleable;->TitlePageIndicator_selectedColor:I

    invoke-virtual {v9, v1, v8}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mColorSelected:I

    .line 182
    sget v1, Lcom/squareup/widgets/R$styleable;->TitlePageIndicator_android_textColor:I

    invoke-virtual {v9, v1, v10}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mColorText:I

    .line 183
    sget v1, Lcom/squareup/widgets/R$styleable;->TitlePageIndicator_selectedBold:I

    move/from16 v3, v18

    invoke-virtual {v9, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mBoldText:Z

    .line 185
    sget v1, Lcom/squareup/widgets/R$styleable;->TitlePageIndicator_android_textSize:I

    move/from16 v3, v17

    .line 186
    invoke-virtual {v9, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    .line 187
    sget v3, Lcom/squareup/widgets/R$styleable;->TitlePageIndicator_footerColor:I

    move/from16 v4, v16

    .line 188
    invoke-virtual {v9, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 189
    iget-object v4, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    invoke-virtual {v4, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 190
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 191
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintFooterLine:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 192
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintFooterLine:Landroid/graphics/Paint;

    iget v4, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 193
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintFooterLine:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 194
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintFooterIndicator:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 195
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintFooterIndicator:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 197
    invoke-virtual {v9}, Landroid/content/res/TypedArray;->recycle()V

    .line 199
    invoke-static/range {p1 .. p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 200
    invoke-static {v1}, Landroidx/core/view/ViewConfigurationCompat;->getScaledPagingTouchSlop(Landroid/view/ViewConfiguration;)I

    move-result v1

    iput v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mTouchSlop:I

    return-void
.end method

.method private calcBounds(ILandroid/graphics/Paint;)Landroid/graphics/RectF;
    .locals 1

    .line 641
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 642
    invoke-direct {p0, p1}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->getTitle(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result p1

    iput p1, v0, Landroid/graphics/RectF;->right:F

    .line 643
    invoke-virtual {p2}, Landroid/graphics/Paint;->descent()F

    move-result p1

    invoke-virtual {p2}, Landroid/graphics/Paint;->ascent()F

    move-result p2

    sub-float/2addr p1, p2

    iput p1, v0, Landroid/graphics/RectF;->bottom:F

    return-object v0
.end method

.method private calculateAllBounds(Landroid/graphics/Paint;)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Paint;",
            ")",
            "Ljava/util/ArrayList<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation

    .line 619
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 621
    iget-object v1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/viewpager/widget/PagerAdapter;->getCount()I

    move-result v1

    .line 622
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->getWidth()I

    move-result v2

    .line 623
    div-int/lit8 v3, v2, 0x2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_0

    .line 625
    invoke-direct {p0, v4, p1}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->calcBounds(ILandroid/graphics/Paint;)Landroid/graphics/RectF;

    move-result-object v5

    .line 626
    iget v6, v5, Landroid/graphics/RectF;->right:F

    iget v7, v5, Landroid/graphics/RectF;->left:F

    sub-float/2addr v6, v7

    .line 627
    iget v7, v5, Landroid/graphics/RectF;->bottom:F

    iget v8, v5, Landroid/graphics/RectF;->top:F

    sub-float/2addr v7, v8

    int-to-float v8, v3

    const/high16 v9, 0x40000000    # 2.0f

    div-float v9, v6, v9

    sub-float/2addr v8, v9

    .line 628
    iget v9, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentOffset:I

    int-to-float v9, v9

    sub-float/2addr v8, v9

    iget v9, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    sub-int v9, v4, v9

    mul-int v9, v9, v2

    int-to-float v9, v9

    add-float/2addr v8, v9

    iput v8, v5, Landroid/graphics/RectF;->left:F

    .line 629
    iget v8, v5, Landroid/graphics/RectF;->left:F

    add-float/2addr v8, v6

    iput v8, v5, Landroid/graphics/RectF;->right:F

    const/4 v6, 0x0

    .line 630
    iput v6, v5, Landroid/graphics/RectF;->top:F

    .line 631
    iput v7, v5, Landroid/graphics/RectF;->bottom:F

    .line 632
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private clipViewOnTheLeft(Landroid/graphics/RectF;FI)V
    .locals 1

    int-to-float p3, p3

    .line 613
    iget v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mClipPadding:F

    add-float/2addr p3, v0

    iput p3, p1, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, p2

    .line 614
    iput v0, p1, Landroid/graphics/RectF;->right:F

    return-void
.end method

.method private clipViewOnTheRight(Landroid/graphics/RectF;FI)V
    .locals 1

    int-to-float p3, p3

    .line 602
    iget v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mClipPadding:F

    sub-float/2addr p3, v0

    iput p3, p1, Landroid/graphics/RectF;->right:F

    .line 603
    iget p3, p1, Landroid/graphics/RectF;->right:F

    sub-float/2addr p3, p2

    iput p3, p1, Landroid/graphics/RectF;->left:F

    return-void
.end method

.method private getTitle(I)Ljava/lang/String;
    .locals 1

    .line 797
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPagerAdapter:Landroidx/viewpager/widget/PagerAdapter;

    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/PagerAdapter;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object p1

    if-nez p1, :cond_0

    const-string p1, ""

    .line 801
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public getClipPadding()F
    .locals 1

    .line 306
    iget v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mClipPadding:F

    return v0
.end method

.method public getFooterColor()I
    .locals 1

    .line 204
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintFooterLine:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method public getFooterIndicatorHeight()F
    .locals 1

    .line 224
    iget v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorHeight:F

    return v0
.end method

.method public getFooterIndicatorPadding()F
    .locals 1

    .line 233
    iget v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterPadding:F

    return v0
.end method

.method public getFooterIndicatorStyle()Lcom/squareup/viewpagerindicator/TitlePageIndicator$IndicatorStyle;
    .locals 1

    .line 242
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorStyle:Lcom/squareup/viewpagerindicator/TitlePageIndicator$IndicatorStyle;

    return-object v0
.end method

.method public getFooterLineHeight()F
    .locals 1

    .line 214
    iget v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    return v0
.end method

.method public getSelectedColor()I
    .locals 1

    .line 251
    iget v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mColorSelected:I

    return v0
.end method

.method public getTextColor()I
    .locals 1

    .line 269
    iget v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mColorText:I

    return v0
.end method

.method public getTextSize()F
    .locals 1

    .line 279
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getTextSize()F

    move-result v0

    return v0
.end method

.method public getTitlePadding()F
    .locals 1

    .line 288
    iget v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mTitlePadding:F

    return v0
.end method

.method public getTopPadding()F
    .locals 1

    .line 297
    iget v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mTopPadding:F

    return v0
.end method

.method public getTypeface()Landroid/graphics/Typeface;
    .locals 1

    .line 320
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method

.method public isSelectedBold()Z
    .locals 1

    .line 260
    iget-boolean v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mBoldText:Z

    return v0
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .line 666
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->invalidate()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 25

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 330
    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 332
    iget-object v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    if-nez v2, :cond_0

    return-void

    .line 335
    :cond_0
    invoke-virtual {v2}, Landroidx/viewpager/widget/ViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroidx/viewpager/widget/PagerAdapter;->getCount()I

    move-result v2

    if-nez v2, :cond_1

    return-void

    .line 342
    :cond_1
    iget v3, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    iget-object v3, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v3}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v3

    iput v3, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    .line 345
    :cond_2
    iget-object v3, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->calculateAllBounds(Landroid/graphics/Paint;)Ljava/util/ArrayList;

    move-result-object v3

    .line 346
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 349
    iget v5, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    const/4 v6, 0x1

    if-lt v5, v4, :cond_3

    sub-int/2addr v4, v6

    .line 350
    invoke-virtual {v0, v4}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->setCurrentItem(I)V

    return-void

    :cond_3
    add-int/lit8 v5, v2, -0x1

    .line 355
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->getWidth()I

    move-result v7

    int-to-float v7, v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    .line 356
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->getLeft()I

    move-result v9

    int-to-float v10, v9

    .line 357
    iget v11, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mClipPadding:F

    add-float/2addr v11, v10

    .line 358
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->getWidth()I

    move-result v12

    .line 359
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->getHeight()I

    move-result v13

    add-int v14, v9, v12

    int-to-float v15, v14

    .line 361
    iget v8, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mClipPadding:F

    sub-float v8, v15, v8

    .line 363
    iget v6, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    move/from16 v16, v4

    .line 365
    iget v4, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentOffset:I

    move/from16 v17, v13

    int-to-float v13, v4

    const/high16 v18, 0x3f800000    # 1.0f

    cmpg-float v13, v13, v7

    if-gtz v13, :cond_4

    goto :goto_0

    :cond_4
    add-int/lit8 v6, v6, 0x1

    sub-int v4, v12, v4

    :goto_0
    int-to-float v4, v4

    mul-float v4, v4, v18

    int-to-float v13, v12

    div-float/2addr v4, v13

    const/high16 v13, 0x3e800000    # 0.25f

    const/16 v18, 0x0

    cmpg-float v19, v4, v13

    if-gtz v19, :cond_5

    const/16 v19, 0x1

    goto :goto_1

    :cond_5
    const/16 v19, 0x0

    :goto_1
    const v20, 0x3d4ccccd    # 0.05f

    cmpg-float v20, v4, v20

    if-gtz v20, :cond_6

    const/16 v20, 0x1

    goto :goto_2

    :cond_6
    const/16 v20, 0x0

    :goto_2
    sub-float v4, v13, v4

    div-float/2addr v4, v13

    .line 377
    iget v13, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    invoke-virtual {v3, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/graphics/RectF;

    move/from16 v21, v7

    .line 378
    iget v7, v13, Landroid/graphics/RectF;->right:F

    move/from16 v22, v12

    iget v12, v13, Landroid/graphics/RectF;->left:F

    sub-float/2addr v7, v12

    .line 379
    iget v12, v13, Landroid/graphics/RectF;->left:F

    cmpg-float v12, v12, v11

    if-gez v12, :cond_7

    .line 381
    invoke-direct {v0, v13, v7, v9}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->clipViewOnTheLeft(Landroid/graphics/RectF;FI)V

    .line 383
    :cond_7
    iget v12, v13, Landroid/graphics/RectF;->right:F

    cmpl-float v12, v12, v8

    if-lez v12, :cond_8

    .line 385
    invoke-direct {v0, v13, v7, v14}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->clipViewOnTheRight(Landroid/graphics/RectF;FI)V

    .line 389
    :cond_8
    iget v7, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    if-lez v7, :cond_b

    const/4 v12, 0x1

    sub-int/2addr v7, v12

    :goto_3
    if-ltz v7, :cond_b

    .line 391
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/graphics/RectF;

    .line 393
    iget v13, v12, Landroid/graphics/RectF;->left:F

    cmpg-float v13, v13, v11

    if-gez v13, :cond_9

    .line 394
    iget v13, v12, Landroid/graphics/RectF;->right:F

    move/from16 v23, v11

    iget v11, v12, Landroid/graphics/RectF;->left:F

    sub-float/2addr v13, v11

    .line 396
    invoke-direct {v0, v12, v13, v9}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->clipViewOnTheLeft(Landroid/graphics/RectF;FI)V

    add-int/lit8 v11, v7, 0x1

    .line 398
    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/graphics/RectF;

    move/from16 v24, v9

    .line 400
    iget v9, v12, Landroid/graphics/RectF;->right:F

    iget v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mTitlePadding:F

    add-float/2addr v9, v1

    iget v1, v11, Landroid/graphics/RectF;->left:F

    cmpl-float v1, v9, v1

    if-lez v1, :cond_a

    .line 401
    iget v1, v11, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v13

    iget v9, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mTitlePadding:F

    sub-float/2addr v1, v9

    iput v1, v12, Landroid/graphics/RectF;->left:F

    .line 402
    iget v1, v12, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v13

    iput v1, v12, Landroid/graphics/RectF;->right:F

    goto :goto_4

    :cond_9
    move/from16 v24, v9

    move/from16 v23, v11

    :cond_a
    :goto_4
    add-int/lit8 v7, v7, -0x1

    move-object/from16 v1, p1

    move/from16 v11, v23

    move/from16 v9, v24

    goto :goto_3

    .line 408
    :cond_b
    iget v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    if-ge v1, v5, :cond_d

    const/4 v5, 0x1

    add-int/2addr v1, v5

    :goto_5
    if-ge v1, v2, :cond_d

    .line 410
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/RectF;

    .line 412
    iget v7, v5, Landroid/graphics/RectF;->right:F

    cmpl-float v7, v7, v8

    if-lez v7, :cond_c

    .line 413
    iget v7, v5, Landroid/graphics/RectF;->right:F

    iget v9, v5, Landroid/graphics/RectF;->left:F

    sub-float/2addr v7, v9

    .line 415
    invoke-direct {v0, v5, v7, v14}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->clipViewOnTheRight(Landroid/graphics/RectF;FI)V

    add-int/lit8 v9, v1, -0x1

    .line 417
    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/RectF;

    .line 419
    iget v11, v5, Landroid/graphics/RectF;->left:F

    iget v12, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mTitlePadding:F

    sub-float/2addr v11, v12

    iget v12, v9, Landroid/graphics/RectF;->right:F

    cmpg-float v11, v11, v12

    if-gez v11, :cond_c

    .line 420
    iget v9, v9, Landroid/graphics/RectF;->right:F

    iget v11, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mTitlePadding:F

    add-float/2addr v9, v11

    iput v9, v5, Landroid/graphics/RectF;->left:F

    .line 421
    iget v9, v5, Landroid/graphics/RectF;->left:F

    add-float/2addr v9, v7

    iput v9, v5, Landroid/graphics/RectF;->right:F

    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 428
    :cond_d
    iget v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mColorText:I

    ushr-int/lit8 v1, v1, 0x18

    const/4 v5, 0x0

    :goto_6
    if-ge v5, v2, :cond_15

    .line 431
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/RectF;

    .line 433
    iget v8, v7, Landroid/graphics/RectF;->left:F

    cmpl-float v8, v8, v10

    if-lez v8, :cond_e

    iget v8, v7, Landroid/graphics/RectF;->left:F

    cmpg-float v8, v8, v15

    if-ltz v8, :cond_f

    :cond_e
    iget v8, v7, Landroid/graphics/RectF;->right:F

    cmpl-float v8, v8, v10

    if-lez v8, :cond_13

    iget v8, v7, Landroid/graphics/RectF;->right:F

    cmpg-float v8, v8, v15

    if-gez v8, :cond_13

    :cond_f
    if-ne v5, v6, :cond_10

    const/4 v8, 0x1

    goto :goto_7

    :cond_10
    const/4 v8, 0x0

    .line 436
    :goto_7
    invoke-direct {v0, v5}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->getTitle(I)Ljava/lang/String;

    move-result-object v9

    .line 439
    iget-object v11, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    if-eqz v8, :cond_11

    if-eqz v20, :cond_11

    iget-boolean v12, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mBoldText:Z

    if-eqz v12, :cond_11

    const/4 v12, 0x1

    goto :goto_8

    :cond_11
    const/4 v12, 0x0

    :goto_8
    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 442
    iget-object v11, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    iget v12, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mColorText:I

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setColor(I)V

    if-eqz v8, :cond_12

    if-eqz v19, :cond_12

    .line 445
    iget-object v11, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    int-to-float v12, v1

    mul-float v12, v12, v4

    float-to-int v12, v12

    sub-int v12, v1, v12

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 447
    :cond_12
    iget v11, v7, Landroid/graphics/RectF;->left:F

    iget v12, v7, Landroid/graphics/RectF;->bottom:F

    iget v13, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mTopPadding:F

    add-float/2addr v12, v13

    iget-object v13, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    move-object/from16 v14, p1

    invoke-virtual {v14, v9, v11, v12, v13}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    if-eqz v8, :cond_14

    if-eqz v19, :cond_14

    .line 451
    iget-object v8, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    iget v11, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mColorSelected:I

    invoke-virtual {v8, v11}, Landroid/graphics/Paint;->setColor(I)V

    .line 452
    iget-object v8, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    iget v11, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mColorSelected:I

    ushr-int/lit8 v11, v11, 0x18

    int-to-float v11, v11

    mul-float v11, v11, v4

    float-to-int v11, v11

    invoke-virtual {v8, v11}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 453
    iget v8, v7, Landroid/graphics/RectF;->left:F

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    iget v11, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mTopPadding:F

    add-float/2addr v7, v11

    iget-object v11, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    invoke-virtual {v14, v9, v8, v7, v11}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_9

    :cond_13
    move-object/from16 v14, p1

    :cond_14
    :goto_9
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_6

    :cond_15
    move-object/from16 v14, p1

    .line 459
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 460
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    const/4 v2, 0x0

    move/from16 v5, v17

    int-to-float v5, v5

    iget v7, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    sub-float v7, v5, v7

    invoke-virtual {v1, v2, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 461
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    move/from16 v2, v22

    int-to-float v2, v2

    iget v7, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    div-float/2addr v7, v8

    sub-float v7, v5, v7

    invoke-virtual {v1, v2, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 462
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 463
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    iget-object v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintFooterLine:Landroid/graphics/Paint;

    invoke-virtual {v14, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 465
    sget-object v1, Lcom/squareup/viewpagerindicator/TitlePageIndicator$1;->$SwitchMap$com$squareup$viewpagerindicator$TitlePageIndicator$IndicatorStyle:[I

    iget-object v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorStyle:Lcom/squareup/viewpagerindicator/TitlePageIndicator$IndicatorStyle;

    invoke-virtual {v2}, Lcom/squareup/viewpagerindicator/TitlePageIndicator$IndicatorStyle;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_18

    const/4 v2, 0x2

    if-eq v1, v2, :cond_16

    goto/16 :goto_a

    :cond_16
    if-eqz v19, :cond_19

    move/from16 v1, v16

    if-lt v6, v1, :cond_17

    goto/16 :goto_a

    .line 480
    :cond_17
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    .line 481
    iget-object v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 482
    iget-object v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    iget v3, v1, Landroid/graphics/RectF;->left:F

    iget v6, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorUnderlinePadding:F

    sub-float/2addr v3, v6

    iget v6, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    sub-float v6, v5, v6

    invoke-virtual {v2, v3, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 484
    iget-object v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    iget v3, v1, Landroid/graphics/RectF;->right:F

    iget v6, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorUnderlinePadding:F

    add-float/2addr v3, v6

    iget v6, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    sub-float v6, v5, v6

    invoke-virtual {v2, v3, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 486
    iget-object v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    iget v3, v1, Landroid/graphics/RectF;->right:F

    iget v6, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorUnderlinePadding:F

    add-float/2addr v3, v6

    iget v6, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    sub-float v6, v5, v6

    iget v7, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorHeight:F

    sub-float/2addr v6, v7

    invoke-virtual {v2, v3, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 488
    iget-object v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget v3, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorUnderlinePadding:F

    sub-float/2addr v1, v3

    iget v3, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    sub-float/2addr v5, v3

    iget v3, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorHeight:F

    sub-float/2addr v5, v3

    invoke-virtual {v2, v1, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 490
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 492
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintFooterIndicator:Landroid/graphics/Paint;

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float v4, v4, v2

    float-to-int v2, v4

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 493
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    iget-object v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintFooterIndicator:Landroid/graphics/Paint;

    invoke-virtual {v14, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 494
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintFooterIndicator:Landroid/graphics/Paint;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    goto :goto_a

    .line 467
    :cond_18
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 468
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    iget v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    sub-float v2, v5, v2

    iget v3, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorHeight:F

    sub-float/2addr v2, v3

    move/from16 v7, v21

    invoke-virtual {v1, v7, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 469
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    iget v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorHeight:F

    add-float/2addr v2, v7

    iget v3, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    sub-float v3, v5, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 470
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    iget v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorHeight:F

    sub-float/2addr v7, v2

    iget v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    sub-float/2addr v5, v2

    invoke-virtual {v1, v7, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 471
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 472
    iget-object v1, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPath:Landroid/graphics/Path;

    iget-object v2, v0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintFooterIndicator:Landroid/graphics/Paint;

    invoke-virtual {v14, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_19
    :goto_a
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .line 728
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    .line 732
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    if-ne v0, v1, :cond_0

    .line 735
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    int-to-float p2, p2

    goto :goto_0

    .line 738
    :cond_0
    new-instance p2, Landroid/graphics/RectF;

    invoke-direct {p2}, Landroid/graphics/RectF;-><init>()V

    .line 739
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->descent()F

    move-result v0

    iget-object v1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->ascent()F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p2, Landroid/graphics/RectF;->bottom:F

    .line 740
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget p2, p2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, p2

    iget p2, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    add-float/2addr v0, p2

    iget p2, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterPadding:F

    add-float/2addr v0, p2

    iget p2, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mTopPadding:F

    add-float/2addr p2, v0

    .line 741
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorStyle:Lcom/squareup/viewpagerindicator/TitlePageIndicator$IndicatorStyle;

    sget-object v1, Lcom/squareup/viewpagerindicator/TitlePageIndicator$IndicatorStyle;->None:Lcom/squareup/viewpagerindicator/TitlePageIndicator$IndicatorStyle;

    if-eq v0, v1, :cond_1

    .line 742
    iget v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorHeight:F

    add-float/2addr p2, v0

    :cond_1
    :goto_0
    float-to-int p2, p2

    .line 747
    invoke-virtual {p0, p1, p2}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->setMeasuredDimension(II)V

    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 1

    .line 690
    iput p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mScrollState:I

    .line 692
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mListener:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 693
    invoke-interface {v0, p1}, Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    :cond_0
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 1

    .line 699
    iput p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    .line 700
    iput p3, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentOffset:I

    .line 701
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 703
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mListener:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 704
    invoke-interface {v0, p1, p2, p3}, Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    :cond_0
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1

    .line 710
    iget v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mScrollState:I

    if-nez v0, :cond_0

    .line 711
    iput p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    .line 712
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->invalidate()V

    .line 715
    :cond_0
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mListener:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_1

    .line 716
    invoke-interface {v0, p1}, Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    :cond_1
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 752
    check-cast p1, Lcom/squareup/viewpagerindicator/TitlePageIndicator$SavedState;

    .line 753
    invoke-virtual {p1}, Lcom/squareup/viewpagerindicator/TitlePageIndicator$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 754
    iget p1, p1, Lcom/squareup/viewpagerindicator/TitlePageIndicator$SavedState;->currentPage:I

    iput p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    .line 755
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->requestLayout()V

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 760
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 761
    new-instance v1, Lcom/squareup/viewpagerindicator/TitlePageIndicator$SavedState;

    invoke-direct {v1, v0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 762
    iget v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    iput v0, v1, Lcom/squareup/viewpagerindicator/TitlePageIndicator$SavedState;->currentPage:I

    return-object v1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .line 500
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 503
    :cond_0
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    const/4 v2, 0x0

    if-eqz v0, :cond_f

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/viewpager/widget/PagerAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    goto/16 :goto_1

    .line 507
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    if-eqz v0, :cond_d

    if-eq v0, v1, :cond_9

    const/4 v3, 0x2

    if-eq v0, v3, :cond_6

    const/4 v3, 0x3

    if-eq v0, v3, :cond_9

    const/4 v3, 0x5

    if-eq v0, v3, :cond_5

    const/4 v3, 0x6

    if-eq v0, v3, :cond_2

    goto/16 :goto_0

    .line 581
    :cond_2
    invoke-static {p1}, Landroidx/core/view/MotionEventCompat;->getActionIndex(Landroid/view/MotionEvent;)I

    move-result v0

    .line 582
    invoke-static {p1, v0}, Landroidx/core/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v3

    .line 583
    iget v4, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mActivePointerId:I

    if-ne v3, v4, :cond_4

    if-nez v0, :cond_3

    const/4 v2, 0x1

    .line 585
    :cond_3
    invoke-static {p1, v2}, Landroidx/core/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mActivePointerId:I

    .line 587
    :cond_4
    iget v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mActivePointerId:I

    .line 588
    invoke-static {p1, v0}, Landroidx/core/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v0

    invoke-static {p1, v0}, Landroidx/core/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result p1

    iput p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mLastMotionX:F

    goto/16 :goto_0

    .line 574
    :cond_5
    invoke-static {p1}, Landroidx/core/view/MotionEventCompat;->getActionIndex(Landroid/view/MotionEvent;)I

    move-result v0

    .line 575
    invoke-static {p1, v0}, Landroidx/core/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v2

    iput v2, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mLastMotionX:F

    .line 576
    invoke-static {p1, v0}, Landroidx/core/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result p1

    iput p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mActivePointerId:I

    goto/16 :goto_0

    .line 516
    :cond_6
    iget v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mActivePointerId:I

    invoke-static {p1, v0}, Landroidx/core/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 517
    invoke-static {p1, v0}, Landroidx/core/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result p1

    .line 518
    iget v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mLastMotionX:F

    sub-float v0, p1, v0

    .line 520
    iget-boolean v2, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mIsDragging:Z

    if-nez v2, :cond_7

    .line 521
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mTouchSlop:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_7

    .line 522
    iput-boolean v1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mIsDragging:Z

    .line 526
    :cond_7
    iget-boolean v2, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mIsDragging:Z

    if-eqz v2, :cond_e

    .line 527
    iget-object v2, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v2}, Landroidx/viewpager/widget/ViewPager;->isFakeDragging()Z

    move-result v2

    if-nez v2, :cond_8

    .line 528
    iget-object v2, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v2}, Landroidx/viewpager/widget/ViewPager;->beginFakeDrag()Z

    .line 531
    :cond_8
    iput p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mLastMotionX:F

    .line 533
    iget-object p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {p1, v0}, Landroidx/viewpager/widget/ViewPager;->fakeDragBy(F)V

    goto :goto_0

    .line 541
    :cond_9
    iget-boolean v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mIsDragging:Z

    if-nez v0, :cond_c

    .line 542
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/viewpager/widget/PagerAdapter;->getCount()I

    move-result v0

    .line 543
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v3, v4

    const/high16 v5, 0x40c00000    # 6.0f

    div-float/2addr v3, v5

    sub-float v5, v4, v3

    add-float/2addr v4, v3

    .line 548
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    cmpg-float v3, p1, v5

    if-gez v3, :cond_a

    .line 551
    iget p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    if-lez p1, :cond_c

    .line 552
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    sub-int/2addr p1, v1

    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    return v1

    :cond_a
    cmpl-float p1, p1, v4

    if-lez p1, :cond_b

    .line 556
    iget p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    sub-int/2addr v0, v1

    if-ge p1, v0, :cond_c

    .line 557
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    add-int/2addr p1, v1

    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    return v1

    .line 562
    :cond_b
    iget-object p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCenterItemClickListener:Lcom/squareup/viewpagerindicator/TitlePageIndicator$OnCenterItemClickListener;

    if-eqz p1, :cond_c

    .line 563
    iget v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    invoke-interface {p1, v0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator$OnCenterItemClickListener;->onCenterItemClick(I)V

    .line 568
    :cond_c
    iput-boolean v2, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mIsDragging:Z

    const/4 p1, -0x1

    .line 569
    iput p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mActivePointerId:I

    .line 570
    iget-object p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->isFakeDragging()Z

    move-result p1

    if-eqz p1, :cond_e

    iget-object p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->endFakeDrag()V

    goto :goto_0

    .line 511
    :cond_d
    invoke-static {p1, v2}, Landroidx/core/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mActivePointerId:I

    .line 512
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    iput p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mLastMotionX:F

    :cond_e
    :goto_0
    return v1

    :cond_f
    :goto_1
    return v2
.end method

.method public setClipPadding(F)V
    .locals 0

    .line 310
    iput p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mClipPadding:F

    .line 311
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->invalidate()V

    return-void
.end method

.method public setCurrentItem(I)V
    .locals 1

    .line 680
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    if-eqz v0, :cond_0

    .line 683
    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    .line 684
    iput p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCurrentPage:I

    .line 685
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->invalidate()V

    return-void

    .line 681
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "ViewPager has not been bound."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setFooterColor(I)V
    .locals 1

    .line 208
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintFooterLine:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 209
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintFooterIndicator:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 210
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->invalidate()V

    return-void
.end method

.method public setFooterIndicatorHeight(F)V
    .locals 0

    .line 228
    iput p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorHeight:F

    .line 229
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->invalidate()V

    return-void
.end method

.method public setFooterIndicatorPadding(F)V
    .locals 0

    .line 237
    iput p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterPadding:F

    .line 238
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->invalidate()V

    return-void
.end method

.method public setFooterIndicatorStyle(Lcom/squareup/viewpagerindicator/TitlePageIndicator$IndicatorStyle;)V
    .locals 0

    .line 246
    iput-object p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterIndicatorStyle:Lcom/squareup/viewpagerindicator/TitlePageIndicator$IndicatorStyle;

    .line 247
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->invalidate()V

    return-void
.end method

.method public setFooterLineHeight(F)V
    .locals 1

    .line 218
    iput p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    .line 219
    iget-object p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintFooterLine:Landroid/graphics/Paint;

    iget v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mFooterLineHeight:F

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 220
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->invalidate()V

    return-void
.end method

.method public setOnCenterItemClickListener(Lcom/squareup/viewpagerindicator/TitlePageIndicator$OnCenterItemClickListener;)V
    .locals 0

    .line 675
    iput-object p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mCenterItemClickListener:Lcom/squareup/viewpagerindicator/TitlePageIndicator$OnCenterItemClickListener;

    return-void
.end method

.method public setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V
    .locals 0

    .line 722
    iput-object p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mListener:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    return-void
.end method

.method public setSelectedBold(Z)V
    .locals 0

    .line 264
    iput-boolean p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mBoldText:Z

    .line 265
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->invalidate()V

    return-void
.end method

.method public setSelectedColor(I)V
    .locals 0

    .line 255
    iput p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mColorSelected:I

    .line 256
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->invalidate()V

    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    .line 273
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 274
    iput p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mColorText:I

    .line 275
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->invalidate()V

    return-void
.end method

.method public setTextSize(F)V
    .locals 1

    .line 283
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 284
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->invalidate()V

    return-void
.end method

.method public setTitlePadding(F)V
    .locals 0

    .line 292
    iput p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mTitlePadding:F

    .line 293
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->invalidate()V

    return-void
.end method

.method public setTopPadding(F)V
    .locals 0

    .line 301
    iput p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mTopPadding:F

    .line 302
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->invalidate()V

    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .locals 1

    .line 315
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPaintText:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 316
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->invalidate()V

    return-void
.end method

.method public setViewPager(Landroidx/viewpager/widget/ViewPager;)V
    .locals 1

    .line 649
    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPagerAdapter:Landroidx/viewpager/widget/PagerAdapter;

    .line 650
    iget-object v0, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mPagerAdapter:Landroidx/viewpager/widget/PagerAdapter;

    if-eqz v0, :cond_0

    .line 653
    iput-object p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    .line 654
    iget-object p1, p0, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->mViewPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {p1, p0}, Landroidx/viewpager/widget/ViewPager;->setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 655
    invoke-virtual {p0}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->invalidate()V

    return-void

    .line 651
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "ViewPager does not have adapter instance."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setViewPager(Landroidx/viewpager/widget/ViewPager;I)V
    .locals 0

    .line 660
    invoke-virtual {p0, p1}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->setViewPager(Landroidx/viewpager/widget/ViewPager;)V

    .line 661
    invoke-virtual {p0, p2}, Lcom/squareup/viewpagerindicator/TitlePageIndicator;->setCurrentItem(I)V

    return-void
.end method
