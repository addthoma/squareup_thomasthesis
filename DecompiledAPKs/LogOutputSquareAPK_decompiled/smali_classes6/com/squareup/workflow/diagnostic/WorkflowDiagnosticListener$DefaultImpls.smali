.class public final Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener$DefaultImpls;
.super Ljava/lang/Object;
.source "WorkflowDiagnosticListener.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static onAfterRenderPass(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public static onAfterSnapshotPass(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V
    .locals 0

    return-void
.end method

.method public static onAfterWorkflowRendered(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;JLjava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public static onBeforeRenderPass(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public static onBeforeSnapshotPass(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V
    .locals 0

    return-void
.end method

.method public static onBeforeWorkflowRendered(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;JLjava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public static onPropsChanged(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Ljava/lang/Long;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public static onRuntimeStarted(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lkotlinx/coroutines/CoroutineScope;Ljava/lang/String;)V
    .locals 0

    const-string p0, "workflowScope"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "rootWorkflowType"

    invoke-static {p2, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static onRuntimeStopped(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V
    .locals 0

    return-void
.end method

.method public static onSinkReceived(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;JLcom/squareup/workflow/WorkflowAction;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
            "J",
            "Lcom/squareup/workflow/WorkflowAction<",
            "**>;)V"
        }
    .end annotation

    const-string p0, "action"

    invoke-static {p3, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static onWorkerOutput(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;JJLjava/lang/Object;)V
    .locals 0

    const-string p0, "output"

    invoke-static {p5, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static onWorkerStarted(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;JJLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    const-string p0, "key"

    invoke-static {p5, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "description"

    invoke-static {p6, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static onWorkerStopped(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;JJ)V
    .locals 0

    return-void
.end method

.method public static onWorkflowAction(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;JLcom/squareup/workflow/WorkflowAction;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
            "J",
            "Lcom/squareup/workflow/WorkflowAction<",
            "**>;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    const-string p0, "action"

    invoke-static {p3, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static onWorkflowStarted(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;JLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 0

    const-string p0, "workflowType"

    invoke-static {p4, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "key"

    invoke-static {p5, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static onWorkflowStopped(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;J)V
    .locals 0

    return-void
.end method
