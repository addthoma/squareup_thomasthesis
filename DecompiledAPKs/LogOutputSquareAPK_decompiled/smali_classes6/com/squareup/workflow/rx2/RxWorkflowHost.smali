.class public interface abstract Lcom/squareup/workflow/rx2/RxWorkflowHost;
.super Ljava/lang/Object;
.source "RxWorkflowHost.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<O:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\u0008f\u0018\u0000*\n\u0008\u0000\u0010\u0001 \u0001*\u00020\u0002*\u0004\u0008\u0001\u0010\u00032\u00020\u0002J\u0008\u0010\r\u001a\u00020\u000eH&R\u001a\u0010\u0004\u001a\n\u0012\u0006\u0008\u0001\u0012\u00028\u00000\u0005X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007R \u0010\u0008\u001a\u0010\u0012\u000c\u0008\u0001\u0012\u0008\u0012\u0004\u0012\u00028\u00010\n0\tX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/workflow/rx2/RxWorkflowHost;",
        "O",
        "",
        "R",
        "outputs",
        "Lio/reactivex/Flowable;",
        "getOutputs",
        "()Lio/reactivex/Flowable;",
        "renderingsAndSnapshots",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/RenderingAndSnapshot;",
        "getRenderingsAndSnapshots",
        "()Lio/reactivex/Observable;",
        "cancel",
        "",
        "runtime-extensions"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract cancel()V
.end method

.method public abstract getOutputs()Lio/reactivex/Flowable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Flowable<",
            "+TO;>;"
        }
    .end annotation
.end method

.method public abstract getRenderingsAndSnapshots()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/workflow/RenderingAndSnapshot<",
            "TR;>;>;"
        }
    .end annotation
.end method
