.class public final Lcom/squareup/workflow/rx2/RxWorkersKt;
.super Ljava/lang/Object;
.source "RxWorkers.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRxWorkers.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 2 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 3 Worker.kt\ncom/squareup/workflow/Worker$Companion\n*L\n1#1,97:1\n56#1,2:98\n276#2:100\n276#2:101\n276#2:104\n276#2:107\n254#3:102\n203#3:103\n256#3:105\n240#3:106\n*E\n*S KotlinDebug\n*F\n+ 1 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n*L\n41#1,2:98\n41#1:100\n57#1:101\n70#1:104\n85#1:107\n70#1:102\n70#1:103\n70#1:105\n85#1:106\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u0003\u001a)\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00040\u0001\"\n\u0008\u0000\u0010\u0004\u0018\u0001*\u00020\u0005*\u000c\u0012\u0008\u0008\u0001\u0012\u0004\u0018\u0001H\u00040\u0006H\u0086\u0008\u001a)\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00040\u0001\"\n\u0008\u0000\u0010\u0004\u0018\u0001*\u00020\u0005*\u000c\u0012\u0008\u0008\u0001\u0012\u0004\u0018\u0001H\u00040\u0007H\u0086\u0008\u001a)\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00040\u0001\"\n\u0008\u0000\u0010\u0004\u0018\u0001*\u00020\u0005*\u000c\u0012\u0008\u0008\u0001\u0012\u0004\u0018\u0001H\u00040\u0008H\u0086\u0008\u001a)\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00040\u0001\"\n\u0008\u0000\u0010\u0004\u0018\u0001*\u00020\u0005*\u000c\u0012\u0008\u0008\u0001\u0012\u0004\u0018\u0001H\u00040\tH\u0086\u0008\u00a8\u0006\n"
    }
    d2 = {
        "asWorker",
        "Lcom/squareup/workflow/Worker;",
        "",
        "Lio/reactivex/Completable;",
        "T",
        "",
        "Lio/reactivex/Maybe;",
        "Lio/reactivex/Observable;",
        "Lio/reactivex/Single;",
        "Lorg/reactivestreams/Publisher;",
        "workflow-rx2"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final asWorker(Lio/reactivex/Completable;)Lcom/squareup/workflow/Worker;
    .locals 3

    const-string v0, "$this$asWorker"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v1, Lcom/squareup/workflow/rx2/RxWorkersKt$asWorker$3;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/workflow/rx2/RxWorkersKt$asWorker$3;-><init>(Lio/reactivex/Completable;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic asWorker(Lio/reactivex/Maybe;)Lcom/squareup/workflow/Worker;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Maybe<",
            "+TT;>;)",
            "Lcom/squareup/workflow/Worker<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$asWorker"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/workflow/rx2/RxWorkersKt$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/workflow/rx2/RxWorkersKt$asWorker$1;-><init>(Lio/reactivex/Maybe;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 102
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    new-instance p0, Lcom/squareup/workflow/rx2/RxWorkersKt$asWorker$$inlined$fromNullable$1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/workflow/rx2/RxWorkersKt$asWorker$$inlined$fromNullable$1;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V

    check-cast p0, Lkotlin/jvm/functions/Function2;

    .line 103
    invoke-static {p0}, Lkotlinx/coroutines/flow/FlowKt;->flow(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p0

    const/4 v0, 0x6

    const-string v2, "T"

    .line 104
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, v1, p0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method

.method public static final synthetic asWorker(Lio/reactivex/Observable;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "+TT;>;)",
            "Lcom/squareup/workflow/Worker<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$asWorker"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    sget-object v0, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p0

    const-string v0, "this.toFlowable(BUFFER)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lorg/reactivestreams/Publisher;

    if-eqz p0, :cond_0

    .line 99
    invoke-static {p0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p0

    const/4 v0, 0x6

    const-string v1, "T"

    .line 100
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const/4 v0, 0x0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1

    .line 99
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final synthetic asWorker(Lio/reactivex/Single;)Lcom/squareup/workflow/Worker;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Single<",
            "+TT;>;)",
            "Lcom/squareup/workflow/Worker<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$asWorker"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/workflow/rx2/RxWorkersKt$asWorker$2;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/workflow/rx2/RxWorkersKt$asWorker$2;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 106
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p0

    const/4 v0, 0x6

    const-string v2, "T"

    .line 107
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, v1, p0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method

.method public static final synthetic asWorker(Lorg/reactivestreams/Publisher;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/reactivestreams/Publisher<",
            "+TT;>;)",
            "Lcom/squareup/workflow/Worker<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$asWorker"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-static {p0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p0

    const/4 v0, 0x6

    const-string v1, "T"

    .line 101
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method
