.class final Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "LaunchWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/LaunchWorkflowKt;->launchWorkflowImpl(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/internal/WorkflowLoop;Lcom/squareup/workflow/StatefulWorkflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lkotlin/Unit;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0004\u0008\u0001\u0010\u0003\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005\"\u0004\u0008\u0003\u0010\u0006\"\u0004\u0008\u0004\u0010\u0007*\u00020\u0008H\u008a@\u00a2\u0006\u0004\u0008\t\u0010\n"
    }
    d2 = {
        "<anonymous>",
        "",
        "PropsT",
        "StateT",
        "OutputT",
        "",
        "RenderingT",
        "RunnerT",
        "Lkotlinx/coroutines/CoroutineScope;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.workflow.LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1"
    f = "LaunchWorkflow.kt"
    i = {
        0x0
    }
    l = {
        0x99
    }
    m = "invokeSuspend"
    n = {
        "$this$launch"
    }
    s = {
        "L$0"
    }
.end annotation


# instance fields
.field final synthetic $diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

.field final synthetic $initialSnapshot:Lcom/squareup/workflow/Snapshot;

.field final synthetic $initialState:Ljava/lang/Object;

.field final synthetic $outputs:Lkotlinx/coroutines/channels/BroadcastChannel;

.field final synthetic $props:Lkotlinx/coroutines/flow/Flow;

.field final synthetic $renderingsAndSnapshots:Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;

.field final synthetic $workerContext:Lkotlin/coroutines/CoroutineContext;

.field final synthetic $workflow:Lcom/squareup/workflow/StatefulWorkflow;

.field final synthetic $workflowLoop:Lcom/squareup/workflow/internal/WorkflowLoop;

.field L$0:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/StatefulWorkflow;Lcom/squareup/workflow/internal/WorkflowLoop;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;Lkotlinx/coroutines/channels/BroadcastChannel;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    iput-object p2, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$workflow:Lcom/squareup/workflow/StatefulWorkflow;

    iput-object p3, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$workflowLoop:Lcom/squareup/workflow/internal/WorkflowLoop;

    iput-object p4, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$props:Lkotlinx/coroutines/flow/Flow;

    iput-object p5, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$initialSnapshot:Lcom/squareup/workflow/Snapshot;

    iput-object p6, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$initialState:Ljava/lang/Object;

    iput-object p7, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$workerContext:Lkotlin/coroutines/CoroutineContext;

    iput-object p8, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$renderingsAndSnapshots:Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;

    iput-object p9, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$outputs:Lkotlinx/coroutines/channels/BroadcastChannel;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p10}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;

    iget-object v2, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    iget-object v3, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$workflow:Lcom/squareup/workflow/StatefulWorkflow;

    iget-object v4, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$workflowLoop:Lcom/squareup/workflow/internal/WorkflowLoop;

    iget-object v5, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$props:Lkotlinx/coroutines/flow/Flow;

    iget-object v6, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$initialSnapshot:Lcom/squareup/workflow/Snapshot;

    iget-object v7, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$initialState:Ljava/lang/Object;

    iget-object v8, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$workerContext:Lkotlin/coroutines/CoroutineContext;

    iget-object v9, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$renderingsAndSnapshots:Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;

    iget-object v10, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$outputs:Lkotlinx/coroutines/channels/BroadcastChannel;

    move-object v1, v0

    move-object v11, p2

    invoke-direct/range {v1 .. v11}, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;-><init>(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/StatefulWorkflow;Lcom/squareup/workflow/internal/WorkflowLoop;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;Lkotlinx/coroutines/channels/BroadcastChannel;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 141
    iget v1, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->label:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->L$0:Ljava/lang/Object;

    check-cast v0, Lkotlinx/coroutines/CoroutineScope;

    :try_start_0
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    .line 160
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 141
    :cond_1
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    .line 142
    iget-object v1, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    if-eqz v1, :cond_2

    iget-object v3, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$workflow:Lcom/squareup/workflow/StatefulWorkflow;

    check-cast v3, Lcom/squareup/workflow/Workflow;

    const/4 v4, 0x0

    invoke-static {v3, v4, v2, v4}, Lcom/squareup/workflow/internal/WorkflowIdKt;->id$default(Lcom/squareup/workflow/Workflow;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/internal/WorkflowId;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/workflow/internal/WorkflowId;->getTypeDebugString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, p1, v3}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onRuntimeStarted(Lkotlinx/coroutines/CoroutineScope;Ljava/lang/String;)V

    .line 145
    :cond_2
    :try_start_1
    iget-object v4, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$workflowLoop:Lcom/squareup/workflow/internal/WorkflowLoop;

    .line 146
    iget-object v5, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$workflow:Lcom/squareup/workflow/StatefulWorkflow;

    .line 147
    iget-object v6, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$props:Lkotlinx/coroutines/flow/Flow;

    .line 148
    iget-object v7, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$initialSnapshot:Lcom/squareup/workflow/Snapshot;

    .line 149
    iget-object v8, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$initialState:Ljava/lang/Object;

    .line 150
    iget-object v9, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$workerContext:Lkotlin/coroutines/CoroutineContext;

    .line 151
    new-instance v1, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1$1;

    iget-object v3, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$renderingsAndSnapshots:Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;

    invoke-direct {v1, v3}, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1$1;-><init>(Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;)V

    move-object v10, v1

    check-cast v10, Lkotlin/jvm/functions/Function2;

    .line 152
    new-instance v1, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1$2;

    iget-object v3, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$outputs:Lkotlinx/coroutines/channels/BroadcastChannel;

    invoke-direct {v1, v3}, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1$2;-><init>(Lkotlinx/coroutines/channels/BroadcastChannel;)V

    move-object v11, v1

    check-cast v11, Lkotlin/jvm/functions/Function2;

    .line 153
    iget-object v12, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    iput-object p1, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->L$0:Ljava/lang/Object;

    iput v2, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->label:I

    move-object v13, p0

    .line 145
    invoke-interface/range {v4 .. v13}, Lcom/squareup/workflow/internal/WorkflowLoop;->runWorkflowLoop(Lcom/squareup/workflow/StatefulWorkflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-ne p1, v0, :cond_3

    return-object v0

    .line 158
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    if-eqz p1, :cond_4

    invoke-interface {p1}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onRuntimeStopped()V

    .line 160
    :cond_4
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1

    .line 158
    :goto_1
    iget-object v0, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$workflowJob$1;->$diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    if-eqz v0, :cond_5

    invoke-interface {v0}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onRuntimeStopped()V

    :cond_5
    throw p1
.end method
