.class public final Lcom/squareup/workflow/WorkflowSession;
.super Ljava/lang/Object;
.source "WorkflowSession.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<OutputT:",
        "Ljava/lang/Object;",
        "RenderingT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\u0018\u0000*\n\u0008\u0000\u0010\u0001 \u0001*\u00020\u0002*\u0006\u0008\u0001\u0010\u0003 \u00012\u00020\u0002B3\u0012\u0012\u0010\u0004\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\u00060\u0005\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0005\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\nR\u001c\u0010\u0008\u001a\u0004\u0018\u00010\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u001d\u0010\u0004\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0010\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/workflow/WorkflowSession;",
        "OutputT",
        "",
        "RenderingT",
        "renderingsAndSnapshots",
        "Lkotlinx/coroutines/flow/Flow;",
        "Lcom/squareup/workflow/RenderingAndSnapshot;",
        "outputs",
        "diagnosticListener",
        "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
        "(Lkotlinx/coroutines/flow/Flow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V",
        "getDiagnosticListener",
        "()Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
        "setDiagnosticListener",
        "(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V",
        "getOutputs",
        "()Lkotlinx/coroutines/flow/Flow;",
        "getRenderingsAndSnapshots",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

.field private final outputs:Lkotlinx/coroutines/flow/Flow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/flow/Flow<",
            "TOutputT;>;"
        }
    .end annotation
.end field

.field private final renderingsAndSnapshots:Lkotlinx/coroutines/flow/Flow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/flow/Flow<",
            "Lcom/squareup/workflow/RenderingAndSnapshot<",
            "TRenderingT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlinx/coroutines/flow/Flow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/flow/Flow<",
            "+",
            "Lcom/squareup/workflow/RenderingAndSnapshot<",
            "+TRenderingT;>;>;",
            "Lkotlinx/coroutines/flow/Flow<",
            "+TOutputT;>;",
            "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
            ")V"
        }
    .end annotation

    const-string v0, "renderingsAndSnapshots"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "outputs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/WorkflowSession;->renderingsAndSnapshots:Lkotlinx/coroutines/flow/Flow;

    iput-object p2, p0, Lcom/squareup/workflow/WorkflowSession;->outputs:Lkotlinx/coroutines/flow/Flow;

    iput-object p3, p0, Lcom/squareup/workflow/WorkflowSession;->diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlinx/coroutines/flow/Flow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 33
    check-cast p3, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/workflow/WorkflowSession;-><init>(Lkotlinx/coroutines/flow/Flow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V

    return-void
.end method


# virtual methods
.method public final getDiagnosticListener()Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/workflow/WorkflowSession;->diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    return-object v0
.end method

.method public final getOutputs()Lkotlinx/coroutines/flow/Flow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/flow/Flow<",
            "TOutputT;>;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/workflow/WorkflowSession;->outputs:Lkotlinx/coroutines/flow/Flow;

    return-object v0
.end method

.method public final getRenderingsAndSnapshots()Lkotlinx/coroutines/flow/Flow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/flow/Flow<",
            "Lcom/squareup/workflow/RenderingAndSnapshot<",
            "TRenderingT;>;>;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/workflow/WorkflowSession;->renderingsAndSnapshots:Lkotlinx/coroutines/flow/Flow;

    return-object v0
.end method

.method public final setDiagnosticListener(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/squareup/workflow/WorkflowSession;->diagnosticListener:Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    return-void
.end method
