.class public final Lcom/squareup/workflow/LayoutRunnerCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "LayoutRunnerCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D::",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        ">",
        "Lcom/squareup/coordinators/Coordinator;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u0003BA\u0012\u0018\u0010\u0004\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00070\u00060\u0005\u0012\u0018\u0010\u0008\u001a\u0014\u0012\u0004\u0012\u00020\n\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\u000b0\t\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\nH\u0016R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0008\u001a\u0014\u0012\u0004\u0012\u00020\n\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\u000b0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\u000f\u001a&\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00018\u00008\u0000 \u0010*\u0012\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00018\u00008\u0000\u0018\u00010\u00050\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/workflow/LayoutRunnerCoordinator;",
        "D",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "Lcom/squareup/coordinators/Coordinator;",
        "legacyScreens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "runnerConstructor",
        "Lkotlin/Function1;",
        "Landroid/view/View;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/ui/ContainerHints;)V",
        "screens",
        "kotlin.jvm.PlatformType",
        "attach",
        "",
        "view",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final containerHints:Lcom/squareup/workflow/ui/ContainerHints;

.field private final runnerConstructor:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/ui/LayoutRunner<",
            "TD;>;>;"
        }
    .end annotation
.end field

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "TD;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "+",
            "Lcom/squareup/workflow/ui/LayoutRunner<",
            "TD;>;>;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            ")V"
        }
    .end annotation

    const-string v0, "legacyScreens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "runnerConstructor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p2, p0, Lcom/squareup/workflow/LayoutRunnerCoordinator;->runnerConstructor:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/squareup/workflow/LayoutRunnerCoordinator;->containerHints:Lcom/squareup/workflow/ui/ContainerHints;

    .line 21
    sget-object p2, Lcom/squareup/workflow/LayoutRunnerCoordinator$screens$1;->INSTANCE:Lcom/squareup/workflow/LayoutRunnerCoordinator$screens$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/LayoutRunnerCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getContainerHints$p(Lcom/squareup/workflow/LayoutRunnerCoordinator;)Lcom/squareup/workflow/ui/ContainerHints;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/workflow/LayoutRunnerCoordinator;->containerHints:Lcom/squareup/workflow/ui/ContainerHints;

    return-object p0
.end method

.method public static final synthetic access$getRunnerConstructor$p(Lcom/squareup/workflow/LayoutRunnerCoordinator;)Lkotlin/jvm/functions/Function1;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/workflow/LayoutRunnerCoordinator;->runnerConstructor:Lkotlin/jvm/functions/Function1;

    return-object p0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 25
    iget-object v0, p0, Lcom/squareup/workflow/LayoutRunnerCoordinator;->containerHints:Lcom/squareup/workflow/ui/ContainerHints;

    invoke-static {p1, v0}, Lcom/squareup/workflow/LayoutRunnerCoordinatorKt;->access$setPosHints$p(Landroid/view/View;Lcom/squareup/workflow/ui/ContainerHints;)V

    .line 27
    iget-object v0, p0, Lcom/squareup/workflow/LayoutRunnerCoordinator;->screens:Lio/reactivex/Observable;

    .line 28
    new-instance v1, Lcom/squareup/workflow/LayoutRunnerCoordinator$attach$1;

    invoke-direct {v1, p1}, Lcom/squareup/workflow/LayoutRunnerCoordinator$attach$1;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Action;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnDispose(Lio/reactivex/functions/Action;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screens\n        .doOnDis\u2026view.setRunner<D>(null) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    new-instance v1, Lcom/squareup/workflow/LayoutRunnerCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/workflow/LayoutRunnerCoordinator$attach$2;-><init>(Lcom/squareup/workflow/LayoutRunnerCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
