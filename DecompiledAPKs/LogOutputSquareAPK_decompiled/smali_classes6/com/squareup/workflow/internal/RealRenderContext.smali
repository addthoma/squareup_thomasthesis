.class public final Lcom/squareup/workflow/internal/RealRenderContext;
.super Ljava/lang/Object;
.source "RealRenderContext.kt"

# interfaces
.implements Lcom/squareup/workflow/RenderContext;
.implements Lcom/squareup/workflow/Sink;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/internal/RealRenderContext$Renderer;,
        Lcom/squareup/workflow/internal/RealRenderContext$WorkerRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<StateT:",
        "Ljava/lang/Object;",
        "OutputT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/RenderContext<",
        "TStateT;TOutputT;>;",
        "Lcom/squareup/workflow/Sink<",
        "Lcom/squareup/workflow/WorkflowAction<",
        "TStateT;+TOutputT;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealRenderContext.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealRenderContext.kt\ncom/squareup/workflow/internal/RealRenderContext\n*L\n1#1,119:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000*\u0004\u0008\u0000\u0010\u0001*\u0008\u0008\u0001\u0010\u0002*\u00020\u00032\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u00042\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u00060\u0005:\u0002+,BG\u0012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0008\u0012\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\n\u0012\u0018\u0010\u000b\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00060\u000c\u00a2\u0006\u0002\u0010\rJ\u0008\u0010\u0013\u001a\u00020\u0014H\u0002J\u0006\u0010\u0015\u001a\u00020\u0014J8\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u0002H\u00180\u0017\"\u0008\u0008\u0002\u0010\u0018*\u00020\u00032\u001e\u0010\u0019\u001a\u001a\u0012\u0004\u0012\u0002H\u0018\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00060\u001aH\u0016Jm\u0010\u001b\u001a\u0002H\u001c\"\u0004\u0008\u0002\u0010\u001d\"\u0008\u0008\u0003\u0010\u001e*\u00020\u0003\"\u0004\u0008\u0004\u0010\u001c2\u0018\u0010\u001f\u001a\u0014\u0012\u0004\u0012\u0002H\u001d\u0012\u0004\u0012\u0002H\u001e\u0012\u0004\u0012\u0002H\u001c0 2\u0006\u0010!\u001a\u0002H\u001d2\u0006\u0010\"\u001a\u00020#2\u001e\u0010\u0019\u001a\u001a\u0012\u0004\u0012\u0002H\u001e\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00060\u001aH\u0016\u00a2\u0006\u0002\u0010$JD\u0010%\u001a\u00020\u0014\"\u0004\u0008\u0002\u0010&2\u000c\u0010\'\u001a\u0008\u0012\u0004\u0012\u0002H&0(2\u0006\u0010\"\u001a\u00020#2\u001e\u0010\u0019\u001a\u001a\u0012\u0004\u0012\u0002H&\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00060\u001aH\u0016J\u001c\u0010)\u001a\u00020\u00142\u0012\u0010*\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006H\u0016R&\u0010\u000e\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00060\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0010R \u0010\u000b\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00060\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/workflow/internal/RealRenderContext;",
        "StateT",
        "OutputT",
        "",
        "Lcom/squareup/workflow/RenderContext;",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "renderer",
        "Lcom/squareup/workflow/internal/RealRenderContext$Renderer;",
        "workerRunner",
        "Lcom/squareup/workflow/internal/RealRenderContext$WorkerRunner;",
        "eventActionsChannel",
        "Lkotlinx/coroutines/channels/SendChannel;",
        "(Lcom/squareup/workflow/internal/RealRenderContext$Renderer;Lcom/squareup/workflow/internal/RealRenderContext$WorkerRunner;Lkotlinx/coroutines/channels/SendChannel;)V",
        "actionSink",
        "getActionSink",
        "()Lcom/squareup/workflow/Sink;",
        "frozen",
        "",
        "checkNotFrozen",
        "",
        "freeze",
        "onEvent",
        "Lcom/squareup/workflow/EventHandler;",
        "EventT",
        "handler",
        "Lkotlin/Function1;",
        "renderChild",
        "ChildRenderingT",
        "ChildPropsT",
        "ChildOutputT",
        "child",
        "Lcom/squareup/workflow/Workflow;",
        "props",
        "key",
        "",
        "(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;",
        "runningWorker",
        "T",
        "worker",
        "Lcom/squareup/workflow/Worker;",
        "send",
        "value",
        "Renderer",
        "WorkerRunner",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final eventActionsChannel:Lkotlinx/coroutines/channels/SendChannel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/channels/SendChannel<",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>;"
        }
    .end annotation
.end field

.field private frozen:Z

.field private final renderer:Lcom/squareup/workflow/internal/RealRenderContext$Renderer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/internal/RealRenderContext$Renderer<",
            "TStateT;TOutputT;>;"
        }
    .end annotation
.end field

.field private final workerRunner:Lcom/squareup/workflow/internal/RealRenderContext$WorkerRunner;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/internal/RealRenderContext$WorkerRunner<",
            "TStateT;TOutputT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/internal/RealRenderContext$Renderer;Lcom/squareup/workflow/internal/RealRenderContext$WorkerRunner;Lkotlinx/coroutines/channels/SendChannel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/internal/RealRenderContext$Renderer<",
            "TStateT;TOutputT;>;",
            "Lcom/squareup/workflow/internal/RealRenderContext$WorkerRunner<",
            "TStateT;TOutputT;>;",
            "Lkotlinx/coroutines/channels/SendChannel<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>;)V"
        }
    .end annotation

    const-string v0, "renderer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workerRunner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventActionsChannel"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/internal/RealRenderContext;->renderer:Lcom/squareup/workflow/internal/RealRenderContext$Renderer;

    iput-object p2, p0, Lcom/squareup/workflow/internal/RealRenderContext;->workerRunner:Lcom/squareup/workflow/internal/RealRenderContext$WorkerRunner;

    iput-object p3, p0, Lcom/squareup/workflow/internal/RealRenderContext;->eventActionsChannel:Lkotlinx/coroutines/channels/SendChannel;

    return-void
.end method

.method public static final synthetic access$getEventActionsChannel$p(Lcom/squareup/workflow/internal/RealRenderContext;)Lkotlinx/coroutines/channels/SendChannel;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/workflow/internal/RealRenderContext;->eventActionsChannel:Lkotlinx/coroutines/channels/SendChannel;

    return-object p0
.end method

.method private final checkNotFrozen()V
    .locals 2

    .line 115
    iget-boolean v0, p0, Lcom/squareup/workflow/internal/RealRenderContext;->frozen:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RenderContext cannot be used after render method returns."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public final freeze()V
    .locals 1

    .line 111
    invoke-direct {p0}, Lcom/squareup/workflow/internal/RealRenderContext;->checkNotFrozen()V

    const/4 v0, 0x1

    .line 112
    iput-boolean v0, p0, Lcom/squareup/workflow/internal/RealRenderContext;->frozen:Z

    return-void
.end method

.method public getActionSink()Lcom/squareup/workflow/Sink;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Sink<",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>;"
        }
    .end annotation

    .line 65
    move-object v0, p0

    check-cast v0, Lcom/squareup/workflow/Sink;

    return-object v0
.end method

.method public makeActionSink()Lcom/squareup/workflow/Sink;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>()",
            "Lcom/squareup/workflow/Sink<",
            "TA;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use RenderContext.actionSink."
    .end annotation

    .line 33
    invoke-static {p0}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->makeActionSink(Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/Sink;

    move-result-object v0

    return-object v0
.end method

.method public onEvent(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/EventHandler;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<EventT:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-TEventT;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>;)",
            "Lcom/squareup/workflow/EventHandler<",
            "TEventT;>;"
        }
    .end annotation

    const-string v0, "handler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-direct {p0}, Lcom/squareup/workflow/internal/RealRenderContext;->checkNotFrozen()V

    .line 71
    new-instance v0, Lcom/squareup/workflow/EventHandler;

    new-instance v1, Lcom/squareup/workflow/internal/RealRenderContext$onEvent$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/workflow/internal/RealRenderContext$onEvent$1;-><init>(Lcom/squareup/workflow/internal/RealRenderContext;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v1}, Lcom/squareup/workflow/EventHandler;-><init>(Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public bridge synthetic onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;
    .locals 0

    .line 33
    invoke-virtual {p0, p1}, Lcom/squareup/workflow/internal/RealRenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/EventHandler;

    move-result-object p1

    check-cast p1, Lkotlin/jvm/functions/Function1;

    return-object p1
.end method

.method public renderChild(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ChildPropsT:",
            "Ljava/lang/Object;",
            "ChildOutputT:",
            "Ljava/lang/Object;",
            "ChildRenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Workflow<",
            "-TChildPropsT;+TChildOutputT;+TChildRenderingT;>;TChildPropsT;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-TChildOutputT;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>;)TChildRenderingT;"
        }
    .end annotation

    const-string v0, "child"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-direct {p0}, Lcom/squareup/workflow/internal/RealRenderContext;->checkNotFrozen()V

    .line 95
    iget-object v0, p0, Lcom/squareup/workflow/internal/RealRenderContext;->renderer:Lcom/squareup/workflow/internal/RealRenderContext$Renderer;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/squareup/workflow/internal/RealRenderContext$Renderer;->render(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Worker<",
            "+TT;>;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>;)V"
        }
    .end annotation

    const-string v0, "worker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    invoke-direct {p0}, Lcom/squareup/workflow/internal/RealRenderContext;->checkNotFrozen()V

    .line 104
    iget-object v0, p0, Lcom/squareup/workflow/internal/RealRenderContext;->workerRunner:Lcom/squareup/workflow/internal/RealRenderContext$WorkerRunner;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/workflow/internal/RealRenderContext$WorkerRunner;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public send(Lcom/squareup/workflow/WorkflowAction;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;)V"
        }
    .end annotation

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-boolean v0, p0, Lcom/squareup/workflow/internal/RealRenderContext;->frozen:Z

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/squareup/workflow/internal/RealRenderContext;->eventActionsChannel:Lkotlinx/coroutines/channels/SendChannel;

    invoke-interface {v0, p1}, Lkotlinx/coroutines/channels/SendChannel;->offer(Ljava/lang/Object;)Z

    return-void

    .line 81
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    .line 82
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected sink to not be sent to until after the render pass. Received action: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 81
    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic send(Ljava/lang/Object;)V
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/internal/RealRenderContext;->send(Lcom/squareup/workflow/WorkflowAction;)V

    return-void
.end method
