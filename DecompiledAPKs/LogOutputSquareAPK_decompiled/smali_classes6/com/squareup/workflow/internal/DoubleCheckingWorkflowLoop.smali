.class public final Lcom/squareup/workflow/internal/DoubleCheckingWorkflowLoop;
.super Lcom/squareup/workflow/internal/RealWorkflowLoop;
.source "WorkflowLoop.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002Jq\u0010\u0003\u001a\u0002H\u0004\"\u0004\u0008\u0000\u0010\u0005\"\u0004\u0008\u0001\u0010\u0006\"\u0008\u0008\u0002\u0010\u0007*\u00020\u0008\"\u0004\u0008\u0003\u0010\u00042\u001e\u0010\t\u001a\u001a\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u00040\n2\u001e\u0010\u000b\u001a\u001a\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u00040\u000c2\u0006\u0010\r\u001a\u0002H\u0005H\u0014\u00a2\u0006\u0002\u0010\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/workflow/internal/DoubleCheckingWorkflowLoop;",
        "Lcom/squareup/workflow/internal/RealWorkflowLoop;",
        "()V",
        "doRender",
        "RenderingT",
        "PropsT",
        "StateT",
        "OutputT",
        "",
        "rootNode",
        "Lcom/squareup/workflow/internal/WorkflowNode;",
        "workflow",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "props",
        "(Lcom/squareup/workflow/internal/WorkflowNode;Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)Ljava/lang/Object;",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 158
    invoke-direct {p0}, Lcom/squareup/workflow/internal/RealWorkflowLoop;-><init>()V

    return-void
.end method


# virtual methods
.method protected doRender(Lcom/squareup/workflow/internal/WorkflowNode;Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/internal/WorkflowNode<",
            "TPropsT;TStateT;TOutputT;TRenderingT;>;",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "-TPropsT;TStateT;+TOutputT;+TRenderingT;>;TPropsT;)TRenderingT;"
        }
    .end annotation

    const-string v0, "rootNode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    invoke-super {p0, p1, p2, p3}, Lcom/squareup/workflow/internal/RealWorkflowLoop;->doRender(Lcom/squareup/workflow/internal/WorkflowNode;Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    invoke-super {p0, p1, p2, p3}, Lcom/squareup/workflow/internal/RealWorkflowLoop;->doRender(Lcom/squareup/workflow/internal/WorkflowNode;Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
