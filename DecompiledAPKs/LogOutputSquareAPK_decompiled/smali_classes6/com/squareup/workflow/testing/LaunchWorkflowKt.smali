.class public final Lcom/squareup/workflow/testing/LaunchWorkflowKt;
.super Ljava/lang/Object;
.source "LaunchWorkflow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0008\u0005\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u00ab\u0001\u0010\u0000\u001a\u0002H\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0004\u0008\u0001\u0010\u0003\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005\"\u0004\u0008\u0003\u0010\u0006\"\u0004\u0008\u0004\u0010\u00012\u0006\u0010\u0007\u001a\u00020\u00082\u001e\u0010\t\u001a\u001a\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00060\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u000c2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u000e28\u0010\u000f\u001a4\u0012\u0004\u0012\u00020\u0008\u0012\u001f\u0012\u001d\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00060\u0011\u00a2\u0006\u000c\u0008\u0012\u0012\u0008\u0008\u0013\u0012\u0004\u0008\u0008(\u0014\u0012\u0004\u0012\u0002H\u00010\u0010\u00a2\u0006\u0002\u0008\u0015\u00a2\u0006\u0002\u0010\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "launchWorkflowForTestFromStateIn",
        "RunnerT",
        "PropsT",
        "StateT",
        "OutputT",
        "",
        "RenderingT",
        "scope",
        "Lkotlinx/coroutines/CoroutineScope;",
        "workflow",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "props",
        "Lkotlinx/coroutines/flow/Flow;",
        "testParams",
        "Lcom/squareup/workflow/testing/WorkflowTestParams;",
        "beforeStart",
        "Lkotlin/Function2;",
        "Lcom/squareup/workflow/WorkflowSession;",
        "Lkotlin/ParameterName;",
        "name",
        "session",
        "Lkotlin/ExtensionFunctionType;",
        "(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/StatefulWorkflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/testing/WorkflowTestParams;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;",
        "workflow-runtime"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final launchWorkflowForTestFromStateIn(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/StatefulWorkflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/testing/WorkflowTestParams;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            "RunnerT:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlinx/coroutines/CoroutineScope;",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "-TPropsT;TStateT;+TOutputT;+TRenderingT;>;",
            "Lkotlinx/coroutines/flow/Flow<",
            "+TPropsT;>;",
            "Lcom/squareup/workflow/testing/WorkflowTestParams<",
            "+TStateT;>;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lkotlinx/coroutines/CoroutineScope;",
            "-",
            "Lcom/squareup/workflow/WorkflowSession<",
            "+TOutputT;+TRenderingT;>;+TRunnerT;>;)TRunnerT;"
        }
    .end annotation

    const-string v0, "scope"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "props"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "testParams"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "beforeStart"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-virtual {p3}, Lcom/squareup/workflow/testing/WorkflowTestParams;->getStartFrom()Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode;

    move-result-object v0

    instance-of v1, v0, Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode$StartFromState;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v0, v2

    :cond_0
    check-cast v0, Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode$StartFromState;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode$StartFromState;->getState()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    goto :goto_0

    :cond_1
    move-object v8, v2

    .line 49
    :goto_0
    invoke-virtual {p3}, Lcom/squareup/workflow/testing/WorkflowTestParams;->getStartFrom()Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode;

    move-result-object v0

    .line 50
    instance-of v1, v0, Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode$StartFromWorkflowSnapshot;

    if-eqz v1, :cond_3

    .line 53
    check-cast v0, Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode$StartFromWorkflowSnapshot;

    invoke-virtual {v0}, Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode$StartFromWorkflowSnapshot;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/internal/TreeSnapshotsKt;->createTreeSnapshot(Lcom/squareup/workflow/Snapshot;Ljava/util/List;)Lcom/squareup/workflow/Snapshot;

    move-result-object v2

    :cond_2
    :goto_1
    move-object v7, v2

    goto :goto_2

    .line 55
    :cond_3
    instance-of v1, v0, Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode$StartFromCompleteSnapshot;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode$StartFromCompleteSnapshot;

    invoke-virtual {v0}, Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode$StartFromCompleteSnapshot;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v2

    goto :goto_1

    .line 61
    :goto_2
    invoke-virtual {p3}, Lcom/squareup/workflow/testing/WorkflowTestParams;->getCheckRenderIdempotence()Z

    move-result p3

    if-eqz p3, :cond_4

    new-instance p3, Lcom/squareup/workflow/internal/DoubleCheckingWorkflowLoop;

    invoke-direct {p3}, Lcom/squareup/workflow/internal/DoubleCheckingWorkflowLoop;-><init>()V

    check-cast p3, Lcom/squareup/workflow/internal/RealWorkflowLoop;

    goto :goto_3

    :cond_4
    new-instance p3, Lcom/squareup/workflow/internal/RealWorkflowLoop;

    invoke-direct {p3}, Lcom/squareup/workflow/internal/RealWorkflowLoop;-><init>()V

    :goto_3
    move-object v4, p3

    check-cast v4, Lcom/squareup/workflow/internal/WorkflowLoop;

    .line 71
    invoke-interface {p0}, Lkotlinx/coroutines/CoroutineScope;->getCoroutineContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object p3

    sget-object v0, Lkotlinx/coroutines/Job;->Key:Lkotlinx/coroutines/Job$Key;

    check-cast v0, Lkotlin/coroutines/CoroutineContext$Key;

    invoke-interface {p3, v0}, Lkotlin/coroutines/CoroutineContext;->minusKey(Lkotlin/coroutines/CoroutineContext$Key;)Lkotlin/coroutines/CoroutineContext;

    move-result-object v9

    move-object v3, p0

    move-object v5, p1

    move-object v6, p2

    move-object v10, p4

    .line 59
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/LaunchWorkflowKt;->launchWorkflowImpl(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/internal/WorkflowLoop;Lcom/squareup/workflow/StatefulWorkflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method
