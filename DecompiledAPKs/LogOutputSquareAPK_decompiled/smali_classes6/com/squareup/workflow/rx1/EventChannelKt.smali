.class public final Lcom/squareup/workflow/rx1/EventChannelKt;
.super Ljava/lang/Object;
.source "EventChannel.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u001a \u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u0008\u0012\u0004\u0012\u0002H\u00020\u0004\u001aN\u0010\u0005\u001a\u00020\u0006\"\u0008\u0008\u0000\u0010\u0007*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0008*\u00020\u0003*\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u0002H\u00080\t2\u000e\u0010\n\u001a\n\u0012\u0006\u0008\u0001\u0012\u0002H\u00070\u000b2\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u00080\rH\u0007\u001aN\u0010\u000e\u001a\u00020\u0006\"\u0008\u0008\u0000\u0010\u0007*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0008*\u00020\u0003*\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u0002H\u00080\t2\u000e\u0010\u000f\u001a\n\u0012\u0006\u0008\u0001\u0012\u0002H\u00070\u00102\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u00080\rH\u0007\u00a8\u0006\u0011"
    }
    d2 = {
        "asRx1EventChannel",
        "Lcom/squareup/workflow/rx1/EventChannel;",
        "E",
        "",
        "Lcom/squareup/workflow/legacy/rx2/EventChannel;",
        "onNext",
        "",
        "T",
        "R",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;",
        "observable",
        "Lrx/Observable;",
        "handler",
        "Lkotlin/Function1;",
        "onSuccess",
        "single",
        "Lrx/Single;",
        "pure-rx1"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final asRx1EventChannel(Lcom/squareup/workflow/legacy/rx2/EventChannel;)Lcom/squareup/workflow/rx1/EventChannel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
            "TE;>;)",
            "Lcom/squareup/workflow/rx1/EventChannel<",
            "TE;>;"
        }
    .end annotation

    const-string v0, "$this$asRx1EventChannel"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v0, Lcom/squareup/workflow/rx1/EventChannelKt$asRx1EventChannel$1;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/rx1/EventChannelKt$asRx1EventChannel$1;-><init>(Lcom/squareup/workflow/legacy/rx2/EventChannel;)V

    check-cast v0, Lcom/squareup/workflow/rx1/EventChannel;

    return-object v0
.end method

.method public static final onNext(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;Lrx/Observable;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
            "*TR;>;",
            "Lrx/Observable<",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+TR;>;)V"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use `Single.asWorker()` and `WorkflowPool.onWorkerResult()`. If you have a hot observable, you should subscribe to it for the lifetime of your workflow and convert its items to events."
    .end annotation

    const-string v0, "$this$onNext"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "observable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-virtual {p1}, Lrx/Observable;->first()Lrx/Observable;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Observable;->toSingle()Lrx/Single;

    move-result-object p1

    const-string v0, "observable.first().toSingle()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1, p2}, Lcom/squareup/workflow/rx1/EventChannelKt;->onSuccess(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;Lrx/Single;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static final onSuccess(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;Lrx/Single;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
            "*TR;>;",
            "Lrx/Single<",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+TR;>;)V"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use `Single.asWorker()` and `WorkflowPool.onWorkerResult()`. If you have a hot observable, you should subscribe to it for the lifetime of your workflow and convert its items to events."
    .end annotation

    const-string v0, "$this$onSuccess"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "single"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "toV2Single(single)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onSuccess(Lio/reactivex/Single;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method
