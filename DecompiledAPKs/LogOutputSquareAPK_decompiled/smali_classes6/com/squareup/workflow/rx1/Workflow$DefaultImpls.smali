.class public final Lcom/squareup/workflow/rx1/Workflow$DefaultImpls;
.super Ljava/lang/Object;
.source "Workflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/rx1/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static abandon(Lcom/squareup/workflow/rx1/Workflow;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+TS;-TE;+TO;>;)V"
        }
    .end annotation

    return-void
.end method

.method public static toCompletable(Lcom/squareup/workflow/rx1/Workflow;)Lrx/Completable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+TS;-TE;+TO;>;)",
            "Lrx/Completable;"
        }
    .end annotation

    .line 64
    invoke-interface {p0}, Lcom/squareup/workflow/rx1/Workflow;->getState()Lrx/Observable;

    move-result-object p0

    invoke-virtual {p0}, Lrx/Observable;->toCompletable()Lrx/Completable;

    move-result-object p0

    const-string v0, "state.toCompletable()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
