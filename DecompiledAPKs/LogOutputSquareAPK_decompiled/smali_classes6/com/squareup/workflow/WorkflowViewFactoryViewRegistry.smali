.class final Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;
.super Ljava/lang/Object;
.source "WorkflowViewFactoryViewRegistry.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/ViewRegistry;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowViewFactoryViewRegistry.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowViewFactoryViewRegistry.kt\ncom/squareup/workflow/WorkflowViewFactoryViewRegistry\n*L\n1#1,77:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0010\u0000\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J9\u0010\u000c\u001a\u00020\r\"\u0008\u0008\u0000\u0010\u000e*\u00020\u00072\u0006\u0010\u000f\u001a\u0002H\u000e2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016\u00a2\u0006\u0002\u0010\u0016J\u0016\u0010\u0017\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002R!\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00068VX\u0096\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\n\u0010\u000b\u001a\u0004\u0008\u0008\u0010\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;",
        "Lcom/squareup/workflow/ui/ViewRegistry;",
        "viewFactory",
        "Lcom/squareup/workflow/WorkflowViewFactory;",
        "(Lcom/squareup/workflow/WorkflowViewFactory;)V",
        "keys",
        "",
        "",
        "getKeys",
        "()Ljava/util/Set;",
        "keys$delegate",
        "Lkotlin/Lazy;",
        "buildView",
        "Landroid/view/View;",
        "RenderingT",
        "initialRendering",
        "initialContainerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "contextForNewView",
        "Landroid/content/Context;",
        "container",
        "Landroid/view/ViewGroup;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;",
        "kclassOrNull",
        "Lkotlin/reflect/KClass;",
        "name",
        "",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final keys$delegate:Lkotlin/Lazy;

.field private final viewFactory:Lcom/squareup/workflow/WorkflowViewFactory;


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/WorkflowViewFactory;)V
    .locals 1

    const-string v0, "viewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;->viewFactory:Lcom/squareup/workflow/WorkflowViewFactory;

    .line 40
    sget-object p1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    new-instance v0, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry$keys$2;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry$keys$2;-><init>(Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lkotlin/LazyKt;->lazy(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;->keys$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static final synthetic access$getViewFactory$p(Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;)Lcom/squareup/workflow/WorkflowViewFactory;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;->viewFactory:Lcom/squareup/workflow/WorkflowViewFactory;

    return-object p0
.end method

.method public static final synthetic access$kclassOrNull(Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;Ljava/lang/String;)Lkotlin/reflect/KClass;
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;->kclassOrNull(Ljava/lang/String;)Lkotlin/reflect/KClass;

    move-result-object p0

    return-object p0
.end method

.method private final kclassOrNull(Ljava/lang/String;)Lkotlin/reflect/KClass;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lkotlin/reflect/KClass<",
            "*>;"
        }
    .end annotation

    .line 70
    :try_start_0
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "Class.forName(name)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/jvm/JvmClassMappingKt;->getKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "Workflow screen key typeName is not a valid class name, ignoring: %s"

    .line 73
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method


# virtual methods
.method public buildView(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RenderingT:",
            "Ljava/lang/Object;",
            ">(TRenderingT;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "initialRendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialContainerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contextForNewView"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    new-instance v0, Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "initialRendering::class.java.name"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3, v2}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 54
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v7

    const-string v1, "BehaviorRelay.create<AnyScreen>()"

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iget-object v1, p0, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;->viewFactory:Lcom/squareup/workflow/WorkflowViewFactory;

    move-object v3, v7

    check-cast v3, Lio/reactivex/Observable;

    move-object v2, v0

    move-object v4, p4

    move-object v5, p3

    move-object v6, p2

    invoke-interface/range {v1 .. v6}, Lcom/squareup/workflow/WorkflowViewFactory;->buildView(Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Landroid/view/ViewGroup;Landroid/content/Context;Lcom/squareup/workflow/ui/ContainerHints;)Landroid/view/View;

    move-result-object p3

    if-eqz p3, :cond_0

    .line 61
    new-instance p4, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry$buildView$2;

    invoke-direct {p4, v0, v7}, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry$buildView$2;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Lcom/jakewharton/rxrelay2/BehaviorRelay;)V

    check-cast p4, Lkotlin/jvm/functions/Function2;

    invoke-static {p3, p1, p2, p4}, Lcom/squareup/workflow/ui/ViewShowRenderingKt;->bindShowRendering(Landroid/view/View;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function2;)V

    return-object p3

    .line 57
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Expected binding for key "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public getKeys()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;->keys$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method
