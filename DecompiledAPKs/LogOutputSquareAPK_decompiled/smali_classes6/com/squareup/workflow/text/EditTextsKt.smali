.class public final Lcom/squareup/workflow/text/EditTextsKt;
.super Ljava/lang/Object;
.source "EditTexts.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\u0012\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "asTextViewLike",
        "Lcom/squareup/workflow/text/WorkflowTextHelper$TextViewLike;",
        "Landroid/widget/TextView;",
        "setWorkflowText",
        "",
        "Landroid/widget/EditText;",
        "text",
        "Lcom/squareup/workflow/text/WorkflowEditableText;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final asTextViewLike(Landroid/widget/TextView;)Lcom/squareup/workflow/text/WorkflowTextHelper$TextViewLike;
    .locals 1

    const-string v0, "$this$asTextViewLike"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    new-instance v0, Lcom/squareup/workflow/text/EditTextsKt$asTextViewLike$1;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/text/EditTextsKt$asTextViewLike$1;-><init>(Landroid/widget/TextView;)V

    check-cast v0, Lcom/squareup/workflow/text/WorkflowTextHelper$TextViewLike;

    return-object v0
.end method

.method public static final setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V
    .locals 1

    const-string v0, "$this$setWorkflowText"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    sget-object v0, Lcom/squareup/workflow/text/WorkflowTextHelper;->Companion:Lcom/squareup/workflow/text/WorkflowTextHelper$Companion;

    check-cast p0, Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/text/WorkflowTextHelper$Companion;->getForView(Landroid/widget/TextView;)Lcom/squareup/workflow/text/WorkflowTextHelper;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/workflow/text/WorkflowTextHelper;->Companion:Lcom/squareup/workflow/text/WorkflowTextHelper$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/text/WorkflowTextHelper$Companion;->createForView(Landroid/widget/TextView;)Lcom/squareup/workflow/text/WorkflowTextHelper;

    move-result-object v0

    .line 23
    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/workflow/text/WorkflowTextHelper;->update(Lcom/squareup/workflow/text/WorkflowEditableText;)V

    return-void
.end method
