.class public final Lcom/squareup/workflow/legacy/WorkflowInput$ReadOnly;
.super Ljava/lang/Object;
.source "WorkflowInput.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/WorkflowInput;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/legacy/WorkflowInput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReadOnly"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/WorkflowInput<",
        "*>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0002H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/workflow/legacy/WorkflowInput$ReadOnly;",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "",
        "()V",
        "sendEvent",
        "",
        "event",
        "legacy-workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/workflow/legacy/WorkflowInput$ReadOnly;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowInput$ReadOnly;

    invoke-direct {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$ReadOnly;-><init>()V

    sput-object v0, Lcom/squareup/workflow/legacy/WorkflowInput$ReadOnly;->INSTANCE:Lcom/squareup/workflow/legacy/WorkflowInput$ReadOnly;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic sendEvent(Ljava/lang/Object;)V
    .locals 0

    .line 33
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowInput$ReadOnly;->sendEvent(Ljava/lang/Void;)V

    return-void
.end method

.method public sendEvent(Ljava/lang/Void;)V
    .locals 1

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
