.class public final Lcom/squareup/workflow/legacy/ScreenKt;
.super Ljava/lang/Object;
.source "Screen.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,161:1\n150#1,4:162\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u001a>\u0010\u0008\u001a\u0018\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u00020\u00040\u0003j\u0008\u0012\u0004\u0012\u0002H\u0001`\u0005\"\n\u0008\u0000\u0010\u0001\u0018\u0001*\u00020\u0002*\u0002H\u00012\u0008\u0008\u0002\u0010\t\u001a\u00020\nH\u0086\u0008\u00a2\u0006\u0002\u0010\u000b\u001a:\u0010\u000c\u001a\u0018\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u00020\u00040\rj\u0008\u0012\u0004\u0012\u0002H\u0001`\u000e\"\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0012\u0004\u0012\u0002H\u00010\u000f2\u0008\u0008\u0002\u0010\t\u001a\u00020\n\u001aX\u0010\u0010\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u00130\u00030\u0011\"\u0008\u0008\u0000\u0010\u0012*\u00020\u0014\"\u0008\u0008\u0001\u0010\u0013*\u00020\u0014*\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003j\u0002`\u00150\u00112\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u00130\r\u001a+\u0010\u0017\u001a\u0002H\u0018\"\u0008\u0008\u0000\u0010\u0018*\u00020\u0002*\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00040\u0003j\u0006\u0012\u0002\u0008\u0003`\u0005\u00a2\u0006\u0002\u0010\u0007\"5\u0010\u0000\u001a\u0002H\u0001\"\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0018\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u00020\u00040\u0003j\u0008\u0012\u0004\u0012\u0002H\u0001`\u00058F\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007*\u001a\u0010\u0019\"\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00032\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003*\u001a\u0010\u001a\"\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\r2\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\r*(\u0010\u001b\u001a\u0004\u0008\u0000\u0010\u0012\"\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u00020\u00040\u00032\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u00020\u00040\u0003*(\u0010\u001c\u001a\u0004\u0008\u0000\u0010\u0012\"\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u00020\u00040\r2\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u00020\u00040\r\u00a8\u0006\u001d"
    }
    d2 = {
        "unwrapV2Screen",
        "ScreenT",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "getUnwrapV2Screen",
        "(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;",
        "asLegacyScreen",
        "value",
        "",
        "(Lcom/squareup/workflow/legacy/V2Screen;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen;",
        "asLegacyScreenKey",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapperKey;",
        "Lkotlin/reflect/KClass;",
        "ofKeyType",
        "Lio/reactivex/Observable;",
        "D",
        "E",
        "",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "key",
        "unwrapAs",
        "T",
        "AnyScreen",
        "AnyScreenKey",
        "V2ScreenWrapper",
        "V2ScreenWrapperKey",
        "pure"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic asLegacyScreen(Lcom/squareup/workflow/legacy/V2Screen;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ScreenT::",
            "Lcom/squareup/workflow/legacy/V2Screen;",
            ">(TScreenT;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    const-string v0, "$this$asLegacyScreen"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    const/4 v1, 0x4

    const-string v2, "ScreenT"

    .line 151
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Lcom/squareup/workflow/legacy/V2Screen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p1

    .line 152
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 150
    invoke-direct {v0, p1, p0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v0
.end method

.method public static synthetic asLegacyScreen$default(Lcom/squareup/workflow/legacy/V2Screen;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const-string p1, ""

    :cond_0
    const-string p2, "$this$asLegacyScreen"

    .line 149
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "value"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    const/4 p3, 0x4

    const-string v0, "ScreenT"

    .line 163
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class p3, Lcom/squareup/workflow/legacy/V2Screen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, p1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p1

    .line 164
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 162
    invoke-direct {p2, p1, p0, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p2
.end method

.method public static final asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ScreenT::",
            "Lcom/squareup/workflow/legacy/V2Screen;",
            ">(",
            "Lkotlin/reflect/KClass<",
            "TScreenT;>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen$Key;"
        }
    .end annotation

    const-string v0, "$this$asLegacyScreenKey"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    new-instance v0, Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {p0}, Lkotlin/jvm/JvmClassMappingKt;->getJavaClass(Lkotlin/reflect/KClass;)Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    const-string v1, "java.name"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p0, p1}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static synthetic asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const-string p1, ""

    .line 145
    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p0

    return-object p0
.end method

.method public static final getUnwrapV2Screen(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ScreenT::",
            "Lcom/squareup/workflow/legacy/V2Screen;",
            ">(",
            "Lcom/squareup/workflow/legacy/Screen;",
            ")TScreenT;"
        }
    .end annotation

    const-string v0, "$this$unwrapV2Screen"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p0, Lcom/squareup/workflow/legacy/V2Screen;

    return-object p0
.end method

.method public static final ofKeyType(Lio/reactivex/Observable;Lcom/squareup/workflow/legacy/Screen$Key;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "TD;TE;>;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "TD;TE;>;>;"
        }
    .end annotation

    const-string v0, "$this$ofKeyType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    new-instance v0, Lcom/squareup/workflow/legacy/ScreenKt$ofKeyType$1;

    invoke-direct {v0, p1}, Lcom/squareup/workflow/legacy/ScreenKt$ofKeyType$1;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;)V

    check-cast v0, Lio/reactivex/functions/Predicate;

    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p0

    .line 125
    sget-object p1, Lcom/squareup/workflow/legacy/ScreenKt$ofKeyType$2;->INSTANCE:Lcom/squareup/workflow/legacy/ScreenKt$ofKeyType$2;

    check-cast p1, Lio/reactivex/functions/Function;

    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "filter { it.key == key }\u2026t as Screen<D, E>\n      }"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final unwrapAs(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/squareup/workflow/legacy/V2Screen;",
            ">(",
            "Lcom/squareup/workflow/legacy/Screen;",
            ")TT;"
        }
    .end annotation

    const-string v0, "$this$unwrapAs"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    if-eqz p0, :cond_0

    check-cast p0, Lcom/squareup/workflow/legacy/V2Screen;

    return-object p0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type T"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
