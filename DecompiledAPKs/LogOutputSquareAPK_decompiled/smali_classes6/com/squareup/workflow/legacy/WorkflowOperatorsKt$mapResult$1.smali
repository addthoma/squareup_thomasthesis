.class final Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$1;
.super Lkotlin/jvm/internal/Lambda;
.source "WorkflowOperators.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/WorkflowOperatorsKt;->mapResult(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Throwable;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0003\"\u0008\u0008\u0003\u0010\u0006*\u00020\u00032\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "S",
        "",
        "E",
        "O1",
        "O2",
        "cause",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_mapResult:Lcom/squareup/workflow/legacy/Workflow;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/Workflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$1;->$this_mapResult:Lcom/squareup/workflow/legacy/Workflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$1;->invoke(Ljava/lang/Throwable;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Throwable;)V
    .locals 4

    .line 135
    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$1;->$this_mapResult:Lcom/squareup/workflow/legacy/Workflow;

    .line 136
    instance-of v1, p1, Ljava/util/concurrent/CancellationException;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v3, p1

    check-cast v3, Ljava/util/concurrent/CancellationException;

    goto :goto_0

    :cond_0
    invoke-static {v2, p1}, Lkotlinx/coroutines/ExceptionsKt;->CancellationException(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/util/concurrent/CancellationException;

    move-result-object v3

    .line 135
    :goto_0
    invoke-interface {v0, v3}, Lcom/squareup/workflow/legacy/Workflow;->cancel(Ljava/util/concurrent/CancellationException;)V

    if-eqz v1, :cond_1

    .line 139
    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$1;->$this_mapResult:Lcom/squareup/workflow/legacy/Workflow;

    check-cast p1, Ljava/util/concurrent/CancellationException;

    invoke-interface {v0, p1}, Lcom/squareup/workflow/legacy/Workflow;->cancel(Ljava/util/concurrent/CancellationException;)V

    goto :goto_1

    :cond_1
    if-eqz p1, :cond_2

    .line 141
    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$1;->$this_mapResult:Lcom/squareup/workflow/legacy/Workflow;

    invoke-static {v2, p1}, Lkotlinx/coroutines/ExceptionsKt;->CancellationException(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/util/concurrent/CancellationException;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/workflow/legacy/Workflow;->cancel(Ljava/util/concurrent/CancellationException;)V

    :cond_2
    :goto_1
    return-void
.end method
