.class public final Lcom/squareup/workflow/legacy/rx2/WorkflowsKt;
.super Ljava/lang/Object;
.source "Workflows.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\">\u0010\u0000\u001a\n\u0012\u0006\u0008\u0001\u0012\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u0010\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u0002H\u00020\u00048FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u0005\u0010\u0006\u001a\u0004\u0008\u0007\u0010\u0008\">\u0010\t\u001a\n\u0012\u0006\u0008\u0001\u0012\u0002H\u000b0\n\"\u0008\u0008\u0000\u0010\u000b*\u00020\u0003*\u0010\u0012\u0004\u0012\u0002H\u000b\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00048FX\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u000c\u0010\u0006\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "result",
        "Lio/reactivex/Maybe;",
        "O",
        "",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "result$annotations",
        "(Lcom/squareup/workflow/legacy/Workflow;)V",
        "getResult",
        "(Lcom/squareup/workflow/legacy/Workflow;)Lio/reactivex/Maybe;",
        "state",
        "Lio/reactivex/Observable;",
        "S",
        "state$annotations",
        "getState",
        "(Lcom/squareup/workflow/legacy/Workflow;)Lio/reactivex/Observable;",
        "legacy-workflow-rx2"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getResult(Lcom/squareup/workflow/legacy/Workflow;)Lio/reactivex/Maybe;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "**+TO;>;)",
            "Lio/reactivex/Maybe<",
            "+TO;>;"
        }
    .end annotation

    const-string v0, "$this$result"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getUnconfined()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    check-cast v0, Lkotlin/coroutines/CoroutineContext;

    new-instance v1, Lcom/squareup/workflow/legacy/rx2/WorkflowsKt$result$1;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/workflow/legacy/rx2/WorkflowsKt$result$1;-><init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-static {v0, v1}, Lkotlinx/coroutines/rx2/RxMaybeKt;->rxMaybe(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;)Lio/reactivex/Maybe;

    move-result-object p0

    .line 65
    sget-object v0, Lcom/squareup/workflow/legacy/rx2/WorkflowsKt$result$2;->INSTANCE:Lcom/squareup/workflow/legacy/rx2/WorkflowsKt$result$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p0, v0}, Lio/reactivex/Maybe;->onErrorResumeNext(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p0

    const-string v0, "rxMaybe(Unconfined) { aw\u2026(error)\n        }\n      }"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getState(Lcom/squareup/workflow/legacy/Workflow;)Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "+TS;**>;)",
            "Lio/reactivex/Observable<",
            "+TS;>;"
        }
    .end annotation

    const-string v0, "$this$state"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getUnconfined()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    check-cast v0, Lkotlin/coroutines/CoroutineContext;

    new-instance v1, Lcom/squareup/workflow/legacy/rx2/WorkflowsKt$state$1;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/workflow/legacy/rx2/WorkflowsKt$state$1;-><init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-static {v0, v1}, Lkotlinx/coroutines/rx2/RxObservableKt;->rxObservable(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;)Lio/reactivex/Observable;

    move-result-object p0

    .line 42
    sget-object v0, Lcom/squareup/workflow/legacy/rx2/WorkflowsKt$state$2;->INSTANCE:Lcom/squareup/workflow/legacy/rx2/WorkflowsKt$state$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->onErrorResumeNext(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    const/4 v0, 0x1

    .line 52
    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p0

    .line 53
    invoke-virtual {p0}, Lio/reactivex/observables/ConnectableObservable;->refCount()Lio/reactivex/Observable;

    move-result-object p0

    const-string v0, "rxObservable(Unconfined)\u2026play(1)\n      .refCount()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static synthetic result$annotations(Lcom/squareup/workflow/legacy/Workflow;)V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    return-void
.end method

.method public static synthetic state$annotations(Lcom/squareup/workflow/legacy/Workflow;)V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    return-void
.end method
