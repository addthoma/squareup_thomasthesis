.class public final Lcom/squareup/workflow/legacy/rx2/ReactorKt;
.super Ljava/lang/Object;
.source "Reactor.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u001ao\u0010\u0000\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u00062\u0006\u0010\u0007\u001a\u0002H\u00022\u0006\u0010\u0008\u001a\u00020\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0007\u00a2\u0006\u0002\u0010\u000c\u001a\"\u0010\r\u001a\u00020\u000b*\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00062\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0002\u001aN\u0010\u000e\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u000f\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0006H\u0007\u00a8\u0006\u0010"
    }
    d2 = {
        "doLaunch",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "S",
        "E",
        "O",
        "",
        "Lcom/squareup/workflow/legacy/rx2/Reactor;",
        "initialState",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "name",
        "",
        "(Lcom/squareup/workflow/legacy/rx2/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Workflow;",
        "getWorkflowCoroutineName",
        "toCoroutineReactor",
        "Lcom/squareup/workflow/legacy/Reactor;",
        "legacy-workflow-rx2"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final doLaunch(Lcom/squareup/workflow/legacy/rx2/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/rx2/Reactor<",
            "TS;TE;+TO;>;TS;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "TS;TE;TO;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    const-string v0, "$this$doLaunch"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 242
    invoke-static {p0}, Lcom/squareup/workflow/legacy/rx2/ReactorKt;->toCoroutineReactor(Lcom/squareup/workflow/legacy/rx2/Reactor;)Lcom/squareup/workflow/legacy/Reactor;

    move-result-object v0

    .line 244
    new-instance v1, Lkotlinx/coroutines/CoroutineName;

    invoke-static {p0, p3}, Lcom/squareup/workflow/legacy/rx2/ReactorKt;->getWorkflowCoroutineName(Lcom/squareup/workflow/legacy/rx2/Reactor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Lkotlinx/coroutines/CoroutineName;-><init>(Ljava/lang/String;)V

    check-cast v1, Lkotlin/coroutines/CoroutineContext;

    .line 242
    invoke-static {v0, p1, p2, v1}, Lcom/squareup/workflow/legacy/ReactorKt;->doLaunch(Lcom/squareup/workflow/legacy/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/CoroutineContext;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic doLaunch$default(Lcom/squareup/workflow/legacy/rx2/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 241
    check-cast p3, Ljava/lang/String;

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/workflow/legacy/rx2/ReactorKt;->doLaunch(Lcom/squareup/workflow/legacy/rx2/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p0

    return-object p0
.end method

.method private static final getWorkflowCoroutineName(Lcom/squareup/workflow/legacy/rx2/Reactor;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/rx2/Reactor<",
            "***>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 270
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "workflow("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p0, 0x29

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final toCoroutineReactor(Lcom/squareup/workflow/legacy/rx2/Reactor;)Lcom/squareup/workflow/legacy/Reactor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/rx2/Reactor<",
            "TS;TE;+TO;>;)",
            "Lcom/squareup/workflow/legacy/Reactor<",
            "TS;TE;TO;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    const-string v0, "$this$toCoroutineReactor"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    new-instance v0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1;-><init>(Lcom/squareup/workflow/legacy/rx2/Reactor;)V

    check-cast v0, Lcom/squareup/workflow/legacy/Reactor;

    return-object v0
.end method
