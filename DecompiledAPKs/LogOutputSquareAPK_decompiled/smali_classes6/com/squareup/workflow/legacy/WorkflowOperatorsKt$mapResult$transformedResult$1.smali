.class final Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "WorkflowOperators.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/WorkflowOperatorsKt;->mapResult(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-TO2;>;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0002H\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0003\"\u0008\u0008\u0003\u0010\u0001*\u00020\u0003*\u00020\u0006H\u008a@\u00a2\u0006\u0004\u0008\u0007\u0010\u0008"
    }
    d2 = {
        "<anonymous>",
        "O2",
        "S",
        "",
        "E",
        "O1",
        "Lkotlinx/coroutines/CoroutineScope;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.workflow.legacy.WorkflowOperatorsKt$mapResult$transformedResult$1"
    f = "WorkflowOperators.kt"
    i = {
        0x0,
        0x1
    }
    l = {
        0x82,
        0x82
    }
    m = "invokeSuspend"
    n = {
        "$this$async",
        "$this$async"
    }
    s = {
        "L$0",
        "L$0"
    }
.end annotation


# instance fields
.field final synthetic $this_mapResult:Lcom/squareup/workflow/legacy/Workflow;

.field final synthetic $transform:Lkotlin/jvm/functions/Function2;

.field L$0:Ljava/lang/Object;

.field L$1:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->$this_mapResult:Lcom/squareup/workflow/legacy/Workflow;

    iput-object p2, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->$transform:Lkotlin/jvm/functions/Function2;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p3}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;

    iget-object v1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->$this_mapResult:Lcom/squareup/workflow/legacy/Workflow;

    iget-object v2, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->$transform:Lkotlin/jvm/functions/Function2;

    invoke-direct {v0, v1, v2, p2}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;-><init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 129
    iget v1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->label:I

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    if-eq v1, v3, :cond_1

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->L$0:Ljava/lang/Object;

    check-cast v0, Lkotlinx/coroutines/CoroutineScope;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    iget-object v1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->L$1:Ljava/lang/Object;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    iget-object v3, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->L$0:Ljava/lang/Object;

    check-cast v3, Lkotlinx/coroutines/CoroutineScope;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    .line 130
    iget-object v1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->$transform:Lkotlin/jvm/functions/Function2;

    iget-object v4, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->$this_mapResult:Lcom/squareup/workflow/legacy/Workflow;

    iput-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->L$0:Ljava/lang/Object;

    iput-object v1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->L$1:Ljava/lang/Object;

    iput v3, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->label:I

    invoke-interface {v4, p0}, Lcom/squareup/workflow/legacy/Workflow;->await(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v3

    if-ne v3, v0, :cond_3

    return-object v0

    :cond_3
    move-object v5, v3

    move-object v3, p1

    move-object p1, v5

    .line 129
    :goto_0
    iput-object v3, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->L$0:Ljava/lang/Object;

    iput v2, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;->label:I

    invoke-interface {v1, p1, p0}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_4

    return-object v0

    :cond_4
    :goto_1
    return-object p1
.end method
