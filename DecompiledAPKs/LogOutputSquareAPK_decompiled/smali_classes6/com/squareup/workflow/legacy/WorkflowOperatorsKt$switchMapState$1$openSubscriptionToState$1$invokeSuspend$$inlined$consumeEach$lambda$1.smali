.class final Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "WorkflowOperators.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lkotlin/Unit;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowOperators.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowOperators.kt\ncom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$1$1\n+ 2 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n*L\n1#1,152:1\n140#2,2:153\n*E\n*S KotlinDebug\n*F\n+ 1 WorkflowOperators.kt\ncom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$1$1\n*L\n108#1,2:153\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0003\"\u0008\u0008\u0003\u0010\u0006*\u00020\u0003*\u00020\u0007H\u008a@\u00a2\u0006\u0004\u0008\u0008\u0010\t\u00a8\u0006\n"
    }
    d2 = {
        "<anonymous>",
        "",
        "S1",
        "",
        "S2",
        "E",
        "O",
        "Lkotlinx/coroutines/CoroutineScope;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
        "com/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $downstreamChannel$inlined:Lkotlinx/coroutines/channels/SendChannel;

.field final synthetic $this_produce$inlined:Lkotlinx/coroutines/channels/ProducerScope;

.field final synthetic $transformerJob$inlined:Lkotlin/jvm/internal/Ref$ObjectRef;

.field final synthetic $upstreamState:Ljava/lang/Object;

.field L$0:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;

.field final synthetic this$0:Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;


# direct methods
.method constructor <init>(Ljava/lang/Object;Lkotlin/coroutines/Continuation;Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;Lkotlinx/coroutines/channels/ProducerScope;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlinx/coroutines/channels/SendChannel;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->$upstreamState:Ljava/lang/Object;

    iput-object p3, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->this$0:Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;

    iput-object p4, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->$this_produce$inlined:Lkotlinx/coroutines/channels/ProducerScope;

    iput-object p5, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->$transformerJob$inlined:Lkotlin/jvm/internal/Ref$ObjectRef;

    iput-object p6, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->$downstreamChannel$inlined:Lkotlinx/coroutines/channels/SendChannel;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;

    iget-object v2, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->$upstreamState:Ljava/lang/Object;

    iget-object v4, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->this$0:Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;

    iget-object v5, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->$this_produce$inlined:Lkotlinx/coroutines/channels/ProducerScope;

    iget-object v6, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->$transformerJob$inlined:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object v7, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->$downstreamChannel$inlined:Lkotlinx/coroutines/channels/SendChannel;

    move-object v1, v0

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;-><init>(Ljava/lang/Object;Lkotlin/coroutines/Continuation;Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;Lkotlinx/coroutines/channels/ProducerScope;Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlinx/coroutines/channels/SendChannel;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 99
    iget v1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->label:I

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    if-eq v1, v3, :cond_1

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->L$0:Ljava/lang/Object;

    check-cast v0, Lkotlinx/coroutines/CoroutineScope;

    :try_start_0
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 114
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 99
    :cond_1
    iget-object v1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->L$0:Ljava/lang/Object;

    check-cast v1, Lkotlinx/coroutines/CoroutineScope;

    :try_start_1
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    .line 101
    :try_start_2
    iget-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->this$0:Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1;->this$0:Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1;->$transform:Lkotlin/jvm/functions/Function3;

    iget-object v4, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->$upstreamState:Ljava/lang/Object;

    iput-object v1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->L$0:Ljava/lang/Object;

    iput v3, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->label:I

    invoke-interface {p1, v1, v4, p0}, Lkotlin/jvm/functions/Function3;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_3

    return-object v0

    .line 99
    :cond_3
    :goto_0
    check-cast p1, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v4, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->$downstreamChannel$inlined:Lkotlinx/coroutines/channels/SendChannel;

    iput-object v1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->L$0:Ljava/lang/Object;

    iput v2, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->label:I

    invoke-static {p1, v4, p0}, Lkotlinx/coroutines/channels/ChannelsKt;->toChannel(Lkotlinx/coroutines/channels/ReceiveChannel;Lkotlinx/coroutines/channels/SendChannel;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-ne p1, v0, :cond_6

    return-object v0

    :catchall_0
    move-exception p1

    .line 107
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$1$1$causeChain$1;->INSTANCE:Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$1$1$causeChain$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lkotlin/sequences/SequencesKt;->generateSequence(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    .line 153
    invoke-interface {p1}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/Throwable;

    .line 108
    instance-of v1, v1, Ljava/util/concurrent/CancellationException;

    xor-int/2addr v1, v3

    invoke-static {v1}, Lkotlin/coroutines/jvm/internal/Boxing;->boxBoolean(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    .line 154
    :goto_1
    check-cast v0, Ljava/lang/Throwable;

    if-nez v0, :cond_7

    .line 114
    :cond_6
    :goto_2
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1

    .line 110
    :cond_7
    iget-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$invokeSuspend$$inlined$consumeEach$lambda$1;->$downstreamChannel$inlined:Lkotlinx/coroutines/channels/SendChannel;

    invoke-interface {p1, v0}, Lkotlinx/coroutines/channels/SendChannel;->close(Ljava/lang/Throwable;)Z

    .line 111
    throw v0
.end method
