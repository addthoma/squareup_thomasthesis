.class public final Lcom/squareup/workflow/ui/ViewRegistryKt;
.super Ljava/lang/Object;
.source "ViewRegistry.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u001a\u0006\u0010\u0004\u001a\u00020\u0001\u001a\'\u0010\u0004\u001a\u00020\u00012\u001a\u0010\u0005\u001a\u000e\u0012\n\u0008\u0001\u0012\u0006\u0012\u0002\u0008\u00030\u00070\u0006\"\u0006\u0012\u0002\u0008\u00030\u0007\u00a2\u0006\u0002\u0010\u0008\u001a\u001f\u0010\u0004\u001a\u00020\u00012\u0012\u0010\t\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00010\u0006\"\u00020\u0001\u00a2\u0006\u0002\u0010\n\u001a1\u0010\u000b\u001a\u00020\u000c\"\u0008\u0008\u0000\u0010\r*\u00020\u000e*\u00020\u00012\u0006\u0010\u000f\u001a\u0002H\r2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014\u001a\u0019\u0010\u0015\u001a\u00020\u0001*\u00020\u00012\n\u0010\u0016\u001a\u0006\u0012\u0002\u0008\u00030\u0007H\u0086\u0002\u001a\u0015\u0010\u0015\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0017\u001a\u00020\u0001H\u0086\u0002\"\u0014\u0010\u0000\u001a\u00020\u0001X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0003\u00a8\u0006\u0018"
    }
    d2 = {
        "defaultViewBindings",
        "Lcom/squareup/workflow/ui/ViewRegistry;",
        "getDefaultViewBindings",
        "()Lcom/squareup/workflow/ui/ViewRegistry;",
        "ViewRegistry",
        "bindings",
        "",
        "Lcom/squareup/workflow/ui/ViewBinding;",
        "([Lcom/squareup/workflow/ui/ViewBinding;)Lcom/squareup/workflow/ui/ViewRegistry;",
        "registries",
        "([Lcom/squareup/workflow/ui/ViewRegistry;)Lcom/squareup/workflow/ui/ViewRegistry;",
        "buildView",
        "Landroid/view/View;",
        "RenderingT",
        "",
        "initialRendering",
        "initialContainerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "container",
        "Landroid/view/ViewGroup;",
        "(Lcom/squareup/workflow/ui/ViewRegistry;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Landroid/view/ViewGroup;)Landroid/view/View;",
        "plus",
        "binding",
        "other",
        "core-android_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final defaultViewBindings:Lcom/squareup/workflow/ui/ViewRegistry;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/workflow/ui/ViewBinding;

    .line 27
    sget-object v1, Lcom/squareup/workflow/ui/NamedBinding;->INSTANCE:Lcom/squareup/workflow/ui/NamedBinding;

    check-cast v1, Lcom/squareup/workflow/ui/ViewBinding;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/workflow/ui/ViewRegistryKt;->ViewRegistry([Lcom/squareup/workflow/ui/ViewBinding;)Lcom/squareup/workflow/ui/ViewRegistry;

    move-result-object v0

    sput-object v0, Lcom/squareup/workflow/ui/ViewRegistryKt;->defaultViewBindings:Lcom/squareup/workflow/ui/ViewRegistry;

    return-void
.end method

.method public static final ViewRegistry()Lcom/squareup/workflow/ui/ViewRegistry;
    .locals 2

    .line 104
    new-instance v0, Lcom/squareup/workflow/ui/BindingViewRegistry;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/squareup/workflow/ui/ViewBinding;

    invoke-direct {v0, v1}, Lcom/squareup/workflow/ui/BindingViewRegistry;-><init>([Lcom/squareup/workflow/ui/ViewBinding;)V

    check-cast v0, Lcom/squareup/workflow/ui/ViewRegistry;

    return-object v0
.end method

.method public static final varargs ViewRegistry([Lcom/squareup/workflow/ui/ViewBinding;)Lcom/squareup/workflow/ui/ViewRegistry;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/squareup/workflow/ui/ViewBinding<",
            "*>;)",
            "Lcom/squareup/workflow/ui/ViewRegistry;"
        }
    .end annotation

    const-string v0, "bindings"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    new-instance v0, Lcom/squareup/workflow/ui/BindingViewRegistry;

    array-length v1, p0

    invoke-static {p0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Lcom/squareup/workflow/ui/ViewBinding;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/ui/BindingViewRegistry;-><init>([Lcom/squareup/workflow/ui/ViewBinding;)V

    check-cast v0, Lcom/squareup/workflow/ui/ViewRegistry;

    return-object v0
.end method

.method public static final varargs ViewRegistry([Lcom/squareup/workflow/ui/ViewRegistry;)Lcom/squareup/workflow/ui/ViewRegistry;
    .locals 2

    const-string v0, "registries"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    new-instance v0, Lcom/squareup/workflow/ui/CompositeViewRegistry;

    array-length v1, p0

    invoke-static {p0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Lcom/squareup/workflow/ui/ViewRegistry;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/ui/CompositeViewRegistry;-><init>([Lcom/squareup/workflow/ui/ViewRegistry;)V

    check-cast v0, Lcom/squareup/workflow/ui/ViewRegistry;

    return-object v0
.end method

.method public static final buildView(Lcom/squareup/workflow/ui/ViewRegistry;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/ui/ViewRegistry;",
            "TRenderingT;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Landroid/view/ViewGroup;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "$this$buildView"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialRendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialContainerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "container.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1, p2, v0, p3}, Lcom/squareup/workflow/ui/ViewRegistry;->buildView(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final getDefaultViewBindings()Lcom/squareup/workflow/ui/ViewRegistry;
    .locals 1

    .line 27
    sget-object v0, Lcom/squareup/workflow/ui/ViewRegistryKt;->defaultViewBindings:Lcom/squareup/workflow/ui/ViewRegistry;

    return-object v0
.end method

.method public static final plus(Lcom/squareup/workflow/ui/ViewRegistry;Lcom/squareup/workflow/ui/ViewBinding;)Lcom/squareup/workflow/ui/ViewRegistry;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/ui/ViewRegistry;",
            "Lcom/squareup/workflow/ui/ViewBinding<",
            "*>;)",
            "Lcom/squareup/workflow/ui/ViewRegistry;"
        }
    .end annotation

    const-string v0, "$this$plus"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "binding"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/workflow/ui/ViewBinding;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 123
    invoke-static {v0}, Lcom/squareup/workflow/ui/ViewRegistryKt;->ViewRegistry([Lcom/squareup/workflow/ui/ViewBinding;)Lcom/squareup/workflow/ui/ViewRegistry;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/workflow/ui/ViewRegistryKt;->plus(Lcom/squareup/workflow/ui/ViewRegistry;Lcom/squareup/workflow/ui/ViewRegistry;)Lcom/squareup/workflow/ui/ViewRegistry;

    move-result-object p0

    return-object p0
.end method

.method public static final plus(Lcom/squareup/workflow/ui/ViewRegistry;Lcom/squareup/workflow/ui/ViewRegistry;)Lcom/squareup/workflow/ui/ViewRegistry;
    .locals 2

    const-string v0, "$this$plus"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/workflow/ui/ViewRegistry;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 p0, 0x1

    aput-object p1, v0, p0

    .line 125
    invoke-static {v0}, Lcom/squareup/workflow/ui/ViewRegistryKt;->ViewRegistry([Lcom/squareup/workflow/ui/ViewRegistry;)Lcom/squareup/workflow/ui/ViewRegistry;

    move-result-object p0

    return-object p0
.end method
