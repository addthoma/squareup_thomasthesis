.class public final Lcom/squareup/workflow/ui/LayoutRunner$Companion;
.super Ljava/lang/Object;
.source "LayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/ui/LayoutRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LayoutRunner.kt\ncom/squareup/workflow/ui/LayoutRunner$Companion\n*L\n1#1,111:1\n93#1:112\n*E\n*S KotlinDebug\n*F\n+ 1 LayoutRunner.kt\ncom/squareup/workflow/ui/LayoutRunner$Companion\n*L\n101#1:112\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002JA\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u0004\"\n\u0008\u0001\u0010\u0005\u0018\u0001*\u00020\u00012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u00072\u001a\u0008\u0008\u0010\u0008\u001a\u0014\u0012\u0004\u0012\u00020\n\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00050\u000b0\tH\u0086\u0008J%\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u0004\"\n\u0008\u0001\u0010\u0005\u0018\u0001*\u00020\u00012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007H\u0086\u0008\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/workflow/ui/LayoutRunner$Companion;",
        "",
        "()V",
        "bind",
        "Lcom/squareup/workflow/ui/ViewBinding;",
        "RenderingT",
        "layoutId",
        "",
        "constructor",
        "Lkotlin/Function1;",
        "Landroid/view/View;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "bindNoRunner",
        "core-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/workflow/ui/LayoutRunner$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 85
    new-instance v0, Lcom/squareup/workflow/ui/LayoutRunner$Companion;

    invoke-direct {v0}, Lcom/squareup/workflow/ui/LayoutRunner$Companion;-><init>()V

    sput-object v0, Lcom/squareup/workflow/ui/LayoutRunner$Companion;->$$INSTANCE:Lcom/squareup/workflow/ui/LayoutRunner$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic bind(ILkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/ui/ViewBinding;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RenderingT:",
            "Ljava/lang/Object;",
            ">(I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "+",
            "Lcom/squareup/workflow/ui/LayoutRunner<",
            "TRenderingT;>;>;)",
            "Lcom/squareup/workflow/ui/ViewBinding<",
            "TRenderingT;>;"
        }
    .end annotation

    const-string v0, "constructor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    new-instance v0, Lcom/squareup/workflow/ui/LayoutRunner$Binding;

    const/4 v1, 0x4

    const-string v2, "RenderingT"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Ljava/lang/Object;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, Lcom/squareup/workflow/ui/LayoutRunner$Binding;-><init>(Lkotlin/reflect/KClass;ILkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/ui/ViewBinding;

    return-object v0
.end method

.method public final synthetic bindNoRunner(I)Lcom/squareup/workflow/ui/ViewBinding;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RenderingT:",
            "Ljava/lang/Object;",
            ">(I)",
            "Lcom/squareup/workflow/ui/ViewBinding<",
            "TRenderingT;>;"
        }
    .end annotation

    .line 101
    move-object v0, p0

    check-cast v0, Lcom/squareup/workflow/ui/LayoutRunner$Companion;

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v0, Lcom/squareup/workflow/ui/LayoutRunner$Companion$bindNoRunner$1;->INSTANCE:Lcom/squareup/workflow/ui/LayoutRunner$Companion$bindNoRunner$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 112
    new-instance v1, Lcom/squareup/workflow/ui/LayoutRunner$Binding;

    const/4 v2, 0x4

    const-string v3, "RenderingT"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v2, Ljava/lang/Object;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v1, v2, p1, v0}, Lcom/squareup/workflow/ui/LayoutRunner$Binding;-><init>(Lkotlin/reflect/KClass;ILkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/workflow/ui/ViewBinding;

    return-object v1
.end method
