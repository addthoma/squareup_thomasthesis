.class public final Lcom/squareup/workflow/ui/HandlesBack$Helper;
.super Ljava/lang/Object;
.source "HandlesBack.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/ui/HandlesBack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Helper"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHandlesBack.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HandlesBack.kt\ncom/squareup/workflow/ui/HandlesBack$Helper\n*L\n1#1,92:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0014\u0010\u0007\u001a\u00020\u0008*\u00020\u00062\u0006\u0010\t\u001a\u00020\nH\u0007J\u0014\u0010\u000b\u001a\u00020\u0008*\u00020\u00062\u0006\u0010\u000c\u001a\u00020\rH\u0007\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/workflow/ui/HandlesBack$Helper;",
        "",
        "()V",
        "onBackPressed",
        "",
        "view",
        "Landroid/view/View;",
        "setBackHandler",
        "",
        "handler",
        "Ljava/lang/Runnable;",
        "setConditionalBackHandler",
        "handlesBack",
        "Lcom/squareup/workflow/ui/HandlesBack;",
        "container_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/workflow/ui/HandlesBack$Helper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/workflow/ui/HandlesBack$Helper;

    invoke-direct {v0}, Lcom/squareup/workflow/ui/HandlesBack$Helper;-><init>()V

    sput-object v0, Lcom/squareup/workflow/ui/HandlesBack$Helper;->INSTANCE:Lcom/squareup/workflow/ui/HandlesBack$Helper;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final onBackPressed(Landroid/view/View;)Z
    .locals 4
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "view"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    instance-of v0, p0, Lcom/squareup/workflow/ui/WorkflowViewStub;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/squareup/workflow/ui/WorkflowViewStub;

    invoke-virtual {p0}, Lcom/squareup/workflow/ui/WorkflowViewStub;->getActual()Landroid/view/View;

    move-result-object p0

    .line 65
    :cond_0
    sget v0, Lcom/squareup/container/R$id;->workflow_back_handler:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/ui/HandlesBack;

    .line 67
    instance-of v1, p0, Lcom/squareup/workflow/ui/HandlesBack;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_3

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 69
    move-object v0, p0

    check-cast v0, Lcom/squareup/workflow/ui/HandlesBack;

    goto :goto_1

    .line 68
    :cond_2
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Should be impossible to have both tag and interface"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    :cond_3
    :goto_1
    if-eqz v0, :cond_4

    .line 72
    invoke-interface {v0}, Lcom/squareup/workflow/ui/HandlesBack;->onBackPressed()Z

    move-result p0

    if-eqz p0, :cond_4

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    :goto_2
    return v2
.end method

.method public static final setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "$this$setBackHandler"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v0, Lcom/squareup/workflow/ui/HandlesBack$Helper$setBackHandler$1;

    invoke-direct {v0, p1}, Lcom/squareup/workflow/ui/HandlesBack$Helper$setBackHandler$1;-><init>(Ljava/lang/Runnable;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p0, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static final setConditionalBackHandler(Landroid/view/View;Lcom/squareup/workflow/ui/HandlesBack;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "$this$setConditionalBackHandler"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handlesBack"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    instance-of v0, p0, Lcom/squareup/workflow/ui/HandlesBack;

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 49
    sget v0, Lcom/squareup/container/R$id;->workflow_back_handler:I

    invoke-virtual {p0, v0, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    return-void

    .line 45
    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p1, "Cannot set back handlers on views that implement "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    const-class p1, Lcom/squareup/workflow/ui/HandlesBack;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 44
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
