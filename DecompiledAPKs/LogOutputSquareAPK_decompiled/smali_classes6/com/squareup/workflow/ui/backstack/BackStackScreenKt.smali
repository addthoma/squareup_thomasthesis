.class public final Lcom/squareup/workflow/ui/backstack/BackStackScreenKt;
.super Ljava/lang/Object;
.source "BackStackScreen.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBackStackScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BackStackScreen.kt\ncom/squareup/workflow/ui/backstack/BackStackScreenKt\n*L\n1#1,92:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0008\u0002\u001a \u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u0008\u0012\u0004\u0012\u0002H\u00020\u0004\u001a\"\u0010\u0005\u001a\n\u0012\u0004\u0012\u0002H\u0002\u0018\u00010\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u0008\u0012\u0004\u0012\u0002H\u00020\u0004\u00a8\u0006\u0006"
    }
    d2 = {
        "toBackStackScreen",
        "Lcom/squareup/workflow/ui/backstack/BackStackScreen;",
        "T",
        "",
        "",
        "toBackStackScreenOrNull",
        "backstack-common"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toBackStackScreen(Ljava/util/List;)Lcom/squareup/workflow/ui/backstack/BackStackScreen;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List<",
            "+TT;>;)",
            "Lcom/squareup/workflow/ui/backstack/BackStackScreen<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$toBackStackScreen"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    move-object v0, p0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 90
    new-instance v0, Lcom/squareup/workflow/ui/backstack/BackStackScreen;

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {p0, v1, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p0

    invoke-direct {v0, v2, p0}, Lcom/squareup/workflow/ui/backstack/BackStackScreen;-><init>(Ljava/lang/Object;Ljava/util/List;)V

    return-object v0

    .line 89
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Failed requirement."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final toBackStackScreenOrNull(Ljava/util/List;)Lcom/squareup/workflow/ui/backstack/BackStackScreen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List<",
            "+TT;>;)",
            "Lcom/squareup/workflow/ui/backstack/BackStackScreen<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$toBackStackScreenOrNull"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 85
    :cond_0
    invoke-static {p0}, Lcom/squareup/workflow/ui/backstack/BackStackScreenKt;->toBackStackScreen(Ljava/util/List;)Lcom/squareup/workflow/ui/backstack/BackStackScreen;

    move-result-object p0

    :goto_0
    return-object p0
.end method
