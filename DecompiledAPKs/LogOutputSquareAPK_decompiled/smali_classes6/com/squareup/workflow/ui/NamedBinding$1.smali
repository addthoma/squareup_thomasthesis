.class final Lcom/squareup/workflow/ui/NamedBinding$1;
.super Lkotlin/jvm/internal/Lambda;
.source "NamedBinding.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function4;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/ui/NamedBinding;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function4<",
        "Lcom/squareup/workflow/ui/Named<",
        "*>;",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Landroid/content/Context;",
        "Landroid/view/ViewGroup;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNamedBinding.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NamedBinding.kt\ncom/squareup/workflow/ui/NamedBinding$1\n*L\n1#1,44:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\n\u0010\u0002\u001a\u0006\u0012\u0002\u0008\u00030\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tH\n\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "Landroid/view/View;",
        "initialRendering",
        "Lcom/squareup/workflow/ui/Named;",
        "initialHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "contextForNewView",
        "Landroid/content/Context;",
        "container",
        "Landroid/view/ViewGroup;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/workflow/ui/NamedBinding$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/workflow/ui/NamedBinding$1;

    invoke-direct {v0}, Lcom/squareup/workflow/ui/NamedBinding$1;-><init>()V

    sput-object v0, Lcom/squareup/workflow/ui/NamedBinding$1;->INSTANCE:Lcom/squareup/workflow/ui/NamedBinding$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/workflow/ui/Named;Lcom/squareup/workflow/ui/ContainerHints;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/ui/Named<",
            "*>;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "initialRendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contextForNewView"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget-object v0, Lcom/squareup/workflow/ui/ViewRegistry;->Companion:Lcom/squareup/workflow/ui/ViewRegistry$Companion;

    check-cast v0, Lcom/squareup/workflow/ui/ContainerHintKey;

    invoke-virtual {p2, v0}, Lcom/squareup/workflow/ui/ContainerHints;->get(Lcom/squareup/workflow/ui/ContainerHintKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/ui/ViewRegistry;

    .line 25
    invoke-virtual {p1}, Lcom/squareup/workflow/ui/Named;->getWrapped()Ljava/lang/Object;

    move-result-object v1

    .line 24
    invoke-interface {v0, v1, p2, p3, p4}, Lcom/squareup/workflow/ui/ViewRegistry;->buildView(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    .line 36
    invoke-static {p3}, Lcom/squareup/workflow/ui/ViewShowRenderingKt;->getShowRendering(Landroid/view/View;)Lkotlin/jvm/functions/Function2;

    move-result-object p4

    if-nez p4, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 38
    :cond_0
    new-instance v0, Lcom/squareup/workflow/ui/NamedBinding$1$1$1;

    invoke-direct {v0, p4}, Lcom/squareup/workflow/ui/NamedBinding$1$1$1;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {p3, p1, p2, v0}, Lcom/squareup/workflow/ui/ViewShowRenderingKt;->bindShowRendering(Landroid/view/View;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function2;)V

    return-object p3
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/workflow/ui/Named;

    check-cast p2, Lcom/squareup/workflow/ui/ContainerHints;

    check-cast p3, Landroid/content/Context;

    check-cast p4, Landroid/view/ViewGroup;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/workflow/ui/NamedBinding$1;->invoke(Lcom/squareup/workflow/ui/Named;Lcom/squareup/workflow/ui/ContainerHints;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method
