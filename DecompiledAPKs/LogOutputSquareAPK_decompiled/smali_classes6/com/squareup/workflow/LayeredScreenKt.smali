.class public final Lcom/squareup/workflow/LayeredScreenKt;
.super Ljava/lang/Object;
.source "LayeredScreen.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLayeredScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LayeredScreen.kt\ncom/squareup/workflow/LayeredScreenKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,87:1\n1360#2:88\n1429#2,3:89\n1360#2:92\n1429#2,3:93\n*E\n*S KotlinDebug\n*F\n+ 1 LayeredScreen.kt\ncom/squareup/workflow/LayeredScreenKt\n*L\n41#1:88\n41#1,3:89\n42#1:92\n42#1,3:93\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0010\u000b\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u001ad\u0010\u0007\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u0008*(\u0012\u0006\u0008\u0001\u0012\u0002H\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003j\u0002`\u00040\u0002j\n\u0012\u0006\u0008\u0001\u0012\u0002H\u0008`\u00052,\u0010\t\u001a(\u0012\u0006\u0008\u0001\u0012\u0002H\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003j\u0002`\u00040\u0002j\n\u0012\u0006\u0008\u0001\u0012\u0002H\u0008`\u0005\u001aD\u0010\n\u001a*\u0012&\u0012$\u0012\u0004\u0012\u00020\u000c\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003j\u0002`\u00040\u0002j\u0008\u0012\u0004\u0012\u00020\u000c`\u00050\u000b*\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003j\u0002`\u00040\u000b\u001a8\u0010\r\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003j\u0002`\u0004*$\u0012\u0004\u0012\u00020\u000c\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003j\u0002`\u00040\u0002j\u0008\u0012\u0004\u0012\u00020\u000c`\u0005\u001a\\\u0010\u000e\u001a$\u0012\u0004\u0012\u0002H\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003j\u0002`\u00040\u0002j\u0008\u0012\u0004\u0012\u0002H\u0008`\u0005\"\u0004\u0008\u0000\u0010\u0008*$\u0012\u0004\u0012\u0002H\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003j\u0002`\u00040\u0002j\u0008\u0012\u0004\u0012\u0002H\u0008`\u00052\u0006\u0010\u000f\u001a\u00020\u0001\"3\u0010\u0000\u001a\u00020\u0001* \u0012\u0002\u0008\u0003\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003j\u0002`\u00040\u0002j\u0006\u0012\u0002\u0008\u0003`\u00058F\u00a2\u0006\u0006\u001a\u0004\u0008\u0000\u0010\u0006*4\u0010\u0010\u001a\u0004\u0008\u0000\u0010\u0008\"\u000e\u0012\u0004\u0012\u0002H\u0008\u0012\u0004\u0012\u0002`\u00040\u00022\u001a\u0012\u0004\u0012\u0002H\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003j\u0002`\u00040\u0002\u00a8\u0006\u0011"
    }
    d2 = {
        "isPersistent",
        "",
        "",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "(Ljava/util/Map;)Z",
        "matches",
        "L",
        "other",
        "toMainAndModal",
        "Lcom/squareup/workflow/ScreenState;",
        "Lcom/squareup/workflow/MainAndModal;",
        "top",
        "withPersistence",
        "persistent",
        "LayeredScreen",
        "pure"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final isPersistent(Ljava/util/Map;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "*+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;)Z"
        }
    .end annotation

    const-string v0, "$this$isPersistent"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    instance-of p0, p0, Lcom/squareup/workflow/TransientLayeredScreen;

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method

.method public static final matches(Ljava/util/Map;Ljava/util/Map;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<",
            "L:Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map<",
            "+T",
            "L;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;",
            "Ljava/util/Map<",
            "+T",
            "L;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;)Z"
        }
    .end annotation

    const-string v0, "$this$matches"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 89
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 90
    check-cast v2, Ljava/util/Map$Entry;

    .line 41
    new-instance v3, Lkotlin/Pair;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/workflow/legacy/Screen;

    iget-object v2, v2, Lcom/squareup/workflow/legacy/Screen;->key:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-direct {v3, v4, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 91
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 42
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 92
    new-instance p1, Ljava/util/ArrayList;

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 93
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 94
    check-cast v1, Ljava/util/Map$Entry;

    .line 42
    new-instance v2, Lkotlin/Pair;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/legacy/Screen;

    iget-object v1, v1, Lcom/squareup/workflow/legacy/Screen;->key:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-direct {v2, v3, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 95
    :cond_1
    check-cast p1, Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method public static final toMainAndModal(Lcom/squareup/workflow/ScreenState;)Lcom/squareup/workflow/ScreenState;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/ScreenState<",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;)",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    const-string v0, "$this$toMainAndModal"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    new-instance v0, Lcom/squareup/workflow/ScreenState;

    sget-object v1, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    iget-object v2, p0, Lcom/squareup/workflow/ScreenState;->screen:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {v1, v2}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v1

    iget-object p0, p0, Lcom/squareup/workflow/ScreenState;->snapshot:Lcom/squareup/workflow/Snapshot;

    invoke-direct {v0, v1, p0}, Lcom/squareup/workflow/ScreenState;-><init>(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)V

    return-object v0
.end method

.method public static final top(Ljava/util/Map;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    const-string v0, "$this$top"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    sget-object v0, Lcom/squareup/workflow/MainAndModal;->MODAL:Lcom/squareup/workflow/MainAndModal;

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/Screen;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/workflow/MainAndModal;->MAIN:Lcom/squareup/workflow/MainAndModal;

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    move-object v0, p0

    check-cast v0, Lcom/squareup/workflow/legacy/Screen;

    :goto_0
    if-eqz v0, :cond_1

    return-object v0

    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final withPersistence(Ljava/util/Map;Z)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<",
            "L:Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map<",
            "T",
            "L;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;Z)",
            "Ljava/util/Map<",
            "T",
            "L;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "$this$withPersistence"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    goto :goto_0

    .line 19
    :cond_0
    new-instance p1, Lcom/squareup/workflow/TransientLayeredScreen;

    invoke-direct {p1, p0}, Lcom/squareup/workflow/TransientLayeredScreen;-><init>(Ljava/util/Map;)V

    move-object p0, p1

    check-cast p0, Ljava/util/Map;

    :goto_0
    return-object p0
.end method
