.class public interface abstract Lcom/squareup/url/InvoiceShareUrlLauncher;
.super Ljava/lang/Object;
.source "InvoiceShareUrlLauncher.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/url/InvoiceShareUrlLauncher$NoOp;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001:\u0001\u000eJ\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J \u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH&J\u0010\u0010\r\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/url/InvoiceShareUrlLauncher;",
        "",
        "dropActivity",
        "",
        "activity",
        "Landroid/app/Activity;",
        "shareUrl",
        "invoiceIdPair",
        "Lcom/squareup/protos/client/IdPair;",
        "email",
        "",
        "urlType",
        "Lcom/squareup/url/UrlType;",
        "takeActivity",
        "NoOp",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract dropActivity(Landroid/app/Activity;)V
.end method

.method public abstract shareUrl(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lcom/squareup/url/UrlType;)V
.end method

.method public abstract takeActivity(Landroid/app/Activity;)V
.end method
