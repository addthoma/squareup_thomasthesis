.class Lcom/starmicronics/starmgsio/u$b;
.super Lcom/starmicronics/starmgsio/n;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starmgsio/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "b"
.end annotation


# instance fields
.field private b:Lcom/starmicronics/starmgsio/ScaleSetting;

.field final synthetic c:Lcom/starmicronics/starmgsio/u;


# direct methods
.method constructor <init>(Lcom/starmicronics/starmgsio/u;Lcom/starmicronics/starmgsio/ScaleSetting;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/u$b;->c:Lcom/starmicronics/starmgsio/u;

    invoke-direct {p0}, Lcom/starmicronics/starmgsio/n;-><init>()V

    iput-object p2, p0, Lcom/starmicronics/starmgsio/u$b;->b:Lcom/starmicronics/starmgsio/ScaleSetting;

    return-void
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/u$b;)Lcom/starmicronics/starmgsio/ScaleSetting;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/u$b;->b:Lcom/starmicronics/starmgsio/ScaleSetting;

    return-object p0
.end method


# virtual methods
.method public run()V
    .locals 11

    iget-object v0, p0, Lcom/starmicronics/starmgsio/u$b;->c:Lcom/starmicronics/starmgsio/u;

    invoke-static {v0}, Lcom/starmicronics/starmgsio/u;->a(Lcom/starmicronics/starmgsio/u;)Lcom/starmicronics/starmgsio/p;

    move-result-object v0

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/starmicronics/starmgsio/u$b;->c:Lcom/starmicronics/starmgsio/u;

    invoke-static {v1}, Lcom/starmicronics/starmgsio/u;->a(Lcom/starmicronics/starmgsio/u;)Lcom/starmicronics/starmgsio/p;

    move-result-object v1

    invoke-interface {v1}, Lcom/starmicronics/starmgsio/p;->b()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/starmicronics/starmgsio/v;

    invoke-direct {v2, p0}, Lcom/starmicronics/starmgsio/v;-><init>(Lcom/starmicronics/starmgsio/u$b;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    monitor-exit v0

    return-void

    :cond_0
    new-instance v1, Lcom/starmicronics/starmgsio/D;

    invoke-direct {v1}, Lcom/starmicronics/starmgsio/D;-><init>()V

    iget-object v2, p0, Lcom/starmicronics/starmgsio/u$b;->b:Lcom/starmicronics/starmgsio/ScaleSetting;

    invoke-virtual {v1, v2}, Lcom/starmicronics/starmgsio/D;->a(Lcom/starmicronics/starmgsio/ScaleSetting;)Lcom/starmicronics/starmgsio/D;

    move-result-object v1

    invoke-virtual {v1}, Lcom/starmicronics/starmgsio/D;->b()[B

    move-result-object v1

    iget-object v2, p0, Lcom/starmicronics/starmgsio/u$b;->c:Lcom/starmicronics/starmgsio/u;

    invoke-static {v2}, Lcom/starmicronics/starmgsio/u;->a(Lcom/starmicronics/starmgsio/u;)Lcom/starmicronics/starmgsio/p;

    move-result-object v2

    array-length v3, v1

    const/16 v4, 0x2710

    const/4 v5, 0x0

    invoke-interface {v2, v1, v5, v3, v4}, Lcom/starmicronics/starmgsio/p;->b([BIII)I

    move-result v1

    if-gez v1, :cond_1

    monitor-exit v0

    return-void

    :cond_1
    const/16 v1, 0x80

    new-array v1, v1, [B

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-wide v3, v2

    const/4 v2, 0x0

    :cond_2
    iget-boolean v6, p0, Lcom/starmicronics/starmgsio/n;->a:Z

    if-nez v6, :cond_5

    array-length v6, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge v2, v6, :cond_5

    const-wide/16 v6, 0x19

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v6, p0, Lcom/starmicronics/starmgsio/u$b;->c:Lcom/starmicronics/starmgsio/u;

    invoke-static {v6}, Lcom/starmicronics/starmgsio/u;->a(Lcom/starmicronics/starmgsio/u;)Lcom/starmicronics/starmgsio/p;

    move-result-object v6

    array-length v7, v1

    const/16 v8, 0x32

    invoke-interface {v6, v1, v2, v7, v8}, Lcom/starmicronics/starmgsio/p;->a([BIII)I

    move-result v6

    if-lez v6, :cond_4

    add-int/2addr v2, v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    new-instance v6, Lcom/starmicronics/starmgsio/H$a;

    invoke-direct {v6}, Lcom/starmicronics/starmgsio/H$a;-><init>()V

    new-array v7, v2, [B

    array-length v8, v7

    invoke-static {v1, v5, v7, v5, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {v6, v7}, Lcom/starmicronics/starmgsio/H$a;->a([B)V

    invoke-virtual {v6}, Lcom/starmicronics/starmgsio/H$a;->a()Lcom/starmicronics/starmgsio/H;

    move-result-object v7

    iget-object v6, v6, Lcom/starmicronics/starmgsio/H$a;->b:[B

    if-eqz v7, :cond_2

    if-eqz v6, :cond_2

    invoke-virtual {v7}, Lcom/starmicronics/starmgsio/H;->a()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0

    :cond_3
    const/16 v5, 0x67

    :goto_0
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/starmicronics/starmgsio/x;

    invoke-direct {v2, p0, v5}, Lcom/starmicronics/starmgsio/x;-><init>(Lcom/starmicronics/starmgsio/u$b;I)V

    :goto_1
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    :cond_4
    const-wide/16 v6, 0x2710

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v3

    cmp-long v10, v6, v8

    if-gez v10, :cond_2

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/starmicronics/starmgsio/w;

    invoke-direct {v2, p0}, Lcom/starmicronics/starmgsio/w;-><init>(Lcom/starmicronics/starmgsio/u$b;)V

    goto :goto_1

    :catch_0
    :cond_5
    :goto_2
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
