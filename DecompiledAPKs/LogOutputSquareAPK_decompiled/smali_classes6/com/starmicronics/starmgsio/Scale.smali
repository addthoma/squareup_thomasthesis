.class public abstract Lcom/starmicronics/starmgsio/Scale;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final CONNECT_ALREADY_CONNECTED:I = 0x65

.field public static final CONNECT_NOT_AVAILABLE:I = 0x64

.field public static final CONNECT_NOT_GRANTED_PERMISSION:I = 0x6b

.field public static final CONNECT_NOT_SUPPORTED:I = 0x6a

.field public static final CONNECT_NOW_CONNECTING:I = 0x6c

.field public static final CONNECT_SUCCESS:I = 0x0

.field public static final CONNECT_TIMEOUT:I = 0x69

.field public static final CONNECT_UNEXPECTED_ERROR:I = 0xc8

.field public static final DISCONNECT_NOT_CONNECTED:I = 0x66

.field public static final DISCONNECT_SUCCESS:I = 0x0

.field public static final DISCONNECT_TIMEOUT:I = 0x69

.field public static final DISCONNECT_UNEXPECTED_DISCONNECTION:I = 0x68

.field public static final DISCONNECT_UNEXPECTED_ERROR:I = 0xc8

.field public static final UPDATE_SETTING_NOT_CONNECTED:I = 0x66

.field public static final UPDATE_SETTING_REQUEST_REJECTED:I = 0x67

.field public static final UPDATE_SETTING_SUCCESS:I = 0x0

.field public static final UPDATE_SETTING_TIMEOUT:I = 0x69

.field public static final UPDATE_SETTING_UNEXPECTED_ERROR:I = 0xc8


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract connect(Lcom/starmicronics/starmgsio/ScaleCallback;)V
.end method

.method public abstract disconnect()V
.end method

.method public abstract updateSetting(Lcom/starmicronics/starmgsio/ScaleSetting;)V
.end method
