.class Lcom/starmicronics/starmgsio/m$b;
.super Ljava/lang/Thread;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starmgsio/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field private a:Lcom/starmicronics/starmgsio/ScaleSetting;

.field private b:I

.field private c:Lcom/starmicronics/starmgsio/N;

.field final synthetic d:Lcom/starmicronics/starmgsio/m;


# direct methods
.method constructor <init>(Lcom/starmicronics/starmgsio/m;Lcom/starmicronics/starmgsio/ScaleSetting;ILcom/starmicronics/starmgsio/N;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/m$b;->d:Lcom/starmicronics/starmgsio/m;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p2, p0, Lcom/starmicronics/starmgsio/m$b;->a:Lcom/starmicronics/starmgsio/ScaleSetting;

    iput p3, p0, Lcom/starmicronics/starmgsio/m$b;->b:I

    iput-object p4, p0, Lcom/starmicronics/starmgsio/m$b;->c:Lcom/starmicronics/starmgsio/N;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/starmicronics/starmgsio/m$b;->d:Lcom/starmicronics/starmgsio/m;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lcom/starmicronics/starmgsio/m$b;->d:Lcom/starmicronics/starmgsio/m;

    invoke-static {v3}, Lcom/starmicronics/starmgsio/m;->j(Lcom/starmicronics/starmgsio/m;)I

    move-result v3

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    iget-object v0, p0, Lcom/starmicronics/starmgsio/m$b;->c:Lcom/starmicronics/starmgsio/N;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Lcom/starmicronics/starmgsio/N;->a(I)V

    monitor-exit v2

    return-void

    :cond_0
    new-instance v3, Lcom/starmicronics/starmgsio/D;

    invoke-direct {v3}, Lcom/starmicronics/starmgsio/D;-><init>()V

    iget-object v5, p0, Lcom/starmicronics/starmgsio/m$b;->a:Lcom/starmicronics/starmgsio/ScaleSetting;

    invoke-virtual {v3, v5}, Lcom/starmicronics/starmgsio/D;->a(Lcom/starmicronics/starmgsio/ScaleSetting;)Lcom/starmicronics/starmgsio/D;

    move-result-object v3

    invoke-virtual {v3}, Lcom/starmicronics/starmgsio/D;->b()[B

    move-result-object v3

    iget-object v5, p0, Lcom/starmicronics/starmgsio/m$b;->d:Lcom/starmicronics/starmgsio/m;

    const/4 v6, 0x1

    invoke-static {v5, v6}, Lcom/starmicronics/starmgsio/m;->b(Lcom/starmicronics/starmgsio/m;I)I

    iget-object v5, p0, Lcom/starmicronics/starmgsio/m$b;->d:Lcom/starmicronics/starmgsio/m;

    new-instance v7, Ljava/util/concurrent/Semaphore;

    const/4 v8, 0x0

    invoke-direct {v7, v8}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    invoke-static {v5, v7}, Lcom/starmicronics/starmgsio/m;->a(Lcom/starmicronics/starmgsio/m;Ljava/util/concurrent/Semaphore;)Ljava/util/concurrent/Semaphore;

    iget-object v5, p0, Lcom/starmicronics/starmgsio/m$b;->d:Lcom/starmicronics/starmgsio/m;

    invoke-static {v5}, Lcom/starmicronics/starmgsio/m;->e(Lcom/starmicronics/starmgsio/m;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/bluetooth/BluetoothGattCharacteristic;->setValue([B)Z

    move-result v3

    const/4 v5, 0x2

    if-nez v3, :cond_1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/m$b;->d:Lcom/starmicronics/starmgsio/m;

    invoke-static {v0, v5}, Lcom/starmicronics/starmgsio/m;->b(Lcom/starmicronics/starmgsio/m;I)I

    iget-object v0, p0, Lcom/starmicronics/starmgsio/m$b;->c:Lcom/starmicronics/starmgsio/N;

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Lcom/starmicronics/starmgsio/N;->a(I)V

    monitor-exit v2

    return-void

    :cond_1
    iget-object v3, p0, Lcom/starmicronics/starmgsio/m$b;->d:Lcom/starmicronics/starmgsio/m;

    invoke-static {v3}, Lcom/starmicronics/starmgsio/m;->n(Lcom/starmicronics/starmgsio/m;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v3

    iget-object v7, p0, Lcom/starmicronics/starmgsio/m$b;->d:Lcom/starmicronics/starmgsio/m;

    invoke-static {v7}, Lcom/starmicronics/starmgsio/m;->e(Lcom/starmicronics/starmgsio/m;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/bluetooth/BluetoothGatt;->writeCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v2, p0, Lcom/starmicronics/starmgsio/m$b;->d:Lcom/starmicronics/starmgsio/m;

    invoke-static {v2}, Lcom/starmicronics/starmgsio/m;->m(Lcom/starmicronics/starmgsio/m;)Ljava/util/concurrent/Semaphore;

    move-result-object v2

    iget v3, p0, Lcom/starmicronics/starmgsio/m$b;->b:I

    int-to-long v9, v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    sub-long/2addr v11, v0

    sub-long/2addr v9, v11

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v9, v10, v0}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/m$b;->d:Lcom/starmicronics/starmgsio/m;

    monitor-enter v0

    :try_start_2
    iget-object v1, p0, Lcom/starmicronics/starmgsio/m$b;->d:Lcom/starmicronics/starmgsio/m;

    invoke-static {v1}, Lcom/starmicronics/starmgsio/m;->j(Lcom/starmicronics/starmgsio/m;)I

    move-result v1

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/starmicronics/starmgsio/m$b;->d:Lcom/starmicronics/starmgsio/m;

    invoke-static {v1}, Lcom/starmicronics/starmgsio/m;->d(Lcom/starmicronics/starmgsio/m;)I

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    :goto_0
    if-nez v6, :cond_3

    iget-object v1, p0, Lcom/starmicronics/starmgsio/m$b;->d:Lcom/starmicronics/starmgsio/m;

    invoke-static {v1, v5}, Lcom/starmicronics/starmgsio/m;->b(Lcom/starmicronics/starmgsio/m;I)I

    iget-object v1, p0, Lcom/starmicronics/starmgsio/m$b;->c:Lcom/starmicronics/starmgsio/N;

    const/16 v2, 0x69

    invoke-virtual {v1, v2}, Lcom/starmicronics/starmgsio/N;->a(I)V

    :cond_3
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
