.class Lcom/starmicronics/starioextension/bi;
.super Lcom/starmicronics/starioextension/d;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/starmicronics/starioextension/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/starmicronics/starioextension/j;IZ)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1, p2, p3}, Lcom/starmicronics/starioextension/i;->h(Ljava/util/List;Lcom/starmicronics/starioextension/j;IZ)V

    return-void
.end method

.method public append(B)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/af;->a(Ljava/util/List;B)V

    return-void
.end method

.method public append([B)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/af;->a(Ljava/util/List;[B)V

    return-void
.end method

.method public appendAbsolutePosition(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/a;->a(Ljava/util/List;I)V

    return-void
.end method

.method public appendAlignment(Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/b;->a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$AlignmentPosition;)V

    return-void
.end method

.method public appendBarcode([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZ)V
    .locals 6

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/starmicronics/starioextension/c;->d(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZ)V

    return-void
.end method

.method public appendBarcodeWithAbsolutePosition([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZI)V
    .locals 0

    invoke-virtual {p0, p6}, Lcom/starmicronics/starioextension/bi;->appendAbsolutePosition(I)V

    invoke-virtual/range {p0 .. p5}, Lcom/starmicronics/starioextension/bi;->appendBarcode([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZ)V

    return-void
.end method

.method public appendBlackMark(Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/k;->a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$BlackMarkType;)V

    return-void
.end method

.method public appendCharacterSpace(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/m;->d(Ljava/util/List;Ljava/lang/Integer;)V

    return-void
.end method

.method public appendCodePage(Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/s;->e(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;)V

    return-void
.end method

.method public appendCutPaper(Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/w;->a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;)V

    return-void
.end method

.method public appendEmphasis(Z)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/y;->a(Ljava/util/List;Z)V

    return-void
.end method

.method public appendFontStyle(Lcom/starmicronics/starioextension/ICommandBuilder$FontStyleType;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/ad;->e(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$FontStyleType;)V

    return-void
.end method

.method public appendHorizontalTabPosition([I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/ag;->e(Ljava/util/List;[I)V

    return-void
.end method

.method public appendInitialization(Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/aj;->a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;)V

    return-void
.end method

.method public appendInternational(Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/al;->a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$InternationalType;)V

    return-void
.end method

.method public appendInvert(Z)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/am;->a(Ljava/util/List;Z)V

    return-void
.end method

.method public appendLineFeed()V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/aq;->a(Ljava/util/List;)V

    return-void
.end method

.method public appendLineFeed(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/aq;->a(Ljava/util/List;I)V

    return-void
.end method

.method public appendLineSpace(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/ar;->e(Ljava/util/List;I)V

    return-void
.end method

.method public appendLogo(Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1, p2}, Lcom/starmicronics/starioextension/at;->a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;I)V

    return-void
.end method

.method public appendMultiple(II)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1, p2}, Lcom/starmicronics/starioextension/ay;->d(Ljava/util/List;II)V

    return-void
.end method

.method public appendPageModeRotation(Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/ba;->d(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V

    return-void
.end method

.method public appendPageModeVerticalAbsolutePosition(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/bb;->d(Ljava/util/List;I)V

    return-void
.end method

.method public appendPdf417([BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;II)V
    .locals 7

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-static/range {v0 .. v6}, Lcom/starmicronics/starioextension/bc;->d(Ljava/util/List;[BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;II)V

    return-void
.end method

.method public appendPdf417WithAbsolutePosition([BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;III)V
    .locals 0

    invoke-virtual {p0, p7}, Lcom/starmicronics/starioextension/bi;->appendAbsolutePosition(I)V

    invoke-virtual/range {p0 .. p6}, Lcom/starmicronics/starioextension/bi;->appendPdf417([BIILcom/starmicronics/starioextension/ICommandBuilder$Pdf417Level;II)V

    return-void
.end method

.method public appendPeripheral(Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1, p2}, Lcom/starmicronics/starioextension/bd;->a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;I)V

    return-void
.end method

.method public appendPrintableArea(Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/be;->a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$PrintableAreaType;)V

    return-void
.end method

.method public appendQrCode([BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/starmicronics/starioextension/bf;->d(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;I)V

    return-void
.end method

.method public appendQrCodeWithAbsolutePosition([BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;II)V
    .locals 0

    invoke-virtual {p0, p5}, Lcom/starmicronics/starioextension/bi;->appendAbsolutePosition(I)V

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/starmicronics/starioextension/bi;->appendQrCode([BLcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeLevel;I)V

    return-void
.end method

.method public appendSound(Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;III)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/starmicronics/starioextension/bh;->a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$SoundChannel;III)V

    return-void
.end method

.method public appendTopMargin(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/bn;->e(Ljava/util/List;I)V

    return-void
.end method

.method public appendUnderLine(Z)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/bo;->a(Ljava/util/List;Z)V

    return-void
.end method

.method public appendUnitFeed(I)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/starmicronics/starioextension/bp;->a(Ljava/util/List;I)V

    return-void
.end method

.method public beginDocument()V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/g;->a(Ljava/util/List;)V

    return-void
.end method

.method public beginPageMode(Landroid/graphics/Rect;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0, p1, p2}, Lcom/starmicronics/starioextension/h;->d(Ljava/util/List;Landroid/graphics/Rect;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V

    return-void
.end method

.method public endDocument()V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/z;->a(Ljava/util/List;)V

    return-void
.end method

.method public endPageMode()V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bi;->a:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/aa;->d(Ljava/util/List;)V

    return-void
.end method
