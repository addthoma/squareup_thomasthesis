.class final Lcom/starmicronics/starioextension/c$9;
.super Ljava/util/HashMap;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/starmicronics/starioextension/c;->c(Ljava/util/List;[BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeSymbology;Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap<",
        "Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;",
        "Ljava/lang/Byte;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode1:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/c$9;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode2:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/starmicronics/starioextension/c$9;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode3:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lcom/starmicronics/starioextension/c$9;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode4:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/c$9;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode5:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    invoke-virtual {p0, v0, v2}, Lcom/starmicronics/starioextension/c$9;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode6:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    invoke-virtual {p0, v0, v3}, Lcom/starmicronics/starioextension/c$9;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode7:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    invoke-virtual {p0, v0, v1}, Lcom/starmicronics/starioextension/c$9;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode8:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    invoke-virtual {p0, v0, v2}, Lcom/starmicronics/starioextension/c$9;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode9:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    invoke-virtual {p0, v0, v3}, Lcom/starmicronics/starioextension/c$9;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
