.class Lcom/starmicronics/starioextension/bg;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/starmicronics/starioextension/IDisplayCommandBuilder;


# static fields
.field private static final a:I = 0xa0

.field private static final b:I = 0x28

.field private static final c:I = 0x5


# instance fields
.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "[B>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/starmicronics/starioextension/bg;->d:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public append(B)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte p1, v0, v1

    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/bg;->append([B)V

    return-void
.end method

.method public append([B)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bg;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public appendBackSpace()V
    .locals 1

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/bg;->append(B)V

    return-void
.end method

.method public appendBitmap(Landroid/graphics/Bitmap;Z)V
    .locals 12

    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    const/16 v1, 0x320

    new-array v1, v1, [B

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    const/16 v3, 0x28

    const/16 v4, 0xa0

    const/4 v5, 0x0

    if-ne v2, v4, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-ne v2, v3, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p1, v4, v3, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p1

    :goto_0
    move-object v7, p1

    new-instance p1, Lcom/starmicronics/starioextension/j;

    const/16 v9, 0xa0

    const/4 v10, 0x0

    sget-object v11, Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;->Normal:Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;

    move-object v6, p1

    move v8, p2

    invoke-direct/range {v6 .. v11}, Lcom/starmicronics/starioextension/j;-><init>(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V

    const/4 p2, 0x0

    :goto_1
    if-ge p2, v4, :cond_4

    const/4 v2, 0x0

    :goto_2
    const/4 v3, 0x5

    if-ge v2, v3, :cond_3

    const/4 v3, 0x0

    const/4 v6, 0x0

    :goto_3
    const/16 v7, 0x8

    if-ge v3, v7, :cond_2

    shl-int/lit8 v6, v6, 0x1

    int-to-byte v6, v6

    mul-int/lit8 v7, v2, 0x8

    add-int/2addr v7, v3

    invoke-virtual {p1, p2, v7}, Lcom/starmicronics/starioextension/j;->a(II)Z

    move-result v7

    if-nez v7, :cond_1

    or-int/lit8 v6, v6, 0x1

    int-to-byte v6, v6

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_2
    mul-int/lit8 v3, p2, 0x5

    add-int/2addr v3, v2

    aput-byte v6, v1, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/bg;->append([B)V

    invoke-virtual {p0, v1}, Lcom/starmicronics/starioextension/bg;->append([B)V

    return-void

    :array_0
    .array-data 1
        0x1bt
        0x20t
    .end array-data
.end method

.method public appendCarriageReturn()V
    .locals 1

    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/bg;->append(B)V

    return-void
.end method

.method public appendClearScreen()V
    .locals 1

    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/bg;->append([B)V

    return-void

    :array_0
    .array-data 1
        0x1bt
        0x5bt
        0x32t
        0x4at
    .end array-data
.end method

.method public appendCodePage(Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;)V
    .locals 3

    sget-object v0, Lcom/starmicronics/starioextension/bg$1;->b:[I

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    const/16 p1, 0x30

    goto :goto_0

    :pswitch_0
    const/16 p1, 0x3d

    goto :goto_0

    :pswitch_1
    const/16 p1, 0x3c

    goto :goto_0

    :pswitch_2
    const/16 p1, 0x3b

    goto :goto_0

    :pswitch_3
    const/16 p1, 0x3a

    goto :goto_0

    :pswitch_4
    const/16 p1, 0x39

    goto :goto_0

    :pswitch_5
    const/16 p1, 0x38

    goto :goto_0

    :pswitch_6
    const/16 p1, 0x37

    goto :goto_0

    :pswitch_7
    const/16 p1, 0x36

    goto :goto_0

    :pswitch_8
    const/16 p1, 0x35

    goto :goto_0

    :pswitch_9
    const/16 p1, 0x34

    goto :goto_0

    :pswitch_a
    const/16 p1, 0x33

    goto :goto_0

    :pswitch_b
    const/16 p1, 0x32

    goto :goto_0

    :pswitch_c
    const/16 p1, 0x31

    :goto_0
    const/4 v0, 0x3

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, 0x1b

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x52

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    aput-byte p1, v0, v1

    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/bg;->append([B)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public appendContrastMode(Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;)V
    .locals 9

    sget-object v0, Lcom/starmicronics/starioextension/bg$1;->d:[I

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x6

    const/4 v1, 0x5

    const/4 v2, 0x4

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x3

    packed-switch p1, :pswitch_data_0

    const/4 p1, 0x3

    goto :goto_0

    :pswitch_0
    const/4 p1, 0x6

    goto :goto_0

    :pswitch_1
    const/4 p1, 0x5

    goto :goto_0

    :pswitch_2
    const/4 p1, 0x4

    goto :goto_0

    :pswitch_3
    const/4 p1, 0x2

    goto :goto_0

    :pswitch_4
    const/4 p1, 0x1

    goto :goto_0

    :pswitch_5
    const/4 p1, 0x0

    :goto_0
    const/4 v7, 0x7

    new-array v7, v7, [B

    const/16 v8, 0x1b

    aput-byte v8, v7, v5

    const/16 v5, 0x5c

    aput-byte v5, v7, v4

    const/16 v4, 0x3f

    aput-byte v4, v7, v3

    const/16 v3, 0x4c

    aput-byte v3, v7, v6

    const/16 v3, 0x44

    aput-byte v3, v7, v2

    aput-byte p1, v7, v1

    aput-byte v6, v7, v0

    invoke-virtual {p0, v7}, Lcom/starmicronics/starioextension/bg;->append([B)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public appendCursorMode(Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CursorMode;)V
    .locals 5

    sget-object v0, Lcom/starmicronics/starioextension/bg$1;->c:[I

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CursorMode;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eq p1, v2, :cond_1

    if-eq p1, v1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    :goto_0
    const/4 v3, 0x6

    new-array v3, v3, [B

    const/16 v4, 0x1b

    aput-byte v4, v3, v0

    const/16 v0, 0x5c

    aput-byte v0, v3, v2

    const/16 v0, 0x3f

    aput-byte v0, v3, v1

    const/4 v0, 0x3

    const/16 v1, 0x4c

    aput-byte v1, v3, v0

    const/4 v0, 0x4

    const/16 v1, 0x43

    aput-byte v1, v3, v0

    const/4 v0, 0x5

    aput-byte p1, v3, v0

    invoke-virtual {p0, v3}, Lcom/starmicronics/starioextension/bg;->append([B)V

    return-void
.end method

.method public appendDeleteToEndOfLine()V
    .locals 1

    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/bg;->append([B)V

    return-void

    :array_0
    .array-data 1
        0x1bt
        0x5bt
        0x30t
        0x4bt
    .end array-data
.end method

.method public appendHomePosition()V
    .locals 1

    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/bg;->append([B)V

    return-void

    :array_0
    .array-data 1
        0x1bt
        0x5bt
        0x48t
        0x27t
    .end array-data
.end method

.method public appendHorizontalTab()V
    .locals 1

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/bg;->append(B)V

    return-void
.end method

.method public appendInternational(Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;)V
    .locals 5

    sget-object v0, Lcom/starmicronics/starioextension/bg$1;->a:[I

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x3

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    packed-switch p1, :pswitch_data_0

    const/4 p1, 0x0

    goto :goto_0

    :pswitch_0
    const/16 p1, 0xd

    goto :goto_0

    :pswitch_1
    const/16 p1, 0xc

    goto :goto_0

    :pswitch_2
    const/16 p1, 0xb

    goto :goto_0

    :pswitch_3
    const/16 p1, 0xa

    goto :goto_0

    :pswitch_4
    const/16 p1, 0x9

    goto :goto_0

    :pswitch_5
    const/16 p1, 0x8

    goto :goto_0

    :pswitch_6
    const/4 p1, 0x7

    goto :goto_0

    :pswitch_7
    const/4 p1, 0x6

    goto :goto_0

    :pswitch_8
    const/4 p1, 0x5

    goto :goto_0

    :pswitch_9
    const/4 p1, 0x4

    goto :goto_0

    :pswitch_a
    const/4 p1, 0x3

    goto :goto_0

    :pswitch_b
    const/4 p1, 0x2

    goto :goto_0

    :pswitch_c
    const/4 p1, 0x1

    :goto_0
    new-array v0, v0, [B

    const/16 v4, 0x1b

    aput-byte v4, v0, v3

    const/16 v3, 0x52

    aput-byte v3, v0, v2

    aput-byte p1, v0, v1

    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/bg;->append([B)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public appendLineFeed()V
    .locals 1

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/bg;->append(B)V

    return-void
.end method

.method public appendSpecifiedPosition(II)V
    .locals 5

    const/4 v0, 0x1

    if-gt v0, p1, :cond_0

    const/16 v1, 0x14

    if-le p1, v1, :cond_1

    :cond_0
    const/4 p1, 0x1

    :cond_1
    const/4 v1, 0x2

    if-gt v0, p2, :cond_2

    if-le p2, v1, :cond_3

    :cond_2
    const/4 p2, 0x1

    :cond_3
    const/4 v2, 0x6

    new-array v2, v2, [B

    const/4 v3, 0x0

    const/16 v4, 0x1b

    aput-byte v4, v2, v3

    const/16 v3, 0x5b

    aput-byte v3, v2, v0

    int-to-byte p2, p2

    aput-byte p2, v2, v1

    const/4 p2, 0x3

    const/16 v0, 0x3b

    aput-byte v0, v2, p2

    const/4 p2, 0x4

    int-to-byte p1, p1

    aput-byte p1, v2, p2

    const/4 p1, 0x5

    const/16 p2, 0x48

    aput-byte p2, v2, p1

    invoke-virtual {p0, v2}, Lcom/starmicronics/starioextension/bg;->append([B)V

    return-void
.end method

.method public appendTurnOn(Z)V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, 0x1b

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x5b

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    aput-byte p1, v0, v1

    const/4 p1, 0x3

    const/16 v1, 0x50

    aput-byte v1, v0, p1

    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/bg;->append([B)V

    return-void
.end method

.method public appendUserDefinedCharacter(II[B)V
    .locals 5

    const/16 v0, 0xb

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, 0x1b

    aput-byte v2, v0, v1

    const/4 v2, 0x1

    const/16 v3, 0x5c

    aput-byte v3, v0, v2

    const/4 v3, 0x2

    const/16 v4, 0x3f

    aput-byte v4, v0, v3

    const/4 v3, 0x3

    const/16 v4, 0x4c

    aput-byte v4, v0, v3

    const/4 v3, 0x4

    const/16 v4, 0x57

    aput-byte v4, v0, v3

    const/4 v3, 0x5

    aput-byte v2, v0, v3

    const/16 v2, 0x3b

    const/4 v3, 0x6

    aput-byte v2, v0, v3

    int-to-byte p1, p1

    const/4 v3, 0x7

    aput-byte p1, v0, v3

    const/16 p1, 0x8

    aput-byte v2, v0, p1

    int-to-byte p1, p2

    const/16 p2, 0x9

    aput-byte p1, v0, p2

    const/16 p1, 0xa

    aput-byte v2, v0, p1

    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/bg;->append([B)V

    const/16 p1, 0x10

    new-array p1, p1, [B

    if-eqz p3, :cond_1

    array-length p2, p1

    array-length v0, p3

    if-gt p2, v0, :cond_0

    array-length p2, p1

    goto :goto_0

    :cond_0
    array-length p2, p3

    :goto_0
    invoke-static {p3, v1, p1, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/bg;->append([B)V

    return-void
.end method

.method public appendUserDefinedDbcsCharacter(II[B)V
    .locals 5

    const/16 v0, 0xc

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, 0x1b

    aput-byte v2, v0, v1

    const/4 v2, 0x1

    const/16 v3, 0x5c

    aput-byte v3, v0, v2

    const/4 v2, 0x2

    const/16 v3, 0x3f

    aput-byte v3, v0, v2

    const/4 v3, 0x3

    const/16 v4, 0x4c

    aput-byte v4, v0, v3

    const/4 v3, 0x4

    const/16 v4, 0x57

    aput-byte v4, v0, v3

    const/4 v3, 0x5

    aput-byte v2, v0, v3

    const/16 v2, 0x3b

    const/4 v3, 0x6

    aput-byte v2, v0, v3

    int-to-byte p1, p1

    const/4 v3, 0x7

    aput-byte p1, v0, v3

    const/16 p1, 0x8

    aput-byte v2, v0, p1

    div-int/lit16 p1, p2, 0x100

    int-to-byte p1, p1

    const/16 v3, 0x9

    aput-byte p1, v0, v3

    rem-int/lit16 p2, p2, 0x100

    int-to-byte p1, p2

    const/16 p2, 0xa

    aput-byte p1, v0, p2

    const/16 p1, 0xb

    aput-byte v2, v0, p1

    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/bg;->append([B)V

    const/16 p1, 0x20

    new-array p1, p1, [B

    if-eqz p3, :cond_1

    array-length p2, p1

    array-length v0, p3

    if-gt p2, v0, :cond_0

    array-length p2, p1

    goto :goto_0

    :cond_0
    array-length p2, p3

    :goto_0
    invoke-static {p3, v1, p1, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/bg;->append([B)V

    return-void
.end method

.method public getCommands()[B
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/bg;->d:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/as;->b(Ljava/util/List;)[B

    move-result-object v0

    return-object v0
.end method

.method public getPassThroughCommands()[B
    .locals 7

    iget-object v0, p0, Lcom/starmicronics/starioextension/bg;->d:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/as;->a(Ljava/util/List;)I

    move-result v0

    rem-int/lit16 v1, v0, 0x100

    int-to-byte v1, v1

    div-int/lit16 v0, v0, 0x100

    int-to-byte v0, v0

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/starmicronics/starioextension/bg;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const/4 v3, 0x6

    new-array v3, v3, [B

    const/4 v4, 0x0

    const/16 v5, 0x1b

    aput-byte v5, v3, v4

    const/4 v5, 0x1

    const/16 v6, 0x1d

    aput-byte v6, v3, v5

    const/4 v5, 0x2

    const/16 v6, 0x42

    aput-byte v6, v3, v5

    const/4 v5, 0x3

    const/16 v6, 0x40

    aput-byte v6, v3, v5

    const/4 v5, 0x4

    aput-byte v1, v3, v5

    const/4 v1, 0x5

    aput-byte v0, v3, v1

    invoke-interface {v2, v4, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    invoke-static {v2}, Lcom/starmicronics/starioextension/as;->b(Ljava/util/List;)[B

    move-result-object v0

    return-object v0
.end method
