.class public final enum Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/IDisplayCommandBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InternationalType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

.field public static final enum Denmark:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

.field public static final enum Denmark2:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

.field public static final enum France:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

.field public static final enum Germany:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

.field public static final enum Italy:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

.field public static final enum Japan:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

.field public static final enum Korea:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

.field public static final enum LatinAmerica:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

.field public static final enum Norway:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

.field public static final enum Spain:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

.field public static final enum Spain2:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

.field public static final enum Sweden:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

.field public static final enum UK:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

.field public static final enum USA:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    const/4 v1, 0x0

    const-string v2, "USA"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->USA:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    const/4 v2, 0x1

    const-string v3, "France"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->France:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    const/4 v3, 0x2

    const-string v4, "Germany"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Germany:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    const/4 v4, 0x3

    const-string v5, "UK"

    invoke-direct {v0, v5, v4}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->UK:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    const/4 v5, 0x4

    const-string v6, "Denmark"

    invoke-direct {v0, v6, v5}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Denmark:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    const/4 v6, 0x5

    const-string v7, "Sweden"

    invoke-direct {v0, v7, v6}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Sweden:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    const/4 v7, 0x6

    const-string v8, "Italy"

    invoke-direct {v0, v8, v7}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Italy:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    const/4 v8, 0x7

    const-string v9, "Spain"

    invoke-direct {v0, v9, v8}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Spain:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    const/16 v9, 0x8

    const-string v10, "Japan"

    invoke-direct {v0, v10, v9}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Japan:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    const/16 v10, 0x9

    const-string v11, "Norway"

    invoke-direct {v0, v11, v10}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Norway:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    const/16 v11, 0xa

    const-string v12, "Denmark2"

    invoke-direct {v0, v12, v11}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Denmark2:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    const/16 v12, 0xb

    const-string v13, "Spain2"

    invoke-direct {v0, v13, v12}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Spain2:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    const/16 v13, 0xc

    const-string v14, "LatinAmerica"

    invoke-direct {v0, v14, v13}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->LatinAmerica:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    new-instance v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    const/16 v14, 0xd

    const-string v15, "Korea"

    invoke-direct {v0, v15, v14}, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Korea:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    const/16 v0, 0xe

    new-array v0, v0, [Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    sget-object v15, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->USA:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    aput-object v15, v0, v1

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->France:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Germany:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->UK:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Denmark:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Sweden:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Italy:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Spain:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Japan:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Norway:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Denmark2:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Spain2:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->LatinAmerica:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    aput-object v1, v0, v13

    sget-object v1, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->Korea:Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    aput-object v1, v0, v14

    sput-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->$VALUES:[Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->$VALUES:[Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;

    return-object v0
.end method
