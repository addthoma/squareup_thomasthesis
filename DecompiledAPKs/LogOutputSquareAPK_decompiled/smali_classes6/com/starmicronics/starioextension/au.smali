.class Lcom/starmicronics/starioextension/au;
.super Lcom/starmicronics/starioextension/e;


# static fields
.field private static final a:I = 0x0

.field private static final b:I = 0x1

.field private static final c:I = 0xf

.field private static final d:I = 0xff

.field private static final e:I = 0x1f400


# instance fields
.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "[B>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/starmicronics/starioextension/e;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    return-void
.end method

.method private a(Lcom/starmicronics/starioextension/SoundSetting;)I
    .locals 2

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/SoundSetting;->getVolume()I

    move-result v0

    const/4 v1, -0x4

    if-eq v0, v1, :cond_2

    const/4 v1, -0x3

    if-eq v0, v1, :cond_1

    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/SoundSetting;->getVolume()I

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1

    :cond_2
    const/16 p1, 0xf

    return p1
.end method


# virtual methods
.method public appendSound(Lcom/starmicronics/starioextension/SoundSetting;)V
    .locals 9

    const-string v0, "Volume"

    const-string v1, "SoundNumber"

    const-string v2, "SoundStorageArea"

    filled-new-array {v2, v1, v0}, [Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/starmicronics/starioextension/SoundSetting;->getChangedParameters([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    if-gtz v4, :cond_c

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/SoundSetting;->getSoundStorageArea()Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    move-result-object v3

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/SoundSetting;->getSoundNumber()I

    move-result v4

    invoke-virtual {p1, v2}, Lcom/starmicronics/starioextension/SoundSetting;->isParameterChanged(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {p1, v1}, Lcom/starmicronics/starioextension/SoundSetting;->isParameterChanged(Ljava/lang/String;)Z

    move-result v1

    const/4 v5, 0x7

    const/4 v6, -0x1

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    if-ltz v4, :cond_0

    if-gt v4, v5, :cond_0

    invoke-static {v4}, Lcom/starmicronics/starioextension/ax;->a(I)I

    move-result v1

    move v2, v1

    const/4 v1, 0x1

    goto :goto_1

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-array v0, v7, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v8

    const-string v1, "SoundNumber %d is out of range. (0-7)"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :cond_2
    if-nez v2, :cond_4

    if-nez v1, :cond_3

    goto :goto_0

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Not allowed to specify only one of SoundStorageArea and SoundNumber."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    :goto_0
    const/4 v1, 0x0

    const/4 v2, -0x1

    :goto_1
    invoke-virtual {p1, v0}, Lcom/starmicronics/starioextension/SoundSetting;->isParameterChanged(Ljava/lang/String;)Z

    move-result v0

    const/16 v4, 0xf

    if-eqz v0, :cond_6

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/au;->a(Lcom/starmicronics/starioextension/SoundSetting;)I

    move-result p1

    if-ltz p1, :cond_5

    if-gt p1, v4, :cond_5

    invoke-static {p1}, Lcom/starmicronics/starioextension/ax;->a(I)I

    move-result v6

    const/4 p1, 0x1

    goto :goto_2

    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v8

    const-string p1, "Volume %d is out of range. (0-15)"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    const/4 p1, 0x0

    :goto_2
    const/16 v0, 0xfd

    if-eqz v1, :cond_a

    if-eqz p1, :cond_8

    sget-object p1, Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;->Area1:Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    if-ne v3, p1, :cond_7

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/aw;->a(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v4}, Lcom/starmicronics/starioextension/aw;->a(B)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v2}, Lcom/starmicronics/starioextension/aw;->a(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v6}, Lcom/starmicronics/starioextension/aw;->a(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :cond_7
    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/aw;->a(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    const/16 v0, 0x10

    invoke-static {v0}, Lcom/starmicronics/starioextension/aw;->a(B)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v2}, Lcom/starmicronics/starioextension/aw;->a(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v6}, Lcom/starmicronics/starioextension/aw;->a(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :cond_8
    sget-object p1, Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;->Area1:Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    if-ne v3, p1, :cond_9

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/aw;->a(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v7}, Lcom/starmicronics/starioextension/aw;->a(B)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v2}, Lcom/starmicronics/starioextension/aw;->a(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v8}, Lcom/starmicronics/starioextension/aw;->a(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_9
    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/aw;->a(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/starmicronics/starioextension/aw;->a(B)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v2}, Lcom/starmicronics/starioextension/aw;->a(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v8}, Lcom/starmicronics/starioextension/aw;->a(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_a
    if-eqz p1, :cond_b

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/aw;->a(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v5}, Lcom/starmicronics/starioextension/aw;->a(B)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v8}, Lcom/starmicronics/starioextension/aw;->a(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v6}, Lcom/starmicronics/starioextension/aw;->a(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_b
    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/aw;->a(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    const/16 v0, 0x1f

    invoke-static {v0}, Lcom/starmicronics/starioextension/aw;->a(B)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v8}, Lcom/starmicronics/starioextension/aw;->a(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v8}, Lcom/starmicronics/starioextension/aw;->a(I)[B

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_3
    return-void

    :cond_c
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0, v3}, Lcom/starmicronics/starioextension/au;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public appendSoundData([BLcom/starmicronics/starioextension/SoundSetting;)V
    .locals 11

    const-string v0, "Volume"

    invoke-virtual {p2, v0}, Lcom/starmicronics/starioextension/SoundSetting;->getChangedParameter(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    if-gtz v2, :cond_9

    invoke-virtual {p2, v0}, Lcom/starmicronics/starioextension/SoundSetting;->isParameterChanged(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    invoke-direct {p0, p2}, Lcom/starmicronics/starioextension/au;->a(Lcom/starmicronics/starioextension/SoundSetting;)I

    move-result p2

    if-ltz p2, :cond_0

    const/16 v0, 0xf

    if-gt p2, v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v0, v1

    const-string p2, "Volume %d is out of range. (0-15)"

    invoke-static {p2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    const/16 p2, 0xff

    :goto_0
    invoke-static {p1}, Lcom/starmicronics/starioextension/bt;->a([B)Lcom/starmicronics/starioextension/bt$a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/bt$a;->c()Lcom/starmicronics/starioextension/bt$b;

    move-result-object v0

    sget-object v3, Lcom/starmicronics/starioextension/bt$b;->b:Lcom/starmicronics/starioextension/bt$b;

    if-ne v0, v3, :cond_8

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/bt$a;->e()I

    move-result v0

    const/16 v3, 0x3200

    if-ne v0, v3, :cond_7

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/bt$a;->h()I

    move-result v0

    const/16 v3, 0x8

    const/16 v4, 0x10

    if-eq v0, v4, :cond_3

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/bt$a;->h()I

    move-result v0

    if-ne v0, v3, :cond_2

    goto :goto_1

    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Unsupported sound format (required bit depth is 8 or 16). Please refer to the SDK manual for supported format."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    invoke-virtual {p1}, Lcom/starmicronics/starioextension/bt$a;->d()I

    move-result v0

    if-ne v0, v2, :cond_6

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/bt$a;->h()I

    move-result v0

    const/4 v5, 0x2

    if-ne v0, v4, :cond_4

    const/4 v0, 0x2

    goto :goto_2

    :cond_4
    const/4 v0, 0x1

    :goto_2
    invoke-virtual {p1}, Lcom/starmicronics/starioextension/bt$a;->j()[B

    move-result-object v4

    array-length v4, v4

    const v6, 0x1f400

    if-gt v4, v6, :cond_5

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/bt$a;->j()[B

    move-result-object v4

    array-length v4, v4

    const/high16 v6, 0x10000

    div-int v7, v4, v6

    mul-int v6, v6, v7

    sub-int/2addr v4, v6

    div-int/lit16 v6, v4, 0x100

    rem-int/lit16 v4, v4, 0x100

    iget-object v8, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    const/16 v9, 0x9

    new-array v9, v9, [B

    const/16 v10, 0x1b

    aput-byte v10, v9, v1

    const/16 v1, 0x1d

    aput-byte v1, v9, v2

    const/16 v1, 0x73

    aput-byte v1, v9, v5

    const/4 v1, 0x3

    const/16 v2, 0x52

    aput-byte v2, v9, v1

    const/4 v1, 0x4

    aput-byte v0, v9, v1

    const/4 v0, 0x5

    int-to-byte p2, p2

    aput-byte p2, v9, v0

    const/4 p2, 0x6

    int-to-byte v0, v4

    aput-byte v0, v9, p2

    const/4 p2, 0x7

    int-to-byte v0, v6

    aput-byte v0, v9, p2

    int-to-byte p2, v7

    aput-byte p2, v9, v3

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p2, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/bt$a;->j()[B

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_5
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-array v0, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/bt$a;->j()[B

    move-result-object p1

    array-length p1, p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v1

    const-string p1, "Data length %d is out of range. (Upper limit is 128000 bytes)"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Unsupported sound format (required number of channel is one). Please refer to the SDK manual for supported format."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Unsupported sound format (required sampling rate is 12.8kHz). Please refer to the SDK manual for supported format."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Unsupported sound format (required WAV format is Linear PCM). Please refer to the SDK manual for supported format."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_9
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0, v1}, Lcom/starmicronics/starioextension/au;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getCommands()[B
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/au;->f:Ljava/util/List;

    invoke-static {v0}, Lcom/starmicronics/starioextension/as;->b(Ljava/util/List;)[B

    move-result-object v0

    return-object v0
.end method
