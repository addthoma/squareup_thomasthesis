.class Lcom/starmicronics/starioextension/at;
.super Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;",
            "I)V"
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/starioextension/at$1;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/at$1;-><init>()V

    const/4 v1, 0x1

    if-ge p2, v1, :cond_0

    const/4 p2, 0x1

    :cond_0
    const/16 v2, 0xff

    if-le p2, v2, :cond_1

    const/16 p2, 0xff

    :cond_1
    const/4 v2, 0x5

    new-array v2, v2, [B

    const/4 v3, 0x0

    const/16 v4, 0x1b

    aput-byte v4, v2, v3

    const/16 v3, 0x1c

    aput-byte v3, v2, v1

    const/4 v1, 0x2

    const/16 v3, 0x70

    aput-byte v3, v2, v1

    const/4 v1, 0x3

    int-to-byte p2, p2

    aput-byte p2, v2, v1

    const/4 p2, 0x4

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result p1

    aput-byte p1, v2, p2

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static b(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;",
            "I)V"
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/starioextension/at$2;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/at$2;-><init>()V

    new-instance v1, Lcom/starmicronics/starioextension/at$3;

    invoke-direct {v1}, Lcom/starmicronics/starioextension/at$3;-><init>()V

    const/4 v2, 0x1

    if-ge p2, v2, :cond_0

    const/4 p2, 0x1

    :cond_0
    const/16 v3, 0xff

    if-le p2, v3, :cond_1

    const/16 p2, 0xff

    :cond_1
    const/16 v3, 0x64

    const/16 v4, 0xa

    const/16 v5, 0x30

    if-ge p2, v3, :cond_2

    div-int/lit8 v3, p2, 0xa

    add-int/2addr v3, v5

    int-to-byte v3, v3

    rem-int/2addr p2, v4

    add-int/2addr p2, v5

    goto :goto_0

    :cond_2
    add-int/lit8 p2, p2, -0x64

    div-int/lit8 v3, p2, 0x5f

    add-int/lit8 v3, v3, 0x20

    int-to-byte v3, v3

    rem-int/lit8 p2, p2, 0x5f

    add-int/lit8 p2, p2, 0x20

    :goto_0
    int-to-byte p2, p2

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result p1

    const/16 v1, 0xc

    new-array v1, v1, [B

    const/16 v6, 0x1b

    const/4 v7, 0x0

    aput-byte v6, v1, v7

    const/16 v6, 0x1d

    aput-byte v6, v1, v2

    const/4 v2, 0x2

    const/16 v6, 0x28

    aput-byte v6, v1, v2

    const/4 v2, 0x3

    const/16 v6, 0x4c

    aput-byte v6, v1, v2

    const/4 v2, 0x4

    const/4 v6, 0x6

    aput-byte v6, v1, v2

    const/4 v2, 0x5

    aput-byte v7, v1, v2

    aput-byte v5, v1, v6

    const/4 v2, 0x7

    const/16 v5, 0x45

    aput-byte v5, v1, v2

    const/16 v2, 0x8

    aput-byte v3, v1, v2

    const/16 v2, 0x9

    aput-byte p2, v1, v2

    aput-byte v0, v1, v4

    const/16 p2, 0xb

    aput-byte p1, v1, p2

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;",
            "I)V"
        }
    .end annotation

    return-void
.end method

.method static d(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;",
            "I)V"
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/starioextension/at$4;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/at$4;-><init>()V

    const/4 v1, 0x1

    if-ge p2, v1, :cond_0

    const/4 p2, 0x1

    :cond_0
    const/16 v2, 0xff

    if-le p2, v2, :cond_1

    const/16 p2, 0xff

    :cond_1
    const/4 v2, 0x4

    new-array v2, v2, [B

    const/4 v3, 0x0

    const/16 v4, 0x1c

    aput-byte v4, v2, v3

    const/16 v3, 0x70

    aput-byte v3, v2, v1

    const/4 v1, 0x2

    int-to-byte p2, p2

    aput-byte p2, v2, v1

    const/4 p2, 0x3

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result p1

    aput-byte p1, v2, p2

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static e(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$LogoSize;",
            "I)V"
        }
    .end annotation

    const/4 p1, 0x1

    if-ge p2, p1, :cond_0

    const/4 p2, 0x1

    :cond_0
    const/16 v0, 0xff

    if-le p2, v0, :cond_1

    const/16 p2, 0xff

    :cond_1
    add-int/lit8 p2, p2, -0x1

    const/4 v0, 0x4

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, 0x1b

    aput-byte v2, v0, v1

    const/16 v1, 0x66

    aput-byte v1, v0, p1

    const/4 p1, 0x2

    int-to-byte p2, p2

    aput-byte p2, v0, p1

    const/4 p1, 0x3

    const/16 p2, 0xa

    aput-byte p2, v0, p1

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
