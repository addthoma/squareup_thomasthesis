.class public Lcom/starmicronics/starioextension/SoundSetting;
.super Ljava/lang/Object;


# static fields
.field static final INITIAL_VALUE:I = 0x0

.field static final PARAM_STRING_COUNT:Ljava/lang/String; = "Count"

.field static final PARAM_STRING_DELAY:Ljava/lang/String; = "Delay"

.field static final PARAM_STRING_INTERVAL:Ljava/lang/String; = "Interval"

.field static final PARAM_STRING_SOUND_NUMBER:Ljava/lang/String; = "SoundNumber"

.field static final PARAM_STRING_SOUND_STORAGE_AREA:Ljava/lang/String; = "SoundStorageArea"

.field static final PARAM_STRING_VOLUME:Ljava/lang/String; = "Volume"

.field public static final VOLUME_MAX:I = -0x4

.field public static final VOLUME_MIN:I = -0x3

.field public static final VOLUME_OFF:I = -0x2


# instance fields
.field private mChangedParameterList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCount:I

.field private mDelay:I

.field private mInterval:I

.field private mSoundNumber:I

.field private mSoundStorageArea:Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

.field private mVolume:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/starioextension/SoundSetting;->mSoundStorageArea:Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    const/4 v0, 0x0

    iput v0, p0, Lcom/starmicronics/starioextension/SoundSetting;->mSoundNumber:I

    iput v0, p0, Lcom/starmicronics/starioextension/SoundSetting;->mVolume:I

    iput v0, p0, Lcom/starmicronics/starioextension/SoundSetting;->mCount:I

    iput v0, p0, Lcom/starmicronics/starioextension/SoundSetting;->mDelay:I

    iput v0, p0, Lcom/starmicronics/starioextension/SoundSetting;->mInterval:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/starmicronics/starioextension/SoundSetting;->mChangedParameterList:Ljava/util/List;

    return-void
.end method

.method private setAsChangedParameter(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/SoundSetting;->mChangedParameterList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/starmicronics/starioextension/SoundSetting;->mChangedParameterList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method getChangedParameter(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, v0}, Lcom/starmicronics/starioextension/SoundSetting;->getChangedParameters([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getChangedParameters()[Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/starmicronics/starioextension/SoundSetting;->mChangedParameterList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method getChangedParameters([Ljava/lang/String;)[Ljava/lang/String;
    .locals 4

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/starmicronics/starioextension/SoundSetting;->mChangedParameterList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    new-array p1, p1, [Ljava/lang/String;

    invoke-interface {v0, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    return-object p1
.end method

.method public getCount()I
    .locals 1

    iget v0, p0, Lcom/starmicronics/starioextension/SoundSetting;->mCount:I

    return v0
.end method

.method public getDelay()I
    .locals 1

    iget v0, p0, Lcom/starmicronics/starioextension/SoundSetting;->mDelay:I

    return v0
.end method

.method public getInterval()I
    .locals 1

    iget v0, p0, Lcom/starmicronics/starioextension/SoundSetting;->mInterval:I

    return v0
.end method

.method public getSoundNumber()I
    .locals 1

    iget v0, p0, Lcom/starmicronics/starioextension/SoundSetting;->mSoundNumber:I

    return v0
.end method

.method public getSoundStorageArea()Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/SoundSetting;->mSoundStorageArea:Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    return-object v0
.end method

.method public getVolume()I
    .locals 1

    iget v0, p0, Lcom/starmicronics/starioextension/SoundSetting;->mVolume:I

    return v0
.end method

.method isParameterChanged(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/SoundSetting;->mChangedParameterList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public setCount(I)V
    .locals 0

    iput p1, p0, Lcom/starmicronics/starioextension/SoundSetting;->mCount:I

    const-string p1, "Count"

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/SoundSetting;->setAsChangedParameter(Ljava/lang/String;)V

    return-void
.end method

.method public setDelay(I)V
    .locals 0

    iput p1, p0, Lcom/starmicronics/starioextension/SoundSetting;->mDelay:I

    const-string p1, "Delay"

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/SoundSetting;->setAsChangedParameter(Ljava/lang/String;)V

    return-void
.end method

.method public setInterval(I)V
    .locals 0

    iput p1, p0, Lcom/starmicronics/starioextension/SoundSetting;->mInterval:I

    const-string p1, "Interval"

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/SoundSetting;->setAsChangedParameter(Ljava/lang/String;)V

    return-void
.end method

.method public setSoundNumber(I)V
    .locals 0

    iput p1, p0, Lcom/starmicronics/starioextension/SoundSetting;->mSoundNumber:I

    const-string p1, "SoundNumber"

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/SoundSetting;->setAsChangedParameter(Ljava/lang/String;)V

    return-void
.end method

.method public setSoundStorageArea(Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/SoundSetting;->mSoundStorageArea:Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;

    const-string p1, "SoundStorageArea"

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/SoundSetting;->setAsChangedParameter(Ljava/lang/String;)V

    return-void
.end method

.method public setVolume(I)V
    .locals 0

    iput p1, p0, Lcom/starmicronics/starioextension/SoundSetting;->mVolume:I

    const-string p1, "Volume"

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/SoundSetting;->setAsChangedParameter(Ljava/lang/String;)V

    return-void
.end method
