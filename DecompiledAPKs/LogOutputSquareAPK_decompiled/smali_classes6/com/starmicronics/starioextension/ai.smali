.class interface abstract Lcom/starmicronics/starioextension/ai;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onAccessoryConnectFailure()V
.end method

.method public abstract onAccessoryConnectSuccess()V
.end method

.method public abstract onAccessoryDisconnect()V
.end method

.method public abstract onBarcodeDataReceive([B)V
.end method

.method public abstract onBarcodeReaderConnect()V
.end method

.method public abstract onBarcodeReaderDisconnect()V
.end method

.method public abstract onBarcodeReaderImpossible()V
.end method

.method public abstract onCashDrawerClose()V
.end method

.method public abstract onCashDrawerOpen()V
.end method

.method public abstract onPrinterCoverClose()V
.end method

.method public abstract onPrinterCoverOpen()V
.end method

.method public abstract onPrinterImpossible()V
.end method

.method public abstract onPrinterOffline()V
.end method

.method public abstract onPrinterOnline()V
.end method

.method public abstract onPrinterPaperEmpty()V
.end method

.method public abstract onPrinterPaperNearEmpty()V
.end method

.method public abstract onPrinterPaperReady()V
.end method

.method public abstract onStatusUpdate(Ljava/lang/String;)V
.end method
