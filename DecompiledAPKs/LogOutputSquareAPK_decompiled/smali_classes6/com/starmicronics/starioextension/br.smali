.class Lcom/starmicronics/starioextension/br;
.super Lcom/starmicronics/starioextension/ah;


# direct methods
.method constructor <init>(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/ah;-><init>(I)V

    return-void
.end method

.method private static a([B)[B
    .locals 17

    move-object/from16 v0, p0

    array-length v1, v0

    const/4 v2, 0x0

    const/16 v3, 0xb

    if-eq v1, v3, :cond_0

    array-length v1, v0

    const/16 v4, 0xc

    if-eq v1, v4, :cond_0

    return-object v2

    :cond_0
    invoke-static {v0, v3}, Lcom/starmicronics/starioextension/n;->a([BI)B

    move-result v1

    const/4 v3, 0x7

    new-array v4, v3, [B

    const/4 v5, 0x0

    aget-byte v6, v0, v5

    const/4 v7, 0x6

    const/4 v8, 0x5

    const/4 v9, 0x4

    const/4 v10, 0x1

    const/16 v11, 0x30

    if-ne v6, v11, :cond_1

    aget-byte v6, v0, v9

    if-ne v6, v11, :cond_1

    aget-byte v6, v0, v8

    if-ne v6, v11, :cond_1

    aget-byte v6, v0, v7

    if-ne v6, v11, :cond_1

    aget-byte v6, v0, v3

    if-ne v6, v11, :cond_1

    const/4 v6, 0x1

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    :goto_0
    aget-byte v12, v0, v5

    const/16 v13, 0x9

    const/16 v14, 0x8

    if-ne v12, v11, :cond_2

    aget-byte v12, v0, v7

    if-ne v12, v11, :cond_2

    aget-byte v3, v0, v3

    if-ne v3, v11, :cond_2

    aget-byte v3, v0, v14

    if-ne v3, v11, :cond_2

    aget-byte v3, v0, v13

    if-ne v3, v11, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    const/16 v12, 0xa

    const/4 v15, 0x2

    const/16 v16, 0x3

    if-eqz v6, :cond_4

    aget-byte v6, v0, v16

    if-gt v11, v6, :cond_3

    aget-byte v6, v0, v16

    const/16 v2, 0x32

    if-gt v6, v2, :cond_3

    aget-byte v2, v0, v10

    aput-byte v2, v4, v5

    aget-byte v2, v0, v15

    aput-byte v2, v4, v10

    aget-byte v2, v0, v14

    aput-byte v2, v4, v15

    aget-byte v2, v0, v13

    aput-byte v2, v4, v16

    aget-byte v2, v0, v12

    aput-byte v2, v4, v9

    aget-byte v0, v0, v16

    aput-byte v0, v4, v8

    int-to-byte v0, v1

    aput-byte v0, v4, v7

    return-object v4

    :cond_3
    aget-byte v2, v0, v14

    if-ne v2, v11, :cond_4

    aget-byte v2, v0, v10

    aput-byte v2, v4, v5

    aget-byte v2, v0, v15

    aput-byte v2, v4, v10

    aget-byte v2, v0, v16

    aput-byte v2, v4, v15

    aget-byte v2, v0, v13

    aput-byte v2, v4, v16

    aget-byte v0, v0, v12

    aput-byte v0, v4, v9

    const/16 v0, 0x33

    aput-byte v0, v4, v8

    int-to-byte v0, v1

    aput-byte v0, v4, v7

    return-object v4

    :cond_4
    if-eqz v3, :cond_6

    aget-byte v2, v0, v8

    if-ne v2, v11, :cond_5

    aget-byte v2, v0, v10

    aput-byte v2, v4, v5

    aget-byte v2, v0, v15

    aput-byte v2, v4, v10

    aget-byte v2, v0, v16

    aput-byte v2, v4, v15

    aget-byte v2, v0, v9

    aput-byte v2, v4, v16

    aget-byte v0, v0, v12

    aput-byte v0, v4, v9

    const/16 v0, 0x34

    aput-byte v0, v4, v8

    int-to-byte v0, v1

    aput-byte v0, v4, v7

    return-object v4

    :cond_5
    const/16 v2, 0x35

    aget-byte v3, v0, v12

    if-gt v2, v3, :cond_6

    aget-byte v2, v0, v12

    const/16 v3, 0x39

    if-gt v2, v3, :cond_6

    aget-byte v2, v0, v10

    aput-byte v2, v4, v5

    aget-byte v2, v0, v15

    aput-byte v2, v4, v10

    aget-byte v2, v0, v16

    aput-byte v2, v4, v15

    aget-byte v2, v0, v9

    aput-byte v2, v4, v16

    aget-byte v2, v0, v8

    aput-byte v2, v4, v9

    aget-byte v0, v0, v12

    aput-byte v0, v4, v8

    int-to-byte v0, v1

    aput-byte v0, v4, v7

    return-object v4

    :cond_6
    const/4 v0, 0x0

    return-object v0
.end method

.method private static b([B)Ljava/lang/String;
    .locals 7

    new-instance v0, Lcom/starmicronics/starioextension/br$2;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/br$2;-><init>()V

    new-instance v1, Lcom/starmicronics/starioextension/br$3;

    invoke-direct {v1}, Lcom/starmicronics/starioextension/br$3;-><init>()V

    new-instance v2, Lcom/starmicronics/starioextension/br$4;

    invoke-direct {v2}, Lcom/starmicronics/starioextension/br$4;-><init>()V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "101"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v4, 0x6

    aget-byte v5, p0, v4

    invoke-virtual {v0, v5}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_2

    and-int/lit8 v6, v0, 0x20

    if-nez v6, :cond_0

    aget-byte v6, p0, v5

    invoke-virtual {v1, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    goto :goto_1

    :cond_0
    aget-byte v6, p0, v5

    invoke-virtual {v2, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    :goto_1
    check-cast v6, Ljava/lang/String;

    if-nez v6, :cond_1

    const/4 p0, 0x0

    return-object p0

    :cond_1
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    const-string p0, "010101"

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-static {p1}, Lcom/starmicronics/starioextension/br;->a([B)[B

    move-result-object p1

    if-nez p1, :cond_1

    return-void

    :cond_1
    invoke-static {p1}, Lcom/starmicronics/starioextension/br;->b([B)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    return-void

    :cond_2
    new-instance v1, Lcom/starmicronics/starioextension/br$1;

    invoke-direct {v1, p0}, Lcom/starmicronics/starioextension/br$1;-><init>(Lcom/starmicronics/starioextension/br;)V

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p0, v0, p2}, Lcom/starmicronics/starioextension/br;->a(Ljava/lang/String;I)V

    const/4 p2, 0x6

    new-array p2, p2, [I

    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_3

    aget-byte v1, p1, v0

    aput v1, p2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p2}, Lcom/starmicronics/starioextension/br;->a([I)V

    return-void
.end method
