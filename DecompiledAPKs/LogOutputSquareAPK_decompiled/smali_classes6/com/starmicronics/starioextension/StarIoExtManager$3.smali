.class Lcom/starmicronics/starioextension/StarIoExtManager$3;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/StarIoExtManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/starmicronics/starioextension/StarIoExtManager;


# direct methods
.method constructor <init>(Lcom/starmicronics/starioextension/StarIoExtManager;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$3;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$3;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->n(Lcom/starmicronics/starioextension/StarIoExtManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$3;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    const/4 v1, 0x0

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->o(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/u;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager;ZLcom/starmicronics/starioextension/u;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$3;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {p1}, Lcom/starmicronics/starioextension/StarIoExtManager;->l(Lcom/starmicronics/starioextension/StarIoExtManager;)Ljava/lang/String;

    move-result-object p1

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "usb:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v0, "android.hardware.usb.action.USB_DEVICE_DETACHED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    return-void

    :cond_1
    const-string p1, "device"

    invoke-virtual {p2, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/hardware/usb/UsbDevice;

    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result p2

    const/16 v0, 0x519

    if-eq p2, v0, :cond_2

    return-void

    :cond_2
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x15

    const-string v1, "usb:sn:"

    if-lt p2, v0, :cond_4

    iget-object p2, p0, Lcom/starmicronics/starioextension/StarIoExtManager$3;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {p2}, Lcom/starmicronics/starioextension/StarIoExtManager;->l(Lcom/starmicronics/starioextension/StarIoExtManager;)Ljava/lang/String;

    move-result-object p2

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p2, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_4

    iget-object p2, p0, Lcom/starmicronics/starioextension/StarIoExtManager$3;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {p2}, Lcom/starmicronics/starioextension/StarIoExtManager;->l(Lcom/starmicronics/starioextension/StarIoExtManager;)Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x7

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-direct {p0}, Lcom/starmicronics/starioextension/StarIoExtManager$3;->a()V

    :cond_3
    return-void

    :cond_4
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const-string v0, "^usb:.+\\-.+"

    const/16 v2, 0x18

    if-lt p2, v2, :cond_6

    iget-object p2, p0, Lcom/starmicronics/starioextension/StarIoExtManager$3;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {p2}, Lcom/starmicronics/starioextension/StarIoExtManager;->l(Lcom/starmicronics/starioextension/StarIoExtManager;)Ljava/lang/String;

    move-result-object p2

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_6

    iget-object p2, p0, Lcom/starmicronics/starioextension/StarIoExtManager$3;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {p2}, Lcom/starmicronics/starioextension/StarIoExtManager;->l(Lcom/starmicronics/starioextension/StarIoExtManager;)Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/starmicronics/starioextension/bs;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    invoke-direct {p0}, Lcom/starmicronics/starioextension/StarIoExtManager$3;->a()V

    :cond_5
    return-void

    :cond_6
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge p2, v2, :cond_b

    iget-object p2, p0, Lcom/starmicronics/starioextension/StarIoExtManager$3;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {p2}, Lcom/starmicronics/starioextension/StarIoExtManager;->l(Lcom/starmicronics/starioextension/StarIoExtManager;)Ljava/lang/String;

    move-result-object p2

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p2, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_7

    iget-object p2, p0, Lcom/starmicronics/starioextension/StarIoExtManager$3;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {p2}, Lcom/starmicronics/starioextension/StarIoExtManager;->l(Lcom/starmicronics/starioextension/StarIoExtManager;)Ljava/lang/String;

    move-result-object p2

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p2, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_b

    :cond_7
    iget-object p2, p0, Lcom/starmicronics/starioextension/StarIoExtManager$3;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {p2}, Lcom/starmicronics/starioextension/StarIoExtManager;->m(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/bs;

    move-result-object p2

    if-nez p2, :cond_8

    return-void

    :cond_8
    iget-object p2, p0, Lcom/starmicronics/starioextension/StarIoExtManager$3;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {p2}, Lcom/starmicronics/starioextension/StarIoExtManager;->m(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/bs;

    move-result-object p2

    const-string v0, "busnum"

    invoke-virtual {p2, v0}, Lcom/starmicronics/starioextension/bs;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$3;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->m(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/bs;

    move-result-object v0

    const-string v1, "devnum"

    invoke-virtual {v0, v1}, Lcom/starmicronics/starioextension/bs;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "^0+"

    const-string v2, ""

    invoke-virtual {p2, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz p2, :cond_9

    if-eqz v0, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "-"

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_9
    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/starmicronics/starioextension/bs;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_a

    invoke-direct {p0}, Lcom/starmicronics/starioextension/StarIoExtManager$3;->a()V

    :cond_a
    return-void

    :cond_b
    invoke-direct {p0}, Lcom/starmicronics/starioextension/StarIoExtManager$3;->a()V

    return-void
.end method
