.class final Lcom/squareup/pushmessages/RealPushServiceRegistrar$unregister$1;
.super Ljava/lang/Object;
.source "RealPushServiceRegistrar.kt"

# interfaces
.implements Lio/reactivex/functions/Action;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/pushmessages/RealPushServiceRegistrar;->unregister()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/pushmessages/RealPushServiceRegistrar;


# direct methods
.method constructor <init>(Lcom/squareup/pushmessages/RealPushServiceRegistrar;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar$unregister$1;->this$0:Lcom/squareup/pushmessages/RealPushServiceRegistrar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 107
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar$unregister$1;->this$0:Lcom/squareup/pushmessages/RealPushServiceRegistrar;

    invoke-static {v0}, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->access$getPushServiceEnabledPreference$p(Lcom/squareup/pushmessages/RealPushServiceRegistrar;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    .line 108
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushServiceRegistrar$unregister$1;->this$0:Lcom/squareup/pushmessages/RealPushServiceRegistrar;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/squareup/pushmessages/RealPushServiceRegistrar;->access$storeRegistrationToken(Lcom/squareup/pushmessages/RealPushServiceRegistrar;Ljava/lang/String;)V

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "[PUSH] Unregistered from push service locally."

    .line 109
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
