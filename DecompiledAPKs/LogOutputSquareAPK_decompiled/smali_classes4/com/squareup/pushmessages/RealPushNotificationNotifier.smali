.class public Lcom/squareup/pushmessages/RealPushNotificationNotifier;
.super Ljava/lang/Object;
.source "RealPushNotificationNotifier.java"

# interfaces
.implements Lcom/squareup/pushmessages/PushNotificationNotifier;
.implements Lmortar/Scoped;


# instance fields
.field private final appContext:Landroid/content/Context;

.field private final notificationIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationManager:Landroid/app/NotificationManager;

.field private final notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

.field private final pushNotifications:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/pushmessages/PushMessage$PushNotification;",
            ">;"
        }
    .end annotation
.end field

.field private final random:Ljava/util/Random;


# direct methods
.method constructor <init>(Landroid/app/Application;Landroid/app/NotificationManager;Lcom/squareup/pushmessages/PushMessageDelegate;Lcom/squareup/notification/NotificationWrapper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier;->appContext:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier;->notificationManager:Landroid/app/NotificationManager;

    .line 43
    const-class p1, Lcom/squareup/pushmessages/PushMessage$PushNotification;

    invoke-interface {p3, p1}, Lcom/squareup/pushmessages/PushMessageDelegate;->observe(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier;->pushNotifications:Lio/reactivex/Observable;

    .line 44
    iput-object p4, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    .line 46
    new-instance p1, Ljava/util/Random;

    invoke-direct {p1}, Ljava/util/Random;-><init>()V

    iput-object p1, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier;->random:Ljava/util/Random;

    .line 47
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier;->notificationIds:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier;->pushNotifications:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/pushmessages/-$$Lambda$O4vgm8jr_4K3lGFamzQk7r6uELA;

    invoke-direct {v1, p0}, Lcom/squareup/pushmessages/-$$Lambda$O4vgm8jr_4K3lGFamzQk7r6uELA;-><init>(Lcom/squareup/pushmessages/RealPushNotificationNotifier;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 3

    .line 75
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier;->notificationIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 76
    iget-object v2, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier;->notificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public showNotification(Lcom/squareup/pushmessages/PushMessage$PushNotification;)V
    .locals 4

    .line 56
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier;->appContext:Landroid/content/Context;

    .line 57
    invoke-virtual {p1}, Lcom/squareup/pushmessages/PushMessage$PushNotification;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/pushmessages/PushMessage$PushNotification;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    :cond_0
    sget-object v1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 56
    :goto_0
    invoke-static {v0, v1}, Lcom/squareup/ui/PaymentActivity;->createPendingIntentForDeepLink(Landroid/content/Context;Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 59
    iget-object v1, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    iget-object v2, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier;->appContext:Landroid/content/Context;

    sget-object v3, Lcom/squareup/notification/Channels;->CUSTOMER_MESSAGES:Lcom/squareup/notification/Channels;

    .line 60
    invoke-virtual {v1, v2, v3}, Lcom/squareup/notification/NotificationWrapper;->getNotificationBuilder(Landroid/content/Context;Lcom/squareup/notification/Channel;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    .line 61
    invoke-virtual {p1}, Lcom/squareup/pushmessages/PushMessage$PushNotification;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    .line 62
    invoke-virtual {p1}, Lcom/squareup/pushmessages/PushMessage$PushNotification;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    .line 63
    invoke-virtual {p1}, Lcom/squareup/pushmessages/PushMessage$PushNotification;->getBody()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroidx/core/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    .line 64
    invoke-virtual {p1, v0}, Landroidx/core/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    new-instance v0, Landroidx/core/app/NotificationCompat$BigTextStyle;

    invoke-direct {v0}, Landroidx/core/app/NotificationCompat$BigTextStyle;-><init>()V

    .line 65
    invoke-virtual {p1, v0}, Landroidx/core/app/NotificationCompat$Builder;->setStyle(Landroidx/core/app/NotificationCompat$Style;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object p1

    .line 66
    invoke-virtual {p1}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object p1

    .line 68
    iget-object v0, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier;->random:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    .line 69
    iget-object v1, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier;->notificationIds:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    iget-object v1, p0, Lcom/squareup/pushmessages/RealPushNotificationNotifier;->notificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v1, v0, p1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method
