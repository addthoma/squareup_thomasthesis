.class final enum Lcom/squareup/pushmessages/PushMessageOp$3;
.super Lcom/squareup/pushmessages/PushMessageOp;
.source "PushMessageOp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/pushmessages/PushMessageOp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 56
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/pushmessages/PushMessageOp;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/pushmessages/PushMessageOp$1;)V

    return-void
.end method


# virtual methods
.method createMessage(Ljava/util/Map;)Lcom/squareup/pushmessages/PushMessage;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/pushmessages/PushMessage;"
        }
    .end annotation

    .line 58
    invoke-static {p1}, Lcom/squareup/pushmessages/PushMessageOp;->access$100(Ljava/util/Map;)Lcom/squareup/pushmessages/PushMessageOp$Alert;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 59
    new-instance v0, Lcom/squareup/pushmessages/PushMessage$SupportMessagingNotification;

    iget-object v1, p1, Lcom/squareup/pushmessages/PushMessageOp$Alert;->title:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/pushmessages/PushMessageOp$Alert;->body:Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Lcom/squareup/pushmessages/PushMessage$SupportMessagingNotification;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method
