.class public final Lcom/squareup/pushmessages/PushMessageLoggedInModule_ProvidePushServicesAsSetForLoggedInFactory;
.super Ljava/lang/Object;
.source "PushMessageLoggedInModule_ProvidePushServicesAsSetForLoggedInFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/Set<",
        "Lmortar/Scoped;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final pushNotificationNotifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/RealPushNotificationNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private final registrarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/RealPushServiceRegistrar;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/RealPushNotificationNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/RealPushServiceRegistrar;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/pushmessages/PushMessageLoggedInModule_ProvidePushServicesAsSetForLoggedInFactory;->pushNotificationNotifierProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/pushmessages/PushMessageLoggedInModule_ProvidePushServicesAsSetForLoggedInFactory;->registrarProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/pushmessages/PushMessageLoggedInModule_ProvidePushServicesAsSetForLoggedInFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/RealPushNotificationNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/RealPushServiceRegistrar;",
            ">;)",
            "Lcom/squareup/pushmessages/PushMessageLoggedInModule_ProvidePushServicesAsSetForLoggedInFactory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/pushmessages/PushMessageLoggedInModule_ProvidePushServicesAsSetForLoggedInFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/pushmessages/PushMessageLoggedInModule_ProvidePushServicesAsSetForLoggedInFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static providePushServicesAsSetForLoggedIn(Lcom/squareup/pushmessages/RealPushNotificationNotifier;Lcom/squareup/pushmessages/RealPushServiceRegistrar;)Ljava/util/Set;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/pushmessages/RealPushNotificationNotifier;",
            "Lcom/squareup/pushmessages/RealPushServiceRegistrar;",
            ")",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation

    .line 43
    invoke-static {p0, p1}, Lcom/squareup/pushmessages/PushMessageLoggedInModule;->providePushServicesAsSetForLoggedIn(Lcom/squareup/pushmessages/RealPushNotificationNotifier;Lcom/squareup/pushmessages/RealPushServiceRegistrar;)Ljava/util/Set;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Set;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/pushmessages/PushMessageLoggedInModule_ProvidePushServicesAsSetForLoggedInFactory;->get()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/pushmessages/PushMessageLoggedInModule_ProvidePushServicesAsSetForLoggedInFactory;->pushNotificationNotifierProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/pushmessages/RealPushNotificationNotifier;

    iget-object v1, p0, Lcom/squareup/pushmessages/PushMessageLoggedInModule_ProvidePushServicesAsSetForLoggedInFactory;->registrarProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/pushmessages/RealPushServiceRegistrar;

    invoke-static {v0, v1}, Lcom/squareup/pushmessages/PushMessageLoggedInModule_ProvidePushServicesAsSetForLoggedInFactory;->providePushServicesAsSetForLoggedIn(Lcom/squareup/pushmessages/RealPushNotificationNotifier;Lcom/squareup/pushmessages/RealPushServiceRegistrar;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
