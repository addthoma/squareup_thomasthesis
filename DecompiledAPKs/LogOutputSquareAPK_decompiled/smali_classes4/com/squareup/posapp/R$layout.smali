.class public final Lcom/squareup/posapp/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/posapp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f0d0000

.field public static final abc_action_bar_up_container:I = 0x7f0d0001

.field public static final abc_action_menu_item_layout:I = 0x7f0d0002

.field public static final abc_action_menu_layout:I = 0x7f0d0003

.field public static final abc_action_mode_bar:I = 0x7f0d0004

.field public static final abc_action_mode_close_item_material:I = 0x7f0d0005

.field public static final abc_activity_chooser_view:I = 0x7f0d0006

.field public static final abc_activity_chooser_view_list_item:I = 0x7f0d0007

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f0d0008

.field public static final abc_alert_dialog_material:I = 0x7f0d0009

.field public static final abc_alert_dialog_title_material:I = 0x7f0d000a

.field public static final abc_cascading_menu_item_layout:I = 0x7f0d000b

.field public static final abc_dialog_title_material:I = 0x7f0d000c

.field public static final abc_expanded_menu_layout:I = 0x7f0d000d

.field public static final abc_list_menu_item_checkbox:I = 0x7f0d000e

.field public static final abc_list_menu_item_icon:I = 0x7f0d000f

.field public static final abc_list_menu_item_layout:I = 0x7f0d0010

.field public static final abc_list_menu_item_radio:I = 0x7f0d0011

.field public static final abc_popup_menu_header_item_layout:I = 0x7f0d0012

.field public static final abc_popup_menu_item_layout:I = 0x7f0d0013

.field public static final abc_screen_content_include:I = 0x7f0d0014

.field public static final abc_screen_simple:I = 0x7f0d0015

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f0d0016

.field public static final abc_screen_toolbar:I = 0x7f0d0017

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f0d0018

.field public static final abc_search_view:I = 0x7f0d0019

.field public static final abc_select_dialog_material:I = 0x7f0d001a

.field public static final abc_tooltip:I = 0x7f0d001b

.field public static final about:I = 0x7f0d001c

.field public static final about_device_setting_layout:I = 0x7f0d001d

.field public static final about_device_settings_view:I = 0x7f0d001e

.field public static final accessible_keypad:I = 0x7f0d001f

.field public static final accessible_pin_tutorial_done_layout:I = 0x7f0d0020

.field public static final accessible_pin_tutorial_step_layout:I = 0x7f0d0021

.field public static final action_bar_for_card:I = 0x7f0d0022

.field public static final action_bar_no_background:I = 0x7f0d0023

.field public static final action_bar_view:I = 0x7f0d0024

.field public static final action_bar_white:I = 0x7f0d0025

.field public static final actionbar_header:I = 0x7f0d0026

.field public static final activate_via_web_view:I = 0x7f0d0027

.field public static final activate_your_account_view:I = 0x7f0d0028

.field public static final activation_retry_view:I = 0x7f0d0029

.field public static final activity_api:I = 0x7f0d002a

.field public static final activity_applet_bill_history_detail_view:I = 0x7f0d002b

.field public static final activity_applet_bill_history_view:I = 0x7f0d002c

.field public static final activity_applet_bill_history_view_loyalty_section:I = 0x7f0d002d

.field public static final activity_applet_bulk_settle_button:I = 0x7f0d002e

.field public static final activity_applet_bulk_settle_header_row:I = 0x7f0d002f

.field public static final activity_applet_bulk_settle_sort_row:I = 0x7f0d0030

.field public static final activity_applet_bulk_settle_tender_row:I = 0x7f0d0031

.field public static final activity_applet_bulk_settle_view:I = 0x7f0d0032

.field public static final activity_applet_bulk_settle_view_all_row:I = 0x7f0d0033

.field public static final activity_applet_contactless_cp_refund_view:I = 0x7f0d0034

.field public static final activity_applet_instant_deposit_row_list:I = 0x7f0d0035

.field public static final activity_applet_instant_deposit_row_sidebar:I = 0x7f0d0036

.field public static final activity_applet_issue_receipt_view:I = 0x7f0d0037

.field public static final activity_applet_issue_refund_done_view:I = 0x7f0d0038

.field public static final activity_applet_issue_refund_view:I = 0x7f0d0039

.field public static final activity_applet_list_header_padding_row:I = 0x7f0d003a

.field public static final activity_applet_list_header_row:I = 0x7f0d003b

.field public static final activity_applet_list_row:I = 0x7f0d003c

.field public static final activity_applet_quicktip_editor:I = 0x7f0d003d

.field public static final activity_applet_refund_error_view:I = 0x7f0d003e

.field public static final activity_applet_refund_itemizations_view:I = 0x7f0d003f

.field public static final activity_applet_refund_tender_row:I = 0x7f0d0040

.field public static final activity_applet_restock_on_itemized_refund_view:I = 0x7f0d0041

.field public static final activity_applet_select_gift_receipt_tender_view:I = 0x7f0d0042

.field public static final activity_applet_select_receipt_tender_view:I = 0x7f0d0043

.field public static final activity_applet_select_refund_tender_view:I = 0x7f0d0044

.field public static final activity_applet_sidebar_search_message:I = 0x7f0d0045

.field public static final activity_applet_tip_options_view:I = 0x7f0d0046

.field public static final activity_applet_transactions_history_list:I = 0x7f0d0047

.field public static final activity_applet_transactions_history_view:I = 0x7f0d0048

.field public static final add_amount_to_account:I = 0x7f0d0049

.field public static final add_note_view:I = 0x7f0d004a

.field public static final add_paid_in_out_view:I = 0x7f0d004b

.field public static final add_payment_view:I = 0x7f0d004c

.field public static final additional_info_view:I = 0x7f0d004d

.field public static final additional_recipients_view:I = 0x7f0d004e

.field public static final adjust_inventory_select_reason:I = 0x7f0d004f

.field public static final adjust_inventory_specify_number:I = 0x7f0d0050

.field public static final alert_dialog_holo:I = 0x7f0d0051

.field public static final all_disputes_view:I = 0x7f0d0052

.field public static final all_items_or_services_detail_view:I = 0x7f0d0053

.field public static final amount_cell:I = 0x7f0d0054

.field public static final announcement_details:I = 0x7f0d0055

.field public static final announcements:I = 0x7f0d0056

.field public static final announcements_row:I = 0x7f0d0057

.field public static final app_feedback_confirmation_view:I = 0x7f0d0058

.field public static final app_feedback_view:I = 0x7f0d0059

.field public static final applet_banner_list:I = 0x7f0d005a

.field public static final applet_banner_sidebar:I = 0x7f0d005b

.field public static final applet_header_list:I = 0x7f0d005c

.field public static final applet_header_sidebar:I = 0x7f0d005d

.field public static final applet_list_row:I = 0x7f0d005e

.field public static final applet_master_view:I = 0x7f0d005f

.field public static final applet_sidebar_row:I = 0x7f0d0060

.field public static final applets_drawer_contents:I = 0x7f0d0061

.field public static final applets_drawer_item:I = 0x7f0d0062

.field public static final applied_locations_banner:I = 0x7f0d0063

.field public static final assert_never_shown_view:I = 0x7f0d0064

.field public static final assign_option_to_item:I = 0x7f0d0065

.field public static final attribute_pair_row:I = 0x7f0d0066

.field public static final audio_permission_card_view:I = 0x7f0d0067

.field public static final audio_permission_layout:I = 0x7f0d0068

.field public static final audio_permission_screen_view:I = 0x7f0d0069

.field public static final auth_glyph_view:I = 0x7f0d006a

.field public static final auth_spinner_glyph:I = 0x7f0d006b

.field public static final auth_spinner_view:I = 0x7f0d006c

.field public static final auth_square_card_personal_info_view:I = 0x7f0d006d

.field public static final auth_square_card_ssn_info_view:I = 0x7f0d006e

.field public static final auto_complete_row:I = 0x7f0d006f

.field public static final background_star:I = 0x7f0d0070

.field public static final balance_activities_empty:I = 0x7f0d0071

.field public static final balance_activities_error:I = 0x7f0d0072

.field public static final balance_activities_layout:I = 0x7f0d0073

.field public static final balance_activities_list:I = 0x7f0d0074

.field public static final balance_activities_load_more:I = 0x7f0d0075

.field public static final balance_activities_view_item_title:I = 0x7f0d0076

.field public static final balance_activity_community_reward_layout:I = 0x7f0d0077

.field public static final balance_activity_details_body_layout:I = 0x7f0d0078

.field public static final balance_activity_details_error_layout:I = 0x7f0d0079

.field public static final balance_activity_details_expense_type_checkable:I = 0x7f0d007a

.field public static final balance_activity_details_layout:I = 0x7f0d007b

.field public static final balance_activity_details_loading_layout:I = 0x7f0d007c

.field public static final balance_applet_list_row:I = 0x7f0d007d

.field public static final balance_applet_master_view:I = 0x7f0d007e

.field public static final balance_header_button_layout:I = 0x7f0d007f

.field public static final balance_header_view:I = 0x7f0d0080

.field public static final balance_spinner:I = 0x7f0d0081

.field public static final balance_transaction_detail_view:I = 0x7f0d0082

.field public static final balance_transactions_view:I = 0x7f0d0083

.field public static final bank_account_fields_ca:I = 0x7f0d0084

.field public static final bank_account_fields_us:I = 0x7f0d0085

.field public static final bank_account_settings_view:I = 0x7f0d0086

.field public static final bank_account_view_contents:I = 0x7f0d0087

.field public static final barcode_not_found:I = 0x7f0d0088

.field public static final barcode_scanners_settings_view:I = 0x7f0d0089

.field public static final bill_history_entry_row_contents:I = 0x7f0d008a

.field public static final bill_history_items_section:I = 0x7f0d008b

.field public static final bill_history_related_bill_section:I = 0x7f0d008c

.field public static final bill_history_related_bill_section_header:I = 0x7f0d008d

.field public static final bill_history_tax_breakdown_row:I = 0x7f0d008e

.field public static final bill_history_tax_breakdown_section:I = 0x7f0d008f

.field public static final bill_history_tender_section:I = 0x7f0d0090

.field public static final bill_history_tender_section_customer_row:I = 0x7f0d0091

.field public static final bill_history_tender_section_header:I = 0x7f0d0092

.field public static final bill_history_total_section:I = 0x7f0d0093

.field public static final bizbank_progress_layout:I = 0x7f0d0094

.field public static final bottom_sheet_dialog_open_in_browser:I = 0x7f0d0095

.field public static final bran_cart_list_footer:I = 0x7f0d0096

.field public static final bran_cart_list_header:I = 0x7f0d0097

.field public static final bran_cart_list_item:I = 0x7f0d0098

.field public static final bran_cart_section_header:I = 0x7f0d0099

.field public static final browser:I = 0x7f0d009a

.field public static final bug_report_dialog_input:I = 0x7f0d009b

.field public static final business_address_view:I = 0x7f0d009c

.field public static final business_info_view:I = 0x7f0d009d

.field public static final buyer_action_container:I = 0x7f0d009e

.field public static final buyer_cart_layout:I = 0x7f0d009f

.field public static final buyer_cart_list_header:I = 0x7f0d00a0

.field public static final buyer_cart_list_item:I = 0x7f0d00a1

.field public static final buyer_flow_action_bar:I = 0x7f0d00a2

.field public static final buyer_flow_action_bar_new_sale:I = 0x7f0d00a3

.field public static final buyer_flow_action_bar_new_sale_portrait:I = 0x7f0d00a4

.field public static final buyer_input_view:I = 0x7f0d00a5

.field public static final buyer_language_select_view:I = 0x7f0d00a6

.field public static final buyer_noho_action_bar:I = 0x7f0d00a7

.field public static final buyer_noho_action_bar_condensed:I = 0x7f0d00a8

.field public static final buyer_order_ticket_name_view:I = 0x7f0d00a9

.field public static final buyer_order_ticket_name_view_contents:I = 0x7f0d00aa

.field public static final cancel_invoice_view:I = 0x7f0d00ab

.field public static final cancel_square_card_reasons_view:I = 0x7f0d00ac

.field public static final cancel_square_card_view:I = 0x7f0d00ad

.field public static final canceled_card_feedback_view:I = 0x7f0d00ae

.field public static final capital_current_plan_layout:I = 0x7f0d00af

.field public static final capital_error_layout:I = 0x7f0d00b0

.field public static final capital_flex_application_pending_layout:I = 0x7f0d00b1

.field public static final capital_flex_offer_layout:I = 0x7f0d00b2

.field public static final capital_loading_layout:I = 0x7f0d00b3

.field public static final capital_no_offer_layout:I = 0x7f0d00b4

.field public static final card_dialog_layout:I = 0x7f0d00b5

.field public static final card_editor:I = 0x7f0d00b6

.field public static final card_expiration_cvv_editor:I = 0x7f0d00b7

.field public static final card_on_file_offline_not_available:I = 0x7f0d00b8

.field public static final card_processing_not_activated_view:I = 0x7f0d00b9

.field public static final card_processing_workflow:I = 0x7f0d00ba

.field public static final card_reader_detail_card_view:I = 0x7f0d00bb

.field public static final card_reader_detail_layout:I = 0x7f0d00bc

.field public static final card_reader_detail_screen_view:I = 0x7f0d00bd

.field public static final card_reader_status_row:I = 0x7f0d00be

.field public static final card_readers_card_view:I = 0x7f0d00bf

.field public static final card_readers_screen_view:I = 0x7f0d00c0

.field public static final cardreaders_ble_footer_view:I = 0x7f0d00c1

.field public static final cardreaders_header_view:I = 0x7f0d00c2

.field public static final cardreaders_learn_more:I = 0x7f0d00c3

.field public static final cart_container_view:I = 0x7f0d00c4

.field public static final cart_discounts_view:I = 0x7f0d00c5

.field public static final cart_entry_image_wrapper_layout:I = 0x7f0d00c6

.field public static final cart_entry_indented_wrapper_layout:I = 0x7f0d00c7

.field public static final cart_entry_row:I = 0x7f0d00c8

.field public static final cart_entry_row_contents:I = 0x7f0d00c9

.field public static final cart_menu_view:I = 0x7f0d00ca

.field public static final cart_recycler_view:I = 0x7f0d00cb

.field public static final cart_sale_list_row:I = 0x7f0d00cc

.field public static final cart_taxes_view:I = 0x7f0d00cd

.field public static final cart_ticket_line_row:I = 0x7f0d00ce

.field public static final cart_ticket_note_list_row:I = 0x7f0d00cf

.field public static final cash_drawer_details:I = 0x7f0d00d0

.field public static final cash_drawer_settings_view:I = 0x7f0d00d1

.field public static final cash_management_disabled_state:I = 0x7f0d00d2

.field public static final cash_management_settings_base_view:I = 0x7f0d00d3

.field public static final cash_management_settings_card_view:I = 0x7f0d00d4

.field public static final cash_management_settings_section_view:I = 0x7f0d00d5

.field public static final categories_list_view:I = 0x7f0d00d6

.field public static final category_assignment_item_row:I = 0x7f0d00d7

.field public static final category_detail_view:I = 0x7f0d00d8

.field public static final category_drop_down_row:I = 0x7f0d00d9

.field public static final category_edit_list_row:I = 0x7f0d00da

.field public static final category_view_content:I = 0x7f0d00db

.field public static final challenge_summary_view:I = 0x7f0d00dc

.field public static final charge_and_tickets_button_content:I = 0x7f0d00dd

.field public static final check_bank_account_info_view:I = 0x7f0d00de

.field public static final check_box_list_row:I = 0x7f0d00df

.field public static final check_password_view:I = 0x7f0d00e0

.field public static final check_row:I = 0x7f0d00e1

.field public static final checkable_preserved_label_row:I = 0x7f0d00e2

.field public static final checkout_applet_main_view:I = 0x7f0d00e3

.field public static final choose_date_view:I = 0x7f0d00e4

.field public static final choose_eligible_item_screen:I = 0x7f0d00e5

.field public static final city_spinner:I = 0x7f0d00e6

.field public static final city_spinner_dropdown:I = 0x7f0d00e7

.field public static final clock_in_or_continue_view:I = 0x7f0d00e8

.field public static final clock_in_out_view:I = 0x7f0d00e9

.field public static final clock_skew_view:I = 0x7f0d00ea

.field public static final close_of_day_dialog_layout:I = 0x7f0d00eb

.field public static final collapsing_header_view:I = 0x7f0d00ec

.field public static final compact_loyalty_reward_row:I = 0x7f0d00ed

.field public static final comparison_range_row:I = 0x7f0d00ee

.field public static final configure_item_buttons:I = 0x7f0d00ef

.field public static final configure_item_checkable_row:I = 0x7f0d00f0

.field public static final configure_item_checkablegroup:I = 0x7f0d00f1

.field public static final configure_item_checkablegroup_noho:I = 0x7f0d00f2

.field public static final configure_item_checkablegroup_section:I = 0x7f0d00f3

.field public static final configure_item_description:I = 0x7f0d00f4

.field public static final configure_item_detail_view:I = 0x7f0d00f5

.field public static final configure_item_discounts:I = 0x7f0d00f6

.field public static final configure_item_duration_section:I = 0x7f0d00f7

.field public static final configure_item_fixed_price_override_section:I = 0x7f0d00f8

.field public static final configure_item_gap_time_section:I = 0x7f0d00f9

.field public static final configure_item_notes_section:I = 0x7f0d00fa

.field public static final configure_item_price_view:I = 0x7f0d00fb

.field public static final configure_item_quantity_section:I = 0x7f0d00fc

.field public static final configure_item_taxes:I = 0x7f0d00fd

.field public static final configure_item_title_section:I = 0x7f0d00fe

.field public static final configure_item_variable_price_item_variation_section:I = 0x7f0d00ff

.field public static final configure_item_variable_price_section:I = 0x7f0d0100

.field public static final confirm_dialog_layout:I = 0x7f0d0101

.field public static final confirm_identity_question:I = 0x7f0d0102

.field public static final confirm_identity_view:I = 0x7f0d0103

.field public static final confirm_magstripe_address_view:I = 0x7f0d0104

.field public static final confirm_transfer_view:I = 0x7f0d0105

.field public static final confirmation_view:I = 0x7f0d0106

.field public static final connected_scales_layout:I = 0x7f0d0107

.field public static final counter_view:I = 0x7f0d0108

.field public static final coupon_container:I = 0x7f0d0109

.field public static final coupon_reward_row:I = 0x7f0d010a

.field public static final coupon_ribbon:I = 0x7f0d010b

.field public static final create_account_view:I = 0x7f0d010c

.field public static final crm_activity_list_row:I = 0x7f0d010d

.field public static final crm_add_coupon_row:I = 0x7f0d010e

.field public static final crm_add_coupon_view:I = 0x7f0d010f

.field public static final crm_add_customers_to_group_view:I = 0x7f0d0110

.field public static final crm_adjust_points:I = 0x7f0d0111

.field public static final crm_all_frequent_items_view:I = 0x7f0d0112

.field public static final crm_all_notes_view:I = 0x7f0d0113

.field public static final crm_appointment_row:I = 0x7f0d0114

.field public static final crm_bill_history_card_view:I = 0x7f0d0115

.field public static final crm_birthday_picker_view:I = 0x7f0d0116

.field public static final crm_birthday_popup:I = 0x7f0d0117

.field public static final crm_choose_card_on_file_view:I = 0x7f0d0118

.field public static final crm_choose_customer_list_create_customer_row:I = 0x7f0d0119

.field public static final crm_choose_customer_screen:I = 0x7f0d011a

.field public static final crm_choose_filter_card_view:I = 0x7f0d011b

.field public static final crm_choose_filters_card_view:I = 0x7f0d011c

.field public static final crm_choose_groups_view:I = 0x7f0d011d

.field public static final crm_choose_reward_item:I = 0x7f0d011e

.field public static final crm_contact_list_bottom_row:I = 0x7f0d011f

.field public static final crm_conversation_card_view:I = 0x7f0d0120

.field public static final crm_conversation_coupon_right_row:I = 0x7f0d0121

.field public static final crm_conversation_message_left_row:I = 0x7f0d0122

.field public static final crm_conversation_message_right_row:I = 0x7f0d0123

.field public static final crm_conversation_view:I = 0x7f0d0124

.field public static final crm_create_group_view:I = 0x7f0d0125

.field public static final crm_create_note_view:I = 0x7f0d0126

.field public static final crm_custom_date_attr_popup:I = 0x7f0d0127

.field public static final crm_customer_activity_view:I = 0x7f0d0128

.field public static final crm_customer_all_appointments_view:I = 0x7f0d0129

.field public static final crm_customer_card_view:I = 0x7f0d012a

.field public static final crm_customer_email_view:I = 0x7f0d012b

.field public static final crm_customer_invoice_view:I = 0x7f0d012c

.field public static final crm_customer_list_error_row:I = 0x7f0d012d

.field public static final crm_customer_list_header_row:I = 0x7f0d012e

.field public static final crm_customer_list_loading_row:I = 0x7f0d012f

.field public static final crm_customer_lookup_view:I = 0x7f0d0130

.field public static final crm_customer_management_settings_view:I = 0x7f0d0131

.field public static final crm_customer_save_card_view:I = 0x7f0d0132

.field public static final crm_customer_verify_postalcode_view:I = 0x7f0d0133

.field public static final crm_display_reward_by_code:I = 0x7f0d0134

.field public static final crm_edit_address_row:I = 0x7f0d0135

.field public static final crm_edit_boolean_attribute_row:I = 0x7f0d0136

.field public static final crm_edit_customer_enum_view:I = 0x7f0d0137

.field public static final crm_edit_customer_view:I = 0x7f0d0138

.field public static final crm_edit_email_attribute_row:I = 0x7f0d0139

.field public static final crm_edit_email_row:I = 0x7f0d013a

.field public static final crm_edit_enum_attribute_row:I = 0x7f0d013b

.field public static final crm_edit_filter_card_view:I = 0x7f0d013c

.field public static final crm_edit_name_row:I = 0x7f0d013d

.field public static final crm_edit_number_attribute_row:I = 0x7f0d013e

.field public static final crm_edit_phone_attribute_row:I = 0x7f0d013f

.field public static final crm_edit_text_attribute_row:I = 0x7f0d0140

.field public static final crm_email_collection_settings_view:I = 0x7f0d0141

.field public static final crm_empty_filter_content:I = 0x7f0d0142

.field public static final crm_frequent_item_row:I = 0x7f0d0143

.field public static final crm_frequent_item_section_row:I = 0x7f0d0144

.field public static final crm_frequent_items_section_view:I = 0x7f0d0145

.field public static final crm_group_edit_view:I = 0x7f0d0146

.field public static final crm_groups_choose_groups_view:I = 0x7f0d0147

.field public static final crm_groups_create_group_view:I = 0x7f0d0148

.field public static final crm_lookup_reward_by_code:I = 0x7f0d0149

.field public static final crm_merge_customers_confirmation_view:I = 0x7f0d014a

.field public static final crm_merge_proposal_item_view:I = 0x7f0d014b

.field public static final crm_multi_option_filter_content:I = 0x7f0d014c

.field public static final crm_multi_select_row:I = 0x7f0d014d

.field public static final crm_note_row:I = 0x7f0d014e

.field public static final crm_notes_section_view_v2:I = 0x7f0d014f

.field public static final crm_profile_attachments_image_preview:I = 0x7f0d0150

.field public static final crm_profile_attachments_overflow_bottom_sheet_dialog:I = 0x7f0d0151

.field public static final crm_profile_attachments_rename_file:I = 0x7f0d0152

.field public static final crm_profile_attachments_upload:I = 0x7f0d0153

.field public static final crm_profile_attachments_upload_bottom_sheet:I = 0x7f0d0154

.field public static final crm_profile_attachments_view:I = 0x7f0d0155

.field public static final crm_recent_customer_row:I = 0x7f0d0156

.field public static final crm_redeem_points_v2:I = 0x7f0d0157

.field public static final crm_reminder_view:I = 0x7f0d0158

.field public static final crm_resolve_duplicates_card_view:I = 0x7f0d0159

.field public static final crm_review_customer_for_merging_view:I = 0x7f0d015a

.field public static final crm_reward_row:I = 0x7f0d015b

.field public static final crm_save_filters_card_view:I = 0x7f0d015c

.field public static final crm_search_customer_row:I = 0x7f0d015d

.field public static final crm_select_loyalty_phone_number_card_view:I = 0x7f0d015e

.field public static final crm_selectable_loyalty_phone_row:I = 0x7f0d015f

.field public static final crm_send_message_view:I = 0x7f0d0160

.field public static final crm_single_option_filter_content:I = 0x7f0d0161

.field public static final crm_single_select_row:I = 0x7f0d0162

.field public static final crm_single_text_filter_content:I = 0x7f0d0163

.field public static final crm_summary_row:I = 0x7f0d0164

.field public static final crm_unknown_attribute_row:I = 0x7f0d0165

.field public static final crm_update_group2_view:I = 0x7f0d0166

.field public static final crm_update_loyalty_phone:I = 0x7f0d0167

.field public static final crm_v2_activity_list_section:I = 0x7f0d0168

.field public static final crm_v2_all_customers_list:I = 0x7f0d0169

.field public static final crm_v2_cardonfile_card_row:I = 0x7f0d016a

.field public static final crm_v2_choose_customer2_view:I = 0x7f0d016b

.field public static final crm_v2_choose_customer3_view:I = 0x7f0d016c

.field public static final crm_v2_choose_customer_to_redeem_reward:I = 0x7f0d016d

.field public static final crm_v2_choose_customer_to_save_card_view:I = 0x7f0d016e

.field public static final crm_v2_choose_enum_attribute_view:I = 0x7f0d016f

.field public static final crm_v2_contact_edit_view:I = 0x7f0d0170

.field public static final crm_v2_contact_list_top_row_create_customer:I = 0x7f0d0171

.field public static final crm_v2_conversation_view:I = 0x7f0d0172

.field public static final crm_v2_create_manual_group_view:I = 0x7f0d0173

.field public static final crm_v2_customer_unit_row:I = 0x7f0d0174

.field public static final crm_v2_customers_list_content:I = 0x7f0d0175

.field public static final crm_v2_edit_attribute_row_boolean:I = 0x7f0d0176

.field public static final crm_v2_edit_attribute_row_date:I = 0x7f0d0177

.field public static final crm_v2_edit_attribute_row_email:I = 0x7f0d0178

.field public static final crm_v2_edit_attribute_row_enum:I = 0x7f0d0179

.field public static final crm_v2_edit_attribute_row_number:I = 0x7f0d017a

.field public static final crm_v2_edit_attribute_row_phone:I = 0x7f0d017b

.field public static final crm_v2_edit_attribute_row_text:I = 0x7f0d017c

.field public static final crm_v2_edit_attribute_row_unknown:I = 0x7f0d017d

.field public static final crm_v2_expiring_points_row:I = 0x7f0d017e

.field public static final crm_v2_expiring_points_view:I = 0x7f0d017f

.field public static final crm_v2_filter_bubble:I = 0x7f0d0180

.field public static final crm_v2_filter_layout:I = 0x7f0d0181

.field public static final crm_v2_groups_list_row:I = 0x7f0d0182

.field public static final crm_v2_intial_circle_view:I = 0x7f0d0183

.field public static final crm_v2_list_header_row:I = 0x7f0d0184

.field public static final crm_v2_loyalty_section_dropdown_dialog:I = 0x7f0d0185

.field public static final crm_v2_loyalty_section_view:I = 0x7f0d0186

.field public static final crm_v2_manage_coupons_and_rewards_row_checkable:I = 0x7f0d0187

.field public static final crm_v2_manage_coupons_and_rewards_row_uncheckable:I = 0x7f0d0188

.field public static final crm_v2_manage_coupons_and_rewards_screen:I = 0x7f0d0189

.field public static final crm_v2_message_list_row:I = 0x7f0d018a

.field public static final crm_v2_message_list_view:I = 0x7f0d018b

.field public static final crm_v2_multiselect_actionbar:I = 0x7f0d018c

.field public static final crm_v2_multiselect_dropdown:I = 0x7f0d018d

.field public static final crm_v2_no_customer_selected_tablet:I = 0x7f0d018e

.field public static final crm_v2_profile_line_data_row:I = 0x7f0d018f

.field public static final crm_v2_profile_rows_and_button_section:I = 0x7f0d0190

.field public static final crm_v2_profile_section_card_on_file:I = 0x7f0d0191

.field public static final crm_v2_profile_section_header:I = 0x7f0d0192

.field public static final crm_v2_profile_smartlinerow:I = 0x7f0d0193

.field public static final crm_v2_rewards_section_view:I = 0x7f0d0194

.field public static final crm_v2_see_all_reward_tiers:I = 0x7f0d0195

.field public static final crm_v2_simple_section:I = 0x7f0d0196

.field public static final crm_v2_smartlinerow_list_row:I = 0x7f0d0197

.field public static final crm_v2_update_customer_view:I = 0x7f0d0198

.field public static final crm_v2_view_all_coupons_and_rewards_row:I = 0x7f0d0199

.field public static final crm_v2_view_all_coupons_and_rewards_screen:I = 0x7f0d019a

.field public static final crm_v2_view_customer:I = 0x7f0d019b

.field public static final crm_v2_view_customer_drop_down_container:I = 0x7f0d019c

.field public static final crm_v2_view_customer_drop_down_content:I = 0x7f0d019d

.field public static final crm_v2_view_group:I = 0x7f0d019e

.field public static final crm_v2_view_groups_list:I = 0x7f0d019f

.field public static final crm_view_customer_attribute_row:I = 0x7f0d01a0

.field public static final crm_view_loyalty_balance:I = 0x7f0d01a1

.field public static final crm_view_note_view:I = 0x7f0d01a2

.field public static final crm_visit_frequency_filter_content:I = 0x7f0d01a3

.field public static final current_drawer_in_progress_state:I = 0x7f0d01a4

.field public static final current_drawer_loading_state:I = 0x7f0d01a5

.field public static final current_drawer_start_state:I = 0x7f0d01a6

.field public static final current_drawer_view:I = 0x7f0d01a7

.field public static final custom_date_view:I = 0x7f0d01a8

.field public static final custom_dialog:I = 0x7f0d01a9

.field public static final customer_card_on_file:I = 0x7f0d01aa

.field public static final customer_card_on_file_row:I = 0x7f0d01ab

.field public static final customer_no_cards_on_file:I = 0x7f0d01ac

.field public static final customer_no_cards_on_file_row:I = 0x7f0d01ad

.field public static final customize_report_view:I = 0x7f0d01ae

.field public static final cvv_editor:I = 0x7f0d01af

.field public static final dark_warning_content:I = 0x7f0d01b0

.field public static final dark_warning_view:I = 0x7f0d01b1

.field public static final date_of_birth_popup:I = 0x7f0d01b2

.field public static final date_picker_layout:I = 0x7f0d01b3

.field public static final date_picker_weekday:I = 0x7f0d01b4

.field public static final date_picker_weekday_cell:I = 0x7f0d01b5

.field public static final date_range_selector:I = 0x7f0d01b6

.field public static final date_range_title:I = 0x7f0d01b7

.field public static final date_row:I = 0x7f0d01b8

.field public static final delegate_lockout_view:I = 0x7f0d01b9

.field public static final deposit_bank_linking_view:I = 0x7f0d01ba

.field public static final deposit_card_linking_view:I = 0x7f0d01bb

.field public static final deposit_linking_result_view:I = 0x7f0d01bc

.field public static final deposit_schedule_view:I = 0x7f0d01bd

.field public static final deposit_settings_view:I = 0x7f0d01be

.field public static final deposit_settings_view_content:I = 0x7f0d01bf

.field public static final deposit_speed_view:I = 0x7f0d01c0

.field public static final design_bottom_navigation_item:I = 0x7f0d01c1

.field public static final design_bottom_sheet_dialog:I = 0x7f0d01c2

.field public static final design_layout_snackbar:I = 0x7f0d01c3

.field public static final design_layout_snackbar_include:I = 0x7f0d01c4

.field public static final design_layout_tab_icon:I = 0x7f0d01c5

.field public static final design_layout_tab_text:I = 0x7f0d01c6

.field public static final design_menu_item_action_area:I = 0x7f0d01c7

.field public static final design_navigation_item:I = 0x7f0d01c8

.field public static final design_navigation_item_header:I = 0x7f0d01c9

.field public static final design_navigation_item_separator:I = 0x7f0d01ca

.field public static final design_navigation_item_subheader:I = 0x7f0d01cb

.field public static final design_navigation_menu:I = 0x7f0d01cc

.field public static final design_navigation_menu_item:I = 0x7f0d01cd

.field public static final design_text_input_end_icon:I = 0x7f0d01ce

.field public static final design_text_input_start_icon:I = 0x7f0d01cf

.field public static final detail_confirmation_view:I = 0x7f0d01d0

.field public static final detail_searchable_list:I = 0x7f0d01d1

.field public static final detail_searchable_list_create_button_row:I = 0x7f0d01d2

.field public static final detail_searchable_list_empty_view:I = 0x7f0d01d3

.field public static final detail_searchable_list_no_search_results_row:I = 0x7f0d01d4

.field public static final detail_searchable_list_view_content:I = 0x7f0d01d5

.field public static final detail_searchable_list_view_draggable_list:I = 0x7f0d01d6

.field public static final device_code_auth_view:I = 0x7f0d01d7

.field public static final device_code_login_view:I = 0x7f0d01d8

.field public static final device_name_view:I = 0x7f0d01d9

.field public static final digit_0:I = 0x7f0d01da

.field public static final digits_1_through_9:I = 0x7f0d01db

.field public static final dipped_card_spinner_view:I = 0x7f0d01dc

.field public static final discount_entry_money_view:I = 0x7f0d01dd

.field public static final discount_entry_percent_view:I = 0x7f0d01de

.field public static final discount_rule_row:I = 0x7f0d01df

.field public static final discount_rule_section_view:I = 0x7f0d01e0

.field public static final discounted_item_deleted_screen:I = 0x7f0d01e1

.field public static final display_reward_screen:I = 0x7f0d01e2

.field public static final disputes_detail_view:I = 0x7f0d01e3

.field public static final disputes_error:I = 0x7f0d01e4

.field public static final draggable_item_variation_row:I = 0x7f0d01e5

.field public static final draggable_service_variation_row:I = 0x7f0d01e6

.field public static final drawer_email_card_view:I = 0x7f0d01e7

.field public static final drawer_history_cash_drawer_details:I = 0x7f0d01e8

.field public static final drawer_history_null_state:I = 0x7f0d01e9

.field public static final drawer_history_paid_in_out_title:I = 0x7f0d01ea

.field public static final drawer_history_row:I = 0x7f0d01eb

.field public static final drawer_history_view:I = 0x7f0d01ec

.field public static final drawer_report_main_view:I = 0x7f0d01ed

.field public static final dropdown_item:I = 0x7f0d01ee

.field public static final duplicate_sku_result:I = 0x7f0d01ef

.field public static final duration_picker_view:I = 0x7f0d01f0

.field public static final edit_catalog_object_label:I = 0x7f0d01f1

.field public static final edit_category_label_view:I = 0x7f0d01f2

.field public static final edit_category_static_top_content:I = 0x7f0d01f3

.field public static final edit_category_static_top_content_tile:I = 0x7f0d01f4

.field public static final edit_category_view:I = 0x7f0d01f5

.field public static final edit_discount_view:I = 0x7f0d01f6

.field public static final edit_discount_view_top:I = 0x7f0d01f7

.field public static final edit_discount_view_top_name:I = 0x7f0d01f8

.field public static final edit_discount_view_top_tag:I = 0x7f0d01f9

.field public static final edit_discount_view_top_value:I = 0x7f0d01fa

.field public static final edit_invoice_1_view:I = 0x7f0d01fb

.field public static final edit_invoice_2_view:I = 0x7f0d01fc

.field public static final edit_invoice_bottom_button:I = 0x7f0d01fd

.field public static final edit_invoice_v2_confirmation_view:I = 0x7f0d01fe

.field public static final edit_invoice_v2_invoice_detail_view:I = 0x7f0d01ff

.field public static final edit_invoice_view:I = 0x7f0d0200

.field public static final edit_item_category_selection_list_row:I = 0x7f0d0201

.field public static final edit_item_category_selection_view:I = 0x7f0d0202

.field public static final edit_item_edit_details:I = 0x7f0d0203

.field public static final edit_item_item_option_and_values_row_content:I = 0x7f0d0204

.field public static final edit_item_label_top_view:I = 0x7f0d0205

.field public static final edit_item_label_view:I = 0x7f0d0206

.field public static final edit_item_main_view:I = 0x7f0d0207

.field public static final edit_item_main_view_single_variation_static_row:I = 0x7f0d0208

.field public static final edit_item_main_view_static_bottom:I = 0x7f0d0209

.field public static final edit_item_main_view_static_top:I = 0x7f0d020a

.field public static final edit_item_main_view_top_category:I = 0x7f0d020b

.field public static final edit_item_main_view_variation_input_fields:I = 0x7f0d020c

.field public static final edit_item_new_category_button:I = 0x7f0d020d

.field public static final edit_item_new_category_name_view:I = 0x7f0d020e

.field public static final edit_item_photo_view:I = 0x7f0d020f

.field public static final edit_item_prices_row:I = 0x7f0d0210

.field public static final edit_item_read_only_details:I = 0x7f0d0211

.field public static final edit_item_unit_selection_card:I = 0x7f0d0212

.field public static final edit_item_unit_selection_create_unit_row:I = 0x7f0d0213

.field public static final edit_item_unit_selection_recycler:I = 0x7f0d0214

.field public static final edit_item_unit_selection_sheet:I = 0x7f0d0215

.field public static final edit_item_variation:I = 0x7f0d0216

.field public static final edit_modifier_set_main_view:I = 0x7f0d0217

.field public static final edit_modifier_set_static_content_top:I = 0x7f0d0218

.field public static final edit_option:I = 0x7f0d0219

.field public static final edit_option_value:I = 0x7f0d021a

.field public static final edit_payment_request_screen:I = 0x7f0d021b

.field public static final edit_payment_request_v2_screen:I = 0x7f0d021c

.field public static final edit_payment_schedule_screen:I = 0x7f0d021d

.field public static final edit_photo_content:I = 0x7f0d021e

.field public static final edit_photo_frame_content:I = 0x7f0d021f

.field public static final edit_quantity_row:I = 0x7f0d0220

.field public static final edit_service_assigned_employees_view:I = 0x7f0d0221

.field public static final edit_service_main_view_single_variation_static_row:I = 0x7f0d0222

.field public static final edit_service_price_type_selection_view:I = 0x7f0d0223

.field public static final edit_service_variation:I = 0x7f0d0224

.field public static final edit_split_ticket_view:I = 0x7f0d0225

.field public static final edit_tax_tax_exempt_all_row:I = 0x7f0d0226

.field public static final edit_text_password:I = 0x7f0d0227

.field public static final edit_ticket_group_ticket_counter:I = 0x7f0d0228

.field public static final edit_ticket_group_view:I = 0x7f0d0229

.field public static final edit_ticket_group_view_custom_header:I = 0x7f0d022a

.field public static final edit_ticket_group_view_delete_button:I = 0x7f0d022b

.field public static final edit_ticket_group_view_header:I = 0x7f0d022c

.field public static final edit_ticket_view:I = 0x7f0d022d

.field public static final edit_unit:I = 0x7f0d022e

.field public static final edit_unit_save_in_progress:I = 0x7f0d022f

.field public static final editor_dialog:I = 0x7f0d0230

.field public static final editor_dialog_percentage:I = 0x7f0d0231

.field public static final education_popup:I = 0x7f0d0232

.field public static final egiftcard_actionbar:I = 0x7f0d0233

.field public static final egiftcard_add_design_settings_screen:I = 0x7f0d0234

.field public static final egiftcard_choose_amount_presets:I = 0x7f0d0235

.field public static final egiftcard_choose_amount_view:I = 0x7f0d0236

.field public static final egiftcard_choose_custom_amount_view:I = 0x7f0d0237

.field public static final egiftcard_choose_design_header:I = 0x7f0d0238

.field public static final egiftcard_choose_design_image:I = 0x7f0d0239

.field public static final egiftcard_choose_design_view:I = 0x7f0d023a

.field public static final egiftcard_choose_email_view:I = 0x7f0d023b

.field public static final egiftcard_crop_custom_design_settings_screen:I = 0x7f0d023c

.field public static final egiftcard_registering_view:I = 0x7f0d023d

.field public static final egiftcard_settings_design_image:I = 0x7f0d023e

.field public static final egiftcard_view_design_settings_screen:I = 0x7f0d023f

.field public static final egiftcard_x2_seller_buyer_activating_view:I = 0x7f0d0240

.field public static final email_collection_view:I = 0x7f0d0241

.field public static final email_collection_view_all_done_contents:I = 0x7f0d0242

.field public static final email_collection_view_enrollment_contents:I = 0x7f0d0243

.field public static final email_password_login_view:I = 0x7f0d0244

.field public static final email_report_view:I = 0x7f0d0245

.field public static final email_sheet_view:I = 0x7f0d0246

.field public static final emoney_brand_selection:I = 0x7f0d0247

.field public static final emoney_payment_validation:I = 0x7f0d0248

.field public static final employee_jobs_button:I = 0x7f0d0249

.field public static final employee_jobs_list:I = 0x7f0d024a

.field public static final employee_jobs_list_modal:I = 0x7f0d024b

.field public static final employee_lock_button:I = 0x7f0d024c

.field public static final employee_management_settings_view:I = 0x7f0d024d

.field public static final employee_selection_view:I = 0x7f0d024e

.field public static final employees_upsell_screen:I = 0x7f0d024f

.field public static final empty_card_activity:I = 0x7f0d0250

.field public static final empty_view:I = 0x7f0d0251

.field public static final emv_applications_options_container:I = 0x7f0d0252

.field public static final emv_approved_view:I = 0x7f0d0253

.field public static final emv_choose_option_view:I = 0x7f0d0254

.field public static final emv_option_item:I = 0x7f0d0255

.field public static final emv_progress_view:I = 0x7f0d0256

.field public static final enable_checkout_links_screen:I = 0x7f0d0257

.field public static final enable_device_settings_view:I = 0x7f0d0258

.field public static final end_current_drawer_card_view:I = 0x7f0d0259

.field public static final ends_date_view:I = 0x7f0d025a

.field public static final ends_view:I = 0x7f0d025b

.field public static final enroll_google_auth_code_view:I = 0x7f0d025c

.field public static final enroll_google_auth_qr_view:I = 0x7f0d025d

.field public static final enroll_sms_view:I = 0x7f0d025e

.field public static final enter_passcode_to_unlock_view:I = 0x7f0d025f

.field public static final enter_reward_code_screen:I = 0x7f0d0260

.field public static final error_balance:I = 0x7f0d0261

.field public static final error_card_activity:I = 0x7f0d0262

.field public static final error_retry_row_phone:I = 0x7f0d0263

.field public static final error_retry_row_tablet:I = 0x7f0d0264

.field public static final estimates_banner:I = 0x7f0d0265

.field public static final expandable_view:I = 0x7f0d0266

.field public static final expiration_editor:I = 0x7f0d0267

.field public static final export_report_dialog:I = 0x7f0d0268

.field public static final favorite_empty_tile:I = 0x7f0d0269

.field public static final favorite_item_icon_tile:I = 0x7f0d026a

.field public static final favorite_item_list_view:I = 0x7f0d026b

.field public static final favorite_page:I = 0x7f0d026c

.field public static final favorite_page_tooltip:I = 0x7f0d026d

.field public static final favorite_text_tile:I = 0x7f0d026e

.field public static final favorite_tile_delete:I = 0x7f0d026f

.field public static final fetch_bank_account_view:I = 0x7f0d0270

.field public static final fetch_in_progress:I = 0x7f0d0271

.field public static final flush_tour_page:I = 0x7f0d0272

.field public static final flying_stars_background_view:I = 0x7f0d0273

.field public static final forgot_password_view:I = 0x7f0d0274

.field public static final get_direct_debit_info_view:I = 0x7f0d0275

.field public static final gift_card_activiation_view:I = 0x7f0d0276

.field public static final gift_card_add_value_view:I = 0x7f0d0277

.field public static final gift_card_choose_type_view:I = 0x7f0d0278

.field public static final gift_card_clear_balance_view:I = 0x7f0d0279

.field public static final gift_card_history_header_row:I = 0x7f0d027a

.field public static final gift_card_history_row:I = 0x7f0d027b

.field public static final gift_card_history_view:I = 0x7f0d027c

.field public static final gift_card_lookup_view:I = 0x7f0d027d

.field public static final gift_card_select_screen_view:I = 0x7f0d027e

.field public static final giftcards_settings_screen:I = 0x7f0d027f

.field public static final glyph_button_auto_complete_edit_text:I = 0x7f0d0280

.field public static final glyph_button_edit_text:I = 0x7f0d0281

.field public static final glyph_radio_row:I = 0x7f0d0282

.field public static final glyph_title_message:I = 0x7f0d0283

.field public static final hardware_peripheral_empty_row:I = 0x7f0d0284

.field public static final hardware_printer_select_view:I = 0x7f0d0285

.field public static final hardware_settings_message:I = 0x7f0d0286

.field public static final header_body_row_layout:I = 0x7f0d0287

.field public static final help_applet_content_row:I = 0x7f0d0288

.field public static final help_applet_master_view:I = 0x7f0d0289

.field public static final help_content_row:I = 0x7f0d028a

.field public static final help_row:I = 0x7f0d028b

.field public static final help_section:I = 0x7f0d028c

.field public static final home_drawer_button:I = 0x7f0d028d

.field public static final home_drawer_button_wide:I = 0x7f0d028e

.field public static final hs__action_download_layout:I = 0x7f0d028f

.field public static final hs__action_view_search:I = 0x7f0d0290

.field public static final hs__authentication_failure_fragment:I = 0x7f0d0291

.field public static final hs__badge_layout:I = 0x7f0d0292

.field public static final hs__bottomsheet_wrapper:I = 0x7f0d0293

.field public static final hs__collapsed_picker_header:I = 0x7f0d0294

.field public static final hs__conversation_fragment:I = 0x7f0d0295

.field public static final hs__conversation_layout:I = 0x7f0d0296

.field public static final hs__conversation_replyboxview:I = 0x7f0d0297

.field public static final hs__conversational_labelledreplyboxview:I = 0x7f0d0298

.field public static final hs__csat_dialog:I = 0x7f0d0299

.field public static final hs__csat_holder:I = 0x7f0d029a

.field public static final hs__csat_view:I = 0x7f0d029b

.field public static final hs__dynamic_form_fragment:I = 0x7f0d029c

.field public static final hs__expanded_picker_header:I = 0x7f0d029d

.field public static final hs__faq_flow_fragment:I = 0x7f0d029e

.field public static final hs__faq_fragment:I = 0x7f0d029f

.field public static final hs__history_loading_view_layout:I = 0x7f0d02a0

.field public static final hs__issue_archival_message_view_holder:I = 0x7f0d02a1

.field public static final hs__messages_list_footer:I = 0x7f0d02a2

.field public static final hs__msg_admin_suggesstion_item:I = 0x7f0d02a3

.field public static final hs__msg_admin_suggesstions_container:I = 0x7f0d02a4

.field public static final hs__msg_agent_typing:I = 0x7f0d02a5

.field public static final hs__msg_attachment_generic:I = 0x7f0d02a6

.field public static final hs__msg_attachment_image:I = 0x7f0d02a7

.field public static final hs__msg_publish_id_layout:I = 0x7f0d02a8

.field public static final hs__msg_request_screenshot:I = 0x7f0d02a9

.field public static final hs__msg_review_request:I = 0x7f0d02aa

.field public static final hs__msg_screenshot_status:I = 0x7f0d02ab

.field public static final hs__msg_system_conversation_redacted_layout:I = 0x7f0d02ac

.field public static final hs__msg_system_divider_layout:I = 0x7f0d02ad

.field public static final hs__msg_system_layout:I = 0x7f0d02ae

.field public static final hs__msg_txt_admin:I = 0x7f0d02af

.field public static final hs__msg_txt_user:I = 0x7f0d02b0

.field public static final hs__msg_user_selectable_option:I = 0x7f0d02b1

.field public static final hs__msg_user_selectable_options_container:I = 0x7f0d02b2

.field public static final hs__new_conversation_fragment:I = 0x7f0d02b3

.field public static final hs__new_conversation_layout:I = 0x7f0d02b4

.field public static final hs__parent_activity:I = 0x7f0d02b5

.field public static final hs__picker_layout:I = 0x7f0d02b6

.field public static final hs__picker_option:I = 0x7f0d02b7

.field public static final hs__question_list_fragment:I = 0x7f0d02b8

.field public static final hs__screenshot_preview_fragment:I = 0x7f0d02b9

.field public static final hs__screenshot_preview_layout:I = 0x7f0d02ba

.field public static final hs__search_fragment:I = 0x7f0d02bb

.field public static final hs__search_list_footer:I = 0x7f0d02bc

.field public static final hs__search_result_footer:I = 0x7f0d02bd

.field public static final hs__search_result_fragment:I = 0x7f0d02be

.field public static final hs__search_result_header:I = 0x7f0d02bf

.field public static final hs__search_result_layout:I = 0x7f0d02c0

.field public static final hs__section_divider:I = 0x7f0d02c1

.field public static final hs__section_list_fragment:I = 0x7f0d02c2

.field public static final hs__section_pager_fragment:I = 0x7f0d02c3

.field public static final hs__simple_list_item_1:I = 0x7f0d02c4

.field public static final hs__single_question_fragment:I = 0x7f0d02c5

.field public static final hs__single_question_layout_with_cardview:I = 0x7f0d02c6

.field public static final hs__support_fragment:I = 0x7f0d02c7

.field public static final hs__user_setup_fragment:I = 0x7f0d02c8

.field public static final hs_simple_recycler_view_item:I = 0x7f0d02c9

.field public static final hud:I = 0x7f0d02ca

.field public static final hud_progress_view:I = 0x7f0d02cb

.field public static final icon_initials_title_value:I = 0x7f0d02cc

.field public static final image_or_icon_content:I = 0x7f0d02cd

.field public static final info_bar_inline_button:I = 0x7f0d02ce

.field public static final info_bar_right_aligned_button:I = 0x7f0d02cf

.field public static final info_box:I = 0x7f0d02d0

.field public static final installments_link_options_view:I = 0x7f0d02d1

.field public static final installments_options_view:I = 0x7f0d02d2

.field public static final installments_qr_code_view:I = 0x7f0d02d3

.field public static final installments_sms_input_view:I = 0x7f0d02d4

.field public static final installments_sms_sent_view:I = 0x7f0d02d5

.field public static final instant_deposits_result_view:I = 0x7f0d02d6

.field public static final intent_how_view:I = 0x7f0d02d7

.field public static final intent_where_view:I = 0x7f0d02d8

.field public static final invoice_applet_invoice_detail_view:I = 0x7f0d02d9

.field public static final invoice_attachment_view:I = 0x7f0d02da

.field public static final invoice_automatic_payment_view:I = 0x7f0d02db

.field public static final invoice_automatic_reminder_edit_view:I = 0x7f0d02dc

.field public static final invoice_automatic_reminders_list_view:I = 0x7f0d02dd

.field public static final invoice_bill_history_view:I = 0x7f0d02de

.field public static final invoice_bottom_dialog_button:I = 0x7f0d02df

.field public static final invoice_bottom_dialog_confirm_button:I = 0x7f0d02e0

.field public static final invoice_bottom_sheet_dialog:I = 0x7f0d02e1

.field public static final invoice_cart_entry_view:I = 0x7f0d02e2

.field public static final invoice_choose_date_view:I = 0x7f0d02e3

.field public static final invoice_confirm_button:I = 0x7f0d02e4

.field public static final invoice_confirm_button_container:I = 0x7f0d02e5

.field public static final invoice_confirmation:I = 0x7f0d02e6

.field public static final invoice_container_view:I = 0x7f0d02e7

.field public static final invoice_container_view_v2:I = 0x7f0d02e8

.field public static final invoice_delivery_method_view:I = 0x7f0d02e9

.field public static final invoice_event_row:I = 0x7f0d02ea

.field public static final invoice_header_contents:I = 0x7f0d02eb

.field public static final invoice_help_text:I = 0x7f0d02ec

.field public static final invoice_image_attachment_content:I = 0x7f0d02ed

.field public static final invoice_image_attachment_view:I = 0x7f0d02ee

.field public static final invoice_message_view:I = 0x7f0d02ef

.field public static final invoice_mobile_analytics_row:I = 0x7f0d02f0

.field public static final invoice_null_state:I = 0x7f0d02f1

.field public static final invoice_paid_view:I = 0x7f0d02f2

.field public static final invoice_payment_type_view:I = 0x7f0d02f3

.field public static final invoice_preview:I = 0x7f0d02f4

.field public static final invoice_preview_webview:I = 0x7f0d02f5

.field public static final invoice_read_only_detail_view:I = 0x7f0d02f6

.field public static final invoice_read_write_detail_view:I = 0x7f0d02f7

.field public static final invoice_section_container_header:I = 0x7f0d02f8

.field public static final invoice_sent_save_view:I = 0x7f0d02f9

.field public static final invoice_state_filter_dropdown_header:I = 0x7f0d02fa

.field public static final invoice_state_filter_layout:I = 0x7f0d02fb

.field public static final invoice_timeline:I = 0x7f0d02fc

.field public static final invoice_timeline_view:I = 0x7f0d02fd

.field public static final invoice_upload_confirmation_view:I = 0x7f0d02fe

.field public static final invoices_applet_invoice_filter_master_view:I = 0x7f0d02ff

.field public static final invoices_applet_invoice_history_view:I = 0x7f0d0300

.field public static final invoices_create_invoice_button:I = 0x7f0d0301

.field public static final invoices_item_select_card:I = 0x7f0d0302

.field public static final invoices_list_header_row:I = 0x7f0d0303

.field public static final item_fee_list_row:I = 0x7f0d0304

.field public static final itemization_row:I = 0x7f0d0305

.field public static final items_applet_category_list_row:I = 0x7f0d0306

.field public static final items_applet_delete_button_row:I = 0x7f0d0307

.field public static final items_applet_modifiers_list_row:I = 0x7f0d0308

.field public static final jail_view:I = 0x7f0d0309

.field public static final jedi_banner_view:I = 0x7f0d030a

.field public static final jedi_button_base_component_view:I = 0x7f0d030b

.field public static final jedi_headline_component_view:I = 0x7f0d030c

.field public static final jedi_help_section:I = 0x7f0d030d

.field public static final jedi_icon_component_view:I = 0x7f0d030e

.field public static final jedi_input_confirmation_component_view:I = 0x7f0d030f

.field public static final jedi_instant_answer_component_view:I = 0x7f0d0310

.field public static final jedi_panel_recycler_view:I = 0x7f0d0311

.field public static final jedi_panel_view:I = 0x7f0d0312

.field public static final jedi_paragraph_component_view:I = 0x7f0d0313

.field public static final jedi_row_component_view:I = 0x7f0d0314

.field public static final jedi_section_header_component_view:I = 0x7f0d0315

.field public static final jedi_text_field_component_view:I = 0x7f0d0316

.field public static final jedi_unsupported_component_view:I = 0x7f0d0317

.field public static final keypad_entry_card_view:I = 0x7f0d0318

.field public static final keypad_entry_money:I = 0x7f0d0319

.field public static final keypad_entry_percent:I = 0x7f0d031a

.field public static final keypad_mode_selection:I = 0x7f0d031b

.field public static final keypad_panel:I = 0x7f0d031c

.field public static final keypad_view:I = 0x7f0d031d

.field public static final keypad_view_container:I = 0x7f0d031e

.field public static final keypad_view_keys:I = 0x7f0d031f

.field public static final landing_view:I = 0x7f0d0320

.field public static final learn_more_contactless:I = 0x7f0d0321

.field public static final learn_more_magstripe:I = 0x7f0d0322

.field public static final learn_more_reader_view:I = 0x7f0d0323

.field public static final legacy_item_variations_message_row:I = 0x7f0d0324

.field public static final legal:I = 0x7f0d0325

.field public static final libraries:I = 0x7f0d0326

.field public static final libraries_footer:I = 0x7f0d0327

.field public static final library_create_new_item_dialog_view:I = 0x7f0d0328

.field public static final library_list_contents:I = 0x7f0d0329

.field public static final library_panel:I = 0x7f0d032a

.field public static final library_panel_list_noho_row:I = 0x7f0d032b

.field public static final license_group:I = 0x7f0d032c

.field public static final license_group_divider:I = 0x7f0d032d

.field public static final license_group_header:I = 0x7f0d032e

.field public static final licenses:I = 0x7f0d032f

.field public static final line_row:I = 0x7f0d0330

.field public static final link_debit_card_result_view:I = 0x7f0d0331

.field public static final link_debit_card_view:I = 0x7f0d0332

.field public static final list_loading_indicator:I = 0x7f0d0333

.field public static final load_more_row:I = 0x7f0d0334

.field public static final loading_dialog_content:I = 0x7f0d0335

.field public static final loading_rewards_screen:I = 0x7f0d0336

.field public static final loading_view:I = 0x7f0d0337

.field public static final location_activity:I = 0x7f0d0338

.field public static final logged_out_container:I = 0x7f0d0339

.field public static final login_alert_dialog:I = 0x7f0d033a

.field public static final login_alert_dialog_button:I = 0x7f0d033b

.field public static final loyalty_report:I = 0x7f0d033c

.field public static final loyalty_report_error:I = 0x7f0d033d

.field public static final loyalty_report_space:I = 0x7f0d033e

.field public static final loyalty_reward_row:I = 0x7f0d033f

.field public static final loyalty_reward_tier:I = 0x7f0d0340

.field public static final loyalty_settings_screen:I = 0x7f0d0341

.field public static final loyalty_tour_page:I = 0x7f0d0342

.field public static final loyalty_tour_view:I = 0x7f0d0343

.field public static final loyalty_view:I = 0x7f0d0344

.field public static final loyalty_view_all_done_contents:I = 0x7f0d0345

.field public static final loyalty_view_cash_app_banner:I = 0x7f0d0346

.field public static final loyalty_view_enrollment_contents:I = 0x7f0d0347

.field public static final loyalty_view_reward_status_contents:I = 0x7f0d0348

.field public static final loyalty_view_reward_tiers:I = 0x7f0d0349

.field public static final marin_action_bar:I = 0x7f0d034a

.field public static final marin_address_layout:I = 0x7f0d034b

.field public static final marin_glyph_message:I = 0x7f0d034c

.field public static final marin_simple_list_item:I = 0x7f0d034d

.field public static final master_detail_ticket_view:I = 0x7f0d034e

.field public static final master_group_list_view:I = 0x7f0d034f

.field public static final master_group_row:I = 0x7f0d0350

.field public static final merchant_category_view:I = 0x7f0d0351

.field public static final merchant_picker_view:I = 0x7f0d0352

.field public static final merchant_profile_business_address_view:I = 0x7f0d0353

.field public static final merchant_profile_content:I = 0x7f0d0354

.field public static final merchant_profile_edit_logo_view:I = 0x7f0d0355

.field public static final merchant_profile_view:I = 0x7f0d0356

.field public static final merchant_subcategory_view:I = 0x7f0d0357

.field public static final merge_ticket_row:I = 0x7f0d0358

.field public static final merge_ticket_view:I = 0x7f0d0359

.field public static final messages_view:I = 0x7f0d035a

.field public static final modifier_assignment:I = 0x7f0d035b

.field public static final modifier_option_row:I = 0x7f0d035c

.field public static final modifiers_detail_view:I = 0x7f0d035d

.field public static final month:I = 0x7f0d035e

.field public static final month_pager_view:I = 0x7f0d035f

.field public static final move_ticket_view:I = 0x7f0d0360

.field public static final mtrl_alert_dialog:I = 0x7f0d0361

.field public static final mtrl_alert_dialog_actions:I = 0x7f0d0362

.field public static final mtrl_alert_dialog_title:I = 0x7f0d0363

.field public static final mtrl_alert_select_dialog_item:I = 0x7f0d0364

.field public static final mtrl_alert_select_dialog_multichoice:I = 0x7f0d0365

.field public static final mtrl_alert_select_dialog_singlechoice:I = 0x7f0d0366

.field public static final mtrl_calendar_day:I = 0x7f0d0367

.field public static final mtrl_calendar_day_of_week:I = 0x7f0d0368

.field public static final mtrl_calendar_days_of_week:I = 0x7f0d0369

.field public static final mtrl_calendar_horizontal:I = 0x7f0d036a

.field public static final mtrl_calendar_month:I = 0x7f0d036b

.field public static final mtrl_calendar_month_labeled:I = 0x7f0d036c

.field public static final mtrl_calendar_month_navigation:I = 0x7f0d036d

.field public static final mtrl_calendar_months:I = 0x7f0d036e

.field public static final mtrl_calendar_vertical:I = 0x7f0d036f

.field public static final mtrl_calendar_year:I = 0x7f0d0370

.field public static final mtrl_layout_snackbar:I = 0x7f0d0371

.field public static final mtrl_layout_snackbar_include:I = 0x7f0d0372

.field public static final mtrl_picker_actions:I = 0x7f0d0373

.field public static final mtrl_picker_dialog:I = 0x7f0d0374

.field public static final mtrl_picker_fullscreen:I = 0x7f0d0375

.field public static final mtrl_picker_header_dialog:I = 0x7f0d0376

.field public static final mtrl_picker_header_fullscreen:I = 0x7f0d0377

.field public static final mtrl_picker_header_selection_text:I = 0x7f0d0378

.field public static final mtrl_picker_header_title_text:I = 0x7f0d0379

.field public static final mtrl_picker_header_toggle:I = 0x7f0d037a

.field public static final mtrl_picker_text_input_date:I = 0x7f0d037b

.field public static final mtrl_picker_text_input_date_range:I = 0x7f0d037c

.field public static final multi_line_row:I = 0x7f0d037d

.field public static final name_value_image_row:I = 0x7f0d037e

.field public static final name_value_row:I = 0x7f0d037f

.field public static final new_ticket_custom_ticket_button:I = 0x7f0d0380

.field public static final new_ticket_no_predefined_tickets:I = 0x7f0d0381

.field public static final new_ticket_select_group_header:I = 0x7f0d0382

.field public static final new_ticket_view:I = 0x7f0d0383

.field public static final no_connected_scales_layout:I = 0x7f0d0384

.field public static final no_invoice_search_results:I = 0x7f0d0385

.field public static final no_reward_screen:I = 0x7f0d0386

.field public static final no_search_results:I = 0x7f0d0387

.field public static final noho_action_bar:I = 0x7f0d0388

.field public static final noho_action_bar_contents:I = 0x7f0d0389

.field public static final noho_action_bar_second_action_icon:I = 0x7f0d038a

.field public static final noho_action_bar_with_two_action_icons:I = 0x7f0d038b

.field public static final noho_address_layout:I = 0x7f0d038c

.field public static final noho_alert_dialog:I = 0x7f0d038d

.field public static final noho_date_picker_contents:I = 0x7f0d038e

.field public static final noho_date_picker_contents_no_year:I = 0x7f0d038f

.field public static final noho_date_picker_dialog:I = 0x7f0d0390

.field public static final noho_date_picker_dialog_optional_year:I = 0x7f0d0391

.field public static final noho_dropdown_menu_view:I = 0x7f0d0392

.field public static final noho_dropdown_selected_view:I = 0x7f0d0393

.field public static final noho_message_view:I = 0x7f0d0394

.field public static final noho_row:I = 0x7f0d0395

.field public static final noho_simple_list_item:I = 0x7f0d0396

.field public static final noho_spinner_item:I = 0x7f0d0397

.field public static final noho_time_picker_contents:I = 0x7f0d0398

.field public static final noho_time_picker_dialog:I = 0x7f0d0399

.field public static final noho_title_value_row:I = 0x7f0d039a

.field public static final notification_action:I = 0x7f0d039b

.field public static final notification_action_tombstone:I = 0x7f0d039c

.field public static final notification_center_main_view:I = 0x7f0d039d

.field public static final notification_center_padding_row:I = 0x7f0d039e

.field public static final notification_media_action:I = 0x7f0d039f

.field public static final notification_media_cancel_action:I = 0x7f0d03a0

.field public static final notification_preferences_contents_layout:I = 0x7f0d03a1

.field public static final notification_preferences_error_layout:I = 0x7f0d03a2

.field public static final notification_preferences_layout:I = 0x7f0d03a3

.field public static final notification_preferences_loading_layout:I = 0x7f0d03a4

.field public static final notification_row:I = 0x7f0d03a5

.field public static final notification_template_big_media:I = 0x7f0d03a6

.field public static final notification_template_big_media_custom:I = 0x7f0d03a7

.field public static final notification_template_big_media_narrow:I = 0x7f0d03a8

.field public static final notification_template_big_media_narrow_custom:I = 0x7f0d03a9

.field public static final notification_template_custom_big:I = 0x7f0d03aa

.field public static final notification_template_icon_group:I = 0x7f0d03ab

.field public static final notification_template_lines_media:I = 0x7f0d03ac

.field public static final notification_template_media:I = 0x7f0d03ad

.field public static final notification_template_media_custom:I = 0x7f0d03ae

.field public static final notification_template_part_chronometer:I = 0x7f0d03af

.field public static final notification_template_part_time:I = 0x7f0d03b0

.field public static final ntep:I = 0x7f0d03b1

.field public static final onboarding_component_address:I = 0x7f0d03b2

.field public static final onboarding_component_bank_account:I = 0x7f0d03b3

.field public static final onboarding_component_button:I = 0x7f0d03b4

.field public static final onboarding_component_click_list_item:I = 0x7f0d03b5

.field public static final onboarding_component_date_picker:I = 0x7f0d03b6

.field public static final onboarding_component_dropdown:I = 0x7f0d03b7

.field public static final onboarding_component_hardware_list:I = 0x7f0d03b8

.field public static final onboarding_component_image:I = 0x7f0d03b9

.field public static final onboarding_component_list:I = 0x7f0d03ba

.field public static final onboarding_component_multi_list_item:I = 0x7f0d03bb

.field public static final onboarding_component_paragraph:I = 0x7f0d03bc

.field public static final onboarding_component_person_name:I = 0x7f0d03bd

.field public static final onboarding_component_single_list_item:I = 0x7f0d03be

.field public static final onboarding_component_text_field:I = 0x7f0d03bf

.field public static final onboarding_component_title:I = 0x7f0d03c0

.field public static final onboarding_component_unsupported_view:I = 0x7f0d03c1

.field public static final onboarding_container:I = 0x7f0d03c2

.field public static final onboarding_error_view:I = 0x7f0d03c3

.field public static final onboarding_finished_view:I = 0x7f0d03c4

.field public static final onboarding_panel_view:I = 0x7f0d03c5

.field public static final onboarding_prompt_view_content:I = 0x7f0d03c6

.field public static final onboarding_silent_exit:I = 0x7f0d03c7

.field public static final online_checkout_buy_link_screen:I = 0x7f0d03c8

.field public static final online_checkout_create_pay_link_screen:I = 0x7f0d03c9

.field public static final online_checkout_pay_link_screen:I = 0x7f0d03ca

.field public static final online_checkout_settings_action_bottom_sheet_dialog:I = 0x7f0d03cb

.field public static final online_checkout_settings_checkout_link_list_screen:I = 0x7f0d03cc

.field public static final online_checkout_settings_checkout_link_screen:I = 0x7f0d03cd

.field public static final online_checkout_settings_create_link_screen:I = 0x7f0d03ce

.field public static final online_checkout_settings_deactivate_confirmation_dialog:I = 0x7f0d03cf

.field public static final online_checkout_settings_delete_confirmation_dialog:I = 0x7f0d03d0

.field public static final online_checkout_settings_edit_link_screen:I = 0x7f0d03d1

.field public static final open_tickets_settings_create_ticket_group_button:I = 0x7f0d03d2

.field public static final open_tickets_settings_ticket_group_header:I = 0x7f0d03d3

.field public static final open_tickets_settings_toggle_as_home_screen:I = 0x7f0d03d4

.field public static final open_tickets_settings_toggle_open_tickets:I = 0x7f0d03d5

.field public static final open_tickets_settings_toggle_predefined_tickets:I = 0x7f0d03d6

.field public static final open_tickets_settings_view:I = 0x7f0d03d7

.field public static final order_confirmation_view:I = 0x7f0d03d8

.field public static final order_corrected_address_view:I = 0x7f0d03d9

.field public static final order_entry_actionbar_edit_tab_glyph:I = 0x7f0d03da

.field public static final order_entry_actionbar_edit_tab_text:I = 0x7f0d03db

.field public static final order_entry_actionbar_sale_tab_glyph:I = 0x7f0d03dc

.field public static final order_entry_actionbar_sale_tab_text:I = 0x7f0d03dd

.field public static final order_entry_actionbar_tab_text_wide:I = 0x7f0d03de

.field public static final order_entry_view:I = 0x7f0d03df

.field public static final order_entry_view_content:I = 0x7f0d03e0

.field public static final order_entry_view_pager_v1:I = 0x7f0d03e1

.field public static final order_entry_view_pager_v2:I = 0x7f0d03e2

.field public static final order_entry_view_wide:I = 0x7f0d03e3

.field public static final order_error_workflow:I = 0x7f0d03e4

.field public static final order_hardware:I = 0x7f0d03e5

.field public static final order_history_entry:I = 0x7f0d03e6

.field public static final order_history_more:I = 0x7f0d03e7

.field public static final order_history_view:I = 0x7f0d03e8

.field public static final order_reader_item_view:I = 0x7f0d03e9

.field public static final order_select_address_layout:I = 0x7f0d03ea

.field public static final order_square_card_business_name_view:I = 0x7f0d03eb

.field public static final order_square_card_signature_button_bar:I = 0x7f0d03ec

.field public static final order_square_card_signature_view:I = 0x7f0d03ed

.field public static final order_square_card_splash_view:I = 0x7f0d03ee

.field public static final order_square_card_stamps_gallery:I = 0x7f0d03ef

.field public static final order_unverified_address_view:I = 0x7f0d03f0

.field public static final order_view:I = 0x7f0d03f1

.field public static final orderhub_add_note_view:I = 0x7f0d03f2

.field public static final orderhub_add_tracking_view:I = 0x7f0d03f3

.field public static final orderhub_alert_settings_view:I = 0x7f0d03f4

.field public static final orderhub_bill_history_view:I = 0x7f0d03f5

.field public static final orderhub_detail_filter_header_row:I = 0x7f0d03f6

.field public static final orderhub_detail_filter_row:I = 0x7f0d03f7

.field public static final orderhub_detail_header_row:I = 0x7f0d03f8

.field public static final orderhub_detail_order_row:I = 0x7f0d03f9

.field public static final orderhub_detail_view:I = 0x7f0d03fa

.field public static final orderhub_item_selection_view:I = 0x7f0d03fb

.field public static final orderhub_mark_shipped_view:I = 0x7f0d03fc

.field public static final orderhub_master_header_row:I = 0x7f0d03fd

.field public static final orderhub_master_row:I = 0x7f0d03fe

.field public static final orderhub_master_view:I = 0x7f0d03ff

.field public static final orderhub_order_adjust_pickup_time:I = 0x7f0d0400

.field public static final orderhub_order_buttons_view:I = 0x7f0d0401

.field public static final orderhub_order_cancelation_processing_view:I = 0x7f0d0402

.field public static final orderhub_order_cancelation_reason_view:I = 0x7f0d0403

.field public static final orderhub_order_customer_view:I = 0x7f0d0404

.field public static final orderhub_order_fulfillment_header_view:I = 0x7f0d0405

.field public static final orderhub_order_fulfillment_recipient_address_view:I = 0x7f0d0406

.field public static final orderhub_order_fulfillment_time_view:I = 0x7f0d0407

.field public static final orderhub_order_fulfillment_view:I = 0x7f0d0408

.field public static final orderhub_order_fulfillments_section_view:I = 0x7f0d0409

.field public static final orderhub_order_id_view:I = 0x7f0d040a

.field public static final orderhub_order_item_view:I = 0x7f0d040b

.field public static final orderhub_order_note_view:I = 0x7f0d040c

.field public static final orderhub_order_price_view:I = 0x7f0d040d

.field public static final orderhub_order_secondary_button:I = 0x7f0d040e

.field public static final orderhub_order_shipment_method_view:I = 0x7f0d040f

.field public static final orderhub_order_status_view:I = 0x7f0d0410

.field public static final orderhub_order_tender_row_view:I = 0x7f0d0411

.field public static final orderhub_order_tender_view:I = 0x7f0d0412

.field public static final orderhub_order_tracking_view:I = 0x7f0d0413

.field public static final orderhub_order_view:I = 0x7f0d0414

.field public static final orderhub_printing_settings_view:I = 0x7f0d0415

.field public static final orderhub_quick_actions_settings_view:I = 0x7f0d0416

.field public static final orderhub_recycler_title_row:I = 0x7f0d0417

.field public static final orders_section:I = 0x7f0d0418

.field public static final overflow_view:I = 0x7f0d0419

.field public static final page_label_edit_view:I = 0x7f0d041a

.field public static final paid_in_out_event_row:I = 0x7f0d041b

.field public static final paid_in_out_main_view:I = 0x7f0d041c

.field public static final paid_in_out_total_row:I = 0x7f0d041d

.field public static final pairing_header_view:I = 0x7f0d041e

.field public static final pairing_help_row_view:I = 0x7f0d041f

.field public static final pairing_help_view:I = 0x7f0d0420

.field public static final pairing_view:I = 0x7f0d0421

.field public static final partial_auth_warning_view:I = 0x7f0d0422

.field public static final passcodes_settings:I = 0x7f0d0423

.field public static final passcodes_settings_create_or_edit_passcode:I = 0x7f0d0424

.field public static final passcodes_settings_create_or_edit_passcode_success:I = 0x7f0d0425

.field public static final passcodes_settings_timeout:I = 0x7f0d0426

.field public static final pay_card_cnp_disabled_view:I = 0x7f0d0427

.field public static final pay_card_on_file_card_row:I = 0x7f0d0428

.field public static final pay_card_swipe_only_view:I = 0x7f0d0429

.field public static final pay_cash_view:I = 0x7f0d042a

.field public static final pay_contactless_view:I = 0x7f0d042b

.field public static final pay_credit_card_screen_view:I = 0x7f0d042c

.field public static final pay_gift_card_screen_view:I = 0x7f0d042d

.field public static final pay_invoice_email_row_view:I = 0x7f0d042e

.field public static final pay_invoice_email_row_view_no_plus:I = 0x7f0d042f

.field public static final pay_invoice_keypad_layout:I = 0x7f0d0430

.field public static final pay_other_tender_layout:I = 0x7f0d0431

.field public static final pay_third_party_card_view:I = 0x7f0d0432

.field public static final payment_cash_options_view:I = 0x7f0d0433

.field public static final payment_choose_card_on_file_customer_view_crm_v2:I = 0x7f0d0434

.field public static final payment_error_view:I = 0x7f0d0435

.field public static final payment_pad_landscape_cart_header_layout:I = 0x7f0d0436

.field public static final payment_pad_landscape_seller_cart_footer_banner:I = 0x7f0d0437

.field public static final payment_pad_landscape_view:I = 0x7f0d0438

.field public static final payment_pad_portrait_view:I = 0x7f0d0439

.field public static final payment_plan_section:I = 0x7f0d043a

.field public static final payment_plan_section_contents:I = 0x7f0d043b

.field public static final payment_processing_contents:I = 0x7f0d043c

.field public static final payment_prompt_layout:I = 0x7f0d043d

.field public static final payment_request_row:I = 0x7f0d043e

.field public static final payment_request_row_contents:I = 0x7f0d043f

.field public static final payment_type_layout:I = 0x7f0d0440

.field public static final payment_type_more_layout:I = 0x7f0d0441

.field public static final payment_types_settings_header_row:I = 0x7f0d0442

.field public static final payment_types_settings_info_row:I = 0x7f0d0443

.field public static final payment_types_settings_view:I = 0x7f0d0444

.field public static final permission_denied_view:I = 0x7f0d0445

.field public static final personal_info_view:I = 0x7f0d0446

.field public static final pick_address_book_contact_view:I = 0x7f0d0447

.field public static final pick_invoice_contact_header:I = 0x7f0d0448

.field public static final pick_invoice_contact_row:I = 0x7f0d0449

.field public static final pinpad:I = 0x7f0d044a

.field public static final popup_actions_view:I = 0x7f0d044b

.field public static final post_auth_coupon_view:I = 0x7f0d044c

.field public static final predefined_tickets_group_or_template_row:I = 0x7f0d044d

.field public static final predefined_tickets_opt_in_view:I = 0x7f0d044e

.field public static final preserved_label_layout:I = 0x7f0d044f

.field public static final preview_attachment_content:I = 0x7f0d0450

.field public static final price_entry_view:I = 0x7f0d0451

.field public static final print_error_footer_row:I = 0x7f0d0452

.field public static final print_error_popup_view:I = 0x7f0d0453

.field public static final print_report_view:I = 0x7f0d0454

.field public static final printer_detail_category_row:I = 0x7f0d0455

.field public static final printer_station_create_station_row:I = 0x7f0d0456

.field public static final printer_station_detail_view:I = 0x7f0d0457

.field public static final printer_stations_list_view:I = 0x7f0d0458

.field public static final quick_amount_entry:I = 0x7f0d0459

.field public static final quick_amounts_settings:I = 0x7f0d045a

.field public static final r12_education_panel_charge:I = 0x7f0d045b

.field public static final r12_education_panel_dip:I = 0x7f0d045c

.field public static final r12_education_panel_start_selling:I = 0x7f0d045d

.field public static final r12_education_panel_swipe:I = 0x7f0d045e

.field public static final r12_education_panel_tap:I = 0x7f0d045f

.field public static final r12_education_panel_video:I = 0x7f0d0460

.field public static final r12_education_panel_welcome:I = 0x7f0d0461

.field public static final r12_education_view:I = 0x7f0d0462

.field public static final r12_pairing_header_content_view:I = 0x7f0d0463

.field public static final r12_speed_test:I = 0x7f0d0464

.field public static final r6_first_time_video_view:I = 0x7f0d0465

.field public static final rate_page:I = 0x7f0d0466

.field public static final rates_tour_view:I = 0x7f0d0467

.field public static final reader_battery_hud:I = 0x7f0d0468

.field public static final reader_message_bar_view:I = 0x7f0d0469

.field public static final reader_status_row:I = 0x7f0d046a

.field public static final reader_warning_view:I = 0x7f0d046b

.field public static final receipt_complete_view:I = 0x7f0d046c

.field public static final receipt_detail_settle_tip_row:I = 0x7f0d046d

.field public static final receipt_detail_settle_tip_row_contents:I = 0x7f0d046e

.field public static final receipt_selection_view:I = 0x7f0d046f

.field public static final receipt_sms_marketing_prompt_view:I = 0x7f0d0470

.field public static final receipt_sms_marketing_spinner_view:I = 0x7f0d0471

.field public static final receipt_view_legacy:I = 0x7f0d0472

.field public static final recent_activity_row:I = 0x7f0d0473

.field public static final record_payment_method_view:I = 0x7f0d0474

.field public static final record_payment_view:I = 0x7f0d0475

.field public static final recurring_frequency_view:I = 0x7f0d0476

.field public static final redeem_rewards_choose_contact:I = 0x7f0d0477

.field public static final redeem_rewards_error_screen:I = 0x7f0d0478

.field public static final referral_view:I = 0x7f0d0479

.field public static final refund_policy_popup_view:I = 0x7f0d047a

.field public static final refund_to_gift_card_section:I = 0x7f0d047b

.field public static final repeat_every_view:I = 0x7f0d047c

.field public static final report_config_card_view:I = 0x7f0d047d

.field public static final report_config_employee_filter_card_view:I = 0x7f0d047e

.field public static final report_config_employees_row:I = 0x7f0d047f

.field public static final report_email_card_view:I = 0x7f0d0480

.field public static final reporting_applet_master_view:I = 0x7f0d0481

.field public static final reports_applet_list_row:I = 0x7f0d0482

.field public static final retry_tender_view:I = 0x7f0d0483

.field public static final review_variations_to_delete:I = 0x7f0d0484

.field public static final review_variations_to_delete_globally:I = 0x7f0d0485

.field public static final root_view:I = 0x7f0d0486

.field public static final root_view_phone:I = 0x7f0d0487

.field public static final root_view_tablet_master_detail:I = 0x7f0d0488

.field public static final root_view_tablet_no_master:I = 0x7f0d0489

.field public static final row_dispute:I = 0x7f0d048a

.field public static final row_file_upload:I = 0x7f0d048b

.field public static final row_header:I = 0x7f0d048c

.field public static final row_load_more:I = 0x7f0d048d

.field public static final row_quick_amounts_value:I = 0x7f0d048e

.field public static final row_summary:I = 0x7f0d048f

.field public static final row_text_answer:I = 0x7f0d0490

.field public static final row_title:I = 0x7f0d0491

.field public static final sales_report_category_row:I = 0x7f0d0492

.field public static final sales_report_category_sales_header:I = 0x7f0d0493

.field public static final sales_report_chart:I = 0x7f0d0494

.field public static final sales_report_chart_view:I = 0x7f0d0495

.field public static final sales_report_customize_details:I = 0x7f0d0496

.field public static final sales_report_discounts_header:I = 0x7f0d0497

.field public static final sales_report_discounts_row:I = 0x7f0d0498

.field public static final sales_report_divider:I = 0x7f0d0499

.field public static final sales_report_divider_large_top:I = 0x7f0d049a

.field public static final sales_report_fee_row:I = 0x7f0d049b

.field public static final sales_report_item_row:I = 0x7f0d049c

.field public static final sales_report_item_variation_row:I = 0x7f0d049d

.field public static final sales_report_overview:I = 0x7f0d049e

.field public static final sales_report_payment_method_row:I = 0x7f0d049f

.field public static final sales_report_payment_method_total_row:I = 0x7f0d04a0

.field public static final sales_report_recycler_view_space:I = 0x7f0d04a1

.field public static final sales_report_sales_details_row:I = 0x7f0d04a2

.field public static final sales_report_sales_summary_header:I = 0x7f0d04a3

.field public static final sales_report_sales_summary_payments_table_header:I = 0x7f0d04a4

.field public static final sales_report_sales_summary_row:I = 0x7f0d04a5

.field public static final sales_report_sales_summary_sales_table_header:I = 0x7f0d04a6

.field public static final sales_report_section_header:I = 0x7f0d04a7

.field public static final sales_report_section_header_with_image:I = 0x7f0d04a8

.field public static final sales_report_subsection_header:I = 0x7f0d04a9

.field public static final sales_report_time_selector_view:I = 0x7f0d04aa

.field public static final sales_report_top_bar_view:I = 0x7f0d04ab

.field public static final sales_report_two_col_payment_method_row:I = 0x7f0d04ac

.field public static final sales_report_two_col_sales_details_row:I = 0x7f0d04ad

.field public static final sales_report_two_toggle_row:I = 0x7f0d04ae

.field public static final sales_report_v2_overview:I = 0x7f0d04af

.field public static final sales_report_view:I = 0x7f0d04b0

.field public static final sales_report_view_v1:I = 0x7f0d04b1

.field public static final save_card_spinner_view:I = 0x7f0d04b2

.field public static final save_spinner_contents:I = 0x7f0d04b3

.field public static final scales_settings_view:I = 0x7f0d04b4

.field public static final screen_container:I = 0x7f0d04b5

.field public static final section_header:I = 0x7f0d04b6

.field public static final section_title_row:I = 0x7f0d04b7

.field public static final secure_touch_key_view:I = 0x7f0d04b8

.field public static final select_dialog_item_material:I = 0x7f0d04b9

.field public static final select_dialog_multichoice_material:I = 0x7f0d04ba

.field public static final select_dialog_singlechoice_material:I = 0x7f0d04bb

.field public static final select_method_content_layout:I = 0x7f0d04bc

.field public static final select_option_values:I = 0x7f0d04bd

.field public static final select_option_values_add_option_row:I = 0x7f0d04be

.field public static final select_option_values_for_variation:I = 0x7f0d04bf

.field public static final select_options:I = 0x7f0d04c0

.field public static final select_single_option_value_for_variation:I = 0x7f0d04c1

.field public static final select_tender_view:I = 0x7f0d04c2

.field public static final select_variations_to_create:I = 0x7f0d04c3

.field public static final seller_cash_received_layout:I = 0x7f0d04c4

.field public static final send_reminder_view:I = 0x7f0d04c5

.field public static final settings_applet_list_row:I = 0x7f0d04c6

.field public static final settings_applet_payment_types_list_row:I = 0x7f0d04c7

.field public static final settings_sections_view:I = 0x7f0d04c8

.field public static final setup_guide_card:I = 0x7f0d04c9

.field public static final setup_guide_dialog_layout:I = 0x7f0d04ca

.field public static final setup_guide_intro_card_contents:I = 0x7f0d04cb

.field public static final setup_guide_order_reader_card_contents:I = 0x7f0d04cc

.field public static final shared_settings_view:I = 0x7f0d04cd

.field public static final show_result_view:I = 0x7f0d04ce

.field public static final sign_out_button_list:I = 0x7f0d04cf

.field public static final sign_out_button_sidebar:I = 0x7f0d04d0

.field public static final sign_view:I = 0x7f0d04d1

.field public static final signature_scaling_tip_option_item:I = 0x7f0d04d2

.field public static final signature_settings_view:I = 0x7f0d04d3

.field public static final signature_tip_option_item:I = 0x7f0d04d4

.field public static final single_choice_list_item:I = 0x7f0d04d5

.field public static final single_picker_view:I = 0x7f0d04d6

.field public static final smart_line_row:I = 0x7f0d04d7

.field public static final smart_line_row_contents:I = 0x7f0d04d8

.field public static final smart_line_row_listitem:I = 0x7f0d04d9

.field public static final smoked_glass_dialog:I = 0x7f0d04da

.field public static final sms_picker_view:I = 0x7f0d04db

.field public static final splash_page_accept_payments:I = 0x7f0d04dc

.field public static final splash_page_build_trust:I = 0x7f0d04dd

.field public static final splash_page_button_bar:I = 0x7f0d04de

.field public static final splash_page_container:I = 0x7f0d04df

.field public static final splash_page_reports:I = 0x7f0d04e0

.field public static final splash_page_sell_in_minutes:I = 0x7f0d04e1

.field public static final split_tender_amount_layout:I = 0x7f0d04e2

.field public static final split_tender_completed_payment_row:I = 0x7f0d04e3

.field public static final split_tender_completed_payments_header:I = 0x7f0d04e4

.field public static final split_tender_custom_amount_row:I = 0x7f0d04e5

.field public static final split_tender_custom_even_split_layout:I = 0x7f0d04e6

.field public static final split_tender_even_split_row:I = 0x7f0d04e7

.field public static final split_tender_help_banner_row:I = 0x7f0d04e8

.field public static final split_ticket_item_row:I = 0x7f0d04e9

.field public static final split_ticket_note_row:I = 0x7f0d04ea

.field public static final split_ticket_tax_total_row:I = 0x7f0d04eb

.field public static final split_ticket_view:I = 0x7f0d04ec

.field public static final square_card_activated_google_pay_view:I = 0x7f0d04ed

.field public static final square_card_activated_view:I = 0x7f0d04ee

.field public static final square_card_activation_card_confirmation_view:I = 0x7f0d04ef

.field public static final square_card_activation_complete_view:I = 0x7f0d04f0

.field public static final square_card_activation_create_pin_view:I = 0x7f0d04f1

.field public static final square_card_code_confirmation_view:I = 0x7f0d04f2

.field public static final square_card_confirm_address_view:I = 0x7f0d04f3

.field public static final square_card_enter_phone_number_view:I = 0x7f0d04f4

.field public static final square_card_for_upsell:I = 0x7f0d04f5

.field public static final square_card_instant_deposit_result_view:I = 0x7f0d04f6

.field public static final square_card_instant_deposit_view:I = 0x7f0d04f7

.field public static final square_card_order_stamp_view:I = 0x7f0d04f8

.field public static final square_card_ordered_deposits_info_view:I = 0x7f0d04f9

.field public static final square_card_ordered_view:I = 0x7f0d04fa

.field public static final square_card_preview_back_name_cvc_pan:I = 0x7f0d04fb

.field public static final square_card_preview_view:I = 0x7f0d04fc

.field public static final square_card_preview_view_back:I = 0x7f0d04fd

.field public static final square_card_preview_view_front:I = 0x7f0d04fe

.field public static final square_card_progress_view:I = 0x7f0d04ff

.field public static final square_card_view_billing_address_layout:I = 0x7f0d0500

.field public static final standard_units_list:I = 0x7f0d0501

.field public static final standard_units_list_create_custom_unit_row:I = 0x7f0d0502

.field public static final star_view:I = 0x7f0d0503

.field public static final start_drawer_modal:I = 0x7f0d0504

.field public static final status_view:I = 0x7f0d0505

.field public static final stock_count_row:I = 0x7f0d0506

.field public static final store_and_forward_quick_enable_view:I = 0x7f0d0507

.field public static final store_and_forward_settings_enable_view:I = 0x7f0d0508

.field public static final store_and_forward_settings_view:I = 0x7f0d0509

.field public static final summary:I = 0x7f0d050a

.field public static final support_simple_spinner_dropdown_item:I = 0x7f0d050b

.field public static final swipe_chip_cards_settings_enable_view:I = 0x7f0d050c

.field public static final swipe_chip_cards_settings_view:I = 0x7f0d050d

.field public static final system_permission_content:I = 0x7f0d050e

.field public static final system_permission_dialog:I = 0x7f0d050f

.field public static final system_permission_revoked_view:I = 0x7f0d0510

.field public static final tablet_tip_button_contents:I = 0x7f0d0511

.field public static final tax_applicable_items_view:I = 0x7f0d0512

.field public static final tax_detail_view:I = 0x7f0d0513

.field public static final tax_fee_type_view:I = 0x7f0d0514

.field public static final tax_item_pricing_view:I = 0x7f0d0515

.field public static final tax_settings_add_tax_row:I = 0x7f0d0516

.field public static final taxes_list_view:I = 0x7f0d0517

.field public static final tender_order_ticket_name_view:I = 0x7f0d0518

.field public static final test_action_chip:I = 0x7f0d0519

.field public static final test_design_checkbox:I = 0x7f0d051a

.field public static final test_reflow_chipgroup:I = 0x7f0d051b

.field public static final test_toolbar:I = 0x7f0d051c

.field public static final test_toolbar_custom_background:I = 0x7f0d051d

.field public static final test_toolbar_elevation:I = 0x7f0d051e

.field public static final test_toolbar_surface:I = 0x7f0d051f

.field public static final text_above_image_splash_page:I = 0x7f0d0520

.field public static final text_above_image_splash_page_button_bar:I = 0x7f0d0521

.field public static final text_above_image_splash_page_container:I = 0x7f0d0522

.field public static final text_view_with_line_height_from_appearance:I = 0x7f0d0523

.field public static final text_view_with_line_height_from_layout:I = 0x7f0d0524

.field public static final text_view_with_line_height_from_style:I = 0x7f0d0525

.field public static final text_view_with_theme_line_height:I = 0x7f0d0526

.field public static final text_view_without_line_height:I = 0x7f0d0527

.field public static final ticket_detail_buttons:I = 0x7f0d0528

.field public static final ticket_detail_checkable_ticket_group:I = 0x7f0d0529

.field public static final ticket_detail_convert_to_custom_ticket_button:I = 0x7f0d052a

.field public static final ticket_detail_header:I = 0x7f0d052b

.field public static final ticket_detail_ticket_group_header:I = 0x7f0d052c

.field public static final ticket_detail_view:I = 0x7f0d052d

.field public static final ticket_employee_row:I = 0x7f0d052e

.field public static final ticket_list_button_row:I = 0x7f0d052f

.field public static final ticket_list_error_row:I = 0x7f0d0530

.field public static final ticket_list_no_tickets:I = 0x7f0d0531

.field public static final ticket_list_section_header_row:I = 0x7f0d0532

.field public static final ticket_list_simple_no_tickets:I = 0x7f0d0533

.field public static final ticket_list_sort_row:I = 0x7f0d0534

.field public static final ticket_list_text_row:I = 0x7f0d0535

.field public static final ticket_list_ticket_row:I = 0x7f0d0536

.field public static final ticket_list_view:I = 0x7f0d0537

.field public static final ticket_list_view_contents:I = 0x7f0d0538

.field public static final ticket_sort_group:I = 0x7f0d0539

.field public static final ticket_template_row:I = 0x7f0d053a

.field public static final ticket_transfer_employees_view:I = 0x7f0d053b

.field public static final ticket_view:I = 0x7f0d053c

.field public static final tile_appearance_view:I = 0x7f0d053d

.field public static final time_picker_popup:I = 0x7f0d053e

.field public static final time_tracking_settings:I = 0x7f0d053f

.field public static final timecards:I = 0x7f0d0540

.field public static final timecards_action_bar:I = 0x7f0d0541

.field public static final timecards_add_or_edit_notes:I = 0x7f0d0542

.field public static final timecards_clockin_confirmation:I = 0x7f0d0543

.field public static final timecards_clockout_confirmation:I = 0x7f0d0544

.field public static final timecards_clockout_summary:I = 0x7f0d0545

.field public static final timecards_clockout_summary_data_cell:I = 0x7f0d0546

.field public static final timecards_clockout_summary_header_cell:I = 0x7f0d0547

.field public static final timecards_clockout_summary_line_separator:I = 0x7f0d0548

.field public static final timecards_end_break_confirmation:I = 0x7f0d0549

.field public static final timecards_list_breaks:I = 0x7f0d054a

.field public static final timecards_list_breaks_button:I = 0x7f0d054b

.field public static final timecards_loading_modal:I = 0x7f0d054c

.field public static final timecards_modal:I = 0x7f0d054d

.field public static final timecards_start_break_or_clockout:I = 0x7f0d054e

.field public static final timecards_success:I = 0x7f0d054f

.field public static final timecards_success_buttons_view:I = 0x7f0d0550

.field public static final timecards_success_modal:I = 0x7f0d0551

.field public static final timecards_view_notes:I = 0x7f0d0552

.field public static final tip_settings_view:I = 0x7f0d0553

.field public static final tip_view:I = 0x7f0d0554

.field public static final tip_view_custom_stub:I = 0x7f0d0555

.field public static final title_value:I = 0x7f0d0556

.field public static final togglebutton_row_check:I = 0x7f0d0557

.field public static final togglebutton_row_radio_preserve:I = 0x7f0d0558

.field public static final togglebutton_row_radio_shorten:I = 0x7f0d0559

.field public static final togglebutton_row_switch_preserve:I = 0x7f0d055a

.field public static final togglebutton_row_switch_shorten:I = 0x7f0d055b

.field public static final tool_tip:I = 0x7f0d055c

.field public static final tour_page:I = 0x7f0d055d

.field public static final tour_popup_view:I = 0x7f0d055e

.field public static final transaction_load_more:I = 0x7f0d055f

.field public static final transaction_load_more_error:I = 0x7f0d0560

.field public static final transactions_history_list_header_row:I = 0x7f0d0561

.field public static final transactions_history_list_row:I = 0x7f0d0562

.field public static final transactions_history_sidebar_list_row:I = 0x7f0d0563

.field public static final transfer_reports_card_payments_row:I = 0x7f0d0564

.field public static final transfer_reports_clickable_fee_row:I = 0x7f0d0565

.field public static final transfer_reports_detail_bank_or_card:I = 0x7f0d0566

.field public static final transfer_reports_detail_card_payments_title:I = 0x7f0d0567

.field public static final transfer_reports_detail_date_time:I = 0x7f0d0568

.field public static final transfer_reports_detail_deposit_number:I = 0x7f0d0569

.field public static final transfer_reports_detail_toggles:I = 0x7f0d056a

.field public static final transfer_reports_detail_view:I = 0x7f0d056b

.field public static final transfer_reports_load_more_layout:I = 0x7f0d056c

.field public static final transfer_reports_title_section:I = 0x7f0d056d

.field public static final transfer_reports_view:I = 0x7f0d056e

.field public static final transfer_result_view:I = 0x7f0d056f

.field public static final transfer_to_bank_view:I = 0x7f0d0570

.field public static final transparent_container:I = 0x7f0d0571

.field public static final troubleshooting_section:I = 0x7f0d0572

.field public static final tutorial2_banner_view:I = 0x7f0d0573

.field public static final tutorial2_dialog:I = 0x7f0d0574

.field public static final tutorial2_tooltip_view:I = 0x7f0d0575

.field public static final tutorial_bar_view:I = 0x7f0d0576

.field public static final tutorial_content:I = 0x7f0d0577

.field public static final tutorial_dialog:I = 0x7f0d0578

.field public static final tutorial_screen_image:I = 0x7f0d0579

.field public static final tutorials:I = 0x7f0d057a

.field public static final two_factor_details_picker_view:I = 0x7f0d057b

.field public static final unit_detail_view:I = 0x7f0d057c

.field public static final unit_picker_view:I = 0x7f0d057d

.field public static final update_existing_variations_with_additional_option:I = 0x7f0d057e

.field public static final verification_code_google_auth_view:I = 0x7f0d057f

.field public static final verification_code_sms_view:I = 0x7f0d0580

.field public static final verify_card_change_failure_view:I = 0x7f0d0581

.field public static final verify_card_change_success_view:I = 0x7f0d0582

.field public static final verifying_card_change_view:I = 0x7f0d0583

.field public static final view_in_dashboard:I = 0x7f0d0584

.field public static final view_notes_entry:I = 0x7f0d0585

.field public static final view_rewards_screen:I = 0x7f0d0586

.field public static final view_stack_layout:I = 0x7f0d0587

.field public static final void_comp_view:I = 0x7f0d0588

.field public static final warning_banner_row:I = 0x7f0d0589

.field public static final warning_confirm_dialog_layout:I = 0x7f0d058a

.field public static final warning_content:I = 0x7f0d058b

.field public static final warning_view:I = 0x7f0d058c

.field public static final week:I = 0x7f0d058d

.field public static final whats_new_tour_view:I = 0x7f0d058e

.field public static final workflow_address_layout:I = 0x7f0d058f

.field public static final world_landing_view:I = 0x7f0d0590

.field public static final wrapping_name_value_row:I = 0x7f0d0591

.field public static final x2_date_picker_layout:I = 0x7f0d0592

.field public static final x2_time_picker_layout:I = 0x7f0d0593

.field public static final xable_auto_complete_edit_text:I = 0x7f0d0594

.field public static final xable_edit_text:I = 0x7f0d0595


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18677
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
