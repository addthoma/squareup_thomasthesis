.class public final Lcom/squareup/referrals/ReferralRunner_Factory;
.super Ljava/lang/Object;
.source "ReferralRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/referrals/ReferralRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/referral/ReferralService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/referrals/ReferralListener;",
            ">;"
        }
    .end annotation
.end field

.field private final arg9Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/referral/ReferralService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/referrals/ReferralListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;",
            ">;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 48
    iput-object p2, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 49
    iput-object p3, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 50
    iput-object p4, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 51
    iput-object p5, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 52
    iput-object p6, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 53
    iput-object p7, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 54
    iput-object p8, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg7Provider:Ljavax/inject/Provider;

    .line 55
    iput-object p9, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg8Provider:Ljavax/inject/Provider;

    .line 56
    iput-object p10, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg9Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/referrals/ReferralRunner_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/referral/ReferralService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/referrals/ReferralListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;",
            ">;)",
            "Lcom/squareup/referrals/ReferralRunner_Factory;"
        }
    .end annotation

    .line 70
    new-instance v11, Lcom/squareup/referrals/ReferralRunner_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/referrals/ReferralRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/server/referral/ReferralService;Ljava/text/DateFormat;Lcom/squareup/analytics/Analytics;Ljava/util/Locale;Lio/reactivex/Scheduler;Lcom/squareup/referrals/ReferralListener;Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;)Lcom/squareup/referrals/ReferralRunner;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/server/referral/ReferralService;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/analytics/Analytics;",
            "Ljava/util/Locale;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/referrals/ReferralListener;",
            "Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;",
            ")",
            "Lcom/squareup/referrals/ReferralRunner;"
        }
    .end annotation

    .line 76
    new-instance v11, Lcom/squareup/referrals/ReferralRunner;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/referrals/ReferralRunner;-><init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/server/referral/ReferralService;Ljava/text/DateFormat;Lcom/squareup/analytics/Analytics;Ljava/util/Locale;Lio/reactivex/Scheduler;Lcom/squareup/referrals/ReferralListener;Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/referrals/ReferralRunner;
    .locals 11

    .line 61
    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/server/referral/ReferralService;

    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/util/Locale;

    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg8Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/referrals/ReferralListener;

    iget-object v0, p0, Lcom/squareup/referrals/ReferralRunner_Factory;->arg9Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;

    invoke-static/range {v1 .. v10}, Lcom/squareup/referrals/ReferralRunner_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/server/referral/ReferralService;Ljava/text/DateFormat;Lcom/squareup/analytics/Analytics;Ljava/util/Locale;Lio/reactivex/Scheduler;Lcom/squareup/referrals/ReferralListener;Lcom/squareup/experiments/ShowMultipleRewardsCopyExperiment;)Lcom/squareup/referrals/ReferralRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/referrals/ReferralRunner_Factory;->get()Lcom/squareup/referrals/ReferralRunner;

    move-result-object v0

    return-object v0
.end method
