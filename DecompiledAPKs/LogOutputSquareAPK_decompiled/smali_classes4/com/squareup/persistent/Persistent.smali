.class public interface abstract Lcom/squareup/persistent/Persistent;
.super Ljava/lang/Object;
.source "Persistent.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract get(Lcom/squareup/persistent/AsyncCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/persistent/AsyncCallback<",
            "TT;>;)V"
        }
    .end annotation
.end method

.method public abstract getSynchronous()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract set(Ljava/lang/Object;Lcom/squareup/persistent/AsyncCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/squareup/persistent/AsyncCallback<",
            "TT;>;)V"
        }
    .end annotation
.end method

.method public abstract setSynchronous(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public abstract setSynchronous(Ljava/lang/Object;Z)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation
.end method
