.class public Lcom/squareup/persistent/RealAtomicSyncedValue;
.super Ljava/lang/Object;
.source "RealAtomicSyncedValue.java"

# interfaces
.implements Lcom/squareup/persistent/AtomicSyncedValue;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/persistent/AtomicSyncedValue<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final gson:Lcom/google/gson/Gson;

.field private hotCache:Z

.field private final syncedString:Lcom/squareup/persistent/SyncedString;

.field private final type:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "TT;>;"
        }
    .end annotation
.end field

.field private value:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/File;Lcom/google/gson/Gson;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Lcom/google/gson/Gson;",
            "Ljava/lang/Class<",
            "TT;>;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lcom/squareup/persistent/SyncedString;

    invoke-direct {v0, p1}, Lcom/squareup/persistent/SyncedString;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/squareup/persistent/RealAtomicSyncedValue;->syncedString:Lcom/squareup/persistent/SyncedString;

    .line 17
    iput-object p2, p0, Lcom/squareup/persistent/RealAtomicSyncedValue;->gson:Lcom/google/gson/Gson;

    .line 18
    iput-object p3, p0, Lcom/squareup/persistent/RealAtomicSyncedValue;->type:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public final declared-synchronized get()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    monitor-enter p0

    .line 28
    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/persistent/RealAtomicSyncedValue;->hotCache:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/persistent/RealAtomicSyncedValue;->value:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 30
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/squareup/persistent/RealAtomicSyncedValue;->gson:Lcom/google/gson/Gson;

    iget-object v1, p0, Lcom/squareup/persistent/RealAtomicSyncedValue;->syncedString:Lcom/squareup/persistent/SyncedString;

    invoke-virtual {v1}, Lcom/squareup/persistent/SyncedString;->get()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/persistent/RealAtomicSyncedValue;->type:Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/persistent/RealAtomicSyncedValue;->value:Ljava/lang/Object;

    const/4 v0, 0x1

    .line 31
    iput-boolean v0, p0, Lcom/squareup/persistent/RealAtomicSyncedValue;->hotCache:Z

    .line 32
    iget-object v0, p0, Lcom/squareup/persistent/RealAtomicSyncedValue;->value:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized set(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    monitor-enter p0

    .line 22
    :try_start_0
    iget-object v0, p0, Lcom/squareup/persistent/RealAtomicSyncedValue;->syncedString:Lcom/squareup/persistent/SyncedString;

    iget-object v1, p0, Lcom/squareup/persistent/RealAtomicSyncedValue;->gson:Lcom/google/gson/Gson;

    invoke-virtual {v1, p1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/persistent/SyncedString;->set(Ljava/lang/String;)V

    .line 23
    iput-object p1, p0, Lcom/squareup/persistent/RealAtomicSyncedValue;->value:Ljava/lang/Object;

    const/4 p1, 0x1

    .line 24
    iput-boolean p1, p0, Lcom/squareup/persistent/RealAtomicSyncedValue;->hotCache:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
