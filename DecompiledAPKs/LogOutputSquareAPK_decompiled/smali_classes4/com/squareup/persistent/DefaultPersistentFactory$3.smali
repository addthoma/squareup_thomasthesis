.class Lcom/squareup/persistent/DefaultPersistentFactory$3;
.super Lcom/squareup/persistent/PersistentFile;
.source "DefaultPersistentFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/persistent/DefaultPersistentFactory;->getByteFile(Ljava/io/File;)Lcom/squareup/persistent/Persistent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/persistent/PersistentFile<",
        "[B>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/persistent/DefaultPersistentFactory;


# direct methods
.method constructor <init>(Lcom/squareup/persistent/DefaultPersistentFactory;Ljava/io/File;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/squareup/persistent/DefaultPersistentFactory$3;->this$0:Lcom/squareup/persistent/DefaultPersistentFactory;

    invoke-direct {p0, p2, p3, p4}, Lcom/squareup/persistent/PersistentFile;-><init>(Ljava/io/File;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 73
    invoke-virtual {p0, p1}, Lcom/squareup/persistent/DefaultPersistentFactory$3;->read(Ljava/io/InputStream;)[B

    move-result-object p1

    return-object p1
.end method

.method protected read(Ljava/io/InputStream;)[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 76
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v1, 0x4000

    new-array v1, v1, [B

    .line 80
    :goto_0
    array-length v2, v1

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v3, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 81
    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 84
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 73
    check-cast p1, [B

    invoke-virtual {p0, p1, p2}, Lcom/squareup/persistent/DefaultPersistentFactory$3;->write([BLjava/io/OutputStream;)V

    return-void
.end method

.method protected write([BLjava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p1, :cond_0

    goto :goto_0

    .line 88
    :cond_0
    invoke-static {}, Lcom/squareup/persistent/DefaultPersistentFactory;->access$100()[B

    move-result-object p1

    :goto_0
    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method
