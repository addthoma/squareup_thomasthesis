.class Lcom/squareup/persistent/DefaultPersistentFactory$2;
.super Lcom/squareup/persistent/PersistentFile;
.source "DefaultPersistentFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/persistent/DefaultPersistentFactory;->getStringFile(Ljava/io/File;)Lcom/squareup/persistent/Persistent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/persistent/PersistentFile<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/persistent/DefaultPersistentFactory;


# direct methods
.method constructor <init>(Lcom/squareup/persistent/DefaultPersistentFactory;Ljava/io/File;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/squareup/persistent/DefaultPersistentFactory$2;->this$0:Lcom/squareup/persistent/DefaultPersistentFactory;

    invoke-direct {p0, p2, p3, p4}, Lcom/squareup/persistent/PersistentFile;-><init>(Ljava/io/File;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 60
    invoke-virtual {p0, p1}, Lcom/squareup/persistent/DefaultPersistentFactory$2;->read(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected read(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 62
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    sget-object v2, Lcom/squareup/util/Strings;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v1, p1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 60
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/persistent/DefaultPersistentFactory$2;->write(Ljava/lang/String;Ljava/io/OutputStream;)V

    return-void
.end method

.method protected write(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    .line 67
    :goto_0
    invoke-static {p1}, Lcom/squareup/util/Strings;->getBytes(Ljava/lang/String;)[B

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method
