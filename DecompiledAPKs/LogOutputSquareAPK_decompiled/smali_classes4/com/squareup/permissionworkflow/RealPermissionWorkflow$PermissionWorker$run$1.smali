.class final Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "RealPermissionWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;->run()Lkotlinx/coroutines/flow/Flow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/flow/FlowCollector<",
        "-",
        "Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult;",
        ">;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lkotlin/Unit;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealPermissionWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealPermissionWorkflow.kt\ncom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1\n*L\n1#1,99:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\u008a@\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lkotlinx/coroutines/flow/FlowCollector;",
        "Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.permissionworkflow.RealPermissionWorkflow$PermissionWorker$run$1"
    f = "RealPermissionWorkflow.kt"
    i = {
        0x0,
        0x0,
        0x1,
        0x1,
        0x2,
        0x2,
        0x2,
        0x2,
        0x3,
        0x3,
        0x3
    }
    l = {
        0x3f,
        0x43,
        0x4b,
        0x48
    }
    m = "invokeSuspend"
    n = {
        "$this$flow",
        "permission",
        "$this$flow",
        "permission",
        "$this$flow",
        "permission",
        "permissionRequestResult",
        "employeeToken",
        "$this$flow",
        "permission",
        "permissionRequestResult"
    }
    s = {
        "L$0",
        "L$1",
        "L$0",
        "L$1",
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$0",
        "L$1",
        "L$2"
    }
.end annotation


# instance fields
.field L$0:Ljava/lang/Object;

.field L$1:Ljava/lang/Object;

.field L$2:Ljava/lang/Object;

.field L$3:Ljava/lang/Object;

.field L$4:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/flow/FlowCollector;

.field final synthetic this$0:Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;


# direct methods
.method constructor <init>(Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->this$0:Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;

    iget-object v1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->this$0:Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;

    invoke-direct {v0, v1, p2}, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;-><init>(Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/flow/FlowCollector;

    iput-object p1, v0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->p$:Lkotlinx/coroutines/flow/FlowCollector;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 59
    iget v1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->label:I

    const/4 v2, 0x4

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-eqz v1, :cond_4

    if-eq v1, v5, :cond_3

    if-eq v1, v4, :cond_2

    if-eq v1, v3, :cond_1

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$2:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/permissions/PermissionAccessResult;

    iget-object v0, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$1:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/permissions/Permission;

    iget-object v0, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$0:Ljava/lang/Object;

    check-cast v0, Lkotlinx/coroutines/flow/FlowCollector;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto/16 :goto_7

    .line 91
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 59
    :cond_1
    iget-object v1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$4:Ljava/lang/Object;

    check-cast v1, Lkotlinx/coroutines/flow/FlowCollector;

    iget-object v3, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$3:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$2:Ljava/lang/Object;

    check-cast v3, Lcom/squareup/permissions/PermissionAccessResult;

    iget-object v4, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$1:Ljava/lang/Object;

    check-cast v4, Lcom/squareup/permissions/Permission;

    iget-object v5, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$0:Ljava/lang/Object;

    check-cast v5, Lkotlinx/coroutines/flow/FlowCollector;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto/16 :goto_4

    :cond_2
    iget-object v1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$1:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/permissions/Permission;

    iget-object v4, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$0:Ljava/lang/Object;

    check-cast v4, Lkotlinx/coroutines/flow/FlowCollector;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$1:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/permissions/Permission;

    iget-object v4, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$0:Ljava/lang/Object;

    check-cast v4, Lkotlinx/coroutines/flow/FlowCollector;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->p$:Lkotlinx/coroutines/flow/FlowCollector;

    .line 60
    iget-object v1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->this$0:Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;

    invoke-static {v1}, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;->access$getPermissionRequestInput$p(Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;)Lcom/squareup/permissionworkflow/PermissionRequestInput;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/permissionworkflow/PermissionRequestInput;->getPermission()Lcom/squareup/permissions/Permission;

    move-result-object v1

    .line 61
    iget-object v6, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->this$0:Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;

    invoke-static {v6}, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;->access$getPermissionRequestInput$p(Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;)Lcom/squareup/permissionworkflow/PermissionRequestInput;

    move-result-object v6

    .line 62
    instance-of v7, v6, Lcom/squareup/permissionworkflow/PermissionRequestInput$ExplicitAccess;

    if-eqz v7, :cond_6

    .line 63
    iget-object v4, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->this$0:Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;

    invoke-static {v4}, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;->access$getPermissionGatekeeper$p(Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;)Lcom/squareup/permissions/PermissionGatekeeper;

    move-result-object v4

    iput-object p1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$0:Ljava/lang/Object;

    iput-object v1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$1:Ljava/lang/Object;

    iput v5, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->label:I

    invoke-static {v4, v1, p0}, Lcom/squareup/permissions/PermissionGatekeepersKt;->seekExplicitPermissionSuspend(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/Permission;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v4

    if-ne v4, v0, :cond_5

    return-object v0

    :cond_5
    move-object v8, v4

    move-object v4, p1

    move-object p1, v8

    :goto_0
    check-cast p1, Lcom/squareup/permissions/PermissionAccessResult;

    :goto_1
    move-object v8, v4

    move-object v4, v1

    move-object v1, v8

    goto :goto_3

    .line 66
    :cond_6
    instance-of v5, v6, Lcom/squareup/permissionworkflow/PermissionRequestInput$ImplicitAccess;

    if-eqz v5, :cond_d

    .line 67
    iget-object v5, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->this$0:Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;

    invoke-static {v5}, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;->access$getPermissionGatekeeper$p(Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;)Lcom/squareup/permissions/PermissionGatekeeper;

    move-result-object v5

    iput-object p1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$0:Ljava/lang/Object;

    iput-object v1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$1:Ljava/lang/Object;

    iput v4, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->label:I

    invoke-static {v5, v1, p0}, Lcom/squareup/permissions/PermissionGatekeepersKt;->seekPermissionSuspend(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/Permission;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v4

    if-ne v4, v0, :cond_7

    return-object v0

    :cond_7
    move-object v8, v4

    move-object v4, p1

    move-object p1, v8

    :goto_2
    check-cast p1, Lcom/squareup/permissions/PermissionAccessResult;

    goto :goto_1

    .line 73
    :goto_3
    instance-of v5, p1, Lcom/squareup/permissions/PermissionAccessResult$Success;

    if-eqz v5, :cond_a

    .line 74
    move-object v5, p1

    check-cast v5, Lcom/squareup/permissions/PermissionAccessResult$Success;

    invoke-virtual {v5}, Lcom/squareup/permissions/PermissionAccessResult$Success;->getEmployeeToken()Ljava/lang/String;

    move-result-object v5

    .line 75
    iget-object v6, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->this$0:Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;

    invoke-static {v6}, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;->access$getPasscodeEmployeeManagement$p(Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker;)Lcom/squareup/permissions/PasscodeEmployeeManagement;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->maybeOneEmployeeByToken(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object v6

    const-string v7, "passcodeEmployeeManageme\u2026yeeByToken(employeeToken)"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v6, Lio/reactivex/MaybeSource;

    iput-object v1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$0:Ljava/lang/Object;

    iput-object v4, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$1:Ljava/lang/Object;

    iput-object p1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$2:Ljava/lang/Object;

    iput-object v5, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$3:Ljava/lang/Object;

    iput-object v1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$4:Ljava/lang/Object;

    iput v3, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->label:I

    .line 76
    invoke-static {v6, p0}, Lkotlinx/coroutines/rx2/RxAwaitKt;->await(Lio/reactivex/MaybeSource;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v3

    if-ne v3, v0, :cond_8

    return-object v0

    :cond_8
    move-object v5, v1

    move-object v8, v3

    move-object v3, p1

    move-object p1, v8

    .line 75
    :goto_4
    check-cast p1, Lcom/squareup/permissions/Employee;

    if-eqz p1, :cond_9

    .line 78
    new-instance v6, Lcom/squareup/protos/client/Employee$Builder;

    invoke-direct {v6}, Lcom/squareup/protos/client/Employee$Builder;-><init>()V

    .line 79
    iget-object v7, p1, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/squareup/protos/client/Employee$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v6

    .line 80
    iget-object v7, p1, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/squareup/protos/client/Employee$Builder;->first_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v6

    .line 81
    iget-object p1, p1, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    invoke-virtual {v6, p1}, Lcom/squareup/protos/client/Employee$Builder;->last_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object p1

    .line 82
    invoke-virtual {p1}, Lcom/squareup/protos/client/Employee$Builder;->build()Lcom/squareup/protos/client/Employee;

    move-result-object p1

    goto :goto_5

    :cond_9
    const/4 p1, 0x0

    .line 85
    :goto_5
    new-instance v6, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult$Success;

    invoke-direct {v6, p1}, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult$Success;-><init>(Lcom/squareup/protos/client/Employee;)V

    check-cast v6, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult;

    move-object p1, v3

    move-object v3, v1

    move-object v1, v5

    goto :goto_6

    .line 88
    :cond_a
    instance-of v3, p1, Lcom/squareup/permissions/PermissionAccessResult$Failure;

    if-eqz v3, :cond_c

    sget-object v3, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult$Failure;->INSTANCE:Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult$Failure;

    move-object v6, v3

    check-cast v6, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorkerResult;

    move-object v3, v1

    .line 72
    :goto_6
    iput-object v1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$0:Ljava/lang/Object;

    iput-object v4, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$1:Ljava/lang/Object;

    iput-object p1, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->L$2:Ljava/lang/Object;

    iput v2, p0, Lcom/squareup/permissionworkflow/RealPermissionWorkflow$PermissionWorker$run$1;->label:I

    .line 71
    invoke-interface {v3, v6, p0}, Lkotlinx/coroutines/flow/FlowCollector;->emit(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_b

    return-object v0

    .line 91
    :cond_b
    :goto_7
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1

    .line 88
    :cond_c
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 67
    :cond_d
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
