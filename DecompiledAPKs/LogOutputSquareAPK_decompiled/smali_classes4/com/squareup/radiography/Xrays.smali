.class public final Lcom/squareup/radiography/Xrays;
.super Ljava/lang/Object;
.source "Xrays.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/radiography/Xrays$Builder;
    }
.end annotation


# instance fields
.field private final showTextFieldContent:Z

.field private final skippedIds:[I

.field private final textFieldMaxLength:I


# direct methods
.method private constructor <init>(Lcom/squareup/radiography/Xrays$Builder;)V
    .locals 1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {p1}, Lcom/squareup/radiography/Xrays$Builder;->access$000(Lcom/squareup/radiography/Xrays$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/radiography/Xrays;->showTextFieldContent:Z

    .line 29
    invoke-static {p1}, Lcom/squareup/radiography/Xrays$Builder;->access$100(Lcom/squareup/radiography/Xrays$Builder;)I

    move-result v0

    iput v0, p0, Lcom/squareup/radiography/Xrays;->textFieldMaxLength:I

    .line 30
    invoke-static {p1}, Lcom/squareup/radiography/Xrays$Builder;->access$200(Lcom/squareup/radiography/Xrays$Builder;)[I

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    new-array p1, p1, [I

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lcom/squareup/radiography/Xrays$Builder;->access$200(Lcom/squareup/radiography/Xrays$Builder;)[I

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/squareup/radiography/Xrays;->skippedIds:[I

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/radiography/Xrays$Builder;Lcom/squareup/radiography/Xrays$1;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/radiography/Xrays;-><init>(Lcom/squareup/radiography/Xrays$Builder;)V

    return-void
.end method

.method private static appendLinePrefix(Ljava/lang/StringBuilder;IJ)V
    .locals 10

    add-int/lit8 v0, p1, -0x1

    const/16 v1, 0xa0

    .line 192
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-gt v2, v0, :cond_5

    const/16 v3, 0x20

    if-lez v2, :cond_0

    .line 195
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v4, 0x1

    shl-int v5, v4, v2

    int-to-long v5, v5

    and-long/2addr v5, p2

    const-wide/16 v7, 0x0

    cmp-long v9, v5, v7

    if-eqz v9, :cond_1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_3

    if-ne v2, v0, :cond_2

    const/16 v3, 0x60

    .line 200
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 202
    :cond_2
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_3
    if-ne v2, v0, :cond_4

    const/16 v3, 0x2b

    .line 206
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_4
    const/16 v3, 0x7c

    .line 208
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    if-lez p1, :cond_6

    const-string p1, "-"

    .line 213
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    return-void
.end method

.method public static create()Lcom/squareup/radiography/Xrays;
    .locals 1

    .line 66
    new-instance v0, Lcom/squareup/radiography/Xrays$Builder;

    invoke-direct {v0}, Lcom/squareup/radiography/Xrays$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/radiography/Xrays$Builder;->build()Lcom/squareup/radiography/Xrays;

    move-result-object v0

    return-object v0
.end method

.method private findLastNonSkippedChildIndex(Landroid/view/ViewGroup;)I
    .locals 2

    .line 174
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 176
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 177
    invoke-direct {p0, v1}, Lcom/squareup/radiography/Xrays;->skipChild(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method private scanRecursively(Ljava/lang/StringBuilder;IJLandroid/view/View;)V
    .locals 10

    .line 153
    invoke-static {p1, p2, p3, p4}, Lcom/squareup/radiography/Xrays;->appendLinePrefix(Ljava/lang/StringBuilder;IJ)V

    .line 154
    invoke-direct {p0, p1, p5}, Lcom/squareup/radiography/Xrays;->viewToString(Ljava/lang/StringBuilder;Landroid/view/View;)V

    const/16 v0, 0xa

    .line 155
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 157
    instance-of v0, p5, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 158
    check-cast p5, Landroid/view/ViewGroup;

    .line 159
    invoke-direct {p0, p5}, Lcom/squareup/radiography/Xrays;->findLastNonSkippedChildIndex(Landroid/view/ViewGroup;)I

    move-result v0

    .line 160
    invoke-virtual {p5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    const/4 v3, 0x0

    :goto_0
    if-gt v3, v1, :cond_2

    if-ne v3, v0, :cond_0

    shl-int v4, v2, p2

    int-to-long v4, v4

    or-long/2addr p3, v4

    .line 165
    :cond_0
    invoke-virtual {p5, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 166
    invoke-direct {p0, v9}, Lcom/squareup/radiography/Xrays;->skipChild(Landroid/view/View;)Z

    move-result v4

    if-nez v4, :cond_1

    add-int/lit8 v6, p2, 0x1

    move-object v4, p0

    move-object v5, p1

    move-wide v7, p3

    .line 167
    invoke-direct/range {v4 .. v9}, Lcom/squareup/radiography/Xrays;->scanRecursively(Ljava/lang/StringBuilder;IJLandroid/view/View;)V

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private skipChild(Landroid/view/View;)Z
    .locals 1

    .line 185
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/squareup/radiography/Xrays;->skippedIds:[I

    invoke-static {v0, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result p1

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private viewToString(Ljava/lang/StringBuilder;Landroid/view/View;)V
    .locals 5

    if-nez p2, :cond_0

    const-string p2, "null"

    .line 220
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void

    .line 224
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " { "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 227
    :try_start_0
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "id:"

    .line 228
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    :catch_0
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    goto :goto_0

    :cond_2
    const-string v0, "GONE, "

    .line 236
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_3
    const-string v0, "INVISIBLE, "

    .line 239
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    :goto_0
    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "x"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "px"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    invoke-virtual {p2}, Landroid/view/View;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, ", focused"

    .line 246
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, ", disabled"

    .line 250
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    :cond_5
    invoke-virtual {p2}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, ", selected"

    .line 254
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    :cond_6
    instance-of v0, p2, Landroid/widget/TextView;

    if-eqz v0, :cond_9

    .line 258
    move-object v0, p2

    check-cast v0, Landroid/widget/TextView;

    .line 259
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_8

    const-string v2, ", text-length:"

    .line 261
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 262
    iget-boolean v2, p0, Lcom/squareup/radiography/Xrays;->showTextFieldContent:Z

    if-eqz v2, :cond_8

    .line 263
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    iget v3, p0, Lcom/squareup/radiography/Xrays;->textFieldMaxLength:I

    if-le v2, v3, :cond_7

    .line 264
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    iget v4, p0, Lcom/squareup/radiography/Xrays;->textFieldMaxLength:I

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v1, v3, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "\u2026"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_7
    const-string v2, ", text:\""

    .line 266
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, "\""

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    :cond_8
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v2, 0x3

    if-lt v1, v2, :cond_9

    .line 270
    invoke-virtual {v0}, Landroid/widget/TextView;->isInputMethodTarget()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, ", ime-target"

    .line 271
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    :cond_9
    instance-of v0, p2, Landroid/widget/Checkable;

    if-eqz v0, :cond_a

    .line 276
    check-cast p2, Landroid/widget/Checkable;

    .line 277
    invoke-interface {p2}, Landroid/widget/Checkable;->isChecked()Z

    move-result p2

    if-eqz p2, :cond_a

    const-string p2, ", checked"

    .line 278
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    const-string p2, " }"

    .line 281
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method public static varargs withSkippedIds([I)Lcom/squareup/radiography/Xrays;
    .locals 1

    .line 70
    new-instance v0, Lcom/squareup/radiography/Xrays$Builder;

    invoke-direct {v0}, Lcom/squareup/radiography/Xrays$Builder;-><init>()V

    invoke-virtual {v0, p0}, Lcom/squareup/radiography/Xrays$Builder;->skippedIds([I)Lcom/squareup/radiography/Xrays$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/radiography/Xrays$Builder;->build()Lcom/squareup/radiography/Xrays;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public scan(Landroid/view/View;)Ljava/lang/String;
    .locals 1

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    invoke-virtual {p0, v0, p1}, Lcom/squareup/radiography/Xrays;->scan(Ljava/lang/StringBuilder;Landroid/view/View;)V

    .line 130
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public scan(Ljava/lang/StringBuilder;Landroid/view/View;)V
    .locals 8

    .line 139
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const-string v1, "\n"

    if-eqz p2, :cond_0

    :try_start_0
    const-string/jumbo v2, "window-focus:"

    .line 142
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Landroid/view/View;->hasWindowFocus()Z

    move-result v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object v7, p2

    .line 144
    invoke-direct/range {v2 .. v7}, Lcom/squareup/radiography/Xrays;->scanRecursively(Ljava/lang/StringBuilder;IJLandroid/view/View;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p2

    .line 146
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception when going through view hierarchy: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    return-void
.end method

.method public scanAllWindows()Ljava/lang/String;
    .locals 6

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 84
    invoke-static {}, Lcom/squareup/radiography/WindowScanner;->getInstance()Lcom/squareup/radiography/WindowScanner;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/radiography/WindowScanner;->findAllRootViews()Ljava/util/List;

    move-result-object v1

    .line 86
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 87
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_0

    const-string v3, "\n"

    .line 88
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    const/4 v4, 0x0

    .line 92
    instance-of v5, v3, Landroid/view/WindowManager$LayoutParams;

    if-eqz v5, :cond_1

    .line 93
    check-cast v3, Landroid/view/WindowManager$LayoutParams;

    .line 94
    invoke-virtual {v3}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 96
    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_1
    if-nez v4, :cond_2

    .line 100
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    .line 102
    :cond_2
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ":\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    invoke-virtual {p0, v0, v2}, Lcom/squareup/radiography/Xrays;->scan(Ljava/lang/StringBuilder;Landroid/view/View;)V

    goto :goto_0

    .line 105
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public scanFromRoot(Landroid/view/View;)Ljava/lang/String;
    .locals 0

    .line 114
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/radiography/Xrays;->scan(Landroid/view/View;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public scanFromRoot(Ljava/lang/StringBuilder;Landroid/view/View;)V
    .locals 0

    .line 123
    invoke-virtual {p2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/radiography/Xrays;->scan(Ljava/lang/StringBuilder;Landroid/view/View;)V

    return-void
.end method
