.class public final Lcom/squareup/quantity/PerUnitFormatter_Factory;
.super Ljava/lang/Object;
.source "PerUnitFormatter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        ">;"
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/quantity/PerUnitFormatter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/quantity/PerUnitFormatter_Factory;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/quantity/PerUnitFormatter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/quantity/PerUnitFormatter_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/quantity/PerUnitFormatter_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/quantity/PerUnitFormatter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/text/Formatter;Ljavax/inject/Provider;)Lcom/squareup/quantity/PerUnitFormatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/quantity/PerUnitFormatter;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/quantity/PerUnitFormatter;

    invoke-direct {v0, p0, p1}, Lcom/squareup/quantity/PerUnitFormatter;-><init>(Lcom/squareup/text/Formatter;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/quantity/PerUnitFormatter;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/quantity/PerUnitFormatter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/quantity/PerUnitFormatter_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/squareup/quantity/PerUnitFormatter_Factory;->newInstance(Lcom/squareup/text/Formatter;Ljavax/inject/Provider;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/quantity/PerUnitFormatter_Factory;->get()Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v0

    return-object v0
.end method
