.class public final Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;
.super Ljava/lang/Object;
.source "QuickAmountsTutorial.kt"

# interfaces
.implements Lcom/squareup/tutorialv2/Tutorial;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/quickamounts/ui/QuickAmountsTutorial$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nQuickAmountsTutorial.kt\nKotlin\n*S Kotlin\n*F\n+ 1 QuickAmountsTutorial.kt\ncom/squareup/quickamounts/ui/QuickAmountsTutorial\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n*L\n1#1,204:1\n119#2,4:205\n*E\n*S KotlinDebug\n*F\n+ 1 QuickAmountsTutorial.kt\ncom/squareup/quickamounts/ui/QuickAmountsTutorial\n*L\n51#1,4:205\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0000\u0018\u0000 )2\u00020\u0001:\u0001)B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0008\u0010\u0014\u001a\u00020\u000fH\u0002J\u0008\u0010\u0015\u001a\u00020\u0016H\u0002J\u0008\u0010\u0017\u001a\u00020\u000fH\u0002J\u0008\u0010\u0018\u001a\u00020\u000fH\u0002J\u0010\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0008\u0010\u001c\u001a\u00020\u0016H\u0016J\u0008\u0010\u001d\u001a\u00020\u0016H\u0016J\u001a\u0010\u001e\u001a\u00020\u00162\u0006\u0010\u001f\u001a\u00020 2\u0008\u0010!\u001a\u0004\u0018\u00010\"H\u0016J\u0018\u0010#\u001a\u00020\u00162\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010$\u001a\u00020%H\u0016J\u000e\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\'H\u0016J\u0008\u0010(\u001a\u00020\u000fH\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\r\u001a\u0010\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0011\u001a\u0010\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u00120\u00120\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0013\u001a\u0010\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u00120\u00120\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;",
        "Lcom/squareup/tutorialv2/Tutorial;",
        "quickAmountsSettings",
        "Lcom/squareup/quickamounts/QuickAmountsSettings;",
        "appletsDrawerRunner",
        "Lcom/squareup/applet/AppletsDrawerRunner;",
        "startMode",
        "Lcom/squareup/quickamounts/ui/StartMode;",
        "appIdling",
        "Lcom/squareup/ui/main/AppIdling;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/quickamounts/ui/StartMode;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/analytics/Analytics;)V",
        "output",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/tutorialv2/TutorialState;",
        "kotlin.jvm.PlatformType",
        "padDisplayed",
        "",
        "quickAmountsDisplayed",
        "autoAmountsTutorial",
        "finishTutorial",
        "",
        "manualTutorialFirstPage",
        "manualTutorialSecondPage",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitRequested",
        "onExitScope",
        "onTutorialEvent",
        "name",
        "",
        "value",
        "",
        "onTutorialPendingActionEvent",
        "pendingAction",
        "Lcom/squareup/tutorialv2/TutorialCore$PendingAction;",
        "tutorialState",
        "Lio/reactivex/Observable;",
        "updatedAmountsTutorial",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/quickamounts/ui/QuickAmountsTutorial$Companion;

.field public static final QUICK_AMOUNTS_TUTORIAL_KEEP:Ljava/lang/String; = "Quick Amounts Keep"

.field public static final QUICK_AMOUNTS_TUTORIAL_NEXT:Ljava/lang/String; = "Quick Amounts Next"

.field public static final QUICK_AMOUNTS_TUTORIAL_REJECT:Ljava/lang/String; = "Quick Amounts Reject"

.field public static final QUICK_AMOUNTS_TUTORIAL_UPDATE_ACKNOWLEDGED:Ljava/lang/String; = "Quick Amounts Update Acknowledged"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final appIdling:Lcom/squareup/ui/main/AppIdling;

.field private final appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

.field private final output:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/tutorialv2/TutorialState;",
            ">;"
        }
    .end annotation
.end field

.field private final padDisplayed:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final quickAmountsDisplayed:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

.field private final startMode:Lcom/squareup/quickamounts/ui/StartMode;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->Companion:Lcom/squareup/quickamounts/ui/QuickAmountsTutorial$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/quickamounts/ui/StartMode;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "quickAmountsSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appletsDrawerRunner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "startMode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appIdling"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    iput-object p2, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

    iput-object p3, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->startMode:Lcom/squareup/quickamounts/ui/StartMode;

    iput-object p4, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->appIdling:Lcom/squareup/ui/main/AppIdling;

    iput-object p5, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->analytics:Lcom/squareup/analytics/Analytics;

    .line 42
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<TutorialState>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 43
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<Boolean>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->padDisplayed:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 44
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->quickAmountsDisplayed:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$autoAmountsTutorial(Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;)Lcom/squareup/tutorialv2/TutorialState;
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->autoAmountsTutorial()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getOutput$p(Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getStartMode$p(Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;)Lcom/squareup/quickamounts/ui/StartMode;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->startMode:Lcom/squareup/quickamounts/ui/StartMode;

    return-object p0
.end method

.method public static final synthetic access$manualTutorialFirstPage(Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;)Lcom/squareup/tutorialv2/TutorialState;
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->manualTutorialFirstPage()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updatedAmountsTutorial(Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;)Lcom/squareup/tutorialv2/TutorialState;
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->updatedAmountsTutorial()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p0

    return-object p0
.end method

.method private final autoAmountsTutorial()Lcom/squareup/tutorialv2/TutorialState;
    .locals 4

    .line 136
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    sget v1, Lcom/squareup/quickamounts/ui/impl/R$string;->quick_amounts_tutorial_message:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 138
    sget v1, Lcom/squareup/quickamounts/ui/R$id;->quick_amounts:I

    sget-object v2, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->TOOLTIP_BELOW_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    .line 139
    sget-object v3, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_CENTER:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    .line 137
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/tutorialv2/TutorialState$Builder;->idAnchor(ILcom/squareup/tutorialv2/TutorialState$TooltipGravity;Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 142
    sget v1, Lcom/squareup/quickamounts/ui/impl/R$string;->quick_amounts_tutorial_button_deny:I

    const-string v2, "Quick Amounts Reject"

    .line 141
    invoke-virtual {v0, v1, v2}, Lcom/squareup/tutorialv2/TutorialState$Builder;->primaryButton(ILjava/lang/String;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 146
    sget v1, Lcom/squareup/quickamounts/ui/impl/R$string;->quick_amounts_tutorial_button_accept:I

    const-string v2, "Quick Amounts Keep"

    .line 145
    invoke-virtual {v0, v1, v2}, Lcom/squareup/tutorialv2/TutorialState$Builder;->secondaryButton(ILjava/lang/String;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method

.method private final finishTutorial()V
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/tutorialv2/TutorialState;->FINISHED:Lcom/squareup/tutorialv2/TutorialState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final manualTutorialFirstPage()Lcom/squareup/tutorialv2/TutorialState;
    .locals 4

    .line 153
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    sget v1, Lcom/squareup/quickamounts/ui/impl/R$string;->quick_amounts_manual_tutorial_message1:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 155
    sget v1, Lcom/squareup/quickamounts/ui/R$id;->quick_amounts:I

    sget-object v2, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->TOOLTIP_BELOW_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    .line 156
    sget-object v3, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_CENTER:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    .line 154
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/tutorialv2/TutorialState$Builder;->idAnchor(ILcom/squareup/tutorialv2/TutorialState$TooltipGravity;Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 158
    invoke-virtual {v0, v1, v2}, Lcom/squareup/tutorialv2/TutorialState$Builder;->step(II)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 160
    sget v1, Lcom/squareup/quickamounts/ui/impl/R$string;->quick_amounts_manual_tutorial_button_next:I

    const-string v2, "Quick Amounts Next"

    .line 159
    invoke-virtual {v0, v1, v2}, Lcom/squareup/tutorialv2/TutorialState$Builder;->primaryButton(ILjava/lang/String;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 163
    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method

.method private final manualTutorialSecondPage()Lcom/squareup/tutorialv2/TutorialState;
    .locals 4

    .line 167
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    sget v1, Lcom/squareup/quickamounts/ui/impl/R$string;->quick_amounts_manual_tutorial_message2:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 169
    sget v1, Lcom/squareup/quickamounts/ui/R$id;->quick_amounts:I

    sget-object v2, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->TOOLTIP_BELOW_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    .line 170
    sget-object v3, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_CENTER:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    .line 168
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/tutorialv2/TutorialState$Builder;->idAnchor(ILcom/squareup/tutorialv2/TutorialState$TooltipGravity;Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    const/4 v1, 0x2

    .line 172
    invoke-virtual {v0, v1, v1}, Lcom/squareup/tutorialv2/TutorialState$Builder;->step(II)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 174
    sget v1, Lcom/squareup/quickamounts/ui/impl/R$string;->quick_amounts_tutorial_button_deny:I

    const-string v2, "Quick Amounts Reject"

    .line 173
    invoke-virtual {v0, v1, v2}, Lcom/squareup/tutorialv2/TutorialState$Builder;->primaryButton(ILjava/lang/String;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 178
    sget v1, Lcom/squareup/quickamounts/ui/impl/R$string;->quick_amounts_tutorial_button_accept:I

    const-string v2, "Quick Amounts Keep"

    .line 177
    invoke-virtual {v0, v1, v2}, Lcom/squareup/tutorialv2/TutorialState$Builder;->secondaryButton(ILjava/lang/String;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method

.method private final updatedAmountsTutorial()Lcom/squareup/tutorialv2/TutorialState;
    .locals 4

    .line 185
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    sget v1, Lcom/squareup/quickamounts/ui/impl/R$string;->quick_amounts_tutorial_update_message:I

    invoke-virtual {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 187
    sget v1, Lcom/squareup/quickamounts/ui/R$id;->quick_amounts:I

    sget-object v2, Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;->TOOLTIP_BELOW_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;

    .line 188
    sget-object v3, Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;->TOOLTIP_ALIGN_CENTER:Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;

    .line 186
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/tutorialv2/TutorialState$Builder;->idAnchor(ILcom/squareup/tutorialv2/TutorialState$TooltipGravity;Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 191
    sget v1, Lcom/squareup/quickamounts/ui/impl/R$string;->quick_amounts_tutorial_update_button:I

    const-string v2, "Quick Amounts Update Acknowledged"

    .line 190
    invoke-virtual {v0, v1, v2}, Lcom/squareup/tutorialv2/TutorialState$Builder;->primaryButton(ILjava/lang/String;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object v0

    .line 194
    invoke-virtual {v0}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 5

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 52
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->padDisplayed:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    iget-object v1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->quickAmountsDisplayed:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v1, Lio/reactivex/Observable;

    iget-object v2, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->appIdling:Lcom/squareup/ui/main/AppIdling;

    invoke-virtual {v2}, Lcom/squareup/ui/main/AppIdling;->onIdleChanged()Lio/reactivex/Observable;

    move-result-object v2

    .line 53
    iget-object v3, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

    invoke-virtual {v3}, Lcom/squareup/applet/AppletsDrawerRunner;->drawerStatePublisher()Lio/reactivex/Observable;

    move-result-object v3

    sget-object v4, Lcom/squareup/applet/AppletsDrawerRunner$DrawerState;->CLOSED:Lcom/squareup/applet/AppletsDrawerRunner$DrawerState;

    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v3

    const-string v4, "appletsDrawerRunner.draw\u2026isher().startWith(CLOSED)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    check-cast v0, Lio/reactivex/ObservableSource;

    check-cast v1, Lio/reactivex/ObservableSource;

    check-cast v2, Lio/reactivex/ObservableSource;

    check-cast v3, Lio/reactivex/ObservableSource;

    .line 207
    new-instance v4, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial$onEnterScope$$inlined$combineLatest$1;

    invoke-direct {v4}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial$onEnterScope$$inlined$combineLatest$1;-><init>()V

    check-cast v4, Lio/reactivex/functions/Function4;

    .line 205
    invoke-static {v0, v1, v2, v3, v4}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026ion(t1, t2, t3, t4) }\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "combineLatest(\n        p\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    new-instance v1, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial$onEnterScope$2;-><init>(Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 75
    iget-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->startMode:Lcom/squareup/quickamounts/ui/StartMode;

    sget-object v0, Lcom/squareup/quickamounts/ui/StartMode$ManualEligibilityStart;->INSTANCE:Lcom/squareup/quickamounts/ui/StartMode$ManualEligibilityStart;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    const-string v0, "KeyPad Displayed"

    .line 76
    invoke-virtual {p0, v0, p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->onTutorialEvent(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onExitRequested()V
    .locals 0

    .line 123
    invoke-direct {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->finishTutorial()V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onTutorialEvent(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    const-string p2, "name"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p2

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sparse-switch p2, :sswitch_data_0

    goto/16 :goto_1

    :sswitch_0
    const-string p2, "Favorites Tab Selected"

    .line 110
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto/16 :goto_0

    :sswitch_1
    const-string p2, "Quick Amounts Update Acknowledged"

    .line 104
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 105
    invoke-direct {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->finishTutorial()V

    goto/16 :goto_1

    :sswitch_2
    const-string p2, "Shown SelectMethodScreen"

    .line 110
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto/16 :goto_0

    :sswitch_3
    const-string p2, "Shown EnterPasscodeToUnlockScreen"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto/16 :goto_0

    :sswitch_4
    const-string p2, "Shown BuyerCartScreen"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :sswitch_5
    const-string p2, "KeyPad Displayed"

    .line 87
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 88
    iget-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->padDisplayed:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_1

    :sswitch_6
    const-string p2, "Quick Amounts Next"

    .line 101
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 102
    iget-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-direct {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->manualTutorialSecondPage()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_1

    :sswitch_7
    const-string p2, "Quick Amounts Keep"

    .line 93
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 94
    iget-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialAnalyticsKt;->onClickedWalkthrough(Lcom/squareup/analytics/Analytics;)V

    .line 95
    invoke-direct {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->finishTutorial()V

    goto :goto_1

    :sswitch_8
    const-string p2, "Quick Amounts Reject"

    .line 97
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 98
    invoke-direct {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->finishTutorial()V

    .line 99
    iget-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    sget-object p2, Lcom/squareup/quickamounts/QuickAmountsStatus;->DISABLED:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-interface {p1, p2}, Lcom/squareup/quickamounts/QuickAmountsSettings;->setFeatureStatus(Lcom/squareup/quickamounts/QuickAmountsStatus;)V

    goto :goto_1

    :sswitch_9
    const-string p2, "Quick Amounts Displayed"

    .line 90
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 91
    iget-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->quickAmountsDisplayed:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_1

    :sswitch_a
    const-string p2, "Quick Amounts Hidden"

    .line 107
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 108
    iget-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->quickAmountsDisplayed:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_1

    :sswitch_b
    const-string p2, "Shown PaymentPromptScreen"

    .line 110
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 115
    :goto_0
    iget-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->padDisplayed:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    :goto_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7176a1b9 -> :sswitch_b
        -0x2a0782be -> :sswitch_a
        -0x22c329b7 -> :sswitch_9
        -0x192cacc9 -> :sswitch_8
        -0x9a0eee3 -> :sswitch_7
        -0x99f8f75 -> :sswitch_6
        -0x8e5028b -> :sswitch_5
        -0x5eb0f90 -> :sswitch_4
        0xab78a32 -> :sswitch_3
        0x1fbd2f78 -> :sswitch_2
        0x2c5ee2c7 -> :sswitch_1
        0x57cb5a4f -> :sswitch_0
    .end sparse-switch
.end method

.method public onTutorialPendingActionEvent(Ljava/lang/String;Lcom/squareup/tutorialv2/TutorialCore$PendingAction;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "pendingAction"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public tutorialState()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tutorialv2/TutorialState;",
            ">;"
        }
    .end annotation

    .line 120
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
