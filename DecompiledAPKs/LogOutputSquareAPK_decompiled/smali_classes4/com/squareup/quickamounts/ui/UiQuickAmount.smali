.class public final Lcom/squareup/quickamounts/ui/UiQuickAmount;
.super Ljava/lang/Object;
.source "UiQuickAmount.kt"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/squareup/quickamounts/ui/UiQuickAmount;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0011\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0000H\u0096\u0002J\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\r\u001a\u0004\u0018\u00010\u0013H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u000cH\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/quickamounts/ui/UiQuickAmount;",
        "",
        "amount",
        "Lcom/squareup/protos/connect/v2/common/Money;",
        "formattedAmount",
        "",
        "(Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/CharSequence;)V",
        "getAmount",
        "()Lcom/squareup/protos/connect/v2/common/Money;",
        "getFormattedAmount",
        "()Ljava/lang/CharSequence;",
        "compareTo",
        "",
        "other",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "",
        "hashCode",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amount:Lcom/squareup/protos/connect/v2/common/Money;

.field private final formattedAmount:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/CharSequence;)V
    .locals 1

    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formattedAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/quickamounts/ui/UiQuickAmount;->amount:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/quickamounts/ui/UiQuickAmount;->formattedAmount:Ljava/lang/CharSequence;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/quickamounts/ui/UiQuickAmount;Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/CharSequence;ILjava/lang/Object;)Lcom/squareup/quickamounts/ui/UiQuickAmount;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/quickamounts/ui/UiQuickAmount;->amount:Lcom/squareup/protos/connect/v2/common/Money;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/quickamounts/ui/UiQuickAmount;->formattedAmount:Ljava/lang/CharSequence;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/quickamounts/ui/UiQuickAmount;->copy(Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/CharSequence;)Lcom/squareup/quickamounts/ui/UiQuickAmount;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public compareTo(Lcom/squareup/quickamounts/ui/UiQuickAmount;)I
    .locals 4

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/UiQuickAmount;->amount:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/connect/v2/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object p1, p1, Lcom/squareup/quickamounts/ui/UiQuickAmount;->amount:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/common/Money;->amount:Ljava/lang/Long;

    const-string v2, "other.amount.amount"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long p1, v0, v2

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 6
    check-cast p1, Lcom/squareup/quickamounts/ui/UiQuickAmount;

    invoke-virtual {p0, p1}, Lcom/squareup/quickamounts/ui/UiQuickAmount;->compareTo(Lcom/squareup/quickamounts/ui/UiQuickAmount;)I

    move-result p1

    return p1
.end method

.method public final component1()Lcom/squareup/protos/connect/v2/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/quickamounts/ui/UiQuickAmount;->amount:Lcom/squareup/protos/connect/v2/common/Money;

    return-object v0
.end method

.method public final component2()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/quickamounts/ui/UiQuickAmount;->formattedAmount:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/CharSequence;)Lcom/squareup/quickamounts/ui/UiQuickAmount;
    .locals 1

    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formattedAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/quickamounts/ui/UiQuickAmount;

    invoke-direct {v0, p1, p2}, Lcom/squareup/quickamounts/ui/UiQuickAmount;-><init>(Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/quickamounts/ui/UiQuickAmount;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/quickamounts/ui/UiQuickAmount;

    iget-object v0, p0, Lcom/squareup/quickamounts/ui/UiQuickAmount;->amount:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v1, p1, Lcom/squareup/quickamounts/ui/UiQuickAmount;->amount:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/quickamounts/ui/UiQuickAmount;->formattedAmount:Ljava/lang/CharSequence;

    iget-object p1, p1, Lcom/squareup/quickamounts/ui/UiQuickAmount;->formattedAmount:Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmount()Lcom/squareup/protos/connect/v2/common/Money;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/UiQuickAmount;->amount:Lcom/squareup/protos/connect/v2/common/Money;

    return-object v0
.end method

.method public final getFormattedAmount()Ljava/lang/CharSequence;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/UiQuickAmount;->formattedAmount:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/quickamounts/ui/UiQuickAmount;->amount:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/quickamounts/ui/UiQuickAmount;->formattedAmount:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UiQuickAmount(amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/ui/UiQuickAmount;->amount:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", formattedAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/ui/UiQuickAmount;->formattedAmount:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
