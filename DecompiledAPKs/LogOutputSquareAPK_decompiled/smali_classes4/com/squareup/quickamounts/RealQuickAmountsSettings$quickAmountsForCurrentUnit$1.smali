.class final Lcom/squareup/quickamounts/RealQuickAmountsSettings$quickAmountsForCurrentUnit$1;
.super Ljava/lang/Object;
.source "RealQuickAmountsSettings.kt"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/RealQuickAmountsSettings;->quickAmountsForCurrentUnit(Lcom/squareup/cogs/Cogs;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogTask<",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealQuickAmountsSettings.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealQuickAmountsSettings.kt\ncom/squareup/quickamounts/RealQuickAmountsSettings$quickAmountsForCurrentUnit$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,178:1\n704#2:179\n777#2,2:180\n*E\n*S KotlinDebug\n*F\n+ 1 RealQuickAmountsSettings.kt\ncom/squareup/quickamounts/RealQuickAmountsSettings$quickAmountsForCurrentUnit$1\n*L\n82#1:179\n82#1,2:180\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u000e\u0010\u0004\u001a\n \u0003*\u0004\u0018\u00010\u00050\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;",
        "kotlin.jvm.PlatformType",
        "local",
        "Lcom/squareup/shared/catalog/Catalog$Local;",
        "perform"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/quickamounts/RealQuickAmountsSettings;


# direct methods
.method constructor <init>(Lcom/squareup/quickamounts/RealQuickAmountsSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$quickAmountsForCurrentUnit$1;->this$0:Lcom/squareup/quickamounts/RealQuickAmountsSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 0

    .line 35
    invoke-virtual {p0, p1}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$quickAmountsForCurrentUnit$1;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;",
            ">;"
        }
    .end annotation

    .line 81
    const-class v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->readAllCatalogConnectV2Objects(Ljava/lang/Class;)Ljava/util/List;

    move-result-object p1

    const-string v0, "local.readAllCatalogConn\u2026QuickAmounts::class.java)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 180
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

    .line 82
    iget-object v3, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$quickAmountsForCurrentUnit$1;->this$0:Lcom/squareup/quickamounts/RealQuickAmountsSettings;

    invoke-static {v3}, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->access$getUserToken$p(Lcom/squareup/quickamounts/RealQuickAmountsSettings;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;->isAvailableAtLocation(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 181
    :cond_1
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
