.class final Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;
.super Ljava/lang/Object;
.source "QuickAmountsSettingsLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "RowData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0018\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0082\u0008\u0018\u00002\u00020\u0001BO\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\u0007\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u0012\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000c0\u000e\u00a2\u0006\u0002\u0010\u000fJ\t\u0010\u001c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0007H\u00c6\u0003J\t\u0010 \u001a\u00020\u0007H\u00c6\u0003J\u000f\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH\u00c6\u0003J\u0015\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000c0\u000eH\u00c6\u0003Ja\u0010#\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00072\u0008\u0008\u0002\u0010\t\u001a\u00020\u00072\u000e\u0008\u0002\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b2\u0014\u0008\u0002\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000c0\u000eH\u00c6\u0001J\u0013\u0010$\u001a\u00020\u00072\u0008\u0010%\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010&\u001a\u00020\'H\u00d6\u0001J\t\u0010(\u001a\u00020)H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0013R\u0011\u0010\t\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0013R\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u001d\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000c0\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001b\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;",
        "",
        "stableId",
        "",
        "amountEditText",
        "Lcom/squareup/workflow/text/WorkflowEditableText;",
        "editable",
        "",
        "clearable",
        "focus",
        "onClear",
        "Lkotlin/Function0;",
        "",
        "onFocused",
        "Lkotlin/Function1;",
        "(JLcom/squareup/workflow/text/WorkflowEditableText;ZZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V",
        "getAmountEditText",
        "()Lcom/squareup/workflow/text/WorkflowEditableText;",
        "getClearable",
        "()Z",
        "getEditable",
        "getFocus",
        "getOnClear",
        "()Lkotlin/jvm/functions/Function0;",
        "getOnFocused",
        "()Lkotlin/jvm/functions/Function1;",
        "getStableId",
        "()J",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amountEditText:Lcom/squareup/workflow/text/WorkflowEditableText;

.field private final clearable:Z

.field private final editable:Z

.field private final focus:Z

.field private final onClear:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onFocused:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final stableId:J


# direct methods
.method public constructor <init>(JLcom/squareup/workflow/text/WorkflowEditableText;ZZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "ZZZ",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "amountEditText"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClear"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onFocused"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->stableId:J

    iput-object p3, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->amountEditText:Lcom/squareup/workflow/text/WorkflowEditableText;

    iput-boolean p4, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->editable:Z

    iput-boolean p5, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->clearable:Z

    iput-boolean p6, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->focus:Z

    iput-object p7, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->onClear:Lkotlin/jvm/functions/Function0;

    iput-object p8, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->onFocused:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;JLcom/squareup/workflow/text/WorkflowEditableText;ZZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;
    .locals 9

    move-object v0, p0

    and-int/lit8 v1, p9, 0x1

    if-eqz v1, :cond_0

    iget-wide v1, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->stableId:J

    goto :goto_0

    :cond_0
    move-wide v1, p1

    :goto_0
    and-int/lit8 v3, p9, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->amountEditText:Lcom/squareup/workflow/text/WorkflowEditableText;

    goto :goto_1

    :cond_1
    move-object v3, p3

    :goto_1
    and-int/lit8 v4, p9, 0x4

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->editable:Z

    goto :goto_2

    :cond_2
    move v4, p4

    :goto_2
    and-int/lit8 v5, p9, 0x8

    if-eqz v5, :cond_3

    iget-boolean v5, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->clearable:Z

    goto :goto_3

    :cond_3
    move v5, p5

    :goto_3
    and-int/lit8 v6, p9, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->focus:Z

    goto :goto_4

    :cond_4
    move v6, p6

    :goto_4
    and-int/lit8 v7, p9, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->onClear:Lkotlin/jvm/functions/Function0;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p7

    :goto_5
    and-int/lit8 v8, p9, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->onFocused:Lkotlin/jvm/functions/Function1;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p8

    :goto_6
    move-wide p1, v1

    move-object p3, v3

    move p4, v4

    move p5, v5

    move p6, v6

    move-object/from16 p7, v7

    move-object/from16 p8, v8

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->copy(JLcom/squareup/workflow/text/WorkflowEditableText;ZZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->stableId:J

    return-wide v0
.end method

.method public final component2()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->amountEditText:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->editable:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->clearable:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->focus:Z

    return v0
.end method

.method public final component6()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->onClear:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component7()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->onFocused:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(JLcom/squareup/workflow/text/WorkflowEditableText;ZZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "ZZZ",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;"
        }
    .end annotation

    const-string v0, "amountEditText"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClear"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onFocused"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;

    move-object v1, v0

    move-wide v2, p1

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    invoke-direct/range {v1 .. v9}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;-><init>(JLcom/squareup/workflow/text/WorkflowEditableText;ZZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;

    iget-wide v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->stableId:J

    iget-wide v2, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->stableId:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->amountEditText:Lcom/squareup/workflow/text/WorkflowEditableText;

    iget-object v1, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->amountEditText:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->editable:Z

    iget-boolean v1, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->editable:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->clearable:Z

    iget-boolean v1, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->clearable:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->focus:Z

    iget-boolean v1, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->focus:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->onClear:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->onClear:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->onFocused:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->onFocused:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmountEditText()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->amountEditText:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final getClearable()Z
    .locals 1

    .line 243
    iget-boolean v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->clearable:Z

    return v0
.end method

.method public final getEditable()Z
    .locals 1

    .line 242
    iget-boolean v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->editable:Z

    return v0
.end method

.method public final getFocus()Z
    .locals 1

    .line 244
    iget-boolean v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->focus:Z

    return v0
.end method

.method public final getOnClear()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 245
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->onClear:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnFocused()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 246
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->onFocused:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getStableId()J
    .locals 2

    .line 240
    iget-wide v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->stableId:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 4

    iget-wide v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->stableId:J

    invoke-static {v0, v1}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->amountEditText:Lcom/squareup/workflow/text/WorkflowEditableText;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->editable:Z

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->clearable:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->focus:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->onClear:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->onFocused:Lkotlin/jvm/functions/Function1;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RowData(stableId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->stableId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", amountEditText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->amountEditText:Lcom/squareup/workflow/text/WorkflowEditableText;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", editable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->editable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", clearable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->clearable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", focus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->focus:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", onClear="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->onClear:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onFocused="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->onFocused:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
