.class final Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$5;
.super Lkotlin/jvm/internal/Lambda;
.source "RealQuickAmountsSettingsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->settingsScreen(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/workflow/RenderContext;Z)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "clearedIndex",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic $quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

.field final synthetic this$0:Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/quickamounts/QuickAmounts;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$5;->this$0:Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;

    iput-object p2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$5;->$context:Lcom/squareup/workflow/RenderContext;

    iput-object p3, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$5;->$quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 62
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$5;->invoke(I)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(I)V
    .locals 5

    .line 173
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$5;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    new-instance v1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$ClearSetAmount;

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$5;->$quickAmounts:Lcom/squareup/quickamounts/QuickAmounts;

    iget-object v3, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$5;->this$0:Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;

    invoke-static {v3}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->access$getCurrency$p(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;)Lcom/squareup/protos/connect/v2/common/Currency;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$settingsScreen$5;->this$0:Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;

    invoke-static {v4}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;->access$getAnalytics$p(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v4

    invoke-direct {v1, v2, p1, v3, v4}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$ClearSetAmount;-><init>(Lcom/squareup/quickamounts/QuickAmounts;ILcom/squareup/protos/connect/v2/common/Currency;Lcom/squareup/analytics/Analytics;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
