.class public abstract Lcom/squareup/receiving/ReceivedResponse$Error;
.super Lcom/squareup/receiving/ReceivedResponse;
.source "ReceivedResponse.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/receiving/ReceivedResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Error"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;,
        Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;,
        Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;,
        Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/receiving/ReceivedResponse<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000*\u0006\u0008\u0001\u0010\u0001 \u00012\u0008\u0012\u0004\u0012\u0002H\u00010\u0002:\u0004\u0008\t\n\u000bB\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u0082\u0001\u0004\u000c\r\u000e\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/receiving/ReceivedResponse$Error;",
        "T",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "retryable",
        "",
        "(Z)V",
        "getRetryable",
        "()Z",
        "ClientError",
        "NetworkError",
        "ServerError",
        "SessionExpired",
        "Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;",
        "Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;",
        "Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;",
        "Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final retryable:Z


# direct methods
.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, 0x0

    .line 53
    invoke-direct {p0, v0}, Lcom/squareup/receiving/ReceivedResponse;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/receiving/ReceivedResponse$Error;->retryable:Z

    return-void
.end method

.method public synthetic constructor <init>(ZLkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/receiving/ReceivedResponse$Error;-><init>(Z)V

    return-void
.end method


# virtual methods
.method public final getRetryable()Z
    .locals 1

    .line 53
    iget-boolean v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error;->retryable:Z

    return v0
.end method
