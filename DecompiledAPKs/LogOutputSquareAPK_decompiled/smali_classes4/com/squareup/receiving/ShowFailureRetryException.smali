.class final Lcom/squareup/receiving/ShowFailureRetryException;
.super Ljava/lang/Exception;
.source "StandardReceiver.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0004\u0008\u0002\u0018\u00002\u00060\u0001j\u0002`\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/receiving/ShowFailureRetryException;",
        "Ljava/lang/Exception;",
        "Lkotlin/Exception;",
        "showFailure",
        "",
        "(Ljava/lang/Object;)V",
        "getShowFailure",
        "()Ljava/lang/Object;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final showFailure:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1

    const-string v0, "showFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Throw to trigger retryWhen."

    .line 225
    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/receiving/ShowFailureRetryException;->showFailure:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final getShowFailure()Ljava/lang/Object;
    .locals 1

    .line 224
    iget-object v0, p0, Lcom/squareup/receiving/ShowFailureRetryException;->showFailure:Ljava/lang/Object;

    return-object v0
.end method
