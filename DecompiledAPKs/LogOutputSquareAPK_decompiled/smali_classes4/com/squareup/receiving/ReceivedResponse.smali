.class public abstract Lcom/squareup/receiving/ReceivedResponse;
.super Ljava/lang/Object;
.source "ReceivedResponse.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/receiving/ReceivedResponse$Okay;,
        Lcom/squareup/receiving/ReceivedResponse$Error;,
        Lcom/squareup/receiving/ReceivedResponse$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReceivedResponse.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReceivedResponse.kt\ncom/squareup/receiving/ReceivedResponse\n*L\n1#1,239:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u000c*\u0006\u0008\u0000\u0010\u0001 \u00012\u00020\u0002:\u0003\u000c\r\u000eB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J&\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u0000\"\u0004\u0008\u0001\u0010\u00052\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u0002H\u00050\u0007J+\u0010\u0004\u001a\u0002H\u0008\"\u0004\u0008\u0001\u0010\u00082\u0016\u0010\t\u001a\u0012\u0012\u0006\u0008\u0000\u0012\u00028\u0000\u0012\u0006\u0008\u0001\u0012\u0002H\u00080\nH&\u00a2\u0006\u0002\u0010\u000b\u0082\u0001\u0002\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/receiving/ReceivedResponse;",
        "T",
        "",
        "()V",
        "map",
        "S",
        "mapper",
        "Lkotlin/Function1;",
        "M",
        "javaMapper",
        "Lcom/squareup/receiving/ReceivedMapper;",
        "(Lcom/squareup/receiving/ReceivedMapper;)Ljava/lang/Object;",
        "Companion",
        "Error",
        "Okay",
        "Lcom/squareup/receiving/ReceivedResponse$Okay;",
        "Lcom/squareup/receiving/ReceivedResponse$Error;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/receiving/ReceivedResponse$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/receiving/ReceivedResponse$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/receiving/ReceivedResponse$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/receiving/ReceivedResponse;->Companion:Lcom/squareup/receiving/ReceivedResponse$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/receiving/ReceivedResponse;-><init>()V

    return-void
.end method

.method public static final rejectIfNot(Lcom/squareup/receiving/ReceivedResponse;Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/ReceivedResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/receiving/ReceivedResponse;->Companion:Lcom/squareup/receiving/ReceivedResponse$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/receiving/ReceivedResponse$Companion;->rejectIfNot(Lcom/squareup/receiving/ReceivedResponse;Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/ReceivedResponse;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+TS;>;)",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TS;>;"
        }
    .end annotation

    const-string v0, "mapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;

    move-object v1, p0

    check-cast v1, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;

    invoke-virtual {v1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;->getResponse()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse;

    goto :goto_0

    .line 106
    :cond_0
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    move-object v1, p0

    check-cast v1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {v1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse;

    goto :goto_0

    .line 107
    :cond_1
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    move-object v0, p0

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    invoke-virtual {v0}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getResponse()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {p1, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    :cond_2
    invoke-virtual {v0}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getStatusCode()I

    move-result p1

    new-instance v0, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    invoke-direct {v0, v1, p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;-><init>(Ljava/lang/Object;I)V

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse;

    goto :goto_0

    .line 108
    :cond_3
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz v0, :cond_4

    new-instance p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    move-object v0, p0

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    invoke-virtual {v0}, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;->getStatusCode()I

    move-result v0

    invoke-direct {p1, v0}, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;-><init>(I)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse;

    goto :goto_0

    .line 109
    :cond_4
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz v0, :cond_5

    sget-object p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;->INSTANCE:Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    move-object v0, p1

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse;

    goto :goto_0

    .line 110
    :cond_5
    instance-of v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    if-eqz v0, :cond_7

    move-object v0, p0

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    invoke-virtual {v0}, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;->getResponse()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    :cond_6
    new-instance p1, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    invoke-direct {p1, v1}, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;-><init>(Ljava/lang/Object;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/receiving/ReceivedResponse;

    :goto_0
    return-object v0

    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public abstract map(Lcom/squareup/receiving/ReceivedMapper;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/ReceivedMapper<",
            "-TT;+TM;>;)TM;"
        }
    .end annotation
.end method
