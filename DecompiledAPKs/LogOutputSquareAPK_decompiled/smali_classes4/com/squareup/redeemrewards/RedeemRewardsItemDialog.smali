.class public final Lcom/squareup/redeemrewards/RedeemRewardsItemDialog;
.super Lcom/squareup/redeemrewards/InRedeemRewardScope;
.source "RedeemRewardsItemDialog.kt"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory;,
        Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Runner;,
        Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;,
        Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0008\u0007\u0018\u0000 \u000b2\u00020\u0001:\u0004\u000b\u000c\r\u000eB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0014\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardsItemDialog;",
        "Lcom/squareup/redeemrewards/InRedeemRewardScope;",
        "redeemRewardScope",
        "Lcom/squareup/redeemrewards/RedeemRewardsScope;",
        "(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "Companion",
        "Factory",
        "RedeemRewardItemScreenData",
        "Runner",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/redeemrewards/RedeemRewardsItemDialog;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog;->Companion:Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Companion;

    .line 79
    sget-object v0, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Companion$CREATOR$1;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "fromParcel {\n      Redee\u2026ava.classLoader)!!)\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V
    .locals 1

    const-string v0, "redeemRewardScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/InRedeemRewardScope;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-super {p0, p1, p2}, Lcom/squareup/redeemrewards/InRedeemRewardScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 74
    invoke-virtual {p0}, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog;->getRedeemRewardsScope$redeem_rewards_release()Lcom/squareup/redeemrewards/RedeemRewardsScope;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
