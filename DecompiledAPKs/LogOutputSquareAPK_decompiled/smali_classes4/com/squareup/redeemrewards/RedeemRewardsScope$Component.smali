.class public interface abstract Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;
.super Ljava/lang/Object;
.source "RedeemRewardsScope.kt"

# interfaces
.implements Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$Component;
.implements Lcom/squareup/ui/ErrorsBarView$Component;
.implements Lcom/squareup/configure/item/ConfigureItemScope$ParentComponent;


# annotations
.annotation runtime Lcom/squareup/crm/RolodexContactLoader$SharedScope;
.end annotation

.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/redeemrewards/RedeemRewardsScope;
.end annotation

.annotation runtime Lcom/squareup/ui/ErrorsBarPresenter$SharedScope;
.end annotation

.annotation runtime Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/redeemrewards/RedeemRewardsScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/RedeemRewardsScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003J\u0008\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0006\u001a\u00020\u0007H&J\u0008\u0010\u0008\u001a\u00020\tH&J\u0008\u0010\n\u001a\u00020\u000bH&J\u0008\u0010\u000c\u001a\u00020\rH&J\u0008\u0010\u000e\u001a\u00020\u000fH&\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;",
        "Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$Component;",
        "Lcom/squareup/ui/ErrorsBarView$Component;",
        "Lcom/squareup/configure/item/ConfigureItemScope$ParentComponent;",
        "chooseCustomerCoordinatorV2",
        "Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;",
        "chooseItemInCategoryCoordinator",
        "Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;",
        "displayRewardByCodeCoordinator",
        "Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;",
        "lookupRewardByCodeCoordinator",
        "Lcom/squareup/redeemrewards/LookupRewardByCodeCoordinator;",
        "redeemPointsV2Coordinator",
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;",
        "redeemRewardsScopeRunner",
        "Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract chooseCustomerCoordinatorV2()Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;
.end method

.method public abstract chooseItemInCategoryCoordinator()Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;
.end method

.method public abstract displayRewardByCodeCoordinator()Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;
.end method

.method public abstract lookupRewardByCodeCoordinator()Lcom/squareup/redeemrewards/LookupRewardByCodeCoordinator;
.end method

.method public abstract redeemPointsV2Coordinator()Lcom/squareup/redeemrewards/RedeemRewardsV2Coordinator;
.end method

.method public abstract redeemRewardsScopeRunner()Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;
.end method
