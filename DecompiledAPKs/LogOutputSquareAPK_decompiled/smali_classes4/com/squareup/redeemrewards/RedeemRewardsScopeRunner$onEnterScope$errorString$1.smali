.class final Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$errorString$1;
.super Ljava/lang/Object;
.source "RedeemRewardsScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func2<",
        "TT1;TT2;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u001a\u0010\u0005\u001a\u0016\u0012\u0004\u0012\u00020\u0007 \u0004*\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00060\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "response1",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
        "kotlin.jvm.PlatformType",
        "response2",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$errorString$1;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/loyalty/LoyaltyStatusResponse;

    check-cast p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$errorString$1;->call(Lcom/squareup/loyalty/LoyaltyStatusResponse;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/loyalty/LoyaltyStatusResponse;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 330
    instance-of p1, p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$ErrorRetrievingLoyalty;

    if-eqz p1, :cond_0

    .line 331
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$errorString$1;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-static {p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->access$getRes$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/squareup/util/Res;

    move-result-object p1

    sget p2, Lcom/squareup/common/strings/R$string;->server_error_message:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 334
    :cond_0
    instance-of p1, p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_3

    .line 335
    check-cast p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    instance-of p2, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-nez p2, :cond_1

    const/4 p1, 0x0

    :cond_1
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;

    if-eqz p1, :cond_2

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz p1, :cond_2

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    if-eqz p1, :cond_2

    goto :goto_0

    .line 336
    :cond_2
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$errorString$1;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-static {p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->access$getPointsTermsFormatter$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/squareup/loyalty/PointsTermsFormatter;

    move-result-object p1

    sget p2, Lcom/squareup/redeemrewards/R$string;->points_redemption_error_generic:I

    invoke-virtual {p1, p2}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_3
    const-string p1, ""

    :goto_0
    return-object p1
.end method
