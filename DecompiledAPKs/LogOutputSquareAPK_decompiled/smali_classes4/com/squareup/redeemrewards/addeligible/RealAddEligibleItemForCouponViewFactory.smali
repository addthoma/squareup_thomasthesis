.class public final Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponViewFactory;
.super Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponViewFactory;
.source "RealAddEligibleItemForCouponViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponViewFactory;",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponViewFactory;",
        "chooseEligibleItemLayoutRunnerFactory",
        "Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$Factory;",
        "(Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$Factory;)V
    .locals 22
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "chooseEligibleItemLayoutRunnerFactory"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 12
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 13
    const-class v3, Lcom/squareup/redeemrewards/addeligible/LoadingDialogScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    .line 14
    sget-object v4, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponViewFactory$1;->INSTANCE:Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponViewFactory$1;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 12
    invoke-virtual {v2, v3, v4}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    move-result-object v2

    check-cast v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 17
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 18
    const-class v3, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemDialog;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    .line 19
    sget-object v4, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponViewFactory$2;->INSTANCE:Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponViewFactory$2;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 17
    invoke-virtual {v2, v3, v4}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    move-result-object v2

    check-cast v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 22
    sget-object v4, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 23
    const-class v2, Lcom/squareup/redeemrewards/addeligible/DiscountedItemDeletedScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v5

    .line 24
    sget v6, Lcom/squareup/redeemrewards/addeligible/impl/R$layout;->discounted_item_deleted_screen:I

    .line 25
    sget-object v2, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponViewFactory$3;->INSTANCE:Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponViewFactory$3;

    move-object v9, v2

    check-cast v9, Lkotlin/jvm/functions/Function1;

    .line 26
    new-instance v2, Lcom/squareup/workflow/ScreenHint;

    sget-object v16, Lcom/squareup/container/spot/Spots;->HERE_CROSS_FADE:Lcom/squareup/container/spot/Spot;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x1df

    const/16 v21, 0x0

    move-object v10, v2

    invoke-direct/range {v10 .. v21}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v8, 0x0

    const/16 v10, 0x8

    move-object v7, v2

    .line 22
    invoke-static/range {v4 .. v11}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object v2

    check-cast v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    .line 29
    sget-object v4, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 30
    const-class v2, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v5

    .line 31
    sget v6, Lcom/squareup/redeemrewards/addeligible/impl/R$layout;->choose_eligible_item_screen:I

    .line 32
    new-instance v2, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponViewFactory$4;

    invoke-direct {v2, v0}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponViewFactory$4;-><init>(Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$Factory;)V

    move-object v9, v2

    check-cast v9, Lkotlin/jvm/functions/Function1;

    .line 33
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v16, Lcom/squareup/container/spot/Spots;->HERE_CROSS_FADE:Lcom/squareup/container/spot/Spot;

    move-object v10, v0

    invoke-direct/range {v10 .. v21}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/16 v10, 0x8

    move-object v7, v0

    .line 29
    invoke-static/range {v4 .. v11}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x3

    aput-object v0, v1, v2

    move-object/from16 v0, p0

    .line 11
    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
