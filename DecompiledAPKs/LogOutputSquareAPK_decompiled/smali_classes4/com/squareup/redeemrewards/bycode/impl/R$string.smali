.class public final Lcom/squareup/redeemrewards/bycode/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/bycode/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final coupon_redeem_reward:I = 0x7f1205a8

.field public static final coupon_redeem_reward_subtitle:I = 0x7f1205a9

.field public static final coupon_search_failed:I = 0x7f1205ae

.field public static final coupon_search_reward_already_in_cart_title:I = 0x7f1205af

.field public static final coupon_search_reward_already_redeemed_title:I = 0x7f1205b0

.field public static final coupon_search_reward_expired_title:I = 0x7f1205b3

.field public static final coupon_search_reward_not_found_subtitle:I = 0x7f1205b6

.field public static final coupon_search_reward_not_found_title:I = 0x7f1205b7

.field public static final coupon_search_try_again:I = 0x7f1205b8

.field public static final enter_reward_code_hint:I = 0x7f120a7f

.field public static final enter_reward_code_hint_alphanumeric:I = 0x7f120a80

.field public static final enter_reward_code_title:I = 0x7f120a81


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
