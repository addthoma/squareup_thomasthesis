.class public final Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator_Factory;
.super Ljava/lang/Object;
.source "DisplayRewardByCodeCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final formatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator_Factory;->formatterProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator_Factory;->transactionProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)",
            "Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Lcom/squareup/payment/Transaction;)Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;
    .locals 1

    .line 47
    new-instance v0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;-><init>(Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Lcom/squareup/payment/Transaction;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;

    iget-object v1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator_Factory;->formatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    iget-object v2, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/payment/Transaction;

    invoke-static {v0, v1, v2}, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator_Factory;->newInstance(Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Lcom/squareup/payment/Transaction;)Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator_Factory;->get()Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;

    move-result-object v0

    return-object v0
.end method
