.class public final Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "DisplayRewardByCodeCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;",
        "formatter",
        "Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "(Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Lcom/squareup/payment/Transaction;)V",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

.field private final runner:Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Lcom/squareup/payment/Transaction;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transaction"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;->runner:Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;

    iput-object p2, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;->formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    iput-object p3, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;->transaction:Lcom/squareup/payment/Transaction;

    return-void
.end method

.method public static final synthetic access$getFormatter$p(Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;)Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;->formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;)Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;->runner:Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$getTransaction$p(Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 10

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 38
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 39
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v2, Lcom/squareup/redeemrewards/R$string;->coupon_redeem_rewards:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 40
    new-instance v1, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$1;-><init>(Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    const/high16 v0, 0x10e0000

    .line 42
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    .line 44
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 45
    iget-object v1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;->runner:Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;->onLookupRewardResult()Lrx/Observable;

    move-result-object v1

    const/4 v7, 0x1

    .line 46
    invoke-virtual {v1, v7}, Lrx/Observable;->take(I)Lrx/Observable;

    move-result-object v8

    const-string v1, "runner.onLookupRewardResult()\n        .take(1)"

    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    new-instance v9, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;

    move-object v1, v9

    move-object v2, p0

    move-object v3, p1

    move-object v5, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;-><init>(Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;Landroid/view/View;Landroid/content/res/Resources;Ljava/util/concurrent/atomic/AtomicBoolean;I)V

    check-cast v9, Lkotlin/jvm/functions/Function1;

    invoke-static {v8, p1, v9}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    .line 108
    invoke-virtual {v0, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 110
    sget v0, Lcom/squareup/redeemrewards/R$id;->crm_search_reward_again:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    .line 111
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    .line 112
    new-instance v1, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$3;

    invoke-direct {v1, p0}, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$3;-><init>(Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    return-void
.end method
