.class final Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$3$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ViewRewardsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$3;->invoke(Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Mutator<",
        "Lcom/squareup/redeemrewards/ViewRewardsState;",
        ">;",
        "Lcom/squareup/redeemrewards/ViewRewardsOutput$RemoveCoupon;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/redeemrewards/ViewRewardsOutput$RemoveCoupon;",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/redeemrewards/ViewRewardsState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $couponRow:Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$3$1;->$couponRow:Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/redeemrewards/ViewRewardsOutput$RemoveCoupon;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/redeemrewards/ViewRewardsState;",
            ">;)",
            "Lcom/squareup/redeemrewards/ViewRewardsOutput$RemoveCoupon;"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    new-instance p1, Lcom/squareup/redeemrewards/ViewRewardsOutput$RemoveCoupon;

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$3$1;->$couponRow:Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;

    invoke-virtual {v0}, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;->getCoupon()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    const-string v1, "couponRow.coupon.coupon_id"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p1, v0, v1, v2, v1}, Lcom/squareup/redeemrewards/ViewRewardsOutput$RemoveCoupon;-><init>(Ljava/lang/String;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Mutator;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$3$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/redeemrewards/ViewRewardsOutput$RemoveCoupon;

    move-result-object p1

    return-object p1
.end method
