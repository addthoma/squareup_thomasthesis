.class public final Lcom/squareup/redeemrewards/impl/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final coupon_empty_label:I = 0x7f0a03b7

.field public static final coupon_recycler_view:I = 0x7f0a03b9

.field public static final crm_contact_list:I = 0x7f0a040f

.field public static final crm_search_box:I = 0x7f0a04e7

.field public static final crm_search_message:I = 0x7f0a04ea

.field public static final crm_search_progress_bar:I = 0x7f0a04eb

.field public static final errorMessageLabel:I = 0x7f0a071b

.field public static final glyph:I = 0x7f0a07ae

.field public static final loyalty_rewards_recycler_view:I = 0x7f0a0994

.field public static final redeem_rewards_loyalty_rewards_subtitle:I = 0x7f0a0d2e

.field public static final redeem_rewards_loyalty_rewards_title:I = 0x7f0a0d2f

.field public static final redeem_rewards_workflow_choose_customer:I = 0x7f0a0d32

.field public static final reward_row:I = 0x7f0a0d7c

.field public static final reward_row_action:I = 0x7f0a0d7d

.field public static final reward_row_action_text:I = 0x7f0a0d7f

.field public static final reward_row_counter_view:I = 0x7f0a0d80

.field public static final reward_row_icon:I = 0x7f0a0d81

.field public static final reward_row_points_circle:I = 0x7f0a0d82

.field public static final reward_row_subtitle:I = 0x7f0a0d83

.field public static final reward_row_title:I = 0x7f0a0d84

.field public static final rewards_empty_label:I = 0x7f0a0d87

.field public static final view_rewards_screen:I = 0x7f0a1101

.field public static final view_rewards_screen_content:I = 0x7f0a1102

.field public static final view_rewards_screen_progress_bar:I = 0x7f0a1103


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
