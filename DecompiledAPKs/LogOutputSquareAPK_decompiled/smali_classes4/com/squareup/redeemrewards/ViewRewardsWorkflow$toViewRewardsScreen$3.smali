.class final Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$3;
.super Lkotlin/jvm/internal/Lambda;
.source "ViewRewardsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->toViewRewardsScreen(Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/workflow/Sink;)Lcom/squareup/redeemrewards/ViewRewardsScreen$Type$Interactive;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "couponRow",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic this$0:Lcom/squareup/redeemrewards/ViewRewardsWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$3;->this$0:Lcom/squareup/redeemrewards/ViewRewardsWorkflow;

    iput-object p2, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$3;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$3;->invoke(Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;)V
    .locals 4

    const-string v0, "couponRow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;->isApplied()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$3;->$sink:Lcom/squareup/workflow/Sink;

    iget-object v2, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$3;->this$0:Lcom/squareup/redeemrewards/ViewRewardsWorkflow;

    new-instance v3, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$3$1;

    invoke-direct {v3, p1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$3$1;-><init>(Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    invoke-static {v2, v1, v3, p1, v1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    goto :goto_0

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$3;->$sink:Lcom/squareup/workflow/Sink;

    iget-object v2, p0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$toViewRewardsScreen$3;->this$0:Lcom/squareup/redeemrewards/ViewRewardsWorkflow;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow$CouponRow;->getCoupon()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object p1

    const/4 v3, 0x2

    invoke-static {v2, p1, v1, v3, v1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->applyCouponAction$default(Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/protos/client/coupons/Coupon;Ljava/lang/Integer;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
