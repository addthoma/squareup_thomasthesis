.class final Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealRedeemRewardWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14;->invoke(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Mutator<",
        "Lcom/squareup/redeemrewards/RedeemRewardState;",
        ">;",
        "Lcom/squareup/redeemrewards/RedeemRewardOutput$AddMatchingItemAndClose;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/redeemrewards/RedeemRewardOutput$AddMatchingItemAndClose;",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/redeemrewards/RedeemRewardState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $output:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;

.field final synthetic this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14;

    iput-object p2, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14$1;->$output:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/redeemrewards/RedeemRewardOutput$AddMatchingItemAndClose;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            ">;)",
            "Lcom/squareup/redeemrewards/RedeemRewardOutput$AddMatchingItemAndClose;"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    iget-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14;

    iget-object p1, p1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    invoke-static {p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->access$getHoldsCoupons$p(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;)Lcom/squareup/checkout/HoldsCoupons;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14;

    iget-object v0, v0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14;->$state:Lcom/squareup/redeemrewards/RedeemRewardState;

    check-cast v0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;

    invoke-virtual {v0}, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->getCouponToAdd()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/checkout/HoldsCoupons;->apply(Lcom/squareup/protos/client/coupons/Coupon;)V

    .line 187
    iget-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14;

    iget-object p1, p1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    invoke-static {p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->access$getHoldsCustomer$p(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;)Lcom/squareup/payment/Transaction;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14;

    iget-object v0, v0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14;->$state:Lcom/squareup/redeemrewards/RedeemRewardState;

    check-cast v0, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;

    invoke-virtual {v0}, Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;->getNextState()Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v1}, Lcom/squareup/payment/Transaction;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 188
    new-instance p1, Lcom/squareup/redeemrewards/RedeemRewardOutput$AddMatchingItemAndClose;

    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14$1;->$output:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;

    check-cast v0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput$MatchingItemAdded;

    invoke-direct {p1, v0}, Lcom/squareup/redeemrewards/RedeemRewardOutput$AddMatchingItemAndClose;-><init>(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput$MatchingItemAdded;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Mutator;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$render$14$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/redeemrewards/RedeemRewardOutput$AddMatchingItemAndClose;

    move-result-object p1

    return-object p1
.end method
