.class public final Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;
.super Ljava/lang/Object;
.source "RedeemRewardsScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final contactLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final couponDiscountFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final couponServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/CouponsServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final holdsCouponsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkout/HoldsCoupons;",
            ">;"
        }
    .end annotation
.end field

.field private final holdsCustomerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/crm/HoldsCustomer;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryListEntryHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListEntryHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyCalculatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyCalculator;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyRulesFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyRulesFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltySettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final pointsTermsFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final redeemRewardsFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final rewardAdapterHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/ui/RewardAdapterHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final threadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/CouponsServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkout/HoldsCoupons;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/crm/HoldsCustomer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/ui/RewardAdapterHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListEntryHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyRulesFormatter;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 99
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 100
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 101
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->contactLoaderProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 102
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->couponServiceProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 103
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->loyaltyServiceProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 104
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 105
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 106
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->holdsCouponsProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 107
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->holdsCustomerProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 108
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 109
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->loyaltyCalculatorProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 110
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 111
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->pointsTermsFormatterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 112
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->couponDiscountFormatterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 113
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->rewardAdapterHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 114
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 115
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->redeemRewardsFlowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 116
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 117
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->cogsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 118
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->accountSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 119
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->libraryListEntryHandlerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 120
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->loyaltyRulesFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/CouponsServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkout/HoldsCoupons;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/crm/HoldsCustomer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/ui/RewardAdapterHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListEntryHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyRulesFormatter;",
            ">;)",
            "Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    .line 145
    new-instance v23, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;

    move-object/from16 v0, v23

    invoke-direct/range {v0 .. v22}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v23
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/loyalty/CouponsServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/checkout/HoldsCoupons;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/LoyaltyCalculator;Lcom/squareup/payment/Transaction;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Lcom/squareup/loyalty/ui/RewardAdapterHelper;Lcom/squareup/analytics/Analytics;Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;Lcom/squareup/settings/server/Features;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListEntryHandler;Lcom/squareup/loyalty/LoyaltyRulesFormatter;)Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;
    .locals 24

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    .line 158
    new-instance v23, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    move-object/from16 v0, v23

    invoke-direct/range {v0 .. v22}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;-><init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/loyalty/CouponsServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/checkout/HoldsCoupons;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/LoyaltyCalculator;Lcom/squareup/payment/Transaction;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Lcom/squareup/loyalty/ui/RewardAdapterHelper;Lcom/squareup/analytics/Analytics;Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;Lcom/squareup/settings/server/Features;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListEntryHandler;Lcom/squareup/loyalty/LoyaltyRulesFormatter;)V

    return-object v23
.end method


# virtual methods
.method public get()Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;
    .locals 24

    move-object/from16 v0, p0

    .line 125
    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->contactLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/crm/RolodexContactLoader;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->couponServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/loyalty/CouponsServiceHelper;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->loyaltyServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lio/reactivex/Scheduler;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->holdsCouponsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/checkout/HoldsCoupons;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->holdsCustomerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/payment/crm/HoldsCustomer;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/loyalty/LoyaltySettings;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->loyaltyCalculatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/loyalty/LoyaltyCalculator;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->pointsTermsFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/loyalty/PointsTermsFormatter;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->couponDiscountFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->rewardAdapterHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/loyalty/ui/RewardAdapterHelper;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->redeemRewardsFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/cogs/Cogs;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->accountSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->libraryListEntryHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/librarylist/LibraryListEntryHandler;

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->loyaltyRulesFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/loyalty/LoyaltyRulesFormatter;

    invoke-static/range {v2 .. v23}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->newInstance(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/loyalty/CouponsServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/checkout/HoldsCoupons;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/LoyaltyCalculator;Lcom/squareup/payment/Transaction;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Lcom/squareup/loyalty/ui/RewardAdapterHelper;Lcom/squareup/analytics/Analytics;Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;Lcom/squareup/settings/server/Features;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListEntryHandler;Lcom/squareup/loyalty/LoyaltyRulesFormatter;)Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner_Factory;->get()Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    move-result-object v0

    return-object v0
.end method
