.class public interface abstract Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;
.super Ljava/lang/Object;
.source "ChooseCustomerScreenV2.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/ChooseCustomerScreenV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008`\u0018\u00002\u00020\u0001J\u0008\u0010\u000e\u001a\u00020\u000fH&J\u0010\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0012H&J\u0010\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\tH&J\u0008\u0010\u0015\u001a\u00020\u000fH&R\u0018\u0010\u0002\u001a\u00020\u0003X\u00a6\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0004\u0010\u0005\"\u0004\u0008\u0006\u0010\u0007R\u0018\u0010\u0008\u001a\u00020\tX\u00a6\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\r\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;",
        "",
        "contactListScrollPosition",
        "",
        "getContactListScrollPosition",
        "()I",
        "setContactListScrollPosition",
        "(I)V",
        "contactLoaderSearchTerm",
        "",
        "getContactLoaderSearchTerm",
        "()Ljava/lang/String;",
        "setContactLoaderSearchTerm",
        "(Ljava/lang/String;)V",
        "closeChooseCustomerScreen",
        "",
        "selectContact",
        "contact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "setSearchTerm",
        "term",
        "showLookupRewardByCodeScreen",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract closeChooseCustomerScreen()V
.end method

.method public abstract getContactListScrollPosition()I
.end method

.method public abstract getContactLoaderSearchTerm()Ljava/lang/String;
.end method

.method public abstract selectContact(Lcom/squareup/protos/client/rolodex/Contact;)V
.end method

.method public abstract setContactListScrollPosition(I)V
.end method

.method public abstract setContactLoaderSearchTerm(Ljava/lang/String;)V
.end method

.method public abstract setSearchTerm(Ljava/lang/String;)V
.end method

.method public abstract showLookupRewardByCodeScreen()V
.end method
