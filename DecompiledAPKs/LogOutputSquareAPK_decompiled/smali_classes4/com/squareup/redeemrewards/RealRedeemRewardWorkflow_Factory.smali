.class public final Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealRedeemRewardWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewards/ViewRewardsWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkout/HoldsCoupons;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewards/ViewRewardsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkout/HoldsCoupons;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 45
    iput-object p2, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 46
    iput-object p3, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 47
    iput-object p4, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 48
    iput-object p5, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 49
    iput-object p6, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 50
    iput-object p7, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 51
    iput-object p8, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->arg7Provider:Ljavax/inject/Provider;

    .line 52
    iput-object p9, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->arg8Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewards/ViewRewardsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkout/HoldsCoupons;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;)",
            "Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;"
        }
    .end annotation

    .line 66
    new-instance v10, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeWorkflow;Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflow;Lcom/squareup/util/Res;Lcom/squareup/checkout/HoldsCoupons;Lcom/squareup/payment/Transaction;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyalty/LoyaltySettings;)Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;
    .locals 11

    .line 73
    new-instance v10, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;-><init>(Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeWorkflow;Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflow;Lcom/squareup/util/Res;Lcom/squareup/checkout/HoldsCoupons;Lcom/squareup/payment/Transaction;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyalty/LoyaltySettings;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;
    .locals 10

    .line 57
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeWorkflow;

    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/redeemrewards/ViewRewardsWorkflow;

    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflow;

    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/checkout/HoldsCoupons;

    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->arg8Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/loyalty/LoyaltySettings;

    invoke-static/range {v1 .. v9}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->newInstance(Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeWorkflow;Lcom/squareup/redeemrewards/ViewRewardsWorkflow;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflow;Lcom/squareup/util/Res;Lcom/squareup/checkout/HoldsCoupons;Lcom/squareup/payment/Transaction;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyalty/LoyaltySettings;)Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow_Factory;->get()Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    move-result-object v0

    return-object v0
.end method
