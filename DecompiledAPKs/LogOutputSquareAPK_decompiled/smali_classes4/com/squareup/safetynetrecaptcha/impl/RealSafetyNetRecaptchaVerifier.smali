.class public final Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;
.super Ljava/lang/Object;
.source "RealSafetyNetRecaptchaVerifier.kt"

# interfaces
.implements Lcom/squareup/safetynetrecaptcha/SafetyNetRecaptchaVerifier;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B1\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0001\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0016\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u0010\u001a\u00020\u000bH\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;",
        "Lcom/squareup/safetynetrecaptcha/SafetyNetRecaptchaVerifier;",
        "application",
        "Landroid/app/Application;",
        "googleApiAvailability",
        "Lcom/google/android/gms/common/GoogleApiAvailability;",
        "safetyNetClient",
        "Lcom/google/android/gms/safetynet/SafetyNetClient;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "installationId",
        "",
        "(Landroid/app/Application;Lcom/google/android/gms/common/GoogleApiAvailability;Lcom/google/android/gms/safetynet/SafetyNetClient;Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V",
        "verifyWithRecaptcha",
        "Lio/reactivex/Single;",
        "Lcom/squareup/safetynetrecaptcha/CaptchaResult;",
        "apiKey",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final application:Landroid/app/Application;

.field private final googleApiAvailability:Lcom/google/android/gms/common/GoogleApiAvailability;

.field private final installationId:Ljava/lang/String;

.field private final safetyNetClient:Lcom/google/android/gms/safetynet/SafetyNetClient;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/google/android/gms/common/GoogleApiAvailability;Lcom/google/android/gms/safetynet/SafetyNetClient;Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V
    .locals 1
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/settings/InstallationId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "googleApiAvailability"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "safetyNetClient"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "installationId"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;->application:Landroid/app/Application;

    iput-object p2, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;->googleApiAvailability:Lcom/google/android/gms/common/GoogleApiAvailability;

    iput-object p3, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;->safetyNetClient:Lcom/google/android/gms/safetynet/SafetyNetClient;

    iput-object p4, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p5, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;->installationId:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getInstallationId$p(Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;)Ljava/lang/String;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;->installationId:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getSafetyNetClient$p(Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;)Lcom/google/android/gms/safetynet/SafetyNetClient;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;->safetyNetClient:Lcom/google/android/gms/safetynet/SafetyNetClient;

    return-object p0
.end method


# virtual methods
.method public verifyWithRecaptcha(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/safetynetrecaptcha/CaptchaResult;",
            ">;"
        }
    .end annotation

    const-string v0, "apiKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;->googleApiAvailability:Lcom/google/android/gms/common/GoogleApiAvailability;

    iget-object v1, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;->application:Landroid/app/Application;

    check-cast v1, Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    sget-object p1, Lcom/squareup/safetynetrecaptcha/CaptchaResult$Error;->INSTANCE:Lcom/squareup/safetynetrecaptcha/CaptchaResult$Error;

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(CaptchaResult.Error)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 40
    :cond_0
    new-instance v0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;-><init>(Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/SingleOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.create { emitter \u2026sult.Error)\n      }\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
