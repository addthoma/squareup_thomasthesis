.class public final Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier_Factory;
.super Ljava/lang/Object;
.source "RealSafetyNetRecaptchaVerifier_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/android/gms/common/GoogleApiAvailability;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/android/gms/safetynet/SafetyNetClient;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/android/gms/common/GoogleApiAvailability;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/android/gms/safetynet/SafetyNetClient;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 32
    iput-object p4, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 33
    iput-object p5, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier_Factory;->arg4Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/android/gms/common/GoogleApiAvailability;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/android/gms/safetynet/SafetyNetClient;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier_Factory;"
        }
    .end annotation

    .line 44
    new-instance v6, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Landroid/app/Application;Lcom/google/android/gms/common/GoogleApiAvailability;Lcom/google/android/gms/safetynet/SafetyNetClient;Lcom/squareup/analytics/Analytics;Ljava/lang/String;)Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;
    .locals 7

    .line 49
    new-instance v6, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;-><init>(Landroid/app/Application;Lcom/google/android/gms/common/GoogleApiAvailability;Lcom/google/android/gms/safetynet/SafetyNetClient;Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;
    .locals 5

    .line 38
    iget-object v0, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/GoogleApiAvailability;

    iget-object v2, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/safetynet/SafetyNetClient;

    iget-object v3, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/analytics/Analytics;

    iget-object v4, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier_Factory;->newInstance(Landroid/app/Application;Lcom/google/android/gms/common/GoogleApiAvailability;Lcom/google/android/gms/safetynet/SafetyNetClient;Lcom/squareup/analytics/Analytics;Ljava/lang/String;)Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier_Factory;->get()Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;

    move-result-object v0

    return-object v0
.end method
