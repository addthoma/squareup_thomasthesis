.class public final Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideCoreSettingsCardParamsFactory;
.super Ljava/lang/Object;
.source "ReportsAppletCommonModule_ProvideCoreSettingsCardParamsFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideCoreSettingsCardParamsFactory;->deviceProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideCoreSettingsCardParamsFactory;->flowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideCoreSettingsCardParamsFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideCoreSettingsCardParamsFactory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideCoreSettingsCardParamsFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideCoreSettingsCardParamsFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCoreSettingsCardParams(Lcom/squareup/util/Device;Lflow/Flow;)Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;
    .locals 0

    .line 42
    invoke-static {p0, p1}, Lcom/squareup/reports/applet/ReportsAppletCommonModule;->provideCoreSettingsCardParams(Lcom/squareup/util/Device;Lflow/Flow;)Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideCoreSettingsCardParamsFactory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    iget-object v1, p0, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideCoreSettingsCardParamsFactory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflow/Flow;

    invoke-static {v0, v1}, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideCoreSettingsCardParamsFactory;->provideCoreSettingsCardParams(Lcom/squareup/util/Device;Lflow/Flow;)Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/reports/applet/ReportsAppletCommonModule_ProvideCoreSettingsCardParamsFactory;->get()Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;

    move-result-object v0

    return-object v0
.end method
