.class public Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;
.super Ljava/lang/Object;
.source "SalesSummaryReport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChartData"
.end annotation


# instance fields
.field public final chartEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;",
            ">;"
        }
    .end annotation
.end field

.field public final overlap:Z


# direct methods
.method constructor <init>(Ljava/util/List;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/SalesChartEntry;",
            ">;Z)V"
        }
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;->chartEntries:Ljava/util/List;

    .line 14
    iput-boolean p2, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;->overlap:Z

    return-void
.end method
