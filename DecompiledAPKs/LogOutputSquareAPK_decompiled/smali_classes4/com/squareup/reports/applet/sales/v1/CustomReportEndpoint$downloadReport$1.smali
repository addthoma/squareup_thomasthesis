.class final Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$1;
.super Lkotlin/jvm/internal/Lambda;
.source "CustomReportEndpoint.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->downloadReport(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)Lrx/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCustomReportEndpoint.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CustomReportEndpoint.kt\ncom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$1\n*L\n1#1,680:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "salesSummaryResponse",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$1;

    invoke-direct {v0}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$1;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$1;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 70
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$1;->invoke(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Z
    .locals 2

    const-string v0, "salesSummaryResponse"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    const-string v0, "salesSummaryResponse.custom_report"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method
