.class public Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen$Factory;
.super Ljava/lang/Object;
.source "TimeDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$1(Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;Landroid/widget/TimePicker;Landroid/content/Context;Landroid/view/View;Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;Lcom/squareup/reports/applet/sales/v1/ReportConfig;)Landroid/app/Dialog;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 58
    invoke-static {p0}, Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;->access$000(Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 60
    iget-object p5, p5, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    invoke-virtual {v0, p5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    goto :goto_0

    .line 62
    :cond_0
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 63
    iget-object p5, p5, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    invoke-virtual {v0, p5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    :goto_0
    const/16 p5, 0xb

    .line 65
    invoke-virtual {v0, p5}, Ljava/util/Calendar;->get(I)I

    move-result p5

    const/16 v1, 0xc

    .line 66
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 67
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p5

    invoke-virtual {p1, p5}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 68
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p5

    invoke-virtual {p1, p5}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    .line 70
    new-instance p5, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {p5, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 71
    invoke-virtual {p5, p3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p2

    const p3, 0x104000a

    new-instance p5, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$TimeDialogScreen$Factory$NqG_SSfp99_87eq2LffXrQzVJFQ;

    invoke-direct {p5, p1, p0, p4}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$TimeDialogScreen$Factory$NqG_SSfp99_87eq2LffXrQzVJFQ;-><init>(Landroid/widget/TimePicker;Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;)V

    .line 72
    invoke-virtual {p2, p3, p5}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    const/high16 p1, 0x1040000

    .line 86
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    const/4 p1, 0x1

    .line 87
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    .line 88
    invoke-virtual {p0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$0(Landroid/widget/TimePicker;Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 77
    invoke-virtual {p0}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p3

    .line 78
    invoke-virtual {p0}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object p4

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result p4

    .line 79
    invoke-static {p1}, Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;->access$000(Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 80
    invoke-virtual {p2, p3, p4}, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->updateStartTime(II)V

    goto :goto_0

    .line 82
    :cond_0
    invoke-virtual {p2, p3, p4}, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->updateEndTime(II)V

    .line 84
    :goto_0
    invoke-virtual {p0}, Landroid/widget/TimePicker;->clearFocus()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 45
    invoke-static {p1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;

    .line 47
    const-class v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScope$Component;

    .line 48
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScope$Component;

    .line 49
    invoke-interface {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScope$Component;->editReportConfigRunner()Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    move-result-object v6

    .line 50
    sget v0, Lcom/squareup/flowlegacy/R$layout;->time_picker_popup:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 52
    sget v0, Lcom/squareup/flowlegacy/R$id;->timePicker:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/TimePicker;

    .line 53
    invoke-static {p1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 55
    invoke-virtual {v6}, Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;->configInEdit()Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    new-instance v7, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$TimeDialogScreen$Factory$0b7S0cvghb4OoUag2UUisPM8Jxg;

    move-object v1, v7

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$TimeDialogScreen$Factory$0b7S0cvghb4OoUag2UUisPM8Jxg;-><init>(Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;Landroid/widget/TimePicker;Landroid/content/Context;Landroid/view/View;Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;)V

    invoke-virtual {v0, v7}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
