.class public final Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$loadInitialState$$inlined$zip$1;
.super Ljava/lang/Object;
.source "RxKotlin.kt"

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor;->loadInitialState()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/BiFunction<",
        "Ljava/util/Set<",
        "Lcom/squareup/permissions/Employee;",
        ">;",
        "Ljava/util/List<",
        "+",
        "Ljava/lang/String;",
        ">;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRxKotlin.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RxKotlin.kt\ncom/squareup/util/rx2/Singles$zip$1\n+ 2 ReportConfigEmployeeFilterReactor.kt\ncom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,1655:1\n105#2:1656\n106#2,6:1660\n113#2,5:1667\n1360#3:1657\n1429#3,2:1658\n1431#3:1666\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0006\u0010\u0000\u001a\u0002H\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0001*\u00020\u00032\u0006\u0010\u0005\u001a\u0002H\u00022\u0006\u0010\u0006\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "R",
        "T",
        "",
        "U",
        "t",
        "u",
        "apply",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
        "com/squareup/util/rx2/Singles$zip$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Employee;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Ljava/lang/String;",
            ">;)TR;"
        }
    .end annotation

    const-string v0, "t"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "u"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1452
    check-cast p2, Ljava/util/List;

    check-cast p1, Ljava/util/Set;

    .line 1656
    check-cast p1, Ljava/lang/Iterable;

    .line 1657
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 1658
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1659
    check-cast v1, Lcom/squareup/permissions/Employee;

    .line 1660
    new-instance v2, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;

    .line 1661
    iget-object v3, v1, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    const-string v4, "employee.firstName"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1662
    iget-object v4, v1, Lcom/squareup/permissions/Employee;->lastName:Ljava/lang/String;

    const-string v5, "employee.lastName"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1663
    iget-object v5, v1, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    const-string v6, "employee.token"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1664
    iget-object v1, v1, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    invoke-interface {p2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 1660
    invoke-direct {v2, v3, v4, v5, v1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$Employee;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1665
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1666
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 1667
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1668
    new-instance p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$AllEmployeesSelected;

    invoke-direct {p1, v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$AllEmployeesSelected;-><init>(Ljava/util/List;)V

    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;

    goto :goto_1

    .line 1670
    :cond_1
    new-instance p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$FilterByEmployeeSelected;

    invoke-direct {p1, v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState$FilterByEmployeeSelected;-><init>(Ljava/util/List;)V

    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterReactor$EmployeeFilterState;

    :goto_1
    return-object p1
.end method
