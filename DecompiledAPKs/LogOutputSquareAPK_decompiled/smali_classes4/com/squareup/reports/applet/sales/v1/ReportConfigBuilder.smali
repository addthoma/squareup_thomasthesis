.class public Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;
.super Ljava/lang/Object;
.source "ReportConfigBuilder.java"


# instance fields
.field private final DEFAULT_FILTER_BY_THIS_DEVICE:Z

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final dateAndTimeRangePhrase:Lcom/squareup/phrase/Phrase;

.field private final dateFormat:Ljava/text/DateFormat;

.field private final rangePhrase:Lcom/squareup/phrase/Phrase;

.field private final singleDayRangePhrase:Lcom/squareup/phrase/Phrase;

.field private final timeFormat:Ljava/text/DateFormat;


# direct methods
.method constructor <init>(Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 36
    iput-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->DEFAULT_FILTER_BY_THIS_DEVICE:Z

    .line 40
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->dateFormat:Ljava/text/DateFormat;

    .line 41
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->timeFormat:Ljava/text/DateFormat;

    .line 42
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->analytics:Lcom/squareup/analytics/Analytics;

    .line 43
    sget p1, Lcom/squareup/reports/applet/R$string;->report_calendar_date_range:I

    invoke-interface {p3, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->rangePhrase:Lcom/squareup/phrase/Phrase;

    .line 44
    sget p1, Lcom/squareup/reports/applet/R$string;->report_calendar_single_day_time_range:I

    invoke-interface {p3, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->singleDayRangePhrase:Lcom/squareup/phrase/Phrase;

    .line 45
    sget p1, Lcom/squareup/reports/applet/R$string;->report_calendar_date_range_with_times:I

    invoke-interface {p3, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->dateAndTimeRangePhrase:Lcom/squareup/phrase/Phrase;

    return-void
.end method

.method private updateFormattedDateRange(ZZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;
    .locals 4

    const-string v0, "end_time"

    const-string v1, "start_time"

    if-eqz p2, :cond_1

    if-eqz p1, :cond_0

    .line 272
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {p1, p3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 274
    :cond_0
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->singleDayRangePhrase:Lcom/squareup/phrase/Phrase;

    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {p2, p3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    const-string v2, "date"

    invoke-virtual {p1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    .line 275
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->singleDayRangePhrase:Lcom/squareup/phrase/Phrase;

    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->timeFormat:Ljava/text/DateFormat;

    invoke-virtual {p2, p3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    .line 276
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->singleDayRangePhrase:Lcom/squareup/phrase/Phrase;

    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->timeFormat:Ljava/text/DateFormat;

    invoke-virtual {p2, p4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    .line 277
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->singleDayRangePhrase:Lcom/squareup/phrase/Phrase;

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    const-string p2, "date_end"

    const-string v2, "date_start"

    if-eqz p1, :cond_2

    .line 281
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->rangePhrase:Lcom/squareup/phrase/Phrase;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {v0, p3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, v2, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    .line 282
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->rangePhrase:Lcom/squareup/phrase/Phrase;

    iget-object p3, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {p3, p4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    .line 283
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->rangePhrase:Lcom/squareup/phrase/Phrase;

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 285
    :cond_2
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->dateAndTimeRangePhrase:Lcom/squareup/phrase/Phrase;

    iget-object v3, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {v3, p3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    .line 286
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->dateAndTimeRangePhrase:Lcom/squareup/phrase/Phrase;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->timeFormat:Ljava/text/DateFormat;

    invoke-virtual {v2, p3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, v1, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    .line 287
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->dateAndTimeRangePhrase:Lcom/squareup/phrase/Phrase;

    iget-object p3, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {p3, p4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    .line 288
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->dateAndTimeRangePhrase:Lcom/squareup/phrase/Phrase;

    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->timeFormat:Ljava/text/DateFormat;

    invoke-virtual {p2, p4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    .line 289
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->dateAndTimeRangePhrase:Lcom/squareup/phrase/Phrase;

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private updateToDayEnd(Ljava/util/Calendar;)V
    .locals 8

    const/4 v0, 0x1

    .line 183
    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v0, 0x2

    .line 184
    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v0, 0x5

    .line 185
    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 186
    invoke-virtual {p1}, Ljava/util/Calendar;->clear()V

    const/16 v5, 0x17

    const/16 v6, 0x3b

    const/16 v7, 0x3b

    move-object v1, p1

    .line 187
    invoke-virtual/range {v1 .. v7}, Ljava/util/Calendar;->set(IIIIII)V

    return-void
.end method

.method private updateToMidnight(Ljava/util/Calendar;)V
    .locals 8

    const/4 v0, 0x1

    .line 191
    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v0, 0x2

    .line 192
    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v0, 0x5

    .line 193
    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 194
    invoke-virtual {p1}, Ljava/util/Calendar;->clear()V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    .line 195
    invoke-virtual/range {v1 .. v7}, Ljava/util/Calendar;->set(IIIIII)V

    return-void
.end method


# virtual methods
.method public changeEndTime(Lcom/squareup/reports/applet/sales/v1/ReportConfig;II)Lcom/squareup/reports/applet/sales/v1/ReportConfig;
    .locals 22

    move-object/from16 v0, p1

    move/from16 v5, p2

    .line 96
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 97
    iget-object v1, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    invoke-virtual {v7, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/4 v1, 0x1

    .line 98
    invoke-virtual {v7, v1}, Ljava/util/Calendar;->get(I)I

    move-result v8

    const/4 v1, 0x2

    .line 99
    invoke-virtual {v7, v1}, Ljava/util/Calendar;->get(I)I

    move-result v9

    const/4 v1, 0x5

    .line 100
    invoke-virtual {v7, v1}, Ljava/util/Calendar;->get(I)I

    move-result v10

    .line 102
    iget-object v1, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    .line 103
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v11

    .line 104
    invoke-virtual {v11, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 105
    invoke-static {v11, v7}, Lcom/squareup/util/Times;->sameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0xb

    .line 106
    invoke-virtual {v11, v2}, Ljava/util/Calendar;->get(I)I

    move-result v12

    const/16 v2, 0xc

    .line 107
    invoke-virtual {v11, v2}, Ljava/util/Calendar;->get(I)I

    move-result v13

    if-gt v12, v5, :cond_0

    if-ne v12, v5, :cond_1

    move/from16 v6, p3

    if-le v13, v6, :cond_2

    goto :goto_0

    :cond_0
    move/from16 v6, p3

    .line 115
    :goto_0
    invoke-virtual {v11}, Ljava/util/Calendar;->clear()V

    move-object v1, v11

    move v2, v8

    move v3, v9

    move v4, v10

    move/from16 v5, p2

    move/from16 v6, p3

    .line 116
    invoke-virtual/range {v1 .. v6}, Ljava/util/Calendar;->set(IIIII)V

    .line 117
    invoke-virtual {v11}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    move-object v14, v1

    move v5, v12

    move v6, v13

    goto :goto_1

    :cond_1
    move/from16 v6, p3

    :cond_2
    move-object v14, v1

    .line 120
    :goto_1
    invoke-virtual {v7}, Ljava/util/Calendar;->clear()V

    move-object v1, v7

    move v2, v8

    move v3, v9

    move v4, v10

    .line 121
    invoke-virtual/range {v1 .. v6}, Ljava/util/Calendar;->set(IIIII)V

    .line 122
    invoke-virtual {v7}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v15

    const/4 v1, 0x0

    .line 124
    invoke-static {v11, v7}, Lcom/squareup/util/Times;->sameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v2

    move-object/from16 v3, p0

    invoke-direct {v3, v1, v2, v14, v15}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->updateFormattedDateRange(ZZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v13

    .line 125
    new-instance v1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    iget-object v2, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->timeZone:Ljava/lang/String;

    iget-boolean v4, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->fullDays:Z

    const/16 v18, 0x0

    iget-boolean v5, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->itemDetails:Z

    iget-boolean v6, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->filterByThisDevice:Z

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->selectedEmployees:Ljava/util/List;

    move-object v12, v1

    move-object/from16 v16, v2

    move/from16 v17, v4

    move/from16 v19, v5

    move/from16 v20, v6

    move-object/from16 v21, v0

    invoke-direct/range {v12 .. v21}, Lcom/squareup/reports/applet/sales/v1/ReportConfig;-><init>(Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;ZZZZLjava/util/List;)V

    return-object v1
.end method

.method public changeStartTime(Lcom/squareup/reports/applet/sales/v1/ReportConfig;II)Lcom/squareup/reports/applet/sales/v1/ReportConfig;
    .locals 22

    move-object/from16 v0, p1

    move/from16 v5, p2

    .line 61
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 62
    iget-object v1, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    invoke-virtual {v7, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/4 v1, 0x1

    .line 63
    invoke-virtual {v7, v1}, Ljava/util/Calendar;->get(I)I

    move-result v8

    const/4 v1, 0x2

    .line 64
    invoke-virtual {v7, v1}, Ljava/util/Calendar;->get(I)I

    move-result v9

    const/4 v1, 0x5

    .line 65
    invoke-virtual {v7, v1}, Ljava/util/Calendar;->get(I)I

    move-result v10

    .line 67
    iget-object v1, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    .line 68
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v11

    .line 69
    iget-object v2, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    invoke-virtual {v11, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 70
    invoke-static {v7, v11}, Lcom/squareup/util/Times;->sameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0xb

    .line 71
    invoke-virtual {v11, v2}, Ljava/util/Calendar;->get(I)I

    move-result v12

    const/16 v2, 0xc

    .line 72
    invoke-virtual {v11, v2}, Ljava/util/Calendar;->get(I)I

    move-result v13

    if-gt v5, v12, :cond_0

    if-ne v5, v12, :cond_1

    move/from16 v6, p3

    if-le v6, v13, :cond_2

    goto :goto_0

    :cond_0
    move/from16 v6, p3

    .line 80
    :goto_0
    invoke-virtual {v11}, Ljava/util/Calendar;->clear()V

    move-object v1, v11

    move v2, v8

    move v3, v9

    move v4, v10

    move/from16 v5, p2

    move/from16 v6, p3

    .line 81
    invoke-virtual/range {v1 .. v6}, Ljava/util/Calendar;->set(IIIII)V

    .line 82
    invoke-virtual {v11}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    move-object v15, v1

    move v5, v12

    move v6, v13

    goto :goto_1

    :cond_1
    move/from16 v6, p3

    :cond_2
    move-object v15, v1

    .line 85
    :goto_1
    invoke-virtual {v7}, Ljava/util/Calendar;->clear()V

    move-object v1, v7

    move v2, v8

    move v3, v9

    move v4, v10

    .line 86
    invoke-virtual/range {v1 .. v6}, Ljava/util/Calendar;->set(IIIII)V

    .line 87
    invoke-virtual {v7}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v14

    const/4 v1, 0x0

    .line 89
    invoke-static {v7, v11}, Lcom/squareup/util/Times;->sameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v2

    move-object/from16 v3, p0

    invoke-direct {v3, v1, v2, v14, v15}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->updateFormattedDateRange(ZZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v13

    .line 90
    new-instance v1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    iget-object v2, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->timeZone:Ljava/lang/String;

    iget-boolean v4, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->fullDays:Z

    const/16 v18, 0x0

    iget-boolean v5, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->itemDetails:Z

    iget-boolean v6, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->filterByThisDevice:Z

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->selectedEmployees:Ljava/util/List;

    move-object v12, v1

    move-object/from16 v16, v2

    move/from16 v17, v4

    move/from16 v19, v5

    move/from16 v20, v6

    move-object/from16 v21, v0

    invoke-direct/range {v12 .. v21}, Lcom/squareup/reports/applet/sales/v1/ReportConfig;-><init>(Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;ZZZZLjava/util/List;)V

    return-object v1
.end method

.method public dayRange(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Ljava/util/Calendar;Ljava/util/Calendar;)Lcom/squareup/reports/applet/sales/v1/ReportConfig;
    .locals 24

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const/4 v3, 0x1

    .line 237
    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/4 v4, 0x2

    .line 238
    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v6

    const/4 v7, 0x5

    .line 239
    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 240
    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 241
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v10

    .line 242
    invoke-virtual {v2, v7}, Ljava/util/Calendar;->get(I)I

    move-result v11

    .line 243
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 245
    iget-object v4, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/16 v12, 0xb

    .line 246
    invoke-virtual {v2, v12}, Ljava/util/Calendar;->get(I)I

    move-result v8

    const/16 v13, 0xc

    .line 247
    invoke-virtual {v2, v13}, Ljava/util/Calendar;->get(I)I

    move-result v9

    .line 248
    invoke-virtual {v2}, Ljava/util/Calendar;->clear()V

    move-object v4, v2

    move v7, v1

    .line 249
    invoke-virtual/range {v4 .. v9}, Ljava/util/Calendar;->set(IIIII)V

    .line 250
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    .line 252
    iget-object v4, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 253
    invoke-virtual {v2, v12}, Ljava/util/Calendar;->get(I)I

    move-result v12

    .line 254
    invoke-virtual {v2, v13}, Ljava/util/Calendar;->get(I)I

    move-result v13

    .line 255
    invoke-virtual {v2}, Ljava/util/Calendar;->clear()V

    move-object v8, v2

    move v9, v3

    .line 256
    invoke-virtual/range {v8 .. v13}, Ljava/util/Calendar;->set(IIIII)V

    .line 257
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    .line 259
    iget-boolean v3, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->fullDays:Z

    const/4 v4, 0x0

    move-object/from16 v5, p0

    .line 260
    invoke-direct {v5, v3, v4, v1, v2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->updateFormattedDateRange(ZZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    .line 262
    new-instance v3, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    iget-object v4, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->timeZone:Ljava/lang/String;

    iget-boolean v6, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->fullDays:Z

    iget-boolean v7, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->itemDetails:Z

    iget-boolean v8, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->filterByThisDevice:Z

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->selectedEmployees:Ljava/util/List;

    const/16 v20, 0x0

    move-object v14, v3

    move-object/from16 v16, v1

    move-object/from16 v17, v2

    move-object/from16 v18, v4

    move/from16 v19, v6

    move/from16 v21, v7

    move/from16 v22, v8

    move-object/from16 v23, v0

    invoke-direct/range {v14 .. v23}, Lcom/squareup/reports/applet/sales/v1/ReportConfig;-><init>(Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;ZZZZLjava/util/List;)V

    return-object v3
.end method

.method public defaultReport()Lcom/squareup/reports/applet/sales/v1/ReportConfig;
    .locals 11

    .line 49
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 50
    invoke-direct {p0, v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->updateToMidnight(Ljava/util/Calendar;)V

    .line 51
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    .line 52
    invoke-direct {p0, v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->updateToDayEnd(Ljava/util/Calendar;)V

    .line 53
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    .line 54
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {v1, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 55
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v5

    .line 56
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    .line 57
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v10

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x1

    move-object v1, v0

    invoke-direct/range {v1 .. v10}, Lcom/squareup/reports/applet/sales/v1/ReportConfig;-><init>(Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;ZZZZLjava/util/List;)V

    return-object v0
.end method

.method public setAllDayChecked(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Z)Lcom/squareup/reports/applet/sales/v1/ReportConfig;
    .locals 12

    .line 132
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 134
    iget-object v1, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    if-eqz p2, :cond_0

    .line 136
    invoke-direct {p0, v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->updateToMidnight(Ljava/util/Calendar;)V

    goto :goto_0

    .line 138
    :cond_0
    invoke-direct {p0, v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->updateToMidnight(Ljava/util/Calendar;)V

    .line 140
    :goto_0
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    .line 143
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 144
    iget-object v2, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    if-eqz p2, :cond_1

    .line 146
    invoke-direct {p0, v1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->updateToDayEnd(Ljava/util/Calendar;)V

    goto :goto_1

    :cond_1
    const/4 v2, 0x1

    .line 148
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v6

    const/4 v2, 0x2

    .line 149
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v7

    const/4 v2, 0x5

    .line 150
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v8

    .line 151
    invoke-virtual {v1}, Ljava/util/Calendar;->clear()V

    const/16 v9, 0x13

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v5, v1

    .line 152
    invoke-virtual/range {v5 .. v11}, Ljava/util/Calendar;->set(IIIIII)V

    .line 154
    :goto_1
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    .line 156
    invoke-static {v0, v1}, Lcom/squareup/util/Times;->sameDate(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v0

    invoke-direct {p0, p2, v0, v4, v5}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->updateFormattedDateRange(ZZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 158
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/reports/applet/sales/v1/log/ToggleAllDaySettingEvent;

    invoke-direct {v1, p2}, Lcom/squareup/reports/applet/sales/v1/log/ToggleAllDaySettingEvent;-><init>(Z)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 159
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    iget-object v6, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->timeZone:Ljava/lang/String;

    const/4 v8, 0x0

    iget-boolean v9, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->itemDetails:Z

    iget-boolean v10, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->filterByThisDevice:Z

    iget-object v11, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->selectedEmployees:Ljava/util/List;

    move-object v2, v0

    move v7, p2

    invoke-direct/range {v2 .. v11}, Lcom/squareup/reports/applet/sales/v1/ReportConfig;-><init>(Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;ZZZZLjava/util/List;)V

    return-object v0
.end method

.method public setDeviceFilterChecked(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Z)Lcom/squareup/reports/applet/sales/v1/ReportConfig;
    .locals 11

    .line 171
    new-instance v10, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    iget-object v1, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->formattedDateRange:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    iget-object v3, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    iget-object v4, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->timeZone:Ljava/lang/String;

    iget-boolean v5, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->fullDays:Z

    iget-boolean v7, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->itemDetails:Z

    iget-object v9, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->selectedEmployees:Ljava/util/List;

    const/4 v6, 0x0

    move-object v0, v10

    move v8, p2

    invoke-direct/range {v0 .. v9}, Lcom/squareup/reports/applet/sales/v1/ReportConfig;-><init>(Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;ZZZZLjava/util/List;)V

    return-object v10
.end method

.method public setEmployeeFilter(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Ljava/util/List;)Lcom/squareup/reports/applet/sales/v1/ReportConfig;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;"
        }
    .end annotation

    .line 177
    new-instance v10, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    iget-object v1, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->formattedDateRange:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    iget-object v3, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    iget-object v4, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->timeZone:Ljava/lang/String;

    iget-boolean v5, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->fullDays:Z

    iget-boolean v7, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->itemDetails:Z

    iget-boolean v8, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->filterByThisDevice:Z

    const/4 v6, 0x0

    move-object v0, v10

    move-object v9, p2

    invoke-direct/range {v0 .. v9}, Lcom/squareup/reports/applet/sales/v1/ReportConfig;-><init>(Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;ZZZZLjava/util/List;)V

    return-object v10
.end method

.method public setItemDetailsChecked(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Z)Lcom/squareup/reports/applet/sales/v1/ReportConfig;
    .locals 12

    .line 164
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/reports/applet/sales/v1/log/ToggleItemDetailsSettingEvent;

    invoke-direct {v1, p2}, Lcom/squareup/reports/applet/sales/v1/log/ToggleItemDetailsSettingEvent;-><init>(Z)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 165
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    iget-object v3, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->formattedDateRange:Ljava/lang/String;

    iget-object v4, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    iget-object v5, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    iget-object v6, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->timeZone:Ljava/lang/String;

    iget-boolean v7, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->fullDays:Z

    iget-boolean v10, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->filterByThisDevice:Z

    iget-object v11, p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->selectedEmployees:Ljava/util/List;

    const/4 v8, 0x0

    move-object v2, v0

    move v9, p2

    invoke-direct/range {v2 .. v11}, Lcom/squareup/reports/applet/sales/v1/ReportConfig;-><init>(Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;ZZZZLjava/util/List;)V

    return-object v0
.end method

.method public singleDay(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Ljava/util/Calendar;)Lcom/squareup/reports/applet/sales/v1/ReportConfig;
    .locals 24

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    const/4 v2, 0x1

    .line 199
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v9

    const/4 v3, 0x2

    .line 200
    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v10

    const/4 v3, 0x5

    .line 201
    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 203
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v11

    .line 205
    iget-object v3, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    invoke-virtual {v11, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/16 v3, 0xb

    .line 206
    invoke-virtual {v11, v3}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/16 v5, 0xc

    .line 207
    invoke-virtual {v11, v5}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 208
    iget-object v7, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    invoke-virtual {v11, v7}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 209
    invoke-virtual {v11, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 210
    invoke-virtual {v11, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    if-gt v4, v3, :cond_1

    if-ne v4, v3, :cond_0

    if-le v6, v5, :cond_0

    goto :goto_0

    :cond_0
    move v12, v3

    move v7, v4

    move v13, v5

    move v8, v6

    goto :goto_1

    :cond_1
    :goto_0
    move v7, v3

    move v12, v4

    move v8, v5

    move v13, v6

    .line 221
    :goto_1
    invoke-virtual {v11}, Ljava/util/Calendar;->clear()V

    move-object v3, v11

    move v4, v9

    move v5, v10

    move v6, v1

    .line 222
    invoke-virtual/range {v3 .. v8}, Ljava/util/Calendar;->set(IIIII)V

    .line 223
    invoke-virtual {v11}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v15

    .line 225
    invoke-virtual {v11}, Ljava/util/Calendar;->clear()V

    move v7, v12

    move v8, v13

    .line 226
    invoke-virtual/range {v3 .. v8}, Ljava/util/Calendar;->set(IIIII)V

    .line 227
    invoke-virtual {v11}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    .line 228
    iget-boolean v3, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->fullDays:Z

    move-object/from16 v4, p0

    invoke-direct {v4, v3, v2, v15, v1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->updateFormattedDateRange(ZZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 230
    new-instance v3, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    iget-object v5, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->timeZone:Ljava/lang/String;

    iget-boolean v6, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->fullDays:Z

    const/16 v20, 0x0

    iget-boolean v7, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->itemDetails:Z

    iget-boolean v8, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->filterByThisDevice:Z

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->selectedEmployees:Ljava/util/List;

    move-object v14, v3

    move-object v9, v15

    move-object v15, v2

    move-object/from16 v16, v9

    move-object/from16 v17, v1

    move-object/from16 v18, v5

    move/from16 v19, v6

    move/from16 v21, v7

    move/from16 v22, v8

    move-object/from16 v23, v0

    invoke-direct/range {v14 .. v23}, Lcom/squareup/reports/applet/sales/v1/ReportConfig;-><init>(Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;ZZZZLjava/util/List;)V

    return-object v3
.end method
