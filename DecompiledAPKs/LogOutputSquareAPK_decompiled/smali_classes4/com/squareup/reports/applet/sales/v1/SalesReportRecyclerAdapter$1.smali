.class synthetic Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$1;
.super Ljava/lang/Object;
.source "SalesReportRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$protos$beemo$api$v3$reporting$GroupByType:[I

.field static final synthetic $SwitchMap$com$squareup$reports$applet$sales$v1$CategorySalesRow$CategorySalesRowType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 494
    invoke-static {}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->values()[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$1;->$SwitchMap$com$squareup$protos$beemo$api$v3$reporting$GroupByType:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$1;->$SwitchMap$com$squareup$protos$beemo$api$v3$reporting$GroupByType:[I

    sget-object v2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->HOUR_OF_DAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$1;->$SwitchMap$com$squareup$protos$beemo$api$v3$reporting$GroupByType:[I

    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$1;->$SwitchMap$com$squareup$protos$beemo$api$v3$reporting$GroupByType:[I

    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->MONTH:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    .line 310
    :catch_2
    invoke-static {}, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->values()[Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$1;->$SwitchMap$com$squareup$reports$applet$sales$v1$CategorySalesRow$CategorySalesRowType:[I

    :try_start_3
    sget-object v3, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$1;->$SwitchMap$com$squareup$reports$applet$sales$v1$CategorySalesRow$CategorySalesRowType:[I

    sget-object v4, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->CATEGORY:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    invoke-virtual {v4}, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->ordinal()I

    move-result v4

    aput v0, v3, v4
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$1;->$SwitchMap$com$squareup$reports$applet$sales$v1$CategorySalesRow$CategorySalesRowType:[I

    sget-object v3, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->ITEM:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    invoke-virtual {v3}, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->ordinal()I

    move-result v3

    aput v1, v0, v3
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$1;->$SwitchMap$com$squareup$reports$applet$sales$v1$CategorySalesRow$CategorySalesRowType:[I

    sget-object v1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->ITEM_VARIATION:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    invoke-virtual {v1}, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    return-void
.end method
