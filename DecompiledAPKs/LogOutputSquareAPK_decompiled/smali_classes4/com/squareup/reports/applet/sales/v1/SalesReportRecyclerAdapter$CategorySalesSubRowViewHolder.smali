.class Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;
.super Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;
.source "SalesReportRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CategorySalesSubRowViewHolder"
.end annotation


# instance fields
.field private final nameView:Landroid/widget/TextView;

.field private final priceView:Landroid/widget/TextView;

.field private final quantityView:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

.field private final threeColumns:Z


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V
    .locals 1

    .line 627
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    .line 628
    invoke-direct {p0, p2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 629
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/reports/applet/R$id;->sales_category_subrow_name:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;->nameView:Landroid/widget/TextView;

    .line 630
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/reports/applet/R$id;->sales_category_subrow_quantity:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;->quantityView:Landroid/widget/TextView;

    .line 631
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/reports/applet/R$id;->sales_category_subrow_price:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;->priceView:Landroid/widget/TextView;

    .line 632
    instance-of p1, p2, Lcom/squareup/widgets/HorizontalThreeChildLayout;

    iput-boolean p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;->threeColumns:Z

    return-void
.end method


# virtual methods
.method public bind()V
    .locals 4

    .line 636
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    .line 637
    invoke-static {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$000(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;->getPosition()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-static {v2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$1300(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getCategorySalesRow(I)Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;

    move-result-object v0

    .line 638
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;->nameView:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->formattedName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 639
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;->quantityView:Landroid/widget/TextView;

    iget-boolean v3, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;->threeColumns:Z

    invoke-virtual {v1, v2, v0, v3}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->setQuantityViewText(Landroid/widget/TextView;Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;Z)V

    .line 640
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CategorySalesSubRowViewHolder;->priceView:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;->formattedMoney:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
