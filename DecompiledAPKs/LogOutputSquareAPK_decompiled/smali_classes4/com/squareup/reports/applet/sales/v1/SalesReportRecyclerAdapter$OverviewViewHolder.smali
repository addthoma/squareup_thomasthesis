.class Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;
.super Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;
.source "SalesReportRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OverviewViewHolder"
.end annotation


# instance fields
.field private final salesReportOverviewAverageSales:Landroid/widget/TextView;

.field private final salesReportOverviewCoverCount:Landroid/widget/TextView;

.field private final salesReportOverviewCoverCountHolder:Landroid/view/View;

.field private final salesReportOverviewCoverCountSubtitle:Landroid/widget/TextView;

.field private final salesReportOverviewGrossSales:Landroid/widget/TextView;

.field private final salesReportOverviewSalesCount:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V
    .locals 0

    .line 379
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    .line 380
    invoke-direct {p0, p2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 381
    sget p1, Lcom/squareup/reports/applet/R$id;->sales_report_overview_gross_sales:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;->salesReportOverviewGrossSales:Landroid/widget/TextView;

    .line 382
    sget p1, Lcom/squareup/reports/applet/R$id;->sales_report_overview_sales_count:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;->salesReportOverviewSalesCount:Landroid/widget/TextView;

    .line 383
    sget p1, Lcom/squareup/reports/applet/R$id;->sales_report_overview_average_sales:I

    .line 384
    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;->salesReportOverviewAverageSales:Landroid/widget/TextView;

    .line 385
    sget p1, Lcom/squareup/reports/applet/R$id;->sales_report_overview_cover_count_holder:I

    .line 386
    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;->salesReportOverviewCoverCountHolder:Landroid/view/View;

    .line 387
    sget p1, Lcom/squareup/reports/applet/R$id;->sales_report_overview_cover_count:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;->salesReportOverviewCoverCount:Landroid/widget/TextView;

    .line 388
    sget p1, Lcom/squareup/reports/applet/R$id;->sales_report_overview_cover_count_subtext:I

    .line 389
    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;->salesReportOverviewCoverCountSubtitle:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public bind()V
    .locals 5

    .line 393
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-static {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$000(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getOverviewAmounts()Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter$OverviewAmounts;

    move-result-object v0

    .line 394
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;->salesReportOverviewGrossSales:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter$OverviewAmounts;->formattedGrossSalesMoney:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 395
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;->salesReportOverviewSalesCount:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter$OverviewAmounts;->formattedSalesCount:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 396
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;->salesReportOverviewAverageSales:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter$OverviewAmounts;->formattedAverageSalesMoney:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 397
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;->salesReportOverviewCoverCountHolder:Landroid/view/View;

    iget-object v2, v0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter$OverviewAmounts;->formattedCoverCount:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 399
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;->salesReportOverviewCoverCount:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter$OverviewAmounts;->formattedCoverCount:Ljava/lang/String;

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-static {v1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 401
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;->salesReportOverviewCoverCountSubtitle:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter$OverviewAmounts;->formattedCoverCount:Ljava/lang/String;

    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    :goto_2
    invoke-static {v1, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 403
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$OverviewViewHolder;->salesReportOverviewCoverCount:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter$OverviewAmounts;->formattedCoverCount:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
