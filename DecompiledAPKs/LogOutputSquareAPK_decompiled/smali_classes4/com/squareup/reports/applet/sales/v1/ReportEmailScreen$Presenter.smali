.class Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;
.super Lcom/squareup/reports/applet/BaseEmailCardPresenter;
.source "ReportEmailScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation


# static fields
.field private static final EMAIL_SENT:Ljava/lang/String; = "report_email_sent"


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private emailSent:Z

.field private final flow:Lflow/Flow;

.field private final res:Lcom/squareup/util/Res;

.field private final retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private final salesSummaryEmailSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/analytics/Analytics;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 0
    .param p4    # Lcom/squareup/settings/LocalSetting;
        .annotation runtime Lcom/squareup/settings/SalesSummaryEmail;
        .end annotation
    .end param
    .param p8    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p9    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/analytics/Analytics;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 66
    invoke-direct {p0, p8, p9}, Lcom/squareup/reports/applet/BaseEmailCardPresenter;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    const/4 p8, 0x0

    .line 59
    iput-boolean p8, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->emailSent:Z

    .line 67
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->flow:Lflow/Flow;

    .line 68
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 69
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 70
    iput-object p5, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 71
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->salesSummaryEmailSetting:Lcom/squareup/settings/LocalSetting;

    .line 72
    iput-object p6, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 73
    iput-object p7, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private goBack()V
    .locals 4

    .line 133
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public getActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 4

    .line 87
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/reports/applet/R$string;->sales_report_email_report:I

    .line 88
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$dpxaDOc3uDnVDJrVL434TyqBOWU;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$dpxaDOc3uDnVDJrVL434TyqBOWU;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;)V

    .line 89
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultEmailRecipient()Ljava/lang/String;
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getEmail()Ljava/lang/String;

    move-result-object v0

    .line 129
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->salesSummaryEmailSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v1, v0}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public onBackPressed()Z
    .locals 1

    .line 98
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->goBack()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 77
    invoke-super {p0, p1}, Lcom/squareup/reports/applet/BaseEmailCardPresenter;->onLoad(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "report_email_sent"

    .line 79
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->emailSent:Z

    .line 80
    iget-boolean p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->emailSent:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 81
    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->showEmailMessage(Z)V

    :cond_0
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 94
    iget-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->emailSent:Z

    const-string v1, "report_email_sent"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public sendEmail(Ljava/lang/String;)V
    .locals 5

    const/4 v0, 0x1

    .line 103
    iput-boolean v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->emailSent:Z

    .line 104
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->getDefaultEmailRecipient()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v0

    .line 105
    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 106
    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/UserSettings;->getEmail()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 107
    iget-object v3, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v4, Lcom/squareup/reports/applet/sales/v1/SalesReportEmailSentEvent;

    invoke-direct {v4, v2, v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportEmailSentEvent;-><init>(ZZ)V

    invoke-interface {v3, v4}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 109
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/reports/applet/BaseEmailCardView;

    invoke-virtual {v1}, Lcom/squareup/reports/applet/BaseEmailCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v1

    check-cast v1, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen;

    .line 110
    invoke-static {v1}, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen;->access$000(Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen;)Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    move-result-object v1

    .line 111
    iget-object v2, v1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->startTime:Ljava/util/Date;

    invoke-static {v2}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 112
    iget-object v1, v1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;->endTime:Ljava/util/Date;

    invoke-static {v1}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 113
    iget-object v3, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->salesSummaryEmailSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v3, p1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 114
    iget-object v3, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v4, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;

    invoke-direct {v4, v2, v1, p1, v0}, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v3, v4}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    .line 115
    invoke-virtual {p0, v0}, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->showEmailMessage(Z)V

    return-void
.end method

.method showEmailMessage(Z)V
    .locals 7

    .line 119
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/reports/applet/BaseEmailCardView;

    sget v2, Lcom/squareup/billhistoryui/R$string;->no_internet_connection:I

    sget v3, Lcom/squareup/reports/applet/R$string;->sales_report_email_offline:I

    const/4 v4, 0x1

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/reports/applet/BaseEmailCardView;->showEmailMessage(IIZLcom/squareup/glyph/GlyphTypeface$Glyph;Z)V

    goto :goto_0

    .line 123
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/BaseEmailCardView;

    sget v1, Lcom/squareup/reports/applet/R$string;->sales_report_email_sent:I

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/reports/applet/BaseEmailCardView;->showEmailMessage(ILcom/squareup/glyph/GlyphTypeface$Glyph;Z)V

    :goto_0
    return-void
.end method
