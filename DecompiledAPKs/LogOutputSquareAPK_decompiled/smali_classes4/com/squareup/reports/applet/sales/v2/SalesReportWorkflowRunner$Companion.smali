.class public final Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner$Companion;
.super Ljava/lang/Object;
.source "SalesReportWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner$Companion;",
        "",
        "()V",
        "NAME",
        "",
        "getNAME",
        "()Ljava/lang/String;",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner$Companion;

.field private static final NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 10
    new-instance v0, Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner$Companion;

    invoke-direct {v0}, Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner$Companion;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner$Companion;->$$INSTANCE:Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner$Companion;

    .line 11
    const-class v0, Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SalesReportWorkflowRunner::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner$Companion;->NAME:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getNAME()Ljava/lang/String;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner$Companion;->NAME:Ljava/lang/String;

    return-object v0
.end method
