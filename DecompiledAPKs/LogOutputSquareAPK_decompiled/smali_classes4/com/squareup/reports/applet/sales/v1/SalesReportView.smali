.class public Lcom/squareup/reports/applet/sales/v1/SalesReportView;
.super Landroid/widget/LinearLayout;
.source "SalesReportView.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private adapter:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

.field chartDateFormatter:Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private salesReport:Lcom/squareup/widgets/SquareViewAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    const-class p2, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Component;->inject(Lcom/squareup/reports/applet/sales/v1/SalesReportView;)V

    return-void
.end method


# virtual methods
.method public categoryRangeInserted(II)V
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->adapter:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->categoryRangeInserted(II)V

    return-void
.end method

.method public categoryRangeRemoved(II)V
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->adapter:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->categoryRangeRemoved(II)V

    return-void
.end method

.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 78
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 7

    .line 42
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 44
    sget v0, Lcom/squareup/reports/applet/R$id;->sales_report_container:I

    invoke-virtual {p0, v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->salesReport:Lcom/squareup/widgets/SquareViewAnimator;

    .line 45
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 46
    new-instance v4, Lcom/squareup/util/Res$RealRes;

    invoke-direct {v4, v0}, Lcom/squareup/util/Res$RealRes;-><init>(Landroid/content/res/Resources;)V

    .line 47
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    iget-object v3, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->chartDateFormatter:Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v5

    iget-object v6, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;Lcom/squareup/reports/applet/sales/v1/ChartDateFormatter;Lcom/squareup/util/Res;ZLcom/squareup/quantity/PerUnitFormatter;)V

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->adapter:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    .line 49
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 50
    sget v0, Lcom/squareup/reports/applet/R$id;->sales_report_recycler_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 51
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    .line 52
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    .line 53
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->adapter:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setOverScrollMode(I)V

    .line 57
    sget v0, Lcom/squareup/reports/applet/R$id;->sales_report_error_state:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 58
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->clearGlyph()V

    .line 59
    new-instance v1, Lcom/squareup/reports/applet/sales/v1/SalesReportView$1;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportView$1;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    sget v0, Lcom/squareup/reports/applet/R$id;->sales_report_no_transactions_for_time_period:I

    .line 66
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 67
    new-instance v1, Lcom/squareup/reports/applet/sales/v1/SalesReportView$2;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportView$2;-><init>(Lcom/squareup/reports/applet/sales/v1/SalesReportView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->presenter:Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public showErrorState()V
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->salesReport:Lcom/squareup/widgets/SquareViewAnimator;

    sget v1, Lcom/squareup/reports/applet/R$id;->sales_report_error_state:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method public showLoadingState()V
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->salesReport:Lcom/squareup/widgets/SquareViewAnimator;

    sget v1, Lcom/squareup/reports/applet/R$id;->sales_report_progress_bar:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method public showNoTransactions()V
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->salesReport:Lcom/squareup/widgets/SquareViewAnimator;

    sget v1, Lcom/squareup/reports/applet/R$id;->sales_report_no_transactions_for_time_period:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method public showReportSection()V
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->salesReport:Lcom/squareup/widgets/SquareViewAnimator;

    sget v1, Lcom/squareup/reports/applet/R$id;->sales_report_recycler_view:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method public update()V
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportView;->adapter:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->update()V

    return-void
.end method
