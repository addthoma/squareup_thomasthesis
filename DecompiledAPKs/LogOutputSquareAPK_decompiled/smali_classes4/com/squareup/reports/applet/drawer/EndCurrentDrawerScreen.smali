.class public final Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen;
.super Lcom/squareup/reports/applet/InReportsAppletScope;
.source "EndCurrentDrawerScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Component;,
        Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen;

    invoke-direct {v0}, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen;->INSTANCE:Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen;

    .line 137
    sget-object v0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen;->INSTANCE:Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/squareup/reports/applet/InReportsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 140
    sget v0, Lcom/squareup/reports/applet/R$layout;->end_current_drawer_card_view:I

    return v0
.end method
