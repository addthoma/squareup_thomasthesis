.class public final Lcom/squareup/reports/applet/drawer/DrawerHistoryView_MembersInjector;
.super Ljava/lang/Object;
.source "DrawerHistoryView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/reports/applet/drawer/DrawerHistoryView;",
        ">;"
    }
.end annotation


# instance fields
.field private final appNameFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView_MembersInjector;->appNameFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/reports/applet/drawer/DrawerHistoryView;",
            ">;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectAppNameFormatter(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;Lcom/squareup/util/AppNameFormatter;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;Ljava/lang/Object;)V
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;->presenter:Lcom/squareup/reports/applet/drawer/DrawerHistoryScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;)V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView_MembersInjector;->injectPresenter(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;Ljava/lang/Object;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/DrawerHistoryView_MembersInjector;->appNameFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/AppNameFormatter;

    invoke-static {p1, v0}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView_MembersInjector;->injectAppNameFormatter(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;Lcom/squareup/util/AppNameFormatter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/reports/applet/drawer/DrawerHistoryView;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/drawer/DrawerHistoryView_MembersInjector;->injectMembers(Lcom/squareup/reports/applet/drawer/DrawerHistoryView;)V

    return-void
.end method
