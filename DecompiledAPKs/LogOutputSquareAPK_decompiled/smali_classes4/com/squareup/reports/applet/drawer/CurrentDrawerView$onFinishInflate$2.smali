.class public final Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "CurrentDrawerView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$2",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "v",
        "Landroid/view/View;",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/reports/applet/drawer/CurrentDrawerView;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 80
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$2;->this$0:Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    const-string v0, "v"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$2;->this$0:Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->getPresenter$reports_applet_release()Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$2;->this$0:Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    invoke-static {v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->access$getShiftDescription$p(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->saveShiftDescription(Ljava/lang/String;)V

    .line 83
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$2;->this$0:Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 84
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView$onFinishInflate$2;->this$0:Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->getPresenter$reports_applet_release()Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->endDrawerClicked()V

    return-void
.end method
