.class public final synthetic Lcom/squareup/reports/applet/drawer/cashmanagementsettings/-$$Lambda$vJ3JwfGLGDMwiv3ikKp6svsw7qs;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/-$$Lambda$vJ3JwfGLGDMwiv3ikKp6svsw7qs;->f$0:Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/-$$Lambda$vJ3JwfGLGDMwiv3ikKp6svsw7qs;->f$0:Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardView;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/settings/cashmanagement/CashManagementSettingsBaseView;->setCashManagementEnabled(Z)V

    return-void
.end method
