.class Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "CashDrawerDetailsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView$1;->this$0:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .line 58
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView$1;->this$0:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    invoke-static {p1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->access$000(Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;)Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 59
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView$1;->this$0:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;

    invoke-static {p1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;->access$000(Lcom/squareup/reports/applet/drawer/CashDrawerDetailsView;)Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->updateDifference()V

    :cond_0
    return-void
.end method
