.class public final Lcom/squareup/reports/applet/ReportsAppletClientActionTranslator;
.super Ljava/lang/Object;
.source "ReportsAppletClientActionTranslator.kt"

# interfaces
.implements Lcom/squareup/clientactiontranslation/ClientActionTranslator;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0001\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/reports/applet/ReportsAppletClientActionTranslator;",
        "Lcom/squareup/clientactiontranslation/ClientActionTranslator;",
        "()V",
        "translate",
        "Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;",
        "clientAction",
        "Lcom/squareup/protos/client/ClientAction;",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public translate(Lcom/squareup/protos/client/ClientAction;)Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;
    .locals 3

    const-string v0, "clientAction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;

    const-string v0, "square-register://reports"

    invoke-direct {p1, v0}, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    goto :goto_0

    .line 13
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    if-eqz v0, :cond_1

    new-instance p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;

    const-string v0, "square-register://reports/navigate?navigationID=sales"

    invoke-direct {p1, v0}, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    goto :goto_0

    .line 16
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    const-string v1, "square-register://reports/navigate?navigationID=disputes"

    if-eqz v0, :cond_2

    new-instance p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;

    invoke-direct {p1, v1}, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    goto :goto_0

    .line 19
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;

    .line 20
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&paymentToken="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    iget-object p1, p1, Lcom/squareup/protos/client/ClientAction;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    iget-object p1, p1, Lcom/squareup/protos/client/ClientAction$ViewDispute;->dispute_payment_token:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 19
    invoke-direct {v0, p1}, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;-><init>(Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    goto :goto_0

    .line 23
    :cond_3
    sget-object p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Unhandled;->INSTANCE:Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Unhandled;

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    :goto_0
    return-object p1
.end method
