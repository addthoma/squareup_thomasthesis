.class public final Lcom/squareup/reports/applet/ReportsApplet;
.super Lcom/squareup/applet/HistoryFactoryApplet;
.source "ReportsApplet.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/ReportsApplet$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a2\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u0007\u0018\u0000 :2\u00020\u0001:\u0001:BU\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0002\u0010\u0015J\u0006\u0010\u001e\u001a\u00020\u001fJ\u0006\u0010 \u001a\u00020\u001fJ\u000e\u0010!\u001a\u0008\u0012\u0004\u0012\u00020#0\"H\u0016J\u001a\u0010$\u001a\u0004\u0018\u00010\n2\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020&H\u0016J\u0018\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020&2\u0006\u0010+\u001a\u00020,H\u0002J\u0018\u0010-\u001a\u0008\u0012\u0004\u0012\u00020&0.2\u0008\u0010/\u001a\u0004\u0018\u000100H\u0014J\u0008\u00101\u001a\u00020\u0017H\u0016J\u0010\u00102\u001a\n\u0012\u0004\u0012\u000204\u0018\u000103H\u0016J\u0010\u00105\u001a\u00020\u00172\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0010\u00106\u001a\u00020\u001f2\u0006\u00107\u001a\u00020)H\u0002J\u000e\u00108\u001a\u0008\u0012\u0004\u0012\u0002090\"H\u0016R\u0014\u0010\u0016\u001a\u00020\u0017X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001a\u001a\u00020\u001b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001c\u0010\u001dR\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006;"
    }
    d2 = {
        "Lcom/squareup/reports/applet/ReportsApplet;",
        "Lcom/squareup/applet/HistoryFactoryApplet;",
        "container",
        "Ldagger/Lazy;",
        "Lcom/squareup/ui/main/PosContainer;",
        "device",
        "Lcom/squareup/util/Device;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "reportsAppletEntryPoint",
        "Lcom/squareup/reports/applet/ReportsAppletEntryPoint;",
        "handlesDisputes",
        "Lcom/squareup/disputes/api/HandlesDisputes;",
        "disputesSection",
        "Lcom/squareup/reports/applet/disputes/DisputesSection;",
        "currentDrawerSection",
        "Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;",
        "resources",
        "Landroid/content/res/Resources;",
        "reportsAppletHeader",
        "Lcom/squareup/reports/applet/ReportsAppletHeader;",
        "(Ldagger/Lazy;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/reports/applet/ReportsAppletEntryPoint;Lcom/squareup/disputes/api/HandlesDisputes;Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;Landroid/content/res/Resources;Lcom/squareup/reports/applet/ReportsAppletHeader;)V",
        "analyticsName",
        "",
        "getAnalyticsName",
        "()Ljava/lang/String;",
        "initialDetailScreen",
        "Lflow/path/Path;",
        "getInitialDetailScreen",
        "()Lflow/path/Path;",
        "activateCurrentDrawerReport",
        "",
        "activateDisputesReport",
        "badge",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/applet/Applet$Badge;",
        "getEntryPoint",
        "origin",
        "Lcom/squareup/container/ContainerTreeKey;",
        "target",
        "getHistoryFactory",
        "Lcom/squareup/ui/main/HistoryFactory;",
        "screen",
        "section",
        "Lcom/squareup/applet/AppletSection;",
        "getHomeScreens",
        "",
        "currentHistory",
        "Lflow/History;",
        "getIntentScreenExtra",
        "getPermissions",
        "",
        "Lcom/squareup/permissions/Permission;",
        "getText",
        "resetHistory",
        "historyFactory",
        "visibility",
        "",
        "Companion",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/reports/applet/ReportsApplet$Companion;

.field public static final INTENT_SCREEN_EXTRA:Ljava/lang/String; = "REPORTS"


# instance fields
.field private final analyticsName:Ljava/lang/String;

.field private final currentDrawerSection:Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;

.field private final device:Lcom/squareup/util/Device;

.field private final disputesSection:Lcom/squareup/reports/applet/disputes/DisputesSection;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

.field private final reportsAppletEntryPoint:Lcom/squareup/reports/applet/ReportsAppletEntryPoint;

.field private final reportsAppletHeader:Lcom/squareup/reports/applet/ReportsAppletHeader;

.field private final resources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/reports/applet/ReportsApplet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/reports/applet/ReportsApplet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/reports/applet/ReportsApplet;->Companion:Lcom/squareup/reports/applet/ReportsApplet$Companion;

    return-void
.end method

.method public constructor <init>(Ldagger/Lazy;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/reports/applet/ReportsAppletEntryPoint;Lcom/squareup/disputes/api/HandlesDisputes;Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;Landroid/content/res/Resources;Lcom/squareup/reports/applet/ReportsAppletHeader;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/reports/applet/ReportsAppletEntryPoint;",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            "Lcom/squareup/reports/applet/disputes/DisputesSection;",
            "Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;",
            "Landroid/content/res/Resources;",
            "Lcom/squareup/reports/applet/ReportsAppletHeader;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reportsAppletEntryPoint"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handlesDisputes"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "disputesSection"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentDrawerSection"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reportsAppletHeader"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/applet/HistoryFactoryApplet;-><init>(Ldagger/Lazy;)V

    iput-object p2, p0, Lcom/squareup/reports/applet/ReportsApplet;->device:Lcom/squareup/util/Device;

    iput-object p3, p0, Lcom/squareup/reports/applet/ReportsApplet;->features:Lcom/squareup/settings/server/Features;

    iput-object p4, p0, Lcom/squareup/reports/applet/ReportsApplet;->reportsAppletEntryPoint:Lcom/squareup/reports/applet/ReportsAppletEntryPoint;

    iput-object p5, p0, Lcom/squareup/reports/applet/ReportsApplet;->handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

    iput-object p6, p0, Lcom/squareup/reports/applet/ReportsApplet;->disputesSection:Lcom/squareup/reports/applet/disputes/DisputesSection;

    iput-object p7, p0, Lcom/squareup/reports/applet/ReportsApplet;->currentDrawerSection:Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;

    iput-object p8, p0, Lcom/squareup/reports/applet/ReportsApplet;->resources:Landroid/content/res/Resources;

    iput-object p9, p0, Lcom/squareup/reports/applet/ReportsApplet;->reportsAppletHeader:Lcom/squareup/reports/applet/ReportsAppletHeader;

    const-string p1, "reports"

    .line 49
    iput-object p1, p0, Lcom/squareup/reports/applet/ReportsApplet;->analyticsName:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$getResources$p(Lcom/squareup/reports/applet/ReportsApplet;)Landroid/content/res/Resources;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/reports/applet/ReportsApplet;->resources:Landroid/content/res/Resources;

    return-object p0
.end method

.method private final getHistoryFactory(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/applet/AppletSection;)Lcom/squareup/ui/main/HistoryFactory;
    .locals 1

    .line 112
    new-instance v0, Lcom/squareup/reports/applet/ReportsApplet$getHistoryFactory$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/reports/applet/ReportsApplet$getHistoryFactory$1;-><init>(Lcom/squareup/reports/applet/ReportsApplet;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/applet/AppletSection;)V

    check-cast v0, Lcom/squareup/ui/main/HistoryFactory;

    return-object v0
.end method

.method private final resetHistory(Lcom/squareup/ui/main/HistoryFactory;)V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet;->container:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    .line 105
    sget-object v1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-interface {v0, p1, v1}, Lcom/squareup/ui/main/PosContainer;->resetHistory(Lcom/squareup/ui/main/HistoryFactory;Lflow/Direction;)V

    return-void
.end method


# virtual methods
.method public final activateCurrentDrawerReport()V
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet;->currentDrawerSection:Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    const-string v1, "currentDrawerSection.initialScreen"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    iget-object v1, p0, Lcom/squareup/reports/applet/ReportsApplet;->currentDrawerSection:Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;

    check-cast v1, Lcom/squareup/applet/AppletSection;

    invoke-direct {p0, v0, v1}, Lcom/squareup/reports/applet/ReportsApplet;->getHistoryFactory(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/applet/AppletSection;)Lcom/squareup/ui/main/HistoryFactory;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/reports/applet/ReportsApplet;->resetHistory(Lcom/squareup/ui/main/HistoryFactory;)V

    return-void
.end method

.method public final activateDisputesReport()V
    .locals 3

    .line 96
    new-instance v0, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v1}, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    iget-object v1, p0, Lcom/squareup/reports/applet/ReportsApplet;->disputesSection:Lcom/squareup/reports/applet/disputes/DisputesSection;

    check-cast v1, Lcom/squareup/applet/AppletSection;

    invoke-direct {p0, v0, v1}, Lcom/squareup/reports/applet/ReportsApplet;->getHistoryFactory(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/applet/AppletSection;)Lcom/squareup/ui/main/HistoryFactory;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/reports/applet/ReportsApplet;->resetHistory(Lcom/squareup/ui/main/HistoryFactory;)V

    return-void
.end method

.method public badge()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/applet/Applet$Badge;",
            ">;"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet;->handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

    invoke-interface {v0}, Lcom/squareup/disputes/api/HandlesDisputes;->disputeNotification()Lio/reactivex/Observable;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 67
    new-instance v1, Lcom/squareup/reports/applet/ReportsApplet$badge$1;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/ReportsApplet$badge$1;-><init>(Lcom/squareup/reports/applet/ReportsApplet;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "handlesDisputes.disputeN\u2026HIGH) else Hidden\n      }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getAnalyticsName()Ljava/lang/String;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet;->analyticsName:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getEntryPoint(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/applet/AppletEntryPoint;
    .locals 0

    .line 37
    invoke-virtual {p0, p1, p2}, Lcom/squareup/reports/applet/ReportsApplet;->getEntryPoint(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/reports/applet/ReportsAppletEntryPoint;

    move-result-object p1

    check-cast p1, Lcom/squareup/applet/AppletEntryPoint;

    return-object p1
.end method

.method public getEntryPoint(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/reports/applet/ReportsAppletEntryPoint;
    .locals 1

    const-string v0, "origin"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "target"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    const-class v0, Lcom/squareup/reports/applet/ReportsAppletScope;

    invoke-static {p1, p2, v0}, Lcom/squareup/container/ContainerTreeKey;->isEnteringScope(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 76
    iget-object p1, p0, Lcom/squareup/reports/applet/ReportsApplet;->reportsAppletEntryPoint:Lcom/squareup/reports/applet/ReportsAppletEntryPoint;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method protected getHomeScreens(Lflow/History;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/History;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    .line 57
    sget-object p1, Lcom/squareup/reports/applet/ReportsMasterScreen;->INSTANCE:Lcom/squareup/reports/applet/ReportsMasterScreen;

    const-string v0, "ReportsMasterScreen.INSTANCE"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getInitialDetailScreen()Lflow/path/Path;
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet;->reportsAppletEntryPoint:Lcom/squareup/reports/applet/ReportsAppletEntryPoint;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/ReportsAppletEntryPoint;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    const-string v1, "reportsAppletEntryPoint.initialScreen"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lflow/path/Path;

    return-object v0
.end method

.method public getIntentScreenExtra()Ljava/lang/String;
    .locals 1

    const-string v0, "REPORTS"

    return-object v0
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_REPORTS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/permissions/Permission;

    const/4 v1, 0x0

    .line 87
    sget-object v2, Lcom/squareup/permissions/Permission;->VIEW_SALES_REPORTS:Lcom/squareup/permissions/Permission;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/squareup/permissions/Permission;->VIEW_CASH_DRAWER_REPORTS:Lcom/squareup/permissions/Permission;

    aput-object v2, v0, v1

    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lcom/squareup/reports/applet/ReportsApplet;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_VIEW_DETAILED_SALES_REPORTS_PERMISSION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 89
    sget-object v1, Lcom/squareup/permissions/Permission;->DETAILED_SALES_REPORTS:Lcom/squareup/permissions/Permission;

    invoke-static {v0, v1}, Lkotlin/collections/SetsKt;->plus(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method public getText(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet;->reportsAppletHeader:Lcom/squareup/reports/applet/ReportsAppletHeader;

    invoke-interface {v0}, Lcom/squareup/reports/applet/ReportsAppletHeader;->getHeaderId()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(reportsAppletHeader.headerId)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public visibility()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REPORTS_APPLET_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "just(device.isTablet || \u2026d(REPORTS_APPLET_MOBILE))"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
