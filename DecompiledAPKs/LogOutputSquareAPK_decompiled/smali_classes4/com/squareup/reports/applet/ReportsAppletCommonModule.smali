.class public abstract Lcom/squareup/reports/applet/ReportsAppletCommonModule;
.super Ljava/lang/Object;
.source "ReportsAppletCommonModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/disputes/DisputesModule;,
        Lcom/squareup/loyaltyreport/LoyaltyReportModule;,
        Lcom/squareup/salesreport/SalesReportDeviceSettingsModule;,
        Lcom/squareup/salesreport/SalesReportModule;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/ReportsAppletCommonModule$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008!\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\'J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\'J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH!\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/reports/applet/ReportsAppletCommonModule;",
        "",
        "()V",
        "bindAppletSectionListPresenter",
        "Lcom/squareup/applet/AppletSectionsListPresenter;",
        "presenter",
        "Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;",
        "bindCashManagementPermissionHolder",
        "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
        "cashManagementPermissionHolder",
        "Lcom/squareup/reports/applet/drawer/ReportsCashManagementPermissionsHolder;",
        "bindSalesReportWorkflowRunner",
        "Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;",
        "workflowRunner",
        "Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;",
        "Companion",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/reports/applet/ReportsAppletCommonModule$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/reports/applet/ReportsAppletCommonModule$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/reports/applet/ReportsAppletCommonModule$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/reports/applet/ReportsAppletCommonModule;->Companion:Lcom/squareup/reports/applet/ReportsAppletCommonModule$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideAppletMasterViewPresenter(Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/reports/applet/ReportsAppletHeader;Lcom/squareup/util/Res;Lcom/squareup/applet/AppletSelection;)Lcom/squareup/applet/AppletMasterViewPresenter;
    .locals 1
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/reports/applet/ReportsAppletScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/reports/applet/ReportsAppletCommonModule;->Companion:Lcom/squareup/reports/applet/ReportsAppletCommonModule$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/squareup/reports/applet/ReportsAppletCommonModule$Companion;->provideAppletMasterViewPresenter(Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/reports/applet/ReportsAppletHeader;Lcom/squareup/util/Res;Lcom/squareup/applet/AppletSelection;)Lcom/squareup/applet/AppletMasterViewPresenter;

    move-result-object p0

    return-object p0
.end method

.method public static final provideCoreSettingsCardParams(Lcom/squareup/util/Device;Lflow/Flow;)Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/reports/applet/ReportsAppletCommonModule;->Companion:Lcom/squareup/reports/applet/ReportsAppletCommonModule$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/reports/applet/ReportsAppletCommonModule$Companion;->provideCoreSettingsCardParams(Lcom/squareup/util/Device;Lflow/Flow;)Lcom/squareup/settings/ui/SettingsCardPresenter$CoreParameters;

    move-result-object p0

    return-object p0
.end method

.method public static final provideReportConfig()Lrx/subjects/BehaviorSubject;
    .locals 1
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/reports/applet/ReportsAppletScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/reports/applet/ReportsAppletCommonModule;->Companion:Lcom/squareup/reports/applet/ReportsAppletCommonModule$Companion;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/ReportsAppletCommonModule$Companion;->provideReportConfig()Lrx/subjects/BehaviorSubject;

    move-result-object v0

    return-object v0
.end method

.method public static final provideReportsAppletSectionsListPresenter(Lflow/Flow;Lcom/squareup/reports/applet/ReportsAppletSectionsList;Lcom/squareup/util/Device;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;Lcom/squareup/settings/server/Features;)Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;
    .locals 9
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/reports/applet/ReportsAppletScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/reports/applet/ReportsAppletCommonModule;->Companion:Lcom/squareup/reports/applet/ReportsAppletCommonModule$Companion;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lcom/squareup/reports/applet/ReportsAppletCommonModule$Companion;->provideReportsAppletSectionsListPresenter(Lflow/Flow;Lcom/squareup/reports/applet/ReportsAppletSectionsList;Lcom/squareup/util/Device;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;Lcom/squareup/settings/server/Features;)Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract bindAppletSectionListPresenter(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;)Lcom/squareup/applet/AppletSectionsListPresenter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindCashManagementPermissionHolder(Lcom/squareup/reports/applet/drawer/ReportsCashManagementPermissionsHolder;)Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/reports/applet/ReportsAppletScope;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindSalesReportWorkflowRunner(Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;)Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
