.class public Lcom/squareup/register/tutorial/InvoiceTutorialRunner$NoInvoiceTutorialRunner;
.super Ljava/lang/Object;
.source "InvoiceTutorialRunner.java"

# interfaces
.implements Lcom/squareup/register/tutorial/InvoiceTutorialRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/tutorial/InvoiceTutorialRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NoInvoiceTutorialRunner"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public customerOnInvoice(Z)V
    .locals 1

    .line 76
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No tutorials"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public forceStartFirstInvoiceTutorial()V
    .locals 2

    .line 44
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No tutorials"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public invoiceCustomAmountUpdated(Z)V
    .locals 1

    .line 52
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No tutorials"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public invoiceCustomerUpdated(Z)V
    .locals 1

    .line 56
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No tutorials"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public isDraftInvoice(Z)V
    .locals 1

    .line 72
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No tutorials"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public itemOnInvoice(Z)V
    .locals 1

    .line 68
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No tutorials"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public readyToFinishInvoices()V
    .locals 2

    .line 84
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No tutorials"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public readyToFinishInvoicesQuietly()V
    .locals 2

    .line 80
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No tutorials"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setCreatingCustomAmount(Z)V
    .locals 1

    .line 60
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No tutorials"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setInvoicesAppletActiveState(Z)V
    .locals 1

    .line 48
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No tutorials"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public startInvoiceTutorialIfPossible()V
    .locals 2

    .line 40
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No tutorials"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public updateInvoicePreviewBanner(Ljava/lang/String;)V
    .locals 1

    .line 64
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No tutorials"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
