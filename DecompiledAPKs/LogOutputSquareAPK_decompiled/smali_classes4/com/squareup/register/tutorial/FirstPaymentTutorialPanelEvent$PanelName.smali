.class final enum Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;
.super Ljava/lang/Enum;
.source "FirstPaymentTutorialPanelEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "PanelName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

.field public static final enum payment_tutorial_0_choose_or_create_ticket:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

.field public static final enum payment_tutorial_1_start:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

.field public static final enum payment_tutorial_2_tap_charge:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

.field public static final enum payment_tutorial_3_cash:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

.field public static final enum payment_tutorial_3_plug_in_or_swipe:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

.field public static final enum payment_tutorial_3a_enter_card_info:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

.field public static final enum payment_tutorial_3a_enter_cash_amount:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

.field public static final enum payment_tutorial_3a_enter_invoice_info:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

.field public static final enum payment_tutorial_4_tip:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

.field public static final enum payment_tutorial_5_sign:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

.field public static final enum payment_tutorial_6_receipt:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

.field public static final enum payment_tutorial_7_finished:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .line 23
    new-instance v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    const/4 v1, 0x0

    const-string v2, "payment_tutorial_0_choose_or_create_ticket"

    invoke-direct {v0, v2, v1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_0_choose_or_create_ticket:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    .line 24
    new-instance v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    const/4 v2, 0x1

    const-string v3, "payment_tutorial_1_start"

    invoke-direct {v0, v3, v2}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_1_start:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    .line 25
    new-instance v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    const/4 v3, 0x2

    const-string v4, "payment_tutorial_2_tap_charge"

    invoke-direct {v0, v4, v3}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_2_tap_charge:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    .line 26
    new-instance v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    const/4 v4, 0x3

    const-string v5, "payment_tutorial_3_cash"

    invoke-direct {v0, v5, v4}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3_cash:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    .line 27
    new-instance v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    const/4 v5, 0x4

    const-string v6, "payment_tutorial_3_plug_in_or_swipe"

    invoke-direct {v0, v6, v5}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3_plug_in_or_swipe:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    .line 28
    new-instance v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    const/4 v6, 0x5

    const-string v7, "payment_tutorial_3a_enter_card_info"

    invoke-direct {v0, v7, v6}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3a_enter_card_info:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    .line 29
    new-instance v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    const/4 v7, 0x6

    const-string v8, "payment_tutorial_3a_enter_invoice_info"

    invoke-direct {v0, v8, v7}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3a_enter_invoice_info:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    .line 30
    new-instance v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    const/4 v8, 0x7

    const-string v9, "payment_tutorial_3a_enter_cash_amount"

    invoke-direct {v0, v9, v8}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3a_enter_cash_amount:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    .line 31
    new-instance v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    const/16 v9, 0x8

    const-string v10, "payment_tutorial_4_tip"

    invoke-direct {v0, v10, v9}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_4_tip:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    .line 32
    new-instance v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    const/16 v10, 0x9

    const-string v11, "payment_tutorial_5_sign"

    invoke-direct {v0, v11, v10}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_5_sign:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    .line 33
    new-instance v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    const/16 v11, 0xa

    const-string v12, "payment_tutorial_6_receipt"

    invoke-direct {v0, v12, v11}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_6_receipt:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    .line 34
    new-instance v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    const/16 v12, 0xb

    const-string v13, "payment_tutorial_7_finished"

    invoke-direct {v0, v13, v12}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_7_finished:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    .line 22
    sget-object v13, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_0_choose_or_create_ticket:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_1_start:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_2_tap_charge:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3_cash:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3_plug_in_or_swipe:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3a_enter_card_info:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3a_enter_invoice_info:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3a_enter_cash_amount:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_4_tip:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_5_sign:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_6_receipt:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_7_finished:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    aput-object v1, v0, v12

    sput-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->$VALUES:[Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;
    .locals 1

    .line 22
    const-class v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;
    .locals 1

    .line 22
    sget-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->$VALUES:[Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-virtual {v0}, [Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    return-object v0
.end method


# virtual methods
.method public shouldSkip(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;)Z
    .locals 0

    if-ne p0, p1, :cond_1

    .line 38
    sget-object p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_5_sign:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    if-eq p0, p1, :cond_0

    sget-object p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_6_receipt:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    if-eq p0, p1, :cond_0

    sget-object p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_7_finished:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    if-ne p0, p1, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
