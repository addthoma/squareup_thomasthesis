.class abstract Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;
.super Lcom/squareup/register/tutorial/RegisterTutorial$BaseRegisterTutorial;
.source "AbstractFirstPaymentTutorial.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;
    }
.end annotation


# instance fields
.field private final NO_ACTION:Ljava/lang/Runnable;

.field private final accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final checkoutWorkflowRunner:Lcom/squareup/ui/main/CheckoutWorkflowRunner;

.field private final context:Landroid/app/Application;

.field private final declineActionName:Lcom/squareup/analytics/RegisterActionName;

.field private final declineViewName:Lcom/squareup/analytics/RegisterViewName;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final finishViewName:Lcom/squareup/analytics/RegisterViewName;

.field private final firstPaymentTooltipStatus:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/FirstPaymentTooltipStatus;",
            ">;"
        }
    .end annotation
.end field

.field private final homeIdentifier:Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;

.field private final isX2:Z

.field private lastNonFlowScreen:Lcom/squareup/container/ContainerTreeKey;

.field private final loggingHelper:Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

.field private onEndAction:Ljava/lang/Runnable;

.field private final openTicketsSelector:Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

.field private final presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tablet:Z

.field private final textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

.field protected final transaction:Lcom/squareup/payment/Transaction;

.field private tutorialState:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;


# direct methods
.method constructor <init>(Lcom/squareup/accountfreeze/AccountFreeze;Landroid/app/Application;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/OpenTicketsSettings;ZLcom/squareup/badbus/BadBus;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/settings/LocalSetting;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            "Landroid/app/Application;",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Z",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;",
            "Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;",
            "Lcom/squareup/analytics/RegisterActionName;",
            "Lcom/squareup/analytics/RegisterViewName;",
            "Lcom/squareup/analytics/RegisterViewName;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/FirstPaymentTooltipStatus;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            "Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;",
            "Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;",
            "Z)V"
        }
    .end annotation

    move-object v0, p0

    .line 130
    invoke-direct {p0}, Lcom/squareup/register/tutorial/RegisterTutorial$BaseRegisterTutorial;-><init>()V

    .line 50
    sget-object v1, Lcom/squareup/register/tutorial/-$$Lambda$AbstractFirstPaymentTutorial$yGeBY0wT80PxI2pWOOuaAHtOQsg;->INSTANCE:Lcom/squareup/register/tutorial/-$$Lambda$AbstractFirstPaymentTutorial$yGeBY0wT80PxI2pWOOuaAHtOQsg;

    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->NO_ACTION:Ljava/lang/Runnable;

    .line 97
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 103
    sget-object v1, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->NOT_STARTED:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->tutorialState:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    .line 104
    iget-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->NO_ACTION:Ljava/lang/Runnable;

    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onEndAction:Ljava/lang/Runnable;

    move-object v1, p1

    .line 131
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

    move-object v1, p2

    .line 132
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->context:Landroid/app/Application;

    move-object v1, p3

    .line 133
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    move-object v1, p4

    .line 134
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    move-object v1, p5

    .line 135
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p6

    .line 136
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    move v1, p7

    .line 137
    iput-boolean v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->tablet:Z

    move-object v1, p8

    .line 138
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->bus:Lcom/squareup/badbus/BadBus;

    move-object v1, p9

    .line 139
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->res:Lcom/squareup/util/Res;

    move-object v1, p10

    .line 140
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, p11

    .line 141
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    move-object v1, p12

    .line 142
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    move-object/from16 v1, p14

    .line 143
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->homeIdentifier:Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;

    move-object/from16 v1, p15

    .line 144
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->declineActionName:Lcom/squareup/analytics/RegisterActionName;

    move-object/from16 v1, p16

    .line 145
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->declineViewName:Lcom/squareup/analytics/RegisterViewName;

    move-object/from16 v1, p17

    .line 146
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->finishViewName:Lcom/squareup/analytics/RegisterViewName;

    move-object v1, p13

    .line 147
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->openTicketsSelector:Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;

    move-object/from16 v1, p18

    .line 148
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->firstPaymentTooltipStatus:Lcom/squareup/settings/LocalSetting;

    move-object/from16 v1, p19

    .line 149
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->transaction:Lcom/squareup/payment/Transaction;

    move-object/from16 v1, p20

    .line 150
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->checkoutWorkflowRunner:Lcom/squareup/ui/main/CheckoutWorkflowRunner;

    move-object/from16 v1, p21

    .line 151
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    move-object/from16 v1, p22

    .line 152
    iput-object v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->loggingHelper:Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

    move/from16 v1, p23

    .line 153
    iput-boolean v1, v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->isX2:Z

    return-void
.end method

.method private inferSaveTicketClicked(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)Z
    .locals 0

    .line 336
    invoke-virtual {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->everythingChanged()Z

    move-result p1

    return p1
.end method

.method private isHomeScreen(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1

    .line 357
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->homeIdentifier:Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;

    invoke-interface {v0, p1}, Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;->isOrderEntryScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p1

    return p1
.end method

.method private isOpenTicketsHomeScreen(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1

    .line 459
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->openTicketsSelector:Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;

    invoke-interface {v0, p1}, Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;->isOpenTicketsHomeScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p1

    return p1
.end method

.method static synthetic lambda$isTriggered$2()Ljava/lang/Boolean;
    .locals 1

    const/4 v0, 0x0

    .line 206
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$new$0()V
    .locals 0

    return-void
.end method

.method private onFinishTutorial()V
    .locals 3

    .line 440
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->getFinishPrompt()Lio/reactivex/Observable;

    move-result-object v0

    .line 441
    iget-object v1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    new-instance v2, Lcom/squareup/register/tutorial/-$$Lambda$AbstractFirstPaymentTutorial$aYjZdewOKuWfgY7j2sz04gQ-xNQ;

    invoke-direct {v2, p0}, Lcom/squareup/register/tutorial/-$$Lambda$AbstractFirstPaymentTutorial$aYjZdewOKuWfgY7j2sz04gQ-xNQ;-><init>(Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;)V

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private onHandlePostPaymentScreen(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 6

    .line 399
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    invoke-virtual {v0, p1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->onHandleTipScreen(Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 400
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_0

    .line 401
    sget-object p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_4_tip:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)V

    return v2

    .line 405
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    invoke-virtual {v0, p1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->onHandleSignatureScreen(Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 406
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 407
    sget-object p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_5_sign:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)V

    return v2

    .line 411
    :cond_1
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    invoke-virtual {v0, p1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->onHandleReceiptScreen(Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 412
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 413
    invoke-direct {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->setTutorialReadyToFinish()V

    .line 414
    sget-object p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_6_receipt:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)V

    return v2

    .line 418
    :cond_2
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    invoke-virtual {v0, p1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->onHandleDoneScreen(Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 419
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 420
    invoke-direct {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->setTutorialReadyToFinish()V

    .line 421
    sget-object p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_7_finished:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)V

    return v2

    .line 425
    :cond_3
    const-class v0, Lcom/squareup/ui/buyer/InInvoiceSentSavedScreen;

    invoke-virtual {p1, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 426
    invoke-direct {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->setTutorialReadyToFinish()V

    .line 427
    sget-object p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_7_finished:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->context:Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_new_sale:I

    sget v4, Lcom/squareup/checkout/R$string;->new_sale:I

    const-string v5, "new_sale"

    .line 428
    invoke-static {v0, v1, v3, v5, v4}, Lcom/squareup/register/tutorial/TutorialPhrases;->addMediumWeight(Landroid/content/Context;Lcom/squareup/util/Res;ILjava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 427
    invoke-virtual {p0, p1, v0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Lcom/squareup/phrase/Phrase;)V

    return v2

    :cond_4
    const/4 p1, 0x0

    return p1
.end method

.method private setTutorialReadyToFinish()V
    .locals 1

    .line 455
    sget-object v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->READY_TO_FINISH:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    iput-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->tutorialState:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    return-void
.end method


# virtual methods
.method final cartChanged(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 2

    .line 313
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->tutorialState:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    sget-object v1, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->RUNNING:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->getLastNonFlowScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->isHomeScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 314
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->inferSaveTicketClicked(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 322
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {p1}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object p1

    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne p1, v0, :cond_1

    .line 323
    iget-object p1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/TutorialPresenter;->hideTutorialBar()V

    return-void

    .line 326
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onHomeUpdate()V

    :cond_2
    return-void
.end method

.method public clearOnEndAction()V
    .locals 1

    .line 294
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->NO_ACTION:Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onEndAction:Ljava/lang/Runnable;

    return-void
.end method

.method abstract completeButtonAction()V
.end method

.method final exitQuietly(Z)V
    .locals 1

    .line 350
    sget-object v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->STOPPED:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    iput-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->tutorialState:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    .line 351
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->hideTutorialBar()V

    .line 352
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->endTutorial()V

    if-eqz p1, :cond_0

    .line 353
    iget-object p1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->declineActionName:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    :cond_0
    return-void
.end method

.method public forceStart()V
    .locals 1

    .line 280
    sget-object v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->STOPPED:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    iput-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->tutorialState:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    .line 281
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/register/tutorial/TutorialPresenter;->replaceTutorial(Lcom/squareup/register/tutorial/RegisterTutorial;)V

    return-void
.end method

.method abstract getFinishPrompt()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/register/tutorial/TutorialDialog$Prompt;",
            ">;"
        }
    .end annotation
.end method

.method protected final getLastNonFlowScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 361
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->lastNonFlowScreen:Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method

.method public handlePromptTap(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;)V
    .locals 0

    .line 200
    check-cast p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$FirstPaymentTutorialPrompt;

    invoke-virtual {p1, p2, p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$FirstPaymentTutorialPrompt;->handleTap(Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;)V

    return-void
.end method

.method final homeInteractionModeChanged(Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;)V
    .locals 2

    .line 303
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->tutorialState:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    sget-object v1, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->RUNNING:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->getLastNonFlowScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->isHomeScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 304
    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne p1, v0, :cond_0

    .line 305
    iget-object p1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/TutorialPresenter;->hideTutorialBar()V

    return-void

    .line 308
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onHomeUpdate()V

    :cond_1
    return-void
.end method

.method protected isPaymentTypeScreen(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1

    .line 299
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->checkoutWorkflowRunner:Lcom/squareup/ui/main/CheckoutWorkflowRunner;

    invoke-interface {v0, p1}, Lcom/squareup/ui/main/CheckoutWorkflowRunner;->isSelectMethodScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p1

    return p1
.end method

.method protected final isRunning()Z
    .locals 2

    .line 446
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->tutorialState:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    sget-object v1, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->RUNNING:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected final isTablet()Z
    .locals 1

    .line 451
    iget-boolean v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->tablet:Z

    return v0
.end method

.method public isTriggered()Z
    .locals 3

    .line 205
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    .line 206
    invoke-interface {v0}, Lcom/squareup/payment/pending/PendingTransactionsStore;->hasPendingTransactions()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/register/tutorial/-$$Lambda$AbstractFirstPaymentTutorial$1yTRLSqmrIvQOuumNFRTEGArm9Y;->INSTANCE:Lcom/squareup/register/tutorial/-$$Lambda$AbstractFirstPaymentTutorial$1yTRLSqmrIvQOuumNFRTEGArm9Y;

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2BlockingSupport;->getValueOrProvideDefault(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 208
    iget-object v1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->tutorialState:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    sget-object v2, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->STOPPED:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 209
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getTutorialSettings()Lcom/squareup/settings/server/TutorialSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/TutorialSettings;->shouldAutoStartFirstPaymentTutorial()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

    .line 210
    invoke-interface {v1}, Lcom/squareup/accountfreeze/AccountFreeze;->isFrozen()Z

    move-result v1

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    .line 212
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->shouldTrigger()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$onEnterScope$1$AbstractFirstPaymentTutorial(Lkotlin/Unit;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 193
    iget-object p1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->tutorialState:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    sget-object v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->NOT_STARTED:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    if-ne p1, v0, :cond_0

    .line 194
    iget-object p1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/TutorialPresenter;->maybeLookForNewTutorial()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onFinishTutorial$3$AbstractFirstPaymentTutorial(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 441
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-boolean v1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->isX2:Z

    invoke-virtual {v0, p1, v1}, Lcom/squareup/register/tutorial/TutorialPresenter;->prompt(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Z)V

    return-void
.end method

.method public onEnd()V
    .locals 1

    .line 285
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onEndAction:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 183
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/register/tutorial/-$$Lambda$S1f7v0gvXBpMyawZ61njSa0ggEw;

    invoke-direct {v1, p0}, Lcom/squareup/register/tutorial/-$$Lambda$S1f7v0gvXBpMyawZ61njSa0ggEw;-><init>(Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 186
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 187
    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->interactionMode()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/register/tutorial/-$$Lambda$9upVXNPmlLv0MKRg0JMNQVpOgCk;

    invoke-direct {v1, p0}, Lcom/squareup/register/tutorial/-$$Lambda$9upVXNPmlLv0MKRg0JMNQVpOgCk;-><init>(Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;)V

    .line 188
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 186
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 190
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 191
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailable()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/register/tutorial/-$$Lambda$AbstractFirstPaymentTutorial$L9W8727LCtLoAI9Tsd5tNuyt4ck;

    invoke-direct {v1, p0}, Lcom/squareup/register/tutorial/-$$Lambda$AbstractFirstPaymentTutorial$L9W8727LCtLoAI9Tsd5tNuyt4ck;-><init>(Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;)V

    .line 192
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 190
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onFinishedReceipt()V
    .locals 2

    .line 273
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->tutorialState:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    sget-object v1, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->RUNNING:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    if-ne v0, v1, :cond_0

    .line 274
    sget-object v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->READY_TO_FINISH:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    iput-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->tutorialState:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    :cond_0
    return-void
.end method

.method abstract onHandlePaymentScreen(Lcom/squareup/container/ContainerTreeKey;)Z
.end method

.method abstract onHomeUpdate()V
.end method

.method public onRequestExitTutorial()V
    .locals 4

    .line 216
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->tutorialState:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    sget-object v2, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->RUNNING:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->READY_TO_FINISH:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->in([Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    new-instance v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EarlyExitTutorial;

    invoke-direct {v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EarlyExitTutorial;-><init>()V

    .line 218
    iget-object v1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-boolean v2, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->isX2:Z

    invoke-virtual {v1, v0, v2}, Lcom/squareup/register/tutorial/TutorialPresenter;->prompt(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Z)V

    .line 219
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->declineViewName:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    :cond_0
    return-void
.end method

.method public onShowScreen(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 2

    .line 224
    sget-object v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$1;->$SwitchMap$com$squareup$register$tutorial$AbstractFirstPaymentTutorial$State:[I

    iget-object v1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->tutorialState:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    invoke-virtual {v1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 237
    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->isHomeScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->isOpenTicketsHomeScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 246
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onShowScreenWhileRunning(Lcom/squareup/container/ContainerTreeKey;)V

    goto :goto_1

    .line 239
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/TutorialPresenter;->hideTutorialBar()V

    .line 241
    iget-object p1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->firstPaymentTooltipStatus:Lcom/squareup/settings/LocalSetting;

    sget-object v0, Lcom/squareup/settings/FirstPaymentTooltipStatus;->DISMISSED:Lcom/squareup/settings/FirstPaymentTooltipStatus;

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 242
    invoke-direct {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onFinishTutorial()V

    .line 243
    iget-object p1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->finishViewName:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    goto :goto_1

    .line 251
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown tutorial state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->tutorialState:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 233
    :cond_3
    invoke-virtual {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onShowScreenWhileRunning(Lcom/squareup/container/ContainerTreeKey;)V

    goto :goto_1

    .line 227
    :cond_4
    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->isHomeScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->isOpenTicketsHomeScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 228
    :cond_5
    invoke-virtual {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->startTutorial(Lcom/squareup/container/ContainerTreeKey;)V

    :cond_6
    :goto_1
    return-void
.end method

.method protected final onShowScreenWhileRunning(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 3

    .line 376
    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->isHomeScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    iget-object v1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v1

    sget-object v2, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne v1, v2, :cond_0

    .line 378
    iget-object v1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v1}, Lcom/squareup/register/tutorial/TutorialPresenter;->hideTutorialBar()V

    .line 380
    :cond_0
    instance-of v1, p1, Lcom/squareup/register/tutorial/TutorialDialogScreen;

    if-eqz v1, :cond_1

    return-void

    :cond_1
    if-eqz v0, :cond_2

    .line 385
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onHomeUpdate()V

    goto :goto_0

    .line 386
    :cond_2
    invoke-virtual {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onHandlePaymentScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onHandlePostPaymentScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 387
    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->isOpenTicketsHomeScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 388
    sget-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_0_choose_or_create_ticket:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    iget-object v1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    .line 389
    invoke-virtual {v1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getOpenTicketsText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 388
    invoke-virtual {p0, v0, v1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 392
    :cond_3
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialPresenter;->hideTutorialBar()V

    .line 395
    :cond_4
    :goto_0
    iput-object p1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->lastNonFlowScreen:Lcom/squareup/container/ContainerTreeKey;

    return-void
.end method

.method public onShowingThanks()V
    .locals 6

    .line 257
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->getLastNonFlowScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    .line 258
    iget-object v1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->tutorialState:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    sget-object v3, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->RUNNING:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    sget-object v3, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->READY_TO_FINISH:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->in([Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-class v1, Lcom/squareup/ui/buyer/InReceiptScreen;

    .line 259
    invoke-virtual {v0, v1}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 260
    invoke-static {v0}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;->isReceiptSelectionScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 262
    :cond_0
    sget-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_7_finished:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    iget-object v1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->context:Landroid/app/Application;

    iget-object v2, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_new_sale:I

    sget v4, Lcom/squareup/checkout/R$string;->new_sale:I

    const-string v5, "new_sale"

    .line 263
    invoke-static {v1, v2, v3, v5, v4}, Lcom/squareup/register/tutorial/TutorialPhrases;->addMediumWeight(Landroid/content/Context;Lcom/squareup/util/Res;ILjava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 262
    invoke-virtual {p0, v0, v1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Lcom/squareup/phrase/Phrase;)V

    :cond_1
    return-void
.end method

.method protected final setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Lcom/squareup/phrase/Phrase;)V
    .locals 0

    .line 371
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected final setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)V
    .locals 1

    .line 366
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->loggingHelper:Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->logPanel(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;)V

    .line 367
    iget-object p1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->presenter:Lcom/squareup/register/tutorial/TutorialPresenter;

    invoke-virtual {p1, p2}, Lcom/squareup/register/tutorial/TutorialPresenter;->setContent(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setOnEndAction(Ljava/lang/Runnable;)V
    .locals 1

    const-string v0, "action should not be null"

    .line 289
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 290
    iput-object p1, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onEndAction:Ljava/lang/Runnable;

    return-void
.end method

.method abstract shouldTrigger()Z
.end method

.method final startTutorial(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 2

    .line 341
    iget-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->setCurrentPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    .line 342
    sget-object v0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;->RUNNING:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    iput-object v0, p0, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->tutorialState:Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial$State;

    .line 344
    invoke-virtual {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onShowScreenWhileRunning(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method
