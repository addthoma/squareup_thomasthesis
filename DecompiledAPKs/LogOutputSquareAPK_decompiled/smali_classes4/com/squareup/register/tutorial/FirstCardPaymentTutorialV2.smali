.class public Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;
.super Ljava/lang/Object;
.source "FirstCardPaymentTutorialV2.java"

# interfaces
.implements Lcom/squareup/tutorialv2/Tutorial;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;,
        Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;
    }
.end annotation


# instance fields
.field canSeeRatesTour:Z

.field private final device:Lcom/squareup/util/Device;

.field private final eventLocker:Lcom/squareup/register/tutorial/TutorialEventLocker;

.field private final features:Lcom/squareup/settings/server/Features;

.field hasItems:Z

.field hasPosBestAvailableProductIntent:Z

.field hasSeenRatesTour:Z

.field private latestCapabilities:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;"
        }
    .end annotation
.end field

.field private final lockScreenMonitor:Lcom/squareup/permissions/ui/LockScreenMonitor;

.field private final loggingHelper:Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final oracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

.field private final orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

.field private final output:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/tutorialv2/TutorialState;",
            ">;"
        }
    .end annotation
.end field

.field private readyForCard:Z

.field private readyToFinish:Z

.field redirectToFavoritesOrKeypad:Z

.field private final resources:Landroid/content/res/Resources;

.field private returnToState:Lcom/squareup/tutorialv2/TutorialState;

.field private final runner:Lcom/squareup/register/tutorial/CardTutorialRunner;

.field private final textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;


# direct methods
.method constructor <init>(Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;Lcom/squareup/register/tutorial/CardTutorialRunner;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/Features;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/util/Device;Landroid/content/res/Resources;Lcom/squareup/permissions/ui/LockScreenMonitor;Lcom/squareup/register/tutorial/TutorialEventLocker;Lcom/squareup/feetutorial/FeeTutorial;)V
    .locals 1
    .param p5    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 88
    const-class v0, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    .line 89
    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->latestCapabilities:Ljava/util/EnumSet;

    .line 113
    iput-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->loggingHelper:Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

    .line 114
    iput-object p2, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    .line 115
    iput-object p3, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->runner:Lcom/squareup/register/tutorial/CardTutorialRunner;

    .line 116
    iput-object p4, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->oracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    .line 117
    iput-object p5, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->mainScheduler:Lio/reactivex/Scheduler;

    .line 118
    iput-object p6, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->features:Lcom/squareup/settings/server/Features;

    .line 119
    iput-object p7, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    .line 120
    iput-object p8, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->device:Lcom/squareup/util/Device;

    .line 121
    iput-object p9, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->resources:Landroid/content/res/Resources;

    .line 122
    iput-object p10, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->lockScreenMonitor:Lcom/squareup/permissions/ui/LockScreenMonitor;

    .line 123
    iput-object p11, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->eventLocker:Lcom/squareup/register/tutorial/TutorialEventLocker;

    .line 125
    invoke-interface {p12}, Lcom/squareup/feetutorial/FeeTutorial;->getCanShow()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->canSeeRatesTour:Z

    const/4 p1, 0x0

    .line 126
    iput-boolean p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->hasItems:Z

    .line 127
    iput-boolean p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->hasPosBestAvailableProductIntent:Z

    .line 128
    iput-boolean p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->redirectToFavoritesOrKeypad:Z

    return-void
.end method

.method private configureOrderEntryPage()V
    .locals 2

    .line 500
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->shouldStartOnLibraryOrFavorites()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 501
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 502
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV1:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->setCurrentPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    goto :goto_0

    .line 504
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->LIBRARY:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->setCurrentPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    goto :goto_0

    .line 507
    :cond_1
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    sget-object v1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->setCurrentPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    :goto_0
    return-void
.end method

.method private getOrderEntryPageText()Ljava/lang/CharSequence;
    .locals 1

    .line 431
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->shouldStartOnLibraryOrFavorites()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 432
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 433
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->getOrderEntryPageTextForTablet()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    .line 435
    :cond_0
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->getOrderEntryPageTextForPhone()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    .line 438
    :cond_1
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getStartTextWithMinimum()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private getOrderEntryPageTextForPhone()Ljava/lang/CharSequence;
    .locals 3

    .line 469
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->resources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 470
    sget-object v1, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$1;->$SwitchMap$com$squareup$orderentry$pages$OrderEntryPageKey:[I

    iget-object v2, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    invoke-virtual {v2}, Lcom/squareup/orderentry/pages/OrderEntryPages;->getCurrentPage()Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x2

    packed-switch v1, :pswitch_data_0

    const/4 v0, 0x0

    return-object v0

    :pswitch_0
    if-ne v0, v2, :cond_1

    .line 486
    iget-boolean v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->hasItems:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    .line 487
    invoke-virtual {v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getStartTextItemsKeypadPhoneLandscape()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    .line 488
    invoke-virtual {v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getStartTextWithMinimum()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    return-object v0

    .line 490
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->hasItems:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    .line 491
    invoke-virtual {v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getStartTextItemsKeypadPhonePortrait()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    .line 492
    invoke-virtual {v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getStartTextWithMinimum()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_1
    return-object v0

    :pswitch_1
    if-ne v0, v2, :cond_3

    .line 480
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    iget-boolean v1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->hasItems:Z

    invoke-virtual {v0, v1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getStartTextItemsLibraryPhoneLandscape(Z)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    .line 482
    :cond_3
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    iget-boolean v1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->hasItems:Z

    invoke-virtual {v0, v1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getStartTextItemsLibraryPhonePortrait(Z)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getOrderEntryPageTextForTablet()Ljava/lang/CharSequence;
    .locals 3

    .line 442
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->resources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 443
    sget-object v1, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$1;->$SwitchMap$com$squareup$orderentry$pages$OrderEntryPageKey:[I

    iget-object v2, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    invoke-virtual {v2}, Lcom/squareup/orderentry/pages/OrderEntryPages;->getCurrentPage()Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x2

    packed-switch v1, :pswitch_data_0

    const/4 v0, 0x0

    return-object v0

    :pswitch_0
    if-ne v0, v2, :cond_0

    .line 459
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    iget-boolean v1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->hasItems:Z

    invoke-virtual {v0, v1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getStartTextItemsKeypadTabletLandscape(Z)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    .line 461
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    iget-boolean v1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->hasItems:Z

    invoke-virtual {v0, v1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getStartTextItemsKeypadTabletPortrait(Z)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    :pswitch_1
    if-ne v0, v2, :cond_1

    .line 453
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    iget-boolean v1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->hasItems:Z

    invoke-virtual {v0, v1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getStartTextItemsLibraryTabletLandscape(Z)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    .line 455
    :cond_1
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    iget-boolean v1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->hasItems:Z

    invoke-virtual {v0, v1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getStartTextItemsLibraryTabletPortrait(Z)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private handleDialogPrimary(Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;)V
    .locals 1

    .line 355
    sget-object v0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$1;->$SwitchMap$com$squareup$register$tutorial$FirstCardPaymentTutorialV2$DialogKeys:[I

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 366
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->runner:Lcom/squareup/register/tutorial/CardTutorialRunner;

    invoke-interface {p1}, Lcom/squareup/register/tutorial/CardTutorialRunner;->linkBank()V

    .line 367
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->stopImmediately()V

    :goto_0
    return-void

    .line 361
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->stopImmediately()V

    :cond_2
    return-void
.end method

.method private handleDialogSecondary(Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;)V
    .locals 1

    .line 372
    sget-object v0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$1;->$SwitchMap$com$squareup$register$tutorial$FirstCardPaymentTutorialV2$DialogKeys:[I

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 381
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->stopImmediately()V

    :goto_0
    return-void

    .line 378
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No secondary button on finish dialog"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 374
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->stopImmediately()V

    return-void
.end method

.method private logAndNextState(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;
    .locals 6

    .line 186
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->lockScreenMonitor:Lcom/squareup/permissions/ui/LockScreenMonitor;

    invoke-interface {v0}, Lcom/squareup/permissions/ui/LockScreenMonitor;->showLockScreen()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->eventLocker:Lcom/squareup/register/tutorial/TutorialEventLocker;

    invoke-virtual {v0, p1}, Lcom/squareup/register/tutorial/TutorialEventLocker;->isLocked(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_5

    :cond_0
    const/4 v0, -0x1

    .line 190
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x0

    const-string v3, "Dismissed AudioPermissionCardScreen"

    const-string v4, "Leaving RatesTourScreen"

    const/4 v5, 0x1

    sparse-switch v1, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v1, "Applet Selected"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x25

    goto/16 :goto_0

    :sswitch_1
    const-string v1, "Shown SignScreen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x13

    goto/16 :goto_0

    :sswitch_2
    const-string v1, "Shown ReceiptSelectionScreen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x17

    goto/16 :goto_0

    :sswitch_3
    const-string v1, "X2 shown signature"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x14

    goto/16 :goto_0

    :sswitch_4
    const-string v1, "Shown AuthSpinnerScreen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x21

    goto/16 :goto_0

    :sswitch_5
    const-string v1, "Shown TipScreen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x11

    goto/16 :goto_0

    :sswitch_6
    const-string v1, "Shown SelectMethodScreen, above max card amount"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0xd

    goto/16 :goto_0

    :sswitch_7
    const-string v1, "Favorites Tab Selected"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x8

    goto/16 :goto_0

    :sswitch_8
    const-string v1, "On home edit mode"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x24

    goto/16 :goto_0

    :sswitch_9
    const-string v1, "Shown ReceiptScreenLegacy"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x15

    goto/16 :goto_0

    :sswitch_a
    const-string v1, "X2 shown receipt entry"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x16

    goto/16 :goto_0

    :sswitch_b
    const-string v1, "Shown EditInvoiceScreen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x10

    goto/16 :goto_0

    :sswitch_c
    const-string v1, "Shown SelectMethodScreen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0xe

    goto/16 :goto_0

    :sswitch_d
    const-string v1, "X2 on pip closed"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x6

    goto/16 :goto_0

    :sswitch_e
    const-string v1, "Shown EnterPasscodeToUnlockScreen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x23

    goto/16 :goto_0

    :sswitch_f
    const-string v1, "Shown RatesTourScreen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x1f

    goto/16 :goto_0

    :sswitch_10
    const-string v1, "KeyPad Displayed"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x9

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x3

    goto/16 :goto_0

    :sswitch_12
    const-string v1, "TutorialV2DialogScreen primary tapped"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x1d

    goto/16 :goto_0

    :sswitch_13
    const-string v1, "SelectMethodScreen selected a non-card option"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x22

    goto/16 :goto_0

    :sswitch_14
    const-string v1, "X2 shown tip"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x12

    goto/16 :goto_0

    :sswitch_15
    const-string v1, "Shown OrderEntryScreen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    goto/16 :goto_0

    :sswitch_16
    const-string v1, "Shown PayCardCreditScreen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0xf

    goto/16 :goto_0

    :sswitch_17
    const-string v1, "Shown SelectMethodScreen, below min card amount"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0xc

    goto/16 :goto_0

    :sswitch_18
    const-string v1, "Leaving SelectMethodScreen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x7

    goto/16 :goto_0

    :sswitch_19
    const-string v1, "TutorialV2DialogScreen secondary tapped"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x1e

    goto/16 :goto_0

    :sswitch_1a
    const-string v1, "Shown Receipt -- All Done"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x1a

    goto/16 :goto_0

    :sswitch_1b
    const-string v1, "Shown JailScreen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    goto/16 :goto_0

    :sswitch_1c
    const-string v1, "Shown AudioPermissionCardScreen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x20

    goto :goto_0

    :sswitch_1d
    const-string v1, "Finished with receipt"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x1c

    goto :goto_0

    :sswitch_1e
    const-string v1, "On OrderEntryScreen and ready for card"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0xb

    goto :goto_0

    :sswitch_1f
    const-string v1, "X2 after done with receipt"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x19

    goto :goto_0

    :sswitch_20
    const-string v1, "Leaving SelectMethodScreen, card amount in range"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0xa

    goto :goto_0

    :sswitch_21
    const-string v1, "Shown ReceiptInputScreen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x18

    goto :goto_0

    :sswitch_22
    const-string v1, "Shown MasterDetailTicketScreen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_23
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_24
    const-string v1, "Shown ReceiptCompleteScreen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/16 v0, 0x1b

    goto :goto_0

    :sswitch_25
    const-string v1, "Dismissed EnterPasscodeToUnlockScreen"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x4

    :cond_1
    :goto_0
    const/4 p1, 0x0

    packed-switch v0, :pswitch_data_0

    return-object p1

    .line 334
    :pswitch_0
    sget-object p1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    return-object p1

    .line 330
    :pswitch_1
    sget-object p1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    return-object p1

    .line 327
    :pswitch_2
    sget-object p1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    return-object p1

    .line 319
    :pswitch_3
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->eventLocker:Lcom/squareup/register/tutorial/TutorialEventLocker;

    filled-new-array {v3}, [Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/register/tutorial/TutorialEventLocker;->lockUntil([Ljava/lang/String;)V

    .line 320
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tutorialv2/TutorialState;

    iput-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->returnToState:Lcom/squareup/tutorialv2/TutorialState;

    .line 321
    sget-object p1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    return-object p1

    .line 315
    :pswitch_4
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->eventLocker:Lcom/squareup/register/tutorial/TutorialEventLocker;

    filled-new-array {v4}, [Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/register/tutorial/TutorialEventLocker;->lockUntil([Ljava/lang/String;)V

    .line 316
    sget-object p1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    return-object p1

    .line 311
    :pswitch_5
    check-cast p2, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    invoke-direct {p0, p2}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->handleDialogSecondary(Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;)V

    return-object p1

    .line 306
    :pswitch_6
    check-cast p2, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    invoke-direct {p0, p2}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->handleDialogPrimary(Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;)V

    return-object p1

    .line 301
    :pswitch_7
    iput-boolean v5, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->readyToFinish:Z

    return-object p1

    .line 294
    :pswitch_8
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->loggingHelper:Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

    sget-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_7_finished:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-virtual {p1, v0}, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->logPanel(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;)V

    .line 295
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 296
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getDoneScreenText()Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    .line 297
    invoke-virtual {p1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getContinuePaymentText()Ljava/lang/CharSequence;

    move-result-object p1

    .line 296
    :goto_1
    invoke-static {p1}, Lcom/squareup/tutorialv2/TutorialState;->display(Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p1

    .line 297
    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    return-object p1

    .line 288
    :pswitch_9
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->loggingHelper:Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

    sget-object p2, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_7_finished:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-virtual {p1, p2}, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->logPanel(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;)V

    .line 289
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getDoneScreenText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/tutorialv2/TutorialState;->display(Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    return-object p1

    .line 284
    :pswitch_a
    sget-object p1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    return-object p1

    .line 280
    :pswitch_b
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->loggingHelper:Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

    sget-object p2, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_6_receipt:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-virtual {p1, p2}, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->logPanel(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;)V

    .line 281
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getReceiptScreenText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/tutorialv2/TutorialState;->display(Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    return-object p1

    .line 273
    :pswitch_c
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->loggingHelper:Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

    sget-object p2, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_5_sign:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-virtual {p1, p2}, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->logPanel(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;)V

    .line 274
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getSignScreenText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/tutorialv2/TutorialState;->display(Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    return-object p1

    .line 267
    :pswitch_d
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->loggingHelper:Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

    sget-object p2, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_4_tip:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-virtual {p1, p2}, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->logPanel(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;)V

    .line 268
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getTipScreenText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/tutorialv2/TutorialState;->display(Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    return-object p1

    .line 261
    :pswitch_e
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->loggingHelper:Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

    sget-object p2, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3a_enter_invoice_info:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-virtual {p1, p2}, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->logPanel(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;)V

    .line 262
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getInvoiceScreenText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/tutorialv2/TutorialState;->display(Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    return-object p1

    .line 256
    :pswitch_f
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->loggingHelper:Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

    sget-object p2, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3a_enter_card_info:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-virtual {p1, p2}, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->logPanel(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;)V

    .line 257
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getCardEntryScreenText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/tutorialv2/TutorialState;->display(Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    return-object p1

    .line 250
    :pswitch_10
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->loggingHelper:Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

    sget-object p2, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3_plug_in_or_swipe:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-virtual {p1, p2}, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->logPanel(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;)V

    .line 251
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    iget-object p2, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->latestCapabilities:Ljava/util/EnumSet;

    .line 252
    invoke-virtual {p1, p2}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getPaymentTypeScreenText(Ljava/util/EnumSet;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 251
    invoke-static {p1}, Lcom/squareup/tutorialv2/TutorialState;->display(Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p1

    .line 252
    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    return-object p1

    .line 245
    :pswitch_11
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->loggingHelper:Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

    sget-object p2, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3_plug_in_or_swipe:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-virtual {p1, p2}, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->logPanel(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;)V

    .line 246
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    sget p2, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_charge_less:I

    .line 247
    invoke-virtual {p1, p2}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->withMaximum(I)Ljava/lang/CharSequence;

    move-result-object p1

    .line 246
    invoke-static {p1}, Lcom/squareup/tutorialv2/TutorialState;->display(Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p1

    .line 247
    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    return-object p1

    .line 240
    :pswitch_12
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->loggingHelper:Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

    sget-object p2, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3_plug_in_or_swipe:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-virtual {p1, p2}, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->logPanel(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;)V

    .line 241
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    sget p2, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_charge_more:I

    .line 242
    invoke-virtual {p1, p2}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->withMinimum(I)Ljava/lang/CharSequence;

    move-result-object p1

    .line 241
    invoke-static {p1}, Lcom/squareup/tutorialv2/TutorialState;->display(Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p1

    .line 242
    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    return-object p1

    .line 236
    :pswitch_13
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->onHomeReadyForCard()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    return-object p1

    .line 233
    :pswitch_14
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledBillPayment;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledBillPayment;

    if-ne p2, p1, :cond_3

    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->onHomeReadyForCard()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    goto :goto_2

    :cond_3
    sget-object p1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    :goto_2
    return-object p1

    .line 228
    :pswitch_15
    sget-object p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_0_choose_or_create_ticket:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    iget-boolean p2, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->readyForCard:Z

    if-eqz p2, :cond_4

    iget-object p2, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    .line 229
    invoke-virtual {p2}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getTapChargeText()Ljava/lang/CharSequence;

    move-result-object p2

    goto :goto_3

    :cond_4
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->getOrderEntryPageText()Ljava/lang/CharSequence;

    move-result-object p2

    .line 228
    :goto_3
    invoke-direct {p0, p1, p2}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->onHomeEvent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    return-object p1

    .line 219
    :pswitch_16
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledBillPayment;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledBillPayment;

    if-ne p2, p1, :cond_5

    .line 220
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->onHomeNotReadyForCard()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    goto :goto_4

    :cond_5
    sget-object p1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    :goto_4
    return-object p1

    .line 216
    :pswitch_17
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->onHomeNotReadyForCard()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    return-object p1

    .line 209
    :pswitch_18
    iget-object p2, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->returnToState:Lcom/squareup/tutorialv2/TutorialState;

    .line 210
    iput-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->returnToState:Lcom/squareup/tutorialv2/TutorialState;

    return-object p2

    :pswitch_19
    const-string p1, "Home not visible"

    if-ne p2, p1, :cond_6

    const/4 v2, 0x1

    .line 204
    :cond_6
    iput-boolean v2, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->readyForCard:Z

    .line 205
    sget-object p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_1_start:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    .line 206
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->getOrderEntryPageText()Ljava/lang/CharSequence;

    move-result-object p2

    .line 205
    invoke-direct {p0, p1, p2}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->onHomeEvent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    return-object p1

    .line 197
    :pswitch_1a
    sget-object p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_0_choose_or_create_ticket:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    iget-object p2, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    .line 198
    invoke-virtual {p2}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getOpenTicketsText()Ljava/lang/CharSequence;

    move-result-object p2

    .line 197
    invoke-direct {p0, p1, p2}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->onHomeEvent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    return-object p1

    .line 193
    :pswitch_1b
    sget-object p1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    return-object p1

    .line 187
    :cond_7
    :goto_5
    sget-object p1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    return-object p1

    :sswitch_data_0
    .sparse-switch
        -0x7fe47a36 -> :sswitch_25
        -0x7ed736b2 -> :sswitch_24
        -0x79cdfdd5 -> :sswitch_23
        -0x79a92406 -> :sswitch_22
        -0x78aca8d3 -> :sswitch_21
        -0x74bd0c1d -> :sswitch_20
        -0x6c71f3ce -> :sswitch_1f
        -0x69b9f3a4 -> :sswitch_1e
        -0x607d3e54 -> :sswitch_1d
        -0x5e41d2ce -> :sswitch_1c
        -0x58a437ab -> :sswitch_1b
        -0x4cf5b256 -> :sswitch_1a
        -0x45ba1ef6 -> :sswitch_19
        -0x43d36e0b -> :sswitch_18
        -0x435292c1 -> :sswitch_17
        -0x37ffe512 -> :sswitch_16
        -0x2f75f541 -> :sswitch_15
        -0x2ddb38ba -> :sswitch_14
        -0x2b49b8e8 -> :sswitch_13
        -0x2a30eac4 -> :sswitch_12
        -0x2963d136 -> :sswitch_11
        -0x8e5028b -> :sswitch_10
        0x1430b88 -> :sswitch_f
        0xab78a32 -> :sswitch_e
        0x17076230 -> :sswitch_d
        0x1fbd2f78 -> :sswitch_c
        0x23e39a80 -> :sswitch_b
        0x3f76f175 -> :sswitch_a
        0x44a117de -> :sswitch_9
        0x54c7e039 -> :sswitch_8
        0x57cb5a4f -> :sswitch_7
        0x5869a0bd -> :sswitch_6
        0x5fe86c58 -> :sswitch_5
        0x6358898e -> :sswitch_4
        0x670db323 -> :sswitch_3
        0x682393ef -> :sswitch_2
        0x7a1e5738 -> :sswitch_1
        0x7cf2cba1 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_17
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_d
        :pswitch_c
        :pswitch_c
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private maybeFinish()Z
    .locals 1

    .line 412
    iget-boolean v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->readyToFinish:Z

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->runner:Lcom/squareup/register/tutorial/CardTutorialRunner;

    invoke-interface {v0}, Lcom/squareup/register/tutorial/CardTutorialRunner;->showFinishDialog()V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private maybeShowFeeTutorial()Z
    .locals 1

    .line 423
    iget-boolean v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->canSeeRatesTour:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->hasSeenRatesTour:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 424
    iput-boolean v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->hasSeenRatesTour:Z

    .line 425
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->runner:Lcom/squareup/register/tutorial/CardTutorialRunner;

    invoke-interface {v0}, Lcom/squareup/register/tutorial/CardTutorialRunner;->maybeShowFeeTutorial()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private onHomeEvent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState;
    .locals 1

    if-nez p2, :cond_0

    .line 393
    sget-object p1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    return-object p1

    .line 395
    :cond_0
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->maybeFinish()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 396
    sget-object p1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    return-object p1

    .line 398
    :cond_1
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->maybeShowFeeTutorial()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 399
    sget-object p1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    return-object p1

    .line 401
    :cond_2
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->loggingHelper:Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;->logPanel(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;)V

    .line 402
    invoke-static {p2}, Lcom/squareup/tutorialv2/TutorialState;->display(Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/tutorialv2/TutorialState$Builder;->build()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    return-object p1
.end method

.method private onHomeNotReadyForCard()Lcom/squareup/tutorialv2/TutorialState;
    .locals 2

    const/4 v0, 0x0

    .line 343
    iput-boolean v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->readyForCard:Z

    .line 344
    sget-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_1_start:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    .line 345
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->getOrderEntryPageText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 344
    invoke-direct {p0, v0, v1}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->onHomeEvent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method

.method private onHomeReadyForCard()Lcom/squareup/tutorialv2/TutorialState;
    .locals 2

    const/4 v0, 0x1

    .line 349
    iput-boolean v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->readyForCard:Z

    .line 350
    sget-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_2_tap_charge:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    iget-object v1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->textRenderer:Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    .line 351
    invoke-virtual {v1}, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;->getTapChargeText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 350
    invoke-direct {p0, v0, v1}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->onHomeEvent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method

.method private shouldStartOnLibraryOrFavorites()Z
    .locals 3

    .line 513
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_ITEMIZED_PAYMENTS_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 516
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->hasItems:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->hasPosBestAvailableProductIntent:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_POS_INTENT_PAYMENTS_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    .line 517
    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v1, 0x1

    :cond_2
    return v1
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 156
    iget-boolean v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->redirectToFavoritesOrKeypad:Z

    if-eqz v0, :cond_0

    .line 157
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->configureOrderEntryPage()V

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->runner:Lcom/squareup/register/tutorial/CardTutorialRunner;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 161
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->oracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStates()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->features:Lcom/squareup/settings/server/Features;

    .line 162
    invoke-static {v1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracleFilters;->asCapabilities(Lcom/squareup/settings/server/Features;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    .line 161
    invoke-static {v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->mainScheduler:Lio/reactivex/Scheduler;

    .line 163
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/register/tutorial/-$$Lambda$So034Bp6YnxhvFW_10Us80BHGG8;

    invoke-direct {v1, p0}, Lcom/squareup/register/tutorial/-$$Lambda$So034Bp6YnxhvFW_10Us80BHGG8;-><init>(Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;)V

    .line 164
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 161
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    const/4 p1, 0x0

    const-string v0, "Shown OrderEntryScreen"

    .line 167
    invoke-virtual {p0, v0, p1}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->onTutorialEvent(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public onExitRequested()V
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->runner:Lcom/squareup/register/tutorial/CardTutorialRunner;

    invoke-interface {v0}, Lcom/squareup/register/tutorial/CardTutorialRunner;->showEarlyExitDialog()V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method onNewReaderCapabilities(Ljava/util/EnumSet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;)V"
        }
    .end annotation

    .line 522
    iput-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->latestCapabilities:Ljava/util/EnumSet;

    return-void
.end method

.method public onTutorialEvent(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const-string v1, "onTutorialEvent(name=%s, value=%s)"

    .line 140
    invoke-static {v1, v0}, Ltimber/log/Timber;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    invoke-direct {p0, p1, p2}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->logAndNextState(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 143
    iget-object p2, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onTutorialPendingActionEvent(Ljava/lang/String;Lcom/squareup/tutorialv2/TutorialCore$PendingAction;)V
    .locals 0

    return-void
.end method

.method public stopImmediately()V
    .locals 2

    .line 407
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/tutorialv2/TutorialState;->FINISHED:Lcom/squareup/tutorialv2/TutorialState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public tutorialState()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tutorialv2/TutorialState;",
            ">;"
        }
    .end annotation

    .line 132
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method
