.class public interface abstract Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;
.super Ljava/lang/Object;
.source "V1TutorialHomeIdentifier.kt"


# annotations
.annotation runtime Lkotlin/Deprecated;
    message = "Use the V2 tutorial framework to post events from your home screen."
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;",
        "",
        "isOrderEntryScreen",
        "",
        "screen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract isOrderEntryScreen(Lcom/squareup/container/ContainerTreeKey;)Z
.end method
