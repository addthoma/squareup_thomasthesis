.class public Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;
.super Lcom/squareup/tutorialv2/TutorialCreator;
.source "FirstCardPaymentTutorialV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Creator"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator$Seed;
    }
.end annotation


# instance fields
.field private final appIdling:Lcom/squareup/ui/main/AppIdling;

.field private final features:Lcom/squareup/settings/server/Features;

.field private orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

.field private seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/tutorialv2/TutorialSeed;",
            ">;"
        }
    .end annotation
.end field

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field startTutorial:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/tutorialv2/TutorialSeed$Priority;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCogsHelper:Lcom/squareup/register/tutorial/TutorialCogsHelper;

.field private final tutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljavax/inject/Provider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/settings/server/Features;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/register/tutorial/TutorialCogsHelper;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;",
            ">;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/main/AppIdling;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            "Lcom/squareup/register/tutorial/TutorialCogsHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 549
    invoke-direct {p0}, Lcom/squareup/tutorialv2/TutorialCreator;-><init>()V

    .line 532
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 541
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->startTutorial:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 550
    iput-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->tutorialProvider:Ljavax/inject/Provider;

    .line 551
    iput-object p2, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 552
    iput-object p3, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->appIdling:Lcom/squareup/ui/main/AppIdling;

    .line 553
    iput-object p4, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->features:Lcom/squareup/settings/server/Features;

    .line 554
    iput-object p5, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    .line 555
    iput-object p6, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->tutorialCogsHelper:Lcom/squareup/register/tutorial/TutorialCogsHelper;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;)Ljavax/inject/Provider;
    .locals 0

    .line 531
    iget-object p0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->tutorialProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$2(Ljava/lang/Boolean;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 571
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method private shouldStart()Z
    .locals 2

    .line 587
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->FIRST_PAYMENT_TUTORIAL_V2:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    .line 588
    invoke-interface {v0}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->hasOrderEntryApplet()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$FirstCardPaymentTutorialV2$Creator(Lcom/squareup/tutorialv2/TutorialSeed$Priority;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 563
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->hasPosBestAvailableProductIntent()Z

    move-result v0

    .line 562
    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->startTutorial(Lcom/squareup/tutorialv2/TutorialSeed$Priority;ZZ)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$FirstCardPaymentTutorialV2$Creator(Lkotlin/Unit;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 569
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getTutorialSettings()Lcom/squareup/settings/server/TutorialSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/TutorialSettings;->shouldAutoStartFirstPaymentTutorial()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$3$FirstCardPaymentTutorialV2$Creator(Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 573
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->startTutorial:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v0, Lcom/squareup/tutorialv2/TutorialSeed$Priority;->AUTO_STARTED:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 559
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->shouldStart()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 560
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->startTutorial:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->tutorialCogsHelper:Lcom/squareup/register/tutorial/TutorialCogsHelper;

    .line 561
    invoke-virtual {v1}, Lcom/squareup/register/tutorial/TutorialCogsHelper;->hasItems()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/register/tutorial/-$$Lambda$FirstCardPaymentTutorialV2$Creator$2ae4OenAZNmgnWp589JbvTHz_pc;

    invoke-direct {v1, p0}, Lcom/squareup/register/tutorial/-$$Lambda$FirstCardPaymentTutorialV2$Creator$2ae4OenAZNmgnWp589JbvTHz_pc;-><init>(Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;)V

    .line 562
    invoke-static {v1}, Lcom/squareup/util/Rx2Tuples;->expandPair(Lio/reactivex/functions/BiConsumer;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 560
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 566
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 567
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailable()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->appIdling:Lcom/squareup/ui/main/AppIdling;

    invoke-virtual {v1}, Lcom/squareup/ui/main/AppIdling;->onIdleChanged()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/register/tutorial/-$$Lambda$FirstCardPaymentTutorialV2$Creator$6uown1sWBWYez9-i6BqgHnG6PiI;

    invoke-direct {v2, p0}, Lcom/squareup/register/tutorial/-$$Lambda$FirstCardPaymentTutorialV2$Creator$6uown1sWBWYez9-i6BqgHnG6PiI;-><init>(Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;)V

    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/register/tutorial/-$$Lambda$FirstCardPaymentTutorialV2$Creator$xNceGagjZIqzOeYJel-FKt10v1M;->INSTANCE:Lcom/squareup/register/tutorial/-$$Lambda$FirstCardPaymentTutorialV2$Creator$xNceGagjZIqzOeYJel-FKt10v1M;

    .line 571
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 572
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/register/tutorial/-$$Lambda$FirstCardPaymentTutorialV2$Creator$w2DPJ6NeZA3FCieEiEc8SBcV7Nw;

    invoke-direct {v1, p0}, Lcom/squareup/register/tutorial/-$$Lambda$FirstCardPaymentTutorialV2$Creator$w2DPJ6NeZA3FCieEiEc8SBcV7Nw;-><init>(Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;)V

    .line 573
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 566
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    :cond_0
    return-void
.end method

.method public ready(Z)V
    .locals 1

    .line 578
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->startTutorial:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/tutorialv2/TutorialSeed$Priority;->AUTO_STARTED:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/tutorialv2/TutorialSeed$Priority;->MERCHANT_STARTED:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    :goto_0
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method startTutorial(Lcom/squareup/tutorialv2/TutorialSeed$Priority;ZZ)V
    .locals 2

    .line 583
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator$Seed;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator$Seed;-><init>(Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;Lcom/squareup/tutorialv2/TutorialSeed$Priority;ZZ)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public triggeredTutorial()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tutorialv2/TutorialSeed;",
            ">;"
        }
    .end annotation

    .line 592
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method
