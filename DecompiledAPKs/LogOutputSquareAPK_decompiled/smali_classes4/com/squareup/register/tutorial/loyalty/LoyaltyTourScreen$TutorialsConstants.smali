.class public interface abstract Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$TutorialsConstants;
.super Ljava/lang/Object;
.source "LoyaltyTourScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TutorialsConstants"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$TutorialsConstants$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0008f\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$TutorialsConstants;",
        "",
        "Companion",
        "pos-tutorials_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADJUSTPOINTS_LEAVING:Ljava/lang/String; = "Leaving LoyaltyTourScreen - Adjust Points"

.field public static final ADJUSTPOINTS_SHOWN:Ljava/lang/String; = "Shown LoyaltyTourScreen - Adjust Points"

.field public static final Companion:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$TutorialsConstants$Companion;

.field public static final ENROLL_LEAVING:Ljava/lang/String; = "Leaving LoyaltyTourScreen - Enroll"

.field public static final ENROLL_SHOWN:Ljava/lang/String; = "Shown LoyaltyTourScreen - Enroll"

.field public static final REDEMPTION_LEAVING:Ljava/lang/String; = "Leaving LoyaltyTourScreen - Redemption"

.field public static final REDEMPTION_SHOWN:Ljava/lang/String; = "Shown LoyaltyTourScreen - Redemption"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$TutorialsConstants$Companion;->$$INSTANCE:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$TutorialsConstants$Companion;

    sput-object v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$TutorialsConstants;->Companion:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$TutorialsConstants$Companion;

    return-void
.end method
