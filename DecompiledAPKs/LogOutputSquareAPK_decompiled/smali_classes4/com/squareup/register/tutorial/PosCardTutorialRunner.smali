.class public Lcom/squareup/register/tutorial/PosCardTutorialRunner;
.super Ljava/lang/Object;
.source "PosCardTutorialRunner.java"

# interfaces
.implements Lcom/squareup/register/tutorial/CardTutorialRunner;


# instance fields
.field private final badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

.field private final bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

.field private final bankLinkingStarter:Lcom/squareup/banklinking/BankLinkingStarter;

.field private feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

.field private final flow:Lflow/Flow;

.field private final subs:Lio/reactivex/disposables/CompositeDisposable;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/banklinking/BankLinkingStarter;Lcom/squareup/feetutorial/FeeTutorial;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    .line 33
    iput-object p1, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->flow:Lflow/Flow;

    .line 34
    iput-object p2, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    .line 35
    iput-object p3, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    .line 36
    iput-object p4, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->bankLinkingStarter:Lcom/squareup/banklinking/BankLinkingStarter;

    .line 37
    iput-object p5, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

    return-void
.end method

.method private createFinishPrompt()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;",
            ">;"
        }
    .end annotation

    .line 81
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-interface {v0}, Lcom/squareup/banklinking/BankAccountSettings;->state()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/register/tutorial/-$$Lambda$PosCardTutorialRunner$IFAApnuZtQWnpTthEzDPQJX2oKI;

    invoke-direct {v1, p0}, Lcom/squareup/register/tutorial/-$$Lambda$PosCardTutorialRunner$IFAApnuZtQWnpTthEzDPQJX2oKI;-><init>(Lcom/squareup/register/tutorial/PosCardTutorialRunner;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method private createNoActionFinishPrompt(I)Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;
    .locals 8

    .line 117
    new-instance v7, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    sget-object v1, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;->FINISHED:Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    sget v2, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_title:I

    sget v4, Lcom/squareup/common/strings/R$string;->done:I

    sget v6, Lcom/squareup/common/tutorial/R$drawable;->tutorial_lifepreserver:I

    const/4 v5, -0x1

    move-object v0, v7

    move v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;-><init>(Ljava/lang/Object;IIIII)V

    return-object v7
.end method

.method private fromBankSettingsState(Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;
    .locals 8

    .line 86
    invoke-virtual {p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->isSuccessful()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    sget p1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_content_verified:I

    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->createNoActionFinishPrompt(I)Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    move-result-object p1

    return-object p1

    .line 89
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->showLinkBankAccount()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    new-instance p1, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    sget-object v2, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;->FINISHED_LINK_BANK:Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    sget v3, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_title:I

    sget v4, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_content_no_bank:I

    sget v5, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_link_bank_account:I

    sget v6, Lcom/squareup/common/strings/R$string;->done:I

    sget v7, Lcom/squareup/common/tutorial/R$drawable;->tutorial_lifepreserver:I

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;-><init>(Ljava/lang/Object;IIIII)V

    return-object p1

    .line 97
    :cond_1
    sget-object v0, Lcom/squareup/register/tutorial/PosCardTutorialRunner$1;->$SwitchMap$com$squareup$protos$client$bankaccount$BankAccountVerificationState:[I

    invoke-virtual {p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->verificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 111
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown bank account verification state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    invoke-virtual {p1}, Lcom/squareup/banklinking/BankAccountSettings$State;->verificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :pswitch_0
    sget p1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_content_failed_verify:I

    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->createNoActionFinishPrompt(I)Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    move-result-object p1

    return-object p1

    .line 104
    :pswitch_1
    sget p1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_content_pending:I

    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->createNoActionFinishPrompt(I)Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    move-result-object p1

    return-object p1

    .line 99
    :pswitch_2
    sget p1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_content_verified:I

    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->createNoActionFinishPrompt(I)Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static synthetic lambda$IFAApnuZtQWnpTthEzDPQJX2oKI(Lcom/squareup/register/tutorial/PosCardTutorialRunner;Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->fromBankSettingsState(Lcom/squareup/banklinking/BankAccountSettings$State;)Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public synthetic lambda$showFinishDialog$0$PosCardTutorialRunner(Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;

    iget-object v2, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    .line 62
    invoke-interface {v2}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v2

    invoke-direct {v1, p1, v2}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;-><init>(Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;Z)V

    .line 60
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public linkBank()V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->bankLinkingStarter:Lcom/squareup/banklinking/BankLinkingStarter;

    invoke-interface {v0}, Lcom/squareup/banklinking/BankLinkingStarter;->maybeStartBankLinking()V

    return-void
.end method

.method public maybeShowFeeTutorial()Z
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

    invoke-interface {v0}, Lcom/squareup/feetutorial/FeeTutorial;->activate()V

    const/4 v0, 0x1

    return v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method public showEarlyExitDialog()V
    .locals 10

    .line 69
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;

    new-instance v9, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;

    sget-object v3, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;->EARLY_EXIT:Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    sget v4, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_exit_title:I

    sget v5, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_exit_content:I

    sget v6, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_exit_continue:I

    sget v7, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_exit_end:I

    sget v8, Lcom/squareup/common/tutorial/R$drawable;->tutorial_lifepreserver:I

    move-object v2, v9

    invoke-direct/range {v2 .. v8}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;-><init>(Ljava/lang/Object;IIIII)V

    iget-object v2, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    .line 76
    invoke-interface {v2}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v2

    invoke-direct {v1, v9, v2}, Lcom/squareup/tutorialv2/TutorialV2DialogScreen;-><init>(Lcom/squareup/tutorialv2/TutorialV2DialogScreen$Prompt;Z)V

    .line 69
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showFinishDialog()V
    .locals 3

    .line 57
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    .line 58
    invoke-direct {p0}, Lcom/squareup/register/tutorial/PosCardTutorialRunner;->createFinishPrompt()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/register/tutorial/-$$Lambda$PosCardTutorialRunner$-VnLnr5PFp6m9TuluqhHuS4zmlI;

    invoke-direct {v2, p0}, Lcom/squareup/register/tutorial/-$$Lambda$PosCardTutorialRunner$-VnLnr5PFp6m9TuluqhHuS4zmlI;-><init>(Lcom/squareup/register/tutorial/PosCardTutorialRunner;)V

    .line 59
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 57
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method
