.class public Lcom/squareup/register/widgets/ScrollToBottomView;
.super Landroid/widget/ScrollView;
.source "ScrollToBottomView.java"


# instance fields
.field private curHeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 14
    iput p1, p0, Lcom/squareup/register/widgets/ScrollToBottomView;->curHeight:I

    return-void
.end method

.method private goDown(I)V
    .locals 1

    const/4 v0, 0x0

    .line 38
    invoke-virtual {p0, v0, p1}, Lcom/squareup/register/widgets/ScrollToBottomView;->scrollTo(II)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onFinishInflate$0$ScrollToBottomView(Landroid/view/View;II)V
    .locals 0

    .line 23
    iput p3, p0, Lcom/squareup/register/widgets/ScrollToBottomView;->curHeight:I

    .line 24
    invoke-direct {p0, p3}, Lcom/squareup/register/widgets/ScrollToBottomView;->goDown(I)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 21
    invoke-super {p0}, Landroid/widget/ScrollView;->onFinishInflate()V

    .line 22
    new-instance v0, Lcom/squareup/register/widgets/-$$Lambda$ScrollToBottomView$VNeyQfQcONf8VseSS0C_UVJ_xF4;

    invoke-direct {v0, p0}, Lcom/squareup/register/widgets/-$$Lambda$ScrollToBottomView$VNeyQfQcONf8VseSS0C_UVJ_xF4;-><init>(Lcom/squareup/register/widgets/ScrollToBottomView;)V

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 31
    iget p1, p0, Lcom/squareup/register/widgets/ScrollToBottomView;->curHeight:I

    if-le p2, p1, :cond_0

    .line 32
    invoke-direct {p0, p2}, Lcom/squareup/register/widgets/ScrollToBottomView;->goDown(I)V

    .line 34
    :cond_0
    iput p2, p0, Lcom/squareup/register/widgets/ScrollToBottomView;->curHeight:I

    return-void
.end method
