.class final Lcom/squareup/register/widgets/ConfirmationStrings$1;
.super Ljava/lang/Object;
.source "ConfirmationStrings.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/ConfirmationStrings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/register/widgets/ConfirmationStrings;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/register/widgets/ConfirmationStrings;
    .locals 4

    .line 70
    new-instance v0, Lcom/squareup/register/widgets/ConfirmationStrings;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/register/widgets/ConfirmationStrings;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 68
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/ConfirmationStrings$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/register/widgets/ConfirmationStrings;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/register/widgets/ConfirmationStrings;
    .locals 0

    .line 75
    new-array p1, p1, [Lcom/squareup/register/widgets/ConfirmationStrings;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 68
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/ConfirmationStrings$1;->newArray(I)[Lcom/squareup/register/widgets/ConfirmationStrings;

    move-result-object p1

    return-object p1
.end method
