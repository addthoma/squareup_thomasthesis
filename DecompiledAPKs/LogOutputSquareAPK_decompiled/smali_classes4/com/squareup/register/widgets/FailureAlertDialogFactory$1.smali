.class final Lcom/squareup/register/widgets/FailureAlertDialogFactory$1;
.super Ljava/lang/Object;
.source "FailureAlertDialogFactory.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/FailureAlertDialogFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;
    .locals 7

    .line 137
    new-instance v6, Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 138
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/register/widgets/FailureAlertDialogFactory$1;)V

    return-object v6
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 135
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/FailureAlertDialogFactory$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/register/widgets/FailureAlertDialogFactory;
    .locals 0

    .line 142
    new-array p1, p1, [Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 135
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/FailureAlertDialogFactory$1;->newArray(I)[Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object p1

    return-object p1
.end method
