.class public Lcom/squareup/register/widgets/TopSquarePortraitLayout;
.super Landroid/view/ViewGroup;
.source "TopSquarePortraitLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 2

    const/4 p1, 0x0

    .line 71
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/TopSquarePortraitLayout;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    const/4 p2, 0x1

    .line 72
    invoke-virtual {p0, p2}, Lcom/squareup/register/widgets/TopSquarePortraitLayout;->getChildAt(I)Landroid/view/View;

    move-result-object p3

    .line 74
    invoke-virtual {p0}, Lcom/squareup/register/widgets/TopSquarePortraitLayout;->getMeasuredWidth()I

    move-result p4

    .line 77
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p5

    sub-int/2addr p4, p5

    div-int/lit8 p4, p4, 0x2

    .line 78
    invoke-virtual {p0}, Lcom/squareup/register/widgets/TopSquarePortraitLayout;->getPaddingTop()I

    move-result p5

    .line 79
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, p4

    .line 80
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, p5

    .line 81
    invoke-virtual {p1, p4, p5, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 84
    invoke-virtual {p0}, Lcom/squareup/register/widgets/TopSquarePortraitLayout;->getPaddingLeft()I

    move-result p1

    add-int/2addr v1, p2

    .line 86
    invoke-virtual {p3}, Landroid/view/View;->getMeasuredWidth()I

    move-result p2

    add-int/2addr p2, p1

    .line 87
    invoke-virtual {p3}, Landroid/view/View;->getMeasuredHeight()I

    move-result p4

    add-int/2addr p4, v1

    .line 89
    invoke-virtual {p3, p1, v1, p2, p4}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    .line 34
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 36
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    .line 37
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    .line 39
    invoke-virtual {p0}, Lcom/squareup/register/widgets/TopSquarePortraitLayout;->getPaddingLeft()I

    move-result v0

    .line 40
    invoke-virtual {p0}, Lcom/squareup/register/widgets/TopSquarePortraitLayout;->getPaddingTop()I

    move-result v1

    .line 41
    invoke-virtual {p0}, Lcom/squareup/register/widgets/TopSquarePortraitLayout;->getPaddingRight()I

    move-result v2

    .line 42
    invoke-virtual {p0}, Lcom/squareup/register/widgets/TopSquarePortraitLayout;->getPaddingBottom()I

    move-result v3

    const/4 v4, 0x0

    .line 44
    invoke-virtual {p0, v4}, Lcom/squareup/register/widgets/TopSquarePortraitLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x1

    .line 45
    invoke-virtual {p0, v6}, Lcom/squareup/register/widgets/TopSquarePortraitLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    sub-int v0, p1, v0

    sub-int/2addr v0, v2

    const/high16 v2, 0x40000000    # 2.0f

    .line 49
    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    sub-int v1, p2, v1

    sub-int/2addr v1, v3

    .line 51
    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 52
    invoke-virtual {v6, v7, v3}, Landroid/view/View;->measure(II)V

    .line 55
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v1, v3

    .line 57
    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 58
    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 59
    invoke-virtual {v5, v3, v3}, Landroid/view/View;->measure(II)V

    sub-int/2addr v1, v0

    .line 63
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    if-eq v1, v0, :cond_0

    .line 65
    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 64
    invoke-virtual {v6, v7, v0}, Landroid/view/View;->measure(II)V

    .line 67
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/register/widgets/TopSquarePortraitLayout;->setMeasuredDimension(II)V

    return-void
.end method
