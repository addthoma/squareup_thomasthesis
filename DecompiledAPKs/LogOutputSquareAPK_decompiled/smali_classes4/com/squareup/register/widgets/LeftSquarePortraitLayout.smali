.class public Lcom/squareup/register/widgets/LeftSquarePortraitLayout;
.super Landroid/view/ViewGroup;
.source "LeftSquarePortraitLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 2

    const/4 p1, 0x0

    .line 53
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/LeftSquarePortraitLayout;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    const/4 p2, 0x1

    .line 54
    invoke-virtual {p0, p2}, Lcom/squareup/register/widgets/LeftSquarePortraitLayout;->getChildAt(I)Landroid/view/View;

    move-result-object p2

    .line 56
    invoke-virtual {p0}, Lcom/squareup/register/widgets/LeftSquarePortraitLayout;->getMeasuredHeight()I

    move-result p3

    .line 58
    invoke-virtual {p0}, Lcom/squareup/register/widgets/LeftSquarePortraitLayout;->getPaddingLeft()I

    move-result p4

    .line 59
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p5

    sub-int p5, p3, p5

    div-int/lit8 p5, p5, 0x2

    .line 60
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, p4

    .line 61
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, p5

    .line 62
    invoke-virtual {p1, p4, p5, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 64
    invoke-virtual {p2}, Landroid/view/View;->getPaddingLeft()I

    move-result p1

    add-int/2addr p1, v0

    .line 65
    div-int/lit8 p3, p3, 0x2

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result p4

    div-int/lit8 p4, p4, 0x2

    sub-int/2addr p3, p4

    .line 66
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result p4

    add-int/2addr v0, p4

    invoke-virtual {p2}, Landroid/view/View;->getPaddingRight()I

    move-result p4

    sub-int/2addr v0, p4

    .line 67
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result p4

    add-int/2addr p4, p3

    .line 69
    invoke-virtual {p2, p1, p3, v0, p4}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    .line 15
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 17
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    .line 18
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    .line 20
    invoke-virtual {p0}, Lcom/squareup/register/widgets/LeftSquarePortraitLayout;->getPaddingLeft()I

    move-result v0

    .line 21
    invoke-virtual {p0}, Lcom/squareup/register/widgets/LeftSquarePortraitLayout;->getPaddingTop()I

    move-result v1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/register/widgets/LeftSquarePortraitLayout;->getPaddingRight()I

    move-result v2

    .line 23
    invoke-virtual {p0}, Lcom/squareup/register/widgets/LeftSquarePortraitLayout;->getPaddingBottom()I

    move-result v3

    const/4 v4, 0x0

    .line 25
    invoke-virtual {p0, v4}, Lcom/squareup/register/widgets/LeftSquarePortraitLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x1

    .line 26
    invoke-virtual {p0, v6}, Lcom/squareup/register/widgets/LeftSquarePortraitLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    sub-int v1, p2, v1

    sub-int v7, v1, v0

    sub-int v0, p1, v0

    sub-int/2addr v0, v2

    .line 31
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/high16 v4, -0x80000000

    .line 32
    invoke-static {v7, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 33
    invoke-virtual {v6, v2, v4}, Landroid/view/View;->measure(II)V

    sub-int/2addr v1, v3

    .line 38
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    .line 39
    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 40
    invoke-virtual {v5, v3, v3}, Landroid/view/View;->measure(II)V

    sub-int/2addr v0, v1

    .line 44
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 45
    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v6, v0, v4}, Landroid/view/View;->measure(II)V

    .line 49
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/register/widgets/LeftSquarePortraitLayout;->setMeasuredDimension(II)V

    return-void
.end method
