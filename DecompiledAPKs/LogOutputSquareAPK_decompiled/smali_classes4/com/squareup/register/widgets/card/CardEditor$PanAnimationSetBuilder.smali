.class Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;
.super Ljava/lang/Object;
.source "CardEditor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/card/CardEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PanAnimationSetBuilder"
.end annotation


# instance fields
.field private duration:J

.field private in:Z

.field final synthetic this$0:Lcom/squareup/register/widgets/card/CardEditor;

.field private translate:Z


# direct methods
.method private constructor <init>(Lcom/squareup/register/widgets/card/CardEditor;)V
    .locals 0

    .line 687
    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/register/widgets/card/CardEditor;Lcom/squareup/register/widgets/card/CardEditor$1;)V
    .locals 0

    .line 687
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;-><init>(Lcom/squareup/register/widgets/card/CardEditor;)V

    return-void
.end method


# virtual methods
.method public build()Landroid/view/animation/Animation;
    .locals 14

    .line 693
    new-instance v0, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 694
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 695
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    iget-boolean v2, p0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->in:Z

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    const/high16 v2, 0x3f800000    # 1.0f

    :goto_0
    iget-boolean v5, p0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->in:Z

    if-eqz v5, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 696
    iget-wide v1, p0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->duration:J

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 697
    iget-boolean v1, p0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->translate:Z

    if-eqz v1, :cond_4

    .line 698
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    const/4 v6, 0x1

    iget-boolean v2, p0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->in:Z

    const/high16 v3, -0x40800000    # -1.0f

    if-eqz v2, :cond_2

    const/high16 v7, -0x40800000    # -1.0f

    goto :goto_2

    :cond_2
    const/4 v7, 0x0

    :goto_2
    const/4 v8, 0x1

    iget-boolean v2, p0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->in:Z

    if-eqz v2, :cond_3

    const/4 v9, 0x0

    goto :goto_3

    :cond_3
    const/high16 v9, -0x40800000    # -1.0f

    :goto_3
    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v5, v1

    invoke-direct/range {v5 .. v13}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    :cond_4
    return-object v0
.end method

.method public doNotTranslate()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;
    .locals 1

    const/4 v0, 0x0

    .line 737
    iput-boolean v0, p0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->translate:Z

    return-object p0
.end method

.method public immediate()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;
    .locals 2

    const-wide/16 v0, 0x0

    .line 727
    iput-wide v0, p0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->duration:J

    return-object p0
.end method

.method public in()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;
    .locals 1

    const/4 v0, 0x1

    .line 711
    iput-boolean v0, p0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->in:Z

    return-object p0
.end method

.method public out()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;
    .locals 1

    const/4 v0, 0x0

    .line 716
    iput-boolean v0, p0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->in:Z

    return-object p0
.end method

.method public overTime()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;
    .locals 5

    .line 721
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {v0}, Lcom/squareup/register/widgets/card/CardEditor;->access$1700(Lcom/squareup/register/widgets/card/CardEditor;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "invalid animation duration"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 722
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->this$0:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-static {v0}, Lcom/squareup/register/widgets/card/CardEditor;->access$1700(Lcom/squareup/register/widgets/card/CardEditor;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->duration:J

    return-object p0
.end method

.method public translate()Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;
    .locals 1

    const/4 v0, 0x1

    .line 732
    iput-boolean v0, p0, Lcom/squareup/register/widgets/card/CardEditor$PanAnimationSetBuilder;->translate:Z

    return-object p0
.end method
