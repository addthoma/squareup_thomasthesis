.class public Lcom/squareup/register/widgets/card/SquidThirdPartyGiftCardPanValidationModule;
.super Ljava/lang/Object;
.source "SquidThirdPartyGiftCardPanValidationModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideThirdPartyGiftCardPanValidationStrategy()Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 15
    new-instance v0, Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;-><init>(Z)V

    return-object v0
.end method
