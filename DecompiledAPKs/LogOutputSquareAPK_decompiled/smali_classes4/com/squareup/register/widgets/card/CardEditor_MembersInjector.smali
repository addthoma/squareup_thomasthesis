.class public final Lcom/squareup/register/widgets/card/CardEditor_MembersInjector;
.super Ljava/lang/Object;
.source "CardEditor_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/register/widgets/card/CardEditor;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor_MembersInjector;->currencyProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/register/widgets/card/CardEditor;",
            ">;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/register/widgets/card/CardEditor_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/register/widgets/card/CardEditor_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectCurrency(Lcom/squareup/register/widgets/card/CardEditor;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditor;->currency:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/register/widgets/card/CardEditor;)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditor_MembersInjector;->currencyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, v0}, Lcom/squareup/register/widgets/card/CardEditor_MembersInjector;->injectCurrency(Lcom/squareup/register/widgets/card/CardEditor;Lcom/squareup/protos/common/CurrencyCode;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/card/CardEditor_MembersInjector;->injectMembers(Lcom/squareup/register/widgets/card/CardEditor;)V

    return-void
.end method
