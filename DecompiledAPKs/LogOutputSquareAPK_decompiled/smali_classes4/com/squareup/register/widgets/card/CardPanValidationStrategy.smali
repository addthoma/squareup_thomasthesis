.class public Lcom/squareup/register/widgets/card/CardPanValidationStrategy;
.super Ljava/lang/Object;
.source "CardPanValidationStrategy.java"

# interfaces
.implements Lcom/squareup/register/widgets/card/PanValidationStrategy;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cvvValid(Ljava/lang/String;Lcom/squareup/Card$Brand;)Z
    .locals 0

    .line 91
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {p2}, Lcom/squareup/Card$Brand;->cvvLength()I

    move-result p2

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public determineCardNumberState(Ljava/lang/String;Lcom/squareup/Card$Brand;Z)Lcom/squareup/register/widgets/card/CardNumberState;
    .locals 3

    .line 112
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2022

    if-ne v0, v1, :cond_0

    .line 113
    sget-object p1, Lcom/squareup/register/widgets/card/CardNumberState;->VALID:Lcom/squareup/register/widgets/card/CardNumberState;

    return-object p1

    .line 116
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 117
    invoke-virtual {p2, v0}, Lcom/squareup/Card$Brand;->isMaximumNumberLength(I)Z

    move-result v1

    .line 118
    invoke-virtual {p2, v0}, Lcom/squareup/Card$Brand;->isValidNumberLength(I)Z

    move-result v2

    if-nez v0, :cond_1

    .line 121
    sget-object p1, Lcom/squareup/register/widgets/card/CardNumberState;->EMPTY:Lcom/squareup/register/widgets/card/CardNumberState;

    return-object p1

    :cond_1
    if-eqz v2, :cond_4

    if-eqz p3, :cond_2

    if-nez v1, :cond_2

    goto :goto_0

    .line 124
    :cond_2
    invoke-virtual {p2, p1}, Lcom/squareup/Card$Brand;->validateLuhnIfRequired(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 125
    sget-object p1, Lcom/squareup/register/widgets/card/CardNumberState;->VALID:Lcom/squareup/register/widgets/card/CardNumberState;

    return-object p1

    .line 127
    :cond_3
    sget-object p1, Lcom/squareup/register/widgets/card/CardNumberState;->INVALID:Lcom/squareup/register/widgets/card/CardNumberState;

    return-object p1

    .line 123
    :cond_4
    :goto_0
    sget-object p1, Lcom/squareup/register/widgets/card/CardNumberState;->PARTIAL:Lcom/squareup/register/widgets/card/CardNumberState;

    return-object p1
.end method

.method public expirationValid(Ljava/lang/String;)Z
    .locals 1

    .line 95
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public getBrand(Ljava/lang/String;)Lcom/squareup/Card$Brand;
    .locals 0

    .line 38
    invoke-static {p1}, Lcom/squareup/CardBrandGuesser;->guessBrand(Ljava/lang/CharSequence;)Lcom/squareup/Card$Brand;

    move-result-object p1

    return-object p1
.end method

.method public getDefaultGlyph(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 0

    .line 42
    invoke-static {p1}, Lcom/squareup/util/ProtoGlyphs;->unbrandedCard(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p1

    return-object p1
.end method

.method public getGlyph(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 0

    .line 83
    invoke-static {p1, p2}, Lcom/squareup/util/ProtoGlyphs;->card(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object p1

    return-object p1
.end method

.method public getInputType()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public getRestartImeAfterScrubChange()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isMaximumLength(Lcom/squareup/Card$Brand;I)Z
    .locals 0

    .line 135
    invoke-virtual {p1, p2}, Lcom/squareup/Card$Brand;->isMaximumNumberLength(I)Z

    move-result p1

    return p1
.end method

.method public panCharacterValid(C)Z
    .locals 0

    .line 99
    invoke-static {p1}, Ljava/lang/Character;->isDigit(C)Z

    move-result p1

    return p1
.end method

.method public postalValid(ZLjava/lang/String;)Z
    .locals 0

    if-nez p1, :cond_1

    .line 87
    invoke-static {p2}, Lcom/squareup/text/CardPostalScrubber;->isValid(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public validateAndBuildCard(Ljava/lang/String;Lcom/squareup/giftcard/GiftCards;)Lcom/squareup/Card;
    .locals 1

    .line 25
    invoke-virtual {p2, p1}, Lcom/squareup/giftcard/GiftCards;->isSquareGiftCard(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    sget-object p2, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;->getBrand(Ljava/lang/String;)Lcom/squareup/Card$Brand;

    move-result-object p2

    .line 26
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/squareup/Card$Brand;->isValidNumberLength(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2, p1}, Lcom/squareup/Card$Brand;->validateLuhnIfRequired(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 27
    new-instance v0, Lcom/squareup/Card$Builder;

    invoke-direct {v0}, Lcom/squareup/Card$Builder;-><init>()V

    .line 28
    invoke-virtual {v0, p1}, Lcom/squareup/Card$Builder;->pan(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p1

    .line 29
    invoke-virtual {p1, p2}, Lcom/squareup/Card$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/Card$InputType;->MANUAL:Lcom/squareup/Card$InputType;

    .line 30
    invoke-virtual {p1, p2}, Lcom/squareup/Card$Builder;->inputType(Lcom/squareup/Card$InputType;)Lcom/squareup/Card$Builder;

    move-result-object p1

    .line 31
    invoke-virtual {p1}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public validateAndContinueBuildingCard(Lcom/squareup/Card;Lcom/squareup/giftcard/GiftCards;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/squareup/Card;
    .locals 1

    .line 49
    invoke-virtual {p1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object p2

    sget-object v0, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    if-ne p2, v0, :cond_0

    .line 50
    new-instance p2, Lcom/squareup/Card$Builder;

    invoke-direct {p2}, Lcom/squareup/Card$Builder;-><init>()V

    .line 51
    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/Card$Builder;->pan(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    .line 52
    invoke-virtual {p1, p2}, Lcom/squareup/Card$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;

    move-result-object p1

    const-string p2, "12"

    .line 53
    invoke-virtual {p1, p2}, Lcom/squareup/Card$Builder;->expirationMonth(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p1

    const-string p2, "99"

    .line 54
    invoke-virtual {p1, p2}, Lcom/squareup/Card$Builder;->expirationYear(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/Card$InputType;->MANUAL:Lcom/squareup/Card$InputType;

    .line 55
    invoke-virtual {p1, p2}, Lcom/squareup/Card$Builder;->inputType(Lcom/squareup/Card$InputType;)Lcom/squareup/Card$Builder;

    move-result-object p1

    .line 56
    invoke-virtual {p1}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object p1

    return-object p1

    .line 59
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object p2

    .line 60
    invoke-virtual {p0, p3, p2}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;->cvvValid(Ljava/lang/String;Lcom/squareup/Card$Brand;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p6, p4}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;->postalValid(ZLjava/lang/String;)Z

    move-result p6

    if-eqz p6, :cond_2

    .line 61
    invoke-virtual {p0, p5}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;->expirationValid(Ljava/lang/String;)Z

    move-result p6

    if-eqz p6, :cond_2

    .line 62
    new-instance p6, Lcom/squareup/Card$Builder;

    invoke-direct {p6}, Lcom/squareup/Card$Builder;-><init>()V

    .line 63
    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p6, p1}, Lcom/squareup/Card$Builder;->pan(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p1

    .line 64
    invoke-virtual {p1, p2}, Lcom/squareup/Card$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;

    move-result-object p1

    const/4 p2, 0x0

    const/4 p6, 0x2

    .line 65
    invoke-virtual {p5, p2, p6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/Card$Builder;->expirationMonth(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p1

    const/4 p2, 0x4

    .line 66
    invoke-virtual {p5, p6, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/Card$Builder;->expirationYear(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p1

    sget-object p2, Lcom/squareup/Card$InputType;->MANUAL:Lcom/squareup/Card$InputType;

    .line 67
    invoke-virtual {p1, p2}, Lcom/squareup/Card$Builder;->inputType(Lcom/squareup/Card$InputType;)Lcom/squareup/Card$Builder;

    move-result-object p1

    .line 68
    invoke-virtual {p1, p3}, Lcom/squareup/Card$Builder;->verification(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p1

    .line 69
    invoke-static {p4}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_1

    .line 70
    invoke-virtual {p1, p4}, Lcom/squareup/Card$Builder;->postalCode(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    .line 72
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object p1

    return-object p1

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method public validateOnlyPan()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
