.class public Lcom/squareup/register/widgets/ConfirmationIds;
.super Ljava/lang/Object;
.source "ConfirmationIds.java"

# interfaces
.implements Lcom/squareup/register/widgets/Confirmation;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/register/widgets/ConfirmationIds;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final bodyResId:I

.field private final cancelResId:I

.field private final confirmResId:I

.field private final titleResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 59
    new-instance v0, Lcom/squareup/register/widgets/ConfirmationIds$1;

    invoke-direct {v0}, Lcom/squareup/register/widgets/ConfirmationIds$1;-><init>()V

    sput-object v0, Lcom/squareup/register/widgets/ConfirmationIds;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput p1, p0, Lcom/squareup/register/widgets/ConfirmationIds;->titleResId:I

    .line 14
    iput p2, p0, Lcom/squareup/register/widgets/ConfirmationIds;->bodyResId:I

    .line 15
    iput p3, p0, Lcom/squareup/register/widgets/ConfirmationIds;->confirmResId:I

    .line 16
    iput p4, p0, Lcom/squareup/register/widgets/ConfirmationIds;->cancelResId:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_6

    .line 31
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_0

    .line 33
    :cond_1
    check-cast p1, Lcom/squareup/register/widgets/ConfirmationIds;

    .line 35
    iget v2, p0, Lcom/squareup/register/widgets/ConfirmationIds;->bodyResId:I

    iget v3, p1, Lcom/squareup/register/widgets/ConfirmationIds;->bodyResId:I

    if-eq v2, v3, :cond_2

    return v1

    .line 36
    :cond_2
    iget v2, p0, Lcom/squareup/register/widgets/ConfirmationIds;->cancelResId:I

    iget v3, p1, Lcom/squareup/register/widgets/ConfirmationIds;->cancelResId:I

    if-eq v2, v3, :cond_3

    return v1

    .line 37
    :cond_3
    iget v2, p0, Lcom/squareup/register/widgets/ConfirmationIds;->confirmResId:I

    iget v3, p1, Lcom/squareup/register/widgets/ConfirmationIds;->confirmResId:I

    if-eq v2, v3, :cond_4

    return v1

    .line 38
    :cond_4
    iget v2, p0, Lcom/squareup/register/widgets/ConfirmationIds;->titleResId:I

    iget p1, p1, Lcom/squareup/register/widgets/ConfirmationIds;->titleResId:I

    if-eq v2, p1, :cond_5

    return v1

    :cond_5
    return v0

    :cond_6
    :goto_0
    return v1
.end method

.method public getStrings(Landroid/content/res/Resources;)Lcom/squareup/register/widgets/Confirmation$Strings;
    .locals 5

    .line 20
    new-instance v0, Lcom/squareup/register/widgets/Confirmation$Strings;

    iget v1, p0, Lcom/squareup/register/widgets/ConfirmationIds;->titleResId:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/squareup/register/widgets/ConfirmationIds;->bodyResId:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/squareup/register/widgets/ConfirmationIds;->confirmResId:I

    .line 21
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/squareup/register/widgets/ConfirmationIds;->cancelResId:I

    invoke-virtual {p1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/register/widgets/Confirmation$Strings;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 45
    iget v0, p0, Lcom/squareup/register/widgets/ConfirmationIds;->titleResId:I

    mul-int/lit8 v0, v0, 0x1f

    .line 46
    iget v1, p0, Lcom/squareup/register/widgets/ConfirmationIds;->bodyResId:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 47
    iget v1, p0, Lcom/squareup/register/widgets/ConfirmationIds;->confirmResId:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 48
    iget v1, p0, Lcom/squareup/register/widgets/ConfirmationIds;->cancelResId:I

    add-int/2addr v0, v1

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 53
    iget p2, p0, Lcom/squareup/register/widgets/ConfirmationIds;->titleResId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 54
    iget p2, p0, Lcom/squareup/register/widgets/ConfirmationIds;->bodyResId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 55
    iget p2, p0, Lcom/squareup/register/widgets/ConfirmationIds;->confirmResId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 56
    iget p2, p0, Lcom/squareup/register/widgets/ConfirmationIds;->cancelResId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
