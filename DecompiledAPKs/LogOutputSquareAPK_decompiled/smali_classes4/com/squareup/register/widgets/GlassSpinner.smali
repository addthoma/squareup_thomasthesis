.class public Lcom/squareup/register/widgets/GlassSpinner;
.super Ljava/lang/Object;
.source "GlassSpinner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/GlassSpinner$GlassSpinnerDisplayState;
    }
.end annotation


# static fields
.field public static final DEBOUNCED_DELAY_IN_MILLIS:J = 0x1f4L

.field public static final MIN_SPINNER_TIME:J = 0x320L

.field public static final MIN_SPINNER_TIME_UNIT:Ljava/util/concurrent/TimeUnit;


# instance fields
.field private debounceDialog:Landroid/app/Dialog;

.field private delayedRunnable:Ljava/lang/Runnable;

.field private displayState:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/register/widgets/GlassSpinner$GlassSpinnerDisplayState;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final showing:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/register/widgets/GlassSpinnerState;",
            ">;"
        }
    .end annotation
.end field

.field private spinnerDialog:Landroid/app/Dialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 72
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sput-object v0, Lcom/squareup/register/widgets/GlassSpinner;->MIN_SPINNER_TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/thread/executor/MainThread;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState$Hide;->INSTANCE:Lcom/squareup/register/widgets/GlassSpinnerState$Hide;

    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->showing:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 83
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinner$GlassSpinnerDisplayState;->DISMISSED:Lcom/squareup/register/widgets/GlassSpinner$GlassSpinnerDisplayState;

    .line 84
    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->create(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->displayState:Lrx/subjects/BehaviorSubject;

    .line 87
    iput-object p1, p0, Lcom/squareup/register/widgets/GlassSpinner;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 88
    iput-object p2, p0, Lcom/squareup/register/widgets/GlassSpinner;->mainScheduler:Lio/reactivex/Scheduler;

    return-void
.end method

.method private cancelShowingSpinner()V
    .locals 2

    .line 290
    iget-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->delayedRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 291
    iget-object v1, p0, Lcom/squareup/register/widgets/GlassSpinner;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-interface {v1, v0}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    .line 292
    iput-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->delayedRunnable:Ljava/lang/Runnable;

    :cond_0
    return-void
.end method

.method public static createGlassSpinnerDialog(Landroid/content/Context;)Lcom/squareup/dialog/GlassDialog;
    .locals 3

    .line 98
    new-instance v0, Lcom/squareup/dialog/GlassDialog;

    sget v1, Lcom/squareup/noho/R$style;->Theme_Noho_Dialog_NoBackground:I

    invoke-direct {v0, p0, v1}, Lcom/squareup/dialog/GlassDialog;-><init>(Landroid/content/Context;I)V

    .line 101
    invoke-static {}, Lcom/squareup/util/Dialogs;->ignoresSearchKeyListener()Landroid/content/DialogInterface$OnKeyListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/dialog/GlassDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 103
    sget v1, Lcom/squareup/widgets/pos/R$layout;->smoked_glass_dialog:I

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p0

    .line 104
    invoke-virtual {v0, p0}, Lcom/squareup/dialog/GlassDialog;->setContentView(Landroid/view/View;)V

    const/4 p0, 0x0

    .line 105
    invoke-virtual {v0, p0}, Lcom/squareup/dialog/GlassDialog;->setCancelable(Z)V

    .line 107
    invoke-virtual {v0}, Lcom/squareup/dialog/GlassDialog;->getWindow()Landroid/view/Window;

    move-result-object p0

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    return-object v0
.end method

.method private hideSpinner(Landroid/content/Context;)V
    .locals 3

    .line 276
    iget-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->spinnerDialog:Landroid/app/Dialog;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 277
    invoke-static {p1}, Lcom/squareup/ui/DialogDestroyer;->get(Landroid/content/Context;)Lcom/squareup/ui/DialogDestroyer;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/register/widgets/GlassSpinner;->spinnerDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/DialogDestroyer;->dismiss(Landroid/app/Dialog;)V

    .line 278
    iput-object v1, p0, Lcom/squareup/register/widgets/GlassSpinner;->spinnerDialog:Landroid/app/Dialog;

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->debounceDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 282
    invoke-static {p1}, Lcom/squareup/ui/DialogDestroyer;->get(Landroid/content/Context;)Lcom/squareup/ui/DialogDestroyer;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->debounceDialog:Landroid/app/Dialog;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/DialogDestroyer;->dismiss(Landroid/app/Dialog;)V

    .line 283
    iput-object v1, p0, Lcom/squareup/register/widgets/GlassSpinner;->debounceDialog:Landroid/app/Dialog;

    .line 286
    :cond_1
    iget-object p1, p0, Lcom/squareup/register/widgets/GlassSpinner;->displayState:Lrx/subjects/BehaviorSubject;

    sget-object v0, Lcom/squareup/register/widgets/GlassSpinner$GlassSpinnerDisplayState;->DISMISSED:Lcom/squareup/register/widgets/GlassSpinner$GlassSpinnerDisplayState;

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private scheduleSpinner(ILandroid/content/Context;)V
    .locals 2

    .line 266
    invoke-direct {p0}, Lcom/squareup/register/widgets/GlassSpinner;->cancelShowingSpinner()V

    .line 267
    new-instance v0, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$Ny0ldZUyLW9LlsMS68j4E3NL_60;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$Ny0ldZUyLW9LlsMS68j4E3NL_60;-><init>(Lcom/squareup/register/widgets/GlassSpinner;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->delayedRunnable:Ljava/lang/Runnable;

    .line 272
    iget-object p1, p0, Lcom/squareup/register/widgets/GlassSpinner;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object p2, p0, Lcom/squareup/register/widgets/GlassSpinner;->delayedRunnable:Ljava/lang/Runnable;

    const-wide/16 v0, 0x1f4

    invoke-interface {p1, p2, v0, v1}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private setTextInDialog(Landroid/view/View;I)V
    .locals 1

    .line 340
    sget v0, Lcom/squareup/widgets/pos/R$id;->message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const-string p2, ""

    .line 342
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 344
    :cond_0
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void
.end method

.method private showDebouncedDialog(Landroid/content/Context;)V
    .locals 2

    .line 305
    iget-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->debounceDialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 306
    new-instance v0, Lcom/squareup/dialog/GlassDialog;

    sget v1, Lcom/squareup/noho/R$style;->Theme_Noho_Dialog_NoBackground_NoDim:I

    invoke-direct {v0, p1, v1}, Lcom/squareup/dialog/GlassDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->debounceDialog:Landroid/app/Dialog;

    .line 308
    iget-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->debounceDialog:Landroid/app/Dialog;

    invoke-static {}, Lcom/squareup/util/Dialogs;->ignoresSearchKeyListener()Landroid/content/DialogInterface$OnKeyListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 309
    iget-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->debounceDialog:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 310
    iget-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->debounceDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    .line 312
    :cond_0
    invoke-static {p1}, Lcom/squareup/ui/DialogDestroyer;->get(Landroid/content/Context;)Lcom/squareup/ui/DialogDestroyer;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->debounceDialog:Landroid/app/Dialog;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/DialogDestroyer;->show(Landroid/app/Dialog;)V

    .line 313
    iget-object p1, p0, Lcom/squareup/register/widgets/GlassSpinner;->displayState:Lrx/subjects/BehaviorSubject;

    sget-object v0, Lcom/squareup/register/widgets/GlassSpinner$GlassSpinnerDisplayState;->BLOCKING:Lcom/squareup/register/widgets/GlassSpinner$GlassSpinnerDisplayState;

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private showSpinnerDialog(ILandroid/content/Context;)V
    .locals 2

    .line 320
    iget-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->spinnerDialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 321
    invoke-static {p2}, Lcom/squareup/register/widgets/GlassSpinner;->createGlassSpinnerDialog(Landroid/content/Context;)Lcom/squareup/dialog/GlassDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->spinnerDialog:Landroid/app/Dialog;

    .line 323
    invoke-static {p2}, Lcom/squareup/sqwidgets/DialogType;->forContext(Landroid/content/Context;)Lcom/squareup/sqwidgets/DialogType;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/register/widgets/GlassSpinner;->spinnerDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/sqwidgets/DialogType;->applyTypeForWindow(Landroid/view/Window;)V

    .line 324
    iget-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->spinnerDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 325
    invoke-direct {p0, v0, p1}, Lcom/squareup/register/widgets/GlassSpinner;->setTextInDialog(Landroid/view/View;I)V

    .line 329
    new-instance p1, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$sCnRS5eyRdSkVsMRXL08mgr9qB8;

    invoke-direct {p1, p0}, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$sCnRS5eyRdSkVsMRXL08mgr9qB8;-><init>(Lcom/squareup/register/widgets/GlassSpinner;)V

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->onDetach(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 331
    :cond_0
    invoke-static {p2}, Lcom/squareup/ui/DialogDestroyer;->get(Landroid/content/Context;)Lcom/squareup/ui/DialogDestroyer;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/register/widgets/GlassSpinner;->spinnerDialog:Landroid/app/Dialog;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/DialogDestroyer;->show(Landroid/app/Dialog;)V

    .line 332
    iget-object p1, p0, Lcom/squareup/register/widgets/GlassSpinner;->displayState:Lrx/subjects/BehaviorSubject;

    sget-object p2, Lcom/squareup/register/widgets/GlassSpinner$GlassSpinnerDisplayState;->SPINNING:Lcom/squareup/register/widgets/GlassSpinner$GlassSpinnerDisplayState;

    invoke-virtual {p1, p2}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private updateShowingFromMainThread(Lcom/squareup/register/widgets/GlassSpinnerState;)V
    .locals 1

    .line 297
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 298
    iget-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->showing:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public clearSpinnerState(Landroid/content/Context;)V
    .locals 0

    .line 256
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 257
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/GlassSpinner;->hideSpinner(Landroid/content/Context;)V

    .line 258
    invoke-direct {p0}, Lcom/squareup/register/widgets/GlassSpinner;->cancelShowingSpinner()V

    return-void
.end method

.method public displayState()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/register/widgets/GlassSpinner$GlassSpinnerDisplayState;",
            ">;"
        }
    .end annotation

    .line 349
    iget-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->displayState:Lrx/subjects/BehaviorSubject;

    return-object v0
.end method

.method public synthetic lambda$null$0$GlassSpinner(ILio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 152
    new-instance p2, Lcom/squareup/register/widgets/GlassSpinnerState$Show;

    const/4 v0, 0x0

    invoke-direct {p2, v0, p1}, Lcom/squareup/register/widgets/GlassSpinnerState$Show;-><init>(ZI)V

    invoke-direct {p0, p2}, Lcom/squareup/register/widgets/GlassSpinner;->updateShowingFromMainThread(Lcom/squareup/register/widgets/GlassSpinnerState;)V

    return-void
.end method

.method public synthetic lambda$null$1$GlassSpinner()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 154
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState$Hide;->INSTANCE:Lcom/squareup/register/widgets/GlassSpinnerState$Hide;

    invoke-direct {p0, v0}, Lcom/squareup/register/widgets/GlassSpinner;->updateShowingFromMainThread(Lcom/squareup/register/widgets/GlassSpinnerState;)V

    return-void
.end method

.method public synthetic lambda$null$10$GlassSpinner(ILio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 202
    new-instance p2, Lcom/squareup/register/widgets/GlassSpinnerState$Show;

    const/4 v0, 0x0

    invoke-direct {p2, v0, p1}, Lcom/squareup/register/widgets/GlassSpinnerState$Show;-><init>(ZI)V

    invoke-direct {p0, p2}, Lcom/squareup/register/widgets/GlassSpinner;->updateShowingFromMainThread(Lcom/squareup/register/widgets/GlassSpinnerState;)V

    return-void
.end method

.method public synthetic lambda$null$11$GlassSpinner()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 204
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState$Hide;->INSTANCE:Lcom/squareup/register/widgets/GlassSpinnerState$Hide;

    invoke-direct {p0, v0}, Lcom/squareup/register/widgets/GlassSpinner;->updateShowingFromMainThread(Lcom/squareup/register/widgets/GlassSpinnerState;)V

    return-void
.end method

.method public synthetic lambda$null$12$GlassSpinner(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 205
    sget-object p1, Lcom/squareup/register/widgets/GlassSpinnerState$Hide;->INSTANCE:Lcom/squareup/register/widgets/GlassSpinnerState$Hide;

    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/GlassSpinner;->updateShowingFromMainThread(Lcom/squareup/register/widgets/GlassSpinnerState;)V

    return-void
.end method

.method public synthetic lambda$null$2$GlassSpinner(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 155
    sget-object p1, Lcom/squareup/register/widgets/GlassSpinnerState$Hide;->INSTANCE:Lcom/squareup/register/widgets/GlassSpinnerState$Hide;

    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/GlassSpinner;->updateShowingFromMainThread(Lcom/squareup/register/widgets/GlassSpinnerState;)V

    return-void
.end method

.method public synthetic lambda$null$4$GlassSpinner(Lrx/functions/Func1;Ljava/lang/Object;)V
    .locals 0

    .line 168
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 169
    invoke-interface {p1, p2}, Lrx/functions/Func1;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/GlassSpinnerState;

    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/GlassSpinner;->updateShowingFromMainThread(Lcom/squareup/register/widgets/GlassSpinnerState;)V

    return-void
.end method

.method public synthetic lambda$null$5$GlassSpinner()V
    .locals 1

    .line 170
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState$Hide;->INSTANCE:Lcom/squareup/register/widgets/GlassSpinnerState$Hide;

    invoke-direct {p0, v0}, Lcom/squareup/register/widgets/GlassSpinner;->updateShowingFromMainThread(Lcom/squareup/register/widgets/GlassSpinnerState;)V

    return-void
.end method

.method public synthetic lambda$null$7$GlassSpinner(Lrx/functions/Func1;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 177
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 178
    invoke-interface {p1, p2}, Lrx/functions/Func1;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/GlassSpinnerState;

    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/GlassSpinner;->updateShowingFromMainThread(Lcom/squareup/register/widgets/GlassSpinnerState;)V

    return-void
.end method

.method public synthetic lambda$null$8$GlassSpinner()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 179
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState$Hide;->INSTANCE:Lcom/squareup/register/widgets/GlassSpinnerState$Hide;

    invoke-direct {p0, v0}, Lcom/squareup/register/widgets/GlassSpinner;->updateShowingFromMainThread(Lcom/squareup/register/widgets/GlassSpinnerState;)V

    return-void
.end method

.method public synthetic lambda$scheduleSpinner$16$GlassSpinner(Landroid/content/Context;I)V
    .locals 1

    .line 269
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/GlassSpinner;->hideSpinner(Landroid/content/Context;)V

    .line 270
    new-instance p1, Lcom/squareup/register/widgets/GlassSpinnerState$Show;

    const/4 v0, 0x0

    invoke-direct {p1, v0, p2}, Lcom/squareup/register/widgets/GlassSpinnerState$Show;-><init>(ZI)V

    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/GlassSpinner;->updateShowingFromMainThread(Lcom/squareup/register/widgets/GlassSpinnerState;)V

    return-void
.end method

.method public synthetic lambda$showOrHideSpinner$14$GlassSpinner(Landroid/content/Context;)V
    .locals 0

    .line 218
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/GlassSpinner;->clearSpinnerState(Landroid/content/Context;)V

    return-void
.end method

.method public synthetic lambda$showOrHideSpinner$15$GlassSpinner(Landroid/content/Context;Lcom/squareup/register/widgets/GlassSpinnerState;)V
    .locals 0

    .line 219
    invoke-virtual {p0, p1, p2}, Lcom/squareup/register/widgets/GlassSpinner;->setSpinnerState(Landroid/content/Context;Lcom/squareup/register/widgets/GlassSpinnerState;)V

    return-void
.end method

.method public synthetic lambda$showSpinnerDialog$17$GlassSpinner()V
    .locals 1

    const/4 v0, 0x0

    .line 329
    iput-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->spinnerDialog:Landroid/app/Dialog;

    return-void
.end method

.method public synthetic lambda$spinnerTransform$3$GlassSpinner(IJLjava/util/concurrent/TimeUnit;Lrx/Observable;)Lrx/Observable;
    .locals 1

    .line 151
    invoke-static {p5}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p5

    new-instance v0, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$vnD9oFyzCcoWGVJCYJeikt4X5A4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$vnD9oFyzCcoWGVJCYJeikt4X5A4;-><init>(Lcom/squareup/register/widgets/GlassSpinner;I)V

    .line 152
    invoke-virtual {p5, v0}, Lio/reactivex/Observable;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    iget-object p5, p0, Lcom/squareup/register/widgets/GlassSpinner;->mainScheduler:Lio/reactivex/Scheduler;

    .line 153
    invoke-static {p2, p3, p4, p5}, Lcom/squareup/util/rx2/Rx2TransformersKt;->delayEmission(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/ObservableTransformer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$hdlnzIyM79hXTOh6OCL284F4vao;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$hdlnzIyM79hXTOh6OCL284F4vao;-><init>(Lcom/squareup/register/widgets/GlassSpinner;)V

    .line 154
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$SKZHuWIMQBNH7wS_OSjtdbgt5HY;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$SKZHuWIMQBNH7wS_OSjtdbgt5HY;-><init>(Lcom/squareup/register/widgets/GlassSpinner;)V

    .line 155
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    .line 156
    sget-object p2, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {p1, p2}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$spinnerTransform$6$GlassSpinner(Lrx/functions/Func1;Lrx/Observable;)Lrx/Observable;
    .locals 1

    .line 166
    new-instance v0, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$WcbOLp9DlEVvJCYXuskYUUrH4lo;

    invoke-direct {v0, p0, p1}, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$WcbOLp9DlEVvJCYXuskYUUrH4lo;-><init>(Lcom/squareup/register/widgets/GlassSpinner;Lrx/functions/Func1;)V

    .line 167
    invoke-virtual {p2, v0}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$GRGkXTWQWo2-ik18xLjTDSZPzmg;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$GRGkXTWQWo2-ik18xLjTDSZPzmg;-><init>(Lcom/squareup/register/widgets/GlassSpinner;)V

    .line 170
    invoke-virtual {p1, p2}, Lrx/Observable;->doOnTerminate(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$spinnerTransformRx2$13$GlassSpinner(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1

    .line 201
    new-instance v0, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$mK66S2XnD6IFbrejPwSdCR-hTJQ;

    invoke-direct {v0, p0, p1}, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$mK66S2XnD6IFbrejPwSdCR-hTJQ;-><init>(Lcom/squareup/register/widgets/GlassSpinner;I)V

    .line 202
    invoke-virtual {p5, v0}, Lio/reactivex/Observable;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    iget-object p5, p0, Lcom/squareup/register/widgets/GlassSpinner;->mainScheduler:Lio/reactivex/Scheduler;

    .line 203
    invoke-static {p2, p3, p4, p5}, Lcom/squareup/util/rx2/Rx2TransformersKt;->delayEmission(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/ObservableTransformer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$dVwFrrU-z86HNk9d2SYT6VjbHkQ;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$dVwFrrU-z86HNk9d2SYT6VjbHkQ;-><init>(Lcom/squareup/register/widgets/GlassSpinner;)V

    .line 204
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$l9cydIbEYbfLoQOVawMTxYmtjkc;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$l9cydIbEYbfLoQOVawMTxYmtjkc;-><init>(Lcom/squareup/register/widgets/GlassSpinner;)V

    .line 205
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$spinnerTransformRx2$9$GlassSpinner(Lrx/functions/Func1;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1

    .line 175
    new-instance v0, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$CBzjhXEq47V3eWAqkK0M7QJY3JQ;

    invoke-direct {v0, p0, p1}, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$CBzjhXEq47V3eWAqkK0M7QJY3JQ;-><init>(Lcom/squareup/register/widgets/GlassSpinner;Lrx/functions/Func1;)V

    .line 176
    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$8ujn2OXuobG6x-J44cxZQm1JvG0;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$8ujn2OXuobG6x-J44cxZQm1JvG0;-><init>(Lcom/squareup/register/widgets/GlassSpinner;)V

    .line 179
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public setSpinnerState(Landroid/content/Context;Lcom/squareup/register/widgets/GlassSpinnerState;)V
    .locals 1

    .line 231
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 233
    instance-of v0, p2, Lcom/squareup/register/widgets/GlassSpinnerState$Hide;

    if-eqz v0, :cond_0

    .line 234
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/GlassSpinner;->clearSpinnerState(Landroid/content/Context;)V

    goto :goto_0

    .line 237
    :cond_0
    instance-of v0, p2, Lcom/squareup/register/widgets/GlassSpinnerState$Show;

    if-eqz v0, :cond_2

    .line 238
    check-cast p2, Lcom/squareup/register/widgets/GlassSpinnerState$Show;

    .line 239
    invoke-virtual {p2}, Lcom/squareup/register/widgets/GlassSpinnerState$Show;->getTextId()I

    move-result v0

    .line 240
    invoke-virtual {p2}, Lcom/squareup/register/widgets/GlassSpinnerState$Show;->getDebounced()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 241
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/GlassSpinner;->showDebouncedDialog(Landroid/content/Context;)V

    .line 242
    invoke-direct {p0, v0, p1}, Lcom/squareup/register/widgets/GlassSpinner;->scheduleSpinner(ILandroid/content/Context;)V

    goto :goto_0

    .line 244
    :cond_1
    invoke-direct {p0, v0, p1}, Lcom/squareup/register/widgets/GlassSpinner;->showSpinnerDialog(ILandroid/content/Context;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;
    .locals 2

    .line 218
    iget-object v0, p0, Lcom/squareup/register/widgets/GlassSpinner;->showing:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v1, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$8GZLmzWiUZPDSqBaRYA7I176N80;

    invoke-direct {v1, p0, p1}, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$8GZLmzWiUZPDSqBaRYA7I176N80;-><init>(Lcom/squareup/register/widgets/GlassSpinner;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->doOnUnsubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$Y3DgTb3CUYw4LWk2I3Z0b93lyzA;

    invoke-direct {v1, p0, p1}, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$Y3DgTb3CUYw4LWk2I3Z0b93lyzA;-><init>(Lcom/squareup/register/widgets/GlassSpinner;Landroid/content/Context;)V

    .line 219
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public spinnerTransform()Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lrx/Observable$Transformer<",
            "TT;TT;>;"
        }
    .end annotation

    const/4 v0, -0x1

    .line 117
    invoke-virtual {p0, v0}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransform(I)Lrx/Observable$Transformer;

    move-result-object v0

    return-object v0
.end method

.method public spinnerTransform(I)Lrx/Observable$Transformer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I)",
            "Lrx/Observable$Transformer<",
            "TT;TT;>;"
        }
    .end annotation

    .line 132
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x0

    invoke-virtual {p0, p1, v1, v2, v0}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransform(IJLjava/util/concurrent/TimeUnit;)Lrx/Observable$Transformer;

    move-result-object p1

    return-object p1
.end method

.method public spinnerTransform(IJLjava/util/concurrent/TimeUnit;)Lrx/Observable$Transformer;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(IJ",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Lrx/Observable$Transformer<",
            "TT;TT;>;"
        }
    .end annotation

    .line 150
    new-instance v6, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$5jRWbYc4fWNv4-AiKDBk0-HEL_Y;

    move-object v0, v6

    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$5jRWbYc4fWNv4-AiKDBk0-HEL_Y;-><init>(Lcom/squareup/register/widgets/GlassSpinner;IJLjava/util/concurrent/TimeUnit;)V

    return-object v6
.end method

.method public spinnerTransform(Lrx/functions/Func1;)Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/Func1<",
            "TT;",
            "Lcom/squareup/register/widgets/GlassSpinnerState;",
            ">;)",
            "Lrx/Observable$Transformer<",
            "TT;TT;>;"
        }
    .end annotation

    .line 166
    new-instance v0, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$Xm478d5wWLORMifumPGj84PuKh4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$Xm478d5wWLORMifumPGj84PuKh4;-><init>(Lcom/squareup/register/widgets/GlassSpinner;Lrx/functions/Func1;)V

    return-object v0
.end method

.method public spinnerTransformRx2()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/reactivex/ObservableTransformer<",
            "TT;TT;>;"
        }
    .end annotation

    const/4 v0, -0x1

    .line 193
    invoke-virtual {p0, v0}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransformRx2(I)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    return-object v0
.end method

.method public spinnerTransformRx2(I)Lio/reactivex/ObservableTransformer;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I)",
            "Lio/reactivex/ObservableTransformer<",
            "TT;TT;>;"
        }
    .end annotation

    .line 186
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x0

    invoke-virtual {p0, p1, v1, v2, v0}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransformRx2(IJLjava/util/concurrent/TimeUnit;)Lio/reactivex/ObservableTransformer;

    move-result-object p1

    return-object p1
.end method

.method public spinnerTransformRx2(IJLjava/util/concurrent/TimeUnit;)Lio/reactivex/ObservableTransformer;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(IJ",
            "Ljava/util/concurrent/TimeUnit;",
            ")",
            "Lio/reactivex/ObservableTransformer<",
            "TT;TT;>;"
        }
    .end annotation

    .line 201
    new-instance v6, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$bsJZrbyV6wxWOhyfdtSaPvE_eGQ;

    move-object v0, v6

    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$bsJZrbyV6wxWOhyfdtSaPvE_eGQ;-><init>(Lcom/squareup/register/widgets/GlassSpinner;IJLjava/util/concurrent/TimeUnit;)V

    return-object v6
.end method

.method public spinnerTransformRx2(Lrx/functions/Func1;)Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/functions/Func1<",
            "TT;",
            "Lcom/squareup/register/widgets/GlassSpinnerState;",
            ">;)",
            "Lio/reactivex/ObservableTransformer<",
            "TT;TT;>;"
        }
    .end annotation

    .line 175
    new-instance v0, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$5L1iiCcfYejmSQJTgaSmqbGwqgM;

    invoke-direct {v0, p0, p1}, Lcom/squareup/register/widgets/-$$Lambda$GlassSpinner$5L1iiCcfYejmSQJTgaSmqbGwqgM;-><init>(Lcom/squareup/register/widgets/GlassSpinner;Lrx/functions/Func1;)V

    return-object v0
.end method
