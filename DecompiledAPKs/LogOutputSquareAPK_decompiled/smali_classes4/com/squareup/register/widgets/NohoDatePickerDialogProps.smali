.class public final Lcom/squareup/register/widgets/NohoDatePickerDialogProps;
.super Ljava/lang/Object;
.source "NohoDatePickerDialogProps.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/NohoDatePickerDialogProps$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0015\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000  2\u00020\u0001:\u0001 B?\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0007H\u00c6\u0003JE\u0010\u0019\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00072\u0008\u0008\u0002\u0010\t\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u001a\u001a\u00020\u00072\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000fR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u000fR\u0011\u0010\t\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000c\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/register/widgets/NohoDatePickerDialogProps;",
        "",
        "currentDate",
        "Lorg/threeten/bp/LocalDate;",
        "minDate",
        "maxDate",
        "allowClear",
        "",
        "allowNoYear",
        "shouldShowYear",
        "(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;ZZZ)V",
        "getAllowClear",
        "()Z",
        "getAllowNoYear",
        "getCurrentDate",
        "()Lorg/threeten/bp/LocalDate;",
        "getMaxDate",
        "getMinDate",
        "getShouldShowYear",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/register/widgets/NohoDatePickerDialogProps$Companion;

.field public static final DEFAULT_LEAP_YEAR:I = 0x7d0

.field private static final DEFAULT_MAX_DATE:Lorg/threeten/bp/LocalDate;

.field private static final DEFAULT_MIN_DATE:Lorg/threeten/bp/LocalDate;


# instance fields
.field private final allowClear:Z

.field private final allowNoYear:Z

.field private final currentDate:Lorg/threeten/bp/LocalDate;

.field private final maxDate:Lorg/threeten/bp/LocalDate;

.field private final minDate:Lorg/threeten/bp/LocalDate;

.field private final shouldShowYear:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/register/widgets/NohoDatePickerDialogProps$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->Companion:Lcom/squareup/register/widgets/NohoDatePickerDialogProps$Companion;

    .line 16
    sget-object v0, Lorg/threeten/bp/Month;->JANUARY:Lorg/threeten/bp/Month;

    const/16 v1, 0x76c

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lorg/threeten/bp/LocalDate;->of(ILorg/threeten/bp/Month;I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-string v1, "LocalDate.of(1900, Month.JANUARY, 1)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->DEFAULT_MIN_DATE:Lorg/threeten/bp/LocalDate;

    .line 19
    sget-object v0, Lorg/threeten/bp/Month;->DECEMBER:Lorg/threeten/bp/Month;

    const/16 v1, 0x833

    const/16 v2, 0x1f

    invoke-static {v1, v0, v2}, Lorg/threeten/bp/LocalDate;->of(ILorg/threeten/bp/Month;I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-string v1, "LocalDate.of(2099, Month.DECEMBER, 31)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->DEFAULT_MAX_DATE:Lorg/threeten/bp/LocalDate;

    return-void
.end method

.method public constructor <init>(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;ZZZ)V
    .locals 1

    const-string v0, "currentDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "minDate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxDate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->currentDate:Lorg/threeten/bp/LocalDate;

    iput-object p2, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->minDate:Lorg/threeten/bp/LocalDate;

    iput-object p3, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->maxDate:Lorg/threeten/bp/LocalDate;

    iput-boolean p4, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->allowClear:Z

    iput-boolean p5, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->allowNoYear:Z

    iput-boolean p6, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->shouldShowYear:Z

    return-void
.end method

.method public synthetic constructor <init>(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;ZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_0

    .line 8
    sget-object p2, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->DEFAULT_MIN_DATE:Lorg/threeten/bp/LocalDate;

    :cond_0
    move-object v2, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_1

    .line 9
    sget-object p3, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->DEFAULT_MAX_DATE:Lorg/threeten/bp/LocalDate;

    :cond_1
    move-object v3, p3

    and-int/lit8 p2, p7, 0x8

    const/4 p3, 0x0

    if-eqz p2, :cond_2

    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    move v4, p4

    :goto_0
    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_3

    const/4 v5, 0x0

    goto :goto_1

    :cond_3
    move v5, p5

    :goto_1
    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_4

    const/4 p6, 0x1

    const/4 v6, 0x1

    goto :goto_2

    :cond_4
    move v6, p6

    :goto_2
    move-object v0, p0

    move-object v1, p1

    .line 12
    invoke-direct/range {v0 .. v6}, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;-><init>(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;ZZZ)V

    return-void
.end method

.method public static final synthetic access$getDEFAULT_MAX_DATE$cp()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 6
    sget-object v0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->DEFAULT_MAX_DATE:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public static final synthetic access$getDEFAULT_MIN_DATE$cp()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 6
    sget-object v0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->DEFAULT_MIN_DATE:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/register/widgets/NohoDatePickerDialogProps;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;ZZZILjava/lang/Object;)Lcom/squareup/register/widgets/NohoDatePickerDialogProps;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->currentDate:Lorg/threeten/bp/LocalDate;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->minDate:Lorg/threeten/bp/LocalDate;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->maxDate:Lorg/threeten/bp/LocalDate;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->allowClear:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->allowNoYear:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->shouldShowYear:Z

    :cond_5
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move p6, v1

    move p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->copy(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;ZZZ)Lcom/squareup/register/widgets/NohoDatePickerDialogProps;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lorg/threeten/bp/LocalDate;
    .locals 1

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->currentDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final component2()Lorg/threeten/bp/LocalDate;
    .locals 1

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->minDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final component3()Lorg/threeten/bp/LocalDate;
    .locals 1

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->maxDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->allowClear:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->allowNoYear:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->shouldShowYear:Z

    return v0
.end method

.method public final copy(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;ZZZ)Lcom/squareup/register/widgets/NohoDatePickerDialogProps;
    .locals 8

    const-string v0, "currentDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "minDate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxDate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;-><init>(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;ZZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->currentDate:Lorg/threeten/bp/LocalDate;

    iget-object v1, p1, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->currentDate:Lorg/threeten/bp/LocalDate;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->minDate:Lorg/threeten/bp/LocalDate;

    iget-object v1, p1, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->minDate:Lorg/threeten/bp/LocalDate;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->maxDate:Lorg/threeten/bp/LocalDate;

    iget-object v1, p1, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->maxDate:Lorg/threeten/bp/LocalDate;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->allowClear:Z

    iget-boolean v1, p1, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->allowClear:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->allowNoYear:Z

    iget-boolean v1, p1, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->allowNoYear:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->shouldShowYear:Z

    iget-boolean p1, p1, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->shouldShowYear:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAllowClear()Z
    .locals 1

    .line 10
    iget-boolean v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->allowClear:Z

    return v0
.end method

.method public final getAllowNoYear()Z
    .locals 1

    .line 11
    iget-boolean v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->allowNoYear:Z

    return v0
.end method

.method public final getCurrentDate()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->currentDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final getMaxDate()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->maxDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final getMinDate()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->minDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final getShouldShowYear()Z
    .locals 1

    .line 12
    iget-boolean v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->shouldShowYear:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->currentDate:Lorg/threeten/bp/LocalDate;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->minDate:Lorg/threeten/bp/LocalDate;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->maxDate:Lorg/threeten/bp/LocalDate;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->allowClear:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->allowNoYear:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->shouldShowYear:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NohoDatePickerDialogProps(currentDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->currentDate:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", minDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->minDate:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", maxDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->maxDate:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", allowClear="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->allowClear:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", allowNoYear="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->allowNoYear:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", shouldShowYear="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->shouldShowYear:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
