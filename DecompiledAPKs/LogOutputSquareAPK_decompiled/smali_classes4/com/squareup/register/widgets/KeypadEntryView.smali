.class public abstract Lcom/squareup/register/widgets/KeypadEntryView;
.super Landroid/widget/LinearLayout;
.source "KeypadEntryView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/container/spot/HasSpot;


# instance fields
.field protected amount:Landroid/widget/TextView;

.field protected keypad:Lcom/squareup/padlock/Padlock;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public getKeypad()Lcom/squareup/padlock/Padlock;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/register/widgets/KeypadEntryView;->keypad:Lcom/squareup/padlock/Padlock;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 53
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 30
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 31
    sget v0, Lcom/squareup/widgets/pos/R$id;->amount_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/register/widgets/KeypadEntryView;->amount:Landroid/widget/TextView;

    .line 32
    sget v0, Lcom/squareup/widgets/pos/R$id;->keypad:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock;

    iput-object v0, p0, Lcom/squareup/register/widgets/KeypadEntryView;->keypad:Lcom/squareup/padlock/Padlock;

    .line 33
    iget-object v0, p0, Lcom/squareup/register/widgets/KeypadEntryView;->keypad:Lcom/squareup/padlock/Padlock;

    invoke-virtual {p0}, Lcom/squareup/register/widgets/KeypadEntryView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->LIGHT:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3, v3}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;ZZ)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/padlock/Padlock;->setTypeface(Landroid/graphics/Typeface;)V

    return-void
.end method

.method public setAmountColor(I)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/register/widgets/KeypadEntryView;->amount:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setAmountText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/register/widgets/KeypadEntryView;->amount:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setListener(Lcom/squareup/padlock/Padlock$OnKeyPressListener;)V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/register/widgets/KeypadEntryView;->keypad:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock;->setOnKeyPressListener(Lcom/squareup/padlock/Padlock$OnKeyPressListener;)V

    return-void
.end method
