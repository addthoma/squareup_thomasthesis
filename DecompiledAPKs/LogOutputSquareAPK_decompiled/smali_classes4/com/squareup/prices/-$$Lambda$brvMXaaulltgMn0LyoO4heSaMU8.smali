.class public final synthetic Lcom/squareup/prices/-$$Lambda$brvMXaaulltgMn0LyoO4heSaMU8;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Func2;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/prices/-$$Lambda$brvMXaaulltgMn0LyoO4heSaMU8;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/prices/-$$Lambda$brvMXaaulltgMn0LyoO4heSaMU8;

    invoke-direct {v0}, Lcom/squareup/prices/-$$Lambda$brvMXaaulltgMn0LyoO4heSaMU8;-><init>()V

    sput-object v0, Lcom/squareup/prices/-$$Lambda$brvMXaaulltgMn0LyoO4heSaMU8;->INSTANCE:Lcom/squareup/prices/-$$Lambda$brvMXaaulltgMn0LyoO4heSaMU8;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/prices/PricingEngineServiceResult$Builder;

    check-cast p2, Ljava/util/Set;

    invoke-virtual {p1, p2}, Lcom/squareup/prices/PricingEngineServiceResult$Builder;->setCartDiscountsToRemove(Ljava/util/Set;)Lcom/squareup/prices/PricingEngineServiceResult$Builder;

    move-result-object p1

    return-object p1
.end method
