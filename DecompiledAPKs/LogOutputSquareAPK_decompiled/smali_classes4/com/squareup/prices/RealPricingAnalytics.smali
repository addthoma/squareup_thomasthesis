.class public Lcom/squareup/prices/RealPricingAnalytics;
.super Ljava/lang/Object;
.source "RealPricingAnalytics.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/analytics/PricingAnalytics;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/prices/RealPricingAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private logEs2Event(Lcom/squareup/eventstream/v2/AppEvent;)V
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/prices/RealPricingAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method


# virtual methods
.method public onRuleSearch(Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;)V
    .locals 1

    .line 17
    new-instance v0, Lcom/squareup/prices/RuleSearchEvent;

    invoke-direct {v0, p1}, Lcom/squareup/prices/RuleSearchEvent;-><init>(Lcom/squareup/shared/pricing/engine/analytics/RuleSearchMetrics;)V

    invoke-direct {p0, v0}, Lcom/squareup/prices/RealPricingAnalytics;->logEs2Event(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method
