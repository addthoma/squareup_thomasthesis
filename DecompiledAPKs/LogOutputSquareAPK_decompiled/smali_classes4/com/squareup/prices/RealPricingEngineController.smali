.class public Lcom/squareup/prices/RealPricingEngineController;
.super Ljava/lang/Object;
.source "RealPricingEngineController.java"

# interfaces
.implements Lcom/squareup/prices/PricingEngineController;


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private generation:J

.field private final pricingEngineService:Lcom/squareup/prices/PricingEngineService;

.field private final scheduler:Lrx/Scheduler;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lrx/Scheduler;Lcom/squareup/payment/Transaction;Lcom/squareup/prices/PricingEngineService;Lcom/squareup/settings/server/Features;)V
    .locals 2
    .param p1    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 29
    iput-wide v0, p0, Lcom/squareup/prices/RealPricingEngineController;->generation:J

    .line 33
    iput-object p1, p0, Lcom/squareup/prices/RealPricingEngineController;->scheduler:Lrx/Scheduler;

    .line 34
    iput-object p2, p0, Lcom/squareup/prices/RealPricingEngineController;->transaction:Lcom/squareup/payment/Transaction;

    .line 35
    iput-object p3, p0, Lcom/squareup/prices/RealPricingEngineController;->pricingEngineService:Lcom/squareup/prices/PricingEngineService;

    .line 36
    iput-object p4, p0, Lcom/squareup/prices/RealPricingEngineController;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onCartChanged$0$RealPricingEngineController(JLcom/squareup/prices/PricingEngineServiceResult;)V
    .locals 7

    if-eqz p3, :cond_2

    .line 72
    iget-wide v0, p0, Lcom/squareup/prices/RealPricingEngineController;->generation:J

    cmp-long v2, v0, p1

    if-eqz v2, :cond_0

    goto :goto_0

    .line 76
    :cond_0
    invoke-virtual {p3}, Lcom/squareup/prices/PricingEngineServiceResult;->getCartDiscountsToRemove()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_1

    .line 77
    iget-object p1, p0, Lcom/squareup/prices/RealPricingEngineController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p3}, Lcom/squareup/prices/PricingEngineServiceResult;->getCartDiscountsToRemove()Ljava/util/Set;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/checkout/Discounts;->toIds(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p2

    sget-object v0, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->PRICING_RULE:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/payment/Transaction;->removeDiscountsFromAllItems(Ljava/lang/Iterable;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)V

    .line 80
    :cond_1
    iget-object v1, p0, Lcom/squareup/prices/RealPricingEngineController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p3}, Lcom/squareup/prices/PricingEngineServiceResult;->getCatalogDiscounts()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {p3}, Lcom/squareup/prices/PricingEngineServiceResult;->getBlocks()Ljava/util/Map;

    move-result-object v3

    .line 81
    invoke-virtual {p3}, Lcom/squareup/prices/PricingEngineServiceResult;->getWholeBlocks()Ljava/util/List;

    move-result-object v4

    iget-object p1, p0, Lcom/squareup/prices/RealPricingEngineController;->features:Lcom/squareup/settings/server/Features;

    sget-object p2, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, p2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v5

    iget-object p1, p0, Lcom/squareup/prices/RealPricingEngineController;->features:Lcom/squareup/settings/server/Features;

    sget-object p2, Lcom/squareup/settings/server/Features$Feature;->ENABLE_WHOLE_PURCHASE_DISCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    .line 82
    invoke-interface {p1, p2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v6

    .line 80
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/payment/Transaction;->applyPricingRuleBlocks(Ljava/util/Map;Ljava/util/Map;Ljava/util/List;ZZ)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onCartChanged(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 4

    .line 47
    iget-wide v0, p0, Lcom/squareup/prices/RealPricingEngineController;->generation:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/squareup/prices/RealPricingEngineController;->generation:J

    .line 51
    invoke-virtual {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->viaPricingEngine()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 56
    :cond_0
    iget-boolean v0, p1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->itemsChanged:Z

    if-nez v0, :cond_1

    iget-boolean p1, p1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->discountsChanged:Z

    if-nez p1, :cond_1

    return-void

    .line 60
    :cond_1
    iget-object p1, p0, Lcom/squareup/prices/RealPricingEngineController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p1

    .line 63
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/squareup/payment/Order;->hasInvoice()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 67
    :cond_2
    iget-wide v0, p0, Lcom/squareup/prices/RealPricingEngineController;->generation:J

    .line 69
    iget-object v2, p0, Lcom/squareup/prices/RealPricingEngineController;->pricingEngineService:Lcom/squareup/prices/PricingEngineService;

    invoke-virtual {v2, p1}, Lcom/squareup/prices/PricingEngineService;->rulesForOrder(Lcom/squareup/payment/Order;)Lrx/Single;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/prices/RealPricingEngineController;->scheduler:Lrx/Scheduler;

    invoke-virtual {p1, v2}, Lrx/Single;->observeOn(Lrx/Scheduler;)Lrx/Single;

    move-result-object p1

    new-instance v2, Lcom/squareup/prices/-$$Lambda$RealPricingEngineController$ToePzvwita7tc2quGy7XFM-nzic;

    invoke-direct {v2, p0, v0, v1}, Lcom/squareup/prices/-$$Lambda$RealPricingEngineController$ToePzvwita7tc2quGy7XFM-nzic;-><init>(Lcom/squareup/prices/RealPricingEngineController;J)V

    invoke-virtual {p1, v2}, Lrx/Single;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    :cond_3
    :goto_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/prices/RealPricingEngineController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->cartChanges()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/prices/-$$Lambda$irzcTK9exUU1I0ckBB9ETdwzX4c;

    invoke-direct {v1, p0}, Lcom/squareup/prices/-$$Lambda$irzcTK9exUU1I0ckBB9ETdwzX4c;-><init>(Lcom/squareup/prices/RealPricingEngineController;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
