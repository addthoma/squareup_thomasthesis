.class public Lcom/squareup/prices/PricingEngineServiceResult;
.super Ljava/lang/Object;
.source "PricingEngineServiceResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/prices/PricingEngineServiceResult$Builder;
    }
.end annotation


# instance fields
.field private final blocks:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;",
            ">;>;"
        }
    .end annotation
.end field

.field private final cartDiscountsToRemove:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogDiscounts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;"
        }
    .end annotation
.end field

.field private final order:Lcom/squareup/payment/Order;

.field private final wholeBlocks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/payment/Order;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            "Ljava/util/Set<",
            "Lcom/squareup/checkout/Discount;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;",
            ">;>;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/prices/PricingEngineServiceResult;->order:Lcom/squareup/payment/Order;

    .line 33
    iput-object p2, p0, Lcom/squareup/prices/PricingEngineServiceResult;->cartDiscountsToRemove:Ljava/util/Set;

    .line 34
    iput-object p3, p0, Lcom/squareup/prices/PricingEngineServiceResult;->catalogDiscounts:Ljava/util/Map;

    .line 35
    iput-object p4, p0, Lcom/squareup/prices/PricingEngineServiceResult;->blocks:Ljava/util/Map;

    .line 36
    iput-object p5, p0, Lcom/squareup/prices/PricingEngineServiceResult;->wholeBlocks:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/Order;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;Lcom/squareup/prices/PricingEngineServiceResult$1;)V
    .locals 0

    .line 18
    invoke-direct/range {p0 .. p5}, Lcom/squareup/prices/PricingEngineServiceResult;-><init>(Lcom/squareup/payment/Order;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public getBlocks()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;",
            ">;>;"
        }
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/squareup/prices/PricingEngineServiceResult;->blocks:Ljava/util/Map;

    return-object v0
.end method

.method public getCartDiscountsToRemove()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/squareup/prices/PricingEngineServiceResult;->cartDiscountsToRemove:Ljava/util/Set;

    return-object v0
.end method

.method public getCatalogDiscounts()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;"
        }
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/squareup/prices/PricingEngineServiceResult;->catalogDiscounts:Ljava/util/Map;

    return-object v0
.end method

.method public getOrder()Lcom/squareup/payment/Order;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/prices/PricingEngineServiceResult;->order:Lcom/squareup/payment/Order;

    return-object v0
.end method

.method public getWholeBlocks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;",
            ">;"
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/squareup/prices/PricingEngineServiceResult;->wholeBlocks:Ljava/util/List;

    return-object v0
.end method
