.class public final Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;
.super Ljava/lang/Object;
.source "PasscodeEmployeeManagement_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final computationSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeCacheUpdaterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeCacheUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementModeDeciderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;"
        }
    .end annotation
.end field

.field private final employeesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodesSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodesSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodesSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeCacheUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->employeesProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p2, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p3, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p4, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p5, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->passcodesSettingsProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p6, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p7, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->employeeCacheUpdaterProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p8, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p9, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->computationSchedulerProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p10, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p11, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/Employees;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/EmployeeManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodesSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeCacheUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;"
        }
    .end annotation

    .line 80
    new-instance v12, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/permissions/Employees;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/permissions/EmployeeCacheUpdater;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;)Lcom/squareup/permissions/PasscodeEmployeeManagement;
    .locals 13

    .line 89
    new-instance v12, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/permissions/PasscodeEmployeeManagement;-><init>(Lcom/squareup/permissions/Employees;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/permissions/EmployeeCacheUpdater;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/permissions/PasscodeEmployeeManagement;
    .locals 12

    .line 68
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->employeesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/permissions/Employees;

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/settings/server/EmployeeManagementSettings;

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->passcodesSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/permissions/PasscodesSettings;

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/permissions/EmployeeManagementModeDecider;

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->employeeCacheUpdaterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/permissions/EmployeeCacheUpdater;

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->computationSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/analytics/Analytics;

    invoke-static/range {v1 .. v11}, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->newInstance(Lcom/squareup/permissions/Employees;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/permissions/EmployeeCacheUpdater;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;)Lcom/squareup/permissions/PasscodeEmployeeManagement;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement_Factory;->get()Lcom/squareup/permissions/PasscodeEmployeeManagement;

    move-result-object v0

    return-object v0
.end method
