.class public Lcom/squareup/permissions/PasscodesSettings$State$Builder;
.super Ljava/lang/Object;
.source "PasscodesSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/PasscodesSettings$State;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private passcodeTimeout:Lcom/squareup/permissions/PasscodesSettings$Timeout;

.field private passcodesEnabled:Z

.field private previousTeamPasscode:Ljava/lang/String;

.field private requestState:Lcom/squareup/permissions/PasscodesSettings$RequestState;

.field private requirePasscodeAfterEachSale:Z

.field private requirePasscodeAfterLogout:Z

.field private requirePasscodeWhenBackingOutOfSale:Z

.field private teamPasscode:Ljava/lang/String;

.field private teamPasscodeEnabled:Z

.field private teamPermissionRoleHasSharedPosAccess:Z

.field private teamRoleToken:Ljava/lang/String;

.field private unsavedOwnerPasscode:Ljava/lang/String;

.field private unsavedOwnerPasscodeConfirmation:Ljava/lang/String;

.field private unsavedTeamPasscode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 574
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/permissions/PasscodesSettings$State;
    .locals 17

    move-object/from16 v0, p0

    .line 690
    new-instance v16, Lcom/squareup/permissions/PasscodesSettings$State;

    iget-object v2, v0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->requestState:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    iget-boolean v3, v0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->passcodesEnabled:Z

    iget-object v4, v0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->teamRoleToken:Ljava/lang/String;

    iget-boolean v5, v0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->teamPasscodeEnabled:Z

    iget-object v6, v0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->previousTeamPasscode:Ljava/lang/String;

    iget-object v7, v0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->teamPasscode:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->unsavedTeamPasscode:Ljava/lang/String;

    iget-object v9, v0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->unsavedOwnerPasscode:Ljava/lang/String;

    iget-object v10, v0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->unsavedOwnerPasscodeConfirmation:Ljava/lang/String;

    iget-boolean v11, v0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->requirePasscodeAfterEachSale:Z

    iget-boolean v12, v0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->requirePasscodeWhenBackingOutOfSale:Z

    iget-object v13, v0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->passcodeTimeout:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    iget-boolean v14, v0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->requirePasscodeAfterLogout:Z

    iget-boolean v15, v0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->teamPermissionRoleHasSharedPosAccess:Z

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/squareup/permissions/PasscodesSettings$State;-><init>(Lcom/squareup/permissions/PasscodesSettings$RequestState;ZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/squareup/permissions/PasscodesSettings$Timeout;ZZ)V

    return-object v16
.end method

.method public clearUnsavedOwnerPasscode()Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 1

    const-string v0, ""

    .line 641
    iput-object v0, p0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->unsavedOwnerPasscode:Ljava/lang/String;

    return-object p0
.end method

.method public clearUnsavedOwnerPasscodeConfirmation()Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 1

    const-string v0, ""

    .line 646
    iput-object v0, p0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->unsavedOwnerPasscodeConfirmation:Ljava/lang/String;

    return-object p0
.end method

.method public clearUnsavedTeamPasscode()Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 1

    const-string v0, ""

    .line 636
    iput-object v0, p0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->unsavedTeamPasscode:Ljava/lang/String;

    return-object p0
.end method

.method public setAccountStatusFields(Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 2

    .line 677
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantRegisterSettings()Lcom/squareup/settings/server/MerchantRegisterSettings;

    move-result-object p1

    .line 678
    invoke-virtual {p1}, Lcom/squareup/settings/server/MerchantRegisterSettings;->getGuestPermissions()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lcom/squareup/permissions/Permission;->EMPLOYEE_ACCESS_REGISTER:Lcom/squareup/permissions/Permission;

    .line 679
    invoke-virtual {v1}, Lcom/squareup/permissions/Permission;->getPermissionString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 681
    invoke-virtual {p1}, Lcom/squareup/settings/server/MerchantRegisterSettings;->getTeamRoleToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setTeamRoleToken(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    .line 682
    invoke-virtual {p1}, Lcom/squareup/settings/server/MerchantRegisterSettings;->getTeamPasscode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setPreviousTeamPasscode(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    .line 683
    invoke-virtual {p1}, Lcom/squareup/settings/server/MerchantRegisterSettings;->getTeamPasscode()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setTeamPasscode(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    .line 684
    invoke-virtual {p0, v0}, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->setTeamPermissionRoleHasSharedPosAccess(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;

    return-object p0
.end method

.method public setPasscodeTimeout(Lcom/squareup/permissions/PasscodesSettings$Timeout;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 0

    .line 662
    iput-object p1, p0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->passcodeTimeout:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    return-object p0
.end method

.method public setPasscodesEnabled(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 0

    .line 596
    iput-boolean p1, p0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->passcodesEnabled:Z

    return-object p0
.end method

.method public setPreviousTeamPasscode(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 0

    .line 611
    iput-object p1, p0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->previousTeamPasscode:Ljava/lang/String;

    return-object p0
.end method

.method public setRequestState(Lcom/squareup/permissions/PasscodesSettings$RequestState;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 0

    .line 591
    iput-object p1, p0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->requestState:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    return-object p0
.end method

.method public setRequirePasscodeAfterEachSale(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 0

    .line 651
    iput-boolean p1, p0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->requirePasscodeAfterEachSale:Z

    return-object p0
.end method

.method public setRequirePasscodeAfterLogout(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 0

    .line 667
    iput-boolean p1, p0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->requirePasscodeAfterLogout:Z

    return-object p0
.end method

.method public setRequirePasscodeWhenBackingOutOfSale(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 0

    .line 657
    iput-boolean p1, p0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->requirePasscodeWhenBackingOutOfSale:Z

    return-object p0
.end method

.method public setTeamPasscode(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 0

    .line 616
    iput-object p1, p0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->teamPasscode:Ljava/lang/String;

    return-object p0
.end method

.method public setTeamPasscodeEnabled(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 0

    .line 606
    iput-boolean p1, p0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->teamPasscodeEnabled:Z

    return-object p0
.end method

.method public setTeamPermissionRoleHasSharedPosAccess(Z)Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 0

    .line 672
    iput-boolean p1, p0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->teamPermissionRoleHasSharedPosAccess:Z

    return-object p0
.end method

.method public setTeamRoleToken(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 0

    .line 601
    iput-object p1, p0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->teamRoleToken:Ljava/lang/String;

    return-object p0
.end method

.method public setUnsavedOwnerPasscode(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 0

    .line 626
    iput-object p1, p0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->unsavedOwnerPasscode:Ljava/lang/String;

    return-object p0
.end method

.method public setUnsavedOwnerPasscodeConfirmation(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 0

    .line 631
    iput-object p1, p0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->unsavedOwnerPasscodeConfirmation:Ljava/lang/String;

    return-object p0
.end method

.method public setUnsavedTeamPasscode(Ljava/lang/String;)Lcom/squareup/permissions/PasscodesSettings$State$Builder;
    .locals 0

    .line 621
    iput-object p1, p0, Lcom/squareup/permissions/PasscodesSettings$State$Builder;->unsavedTeamPasscode:Ljava/lang/String;

    return-object p0
.end method
