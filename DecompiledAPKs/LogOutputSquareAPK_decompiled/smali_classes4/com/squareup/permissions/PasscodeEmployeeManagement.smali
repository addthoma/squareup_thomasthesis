.class public Lcom/squareup/permissions/PasscodeEmployeeManagement;
.super Ljava/lang/Object;
.source "PasscodeEmployeeManagement.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;,
        Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;
    }
.end annotation


# static fields
.field private static final LOCK_IN_BACKGROUND_AFTER_MS:I = 0x1388


# instance fields
.field private final ATTEMPT_SCREEN_LOCK_RUNNABLE:Ljava/lang/Runnable;

.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final computationScheduler:Lio/reactivex/Scheduler;

.field private final currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

.field private disableTimerDisposable:Lio/reactivex/disposables/Disposable;

.field private disableTimerUntil:Lio/reactivex/Completable;

.field private final employeeCacheUpdater:Lcom/squareup/permissions/EmployeeCacheUpdater;

.field private final employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

.field private final employees:Lcom/squareup/permissions/Employees;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

.field private final publishEmployeeLogOut:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final publishLockAttempt:Lio/reactivex/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/PublishSubject<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final settings:Lcom/squareup/settings/server/EmployeeManagementSettings;


# direct methods
.method constructor <init>(Lcom/squareup/permissions/Employees;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/permissions/EmployeeCacheUpdater;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;)V
    .locals 13
    .param p8    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p9    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Computation;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 126
    new-instance v11, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-direct {v11}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/permissions/PasscodeEmployeeManagement;-><init>(Lcom/squareup/permissions/Employees;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/permissions/EmployeeCacheUpdater;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/CurrentPasscodeEmployeeState;Lcom/squareup/analytics/Analytics;)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/permissions/Employees;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/permissions/EmployeeCacheUpdater;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/CurrentPasscodeEmployeeState;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .param p8    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p9    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Computation;
        .end annotation
    .end param

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->publishLockAttempt:Lio/reactivex/subjects/PublishSubject;

    .line 104
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->publishEmployeeLogOut:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 106
    new-instance v0, Lcom/squareup/permissions/-$$Lambda$84mWt5jeeiN6fnvtKRuu2een8EI;

    invoke-direct {v0, p0}, Lcom/squareup/permissions/-$$Lambda$84mWt5jeeiN6fnvtKRuu2een8EI;-><init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;)V

    iput-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->ATTEMPT_SCREEN_LOCK_RUNNABLE:Ljava/lang/Runnable;

    .line 108
    invoke-static {}, Lio/reactivex/disposables/Disposables;->disposed()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->disableTimerDisposable:Lio/reactivex/disposables/Disposable;

    const/4 v0, 0x0

    .line 109
    iput-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->disableTimerUntil:Lio/reactivex/Completable;

    .line 141
    iput-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employees:Lcom/squareup/permissions/Employees;

    .line 142
    iput-object p2, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->settings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 143
    iput-object p3, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 144
    iput-object p4, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 145
    iput-object p5, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    .line 146
    iput-object p6, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    .line 147
    iput-object p7, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employeeCacheUpdater:Lcom/squareup/permissions/EmployeeCacheUpdater;

    .line 148
    iput-object p8, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->mainScheduler:Lio/reactivex/Scheduler;

    .line 149
    iput-object p9, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->computationScheduler:Lio/reactivex/Scheduler;

    .line 150
    iput-object p10, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->features:Lcom/squareup/settings/server/Features;

    .line 151
    iput-object p11, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    .line 152
    iput-object p12, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private cancelTimer()V
    .locals 2

    .line 765
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 766
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->ATTEMPT_SCREEN_LOCK_RUNNABLE:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method

.method private employeeForPasscode(Ljava/lang/String;)Lio/reactivex/functions/Function;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/functions/Function<",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Employee;",
            ">;",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/permissions/Employee;",
            ">;>;"
        }
    .end annotation

    .line 795
    new-instance v0, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$0753NLrlW6Wkry_A2gJGKCzl9UI;

    invoke-direct {v0, p0, p1}, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$0753NLrlW6Wkry_A2gJGKCzl9UI;-><init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;Ljava/lang/String;)V

    return-object v0
.end method

.method private getAnalyticsVersion()Ljava/lang/String;
    .locals 2

    .line 834
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "v1"

    return-object v0

    .line 836
    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantRegisterSettings()Lcom/squareup/settings/server/MerchantRegisterSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/MerchantRegisterSettings;->useTeamPermissions()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "v2"

    return-object v0

    :cond_1
    const-string v0, "v3"

    return-object v0
.end method

.method private getAttemptScreenLockAction(Z)Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;
    .locals 1

    .line 688
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->noPasscode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 689
    sget-object p1, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->SHOW_HOME_SCREEN:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    return-object p1

    .line 693
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->onlyRequireForRestrictedActions()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 694
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isUnlocked()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isCurrentEmployeeAGuest()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 702
    :cond_1
    iget-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings;->getLatestState()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p1

    .line 703
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantRegisterSettings()Lcom/squareup/settings/server/MerchantRegisterSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/MerchantRegisterSettings;->useTeamPermissions()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    .line 704
    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PasscodesSettings;->isTeamPasscodeEnabled(Lcom/squareup/permissions/PasscodesSettings$State;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean p1, p1, Lcom/squareup/permissions/PasscodesSettings$State;->requirePasscodeAfterLogout:Z

    if-nez p1, :cond_5

    .line 706
    :cond_2
    sget-object p1, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->LOG_IN_GUEST_AND_SHOW_HOME_SCREEN_WITH_HUD:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    return-object p1

    :cond_3
    :goto_0
    if-eqz p1, :cond_4

    .line 696
    sget-object p1, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->SHOW_PASSCODE_SCREEN:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    return-object p1

    .line 698
    :cond_4
    sget-object p1, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->LOG_IN_GUEST_AND_SHOW_HOME_SCREEN_NO_HUD:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    return-object p1

    .line 711
    :cond_5
    sget-object p1, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->SHOW_PASSCODE_SCREEN:Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    return-object p1
.end method

.method private getEmployee(Ljava/lang/String;Lcom/squareup/permissions/Permission;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/permissions/Permission;",
            ")",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/permissions/Employee;",
            ">;"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 463
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employees:Lcom/squareup/permissions/Employees;

    .line 464
    invoke-static {p2}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/permissions/Employees;->activeEmployeesWithAnyPermission(Ljava/util/Set;)Lio/reactivex/Observable;

    move-result-object p2

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employees:Lcom/squareup/permissions/Employees;

    .line 465
    invoke-virtual {p2}, Lcom/squareup/permissions/Employees;->activeEmployees()Lio/reactivex/Observable;

    move-result-object p2

    .line 469
    :goto_0
    invoke-virtual {p2}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p2

    .line 470
    invoke-direct {p0, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employeeForPasscode(Ljava/lang/String;)Lio/reactivex/functions/Function;

    move-result-object p1

    invoke-virtual {p2, p1}, Lio/reactivex/Single;->flatMapMaybe(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method

.method private getOwnerEmployeeForPasscode(Ljava/lang/String;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/permissions/Employee;",
            ">;"
        }
    .end annotation

    .line 529
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employees:Lcom/squareup/permissions/Employees;

    invoke-virtual {v0}, Lcom/squareup/permissions/Employees;->getActiveOwnerEmployees()Lio/reactivex/Maybe;

    move-result-object v0

    .line 530
    invoke-direct {p0, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employeeForPasscode(Ljava/lang/String;)Lio/reactivex/functions/Function;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/Maybe;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method

.method private getTeamEmployeeForPasscodeAndPermission(Ljava/lang/String;Ljava/util/Set;)Lio/reactivex/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;)",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/permissions/Employee;",
            ">;"
        }
    .end annotation

    .line 542
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->state()Lio/reactivex/Observable;

    move-result-object v0

    .line 543
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstElement()Lio/reactivex/Maybe;

    move-result-object v0

    new-instance v1, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$aPDNRywGPHEn-kutyycBAO8WVqY;

    invoke-direct {v1, p0, p1}, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$aPDNRywGPHEn-kutyycBAO8WVqY;-><init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;Ljava/lang/String;)V

    .line 544
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Maybe;

    move-result-object p1

    sget-object v0, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$GryR7P0XirexIvuF6ivqq6Jm8k0;->INSTANCE:Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$GryR7P0XirexIvuF6ivqq6Jm8k0;

    .line 546
    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    new-instance v0, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$C5sqo-XHu5VS3OXvJH2mqRAnB30;

    invoke-direct {v0, p0, p2}, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$C5sqo-XHu5VS3OXvJH2mqRAnB30;-><init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;Ljava/util/Set;)V

    .line 547
    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method

.method private getTeamRoleToken()Ljava/lang/String;
    .locals 1

    .line 829
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantRegisterSettings()Lcom/squareup/settings/server/MerchantRegisterSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/MerchantRegisterSettings;->getTeamRoleToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic lambda$YGpnIevrgU9ofxHbbz2L_cLa798(Lcom/squareup/permissions/PasscodeEmployeeManagement;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->setEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$checkIfAccountOwnerPasscode$8(Ljava/lang/String;Lcom/squareup/permissions/Employee;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 589
    invoke-virtual {p1, p0}, Lcom/squareup/permissions/Employee;->hasPasscode(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$getTeamEmployeeForPasscodeAndPermission$5(Lcom/squareup/permissions/PasscodesSettings$State;)Lcom/squareup/permissions/Employee;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 546
    sget-object p0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->GUEST_EMPLOYEE:Lcom/squareup/permissions/Employee;

    return-object p0
.end method

.method static synthetic lambda$null$10(Ljava/lang/String;Lcom/squareup/permissions/Employee;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 809
    invoke-virtual {p1, p0}, Lcom/squareup/permissions/Employee;->hasPasscode(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$onCurrentEmployeeChanged$1(Lcom/squareup/util/Optional;)Lkotlin/Unit;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 204
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method

.method private notifyListenersOfLockAttempt(Z)V
    .locals 1

    .line 790
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 791
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->publishLockAttempt:Lio/reactivex/subjects/PublishSubject;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method private scheduleScreenLockAfterTime(I)V
    .locals 4

    .line 782
    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->cancelTimer()V

    .line 783
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->disableTimerUntil:Lio/reactivex/Completable;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->NEVER:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    invoke-virtual {p1}, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->timeoutInMs()I

    move-result p1

    :goto_0
    if-lez p1, :cond_1

    .line 785
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->ATTEMPT_SCREEN_LOCK_RUNNABLE:Ljava/lang/Runnable;

    int-to-long v2, p1

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    return-void
.end method

.method private scheduleScreenLockAfterUserInteraction()V
    .locals 1

    .line 774
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->settings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->getTimeout()Lcom/squareup/settings/server/PasscodeTimeout;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/settings/server/PasscodeTimeout;->timeoutInMs()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->scheduleScreenLockAfterTime(I)V

    return-void
.end method

.method private setEnabled(Z)V
    .locals 1

    .line 238
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result v0

    if-eq v0, p1, :cond_1

    if-eqz p1, :cond_0

    .line 240
    iget-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {p1}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->upgradeModeFromOwnerToPasscodeEmployeeManagement()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    .line 241
    iget-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {p1}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->refreshCurrentEmployee()V

    .line 244
    iget-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employeeCacheUpdater:Lcom/squareup/permissions/EmployeeCacheUpdater;

    invoke-virtual {p1}, Lcom/squareup/permissions/EmployeeCacheUpdater;->refresh()V

    .line 247
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->onUserInteraction()V

    goto :goto_0

    .line 249
    :cond_0
    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->cancelTimer()V

    .line 250
    iget-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {p1}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->downgradeModeFromPasscodeEmployeeManagementToOwner()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    .line 251
    iget-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {p1}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->clearCurrentEmployee()V

    :cond_1
    :goto_0
    return-void
.end method

.method private shouldScheduleLockScreen()Z
    .locals 2

    .line 821
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 822
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isCurrentEmployeeAGuest()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 825
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result v0

    return v0
.end method

.method private teamEmployeeHasAnyPermission(Ljava/util/Set;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "permissions"

    .line 552
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonEmpty(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    .line 553
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 554
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantRegisterSettings()Lcom/squareup/settings/server/MerchantRegisterSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/MerchantRegisterSettings;->getGuestPermissions()Ljava/util/List;

    move-result-object v0

    .line 556
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/permissions/Permission;

    .line 557
    invoke-virtual {v1}, Lcom/squareup/permissions/Permission;->getPermissionString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method private unlock(Ljava/lang/String;ZLcom/squareup/permissions/Permission;Z)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Lcom/squareup/permissions/Permission;",
            "Z)",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/permissions/Employee;",
            ">;"
        }
    .end annotation

    .line 440
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 441
    invoke-static {}, Lio/reactivex/Maybe;->empty()Lio/reactivex/Maybe;

    move-result-object v0

    if-eqz p4, :cond_1

    if-nez p3, :cond_0

    .line 444
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object p4

    goto :goto_0

    :cond_0
    invoke-static {p3}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p4

    .line 443
    :goto_0
    invoke-direct {p0, p1, p4}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getTeamEmployeeForPasscodeAndPermission(Ljava/lang/String;Ljava/util/Set;)Lio/reactivex/Maybe;

    move-result-object v0

    .line 446
    :cond_1
    invoke-direct {p0, p1, p3}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getEmployee(Ljava/lang/String;Lcom/squareup/permissions/Permission;)Lio/reactivex/Maybe;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/Maybe;->switchIfEmpty(Lio/reactivex/MaybeSource;)Lio/reactivex/Maybe;

    move-result-object p1

    new-instance p3, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$pK8PB3J6K75Y5rszuLSi9W3haCo;

    invoke-direct {p3, p0, p2}, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$pK8PB3J6K75Y5rszuLSi9W3haCo;-><init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;Z)V

    .line 447
    invoke-virtual {p1, p3}, Lio/reactivex/Maybe;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public attemptScreenLock()V
    .locals 1

    const/4 v0, 0x1

    .line 640
    invoke-virtual {p0, v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->attemptScreenLock(Z)V

    return-void
.end method

.method public attemptScreenLock(Z)V
    .locals 6

    .line 648
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isUnlocked()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 650
    :goto_0
    iget-object v2, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {v2}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 651
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isCurrentEmployeeAGuest()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 652
    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getTeamRoleToken()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 654
    :cond_1
    iget-object v2, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {v2}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/permissions/Employee;->roleToken:Ljava/lang/String;

    goto :goto_1

    :cond_2
    move-object v2, v1

    .line 658
    :goto_1
    invoke-direct {p0, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getAttemptScreenLockAction(Z)Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;

    move-result-object p1

    .line 659
    sget-object v3, Lcom/squareup/permissions/PasscodeEmployeeManagement$1;->$SwitchMap$com$squareup$permissions$PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION:[I

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement$ATTEMPT_SCREEN_LOCK_ACTION;->ordinal()I

    move-result p1

    aget p1, v3, p1

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eq p1, v4, :cond_6

    const/4 v5, 0x2

    if-eq p1, v5, :cond_5

    const/4 v5, 0x3

    if-eq p1, v5, :cond_4

    const/4 v3, 0x4

    if-eq p1, v3, :cond_3

    goto :goto_2

    .line 676
    :cond_3
    iget-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getAnalyticsVersion()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0, v2}, Lcom/squareup/permissions/EmployeeManagementEvent;->forPasscodeLogout(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 677
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->clearCurrentEmployee()V

    .line 678
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->setCurrentEmployeeGuest()V

    .line 679
    iget-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getAnalyticsVersion()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getTeamRoleToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/permissions/EmployeeManagementEvent;->forPasscodeLogin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 680
    iget-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->publishEmployeeLogOut:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    goto :goto_2

    .line 670
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->setCurrentEmployeeGuest()V

    .line 671
    iget-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getAnalyticsVersion()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getTeamRoleToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/permissions/EmployeeManagementEvent;->forPasscodeLogin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 672
    iget-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->publishEmployeeLogOut:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    goto :goto_2

    .line 665
    :cond_5
    iget-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getAnalyticsVersion()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0, v2}, Lcom/squareup/permissions/EmployeeManagementEvent;->forPasscodeLogout(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 666
    invoke-direct {p0, v4}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->notifyListenersOfLockAttempt(Z)V

    goto :goto_2

    .line 661
    :cond_6
    iget-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->publishEmployeeLogOut:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    :goto_2
    return-void
.end method

.method public canCurrentEmployeeUseTimecards()Z
    .locals 1

    .line 720
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->settings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isTimecardEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isCurrentEmployeeAGuest()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 724
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/permissions/Employee;->canTrackTime:Z

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public canShowEmployeeManagementSettingsSection()Z
    .locals 1

    .line 257
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isAllowed()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->settings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->canUseTimecards()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method checkIfAccountOwnerPasscode(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;",
            ">;"
        }
    .end annotation

    .line 581
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employees:Lcom/squareup/permissions/Employees;

    invoke-virtual {v0}, Lcom/squareup/permissions/Employees;->getAccountOwnerEmployee()Lio/reactivex/Maybe;

    move-result-object v0

    new-instance v1, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$_MTHRe5TsGU08bJDGSaYTcujvg8;

    invoke-direct {v1, p0}, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$_MTHRe5TsGU08bJDGSaYTcujvg8;-><init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;)V

    .line 582
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Maybe;

    move-result-object v0

    new-instance v1, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$hXBomuEhdUprnKa9a0EenoAkR9U;

    invoke-direct {v1, p1}, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$hXBomuEhdUprnKa9a0EenoAkR9U;-><init>(Ljava/lang/String;)V

    .line 589
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Maybe;

    move-result-object p1

    sget-object v0, Lcom/squareup/permissions/-$$Lambda$XyXM1Ye3leU57ibs38WQse55XaI;->INSTANCE:Lcom/squareup/permissions/-$$Lambda$XyXM1Ye3leU57ibs38WQse55XaI;

    .line 590
    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    .line 591
    invoke-static {}, Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;->access$000()Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method checkIfOwnerPasscode(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;",
            ">;"
        }
    .end annotation

    .line 570
    invoke-direct {p0, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getOwnerEmployeeForPasscode(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object p1

    sget-object v0, Lcom/squareup/permissions/-$$Lambda$XyXM1Ye3leU57ibs38WQse55XaI;->INSTANCE:Lcom/squareup/permissions/-$$Lambda$XyXM1Ye3leU57ibs38WQse55XaI;

    .line 571
    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    .line 572
    invoke-static {}, Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;->access$000()Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public clearCurrentEmployee()V
    .locals 1

    .line 402
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {v0}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->clearCurrentEmployee()V

    return-void
.end method

.method public currentEmployee()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/permissions/Employee;",
            ">;>;"
        }
    .end annotation

    .line 195
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {v0}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployee()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method currentEmployeeHasPermission(Lcom/squareup/permissions/Permission;)Z
    .locals 2

    const-string v0, "permission"

    .line 354
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 359
    sget-object v0, Lcom/squareup/permissions/Permission;->CANCEL_BUYER_FLOW:Lcom/squareup/permissions/Permission;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REQUIRE_EMPLOYEE_PASSCODE_TO_CANCEL_TRANSACTION:Lcom/squareup/settings/server/Features$Feature;

    .line 360
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 364
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isCurrentEmployeeAGuest()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 365
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->teamEmployeeHasAnyPermission(Ljava/util/Set;)Z

    move-result p1

    return p1

    .line 366
    :cond_1
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {v0}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 p1, 0x0

    return p1

    .line 369
    :cond_2
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {v0}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    .line 370
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/Employee;->hasAnyPermission(Ljava/util/Set;)Z

    move-result p1

    return p1
.end method

.method public disableTimer(Lio/reactivex/Completable;)V
    .locals 2

    .line 733
    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->cancelTimer()V

    .line 734
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->disableTimerDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 736
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->disableTimerUntil:Lio/reactivex/Completable;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "disable passcode timer"

    .line 737
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 738
    iput-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->disableTimerUntil:Lio/reactivex/Completable;

    goto :goto_0

    :cond_0
    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "re-disable passcode timer"

    .line 740
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 741
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->disableTimerUntil:Lio/reactivex/Completable;

    invoke-virtual {v0, p1}, Lio/reactivex/Completable;->andThen(Lio/reactivex/CompletableSource;)Lio/reactivex/Completable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->disableTimerUntil:Lio/reactivex/Completable;

    .line 744
    :goto_0
    iget-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->disableTimerUntil:Lio/reactivex/Completable;

    new-instance v0, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$gWxmKa2zSwOfteYS6Xz5KrbO0CA;

    invoke-direct {v0, p0}, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$gWxmKa2zSwOfteYS6Xz5KrbO0CA;-><init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;)V

    invoke-virtual {p1, v0}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->disableTimerDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public getCurrentEmployee()Lcom/squareup/permissions/Employee;
    .locals 2

    .line 315
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 319
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isUnlocked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 320
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {v0}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    return-object v0

    .line 323
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot get current employee token when locked."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getCurrentEmployeeToken()Ljava/lang/String;
    .locals 2

    .line 327
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 331
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isCurrentEmployeeAGuest()Z

    move-result v0

    if-eqz v0, :cond_1

    return-object v1

    .line 335
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isUnlocked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 339
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {v0}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    return-object v0

    .line 336
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot get current employee token when locked."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getEmployeeForTimeTracking(Ljava/lang/String;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/permissions/Employee;",
            ">;"
        }
    .end annotation

    .line 598
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 600
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employees:Lcom/squareup/permissions/Employees;

    invoke-virtual {v0}, Lcom/squareup/permissions/Employees;->allEmployees()Lio/reactivex/Observable;

    move-result-object v0

    .line 602
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 603
    invoke-direct {p0, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employeeForPasscode(Ljava/lang/String;)Lio/reactivex/functions/Function;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/Single;->flatMapMaybe(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public isAccountOwnerLoggedIn()Z
    .locals 1

    .line 760
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 761
    iget-boolean v0, v0, Lcom/squareup/permissions/Employee;->isAccountOwner:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isAllowed()Z
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->isPasscodeEmployeeManagementModeAllowed()Z

    move-result v0

    return v0
.end method

.method public isCurrentEmployeeAGuest()Z
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {v0}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->isCurrentEmployeeGuest()Z

    move-result v0

    return v0
.end method

.method public isCurrentEmployeeNotClockedIn()Z
    .locals 1

    .line 716
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->canCurrentEmployeeUseTimecards()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/permissions/Employee;->timecardId:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isEnabled()Z
    .locals 2

    .line 213
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isOwnerLoggedIn()Z
    .locals 2

    .line 752
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isUnlocked()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 755
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 756
    iget-boolean v0, v0, Lcom/squareup/permissions/Employee;->isOwner:Z

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public isPasscodeScreenShowing()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 388
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentEmployee()Lio/reactivex/Observable;

    move-result-object v0

    .line 389
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$4-7s3_u0OPw06Dl3eZsTru2I1J8;

    invoke-direct {v1, p0}, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$4-7s3_u0OPw06Dl3eZsTru2I1J8;-><init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;)V

    .line 390
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 391
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public isRepeatedLogin()Z
    .locals 1

    .line 395
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {v0}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->isRepeatedLogin()Z

    move-result v0

    return v0
.end method

.method public isTimeTrackingEnabled()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 264
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->settings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isTimecardEnabled()Z

    move-result v0

    return v0
.end method

.method public isTransactionLockModeEnabled()Z
    .locals 3

    .line 276
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantRegisterSettings()Lcom/squareup/settings/server/MerchantRegisterSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/MerchantRegisterSettings;->useTeamPermissions()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 285
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->settings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isTransactionLockModeEnabled()Z

    move-result v0

    return v0

    .line 300
    :cond_1
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->onlyRequireForRestrictedActions()Z

    move-result v0

    if-eqz v0, :cond_2

    return v1

    .line 304
    :cond_2
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->settings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isTransactionLockModeEnabled()Z

    move-result v0

    return v0
.end method

.method public isUnlocked()Z
    .locals 1

    .line 378
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {v0}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$checkIfAccountOwnerPasscode$7$PasscodeEmployeeManagement(Lio/reactivex/disposables/Disposable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 583
    iget-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {p1}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object p1

    .line 584
    sget-object v0, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-ne p1, v0, :cond_0

    return-void

    .line 585
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not possible to check account owner passcode in mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 586
    invoke-virtual {p1}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic lambda$disableTimer$9$PasscodeEmployeeManagement()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "enable passcode timer"

    .line 745
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    .line 746
    iput-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->disableTimerUntil:Lio/reactivex/Completable;

    .line 747
    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->scheduleScreenLockAfterUserInteraction()V

    return-void
.end method

.method public synthetic lambda$employeeForPasscode$12$PasscodeEmployeeManagement(Ljava/lang/String;Ljava/util/Set;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 802
    invoke-static {p2}, Lio/reactivex/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/Observable;

    move-result-object p2

    new-instance v0, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$-_44QMObz5CllZ3hf48ntSc0KM8;

    invoke-direct {v0, p0, p1}, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$-_44QMObz5CllZ3hf48ntSc0KM8;-><init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;Ljava/lang/String;)V

    .line 803
    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->flatMapMaybe(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 814
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstElement()Lio/reactivex/Maybe;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->mainScheduler:Lio/reactivex/Scheduler;

    .line 815
    invoke-virtual {p1, p2}, Lio/reactivex/Maybe;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$getTeamEmployeeForPasscodeAndPermission$4$PasscodeEmployeeManagement(Ljava/lang/String;Lcom/squareup/permissions/PasscodesSettings$State;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 544
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0, p2}, Lcom/squareup/permissions/PasscodesSettings;->isTeamPasscodeEnabled(Lcom/squareup/permissions/PasscodesSettings$State;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p2, p2, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscode:Ljava/lang/String;

    .line 545
    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public synthetic lambda$getTeamEmployeeForPasscodeAndPermission$6$PasscodeEmployeeManagement(Ljava/util/Set;Lcom/squareup/permissions/Employee;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 548
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_1

    invoke-direct {p0, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->teamEmployeeHasAnyPermission(Ljava/util/Set;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public synthetic lambda$isPasscodeScreenShowing$2$PasscodeEmployeeManagement(Lcom/squareup/util/Optional;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 390
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isUnlocked()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$11$PasscodeEmployeeManagement(Ljava/lang/String;Lcom/squareup/permissions/Employee;)Lio/reactivex/MaybeSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 806
    invoke-static {p2}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->computationScheduler:Lio/reactivex/Scheduler;

    .line 808
    invoke-virtual {p2, v0}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p2

    new-instance v0, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$E1lANmEy3AaJocWA5dVsAyebj7o;

    invoke-direct {v0, p1}, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$E1lANmEy3AaJocWA5dVsAyebj7o;-><init>(Ljava/lang/String;)V

    .line 809
    invoke-virtual {p2, v0}, Lio/reactivex/Single;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$0$PasscodeEmployeeManagement(Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 164
    iget-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {p1}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->isTimecardOnlyOwnerMode()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 165
    iget-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employeeCacheUpdater:Lcom/squareup/permissions/EmployeeCacheUpdater;

    invoke-virtual {p1}, Lcom/squareup/permissions/EmployeeCacheUpdater;->refresh()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$unlock$3$PasscodeEmployeeManagement(ZLcom/squareup/permissions/Employee;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 448
    sget-object v0, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->GUEST_EMPLOYEE:Lcom/squareup/permissions/Employee;

    if-eq p2, v0, :cond_0

    .line 449
    invoke-virtual {p2}, Lcom/squareup/permissions/Employee;->role_token()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getTeamRoleToken()Ljava/lang/String;

    move-result-object v0

    .line 450
    :goto_0
    iget-object v1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getAnalyticsVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/squareup/permissions/Employee;->token()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/squareup/permissions/EmployeeManagementEvent;->forPasscodeLogin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 452
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 453
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {v0}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->clearCurrentEmployee()V

    .line 455
    :cond_1
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {v0, p2}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->setCurrentEmployee(Lcom/squareup/permissions/Employee;)V

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    .line 457
    invoke-direct {p0, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->notifyListenersOfLockAttempt(Z)V

    :cond_2
    return-void
.end method

.method public maybeOneEmployeeByToken(Ljava/lang/String;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/permissions/Employee;",
            ">;"
        }
    .end annotation

    .line 348
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employees:Lcom/squareup/permissions/Employees;

    .line 349
    invoke-virtual {v0, p1}, Lcom/squareup/permissions/Employees;->maybeOneEmployeeByToken(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object p1

    goto :goto_0

    .line 350
    :cond_0
    invoke-static {}, Lio/reactivex/Maybe;->empty()Lio/reactivex/Maybe;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public noPasscode()Z
    .locals 2

    .line 218
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->settings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 219
    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->getPasscodeAccess()Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    move-result-object v0

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->NO_PASSCODE:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public onAppPause()V
    .locals 1

    .line 614
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 615
    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->shouldScheduleLockScreen()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 617
    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->settings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->getTimeout()Lcom/squareup/settings/server/PasscodeTimeout;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/settings/server/PasscodeTimeout;->shouldLockOnAppBackgrounded()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x1388

    .line 618
    invoke-direct {p0, v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->scheduleScreenLockAfterTime(I)V

    :cond_1
    return-void
.end method

.method public onAppResume()V
    .locals 1

    .line 623
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 624
    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->shouldScheduleLockScreen()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 627
    :cond_0
    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->scheduleScreenLockAfterUserInteraction()V

    return-void
.end method

.method public onCurrentEmployeeChanged()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 202
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {v0}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->currentEmployee()Lio/reactivex/Observable;

    move-result-object v0

    .line 203
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$Pf0kH7z-9ZI2RpJSdrmfu5BHCv0;->INSTANCE:Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$Pf0kH7z-9ZI2RpJSdrmfu5BHCv0;

    .line 204
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->mainScheduler:Lio/reactivex/Scheduler;

    .line 205
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onEmployeeLogOut()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 191
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->publishEmployeeLogOut:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->settings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 158
    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->onEmployeeManagementSettingChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$YGpnIevrgU9ofxHbbz2L_cLa798;

    invoke-direct {v1, p0}, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$YGpnIevrgU9ofxHbbz2L_cLa798;-><init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;)V

    .line 159
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 156
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 160
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->settings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 162
    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->onTimeTrackingSettingChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$xw7Tn7nfZg-Y5HNQ9c4314MDgpU;

    invoke-direct {v1, p0}, Lcom/squareup/permissions/-$$Lambda$PasscodeEmployeeManagement$xw7Tn7nfZg-Y5HNQ9c4314MDgpU;-><init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;)V

    .line 163
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 160
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 169
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->shouldAutoLoginAsGuest()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 172
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->clearCurrentEmployee()V

    .line 173
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->setCurrentEmployeeGuest()V

    :cond_0
    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->publishLockAttempt:Lio/reactivex/subjects/PublishSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/PublishSubject;->onComplete()V

    .line 179
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->disableTimerDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    return-void
.end method

.method public onLockAttempt()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 187
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->publishLockAttempt:Lio/reactivex/subjects/PublishSubject;

    return-object v0
.end method

.method public onUserInteraction()V
    .locals 1

    .line 632
    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->shouldScheduleLockScreen()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 633
    :cond_0
    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->scheduleScreenLockAfterUserInteraction()V

    return-void
.end method

.method public onlyRequireForRestrictedActions()Z
    .locals 2

    .line 229
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->settings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 230
    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->getPasscodeAccess()Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    move-result-object v0

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->PASSCODE_FOR_RESTRICTED_ACTIONS:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method passcodeProvidesPermission(Ljava/lang/String;Ljava/util/Set;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;",
            ">;"
        }
    .end annotation

    const-string v0, "permissions"

    .line 509
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonEmpty(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    .line 516
    invoke-direct {p0, p1, p2}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getTeamEmployeeForPasscodeAndPermission(Ljava/lang/String;Ljava/util/Set;)Lio/reactivex/Maybe;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employees:Lcom/squareup/permissions/Employees;

    .line 517
    invoke-virtual {v1, p2}, Lcom/squareup/permissions/Employees;->activeEmployeesWithAnyPermission(Ljava/util/Set;)Lio/reactivex/Observable;

    move-result-object p2

    .line 518
    invoke-virtual {p2}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p2

    .line 519
    invoke-direct {p0, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employeeForPasscode(Ljava/lang/String;)Lio/reactivex/functions/Function;

    move-result-object p1

    invoke-virtual {p2, p1}, Lio/reactivex/Single;->flatMapMaybe(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    .line 517
    invoke-virtual {v0, p1}, Lio/reactivex/Maybe;->switchIfEmpty(Lio/reactivex/MaybeSource;)Lio/reactivex/Maybe;

    move-result-object p1

    sget-object p2, Lcom/squareup/permissions/-$$Lambda$XyXM1Ye3leU57ibs38WQse55XaI;->INSTANCE:Lcom/squareup/permissions/-$$Lambda$XyXM1Ye3leU57ibs38WQse55XaI;

    .line 520
    invoke-virtual {p1, p2}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    const/4 p2, 0x0

    .line 521
    invoke-static {p2}, Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;->forPasscodeEmployeeManagement(Lcom/squareup/permissions/Employee;)Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public setCurrentEmployee(Lcom/squareup/permissions/Employee;)V
    .locals 1

    .line 494
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->setCurrentEmployee(Lcom/squareup/permissions/Employee;)V

    return-void
.end method

.method public setCurrentEmployeeGuest()V
    .locals 1

    .line 498
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 499
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {v0}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->setCurrentEmployeeGuest()V

    return-void
.end method

.method setCurrentUserToOwnerWithPasscode(Ljava/lang/String;)Lio/reactivex/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/permissions/Employee;",
            ">;"
        }
    .end annotation

    .line 414
    invoke-direct {p0, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getOwnerEmployeeForPasscode(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/permissions/-$$Lambda$EZB2GjCe75BZNUgna9cwWdXqlB0;

    invoke-direct {v1, v0}, Lcom/squareup/permissions/-$$Lambda$EZB2GjCe75BZNUgna9cwWdXqlB0;-><init>(Lcom/squareup/permissions/CurrentPasscodeEmployeeState;)V

    .line 415
    invoke-virtual {p1, v1}, Lio/reactivex/Maybe;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method

.method setCurrentUserToUserWithPasscodeAndPermission(Ljava/lang/String;Lcom/squareup/permissions/Permission;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/permissions/Permission;",
            ")",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/permissions/Employee;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 425
    invoke-direct {p0, p1, v0, p2, v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->unlock(Ljava/lang/String;ZLcom/squareup/permissions/Permission;Z)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public shouldAutoLoginAsGuest()Z
    .locals 2

    .line 224
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->onlyRequireForRestrictedActions()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public unlock(Ljava/lang/String;)Lio/reactivex/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/permissions/Employee;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 410
    invoke-direct {p0, p1, v0, v1, v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->unlock(Ljava/lang/String;ZLcom/squareup/permissions/Permission;Z)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public unlockGuest()V
    .locals 4

    .line 478
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getAnalyticsVersion()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 479
    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantRegisterSettings()Lcom/squareup/settings/server/MerchantRegisterSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/MerchantRegisterSettings;->getTeamRoleToken()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 478
    invoke-static {v1, v3, v2}, Lcom/squareup/permissions/EmployeeManagementEvent;->forPasscodeLogin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 481
    invoke-virtual {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->setCurrentEmployeeGuest()V

    .line 484
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 487
    invoke-direct {p0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->cancelTimer()V

    :cond_0
    const/4 v0, 0x0

    .line 490
    invoke-direct {p0, v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->notifyListenersOfLockAttempt(Z)V

    return-void
.end method

.method public updateEmployeeTimecardId(Lcom/squareup/permissions/Employee;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 607
    iget-object v0, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->employees:Lcom/squareup/permissions/Employees;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/permissions/Employees;->setTimecardId(Lcom/squareup/permissions/Employee;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Completable;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/Completable;->subscribe()Lio/reactivex/disposables/Disposable;

    if-nez p2, :cond_0

    .line 609
    iget-object p1, p0, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentPasscodeEmployeeState:Lcom/squareup/permissions/CurrentPasscodeEmployeeState;

    invoke-virtual {p1}, Lcom/squareup/permissions/CurrentPasscodeEmployeeState;->clearLastEmployee()V

    :cond_0
    return-void
.end method
