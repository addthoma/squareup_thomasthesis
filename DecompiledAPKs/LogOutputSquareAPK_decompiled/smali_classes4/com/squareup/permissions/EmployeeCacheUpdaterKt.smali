.class public final Lcom/squareup/permissions/EmployeeCacheUpdaterKt;
.super Ljava/lang/Object;
.source "EmployeeCacheUpdater.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0003X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0007X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0008\u001a\u00020\u0007X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "CACHE_UPDATE_TIME_MS",
        "",
        "MAX_RESULTS",
        "",
        "NUMBER_OF_RETRIES",
        "RETRY_DELAY",
        "SENTINEL_END",
        "",
        "SENTINEL_START",
        "employees_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CACHE_UPDATE_TIME_MS:J = 0x493e0L

.field private static final MAX_RESULTS:I = 0x2710

.field private static final NUMBER_OF_RETRIES:I = 0x3

.field private static final RETRY_DELAY:J = 0x2L

.field private static final SENTINEL_END:Ljava/lang/String; = "sentinel_end_value"

.field private static final SENTINEL_START:Ljava/lang/String; = "sentinel_start_value"
