.class public Lcom/squareup/permissions/EmployeeManagementEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "EmployeeManagementEvent.java"


# static fields
.field private static final ENABLE_PASSCODE_ACCESS_EVENT:Ljava/lang/String; = "Employee Management: Enable Passcode Access: Toggle"

.field private static final EVENT_DETAIL_1_MINUTE:Ljava/lang/String; = "One Minute"

.field private static final EVENT_DETAIL_30_SECONDS:Ljava/lang/String; = "Thirty Seconds"

.field private static final EVENT_DETAIL_5_MINUTES:Ljava/lang/String; = "Five Minutes"

.field private static final EVENT_DETAIL_COMPLETE:Ljava/lang/String; = "Complete"

.field private static final EVENT_DETAIL_NONE:Ljava/lang/String; = "None"

.field private static final EVENT_DETAIL_OFF:Ljava/lang/String; = "OFF"

.field private static final EVENT_DETAIL_ON:Ljava/lang/String; = "ON"

.field private static final EVENT_DETAIL_RESTRICT_ACTIONS:Ljava/lang/String; = "RestrictActions"

.field private static final LEVEL_EVENT:Ljava/lang/String; = "Employee Management: Level:"

.field private static final LOCK_AFTER_SALE_EVENT:Ljava/lang/String; = "Employee Management: Lock After Sale: Toggle"

.field private static final LOGIN_SOURCE_EMAIL:Ljava/lang/String; = "email"

.field private static final LOGIN_SOURCE_PASSCODE:Ljava/lang/String; = "passcode"

.field private static final LOG_IN_EVENT:Ljava/lang/String; = "Logging In Employee"

.field private static final LOG_OUT_EVENT:Ljava/lang/String; = "Logging Out Employee"

.field private static final TIMEOUT_EVENT:Ljava/lang/String; = "Employee Management: Timeout:"

.field private static final TIME_TRACKING_EVENT:Ljava/lang/String; = "Employee Management: Time Tracking: Toggle"

.field private static final VERSION_1:Ljava/lang/String; = "v1"

.field private static final VERSION_2:Ljava/lang/String; = "v2"


# instance fields
.field public final detail:Ljava/lang/String;

.field public final employee_remote_id:Ljava/lang/String;

.field public final login_source:Ljava/lang/String;

.field public final role_remote_id:Ljava/lang/String;

.field public final version:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 53
    invoke-direct/range {v0 .. v7}, Lcom/squareup/permissions/EmployeeManagementEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 59
    iput-object p3, p0, Lcom/squareup/permissions/EmployeeManagementEvent;->detail:Ljava/lang/String;

    .line 60
    iput-object p4, p0, Lcom/squareup/permissions/EmployeeManagementEvent;->version:Ljava/lang/String;

    .line 61
    iput-object p5, p0, Lcom/squareup/permissions/EmployeeManagementEvent;->employee_remote_id:Ljava/lang/String;

    .line 62
    iput-object p6, p0, Lcom/squareup/permissions/EmployeeManagementEvent;->role_remote_id:Ljava/lang/String;

    .line 63
    iput-object p7, p0, Lcom/squareup/permissions/EmployeeManagementEvent;->login_source:Ljava/lang/String;

    return-void
.end method

.method public static forEmailLogin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 9

    .line 150
    new-instance v8, Lcom/squareup/permissions/EmployeeManagementEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v2, "Logging In Employee"

    const-string v3, ""

    const-string v7, "email"

    move-object v0, v8

    move-object v4, p0

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/squareup/permissions/EmployeeManagementEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v8
.end method

.method public static forGuestModeToggle(ZZ)Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 3

    if-eqz p0, :cond_0

    const-string p0, "RestrictActions"

    goto :goto_0

    :cond_0
    const-string p0, "Complete"

    .line 78
    :goto_0
    new-instance v0, Lcom/squareup/permissions/EmployeeManagementEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-static {p1}, Lcom/squareup/permissions/EmployeeManagementEvent;->versionLabel(Z)Ljava/lang/String;

    move-result-object p1

    const-string v2, "Employee Management: Level:"

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/squareup/permissions/EmployeeManagementEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static forPasscodeAccessToggle(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;Z)Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 3

    .line 90
    sget-object v0, Lcom/squareup/permissions/EmployeeManagementEvent$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$PasscodeAccess:[I

    invoke-virtual {p0}, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    const-string v1, "Employee Management: Level:"

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    .line 98
    new-instance p0, Lcom/squareup/permissions/EmployeeManagementEvent;

    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 99
    invoke-static {p1}, Lcom/squareup/permissions/EmployeeManagementEvent;->versionLabel(Z)Ljava/lang/String;

    move-result-object p1

    const-string v2, "Complete"

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/squareup/permissions/EmployeeManagementEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object p0

    .line 95
    :cond_0
    new-instance p0, Lcom/squareup/permissions/EmployeeManagementEvent;

    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 96
    invoke-static {p1}, Lcom/squareup/permissions/EmployeeManagementEvent;->versionLabel(Z)Ljava/lang/String;

    move-result-object p1

    const-string v2, "RestrictActions"

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/squareup/permissions/EmployeeManagementEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object p0

    .line 92
    :cond_1
    new-instance p0, Lcom/squareup/permissions/EmployeeManagementEvent;

    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 93
    invoke-static {p1}, Lcom/squareup/permissions/EmployeeManagementEvent;->versionLabel(Z)Ljava/lang/String;

    move-result-object p1

    const-string v2, "None"

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/squareup/permissions/EmployeeManagementEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public static forPasscodeEmployeeManagementEnabledToggle(ZZ)Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 3

    if-eqz p0, :cond_0

    const-string p0, "ON"

    goto :goto_0

    :cond_0
    const-string p0, "OFF"

    .line 72
    :goto_0
    new-instance v0, Lcom/squareup/permissions/EmployeeManagementEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 73
    invoke-static {p1}, Lcom/squareup/permissions/EmployeeManagementEvent;->versionLabel(Z)Ljava/lang/String;

    move-result-object p1

    const-string v2, "Employee Management: Enable Passcode Access: Toggle"

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/squareup/permissions/EmployeeManagementEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static forPasscodeLogin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 9

    .line 138
    new-instance v8, Lcom/squareup/permissions/EmployeeManagementEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v2, "Logging In Employee"

    const-string v3, ""

    const-string v7, "passcode"

    move-object v0, v8

    move-object v4, p0

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/squareup/permissions/EmployeeManagementEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v8
.end method

.method public static forPasscodeLogout(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 9

    .line 144
    new-instance v8, Lcom/squareup/permissions/EmployeeManagementEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v2, "Logging Out Employee"

    const-string v3, ""

    const-string v7, "passcode"

    move-object v0, v8

    move-object v4, p0

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/squareup/permissions/EmployeeManagementEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v8
.end method

.method public static forTimeTrackingToggle(ZZ)Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 3

    if-eqz p0, :cond_0

    const-string p0, "ON"

    goto :goto_0

    :cond_0
    const-string p0, "OFF"

    .line 84
    :goto_0
    new-instance v0, Lcom/squareup/permissions/EmployeeManagementEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 85
    invoke-static {p1}, Lcom/squareup/permissions/EmployeeManagementEvent;->versionLabel(Z)Ljava/lang/String;

    move-result-object p1

    const-string v2, "Employee Management: Time Tracking: Toggle"

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/squareup/permissions/EmployeeManagementEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static forTimeoutChange(Lcom/squareup/settings/server/PasscodeTimeout;Z)Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 5

    .line 106
    instance-of v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    .line 107
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "Unknown class value %s"

    .line 106
    invoke-static {v0, v3, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 108
    check-cast p0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    .line 109
    sget-object v0, Lcom/squareup/permissions/EmployeeManagementEvent$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$Timeout:[I

    invoke-virtual {p0}, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->ordinal()I

    move-result p0

    aget p0, v0, p0

    if-eq p0, v1, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-ne p0, v0, :cond_0

    const-string p0, "Five Minutes"

    goto :goto_0

    .line 123
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Unable to get timeout value."

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    const-string p0, "One Minute"

    goto :goto_0

    :cond_2
    const-string p0, "Thirty Seconds"

    goto :goto_0

    :cond_3
    const-string p0, "None"

    .line 126
    :goto_0
    new-instance v0, Lcom/squareup/permissions/EmployeeManagementEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-static {p1}, Lcom/squareup/permissions/EmployeeManagementEvent;->versionLabel(Z)Ljava/lang/String;

    move-result-object p1

    const-string v2, "Employee Management: Timeout:"

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/squareup/permissions/EmployeeManagementEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static forTransactionLockModeChange(ZZ)Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 3

    if-eqz p0, :cond_0

    const-string p0, "ON"

    goto :goto_0

    :cond_0
    const-string p0, "OFF"

    .line 132
    :goto_0
    new-instance v0, Lcom/squareup/permissions/EmployeeManagementEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 133
    invoke-static {p1}, Lcom/squareup/permissions/EmployeeManagementEvent;->versionLabel(Z)Ljava/lang/String;

    move-result-object p1

    const-string v2, "Employee Management: Lock After Sale: Toggle"

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/squareup/permissions/EmployeeManagementEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static versionLabel(Z)Ljava/lang/String;
    .locals 0

    if-eqz p0, :cond_0

    const-string p0, "v2"

    goto :goto_0

    :cond_0
    const-string p0, "v1"

    :goto_0
    return-object p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 160
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 161
    :cond_1
    check-cast p1, Lcom/squareup/permissions/EmployeeManagementEvent;

    .line 162
    iget-object v2, p0, Lcom/squareup/permissions/EmployeeManagementEvent;->detail:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/permissions/EmployeeManagementEvent;->detail:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/permissions/EmployeeManagementEvent;->version:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/permissions/EmployeeManagementEvent;->version:Ljava/lang/String;

    .line 163
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/permissions/EmployeeManagementEvent;->employee_remote_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/permissions/EmployeeManagementEvent;->employee_remote_id:Ljava/lang/String;

    .line 164
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/permissions/EmployeeManagementEvent;->role_remote_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/permissions/EmployeeManagementEvent;->role_remote_id:Ljava/lang/String;

    .line 165
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/permissions/EmployeeManagementEvent;->login_source:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/permissions/EmployeeManagementEvent;->login_source:Ljava/lang/String;

    .line 166
    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method
