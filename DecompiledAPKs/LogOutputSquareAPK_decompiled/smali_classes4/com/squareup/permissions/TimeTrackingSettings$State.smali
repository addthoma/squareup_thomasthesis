.class public Lcom/squareup/permissions/TimeTrackingSettings$State;
.super Ljava/lang/Object;
.source "TimeTrackingSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/TimeTrackingSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;
    }
.end annotation


# instance fields
.field public final timeTrackingEnabled:Z


# direct methods
.method constructor <init>(Z)V
    .locals 0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-boolean p1, p0, Lcom/squareup/permissions/TimeTrackingSettings$State;->timeTrackingEnabled:Z

    return-void
.end method


# virtual methods
.method builder()Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;
    .locals 2

    .line 85
    new-instance v0, Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;

    invoke-direct {v0}, Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;-><init>()V

    iget-boolean v1, p0, Lcom/squareup/permissions/TimeTrackingSettings$State;->timeTrackingEnabled:Z

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;->setTimeTrackingEnabled(Z)Lcom/squareup/permissions/TimeTrackingSettings$State$Builder;

    move-result-object v0

    return-object v0
.end method
