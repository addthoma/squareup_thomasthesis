.class public abstract Lcom/squareup/permissions/PermissionGatekeeper$EmployeePasscodeUnlockWhen;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "PermissionGatekeeper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/PermissionGatekeeper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "EmployeePasscodeUnlockWhen"
.end annotation


# instance fields
.field private final permission:Lcom/squareup/permissions/Permission;


# direct methods
.method protected constructor <init>(Lcom/squareup/permissions/Permission;)V
    .locals 0

    .line 120
    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    .line 121
    iput-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper$EmployeePasscodeUnlockWhen;->permission:Lcom/squareup/permissions/Permission;

    return-void
.end method


# virtual methods
.method protected getPermission()Lcom/squareup/permissions/Permission;
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper$EmployeePasscodeUnlockWhen;->permission:Lcom/squareup/permissions/Permission;

    return-object v0
.end method
