.class public Lcom/squareup/permissions/PermissionGatekeeper;
.super Ljava/lang/Object;
.source "PermissionGatekeeper.java"


# annotations
.annotation runtime Lcom/squareup/permissions/PermissionGatekeeperScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerPasscodeUnlockWhen;,
        Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerOrAdminPasscodeUnlockWhen;,
        Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;,
        Lcom/squareup/permissions/PermissionGatekeeper$EmployeePasscodeUnlockWhen;,
        Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerOrAdminLoginWhen;,
        Lcom/squareup/permissions/PermissionGatekeeper$When;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final cacheUpdater:Lcom/squareup/permissions/EmployeeCacheUpdater;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

.field private final features:Lcom/squareup/settings/server/Features;

.field private granted:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

.field private requested:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation
.end field

.field private final sharedShouldBeShowingPermissionDeniedScreen:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final shouldShowPermissionDeniedScreenSubject:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private when:Lcom/squareup/permissions/PermissionGatekeeper$When;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/permissions/EmployeeCacheUpdater;Lcom/squareup/permissions/PasscodesSettings;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->requested:Ljava/util/Set;

    .line 187
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->granted:Ljava/util/Set;

    const/4 v0, 0x0

    .line 191
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->shouldShowPermissionDeniedScreenSubject:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 199
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->shouldShowPermissionDeniedScreenSubject:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 200
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->skip(J)Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->sharedShouldBeShowingPermissionDeniedScreen:Lio/reactivex/Observable;

    .line 208
    iput-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->analytics:Lcom/squareup/analytics/Analytics;

    .line 209
    iput-object p2, p0, Lcom/squareup/permissions/PermissionGatekeeper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 210
    iput-object p3, p0, Lcom/squareup/permissions/PermissionGatekeeper;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 211
    iput-object p4, p0, Lcom/squareup/permissions/PermissionGatekeeper;->features:Lcom/squareup/settings/server/Features;

    .line 212
    iput-object p5, p0, Lcom/squareup/permissions/PermissionGatekeeper;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    .line 213
    iput-object p6, p0, Lcom/squareup/permissions/PermissionGatekeeper;->cacheUpdater:Lcom/squareup/permissions/EmployeeCacheUpdater;

    .line 214
    iput-object p7, p0, Lcom/squareup/permissions/PermissionGatekeeper;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/permissions/PermissionGatekeeper;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method private canSkipPasscodeReentry(Lcom/squareup/permissions/Permission;)Z
    .locals 4

    .line 464
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    .line 465
    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->PASSCODE_EMPLOYEE_MANAGEMENT:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 466
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isCurrentEmployeeAGuest()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 468
    :goto_0
    sget-object v1, Lcom/squareup/permissions/PermissionGatekeeper$1;->$SwitchMap$com$squareup$permissions$Permission:[I

    invoke-virtual {p1}, Lcom/squareup/permissions/Permission;->ordinal()I

    move-result p1

    aget p1, v1, p1

    if-eq p1, v2, :cond_1

    return v3

    .line 474
    :cond_1
    iget-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REQUIRE_EMPLOYEE_PASSCODE_TO_CANCEL_TRANSACTION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    .line 475
    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings;->getLatestState()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/permissions/PasscodesSettings;->isTeamPasscodeEnabled(Lcom/squareup/permissions/PasscodesSettings$State;)Z

    move-result p1

    if-nez p1, :cond_2

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :cond_3
    :goto_1
    return v2
.end method

.method static synthetic lambda$attemptPasscode$0(Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;Lio/reactivex/functions/Consumer;Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 392
    iget-boolean v0, p2, Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;->didAuthorize:Z

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p2, Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;->employee:Lcom/squareup/permissions/Employee;

    invoke-static {p0, v0}, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;->access$200(Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;Lcom/squareup/permissions/Employee;)V

    .line 395
    :cond_0
    iget-boolean p0, p2, Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;->didAuthorize:Z

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    invoke-interface {p1, p0}, Lio/reactivex/functions/Consumer;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$attemptPasscode$1(Lcom/squareup/permissions/Employee;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 p0, 0x1

    .line 399
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$attemptPasscode$2(Lcom/squareup/permissions/Employee;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 p0, 0x1

    .line 408
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$attemptPasscode$3(Lio/reactivex/functions/Consumer;Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 413
    iget-boolean p1, p1, Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;->didAuthorize:Z

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {p0, p1}, Lio/reactivex/functions/Consumer;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$attemptPasscode$4(Lio/reactivex/functions/Consumer;Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 416
    iget-boolean p1, p1, Lcom/squareup/permissions/PasscodeEmployeeManagement$PasscodeResult;->didAuthorize:Z

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {p0, p1}, Lio/reactivex/functions/Consumer;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method static permissionSetToString(Ljava/util/Set;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const-string v0, "permissions"

    .line 448
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonEmpty(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    .line 449
    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 451
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/permissions/Permission;

    .line 452
    invoke-virtual {v2}, Lcom/squareup/permissions/Permission;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const-string/jumbo p0, "|"

    .line 455
    invoke-static {v0, p0}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;Z)V
    .locals 0

    .line 354
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAnyAccessGranted(Ljava/util/Set;Lcom/squareup/permissions/PermissionGatekeeper$When;Z)V

    return-void
.end method

.method private runWhenAnyAccessGranted(Ljava/util/Set;Lcom/squareup/permissions/PermissionGatekeeper$When;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;",
            "Lcom/squareup/permissions/PermissionGatekeeper$When;",
            "Z)V"
        }
    .end annotation

    .line 360
    instance-of v0, p2, Lcom/squareup/permissions/PermissionGatekeeper$EmployeePasscodeUnlockWhen;

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    const-string v2, "Should only be using runWhenAnyAccessGranted() with PermissionGatekeeper.When"

    invoke-static {v0, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    const-string v0, "permissions"

    .line 362
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonEmpty(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    .line 363
    iput-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->requested:Ljava/util/Set;

    .line 366
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->canForcePasscodeReentry()Z

    move-result v0

    and-int/2addr p3, v0

    if-nez p3, :cond_1

    .line 368
    iget-object p3, p0, Lcom/squareup/permissions/PermissionGatekeeper;->granted:Ljava/util/Set;

    invoke-interface {p3}, Ljava/util/Set;->isEmpty()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-virtual {p0, p1}, Lcom/squareup/permissions/PermissionGatekeeper;->hasAnyPermission(Ljava/util/Set;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 369
    :cond_0
    iget-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {p1}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object p1

    iput-object p1, p2, Lcom/squareup/permissions/PermissionGatekeeper$When;->authorizedEmployee:Lcom/squareup/permissions/Employee;

    .line 370
    invoke-virtual {p2}, Lcom/squareup/permissions/PermissionGatekeeper$When;->success()V

    .line 371
    invoke-virtual {p2}, Lcom/squareup/permissions/PermissionGatekeeper$When;->resetAuthorizedEmployee()V

    goto :goto_1

    .line 373
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/permissions/PermissionGatekeeper;->shouldAskForPasscode()Z

    move-result p3

    if-eqz p3, :cond_2

    sget-object p3, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_AUTHORIZATION_PROMPT:Lcom/squareup/analytics/RegisterActionName;

    goto :goto_0

    :cond_2
    sget-object p3, Lcom/squareup/analytics/RegisterActionName;->EMPLOYEE_ACCESS_DENIED:Lcom/squareup/analytics/RegisterActionName;

    .line 376
    :goto_0
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/permissions/PasscodeAuthorizationEvent;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p3, v3}, Lcom/squareup/permissions/PasscodeAuthorizationEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 377
    new-instance p3, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;

    invoke-direct {p3, p0, p2, p1}, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;-><init>(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/PermissionGatekeeper$When;Ljava/util/Set;)V

    iput-object p3, p0, Lcom/squareup/permissions/PermissionGatekeeper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    .line 379
    iget-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->shouldShowPermissionDeniedScreenSubject:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method


# virtual methods
.method public allowAccountOwnerOrAdminPasscodeOnly()Z
    .locals 2

    .line 440
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    instance-of v1, v0, Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerOrAdminLoginWhen;

    if-nez v1, :cond_1

    instance-of v0, v0, Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerOrAdminPasscodeUnlockWhen;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public allowAccountOwnerPasscodeOnly()Z
    .locals 1

    .line 433
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    instance-of v0, v0, Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerPasscodeUnlockWhen;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public attemptPasscode(Ljava/lang/String;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/reactivex/functions/Consumer<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/disposables/Disposable;"
        }
    .end annotation

    .line 384
    invoke-static {}, Lio/reactivex/disposables/Disposables;->empty()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 385
    iget-object v1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    instance-of v2, v1, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;

    if-eqz v2, :cond_0

    .line 388
    check-cast v1, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;

    .line 389
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 390
    invoke-static {v1}, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;->access$100(Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, p1, v2}, Lcom/squareup/permissions/EmployeeManagement;->checkPermissionForPasscode(Ljava/lang/String;Ljava/util/Set;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/permissions/-$$Lambda$PermissionGatekeeper$nord-8IhFhyrcVST0q1IU1LKzqk;

    invoke-direct {v0, v1, p2}, Lcom/squareup/permissions/-$$Lambda$PermissionGatekeeper$nord-8IhFhyrcVST0q1IU1LKzqk;-><init>(Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;Lio/reactivex/functions/Consumer;)V

    .line 391
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    goto :goto_0

    .line 397
    :cond_0
    instance-of v2, v1, Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerOrAdminLoginWhen;

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    .line 398
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->setCurrentUserToOwnerWithPasscode(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object p1

    sget-object v0, Lcom/squareup/permissions/-$$Lambda$PermissionGatekeeper$26h3a6WkzK5uEMs_3BPCw1HZmrc;->INSTANCE:Lcom/squareup/permissions/-$$Lambda$PermissionGatekeeper$26h3a6WkzK5uEMs_3BPCw1HZmrc;

    .line 399
    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    .line 400
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    .line 401
    invoke-virtual {p1, p2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    goto :goto_0

    .line 402
    :cond_1
    instance-of v2, v1, Lcom/squareup/permissions/PermissionGatekeeper$EmployeePasscodeUnlockWhen;

    if-eqz v2, :cond_2

    .line 404
    check-cast v1, Lcom/squareup/permissions/PermissionGatekeeper$EmployeePasscodeUnlockWhen;

    invoke-virtual {v1}, Lcom/squareup/permissions/PermissionGatekeeper$EmployeePasscodeUnlockWhen;->getPermission()Lcom/squareup/permissions/Permission;

    move-result-object v0

    .line 405
    iget-object v1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 406
    invoke-virtual {v1, p1, v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->setCurrentUserToUserWithPasscodeAndPermission(Ljava/lang/String;Lcom/squareup/permissions/Permission;)Lio/reactivex/Maybe;

    move-result-object p1

    sget-object v0, Lcom/squareup/permissions/-$$Lambda$PermissionGatekeeper$VX2mvn3Uy6BoWdBXQ8115rs5ew8;->INSTANCE:Lcom/squareup/permissions/-$$Lambda$PermissionGatekeeper$VX2mvn3Uy6BoWdBXQ8115rs5ew8;

    .line 408
    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    .line 409
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->toSingle(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    .line 410
    invoke-virtual {p1, p2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    goto :goto_0

    .line 411
    :cond_2
    instance-of v2, v1, Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerOrAdminPasscodeUnlockWhen;

    if-eqz v2, :cond_3

    .line 412
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->checkIfOwnerPasscode(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/permissions/-$$Lambda$PermissionGatekeeper$8_P0WfZihWYPPod-AFB_IT2kOZI;

    invoke-direct {v0, p2}, Lcom/squareup/permissions/-$$Lambda$PermissionGatekeeper$8_P0WfZihWYPPod-AFB_IT2kOZI;-><init>(Lio/reactivex/functions/Consumer;)V

    .line 413
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    goto :goto_0

    .line 414
    :cond_3
    instance-of v1, v1, Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerPasscodeUnlockWhen;

    if-eqz v1, :cond_4

    .line 415
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->checkIfAccountOwnerPasscode(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/permissions/-$$Lambda$PermissionGatekeeper$krsMqjMJdFrblRihnH08L3Js08g;

    invoke-direct {v0, p2}, Lcom/squareup/permissions/-$$Lambda$PermissionGatekeeper$krsMqjMJdFrblRihnH08L3Js08g;-><init>(Lio/reactivex/functions/Consumer;)V

    .line 416
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    :cond_4
    :goto_0
    return-object v0
.end method

.method public dismiss(Z)V
    .locals 1

    const/4 v0, 0x0

    .line 222
    invoke-virtual {p0, p1, v0}, Lcom/squareup/permissions/PermissionGatekeeper;->dismiss(ZZ)V

    return-void
.end method

.method public dismiss(ZZ)V
    .locals 4

    if-nez p1, :cond_0

    .line 230
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    instance-of v1, v0, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;

    if-eqz v1, :cond_0

    .line 231
    check-cast v0, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;

    .line 232
    iget-object v1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/permissions/PasscodeAuthorizationEvent;

    sget-object v3, Lcom/squareup/analytics/RegisterActionName;->PASSCODE_AUTHORIZATION_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    .line 233
    invoke-static {v0}, Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;->access$100(Lcom/squareup/permissions/PermissionGatekeeper$LoggingPermissionWhenWrapper;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/permissions/PermissionGatekeeper;->permissionSetToString(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/squareup/permissions/PasscodeAuthorizationEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;)V

    .line 232
    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->shouldShowPermissionDeniedScreenSubject:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    .line 240
    iput-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    return-void

    :cond_1
    if-eqz p1, :cond_2

    .line 246
    :try_start_0
    iget-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->requested:Ljava/util/Set;

    iput-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->granted:Ljava/util/Set;

    .line 247
    iget-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    invoke-virtual {p1}, Lcom/squareup/permissions/PermissionGatekeeper$When;->success()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->granted:Ljava/util/Set;

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/permissions/PermissionGatekeeper;->granted:Ljava/util/Set;

    .line 250
    throw p1

    .line 252
    :cond_2
    iget-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    invoke-virtual {p1}, Lcom/squareup/permissions/PermissionGatekeeper$When;->failure()V

    .line 254
    :goto_0
    iget-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    invoke-virtual {p1}, Lcom/squareup/permissions/PermissionGatekeeper$When;->resetAuthorizedEmployee()V

    .line 255
    iput-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    return-void
.end method

.method public dismissPermissionDeniedScreen()V
    .locals 2

    .line 349
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->shouldShowPermissionDeniedScreenSubject:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public hasAnyPermission(Ljava/util/Set;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "permissions"

    .line 273
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonEmpty(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    .line 274
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->granted:Ljava/util/Set;

    invoke-static {v0, p1}, Lcom/squareup/util/SquareCollections;->setsIntersect(Ljava/util/Set;Ljava/util/Set;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0, p1}, Lcom/squareup/permissions/EmployeeManagement;->hasAnyPermission(Ljava/util/Set;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public hasPermission(Lcom/squareup/permissions/Permission;)Z
    .locals 1

    .line 264
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->granted:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0, p1}, Lcom/squareup/permissions/EmployeeManagement;->hasPermission(Lcom/squareup/permissions/Permission;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public loginEmployee(Lcom/squareup/permissions/PermissionGatekeeper$When;)V
    .locals 1

    .line 278
    instance-of v0, p1, Lcom/squareup/permissions/PermissionGatekeeper$EmployeePasscodeUnlockWhen;

    if-nez v0, :cond_1

    instance-of v0, p1, Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerOrAdminLoginWhen;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 280
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "When needs to be an instance of either EmployeePasscodeUnlockWhen or AccountOwnerOrAdminLoginWhen"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 283
    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    .line 286
    iget-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->cacheUpdater:Lcom/squareup/permissions/EmployeeCacheUpdater;

    invoke-virtual {p1}, Lcom/squareup/permissions/EmployeeCacheUpdater;->forceRefresh()V

    .line 288
    iget-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->shouldShowPermissionDeniedScreenSubject:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public runWhenAccessExplicitlyGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V
    .locals 0

    .line 318
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAnyAccessExplicitlyGranted(Ljava/util/Set;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V
    .locals 1

    const/4 v0, 0x0

    .line 292
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;Z)V

    return-void
.end method

.method public runWhenAccessGrantedByAccountOwner(Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerPasscodeUnlockWhen;)V
    .locals 4

    .line 335
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/permissions/PasscodeAuthorizationEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->ACCOUNT_OWNER_PASSCODE_AUTHORIZATION_PROMPT:Lcom/squareup/analytics/RegisterActionName;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/squareup/permissions/PasscodeAuthorizationEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 337
    iput-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    .line 339
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isAccountOwnerLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerPasscodeUnlockWhen;->authorizedEmployee:Lcom/squareup/permissions/Employee;

    .line 341
    invoke-virtual {p1}, Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerPasscodeUnlockWhen;->success()V

    .line 342
    invoke-virtual {p1}, Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerPasscodeUnlockWhen;->resetAuthorizedEmployee()V

    goto :goto_0

    .line 344
    :cond_0
    iget-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->shouldShowPermissionDeniedScreenSubject:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public runWhenAccessGrantedByOwner(Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerOrAdminPasscodeUnlockWhen;)V
    .locals 4

    .line 322
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/permissions/PasscodeAuthorizationEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->OWNER_PASSCODE_AUTHORIZATION_PROMPT:Lcom/squareup/analytics/RegisterActionName;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/squareup/permissions/PasscodeAuthorizationEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 323
    iput-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    .line 325
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isOwnerLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerOrAdminPasscodeUnlockWhen;->authorizedEmployee:Lcom/squareup/permissions/Employee;

    .line 327
    invoke-virtual {p1}, Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerOrAdminPasscodeUnlockWhen;->success()V

    .line 328
    invoke-virtual {p1}, Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerOrAdminPasscodeUnlockWhen;->resetAuthorizedEmployee()V

    goto :goto_0

    .line 330
    :cond_0
    iget-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->shouldShowPermissionDeniedScreenSubject:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public runWhenAnyAccessExplicitlyGranted(Ljava/util/Set;Lcom/squareup/permissions/PermissionGatekeeper$When;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;",
            "Lcom/squareup/permissions/PermissionGatekeeper$When;",
            ")V"
        }
    .end annotation

    .line 300
    sget-object v0, Lcom/squareup/permissions/Permission;->CANCEL_BUYER_FLOW:Lcom/squareup/permissions/Permission;

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->isTeamPermissionsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    .line 301
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->getLatestState()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/permissions/PasscodesSettings$State;->requirePasscodeWhenBackingOutOfSale:Z

    if-nez v0, :cond_0

    .line 302
    iget-object p1, p0, Lcom/squareup/permissions/PermissionGatekeeper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {p1}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object p1

    iput-object p1, p2, Lcom/squareup/permissions/PermissionGatekeeper$When;->authorizedEmployee:Lcom/squareup/permissions/Employee;

    .line 303
    invoke-virtual {p2}, Lcom/squareup/permissions/PermissionGatekeeper$When;->success()V

    .line 304
    invoke-virtual {p2}, Lcom/squareup/permissions/PermissionGatekeeper$When;->resetAuthorizedEmployee()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 307
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/permissions/Permission;

    .line 308
    invoke-direct {p0, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->canSkipPasscodeReentry(Lcom/squareup/permissions/Permission;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :cond_2
    xor-int/2addr v0, v3

    .line 313
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAnyAccessGranted(Ljava/util/Set;Lcom/squareup/permissions/PermissionGatekeeper$When;Z)V

    :goto_0
    return-void
.end method

.method public runWhenAnyAccessGranted(Ljava/util/Set;Lcom/squareup/permissions/PermissionGatekeeper$When;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;",
            "Lcom/squareup/permissions/PermissionGatekeeper$When;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 296
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAnyAccessGranted(Ljava/util/Set;Lcom/squareup/permissions/PermissionGatekeeper$When;Z)V

    return-void
.end method

.method public shouldAskForPasscode()Z
    .locals 2

    .line 422
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->when:Lcom/squareup/permissions/PermissionGatekeeper$When;

    instance-of v1, v0, Lcom/squareup/permissions/PermissionGatekeeper$EmployeePasscodeUnlockWhen;

    if-nez v1, :cond_1

    instance-of v0, v0, Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerOrAdminLoginWhen;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 429
    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->shouldAskForPasscode()Z

    move-result v0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public shouldBeShowingPermissionDeniedScreen()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 218
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper;->sharedShouldBeShowingPermissionDeniedScreen:Lio/reactivex/Observable;

    return-object v0
.end method
