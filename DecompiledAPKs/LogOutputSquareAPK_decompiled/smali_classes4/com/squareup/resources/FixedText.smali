.class public final Lcom/squareup/resources/FixedText;
.super Ljava/lang/Object;
.source "TextModels.kt"

# interfaces
.implements Lcom/squareup/resources/TextModel;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/resources/FixedText$Creator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/lang/CharSequence;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/resources/TextModel<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u0003B\u0012\u0012\u000b\u0010\u0004\u001a\u00078\u0000\u00a2\u0006\u0002\u0008\u0005\u00a2\u0006\u0002\u0010\u0006J\u0013\u0010\n\u001a\u00078\u0000\u00a2\u0006\u0002\u0008\u0005H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0008J#\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\r\u0008\u0002\u0010\u0004\u001a\u00078\u0000\u00a2\u0006\u0002\u0008\u0005H\u00c6\u0001\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\u001a\u0010\u0013\u001a\u00078\u0000\u00a2\u0006\u0002\u0008\u00052\u0006\u0010\u0014\u001a\u00020\u0015H\u0016\u00a2\u0006\u0002\u0010\u0016J\t\u0010\u0017\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\u0019\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u000eH\u00d6\u0001R\u0018\u0010\u0004\u001a\u00078\u0000\u00a2\u0006\u0002\u0008\u0005\u00a2\u0006\n\n\u0002\u0010\t\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/resources/FixedText;",
        "T",
        "",
        "Lcom/squareup/resources/TextModel;",
        "value",
        "Lkotlinx/android/parcel/RawValue;",
        "(Ljava/lang/CharSequence;)V",
        "getValue",
        "()Ljava/lang/CharSequence;",
        "Ljava/lang/CharSequence;",
        "component1",
        "copy",
        "(Ljava/lang/CharSequence;)Lcom/squareup/resources/FixedText;",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "evaluate",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)Ljava/lang/CharSequence;",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "resources_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final value:Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/resources/FixedText$Creator;

    invoke-direct {v0}, Lcom/squareup/resources/FixedText$Creator;-><init>()V

    sput-object v0, Lcom/squareup/resources/FixedText;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/resources/FixedText;->value:Ljava/lang/CharSequence;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/resources/FixedText;Ljava/lang/CharSequence;ILjava/lang/Object;)Lcom/squareup/resources/FixedText;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/resources/FixedText;->value:Ljava/lang/CharSequence;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/resources/FixedText;->copy(Ljava/lang/CharSequence;)Lcom/squareup/resources/FixedText;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/CharSequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/resources/FixedText;->value:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final copy(Ljava/lang/CharSequence;)Lcom/squareup/resources/FixedText;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/squareup/resources/FixedText<",
            "TT;>;"
        }
    .end annotation

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/resources/FixedText;

    invoke-direct {v0, p1}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/resources/FixedText;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/resources/FixedText;

    iget-object v0, p0, Lcom/squareup/resources/FixedText;->value:Ljava/lang/CharSequence;

    iget-object p1, p1, Lcom/squareup/resources/FixedText;->value:Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")TT;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object p1, p0, Lcom/squareup/resources/FixedText;->value:Ljava/lang/CharSequence;

    return-object p1
.end method

.method public final getValue()Ljava/lang/CharSequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/resources/FixedText;->value:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/resources/FixedText;->value:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FixedText(value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/resources/FixedText;->value:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/resources/FixedText;->value:Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    return-void
.end method
