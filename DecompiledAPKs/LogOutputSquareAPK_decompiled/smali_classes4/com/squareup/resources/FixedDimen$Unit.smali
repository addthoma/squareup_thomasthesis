.class public final enum Lcom/squareup/resources/FixedDimen$Unit;
.super Ljava/lang/Enum;
.source "DimenModels.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/resources/FixedDimen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Unit"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/resources/FixedDimen$Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/resources/FixedDimen$Unit;",
        "",
        "androidUnit",
        "",
        "(Ljava/lang/String;II)V",
        "getAndroidUnit$resources_release",
        "()I",
        "PX",
        "DP",
        "resources_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/resources/FixedDimen$Unit;

.field public static final enum DP:Lcom/squareup/resources/FixedDimen$Unit;

.field public static final enum PX:Lcom/squareup/resources/FixedDimen$Unit;


# instance fields
.field private final androidUnit:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/resources/FixedDimen$Unit;

    new-instance v1, Lcom/squareup/resources/FixedDimen$Unit;

    const/4 v2, 0x0

    const-string v3, "PX"

    .line 46
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/resources/FixedDimen$Unit;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/resources/FixedDimen$Unit;->PX:Lcom/squareup/resources/FixedDimen$Unit;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/resources/FixedDimen$Unit;

    const/4 v2, 0x1

    const-string v3, "DP"

    .line 47
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/resources/FixedDimen$Unit;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/resources/FixedDimen$Unit;->DP:Lcom/squareup/resources/FixedDimen$Unit;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/resources/FixedDimen$Unit;->$VALUES:[Lcom/squareup/resources/FixedDimen$Unit;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/resources/FixedDimen$Unit;->androidUnit:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/resources/FixedDimen$Unit;
    .locals 1

    const-class v0, Lcom/squareup/resources/FixedDimen$Unit;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/resources/FixedDimen$Unit;

    return-object p0
.end method

.method public static values()[Lcom/squareup/resources/FixedDimen$Unit;
    .locals 1

    sget-object v0, Lcom/squareup/resources/FixedDimen$Unit;->$VALUES:[Lcom/squareup/resources/FixedDimen$Unit;

    invoke-virtual {v0}, [Lcom/squareup/resources/FixedDimen$Unit;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/resources/FixedDimen$Unit;

    return-object v0
.end method


# virtual methods
.method public final getAndroidUnit$resources_release()I
    .locals 1

    .line 45
    iget v0, p0, Lcom/squareup/resources/FixedDimen$Unit;->androidUnit:I

    return v0
.end method
