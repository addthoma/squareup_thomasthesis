.class public final Lcom/squareup/printer/epson/RealEpsonPrinterScouts;
.super Ljava/lang/Object;
.source "RealEpsonPrinterScouts.kt"

# interfaces
.implements Lcom/squareup/print/EpsonPrinterScouts;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealEpsonPrinterScouts.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealEpsonPrinterScouts.kt\ncom/squareup/printer/epson/RealEpsonPrinterScouts\n*L\n1#1,23:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/printer/epson/RealEpsonPrinterScouts;",
        "Lcom/squareup/print/EpsonPrinterScouts;",
        "epsonTcpPrinterScout",
        "Lcom/squareup/print/EpsonTcpPrinterScout;",
        "epsonUsbPrinterScout",
        "Lcom/squareup/print/EpsonUsbPrinterScout;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/print/EpsonTcpPrinterScout;Lcom/squareup/print/EpsonUsbPrinterScout;Lcom/squareup/settings/server/Features;)V",
        "scouts",
        "",
        "Lcom/squareup/print/PrinterScout;",
        "getScouts",
        "()Ljava/util/List;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final epsonTcpPrinterScout:Lcom/squareup/print/EpsonTcpPrinterScout;

.field private final epsonUsbPrinterScout:Lcom/squareup/print/EpsonUsbPrinterScout;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/print/EpsonTcpPrinterScout;Lcom/squareup/print/EpsonUsbPrinterScout;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "epsonTcpPrinterScout"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "epsonUsbPrinterScout"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/printer/epson/RealEpsonPrinterScouts;->epsonTcpPrinterScout:Lcom/squareup/print/EpsonTcpPrinterScout;

    iput-object p2, p0, Lcom/squareup/printer/epson/RealEpsonPrinterScouts;->epsonUsbPrinterScout:Lcom/squareup/print/EpsonUsbPrinterScout;

    iput-object p3, p0, Lcom/squareup/printer/epson/RealEpsonPrinterScouts;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public getScouts()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterScout;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/printer/epson/RealEpsonPrinterScouts;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_EPSON_PRINTERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/print/PrinterScout;

    const/4 v1, 0x0

    .line 18
    iget-object v2, p0, Lcom/squareup/printer/epson/RealEpsonPrinterScouts;->epsonTcpPrinterScout:Lcom/squareup/print/EpsonTcpPrinterScout;

    check-cast v2, Lcom/squareup/print/PrinterScout;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/squareup/printer/epson/RealEpsonPrinterScouts;->epsonUsbPrinterScout:Lcom/squareup/print/EpsonUsbPrinterScout;

    check-cast v2, Lcom/squareup/print/PrinterScout;

    aput-object v2, v0, v1

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 20
    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method
