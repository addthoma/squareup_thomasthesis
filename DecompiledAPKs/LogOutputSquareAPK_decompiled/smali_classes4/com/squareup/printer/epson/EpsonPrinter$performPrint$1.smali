.class final Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "EpsonPrinter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/printer/epson/EpsonPrinter;->performPrint(Landroid/graphics/Bitmap;Lcom/squareup/print/PrintTimingData;)Lcom/squareup/print/PrintJob$PrintAttempt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lcom/squareup/print/PrintJob$PrintAttempt;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u008a@\u00a2\u0006\u0004\u0008\u0003\u0010\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/print/PrintJob$PrintAttempt;",
        "Lkotlinx/coroutines/CoroutineScope;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.printer.epson.EpsonPrinter$performPrint$1"
    f = "EpsonPrinter.kt"
    i = {
        0x0
    }
    l = {
        0x3d
    }
    m = "invokeSuspend"
    n = {
        "$this$runBlocking"
    }
    s = {
        "L$0"
    }
.end annotation


# instance fields
.field final synthetic $bitmap:Landroid/graphics/Bitmap;

.field final synthetic $printTimingData:Lcom/squareup/print/PrintTimingData;

.field L$0:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;

.field final synthetic this$0:Lcom/squareup/printer/epson/EpsonPrinter;


# direct methods
.method constructor <init>(Lcom/squareup/printer/epson/EpsonPrinter;Lcom/squareup/print/PrintTimingData;Landroid/graphics/Bitmap;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinter;

    iput-object p2, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->$printTimingData:Lcom/squareup/print/PrintTimingData;

    iput-object p3, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->$bitmap:Landroid/graphics/Bitmap;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p4}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinter;

    iget-object v2, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->$printTimingData:Lcom/squareup/print/PrintTimingData;

    iget-object v3, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->$bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, v2, v3, p2}, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;-><init>(Lcom/squareup/printer/epson/EpsonPrinter;Lcom/squareup/print/PrintTimingData;Landroid/graphics/Bitmap;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 54
    iget v1, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->label:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->L$0:Ljava/lang/Object;

    check-cast v0, Lkotlinx/coroutines/CoroutineScope;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_0

    .line 71
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 54
    :cond_1
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    .line 56
    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinter;

    invoke-static {v1}, Lcom/squareup/printer/epson/EpsonPrinter;->access$getEpsonPrinterConnection$p(Lcom/squareup/printer/epson/EpsonPrinter;)Lcom/squareup/printer/epson/EpsonPrinterConnection;

    move-result-object v3

    .line 57
    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinter;

    invoke-static {v1}, Lcom/squareup/printer/epson/EpsonPrinter;->access$getEpsonPrinterSdk$p(Lcom/squareup/printer/epson/EpsonPrinter;)Lcom/squareup/printer/epson/EpsonPrinterSdk;

    move-result-object v4

    .line 58
    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinter;

    invoke-static {v1}, Lcom/squareup/printer/epson/EpsonPrinter;->access$getEpsonPrinterInfo$p(Lcom/squareup/printer/epson/EpsonPrinter;)Lcom/squareup/printer/epson/EpsonPrinterInfo;

    move-result-object v5

    .line 59
    iget-object v6, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->$printTimingData:Lcom/squareup/print/PrintTimingData;

    .line 60
    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinter;

    invoke-static {v1}, Lcom/squareup/printer/epson/EpsonPrinter;->access$getClock$p(Lcom/squareup/printer/epson/EpsonPrinter;)Lcom/squareup/util/Clock;

    move-result-object v7

    const-wide/16 v8, 0x0

    .line 61
    new-instance v1, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1$sendPrintDataResult$1;

    invoke-direct {v1, p0}, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1$sendPrintDataResult$1;-><init>(Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;)V

    move-object v10, v1

    check-cast v10, Lkotlin/jvm/functions/Function1;

    const/16 v12, 0x10

    const/4 v13, 0x0

    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->L$0:Ljava/lang/Object;

    iput v2, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->label:I

    move-object v11, p0

    .line 56
    invoke-static/range {v3 .. v13}, Lcom/squareup/printer/epson/EpsonPrinterConnection;->sendPrintData$default(Lcom/squareup/printer/epson/EpsonPrinterConnection;Lcom/squareup/printer/epson/EpsonPrinterSdk;Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;JLkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_2

    return-object v0

    .line 55
    :cond_2
    :goto_0
    check-cast p1, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;

    .line 68
    new-instance v0, Lcom/squareup/print/PrintJob$PrintAttempt;

    .line 69
    invoke-virtual {p1}, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->getPrintResult()Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinter;

    invoke-virtual {v2}, Lcom/squareup/printer/epson/EpsonPrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;->toString()Ljava/lang/String;

    move-result-object p1

    .line 70
    iget-object v3, p0, Lcom/squareup/printer/epson/EpsonPrinter$performPrint$1;->$printTimingData:Lcom/squareup/print/PrintTimingData;

    .line 68
    invoke-direct {v0, v1, v2, p1, v3}, Lcom/squareup/print/PrintJob$PrintAttempt;-><init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;Lcom/squareup/print/PrintTimingData;)V

    return-object v0
.end method
