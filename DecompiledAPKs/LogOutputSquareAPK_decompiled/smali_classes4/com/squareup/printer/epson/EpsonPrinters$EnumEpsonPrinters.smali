.class public final enum Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;
.super Ljava/lang/Enum;
.source "EpsonPrinters.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/printer/epson/EpsonPrinters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumEpsonPrinters"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;",
        "",
        "attributes",
        "Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;",
        "(Ljava/lang/String;ILcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;)V",
        "getAttributes",
        "()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;",
        "TM_M10",
        "TM_M30",
        "TM_T20",
        "TM_T82",
        "TM_T88",
        "TM_U220",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

.field public static final enum TM_M10:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

.field public static final enum TM_M30:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

.field public static final enum TM_T20:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

.field public static final enum TM_T82:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

.field public static final enum TM_T88:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

.field public static final enum TM_U220:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;


# instance fields
.field private final attributes:Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x6

    new-array v1, v0, [Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    new-instance v2, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    .line 70
    new-instance v3, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    .line 73
    sget-object v4, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;->TWO_INCH:Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    const/4 v5, 0x0

    const-string v6, "TM-m10"

    .line 70
    invoke-direct {v3, v6, v5, v4, v5}, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;-><init>(Ljava/lang/String;ILcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;Z)V

    const-string v4, "TM_M10"

    .line 69
    invoke-direct {v2, v4, v5, v3}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;-><init>(Ljava/lang/String;ILcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;)V

    sput-object v2, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_M10:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    aput-object v2, v1, v5

    new-instance v2, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    .line 78
    new-instance v3, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    .line 81
    sget-object v4, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;->THREE_INCH_HIGH_DPI:Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    const/4 v6, 0x1

    const-string v7, "TM-m30"

    .line 78
    invoke-direct {v3, v7, v6, v4, v5}, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;-><init>(Ljava/lang/String;ILcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;Z)V

    const-string v4, "TM_M30"

    .line 77
    invoke-direct {v2, v4, v6, v3}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;-><init>(Ljava/lang/String;ILcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;)V

    sput-object v2, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_M30:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    aput-object v2, v1, v6

    new-instance v2, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    .line 86
    new-instance v3, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    .line 89
    sget-object v4, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;->THREE_INCH_HIGH_DPI:Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    const-string v7, "TM-T20"

    .line 86
    invoke-direct {v3, v7, v0, v4, v5}, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;-><init>(Ljava/lang/String;ILcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;Z)V

    const/4 v0, 0x2

    const-string v4, "TM_T20"

    .line 85
    invoke-direct {v2, v4, v0, v3}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;-><init>(Ljava/lang/String;ILcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;)V

    sput-object v2, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_T20:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    aput-object v2, v1, v0

    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    .line 94
    new-instance v2, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    .line 97
    sget-object v3, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;->THREE_INCH_HIGH_DPI:Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    const-string v4, "TM-T82"

    const/16 v7, 0xa

    .line 94
    invoke-direct {v2, v4, v7, v3, v5}, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;-><init>(Ljava/lang/String;ILcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;Z)V

    const/4 v3, 0x3

    const-string v4, "TM_T82"

    .line 93
    invoke-direct {v0, v4, v3, v2}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;-><init>(Ljava/lang/String;ILcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;)V

    sput-object v0, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_T82:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    aput-object v0, v1, v3

    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    .line 102
    new-instance v2, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    .line 105
    sget-object v3, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;->THREE_INCH_LOW_DPI:Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    const-string v4, "TM-T88"

    const/16 v7, 0xc

    .line 102
    invoke-direct {v2, v4, v7, v3, v5}, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;-><init>(Ljava/lang/String;ILcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;Z)V

    const/4 v3, 0x4

    const-string v4, "TM_T88"

    .line 101
    invoke-direct {v0, v4, v3, v2}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;-><init>(Ljava/lang/String;ILcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;)V

    sput-object v0, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_T88:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    aput-object v0, v1, v3

    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    .line 110
    new-instance v2, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    .line 113
    sget-object v3, Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;->THREE_INCH_LOW_DPI:Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;

    const-string v4, "TM-U220"

    const/16 v5, 0xf

    .line 110
    invoke-direct {v2, v4, v5, v3, v6}, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;-><init>(Ljava/lang/String;ILcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;Z)V

    const/4 v3, 0x5

    const-string v4, "TM_U220"

    .line 109
    invoke-direct {v0, v4, v3, v2}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;-><init>(Ljava/lang/String;ILcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;)V

    sput-object v0, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_U220:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    aput-object v0, v1, v3

    sput-object v1, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->$VALUES:[Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;",
            ")V"
        }
    .end annotation

    .line 68
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->attributes:Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;
    .locals 1

    const-class v0, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    return-object p0
.end method

.method public static values()[Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;
    .locals 1

    sget-object v0, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->$VALUES:[Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v0}, [Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    return-object v0
.end method


# virtual methods
.method public final getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->attributes:Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    return-object v0
.end method
