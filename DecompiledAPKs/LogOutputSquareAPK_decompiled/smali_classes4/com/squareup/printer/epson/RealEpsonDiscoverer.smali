.class public final Lcom/squareup/printer/epson/RealEpsonDiscoverer;
.super Ljava/lang/Object;
.source "RealEpsonDiscoverer.kt"

# interfaces
.implements Lcom/squareup/printer/epson/EpsonDiscoverer;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealEpsonDiscoverer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealEpsonDiscoverer.kt\ncom/squareup/printer/epson/RealEpsonDiscoverer\n*L\n1#1,95:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J\u0010\u0010\r\u001a\u00020\u000c2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u001f\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u00112\u0006\u0010\u0013\u001a\u00020\u0014H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0015J\u0008\u0010\u0016\u001a\u00020\u0017H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/printer/epson/RealEpsonDiscoverer;",
        "Lcom/squareup/printer/epson/EpsonDiscoverer;",
        "epsonPrinterFactory",
        "Lcom/squareup/printer/epson/EpsonPrinter$Factory;",
        "context",
        "Landroid/app/Application;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/printer/epson/EpsonPrinter$Factory;Landroid/app/Application;Lcom/squareup/settings/server/Features;)V",
        "getConnectionType",
        "Lcom/squareup/print/ConnectionType;",
        "target",
        "",
        "getTarget",
        "deviceInfo",
        "Lcom/epson/epos2/discovery/DeviceInfo;",
        "startEpsonDiscovery",
        "",
        "Lcom/squareup/print/HardwarePrinter;",
        "durationMs",
        "",
        "(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "stopEpsonDiscovery",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final context:Landroid/app/Application;

.field private final epsonPrinterFactory:Lcom/squareup/printer/epson/EpsonPrinter$Factory;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/printer/epson/EpsonPrinter$Factory;Landroid/app/Application;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "epsonPrinterFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer;->epsonPrinterFactory:Lcom/squareup/printer/epson/EpsonPrinter$Factory;

    iput-object p2, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer;->context:Landroid/app/Application;

    iput-object p3, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static final synthetic access$getConnectionType(Lcom/squareup/printer/epson/RealEpsonDiscoverer;Ljava/lang/String;)Lcom/squareup/print/ConnectionType;
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/printer/epson/RealEpsonDiscoverer;->getConnectionType(Ljava/lang/String;)Lcom/squareup/print/ConnectionType;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getEpsonPrinterFactory$p(Lcom/squareup/printer/epson/RealEpsonDiscoverer;)Lcom/squareup/printer/epson/EpsonPrinter$Factory;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer;->epsonPrinterFactory:Lcom/squareup/printer/epson/EpsonPrinter$Factory;

    return-object p0
.end method

.method public static final synthetic access$getTarget(Lcom/squareup/printer/epson/RealEpsonDiscoverer;Lcom/epson/epos2/discovery/DeviceInfo;)Ljava/lang/String;
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/printer/epson/RealEpsonDiscoverer;->getTarget(Lcom/epson/epos2/discovery/DeviceInfo;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private final getConnectionType(Ljava/lang/String;)Lcom/squareup/print/ConnectionType;
    .locals 4

    const-string v0, "TCP"

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    .line 88
    invoke-static {p1, v0, v1, v2, v3}, Lkotlin/text/StringsKt;->startsWith$default(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/print/ConnectionType;->TCP:Lcom/squareup/print/ConnectionType;

    return-object p1

    .line 90
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 91
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown or unsupported Epson connection type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 90
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final getTarget(Lcom/epson/epos2/discovery/DeviceInfo;)Ljava/lang/String;
    .locals 5

    .line 74
    invoke-virtual {p1}, Lcom/epson/epos2/discovery/DeviceInfo;->getTarget()Ljava/lang/String;

    move-result-object v0

    const-string v1, "deviceInfo.target"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "TCP"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lkotlin/text/StringsKt;->startsWith$default(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    invoke-virtual {p1}, Lcom/epson/epos2/discovery/DeviceInfo;->getIpAddress()Ljava/lang/String;

    move-result-object v0

    const-string v1, "deviceInfo.ipAddress"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TCP:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/epson/epos2/discovery/DeviceInfo;->getIpAddress()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 78
    :cond_0
    invoke-virtual {p1}, Lcom/epson/epos2/discovery/DeviceInfo;->getTarget()Ljava/lang/String;

    move-result-object p1

    :goto_0
    const-string v0, "if (deviceInfo.ipAddress\u2026deviceInfo.target\n      }"

    .line 75
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 81
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 82
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown or unsupported Epson target: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 81
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public startEpsonDiscovery(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    instance-of v0, p3, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$1;

    if-eqz v0, :cond_0

    move-object v0, p3

    check-cast v0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$1;

    iget v1, v0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$1;->label:I

    const/high16 v2, -0x80000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    iget p3, v0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$1;->label:I

    sub-int/2addr p3, v2

    iput p3, v0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$1;->label:I

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$1;

    invoke-direct {v0, p0, p3}, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$1;-><init>(Lcom/squareup/printer/epson/RealEpsonDiscoverer;Lkotlin/coroutines/Continuation;)V

    :goto_0
    iget-object p3, v0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v1

    .line 23
    iget v2, v0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$1;->label:I

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    if-ne v2, v3, :cond_1

    iget-object p1, v0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$1;->L$2:Ljava/lang/Object;

    check-cast p1, Lcom/epson/epos2/discovery/FilterOption;

    iget-object p1, v0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$1;->L$1:Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    iget-wide v1, v0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$1;->J$0:J

    iget-object p2, v0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$1;->L$0:Ljava/lang/Object;

    check-cast p2, Lcom/squareup/printer/epson/RealEpsonDiscoverer;

    invoke-static {p3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_2

    .line 56
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 23
    :cond_2
    invoke-static {p3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 26
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    check-cast p3, Ljava/util/List;

    .line 27
    new-instance v2, Lcom/epson/epos2/discovery/FilterOption;

    invoke-direct {v2}, Lcom/epson/epos2/discovery/FilterOption;-><init>()V

    .line 28
    invoke-virtual {v2, v3}, Lcom/epson/epos2/discovery/FilterOption;->setDeviceType(I)V

    const/4 v4, 0x0

    .line 29
    invoke-virtual {v2, v4}, Lcom/epson/epos2/discovery/FilterOption;->setEpsonFilter(I)V

    .line 30
    invoke-virtual {v2, v3}, Lcom/epson/epos2/discovery/FilterOption;->setPortType(I)V

    .line 33
    :try_start_0
    iget-object v5, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer;->context:Landroid/app/Application;

    check-cast v5, Landroid/content/Context;

    new-instance v6, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$2;

    invoke-direct {v6, p0, p3}, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$2;-><init>(Lcom/squareup/printer/epson/RealEpsonDiscoverer;Ljava/util/List;)V

    check-cast v6, Lcom/epson/epos2/discovery/DiscoveryListener;

    invoke-static {v5, v2, v6}, Lcom/epson/epos2/discovery/Discovery;->start(Landroid/content/Context;Lcom/epson/epos2/discovery/FilterOption;Lcom/epson/epos2/discovery/DiscoveryListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v5

    .line 50
    iget-object v6, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer;->features:Lcom/squareup/settings/server/Features;

    sget-object v7, Lcom/squareup/settings/server/Features$Feature;->EPSON_DEBUGGING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v6, v7}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 51
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Epson start discovery failed: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v5, v4}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    :cond_3
    :goto_1
    iput-object p0, v0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$1;->L$0:Ljava/lang/Object;

    iput-wide p1, v0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$1;->J$0:J

    iput-object p3, v0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$1;->L$1:Ljava/lang/Object;

    iput-object v2, v0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$1;->L$2:Ljava/lang/Object;

    iput v3, v0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$1;->label:I

    invoke-static {p1, p2, v0}, Lkotlinx/coroutines/DelayKt;->delay(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v1, :cond_4

    return-object v1

    :cond_4
    move-object p2, p0

    move-object p1, p3

    .line 55
    :goto_2
    invoke-virtual {p2}, Lcom/squareup/printer/epson/RealEpsonDiscoverer;->stopEpsonDiscovery()V

    return-object p1
.end method

.method public stopEpsonDiscovery()V
    .locals 4

    .line 61
    :try_start_0
    invoke-static {}, Lcom/epson/epos2/discovery/Discovery;->stop()V
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 63
    iget-object v1, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->EPSON_DEBUGGING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    move-object v1, v0

    check-cast v1, Ljava/lang/Throwable;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Epson stop discovery failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Ltimber/log/Timber;->w(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void
.end method
