.class public final Lcom/squareup/printer/epson/EpsonPrinters;
.super Ljava/lang/Object;
.source "EpsonPrinters.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/printer/epson/EpsonPrinters$PrinterSpecs;,
        Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;,
        Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0008\u00c6\u0002\u0018\u00002\u00020\u0001:\u0003\u000c\r\u000eB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u001d\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u001d\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00060\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u0008\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/printer/epson/EpsonPrinters;",
        "",
        "()V",
        "epsonTcpDeviceNameMapping",
        "",
        "",
        "Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;",
        "getEpsonTcpDeviceNameMapping",
        "()Ljava/util/Map;",
        "epsonUsbDeviceMapping",
        "",
        "getEpsonUsbDeviceMapping",
        "EnumEpsonPrinters",
        "EpsonPrinterAttributes",
        "PrinterSpecs",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/printer/epson/EpsonPrinters;

.field private static final epsonTcpDeviceNameMapping:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;",
            ">;"
        }
    .end annotation
.end field

.field private static final epsonUsbDeviceMapping:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 14
    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinters;

    invoke-direct {v0}, Lcom/squareup/printer/epson/EpsonPrinters;-><init>()V

    sput-object v0, Lcom/squareup/printer/epson/EpsonPrinters;->INSTANCE:Lcom/squareup/printer/epson/EpsonPrinters;

    const/16 v0, 0xa

    new-array v1, v0, [Lkotlin/Pair;

    const/16 v2, 0xe1f

    .line 22
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_M10:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v3}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/16 v2, 0xe20

    .line 23
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v4, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_M30:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v4}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v4

    invoke-static {v2, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const/16 v2, 0xe03

    .line 24
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v5, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_T20:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v5}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v5

    invoke-static {v2, v5}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    const/4 v5, 0x2

    aput-object v2, v1, v5

    const/16 v2, 0xe15

    .line 25
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v6, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_T20:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v6}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v6

    invoke-static {v2, v6}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    const/4 v6, 0x3

    aput-object v2, v1, v6

    const/16 v2, 0xe27

    .line 26
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v7, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_T82:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v7}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v7

    invoke-static {v2, v7}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    const/4 v7, 0x4

    aput-object v2, v1, v7

    const/16 v2, 0xe11

    .line 27
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v8, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_T82:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v8}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v8

    invoke-static {v2, v8}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    const/4 v8, 0x5

    aput-object v2, v1, v8

    const/16 v2, 0xe28

    .line 28
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v9, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_T82:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v9}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v9

    invoke-static {v2, v9}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    const/4 v9, 0x6

    aput-object v2, v1, v9

    const/16 v2, 0x202

    .line 29
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v10, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_T88:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v10}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v10

    invoke-static {v2, v10}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    const/4 v10, 0x7

    aput-object v2, v1, v10

    const/16 v2, 0xe02

    .line 30
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v11, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_T88:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v11}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v11

    invoke-static {v2, v11}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    const/16 v11, 0x8

    aput-object v2, v1, v11

    const/16 v2, 0xe21

    .line 31
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v12, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_T88:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v12}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v12

    invoke-static {v2, v12}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    const/16 v12, 0x9

    aput-object v2, v1, v12

    .line 21
    invoke-static {v1}, Lkotlin/collections/MapsKt;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v1

    sput-object v1, Lcom/squareup/printer/epson/EpsonPrinters;->epsonUsbDeviceMapping:Ljava/util/Map;

    new-array v0, v0, [Lkotlin/Pair;

    .line 35
    sget-object v1, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_M10:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v1

    const-string v2, "TM-m10"

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v3

    .line 36
    sget-object v1, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_M30:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v1

    const-string v2, "TM-m30"

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v4

    .line 37
    sget-object v1, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_T20:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v1

    const-string v2, "TM-T20II"

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v5

    .line 38
    sget-object v1, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_T20:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v1

    const-string v2, "TM-T20X"

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v6

    .line 39
    sget-object v1, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_T82:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v1

    const-string v2, "TM-T82II"

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v7

    .line 40
    sget-object v1, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_T82:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v1

    const-string v2, "TM-T82III"

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v8

    .line 41
    sget-object v1, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_T82:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v1

    const-string v2, "TM-T82IIIL"

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v9

    .line 42
    sget-object v1, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_T88:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v1

    const-string v2, "TM-T88V"

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v10

    .line 43
    sget-object v1, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_T88:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v1

    const-string v2, "TM-T88VI"

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v11

    .line 44
    sget-object v1, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->TM_U220:Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;

    invoke-virtual {v1}, Lcom/squareup/printer/epson/EpsonPrinters$EnumEpsonPrinters;->getAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    move-result-object v1

    const-string v2, "TM-U220"

    invoke-static {v2, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v0, v12

    .line 34
    invoke-static {v0}, Lkotlin/collections/MapsKt;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/squareup/printer/epson/EpsonPrinters;->epsonTcpDeviceNameMapping:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getEpsonTcpDeviceNameMapping()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;",
            ">;"
        }
    .end annotation

    .line 34
    sget-object v0, Lcom/squareup/printer/epson/EpsonPrinters;->epsonTcpDeviceNameMapping:Ljava/util/Map;

    return-object v0
.end method

.method public final getEpsonUsbDeviceMapping()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;",
            ">;"
        }
    .end annotation

    .line 21
    sget-object v0, Lcom/squareup/printer/epson/EpsonPrinters;->epsonUsbDeviceMapping:Ljava/util/Map;

    return-object v0
.end method
