.class public final enum Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;
.super Ljava/lang/Enum;
.source "EpsonCallbackMapper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/printer/epson/EpsonCallbackMapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EpsonExceptionCode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0015\u0008\u0087\u0001\u0018\u0000 \u00172\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0017B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;",
        "",
        "code",
        "",
        "(Ljava/lang/String;II)V",
        "getCode",
        "()I",
        "ERR_PARAM",
        "ERR_CONNECT",
        "ERR_TIMEOUT",
        "ERR_MEMORY",
        "ERR_ILLEGAL",
        "ERR_PROCESSING",
        "ERR_NOT_FOUND",
        "ERR_IN_USE",
        "ERR_TYPE_INVALID",
        "ERR_DISCONNECT",
        "ERR_ALREADY_OPENED",
        "ERR_ALREADY_USED",
        "ERR_BOX_COUNT_OVER",
        "ERR_BOX_CLIENT_OVER",
        "ERR_UNSUPPORTED",
        "ERR_FAILURE",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

.field public static final Companion:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode$Companion;

.field public static final enum ERR_ALREADY_OPENED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

.field public static final enum ERR_ALREADY_USED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

.field public static final enum ERR_BOX_CLIENT_OVER:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

.field public static final enum ERR_BOX_COUNT_OVER:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

.field public static final enum ERR_CONNECT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

.field public static final enum ERR_DISCONNECT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

.field public static final enum ERR_FAILURE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

.field public static final enum ERR_ILLEGAL:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

.field public static final enum ERR_IN_USE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

.field public static final enum ERR_MEMORY:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

.field public static final enum ERR_NOT_FOUND:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

.field public static final enum ERR_PARAM:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

.field public static final enum ERR_PROCESSING:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

.field public static final enum ERR_TIMEOUT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

.field public static final enum ERR_TYPE_INVALID:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

.field public static final enum ERR_UNSUPPORTED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;


# instance fields
.field private final code:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    const/4 v2, 0x1

    const-string v3, "ERR_PARAM"

    const/4 v4, 0x0

    .line 110
    invoke-direct {v1, v3, v4, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_PARAM:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    const/4 v3, 0x0

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    const/4 v3, 0x2

    const-string v4, "ERR_CONNECT"

    .line 111
    invoke-direct {v1, v4, v2, v3}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_CONNECT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    const/4 v2, 0x3

    const-string v4, "ERR_TIMEOUT"

    .line 112
    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_TIMEOUT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    const/4 v3, 0x4

    const-string v4, "ERR_MEMORY"

    .line 113
    invoke-direct {v1, v4, v2, v3}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_MEMORY:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    const/4 v2, 0x5

    const-string v4, "ERR_ILLEGAL"

    .line 114
    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_ILLEGAL:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    const/4 v3, 0x6

    const-string v4, "ERR_PROCESSING"

    .line 115
    invoke-direct {v1, v4, v2, v3}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_PROCESSING:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    const/4 v2, 0x7

    const-string v4, "ERR_NOT_FOUND"

    .line 116
    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_NOT_FOUND:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    const/16 v3, 0x8

    const-string v4, "ERR_IN_USE"

    .line 117
    invoke-direct {v1, v4, v2, v3}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_IN_USE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    const/16 v2, 0x9

    const-string v4, "ERR_TYPE_INVALID"

    .line 118
    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_TYPE_INVALID:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    const/16 v3, 0xa

    const-string v4, "ERR_DISCONNECT"

    .line 119
    invoke-direct {v1, v4, v2, v3}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_DISCONNECT:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    const/16 v2, 0xb

    const-string v4, "ERR_ALREADY_OPENED"

    .line 120
    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_ALREADY_OPENED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    const/16 v3, 0xc

    const-string v4, "ERR_ALREADY_USED"

    .line 121
    invoke-direct {v1, v4, v2, v3}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_ALREADY_USED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    const/16 v2, 0xd

    const-string v4, "ERR_BOX_COUNT_OVER"

    .line 122
    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_BOX_COUNT_OVER:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    const/16 v3, 0xe

    const-string v4, "ERR_BOX_CLIENT_OVER"

    .line 123
    invoke-direct {v1, v4, v2, v3}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_BOX_CLIENT_OVER:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    const/16 v2, 0xf

    const-string v4, "ERR_UNSUPPORTED"

    .line 124
    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_UNSUPPORTED:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    const-string v3, "ERR_FAILURE"

    const/16 v4, 0xff

    .line 125
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->ERR_FAILURE:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->$VALUES:[Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    new-instance v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->Companion:Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 109
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->code:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;
    .locals 1

    const-class v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;
    .locals 1

    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->$VALUES:[Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    invoke-virtual {v0}, [Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;

    return-object v0
.end method


# virtual methods
.method public final getCode()I
    .locals 1

    .line 109
    iget v0, p0, Lcom/squareup/printer/epson/EpsonCallbackMapper$EpsonExceptionCode;->code:I

    return v0
.end method
