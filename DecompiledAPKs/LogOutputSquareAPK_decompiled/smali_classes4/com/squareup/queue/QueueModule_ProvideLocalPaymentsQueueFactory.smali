.class public final Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsQueueFactory;
.super Ljava/lang/Object;
.source "QueueModule_ProvideLocalPaymentsQueueFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/retrofit/RetrofitQueue;",
        ">;"
    }
.end annotation


# instance fields
.field private final directoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final localPaymentsQueueCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;>;"
        }
    .end annotation
.end field

.field private final tasksQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsQueueFactory;->featuresProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsQueueFactory;->directoryProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsQueueFactory;->localPaymentsQueueCacheProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsQueueFactory;->tasksQueueProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsQueueFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;)",
            "Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsQueueFactory;"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsQueueFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsQueueFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideLocalPaymentsQueue(Lcom/squareup/settings/server/Features;Ljava/io/File;Lcom/squareup/queue/retrofit/QueueCache;Lcom/squareup/queue/retrofit/RetrofitQueue;)Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Ljava/io/File;",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ")",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;"
        }
    .end annotation

    .line 53
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/queue/QueueModule;->provideLocalPaymentsQueue(Lcom/squareup/settings/server/Features;Ljava/io/File;Lcom/squareup/queue/retrofit/QueueCache;Lcom/squareup/queue/retrofit/RetrofitQueue;)Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 4

    .line 41
    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsQueueFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsQueueFactory;->directoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    iget-object v2, p0, Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsQueueFactory;->localPaymentsQueueCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/queue/retrofit/QueueCache;

    iget-object v3, p0, Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsQueueFactory;->tasksQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsQueueFactory;->provideLocalPaymentsQueue(Lcom/squareup/settings/server/Features;Ljava/io/File;Lcom/squareup/queue/retrofit/QueueCache;Lcom/squareup/queue/retrofit/RetrofitQueue;)Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsQueueFactory;->get()Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-result-object v0

    return-object v0
.end method
