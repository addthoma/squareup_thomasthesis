.class public final Lcom/squareup/queue/Cash_MembersInjector;
.super Ljava/lang/Object;
.source "Cash_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/queue/Cash;",
        ">;"
    }
.end annotation


# instance fields
.field private final lastLocalPaymentServerIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final localTenderCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/LocalTenderCache;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/PaymentService;",
            ">;"
        }
    .end annotation
.end field

.field private final taskQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/PaymentService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/LocalTenderCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/queue/Cash_MembersInjector;->lastLocalPaymentServerIdProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/queue/Cash_MembersInjector;->paymentServiceProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/queue/Cash_MembersInjector;->taskQueueProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/queue/Cash_MembersInjector;->localTenderCacheProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/queue/Cash_MembersInjector;->ticketsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/PaymentService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/LocalTenderCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/queue/Cash;",
            ">;"
        }
    .end annotation

    .line 47
    new-instance v6, Lcom/squareup/queue/Cash_MembersInjector;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/queue/Cash_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static injectLastLocalPaymentServerId(Lcom/squareup/queue/Cash;Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation runtime Lcom/squareup/settings/LastLocalPaymentServerId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/Cash;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 62
    iput-object p1, p0, Lcom/squareup/queue/Cash;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    return-void
.end method

.method public static injectLocalTenderCache(Lcom/squareup/queue/Cash;Lcom/squareup/print/LocalTenderCache;)V
    .locals 0

    .line 78
    iput-object p1, p0, Lcom/squareup/queue/Cash;->localTenderCache:Lcom/squareup/print/LocalTenderCache;

    return-void
.end method

.method public static injectPaymentService(Lcom/squareup/queue/Cash;Lcom/squareup/server/payment/PaymentService;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/squareup/queue/Cash;->paymentService:Lcom/squareup/server/payment/PaymentService;

    return-void
.end method

.method public static injectTaskQueue(Lcom/squareup/queue/Cash;Lcom/squareup/queue/retrofit/RetrofitQueue;)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/squareup/queue/Cash;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-void
.end method

.method public static injectTickets(Lcom/squareup/queue/Cash;Lcom/squareup/tickets/Tickets;)V
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/squareup/queue/Cash;->tickets:Lcom/squareup/tickets/Tickets;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/queue/Cash;)V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/queue/Cash_MembersInjector;->lastLocalPaymentServerIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/queue/Cash_MembersInjector;->injectLastLocalPaymentServerId(Lcom/squareup/queue/Cash;Lcom/squareup/settings/LocalSetting;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/queue/Cash_MembersInjector;->paymentServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/payment/PaymentService;

    invoke-static {p1, v0}, Lcom/squareup/queue/Cash_MembersInjector;->injectPaymentService(Lcom/squareup/queue/Cash;Lcom/squareup/server/payment/PaymentService;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/queue/Cash_MembersInjector;->taskQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-static {p1, v0}, Lcom/squareup/queue/Cash_MembersInjector;->injectTaskQueue(Lcom/squareup/queue/Cash;Lcom/squareup/queue/retrofit/RetrofitQueue;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/queue/Cash_MembersInjector;->localTenderCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/LocalTenderCache;

    invoke-static {p1, v0}, Lcom/squareup/queue/Cash_MembersInjector;->injectLocalTenderCache(Lcom/squareup/queue/Cash;Lcom/squareup/print/LocalTenderCache;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/queue/Cash_MembersInjector;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/Tickets;

    invoke-static {p1, v0}, Lcom/squareup/queue/Cash_MembersInjector;->injectTickets(Lcom/squareup/queue/Cash;Lcom/squareup/tickets/Tickets;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/queue/Cash;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/Cash_MembersInjector;->injectMembers(Lcom/squareup/queue/Cash;)V

    return-void
.end method
