.class public final Lcom/squareup/queue/UploadItemizationPhoto_MembersInjector;
.super Ljava/lang/Object;
.source "UploadItemizationPhoto_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/queue/UploadItemizationPhoto;",
        ">;"
    }
.end annotation


# instance fields
.field private final fileThreadExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/PaymentService;",
            ">;"
        }
    .end annotation
.end field

.field private final picassoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/PaymentService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/queue/UploadItemizationPhoto_MembersInjector;->paymentServiceProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/queue/UploadItemizationPhoto_MembersInjector;->mainThreadProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/queue/UploadItemizationPhoto_MembersInjector;->picassoProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/queue/UploadItemizationPhoto_MembersInjector;->fileThreadExecutorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/PaymentService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/queue/UploadItemizationPhoto;",
            ">;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/queue/UploadItemizationPhoto_MembersInjector;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/queue/UploadItemizationPhoto_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectFileThreadExecutor(Lcom/squareup/queue/UploadItemizationPhoto;Ljava/util/concurrent/Executor;)V
    .locals 0
    .annotation runtime Lcom/squareup/thread/FileThread;
    .end annotation

    .line 72
    iput-object p1, p0, Lcom/squareup/queue/UploadItemizationPhoto;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public static injectMainThread(Lcom/squareup/queue/UploadItemizationPhoto;Lcom/squareup/thread/executor/MainThread;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/squareup/queue/UploadItemizationPhoto;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-void
.end method

.method public static injectPaymentService(Lcom/squareup/queue/UploadItemizationPhoto;Lcom/squareup/server/payment/PaymentService;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/squareup/queue/UploadItemizationPhoto;->paymentService:Lcom/squareup/server/payment/PaymentService;

    return-void
.end method

.method public static injectPicasso(Lcom/squareup/queue/UploadItemizationPhoto;Lcom/squareup/picasso/Picasso;)V
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/squareup/queue/UploadItemizationPhoto;->picasso:Lcom/squareup/picasso/Picasso;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/queue/UploadItemizationPhoto;)V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/queue/UploadItemizationPhoto_MembersInjector;->paymentServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/payment/PaymentService;

    invoke-static {p1, v0}, Lcom/squareup/queue/UploadItemizationPhoto_MembersInjector;->injectPaymentService(Lcom/squareup/queue/UploadItemizationPhoto;Lcom/squareup/server/payment/PaymentService;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/queue/UploadItemizationPhoto_MembersInjector;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/executor/MainThread;

    invoke-static {p1, v0}, Lcom/squareup/queue/UploadItemizationPhoto_MembersInjector;->injectMainThread(Lcom/squareup/queue/UploadItemizationPhoto;Lcom/squareup/thread/executor/MainThread;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/queue/UploadItemizationPhoto_MembersInjector;->picassoProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/picasso/Picasso;

    invoke-static {p1, v0}, Lcom/squareup/queue/UploadItemizationPhoto_MembersInjector;->injectPicasso(Lcom/squareup/queue/UploadItemizationPhoto;Lcom/squareup/picasso/Picasso;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/queue/UploadItemizationPhoto_MembersInjector;->fileThreadExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {p1, v0}, Lcom/squareup/queue/UploadItemizationPhoto_MembersInjector;->injectFileThreadExecutor(Lcom/squareup/queue/UploadItemizationPhoto;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/queue/UploadItemizationPhoto;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/UploadItemizationPhoto_MembersInjector;->injectMembers(Lcom/squareup/queue/UploadItemizationPhoto;)V

    return-void
.end method
