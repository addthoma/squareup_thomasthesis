.class public final Lcom/squareup/queue/redundant/QueueConformerWatcher_Factory;
.super Ljava/lang/Object;
.source "QueueConformerWatcher_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/redundant/QueueConformerWatcher;",
        ">;"
    }
.end annotation


# instance fields
.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingCapturesQueueConformerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;",
            ">;"
        }
    .end annotation
.end field

.field private final storedPaymentsQueueConformerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;",
            ">;"
        }
    .end annotation
.end field

.field private final tasksQueueConformerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/TasksQueueConformer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/TasksQueueConformer;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/queue/redundant/QueueConformerWatcher_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/queue/redundant/QueueConformerWatcher_Factory;->pendingCapturesQueueConformerProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/queue/redundant/QueueConformerWatcher_Factory;->storedPaymentsQueueConformerProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p4, p0, Lcom/squareup/queue/redundant/QueueConformerWatcher_Factory;->tasksQueueConformerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/redundant/QueueConformerWatcher_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/TasksQueueConformer;",
            ">;)",
            "Lcom/squareup/queue/redundant/QueueConformerWatcher_Factory;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/queue/redundant/QueueConformerWatcher_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/queue/redundant/QueueConformerWatcher_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lrx/Scheduler;Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;Lcom/squareup/queue/redundant/TasksQueueConformer;)Lcom/squareup/queue/redundant/QueueConformerWatcher;
    .locals 1

    .line 51
    new-instance v0, Lcom/squareup/queue/redundant/QueueConformerWatcher;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/queue/redundant/QueueConformerWatcher;-><init>(Lrx/Scheduler;Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;Lcom/squareup/queue/redundant/TasksQueueConformer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/queue/redundant/QueueConformerWatcher;
    .locals 4

    .line 37
    iget-object v0, p0, Lcom/squareup/queue/redundant/QueueConformerWatcher_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    iget-object v1, p0, Lcom/squareup/queue/redundant/QueueConformerWatcher_Factory;->pendingCapturesQueueConformerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;

    iget-object v2, p0, Lcom/squareup/queue/redundant/QueueConformerWatcher_Factory;->storedPaymentsQueueConformerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;

    iget-object v3, p0, Lcom/squareup/queue/redundant/QueueConformerWatcher_Factory;->tasksQueueConformerProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/queue/redundant/TasksQueueConformer;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/queue/redundant/QueueConformerWatcher_Factory;->newInstance(Lrx/Scheduler;Lcom/squareup/queue/redundant/PendingCapturesQueueConformer;Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;Lcom/squareup/queue/redundant/TasksQueueConformer;)Lcom/squareup/queue/redundant/QueueConformerWatcher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/queue/redundant/QueueConformerWatcher_Factory;->get()Lcom/squareup/queue/redundant/QueueConformerWatcher;

    move-result-object v0

    return-object v0
.end method
