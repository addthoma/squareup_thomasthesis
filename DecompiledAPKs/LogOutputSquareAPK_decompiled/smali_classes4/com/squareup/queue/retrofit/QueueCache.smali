.class public Lcom/squareup/queue/retrofit/QueueCache;
.super Ljava/lang/Object;
.source "QueueCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/tape/ObjectQueue<",
        "*>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final openQueues:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/io/File;",
            "TT;>;"
        }
    .end annotation
.end field

.field private final queueFactory:Lcom/squareup/queue/retrofit/QueueFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/retrofit/QueueFactory<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/queue/retrofit/QueueFactory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/retrofit/QueueFactory<",
            "TT;>;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/queue/retrofit/QueueCache;->queueFactory:Lcom/squareup/queue/retrofit/QueueFactory;

    .line 35
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/retrofit/QueueCache;->openQueues:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public getAllForUiTests()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 49
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/queue/retrofit/QueueCache;->openQueues:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getOrOpen(Ljava/io/File;)Lcom/squareup/tape/ObjectQueue;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")TT;"
        }
    .end annotation

    .line 39
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 40
    iget-object v0, p0, Lcom/squareup/queue/retrofit/QueueCache;->openQueues:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tape/ObjectQueue;

    if-nez v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/squareup/queue/retrofit/QueueCache;->queueFactory:Lcom/squareup/queue/retrofit/QueueFactory;

    invoke-interface {v0, p1}, Lcom/squareup/queue/retrofit/QueueFactory;->open(Ljava/io/File;)Lcom/squareup/tape/ObjectQueue;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/squareup/queue/retrofit/QueueCache;->openQueues:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method
