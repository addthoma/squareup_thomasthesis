.class public interface abstract Lcom/squareup/queue/retrofit/RetrofitQueue;
.super Ljava/lang/Object;
.source "RetrofitQueue.java"

# interfaces
.implements Lcom/squareup/tape/ObjectQueue;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/tape/ObjectQueue<",
        "Lcom/squareup/queue/retrofit/RetrofitTask;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract asList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract close()V
.end method

.method public abstract delayClose(Z)V
.end method
