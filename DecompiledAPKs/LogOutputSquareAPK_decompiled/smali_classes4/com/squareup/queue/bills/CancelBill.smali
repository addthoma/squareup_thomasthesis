.class public Lcom/squareup/queue/bills/CancelBill;
.super Lcom/squareup/queue/TransactionRpcThreadTask;
.source "CancelBill.java"

# interfaces
.implements Lcom/squareup/queue/CancelTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/TransactionRpcThreadTask<",
        "Lcom/squareup/protos/client/bills/CancelBillResponse;",
        ">;",
        "Lcom/squareup/queue/CancelTask;"
    }
.end annotation


# instance fields
.field public final request:Lcom/squareup/protos/client/bills/CancelBillRequest;

.field transient service:Lcom/squareup/server/bills/BillCreationService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/String;)V
    .locals 1

    .line 23
    invoke-direct {p0}, Lcom/squareup/queue/TransactionRpcThreadTask;-><init>()V

    .line 24
    new-instance v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;-><init>()V

    .line 25
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;

    move-result-object p1

    .line 26
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->cancel_bill_type(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;

    move-result-object p1

    .line 27
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;

    move-result-object p1

    .line 28
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->build()Lcom/squareup/protos/client/bills/CancelBillRequest;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/queue/bills/CancelBill;->request:Lcom/squareup/protos/client/bills/CancelBillRequest;

    return-void
.end method


# virtual methods
.method protected callOnRpcThread()Lcom/squareup/protos/client/bills/CancelBillResponse;
    .locals 4

    .line 32
    iget-object v0, p0, Lcom/squareup/queue/bills/CancelBill;->service:Lcom/squareup/server/bills/BillCreationService;

    iget-object v1, p0, Lcom/squareup/queue/bills/CancelBill;->request:Lcom/squareup/protos/client/bills/CancelBillRequest;

    invoke-interface {v0, v1}, Lcom/squareup/server/bills/BillCreationService;->cancelBill(Lcom/squareup/protos/client/bills/CancelBillRequest;)Lcom/squareup/protos/client/bills/CancelBillResponse;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/squareup/queue/bills/CancelBill;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-interface {v1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logCancelBillResponse(Lcom/squareup/protos/client/bills/CancelBillResponse;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 34
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CancelBillResponse;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "CancelBillResponse: %s"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0
.end method

.method protected bridge synthetic callOnRpcThread()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/queue/bills/CancelBill;->callOnRpcThread()Lcom/squareup/protos/client/bills/CancelBillResponse;

    move-result-object v0

    return-object v0
.end method

.method protected handleResponseOnMainThread(Lcom/squareup/protos/client/bills/CancelBillResponse;)Lcom/squareup/server/SimpleResponse;
    .locals 1

    .line 51
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CancelBillResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-direct {v0, p1}, Lcom/squareup/server/SimpleResponse;-><init>(Lcom/squareup/protos/client/Status;)V

    return-object v0
.end method

.method protected bridge synthetic handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/protos/client/bills/CancelBillResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/CancelBill;->handleResponseOnMainThread(Lcom/squareup/protos/client/bills/CancelBillResponse;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    return-object p1
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 47
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/bills/CancelBill;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/CancelBill;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public logEnqueued(Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/queue/bills/CancelBill;->request:Lcom/squareup/protos/client/bills/CancelBillRequest;

    invoke-interface {p1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logCancelBillEnqueued(Lcom/squareup/protos/client/bills/CancelBillRequest;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/queue/bills/CancelBill;->request:Lcom/squareup/protos/client/bills/CancelBillRequest;

    return-object v0
.end method
