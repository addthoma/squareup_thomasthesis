.class public final Lcom/squareup/queue/cashmanagement/CashDrawerShiftEmail;
.super Lcom/squareup/queue/QueueModuleRpcThreadTask;
.source "CashDrawerShiftEmail.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/QueueModuleRpcThreadTask<",
        "Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B%\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0004\u0012\u0006\u0010\u0007\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\u0011\u001a\u00020\u0002H\u0014J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0002H\u0014J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0008\u0010\u0019\u001a\u00020\u001aH\u0016R\u0014\u0010\t\u001a\u0004\u0018\u00010\n8\u0000@\u0000X\u0081\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000b\u001a\u00020\u000c8\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\r\u0010\u000e\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/queue/cashmanagement/CashDrawerShiftEmail;",
        "Lcom/squareup/queue/QueueModuleRpcThreadTask;",
        "Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse;",
        "clientKey",
        "",
        "cashDrawerShiftID",
        "merchantID",
        "email",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
        "cashManagementService",
        "Lcom/squareup/server/cashmanagement/CashManagementService;",
        "request",
        "Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;",
        "request$annotations",
        "()V",
        "getRequest",
        "()Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;",
        "callOnRpcThread",
        "handleResponseOnMainThread",
        "Lcom/squareup/server/SimpleResponse;",
        "response",
        "inject",
        "",
        "component",
        "Lcom/squareup/queue/QueueModule$Component;",
        "secureCopyWithoutPIIForLogs",
        "",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public transient cashManagementService:Lcom/squareup/server/cashmanagement/CashManagementService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final request:Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "clientKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cashDrawerShiftID"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "merchantID"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "email"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lcom/squareup/queue/QueueModuleRpcThreadTask;-><init>()V

    .line 22
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;-><init>()V

    .line 23
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->client_unique_key(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;

    move-result-object p1

    .line 24
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->merchant_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;

    move-result-object p1

    .line 25
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->client_cash_drawer_shift_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;

    move-result-object p1

    .line 26
    invoke-virtual {p1, p4}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->email_address(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;

    move-result-object p1

    .line 27
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;

    move-result-object p1

    const-string p2, "EmailCashDrawerShiftRepo\u2026ess(email)\n      .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftEmail;->request:Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;

    return-void
.end method

.method public static synthetic request$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method protected callOnRpcThread()Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse;
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftEmail;->cashManagementService:Lcom/squareup/server/cashmanagement/CashManagementService;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v1, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftEmail;->request:Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;

    invoke-interface {v0, v1}, Lcom/squareup/server/cashmanagement/CashManagementService;->emailDrawer(Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse;

    move-result-object v0

    const-string v1, "cashManagementService!!.emailDrawer(request)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic callOnRpcThread()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftEmail;->callOnRpcThread()Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getRequest()Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftEmail;->request:Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;

    return-object v0
.end method

.method protected handleResponseOnMainThread(Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse;)Lcom/squareup/server/SimpleResponse;
    .locals 2

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse;->status:Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Status;

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Status;->OK:Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Status;

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {v0, p1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    return-object v0
.end method

.method public bridge synthetic handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftEmail;->handleResponseOnMainThread(Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    return-object p1
.end method

.method public inject(Lcom/squareup/queue/QueueModule$Component;)V
    .locals 1

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-interface {p1, p0}, Lcom/squareup/queue/QueueModule$Component;->inject(Lcom/squareup/queue/cashmanagement/CashDrawerShiftEmail;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/queue/QueueModule$Component;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftEmail;->inject(Lcom/squareup/queue/QueueModule$Component;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftEmail;->request:Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;

    invoke-virtual {v0}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "request.toString()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
