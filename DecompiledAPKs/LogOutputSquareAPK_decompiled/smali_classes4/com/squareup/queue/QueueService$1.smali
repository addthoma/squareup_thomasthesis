.class Lcom/squareup/queue/QueueService$1;
.super Lcom/squareup/server/SquareCallback;
.source "QueueService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/queue/QueueService;->lambda$startTaskFrom$0(Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/QueueService$TaskCallback;Lmortar/MortarScope;Lcom/squareup/queue/retrofit/RetrofitTask;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/server/SquareCallback<",
        "Lcom/squareup/server/SimpleResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/queue/QueueService;

.field final synthetic val$queue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field final synthetic val$taskCallback:Lcom/squareup/queue/QueueService$TaskCallback;


# direct methods
.method constructor <init>(Lcom/squareup/queue/QueueService;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/QueueService$TaskCallback;)V
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/squareup/queue/QueueService$1;->this$0:Lcom/squareup/queue/QueueService;

    iput-object p2, p0, Lcom/squareup/queue/QueueService$1;->val$queue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    iput-object p3, p0, Lcom/squareup/queue/QueueService$1;->val$taskCallback:Lcom/squareup/queue/QueueService$TaskCallback;

    invoke-direct {p0}, Lcom/squareup/server/SquareCallback;-><init>()V

    return-void
.end method

.method private after()V
    .locals 2

    .line 249
    iget-object v0, p0, Lcom/squareup/queue/QueueService$1;->val$queue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->delayClose(Z)V

    return-void
.end method

.method private before()V
    .locals 2

    .line 245
    iget-object v0, p0, Lcom/squareup/queue/QueueService$1;->this$0:Lcom/squareup/queue/QueueService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/queue/QueueService;->access$900(Lcom/squareup/queue/QueueService;Lcom/squareup/queue/retrofit/RetrofitTask;)V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/server/SimpleResponse;)V
    .locals 1

    .line 253
    invoke-direct {p0}, Lcom/squareup/queue/QueueService$1;->before()V

    .line 254
    iget-object v0, p0, Lcom/squareup/queue/QueueService$1;->val$taskCallback:Lcom/squareup/queue/QueueService$TaskCallback;

    invoke-virtual {v0, p1}, Lcom/squareup/queue/QueueService$TaskCallback;->call(Lcom/squareup/server/SimpleResponse;)V

    .line 255
    invoke-direct {p0}, Lcom/squareup/queue/QueueService$1;->after()V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 243
    check-cast p1, Lcom/squareup/server/SimpleResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/QueueService$1;->call(Lcom/squareup/server/SimpleResponse;)V

    return-void
.end method

.method public clientError(Lcom/squareup/server/SimpleResponse;I)V
    .locals 1

    .line 265
    invoke-direct {p0}, Lcom/squareup/queue/QueueService$1;->before()V

    .line 266
    iget-object v0, p0, Lcom/squareup/queue/QueueService$1;->val$taskCallback:Lcom/squareup/queue/QueueService$TaskCallback;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/queue/QueueService$TaskCallback;->clientError(Lcom/squareup/server/SimpleResponse;I)V

    .line 267
    invoke-direct {p0}, Lcom/squareup/queue/QueueService$1;->after()V

    return-void
.end method

.method public bridge synthetic clientError(Ljava/lang/Object;I)V
    .locals 0

    .line 243
    check-cast p1, Lcom/squareup/server/SimpleResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/queue/QueueService$1;->clientError(Lcom/squareup/server/SimpleResponse;I)V

    return-void
.end method

.method public networkError()V
    .locals 1

    .line 277
    invoke-direct {p0}, Lcom/squareup/queue/QueueService$1;->before()V

    .line 278
    iget-object v0, p0, Lcom/squareup/queue/QueueService$1;->val$taskCallback:Lcom/squareup/queue/QueueService$TaskCallback;

    invoke-virtual {v0}, Lcom/squareup/queue/QueueService$TaskCallback;->networkError()V

    .line 279
    invoke-direct {p0}, Lcom/squareup/queue/QueueService$1;->after()V

    return-void
.end method

.method public serverError(I)V
    .locals 1

    .line 271
    invoke-direct {p0}, Lcom/squareup/queue/QueueService$1;->before()V

    .line 272
    iget-object v0, p0, Lcom/squareup/queue/QueueService$1;->val$taskCallback:Lcom/squareup/queue/QueueService$TaskCallback;

    invoke-virtual {v0, p1}, Lcom/squareup/queue/QueueService$TaskCallback;->serverError(I)V

    .line 273
    invoke-direct {p0}, Lcom/squareup/queue/QueueService$1;->after()V

    return-void
.end method

.method public sessionExpired()V
    .locals 1

    .line 259
    invoke-direct {p0}, Lcom/squareup/queue/QueueService$1;->before()V

    .line 260
    iget-object v0, p0, Lcom/squareup/queue/QueueService$1;->val$taskCallback:Lcom/squareup/queue/QueueService$TaskCallback;

    invoke-virtual {v0}, Lcom/squareup/queue/QueueService$TaskCallback;->sessionExpired()V

    .line 261
    invoke-direct {p0}, Lcom/squareup/queue/QueueService$1;->after()V

    return-void
.end method

.method public unexpectedError(Ljava/lang/Throwable;)V
    .locals 1

    .line 283
    invoke-direct {p0}, Lcom/squareup/queue/QueueService$1;->before()V

    .line 284
    iget-object v0, p0, Lcom/squareup/queue/QueueService$1;->val$taskCallback:Lcom/squareup/queue/QueueService$TaskCallback;

    invoke-virtual {v0, p1}, Lcom/squareup/queue/QueueService$TaskCallback;->unexpectedError(Ljava/lang/Throwable;)V

    .line 285
    invoke-direct {p0}, Lcom/squareup/queue/QueueService$1;->after()V

    return-void
.end method
