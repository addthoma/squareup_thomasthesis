.class Lcom/squareup/queue/RpcThreadTask$1;
.super Lcom/squareup/server/SquareCallbackObserver;
.source "RpcThreadTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/queue/RpcThreadTask;->execute(Lcom/squareup/server/SquareCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/server/SquareCallbackObserver<",
        "Lcom/squareup/server/SimpleResponse;",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/queue/RpcThreadTask;


# direct methods
.method constructor <init>(Lcom/squareup/queue/RpcThreadTask;Lcom/squareup/server/SquareCallback;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/queue/RpcThreadTask$1;->this$0:Lcom/squareup/queue/RpcThreadTask;

    invoke-direct {p0, p2}, Lcom/squareup/server/SquareCallbackObserver;-><init>(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method


# virtual methods
.method public handleResponse(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/squareup/server/SimpleResponse;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/queue/RpcThreadTask$1;->this$0:Lcom/squareup/queue/RpcThreadTask;

    invoke-virtual {v0, p1}, Lcom/squareup/queue/RpcThreadTask;->handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    return-object p1
.end method
