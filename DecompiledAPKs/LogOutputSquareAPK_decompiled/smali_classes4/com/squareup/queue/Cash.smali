.class public Lcom/squareup/queue/Cash;
.super Ljava/lang/Object;
.source "Cash.java"

# interfaces
.implements Lcom/squareup/queue/LocalPaymentRetrofitTask;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private adjustmentList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final adjustments:Ljava/lang/String;

.field private final change:Lcom/squareup/Money;

.field private final changeMoney:Lcom/squareup/protos/common/Money;

.field private itemizationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final itemizations:Ljava/lang/String;

.field private final itemsModel:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModel;",
            ">;"
        }
    .end annotation
.end field

.field transient lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;
    .annotation runtime Lcom/squareup/settings/LastLocalPaymentServerId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient localTenderCache:Lcom/squareup/print/LocalTenderCache;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient paymentService:Lcom/squareup/server/payment/PaymentService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final receiptNote:Ljava/lang/String;

.field transient taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final tax:Lcom/squareup/Money;

.field private final taxMoney:Lcom/squareup/protos/common/Money;

.field private final tendered:Lcom/squareup/Money;

.field private final tenderedMoney:Lcom/squareup/protos/common/Money;

.field private ticketId:Ljava/lang/String;

.field transient tickets:Lcom/squareup/tickets/Tickets;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final time:J

.field private final total:Lcom/squareup/Money;

.field private final totalMoney:Lcom/squareup/protos/common/Money;

.field private uuid:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;JLjava/util/List;Ljava/lang/String;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModel;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    if-nez p6, :cond_0

    .line 106
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    move-object v6, v0

    goto :goto_0

    :cond_0
    move-object/from16 v6, p6

    :goto_0
    if-nez p7, :cond_1

    .line 107
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    move-object v7, v0

    goto :goto_1

    :cond_1
    move-object/from16 v7, p7

    :goto_1
    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v8, p8

    move-wide/from16 v9, p9

    move-object/from16 v11, p11

    move-object/from16 v14, p12

    .line 105
    invoke-direct/range {v0 .. v14}, Lcom/squareup/queue/Cash;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;JLjava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;JLjava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/lang/String;",
            "J",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModel;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-object p1, p0, Lcom/squareup/queue/Cash;->changeMoney:Lcom/squareup/protos/common/Money;

    .line 125
    iput-object p2, p0, Lcom/squareup/queue/Cash;->receiptNote:Ljava/lang/String;

    .line 126
    iput-object p3, p0, Lcom/squareup/queue/Cash;->tenderedMoney:Lcom/squareup/protos/common/Money;

    .line 127
    iput-object p4, p0, Lcom/squareup/queue/Cash;->totalMoney:Lcom/squareup/protos/common/Money;

    .line 128
    iput-object p5, p0, Lcom/squareup/queue/Cash;->taxMoney:Lcom/squareup/protos/common/Money;

    .line 129
    iput-object p6, p0, Lcom/squareup/queue/Cash;->adjustmentList:Ljava/util/List;

    .line 130
    iput-object p7, p0, Lcom/squareup/queue/Cash;->itemizationList:Ljava/util/List;

    const-string p1, "uuid"

    .line 131
    invoke-static {p8, p1}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/queue/Cash;->uuid:Ljava/lang/String;

    .line 132
    iput-wide p9, p0, Lcom/squareup/queue/Cash;->time:J

    .line 133
    iput-object p11, p0, Lcom/squareup/queue/Cash;->itemsModel:Ljava/util/List;

    .line 135
    iput-object p12, p0, Lcom/squareup/queue/Cash;->adjustments:Ljava/lang/String;

    .line 136
    iput-object p13, p0, Lcom/squareup/queue/Cash;->itemizations:Ljava/lang/String;

    .line 137
    iput-object p14, p0, Lcom/squareup/queue/Cash;->ticketId:Ljava/lang/String;

    const/4 p1, 0x0

    .line 139
    iput-object p1, p0, Lcom/squareup/queue/Cash;->change:Lcom/squareup/Money;

    iput-object p1, p0, Lcom/squareup/queue/Cash;->tax:Lcom/squareup/Money;

    iput-object p1, p0, Lcom/squareup/queue/Cash;->total:Lcom/squareup/Money;

    iput-object p1, p0, Lcom/squareup/queue/Cash;->tendered:Lcom/squareup/Money;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/queue/Cash;)Ljava/lang/String;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/queue/Cash;->uuid:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/queue/Cash;)Ljava/lang/String;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/queue/Cash;->ticketId:Ljava/lang/String;

    return-object p0
.end method

.method static legacyForTesting(Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;J)Lcom/squareup/queue/Cash;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/lang/String;",
            "J)",
            "Lcom/squareup/queue/Cash;"
        }
    .end annotation

    .line 115
    new-instance v15, Lcom/squareup/queue/Cash;

    .line 116
    invoke-static {}, Lcom/squareup/RegisterGsonProvider;->gson()Lcom/google/gson/Gson;

    move-result-object v0

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 117
    invoke-static {}, Lcom/squareup/RegisterGsonProvider;->gson()Lcom/google/gson/Gson;

    move-result-object v0

    move-object/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v11, 0x0

    const/4 v14, 0x0

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v8, p7

    move-wide/from16 v9, p8

    invoke-direct/range {v0 .. v14}, Lcom/squareup/queue/Cash;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;JLjava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v15
.end method


# virtual methods
.method public asBill(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 10

    .line 274
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 275
    new-instance v0, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;-><init>()V

    .line 276
    invoke-virtual {p0}, Lcom/squareup/queue/Cash;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->id(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    .line 277
    invoke-virtual {p0}, Lcom/squareup/queue/Cash;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    .line 278
    invoke-virtual {p0}, Lcom/squareup/queue/Cash;->getTimestamp()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->timestamp(Ljava/util/Date;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    iget-object v1, p0, Lcom/squareup/queue/Cash;->tenderedMoney:Lcom/squareup/protos/common/Money;

    .line 279
    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->tenderedAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/Cash;->changeMoney:Lcom/squareup/protos/common/Money;

    .line 280
    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->changeAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    move-result-object v0

    .line 281
    invoke-virtual {v0}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->build()Lcom/squareup/billhistory/model/CashTenderHistory;

    move-result-object v0

    .line 275
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    new-instance v9, Lcom/squareup/billhistory/model/BillHistory$Builder;

    invoke-virtual {p0}, Lcom/squareup/queue/Cash;->getUuid()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->forPayment(Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v1

    .line 283
    invoke-static {p0}, Lcom/squareup/payment/OrderTaskHelper;->orderSnapshotForTask(Lcom/squareup/queue/PaymentTask;)Lcom/squareup/payment/OrderSnapshot;

    move-result-object v3

    new-instance v5, Ljava/util/Date;

    invoke-virtual {p0}, Lcom/squareup/queue/Cash;->getTime()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 284
    invoke-virtual {p0}, Lcom/squareup/queue/Cash;->getItemizations()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/server/payment/AdjusterMessages;->fromItemizationMessages(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/billhistory/Bills;->createBillNote(Ljava/util/List;Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    move-result-object v6

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistoryId;Ljava/util/List;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;Ljava/util/List;Z)V

    invoke-virtual {v9}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    return-object p1
.end method

.method public createMessage()Lcom/squareup/server/payment/CashMessage;
    .locals 22

    move-object/from16 v0, p0

    .line 268
    new-instance v18, Lcom/squareup/server/payment/CashMessage;

    move-object/from16 v1, v18

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/queue/Cash;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/queue/Cash;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/queue/Cash;->getChange()Lcom/squareup/protos/common/Money;

    move-result-object v6

    iget-object v6, v6, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 269
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/queue/Cash;->getTax()Lcom/squareup/protos/common/Money;

    move-result-object v8

    iget-object v8, v8, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/queue/Cash;->getAdjustments()Ljava/util/List;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/queue/Cash;->getItemizations()Ljava/util/List;

    move-result-object v11

    iget-object v12, v0, Lcom/squareup/queue/Cash;->receiptNote:Ljava/lang/String;

    iget-object v13, v0, Lcom/squareup/queue/Cash;->uuid:Ljava/lang/String;

    new-instance v14, Ljava/util/Date;

    move-object/from16 v19, v1

    move-wide/from16 v20, v2

    iget-wide v1, v0, Lcom/squareup/queue/Cash;->time:J

    invoke-direct {v14, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 270
    invoke-static {v14}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/queue/Cash;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v1}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object v15

    iget-object v1, v0, Lcom/squareup/queue/Cash;->itemsModel:Ljava/util/List;

    move-object/from16 v17, v1

    const/16 v16, 0x0

    move-object/from16 v1, v19

    move-wide/from16 v2, v20

    invoke-direct/range {v1 .. v17}, Lcom/squareup/server/payment/CashMessage;-><init>(JJJJLjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;)V

    return-object v18
.end method

.method public execute(Lcom/squareup/server/SquareCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 220
    iget-object v0, p0, Lcom/squareup/queue/Cash;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    const-string v1, ""

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 222
    invoke-virtual {p0}, Lcom/squareup/queue/Cash;->createMessage()Lcom/squareup/server/payment/CashMessage;

    move-result-object v0

    .line 224
    iget-object v1, p0, Lcom/squareup/queue/Cash;->paymentService:Lcom/squareup/server/payment/PaymentService;

    new-instance v2, Lcom/squareup/queue/Cash$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/queue/Cash$1;-><init>(Lcom/squareup/queue/Cash;Lcom/squareup/server/SquareCallback;)V

    invoke-interface {v1, v0, v2}, Lcom/squareup/server/payment/PaymentService;->cash(Lcom/squareup/server/payment/CashMessage;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/Cash;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public getAdjustments()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation

    .line 164
    iget-object v0, p0, Lcom/squareup/queue/Cash;->adjustmentList:Ljava/util/List;

    if-nez v0, :cond_1

    .line 165
    iget-object v0, p0, Lcom/squareup/queue/Cash;->adjustments:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/queue/Cash;->adjustmentList:Ljava/util/List;

    goto :goto_0

    .line 169
    :cond_0
    invoke-static {}, Lcom/squareup/RegisterGsonProvider;->gson()Lcom/google/gson/Gson;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/Cash;->adjustments:Ljava/lang/String;

    invoke-static {}, Lcom/squareup/queue/Itemize;->adjustmentListType()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/queue/Cash;->adjustmentList:Ljava/util/List;

    .line 173
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/queue/Cash;->adjustmentList:Ljava/util/List;

    return-object v0
.end method

.method public getChange()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 192
    iget-object v0, p0, Lcom/squareup/queue/Cash;->changeMoney:Lcom/squareup/protos/common/Money;

    iget-object v1, p0, Lcom/squareup/queue/Cash;->change:Lcom/squareup/Money;

    invoke-static {v0, v1}, Lcom/squareup/Money;->launderMoney(Lcom/squareup/protos/common/Money;Lcom/squareup/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getItemizations()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation

    .line 179
    iget-object v0, p0, Lcom/squareup/queue/Cash;->itemizationList:Ljava/util/List;

    if-nez v0, :cond_1

    .line 180
    iget-object v0, p0, Lcom/squareup/queue/Cash;->itemizations:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/queue/Cash;->itemizationList:Ljava/util/List;

    goto :goto_0

    .line 184
    :cond_0
    invoke-static {}, Lcom/squareup/RegisterGsonProvider;->gson()Lcom/google/gson/Gson;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/Cash;->itemizations:Ljava/lang/String;

    invoke-static {}, Lcom/squareup/queue/Itemize;->itemizationListType()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/queue/Cash;->itemizationList:Ljava/util/List;

    .line 188
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/queue/Cash;->itemizationList:Ljava/util/List;

    return-object v0
.end method

.method public getReceiptNote()Ljava/lang/String;
    .locals 1

    .line 196
    iget-object v0, p0, Lcom/squareup/queue/Cash;->receiptNote:Ljava/lang/String;

    return-object v0
.end method

.method public getSwedishRounding()Lcom/squareup/protos/common/Money;
    .locals 4

    .line 200
    invoke-virtual {p0}, Lcom/squareup/queue/Cash;->getAdjustments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/AdjustmentMessage;

    .line 201
    iget-object v2, v1, Lcom/squareup/server/payment/AdjustmentMessage;->type:Ljava/lang/String;

    const-string v3, "swedish_rounding"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 202
    iget-object v0, v1, Lcom/squareup/server/payment/AdjustmentMessage;->amount_money:Lcom/squareup/protos/common/Money;

    return-object v0

    :cond_1
    const-wide/16 v0, 0x0

    .line 205
    invoke-virtual {p0}, Lcom/squareup/queue/Cash;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getTax()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 155
    iget-object v0, p0, Lcom/squareup/queue/Cash;->taxMoney:Lcom/squareup/protos/common/Money;

    iget-object v1, p0, Lcom/squareup/queue/Cash;->tax:Lcom/squareup/Money;

    invoke-static {v0, v1}, Lcom/squareup/Money;->launderMoney(Lcom/squareup/protos/common/Money;Lcom/squareup/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    .line 157
    sget-object v2, Lcom/squareup/protos/common/CurrencyCode;->USD:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getTendered()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 209
    iget-object v0, p0, Lcom/squareup/queue/Cash;->tenderedMoney:Lcom/squareup/protos/common/Money;

    iget-object v1, p0, Lcom/squareup/queue/Cash;->tendered:Lcom/squareup/Money;

    invoke-static {v0, v1}, Lcom/squareup/Money;->launderMoney(Lcom/squareup/protos/common/Money;Lcom/squareup/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getTicketId()Ljava/lang/String;
    .locals 1

    .line 306
    iget-object v0, p0, Lcom/squareup/queue/Cash;->ticketId:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    .line 214
    iget-wide v0, p0, Lcom/squareup/queue/Cash;->time:J

    return-wide v0
.end method

.method public getTimestamp()Ljava/util/Date;
    .locals 3

    .line 151
    new-instance v0, Ljava/util/Date;

    iget-wide v1, p0, Lcom/squareup/queue/Cash;->time:J

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public getTotal()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/squareup/queue/Cash;->totalMoney:Lcom/squareup/protos/common/Money;

    iget-object v1, p0, Lcom/squareup/queue/Cash;->total:Lcom/squareup/Money;

    invoke-static {v0, v1}, Lcom/squareup/Money;->launderMoney(Lcom/squareup/protos/common/Money;Lcom/squareup/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getUuid()Ljava/lang/String;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/queue/Cash;->uuid:Ljava/lang/String;

    return-object v0
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 310
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/Cash;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/Cash;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 288
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cash{change="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    invoke-virtual {p0}, Lcom/squareup/queue/Cash;->getChange()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", receiptNote=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/Cash;->receiptNote:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", currency="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    invoke-virtual {p0}, Lcom/squareup/queue/Cash;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", tendered="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 292
    invoke-virtual {p0}, Lcom/squareup/queue/Cash;->getTendered()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", total="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 293
    invoke-virtual {p0}, Lcom/squareup/queue/Cash;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", adjustmentList="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    invoke-virtual {p0}, Lcom/squareup/queue/Cash;->getAdjustments()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->deepToString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", itemizationList="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    invoke-virtual {p0}, Lcom/squareup/queue/Cash;->getAdjustments()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->deepToString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", uuid=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/Cash;->uuid:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/queue/Cash;->time:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
