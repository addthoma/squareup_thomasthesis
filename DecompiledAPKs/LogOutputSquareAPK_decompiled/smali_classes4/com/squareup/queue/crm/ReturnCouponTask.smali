.class public Lcom/squareup/queue/crm/ReturnCouponTask;
.super Lcom/squareup/queue/RpcThreadTask;
.source "ReturnCouponTask.java"

# interfaces
.implements Lcom/squareup/queue/QueueModule$LoggedInQueueModuleTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/RpcThreadTask<",
        "Lcom/squareup/server/SimpleResponse;",
        "Lcom/squareup/queue/QueueModule$Component;",
        ">;",
        "Lcom/squareup/queue/QueueModule$LoggedInQueueModuleTask;"
    }
.end annotation


# static fields
.field static final UNUSED_POINTS_VALUE:I = -0x1


# instance fields
.field final couponReason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

.field final couponToken:Ljava/lang/String;

.field transient loyaltyService:Lcom/squareup/loyalty/LoyaltyServiceHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field final pointsValue:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/checkout/Discount;)V
    .locals 2

    .line 53
    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->getCouponToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->getCouponReason()Lcom/squareup/protos/client/coupons/Coupon$Reason;

    move-result-object p1

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/queue/crm/ReturnCouponTask;-><init>(Ljava/lang/String;ILcom/squareup/protos/client/coupons/Coupon$Reason;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/protos/client/coupons/Coupon$Reason;)V
    .locals 0

    .line 56
    invoke-direct {p0}, Lcom/squareup/queue/RpcThreadTask;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/queue/crm/ReturnCouponTask;->couponToken:Ljava/lang/String;

    .line 58
    iput p2, p0, Lcom/squareup/queue/crm/ReturnCouponTask;->pointsValue:I

    .line 59
    iput-object p3, p0, Lcom/squareup/queue/crm/ReturnCouponTask;->couponReason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    return-void
.end method


# virtual methods
.method protected callOnRpcThread()Lcom/squareup/server/SimpleResponse;
    .locals 2

    .line 73
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->LOYALTY:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    invoke-virtual {p0}, Lcom/squareup/queue/crm/ReturnCouponTask;->getCouponReason()Lcom/squareup/protos/client/coupons/Coupon$Reason;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/coupons/Coupon$Reason;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    return-object v0

    .line 78
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/squareup/queue/crm/ReturnCouponTask;->loyaltyService:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iget-object v1, p0, Lcom/squareup/queue/crm/ReturnCouponTask;->couponToken:Ljava/lang/String;

    .line 79
    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->returnReward(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    .line 82
    new-instance v1, Lcom/squareup/server/SimpleResponse;

    instance-of v0, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-direct {v1, v0}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 84
    :catch_0
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    return-object v0
.end method

.method protected bridge synthetic callOnRpcThread()Ljava/lang/Object;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/queue/crm/ReturnCouponTask;->callOnRpcThread()Lcom/squareup/server/SimpleResponse;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 99
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 100
    :cond_1
    check-cast p1, Lcom/squareup/queue/crm/ReturnCouponTask;

    .line 101
    invoke-virtual {p0}, Lcom/squareup/queue/crm/ReturnCouponTask;->getCouponReason()Lcom/squareup/protos/client/coupons/Coupon$Reason;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/queue/crm/ReturnCouponTask;->getCouponReason()Lcom/squareup/protos/client/coupons/Coupon$Reason;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/queue/crm/ReturnCouponTask;->couponToken:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/queue/crm/ReturnCouponTask;->couponToken:Ljava/lang/String;

    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public getCouponReason()Lcom/squareup/protos/client/coupons/Coupon$Reason;
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/queue/crm/ReturnCouponTask;->couponReason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    if-eqz v0, :cond_0

    return-object v0

    .line 117
    :cond_0
    iget v0, p0, Lcom/squareup/queue/crm/ReturnCouponTask;->pointsValue:I

    if-lez v0, :cond_1

    .line 118
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->LOYALTY:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    return-object v0

    .line 120
    :cond_1
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->UNKNOWN_REASON:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    return-object v0
.end method

.method protected handleResponseOnMainThread(Lcom/squareup/server/SimpleResponse;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    return-object p1
.end method

.method protected bridge synthetic handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/server/SimpleResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/crm/ReturnCouponTask;->handleResponseOnMainThread(Lcom/squareup/server/SimpleResponse;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    return-object p1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 106
    iget-object v1, p0, Lcom/squareup/queue/crm/ReturnCouponTask;->couponToken:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/queue/crm/ReturnCouponTask;->getCouponReason()Lcom/squareup/protos/client/coupons/Coupon$Reason;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public inject(Lcom/squareup/queue/QueueModule$Component;)V
    .locals 0

    .line 67
    invoke-interface {p1, p0}, Lcom/squareup/queue/QueueModule$Component;->inject(Lcom/squareup/queue/crm/ReturnCouponTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/queue/QueueModule$Component;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/crm/ReturnCouponTask;->inject(Lcom/squareup/queue/QueueModule$Component;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 93
    iget-object v1, p0, Lcom/squareup/queue/crm/ReturnCouponTask;->couponToken:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 94
    invoke-virtual {p0}, Lcom/squareup/queue/crm/ReturnCouponTask;->getCouponReason()Lcom/squareup/protos/client/coupons/Coupon$Reason;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "ReturnCouponTask{couponToken=\'%s\', reason=\'%s\'}"

    .line 93
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
