.class public Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;
.super Lcom/squareup/queue/RpcThreadTask;
.source "SendBuyerLoyaltyStatusTask.java"

# interfaces
.implements Lcom/squareup/queue/QueueModule$LoggedInQueueModuleTask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/RpcThreadTask<",
        "Lcom/squareup/server/SimpleResponse;",
        "Lcom/squareup/queue/QueueModule$Component;",
        ">;",
        "Lcom/squareup/queue/QueueModule$LoggedInQueueModuleTask;"
    }
.end annotation


# instance fields
.field private final employeeToken:Ljava/lang/String;

.field transient loyalty:Lcom/squareup/server/loyalty/LoyaltyService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final phoneToken:Ljava/lang/String;

.field private final requestToken:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;)V
    .locals 1

    .line 53
    invoke-direct {p0}, Lcom/squareup/queue/RpcThreadTask;-><init>()V

    .line 54
    invoke-static {p1}, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;->access$100(Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;->phoneToken:Ljava/lang/String;

    .line 55
    invoke-static {p1}, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;->access$200(Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;->employeeToken:Ljava/lang/String;

    .line 56
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;->requestToken:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$1;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;-><init>(Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;)V

    return-void
.end method

.method private buildRequest()Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest;
    .locals 4

    .line 87
    new-instance v0, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;->employeeToken:Ljava/lang/String;

    .line 88
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/squareup/protos/client/CreatorDetails$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/CreatorDetails$Builder;-><init>()V

    new-instance v2, Lcom/squareup/protos/client/Employee$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/Employee$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;->employeeToken:Ljava/lang/String;

    .line 92
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/Employee$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v2

    .line 93
    invoke-virtual {v2}, Lcom/squareup/protos/client/Employee$Builder;->build()Lcom/squareup/protos/client/Employee;

    move-result-object v2

    .line 91
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/CreatorDetails$Builder;->employee(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/CreatorDetails$Builder;

    move-result-object v1

    .line 94
    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails$Builder;->build()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object v1

    .line 88
    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;->creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;->phoneToken:Ljava/lang/String;

    .line 95
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;->phone_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;->requestToken:Ljava/lang/String;

    .line 96
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;->request_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected callOnRpcThread()Lcom/squareup/server/SimpleResponse;
    .locals 2

    .line 76
    :try_start_0
    iget-object v0, p0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;->loyalty:Lcom/squareup/server/loyalty/LoyaltyService;

    invoke-direct {p0}, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;->buildRequest()Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/server/loyalty/LoyaltyService;->sendLoyaltyStatus(Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    .line 80
    new-instance v1, Lcom/squareup/server/SimpleResponse;

    instance-of v0, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-direct {v1, v0}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 82
    :catch_0
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    return-object v0
.end method

.method protected bridge synthetic callOnRpcThread()Ljava/lang/Object;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;->callOnRpcThread()Lcom/squareup/server/SimpleResponse;

    move-result-object v0

    return-object v0
.end method

.method public getEmployeeToken()Ljava/lang/String;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;->employeeToken:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneToken()Ljava/lang/String;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;->phoneToken:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestToken()Ljava/lang/String;
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;->requestToken:Ljava/lang/String;

    return-object v0
.end method

.method protected handleResponseOnMainThread(Lcom/squareup/server/SimpleResponse;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    return-object p1
.end method

.method protected bridge synthetic handleResponseOnMainThread(Ljava/lang/Object;)Lcom/squareup/server/SimpleResponse;
    .locals 0

    .line 26
    check-cast p1, Lcom/squareup/server/SimpleResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;->handleResponseOnMainThread(Lcom/squareup/server/SimpleResponse;)Lcom/squareup/server/SimpleResponse;

    move-result-object p1

    return-object p1
.end method

.method public inject(Lcom/squareup/queue/QueueModule$Component;)V
    .locals 0

    .line 60
    invoke-interface {p1, p0}, Lcom/squareup/queue/QueueModule$Component;->inject(Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 26
    check-cast p1, Lcom/squareup/queue/QueueModule$Component;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;->inject(Lcom/squareup/queue/QueueModule$Component;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 64
    iget-object v1, p0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;->phoneToken:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;->employeeToken:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;->requestToken:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "SendBuyerLoyaltyStatusTask{phoneToken=\'%s\', employeeToken=\'%s\', requestToken=\'%s\'}"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
