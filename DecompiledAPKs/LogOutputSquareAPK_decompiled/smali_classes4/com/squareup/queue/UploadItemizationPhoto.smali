.class public Lcom/squareup/queue/UploadItemizationPhoto;
.super Ljava/lang/Object;
.source "UploadItemizationPhoto.java"

# interfaces
.implements Lcom/squareup/queue/QueueModule$LoggedInQueueModuleTask;


# static fields
.field private static final SERVER_IMAGE_SIZE:I = 0x200


# instance fields
.field transient fileThreadExecutor:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/squareup/thread/FileThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field final imageToken:Ljava/lang/String;

.field transient mainThread:Lcom/squareup/thread/executor/MainThread;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient paymentService:Lcom/squareup/server/payment/PaymentService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field final photoUrl:Ljava/lang/String;

.field transient picasso:Lcom/squareup/picasso/Picasso;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/queue/UploadItemizationPhoto;->imageToken:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/squareup/queue/UploadItemizationPhoto;->photoUrl:Ljava/lang/String;

    return-void
.end method

.method private decodeFailed(Lcom/squareup/server/SquareCallback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 67
    new-instance v0, Ljava/lang/IllegalStateException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/queue/UploadItemizationPhoto;->photoUrl:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "Image decoding failed: %s"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/queue/UploadItemizationPhoto;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/queue/-$$Lambda$UploadItemizationPhoto$JAZQQbWJf6Mcud6oi2i4Q31vJhE;

    invoke-direct {v1, p1}, Lcom/squareup/queue/-$$Lambda$UploadItemizationPhoto$JAZQQbWJf6Mcud6oi2i4Q31vJhE;-><init>(Lcom/squareup/server/SquareCallback;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$decodeFailed$2(Lcom/squareup/server/SquareCallback;)V
    .locals 2

    .line 68
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    invoke-virtual {p0, v0}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public execute(Lcom/squareup/server/SquareCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/squareup/queue/UploadItemizationPhoto;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/queue/-$$Lambda$UploadItemizationPhoto$RUbiWMroMKJcxHxhwSuVwTJhtxE;

    invoke-direct {v1, p0, p1}, Lcom/squareup/queue/-$$Lambda$UploadItemizationPhoto$RUbiWMroMKJcxHxhwSuVwTJhtxE;-><init>(Lcom/squareup/queue/UploadItemizationPhoto;Lcom/squareup/server/SquareCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/UploadItemizationPhoto;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public inject(Lcom/squareup/queue/QueueModule$Component;)V
    .locals 0

    .line 83
    invoke-interface {p1, p0}, Lcom/squareup/queue/QueueModule$Component;->inject(Lcom/squareup/queue/UploadItemizationPhoto;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/queue/QueueModule$Component;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/UploadItemizationPhoto;->inject(Lcom/squareup/queue/QueueModule$Component;)V

    return-void
.end method

.method public synthetic lambda$execute$1$UploadItemizationPhoto(Lcom/squareup/server/SquareCallback;)V
    .locals 4

    .line 42
    :try_start_0
    iget-object v0, p0, Lcom/squareup/queue/UploadItemizationPhoto;->picasso:Lcom/squareup/picasso/Picasso;

    iget-object v1, p0, Lcom/squareup/queue/UploadItemizationPhoto;->photoUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    const/16 v1, 0x200

    .line 43
    invoke-virtual {v0, v1, v1}, Lcom/squareup/picasso/RequestCreator;->resize(II)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->centerCrop()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->skipMemoryCache()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->get()Landroid/graphics/Bitmap;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/queue/UploadItemizationPhoto;->decodeFailed(Lcom/squareup/server/SquareCallback;)V

    return-void

    .line 57
    :cond_0
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/Bitmaps;->getByteArrayFromBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I)[B

    move-result-object v1

    .line 58
    new-instance v2, Lretrofit/mime/TypedByteArray;

    const-string v3, "image/jpeg"

    invoke-direct {v2, v3, v1}, Lretrofit/mime/TypedByteArray;-><init>(Ljava/lang/String;[B)V

    .line 60
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 62
    iget-object v0, p0, Lcom/squareup/queue/UploadItemizationPhoto;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/queue/-$$Lambda$UploadItemizationPhoto$V9RamRhrLA8sjzO6a5iv-u2RO58;

    invoke-direct {v1, p0, v2, p1}, Lcom/squareup/queue/-$$Lambda$UploadItemizationPhoto$V9RamRhrLA8sjzO6a5iv-u2RO58;-><init>(Lcom/squareup/queue/UploadItemizationPhoto;Lretrofit/mime/TypedOutput;Lcom/squareup/server/SquareCallback;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void

    .line 48
    :catch_0
    invoke-direct {p0, p1}, Lcom/squareup/queue/UploadItemizationPhoto;->decodeFailed(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public synthetic lambda$null$0$UploadItemizationPhoto(Lretrofit/mime/TypedOutput;Lcom/squareup/server/SquareCallback;)V
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/queue/UploadItemizationPhoto;->paymentService:Lcom/squareup/server/payment/PaymentService;

    iget-object v1, p0, Lcom/squareup/queue/UploadItemizationPhoto;->imageToken:Ljava/lang/String;

    invoke-interface {v0, p1, v1, p2}, Lcom/squareup/server/payment/PaymentService;->uploadItemizationPhoto(Lretrofit/mime/TypedOutput;Ljava/lang/String;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UploadItemizationPhoto{imageToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/UploadItemizationPhoto;->imageToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", photoUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/UploadItemizationPhoto;->photoUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
