.class public Lcom/squareup/queue/OtherTenderTask;
.super Ljava/lang/Object;
.source "OtherTenderTask.java"

# interfaces
.implements Lcom/squareup/queue/LocalPaymentRetrofitTask;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field public final adjustmentList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation
.end field

.field public final itemizationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final itemsModel:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModel;",
            ">;"
        }
    .end annotation
.end field

.field transient lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;
    .annotation runtime Lcom/squareup/settings/LastLocalPaymentServerId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient localTenderCache:Lcom/squareup/print/LocalTenderCache;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient paymentService:Lcom/squareup/server/payment/PaymentService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final receiptNote:Ljava/lang/String;

.field transient taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final taxMoney:Lcom/squareup/protos/common/Money;

.field public final tenderNote:Ljava/lang/String;

.field public final tenderType:Lcom/squareup/server/account/protos/OtherTenderType;

.field public final ticketId:Ljava/lang/String;

.field transient tickets:Lcom/squareup/tickets/Tickets;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final time:J

.field public final tipMoney:Lcom/squareup/protos/common/Money;

.field public final totalMoney:Lcom/squareup/protos/common/Money;

.field public final uuid:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/queue/OtherTenderTask;)V
    .locals 4

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iget-object v0, p1, Lcom/squareup/queue/OtherTenderTask;->tipMoney:Lcom/squareup/protos/common/Money;

    iput-object v0, p0, Lcom/squareup/queue/OtherTenderTask;->tipMoney:Lcom/squareup/protos/common/Money;

    .line 111
    iget-object v0, p1, Lcom/squareup/queue/OtherTenderTask;->tenderType:Lcom/squareup/server/account/protos/OtherTenderType;

    iput-object v0, p0, Lcom/squareup/queue/OtherTenderTask;->tenderType:Lcom/squareup/server/account/protos/OtherTenderType;

    .line 112
    iget-object v0, p1, Lcom/squareup/queue/OtherTenderTask;->tenderNote:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v0, "REDACTED_tenderNote"

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/squareup/queue/OtherTenderTask;->tenderNote:Ljava/lang/String;

    .line 113
    iget-object v0, p1, Lcom/squareup/queue/OtherTenderTask;->receiptNote:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "REDACTED_receiptNote"

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/squareup/queue/OtherTenderTask;->receiptNote:Ljava/lang/String;

    .line 114
    iget-object v0, p1, Lcom/squareup/queue/OtherTenderTask;->totalMoney:Lcom/squareup/protos/common/Money;

    iput-object v0, p0, Lcom/squareup/queue/OtherTenderTask;->totalMoney:Lcom/squareup/protos/common/Money;

    .line 115
    iget-object v0, p1, Lcom/squareup/queue/OtherTenderTask;->taxMoney:Lcom/squareup/protos/common/Money;

    iput-object v0, p0, Lcom/squareup/queue/OtherTenderTask;->taxMoney:Lcom/squareup/protos/common/Money;

    .line 116
    iget-object v0, p1, Lcom/squareup/queue/OtherTenderTask;->adjustmentList:Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/queue/OtherTenderTask;->adjustmentList:Ljava/util/List;

    .line 117
    iget-object v0, p1, Lcom/squareup/queue/OtherTenderTask;->itemizationList:Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/queue/OtherTenderTask;->itemizationList:Ljava/util/List;

    .line 118
    iget-object v0, p1, Lcom/squareup/queue/OtherTenderTask;->uuid:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/queue/OtherTenderTask;->uuid:Ljava/lang/String;

    .line 119
    iget-wide v2, p1, Lcom/squareup/queue/OtherTenderTask;->time:J

    iput-wide v2, p0, Lcom/squareup/queue/OtherTenderTask;->time:J

    .line 120
    iput-object v1, p0, Lcom/squareup/queue/OtherTenderTask;->itemsModel:Ljava/util/List;

    .line 121
    iget-object p1, p1, Lcom/squareup/queue/OtherTenderTask;->ticketId:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/queue/OtherTenderTask;->ticketId:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/lang/String;JLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/protos/OtherTenderType;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/lang/String;",
            "J",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModel;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p1, p0, Lcom/squareup/queue/OtherTenderTask;->tenderType:Lcom/squareup/server/account/protos/OtherTenderType;

    .line 89
    iput-object p2, p0, Lcom/squareup/queue/OtherTenderTask;->tenderNote:Ljava/lang/String;

    .line 90
    iput-object p11, p0, Lcom/squareup/queue/OtherTenderTask;->tipMoney:Lcom/squareup/protos/common/Money;

    .line 91
    iput-object p3, p0, Lcom/squareup/queue/OtherTenderTask;->receiptNote:Ljava/lang/String;

    .line 92
    iput-object p4, p0, Lcom/squareup/queue/OtherTenderTask;->totalMoney:Lcom/squareup/protos/common/Money;

    .line 93
    iput-object p5, p0, Lcom/squareup/queue/OtherTenderTask;->taxMoney:Lcom/squareup/protos/common/Money;

    if-nez p6, :cond_0

    .line 95
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p6

    :cond_0
    iput-object p6, p0, Lcom/squareup/queue/OtherTenderTask;->adjustmentList:Ljava/util/List;

    if-nez p7, :cond_1

    .line 98
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p7

    :cond_1
    iput-object p7, p0, Lcom/squareup/queue/OtherTenderTask;->itemizationList:Ljava/util/List;

    const-string p1, "uuid"

    .line 100
    invoke-static {p8, p1}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/queue/OtherTenderTask;->uuid:Ljava/lang/String;

    .line 101
    iput-wide p9, p0, Lcom/squareup/queue/OtherTenderTask;->time:J

    .line 102
    iput-object p12, p0, Lcom/squareup/queue/OtherTenderTask;->itemsModel:Ljava/util/List;

    .line 103
    iput-object p13, p0, Lcom/squareup/queue/OtherTenderTask;->ticketId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public asBill(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 10

    .line 207
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 208
    new-instance v5, Ljava/util/Date;

    iget-wide v0, p0, Lcom/squareup/queue/OtherTenderTask;->time:J

    invoke-direct {v5, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 209
    new-instance v0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/queue/OtherTenderTask;->uuid:Ljava/lang/String;

    .line 210
    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->id(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    iget-object v1, p0, Lcom/squareup/queue/OtherTenderTask;->totalMoney:Lcom/squareup/protos/common/Money;

    .line 211
    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    .line 212
    invoke-virtual {v0, v5}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->timestamp(Ljava/util/Date;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    iget-object v1, p0, Lcom/squareup/queue/OtherTenderTask;->tenderType:Lcom/squareup/server/account/protos/OtherTenderType;

    iget-object v1, v1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_name:Ljava/lang/String;

    .line 213
    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->name(Ljava/lang/String;)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/OtherTenderTask;->tenderNote:Ljava/lang/String;

    .line 214
    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->note(Ljava/lang/String;)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/OtherTenderTask;->tenderType:Lcom/squareup/server/account/protos/OtherTenderType;

    iget-object v1, v1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    .line 215
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->tenderType(I)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    move-result-object v0

    .line 216
    invoke-virtual {v0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->build()Lcom/squareup/billhistory/model/OtherTenderHistory;

    move-result-object v0

    .line 209
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    new-instance v9, Lcom/squareup/billhistory/model/BillHistory$Builder;

    iget-object v0, p0, Lcom/squareup/queue/OtherTenderTask;->uuid:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->forPayment(Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v1

    .line 218
    invoke-static {p0}, Lcom/squareup/payment/OrderTaskHelper;->orderSnapshotForTask(Lcom/squareup/queue/PaymentTask;)Lcom/squareup/payment/OrderSnapshot;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/queue/OtherTenderTask;->tipMoney:Lcom/squareup/protos/common/Money;

    .line 219
    invoke-virtual {p0}, Lcom/squareup/queue/OtherTenderTask;->getItemizations()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/server/payment/AdjusterMessages;->fromItemizationMessages(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/billhistory/Bills;->createBillNote(Ljava/util/List;Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistoryId;Ljava/util/List;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;Ljava/util/List;Z)V

    invoke-virtual {v9}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    return-object p1
.end method

.method public createMessage()Lcom/squareup/server/payment/OtherTenderMessage;
    .locals 20

    move-object/from16 v0, p0

    .line 200
    iget-object v1, v0, Lcom/squareup/queue/OtherTenderTask;->tipMoney:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    goto :goto_0

    :cond_0
    const-wide/16 v1, 0x0

    :goto_0
    move-wide/from16 v18, v1

    .line 201
    new-instance v1, Lcom/squareup/server/payment/OtherTenderMessage;

    move-object v3, v1

    iget-object v2, v0, Lcom/squareup/queue/OtherTenderTask;->totalMoney:Lcom/squareup/protos/common/Money;

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v2, v0, Lcom/squareup/queue/OtherTenderTask;->taxMoney:Lcom/squareup/protos/common/Money;

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-object v8, v0, Lcom/squareup/queue/OtherTenderTask;->adjustmentList:Ljava/util/List;

    iget-object v9, v0, Lcom/squareup/queue/OtherTenderTask;->itemizationList:Ljava/util/List;

    iget-object v10, v0, Lcom/squareup/queue/OtherTenderTask;->receiptNote:Ljava/lang/String;

    iget-object v11, v0, Lcom/squareup/queue/OtherTenderTask;->uuid:Ljava/lang/String;

    new-instance v2, Ljava/util/Date;

    iget-wide v12, v0, Lcom/squareup/queue/OtherTenderTask;->time:J

    invoke-direct {v2, v12, v13}, Ljava/util/Date;-><init>(J)V

    .line 202
    invoke-static {v2}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v12

    iget-object v2, v0, Lcom/squareup/queue/OtherTenderTask;->totalMoney:Lcom/squareup/protos/common/Money;

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 203
    invoke-virtual {v2}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    iget-object v15, v0, Lcom/squareup/queue/OtherTenderTask;->tenderType:Lcom/squareup/server/account/protos/OtherTenderType;

    iget-object v2, v0, Lcom/squareup/queue/OtherTenderTask;->tenderNote:Ljava/lang/String;

    move-object/from16 v16, v2

    iget-object v2, v0, Lcom/squareup/queue/OtherTenderTask;->itemsModel:Ljava/util/List;

    move-object/from16 v17, v2

    invoke-direct/range {v3 .. v19}, Lcom/squareup/server/payment/OtherTenderMessage;-><init>(JJLjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;Ljava/util/List;J)V

    return-object v1
.end method

.method public execute(Lcom/squareup/server/SquareCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 151
    iget-object v0, p0, Lcom/squareup/queue/OtherTenderTask;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    const-string v1, ""

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 153
    invoke-virtual {p0}, Lcom/squareup/queue/OtherTenderTask;->createMessage()Lcom/squareup/server/payment/OtherTenderMessage;

    move-result-object v0

    .line 155
    iget-object v1, p0, Lcom/squareup/queue/OtherTenderTask;->paymentService:Lcom/squareup/server/payment/PaymentService;

    new-instance v2, Lcom/squareup/queue/OtherTenderTask$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/queue/OtherTenderTask$1;-><init>(Lcom/squareup/queue/OtherTenderTask;Lcom/squareup/server/SquareCallback;)V

    invoke-interface {v1, v0, v2}, Lcom/squareup/server/payment/PaymentService;->otherTender(Lcom/squareup/server/payment/OtherTenderMessage;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/OtherTenderTask;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public getAdjustments()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation

    .line 141
    iget-object v0, p0, Lcom/squareup/queue/OtherTenderTask;->adjustmentList:Ljava/util/List;

    return-object v0
.end method

.method public getItemizations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation

    .line 137
    iget-object v0, p0, Lcom/squareup/queue/OtherTenderTask;->itemizationList:Ljava/util/List;

    return-object v0
.end method

.method public getTicketId()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTime()J
    .locals 2

    .line 129
    iget-wide v0, p0, Lcom/squareup/queue/OtherTenderTask;->time:J

    return-wide v0
.end method

.method public getTotal()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/queue/OtherTenderTask;->totalMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getUuid()Ljava/lang/String;
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/queue/OtherTenderTask;->uuid:Ljava/lang/String;

    return-object v0
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 242
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/OtherTenderTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/OtherTenderTask;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 1

    .line 238
    new-instance v0, Lcom/squareup/queue/OtherTenderTask;

    invoke-direct {v0, p0}, Lcom/squareup/queue/OtherTenderTask;-><init>(Lcom/squareup/queue/OtherTenderTask;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 223
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OtherTender{, tenderType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/OtherTenderTask;->tenderType:Lcom/squareup/server/account/protos/OtherTenderType;

    iget-object v1, v1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", tenderNote="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/OtherTenderTask;->tenderNote:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", receiptNote=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/OtherTenderTask;->receiptNote:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", currency="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/OtherTenderTask;->totalMoney:Lcom/squareup/protos/common/Money;

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", total="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/OtherTenderTask;->totalMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", tip="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/OtherTenderTask;->tipMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", adjustmentList="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/OtherTenderTask;->adjustmentList:Ljava/util/List;

    .line 230
    invoke-interface {v2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->deepToString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", itemizationList="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/OtherTenderTask;->itemizationList:Ljava/util/List;

    .line 231
    invoke-interface {v2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->deepToString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", uuid=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/OtherTenderTask;->uuid:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/queue/OtherTenderTask;->time:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
