.class public Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;
.super Ljava/lang/Object;
.source "StoreAndForwardQueueFactory.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/StoreAndForwardQueueFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EnqueuedPaymentBytes"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final captureBytes:[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final completeBillBytes:[B

.field public final paymentBytes:[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final paymentCompleted:Z

.field public final paymentV2Bytes:[B

.field public final storeAndForwardBillBytes:[B


# direct methods
.method public constructor <init>([B[B[B[B[BZ)V
    .locals 0

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    iput-object p1, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->captureBytes:[B

    .line 126
    iput-object p2, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->completeBillBytes:[B

    .line 127
    iput-object p3, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->paymentBytes:[B

    .line 128
    iput-object p4, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->paymentV2Bytes:[B

    .line 129
    iput-object p5, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->storeAndForwardBillBytes:[B

    .line 130
    iput-boolean p6, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->paymentCompleted:Z

    return-void
.end method
