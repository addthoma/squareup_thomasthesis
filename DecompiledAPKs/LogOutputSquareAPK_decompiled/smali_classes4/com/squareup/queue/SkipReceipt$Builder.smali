.class public Lcom/squareup/queue/SkipReceipt$Builder;
.super Ljava/lang/Object;
.source "SkipReceipt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/SkipReceipt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private paymentId:Ljava/lang/String;

.field private uniqueKey:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/queue/SkipReceipt$Builder;)Ljava/lang/String;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/queue/SkipReceipt$Builder;->paymentId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/queue/SkipReceipt$Builder;)Ljava/lang/String;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/queue/SkipReceipt$Builder;->uniqueKey:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/queue/SkipReceipt;
    .locals 2

    .line 56
    new-instance v0, Lcom/squareup/queue/SkipReceipt;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/queue/SkipReceipt;-><init>(Lcom/squareup/queue/SkipReceipt$Builder;Lcom/squareup/queue/SkipReceipt$1;)V

    return-object v0
.end method

.method public paymentId(Ljava/lang/String;)Lcom/squareup/queue/SkipReceipt$Builder;
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/queue/SkipReceipt$Builder;->paymentId:Ljava/lang/String;

    return-object p0
.end method

.method uniqueKey(Ljava/lang/String;)Lcom/squareup/queue/SkipReceipt$Builder;
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/squareup/queue/SkipReceipt$Builder;->uniqueKey:Ljava/lang/String;

    return-object p0
.end method
