.class final Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$insert$1;
.super Ljava/lang/Object;
.source "PendingCapturesSqliteStore.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->insert(Lcom/squareup/queue/sqlite/PendingCapturesEntry;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $entry:Lcom/squareup/queue/sqlite/PendingCapturesEntry;

.field final synthetic this$0:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;


# direct methods
.method constructor <init>(Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;Lcom/squareup/queue/sqlite/PendingCapturesEntry;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$insert$1;->this$0:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;

    iput-object p2, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$insert$1;->$entry:Lcom/squareup/queue/sqlite/PendingCapturesEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 43
    invoke-virtual {p0}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$insert$1;->call()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final call()Z
    .locals 5

    .line 177
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$insert$1;->this$0:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;

    invoke-static {v0}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->access$getInsert$p(Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;)Lcom/squareup/queue/sqlite/PendingCapturesModel$InsertEntry;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$insert$1;->$entry:Lcom/squareup/queue/sqlite/PendingCapturesEntry;

    invoke-virtual {v1}, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->entry_id()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$insert$1;->$entry:Lcom/squareup/queue/sqlite/PendingCapturesEntry;

    invoke-virtual {v2}, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->timestamp_ms()J

    move-result-wide v2

    iget-object v4, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$insert$1;->$entry:Lcom/squareup/queue/sqlite/PendingCapturesEntry;

    invoke-virtual {v4}, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->data()[B

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/queue/sqlite/PendingCapturesModel$InsertEntry;->bind(Ljava/lang/String;J[B)V

    .line 179
    :try_start_0
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$insert$1;->this$0:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;

    invoke-static {v0}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->access$getDb$p(Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;)Lcom/squareup/sqlbrite3/BriteDatabase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$insert$1;->this$0:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;

    invoke-static {v1}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->access$getInsert$p(Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;)Lcom/squareup/queue/sqlite/PendingCapturesModel$InsertEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/queue/sqlite/PendingCapturesModel$InsertEntry;->getTable()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$insert$1;->this$0:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;

    invoke-static {v2}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->access$getInsert$p(Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;)Lcom/squareup/queue/sqlite/PendingCapturesModel$InsertEntry;

    move-result-object v2

    check-cast v2, Landroidx/sqlite/db/SupportSQLiteStatement;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/sqlbrite3/BriteDatabase;->executeInsert(Ljava/lang/String;Landroidx/sqlite/db/SupportSQLiteStatement;)J

    move-result-wide v0
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-ltz v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    .line 181
    new-instance v1, Ljava/lang/RuntimeException;

    iget-object v2, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$insert$1;->this$0:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;

    iget-object v3, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$insert$1;->$entry:Lcom/squareup/queue/sqlite/PendingCapturesEntry;

    invoke-static {v2, v3}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->access$failedInsertMessage(Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;Lcom/squareup/queue/sqlite/PendingCapturesEntry;)Ljava/lang/String;

    move-result-object v2

    check-cast v0, Ljava/lang/Throwable;

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method
