.class public final Lcom/squareup/queue/sqlite/StoredPaymentsModel$Mapper;
.super Ljava/lang/Object;
.source "StoredPaymentsModel.java"

# interfaces
.implements Lcom/squareup/sqldelight/prerelease/RowMapper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/StoredPaymentsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Mapper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/queue/sqlite/StoredPaymentsModel;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/sqldelight/prerelease/RowMapper<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final storedPaymentsModelFactory:Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory<",
            "TT;>;)V"
        }
    .end annotation

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object p1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Mapper;->storedPaymentsModelFactory:Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;

    return-void
.end method


# virtual methods
.method public map(Landroid/database/Cursor;)Lcom/squareup/queue/sqlite/StoredPaymentsModel;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")TT;"
        }
    .end annotation

    .line 108
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Mapper;->storedPaymentsModelFactory:Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;

    iget-object v1, v0, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;->creator:Lcom/squareup/queue/sqlite/StoredPaymentsModel$Creator;

    const/4 v0, 0x0

    .line 109
    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    move-object v2, v0

    const/4 v0, 0x1

    .line 110
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x2

    .line 111
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v0, 0x3

    .line 112
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    .line 108
    invoke-interface/range {v1 .. v6}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Creator;->create(Ljava/lang/Long;Ljava/lang/String;J[B)Lcom/squareup/queue/sqlite/StoredPaymentsModel;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic map(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 0

    .line 99
    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Mapper;->map(Landroid/database/Cursor;)Lcom/squareup/queue/sqlite/StoredPaymentsModel;

    move-result-object p1

    return-object p1
.end method
