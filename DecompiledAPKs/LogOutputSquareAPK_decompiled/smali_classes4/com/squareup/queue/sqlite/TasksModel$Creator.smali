.class public interface abstract Lcom/squareup/queue/sqlite/TasksModel$Creator;
.super Ljava/lang/Object;
.source "TasksModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/TasksModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Creator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/queue/sqlite/TasksModel;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract create(Ljava/lang/Long;Ljava/lang/String;JJ[B)Lcom/squareup/queue/sqlite/TasksModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "JJ[B)TT;"
        }
    .end annotation
.end method
