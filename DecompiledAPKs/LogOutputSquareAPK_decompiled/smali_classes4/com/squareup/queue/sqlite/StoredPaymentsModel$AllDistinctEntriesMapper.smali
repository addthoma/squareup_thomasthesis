.class public final Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesMapper;
.super Ljava/lang/Object;
.source "StoredPaymentsModel.java"

# interfaces
.implements Lcom/squareup/sqldelight/prerelease/RowMapper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/StoredPaymentsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AllDistinctEntriesMapper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesModel;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/sqldelight/prerelease/RowMapper<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final creator:Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesCreator<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesCreator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesCreator<",
            "TT;>;)V"
        }
    .end annotation

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesMapper;->creator:Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesCreator;

    return-void
.end method


# virtual methods
.method public map(Landroid/database/Cursor;)Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesModel;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")TT;"
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesMapper;->creator:Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesCreator;

    const/4 v1, 0x0

    .line 87
    invoke-interface {p1, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :goto_0
    const/4 v2, 0x1

    .line 88
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    .line 89
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const/4 v5, 0x3

    .line 90
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    .line 86
    invoke-interface/range {v0 .. v5}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesCreator;->create(Ljava/lang/Long;Ljava/lang/String;J[B)Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesModel;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic map(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 0

    .line 76
    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesMapper;->map(Landroid/database/Cursor;)Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesModel;

    move-result-object p1

    return-object p1
.end method
