.class public Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;
.super Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;
.source "PendingCapturesSqliteQueue.java"

# interfaces
.implements Lcom/squareup/queue/sqlite/PendingCapturesMonitor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesLogger;,
        Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesConverter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue<",
        "Lcom/squareup/queue/retrofit/RetrofitTask;",
        ">;",
        "Lcom/squareup/queue/sqlite/PendingCapturesMonitor;"
    }
.end annotation


# instance fields
.field private final converter:Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesConverter;

.field private final mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final queueDelegate:Lcom/squareup/queue/sqlite/shared/SqliteQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/sqlite/shared/SqliteQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation
.end field

.field private final store:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;


# direct methods
.method public constructor <init>(Lcom/squareup/queue/sqlite/shared/SqliteQueue;Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 0
    .param p4    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/shared/SqliteQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")V"
        }
    .end annotation

    .line 67
    invoke-direct {p0, p1}, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;-><init>(Lcom/squareup/queue/sqlite/shared/SqliteQueue;)V

    .line 68
    iput-object p1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->queueDelegate:Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    .line 69
    iput-object p2, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->store:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;

    .line 70
    new-instance p1, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesConverter;

    invoke-direct {p1, p3}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesConverter;-><init>(Lcom/squareup/tape/FileObjectQueue$Converter;)V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->converter:Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesConverter;

    .line 71
    iput-object p4, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-void
.end method


# virtual methods
.method public add(Lcom/squareup/queue/retrofit/RetrofitTask;)Lio/reactivex/Completable;
    .locals 2

    .line 75
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 76
    instance-of v0, p1, Lcom/squareup/queue/CaptureTask;

    const-string v1, "Pending captures queue only permits CaptureTask entries"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 78
    invoke-super {p0, p1}, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;->add(Ljava/lang/Object;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic add(Ljava/lang/Object;)Lio/reactivex/Completable;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/queue/retrofit/RetrofitTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->add(Lcom/squareup/queue/retrofit/RetrofitTask;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method

.method public allPendingCapturesAsBillHistory(Lcom/squareup/util/Res;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;>;"
        }
    .end annotation

    .line 101
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 102
    invoke-virtual {p0}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->pendingCapturesCount()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->store:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;

    .line 104
    invoke-virtual {v1}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->allEntriesAsStream()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;

    iget-object v3, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->converter:Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesConverter;

    invoke-direct {v2, p1, v3}, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;)V

    .line 103
    invoke-static {v1, v2}, Lcom/squareup/queue/sqlite/SqliteQueues;->convertStreamAndBuffer(Lrx/Observable;Lrx/functions/Func1;)Lrx/Observable$Transformer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public fetchTransaction(Lcom/squareup/util/Res;Ljava/lang/String;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Single<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;>;"
        }
    .end annotation

    .line 111
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 112
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->store:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;

    invoke-virtual {v0, p2}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->fetchEntry(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p2

    new-instance v0, Lcom/squareup/queue/sqlite/-$$Lambda$PendingCapturesSqliteQueue$JzJKmHUvQNF6bH4icGnyWdg4V4o;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/sqlite/-$$Lambda$PendingCapturesSqliteQueue$JzJKmHUvQNF6bH4icGnyWdg4V4o;-><init>(Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;Lcom/squareup/util/Res;)V

    .line 113
    invoke-virtual {p2, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 112
    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$fetchTransaction$1$PendingCapturesSqliteQueue(Lcom/squareup/util/Res;Lcom/squareup/util/Optional;)Lcom/squareup/util/Optional;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 113
    new-instance v0, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->converter:Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesConverter;

    invoke-direct {v0, p1, v1}, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance p1, Lcom/squareup/queue/sqlite/-$$Lambda$sEJrRERLha0AV2W2fJzImb-FGbM;

    invoke-direct {p1, v0}, Lcom/squareup/queue/sqlite/-$$Lambda$sEJrRERLha0AV2W2fJzImb-FGbM;-><init>(Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;)V

    invoke-virtual {p2, p1}, Lcom/squareup/util/Optional;->map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$oldestPendingCapture$0$PendingCapturesSqliteQueue(Lcom/squareup/util/Optional;)Lcom/squareup/util/Optional;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 94
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->converter:Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesConverter;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/queue/sqlite/-$$Lambda$gZlp6PRK3c2QCNUa-1U-D6thKHc;

    invoke-direct {v1, v0}, Lcom/squareup/queue/sqlite/-$$Lambda$gZlp6PRK3c2QCNUa-1U-D6thKHc;-><init>(Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue$PendingCapturesConverter;)V

    invoke-virtual {p1, v1}, Lcom/squareup/util/Optional;->map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method

.method public oldestPendingCapture()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;"
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 93
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->store:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->oldestEntry()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/queue/sqlite/-$$Lambda$PendingCapturesSqliteQueue$eIabHsglvnSQvBOzCzanY6PRJD8;

    invoke-direct {v1, p0}, Lcom/squareup/queue/sqlite/-$$Lambda$PendingCapturesSqliteQueue$eIabHsglvnSQvBOzCzanY6PRJD8;-><init>(Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;)V

    .line 94
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    .line 93
    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public pendingCapturesCount()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 82
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 83
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->queueDelegate:Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    invoke-interface {v0}, Lcom/squareup/queue/sqlite/shared/SqliteQueue;->size()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public ripenedPendingCapturesCount()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 87
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 88
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteQueue;->store:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->ripenedCount()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method
