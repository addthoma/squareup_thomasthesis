.class public Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;
.super Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;
.source "StoredPaymentsSqliteQueue.java"

# interfaces
.implements Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue<",
        "Lcom/squareup/payment/offline/StoredPayment;",
        ">;",
        "Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;"
    }
.end annotation


# instance fields
.field private final converter:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;

.field private final store:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;


# direct methods
.method public constructor <init>(Lcom/squareup/queue/sqlite/shared/SqliteQueue;Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/shared/SqliteQueue<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;",
            "Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;",
            "Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;",
            ")V"
        }
    .end annotation

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;-><init>(Lcom/squareup/queue/sqlite/shared/SqliteQueue;)V

    .line 56
    iput-object p2, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;->store:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;

    .line 57
    new-instance p1, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;

    invoke-direct {p1, p3}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;-><init>(Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;)V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;->converter:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;

    return-void
.end method


# virtual methods
.method public allDistinctStoredPaymentIds()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 73
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;->store:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->allDistinctEntryIds()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public allDistinctStoredPaymentsAsBillHistory(Lcom/squareup/util/Res;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;>;"
        }
    .end annotation

    .line 65
    invoke-virtual {p0}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;->distinctStoredPaymentsCount()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;->store:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;

    .line 67
    invoke-virtual {v1}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->allDistinctEntriesAsStream()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;

    iget-object v3, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;->converter:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;

    invoke-direct {v2, p1, v3}, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;)V

    .line 66
    invoke-static {v1, v2}, Lcom/squareup/queue/sqlite/SqliteQueues;->convertStreamAndBuffer(Lrx/Observable;Lrx/functions/Func1;)Lrx/Observable$Transformer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public distinctStoredPaymentsCount()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;->store:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->distinctCount()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public fetchTransaction(Lcom/squareup/util/Res;Ljava/lang/String;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Single<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;>;"
        }
    .end annotation

    .line 88
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;->store:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;

    .line 89
    invoke-virtual {v0, p2}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->fetchEntry(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p2

    new-instance v0, Lcom/squareup/queue/sqlite/-$$Lambda$StoredPaymentsSqliteQueue$cqEB3EhIiNwbD5zQBEA5LuvPTBg;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/sqlite/-$$Lambda$StoredPaymentsSqliteQueue$cqEB3EhIiNwbD5zQBEA5LuvPTBg;-><init>(Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;Lcom/squareup/util/Res;)V

    .line 90
    invoke-virtual {p2, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 88
    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$fetchTransaction$1$StoredPaymentsSqliteQueue(Lcom/squareup/util/Res;Lcom/squareup/util/Optional;)Lcom/squareup/util/Optional;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 90
    new-instance v0, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;->converter:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;

    invoke-direct {v0, p1, v1}, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance p1, Lcom/squareup/queue/sqlite/-$$Lambda$PtGayPLi5GYZxp_NytxKoi2tGxM;

    invoke-direct {p1, v0}, Lcom/squareup/queue/sqlite/-$$Lambda$PtGayPLi5GYZxp_NytxKoi2tGxM;-><init>(Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;)V

    invoke-virtual {p2, p1}, Lcom/squareup/util/Optional;->map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$oldestStoredPayment$0$StoredPaymentsSqliteQueue(Lcom/squareup/util/Optional;)Lcom/squareup/util/Optional;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;->converter:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/queue/sqlite/-$$Lambda$eeKY7pIdP6j5Xjf_mMKQAml8xRA;

    invoke-direct {v1, v0}, Lcom/squareup/queue/sqlite/-$$Lambda$eeKY7pIdP6j5Xjf_mMKQAml8xRA;-><init>(Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;)V

    invoke-virtual {p1, v1}, Lcom/squareup/util/Optional;->map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method

.method public oldestStoredPayment()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;>;"
        }
    .end annotation

    .line 77
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;->store:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;

    .line 78
    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->oldestEntry()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/queue/sqlite/-$$Lambda$StoredPaymentsSqliteQueue$nO2J8G3enFoH68wsV-SQMBlBaVg;

    invoke-direct {v1, p0}, Lcom/squareup/queue/sqlite/-$$Lambda$StoredPaymentsSqliteQueue$nO2J8G3enFoH68wsV-SQMBlBaVg;-><init>(Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;)V

    .line 79
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    .line 77
    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method
