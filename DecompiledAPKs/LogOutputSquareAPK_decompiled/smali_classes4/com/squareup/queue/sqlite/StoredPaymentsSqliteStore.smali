.class public final Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;
.super Ljava/lang/Object;
.source "StoredPaymentsSqliteStore.kt"

# interfaces
.implements Lcom/squareup/queue/sqlite/SqliteQueueStore;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$DatabaseCallback;,
        Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/queue/sqlite/SqliteQueueStore<",
        "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u0000 22\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u000223B+\u0008\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0008\u0008\u0001\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u000c\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\u00020\rJ\u0012\u0010\u000e\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00100\u000f0\rJ\u0014\u0010&\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020\u000f0\rH\u0016J\u0008\u0010\'\u001a\u00020(H\u0016J\u000e\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\rH\u0016J\u000e\u0010)\u001a\u0008\u0012\u0004\u0012\u00020\u00120*H\u0016J\u000e\u0010+\u001a\u0008\u0012\u0004\u0012\u00020\u00120*H\u0016J\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00120\rJ\u001c\u0010,\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020!0*2\u0006\u0010-\u001a\u00020\u0010H\u0016J\u0014\u0010 \u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020!0\rH\u0016J\u0016\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020.0*2\u0006\u0010/\u001a\u00020\u0002H\u0016J\u0012\u0010$\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020!0\rJ\u0008\u00100\u001a\u000201H\u0002R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000e\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00100\u000f0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0019\u001a\u00020\u001a8\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u001b\u0010\u001c\u001a\u0004\u0008\u001d\u0010\u001eR\u0014\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00120\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010 \u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020!0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010$\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020!0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00064"
    }
    d2 = {
        "Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;",
        "Lcom/squareup/queue/sqlite/SqliteQueueStore;",
        "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
        "context",
        "Landroid/app/Application;",
        "userDir",
        "Ljava/io/File;",
        "sqlBrite",
        "Lcom/squareup/sqlbrite3/SqlBrite;",
        "fileScheduler",
        "Lio/reactivex/Scheduler;",
        "(Landroid/app/Application;Ljava/io/File;Lcom/squareup/sqlbrite3/SqlBrite;Lio/reactivex/Scheduler;)V",
        "allDistinctEntries",
        "Lio/reactivex/Observable;",
        "allDistinctEntryIds",
        "",
        "",
        "count",
        "",
        "db",
        "Lcom/squareup/sqlbrite3/BriteDatabase;",
        "deleteAll",
        "Lcom/squareup/queue/sqlite/StoredPaymentsModel$DeleteAllEntries;",
        "deleteFirst",
        "Lcom/squareup/queue/sqlite/StoredPaymentsModel$DeleteFirstEntry;",
        "disposables",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "disposables$annotations",
        "()V",
        "getDisposables",
        "()Lio/reactivex/disposables/CompositeDisposable;",
        "distinctCount",
        "firstEntry",
        "Lcom/squareup/util/Optional;",
        "insert",
        "Lcom/squareup/queue/sqlite/StoredPaymentsModel$InsertEntry;",
        "oldestEntry",
        "allDistinctEntriesAsStream",
        "allEntries",
        "close",
        "Lio/reactivex/Completable;",
        "deleteAllEntries",
        "Lio/reactivex/Single;",
        "deleteFirstEntry",
        "fetchEntry",
        "entryId",
        "",
        "entry",
        "preload",
        "",
        "Companion",
        "DatabaseCallback",
        "queue_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$Companion;


# instance fields
.field private final allDistinctEntries:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final allDistinctEntryIds:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final count:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final db:Lcom/squareup/sqlbrite3/BriteDatabase;

.field private final deleteAll:Lcom/squareup/queue/sqlite/StoredPaymentsModel$DeleteAllEntries;

.field private final deleteFirst:Lcom/squareup/queue/sqlite/StoredPaymentsModel$DeleteFirstEntry;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final distinctCount:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final fileScheduler:Lio/reactivex/Scheduler;

.field private final firstEntry:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
            ">;>;"
        }
    .end annotation
.end field

.field private final insert:Lcom/squareup/queue/sqlite/StoredPaymentsModel$InsertEntry;

.field private final oldestEntry:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->Companion:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Ljava/io/File;Lcom/squareup/sqlbrite3/SqlBrite;Lio/reactivex/Scheduler;)V
    .locals 9
    .param p2    # Ljava/io/File;
        .annotation runtime Lcom/squareup/user/UserDirectory;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userDir"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sqlBrite"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p4, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->fileScheduler:Lio/reactivex/Scheduler;

    .line 66
    new-instance p4, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p4}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p4, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 69
    check-cast p1, Landroid/content/Context;

    invoke-static {p1}, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;->builder(Landroid/content/Context;)Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;

    move-result-object p1

    .line 70
    new-instance p4, Ljava/io/File;

    const-string v0, "store-and-forward.db"

    invoke-direct {p4, p2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;->name(Ljava/lang/String;)Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;

    move-result-object p1

    .line 71
    new-instance p2, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$DatabaseCallback;

    invoke-direct {p2}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$DatabaseCallback;-><init>()V

    check-cast p2, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;

    invoke-virtual {p1, p2}, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;->callback(Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;)Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;

    move-result-object p1

    .line 72
    invoke-virtual {p1}, Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;->build()Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;

    move-result-object p1

    .line 74
    new-instance p2, Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelperFactory;

    invoke-direct {p2}, Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelperFactory;-><init>()V

    invoke-virtual {p2, p1}, Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelperFactory;->create(Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;)Landroidx/sqlite/db/SupportSQLiteOpenHelper;

    move-result-object p1

    .line 75
    iget-object p2, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->fileScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p3, p1, p2}, Lcom/squareup/sqlbrite3/SqlBrite;->wrapDatabaseHelper(Landroidx/sqlite/db/SupportSQLiteOpenHelper;Lio/reactivex/Scheduler;)Lcom/squareup/sqlbrite3/BriteDatabase;

    move-result-object p1

    const-string p2, "sqlBrite.wrapDatabaseHelper(helper, fileScheduler)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    .line 77
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    .line 79
    sget-object p1, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;->Companion:Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion;

    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion;->getFACTORY()Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;->count()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    move-result-object v1

    const-string p1, "FACTORY.count()"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 80
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 81
    iget-object v3, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->fileScheduler:Lio/reactivex/Scheduler;

    .line 82
    iget-object v4, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 84
    sget-object p2, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$1;->INSTANCE:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$1;

    move-object v6, p2

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const-string v5, "Unable to fetch count"

    move-object v2, p1

    .line 78
    invoke-static/range {v0 .. v6}, Lcom/squareup/queue/sqlite/QueueStoresKt;->queryResults(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Ljava/lang/Object;Lio/reactivex/Scheduler;Lio/reactivex/disposables/CompositeDisposable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p2

    .line 85
    invoke-virtual {p2}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p2

    const-string p3, "db\n        .queryResults\u2026  .distinctUntilChanged()"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->count:Lio/reactivex/Observable;

    .line 87
    iget-object v2, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    .line 89
    sget-object p2, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;->Companion:Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion;

    invoke-virtual {p2}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion;->getFACTORY()Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;->distinctCount()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    move-result-object v3

    const-string p2, "FACTORY.distinctCount()"

    invoke-static {v3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    iget-object v5, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->fileScheduler:Lio/reactivex/Scheduler;

    .line 92
    iget-object v6, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 94
    sget-object p2, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$2;->INSTANCE:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$2;

    move-object v8, p2

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const-string v7, "Unable to fetch distinct count"

    move-object v4, p1

    .line 88
    invoke-static/range {v2 .. v8}, Lcom/squareup/queue/sqlite/QueueStoresKt;->queryResults(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Ljava/lang/Object;Lio/reactivex/Scheduler;Lio/reactivex/disposables/CompositeDisposable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p1

    .line 95
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->distinctCount:Lio/reactivex/Observable;

    .line 97
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    .line 98
    sget-object p1, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;->Companion:Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion;

    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion;->getFACTORY()Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;->firstEntry()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    move-result-object v1

    const-string p1, "FACTORY.firstEntry()"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    iget-object v2, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->fileScheduler:Lio/reactivex/Scheduler;

    .line 100
    iget-object v3, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 102
    sget-object p1, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$3;->INSTANCE:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$3;

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const-string v4, "Unable to fetch first entry"

    .line 97
    invoke-static/range {v0 .. v5}, Lcom/squareup/queue/sqlite/QueueStoresKt;->queryOptionalResults(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Lio/reactivex/Scheduler;Lio/reactivex/disposables/CompositeDisposable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->firstEntry:Lio/reactivex/Observable;

    .line 106
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    .line 107
    sget-object p1, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;->Companion:Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion;

    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion;->getFACTORY()Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;->oldestEntry()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    move-result-object v1

    const-string p1, "FACTORY.oldestEntry()"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    iget-object v2, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->fileScheduler:Lio/reactivex/Scheduler;

    .line 109
    iget-object v3, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 111
    sget-object p1, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$4;->INSTANCE:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$4;

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const-string v4, "Unable to fetch oldest entry"

    .line 106
    invoke-static/range {v0 .. v5}, Lcom/squareup/queue/sqlite/QueueStoresKt;->queryOptionalResults(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Lio/reactivex/Scheduler;Lio/reactivex/disposables/CompositeDisposable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->oldestEntry:Lio/reactivex/Observable;

    .line 115
    iget-object p1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    .line 116
    sget-object p2, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;->Companion:Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion;

    invoke-virtual {p2}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion;->getFACTORY()Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;->allDistinctEntries()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    move-result-object p2

    const-string p3, "FACTORY.allDistinctEntries()"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    sget-object p3, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;->Companion:Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion;

    invoke-virtual {p3}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion;->getALL_DISTINCT_ENTRIES_MAPPER()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object p3

    .line 118
    iget-object p4, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->fileScheduler:Lio/reactivex/Scheduler;

    const-string v0, "Unable to fetch all distinct entries"

    .line 115
    invoke-static {p1, p2, p3, p4, v0}, Lcom/squareup/queue/sqlite/QueueStoresKt;->allEntriesAsStream(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Lcom/squareup/sqldelight/prerelease/RowMapper;Lio/reactivex/Scheduler;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->allDistinctEntries:Lio/reactivex/Observable;

    .line 122
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    .line 123
    sget-object p1, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;->Companion:Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion;

    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion;->getFACTORY()Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;->allDistinctEntryIds()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    move-result-object v1

    const-string p1, "FACTORY.allDistinctEntryIds()"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 125
    iget-object v3, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->fileScheduler:Lio/reactivex/Scheduler;

    .line 126
    iget-object v4, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 128
    sget-object p1, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$5;->INSTANCE:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$5;

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const-string v5, "Unable to fetch all distinct entry ids"

    .line 122
    invoke-static/range {v0 .. v6}, Lcom/squareup/queue/sqlite/QueueStoresKt;->queryResults(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Ljava/lang/Object;Lio/reactivex/Scheduler;Lio/reactivex/disposables/CompositeDisposable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->allDistinctEntryIds:Lio/reactivex/Observable;

    .line 132
    new-instance p1, Lcom/squareup/queue/sqlite/StoredPaymentsModel$InsertEntry;

    iget-object p2, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-virtual {p2}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$InsertEntry;-><init>(Landroidx/sqlite/db/SupportSQLiteDatabase;)V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->insert:Lcom/squareup/queue/sqlite/StoredPaymentsModel$InsertEntry;

    .line 133
    new-instance p1, Lcom/squareup/queue/sqlite/StoredPaymentsModel$DeleteFirstEntry;

    iget-object p2, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-virtual {p2}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$DeleteFirstEntry;-><init>(Landroidx/sqlite/db/SupportSQLiteDatabase;)V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->deleteFirst:Lcom/squareup/queue/sqlite/StoredPaymentsModel$DeleteFirstEntry;

    .line 134
    new-instance p1, Lcom/squareup/queue/sqlite/StoredPaymentsModel$DeleteAllEntries;

    iget-object p2, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-virtual {p2}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$DeleteAllEntries;-><init>(Landroidx/sqlite/db/SupportSQLiteDatabase;)V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->deleteAll:Lcom/squareup/queue/sqlite/StoredPaymentsModel$DeleteAllEntries;

    return-void
.end method

.method public static final synthetic access$getDb$p(Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;)Lcom/squareup/sqlbrite3/BriteDatabase;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    return-object p0
.end method

.method public static final synthetic access$getInsert$p(Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;)Lcom/squareup/queue/sqlite/StoredPaymentsModel$InsertEntry;
    .locals 0

    .line 47
    iget-object p0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->insert:Lcom/squareup/queue/sqlite/StoredPaymentsModel$InsertEntry;

    return-object p0
.end method

.method public static final synthetic access$preload(Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->preload()V

    return-void
.end method

.method public static final create(Landroid/app/Application;Ljava/io/File;Lio/reactivex/Scheduler;)Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;
    .locals 1
    .param p1    # Ljava/io/File;
        .annotation runtime Lcom/squareup/user/UserDirectory;
        .end annotation
    .end param
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->Companion:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$Companion;->create(Landroid/app/Application;Ljava/io/File;Lio/reactivex/Scheduler;)Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic disposables$annotations()V
    .locals 0

    return-void
.end method

.method private final preload()V
    .locals 2

    .line 226
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->count:Lio/reactivex/Observable;

    invoke-virtual {v1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 227
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->distinctCount:Lio/reactivex/Observable;

    invoke-virtual {v1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 228
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->firstEntry:Lio/reactivex/Observable;

    invoke-virtual {v1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 229
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->oldestEntry:Lio/reactivex/Observable;

    invoke-virtual {v1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 230
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->allDistinctEntryIds:Lio/reactivex/Observable;

    invoke-virtual {v1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method


# virtual methods
.method public final allDistinctEntriesAsStream()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
            ">;"
        }
    .end annotation

    .line 184
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->allDistinctEntries:Lio/reactivex/Observable;

    return-object v0
.end method

.method public final allDistinctEntryIds()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 201
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->allDistinctEntryIds:Lio/reactivex/Observable;

    return-object v0
.end method

.method public allEntries()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
            ">;>;"
        }
    .end annotation

    .line 219
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->count:Lio/reactivex/Observable;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->allDistinctEntries:Lio/reactivex/Observable;

    invoke-static {v0, v1}, Lcom/squareup/queue/sqlite/QueueStoresKt;->allEntriesAsList(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public close()Lio/reactivex/Completable;
    .locals 2

    .line 213
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 214
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    invoke-static {v0}, Lcom/squareup/queue/sqlite/QueueStoresKt;->closeReactive(Lcom/squareup/sqlbrite3/BriteDatabase;)Lio/reactivex/Completable;

    move-result-object v0

    .line 215
    iget-object v1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->fileScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Completable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v0

    const-string v1, "db.closeReactive()\n     \u2026ubscribeOn(fileScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public count()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 143
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->count:Lio/reactivex/Observable;

    return-object v0
.end method

.method public deleteAllEntries()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 207
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    .line 208
    iget-object v1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->deleteAll:Lcom/squareup/queue/sqlite/StoredPaymentsModel$DeleteAllEntries;

    check-cast v1, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;

    invoke-static {v0, v1}, Lcom/squareup/queue/sqlite/QueueStoresKt;->delete(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;)Lio/reactivex/Single;

    move-result-object v0

    .line 209
    iget-object v1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->fileScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "db\n      .delete(deleteA\u2026ubscribeOn(fileScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public deleteFirstEntry()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 203
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    .line 204
    iget-object v1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->deleteFirst:Lcom/squareup/queue/sqlite/StoredPaymentsModel$DeleteFirstEntry;

    check-cast v1, Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;

    invoke-static {v0, v1}, Lcom/squareup/queue/sqlite/QueueStoresKt;->delete(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;)Lio/reactivex/Single;

    move-result-object v0

    .line 205
    iget-object v1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->fileScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "db\n      .delete(deleteF\u2026ubscribeOn(fileScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final distinctCount()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 152
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->distinctCount:Lio/reactivex/Observable;

    return-object v0
.end method

.method public fetchEntry(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
            ">;>;"
        }
    .end annotation

    const-string v0, "entryId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    iget-object v1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->db:Lcom/squareup/sqlbrite3/BriteDatabase;

    .line 158
    sget-object v0, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;->Companion:Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion;->getFACTORY()Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;->getEntry(Ljava/lang/String;)Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    move-result-object v2

    const-string v0, "FACTORY.getEntry(entryId)"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    iget-object v3, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->fileScheduler:Lio/reactivex/Scheduler;

    .line 160
    iget-object v4, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to fetch entry with entryId: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 162
    sget-object p1, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$fetchEntry$fetchEntryResult$1;->INSTANCE:Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$fetchEntry$fetchEntryResult$1;

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 157
    invoke-static/range {v1 .. v6}, Lcom/squareup/queue/sqlite/QueueStoresKt;->queryOptionalResults(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Lio/reactivex/Scheduler;Lio/reactivex/disposables/CompositeDisposable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p1

    const-wide/16 v0, 0x1

    .line 164
    invoke-virtual {p1, v0, v1}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object p1

    check-cast p1, Lio/reactivex/ObservableSource;

    invoke-static {p1}, Lio/reactivex/Single;->fromObservable(Lio/reactivex/ObservableSource;)Lio/reactivex/Single;

    move-result-object p1

    .line 165
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->fileScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.fromObservable(fe\u2026ubscribeOn(fileScheduler)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public firstEntry()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
            ">;>;"
        }
    .end annotation

    .line 154
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->firstEntry:Lio/reactivex/Observable;

    return-object v0
.end method

.method public final getDisposables()Lio/reactivex/disposables/CompositeDisposable;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    return-object v0
.end method

.method public insert(Lcom/squareup/queue/sqlite/StoredPaymentsEntry;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "entry"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    new-instance v0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$insert$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore$insert$1;-><init>(Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;Lcom/squareup/queue/sqlite/StoredPaymentsEntry;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object p1

    .line 195
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->fileScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single\n      .fromCallab\u2026ubscribeOn(fileScheduler)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic insert(Ljava/lang/Object;)Lio/reactivex/Single;
    .locals 0

    .line 47
    check-cast p1, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->insert(Lcom/squareup/queue/sqlite/StoredPaymentsEntry;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public final oldestEntry()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
            ">;>;"
        }
    .end annotation

    .line 172
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->oldestEntry:Lio/reactivex/Observable;

    return-object v0
.end method
