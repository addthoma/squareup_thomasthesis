.class public Lcom/squareup/queue/QueueJobCreator;
.super Lcom/squareup/backgroundjob/BackgroundJobCreator;
.source "QueueJobCreator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;
    }
.end annotation


# instance fields
.field private final jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

.field private final queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;


# direct methods
.method public constructor <init>(Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/queue/QueueServiceStarter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 37
    invoke-direct {p0, p2}, Lcom/squareup/backgroundjob/BackgroundJobCreator;-><init>(Lcom/squareup/backgroundjob/BackgroundJobManager;)V

    .line 38
    iput-object p1, p0, Lcom/squareup/queue/QueueJobCreator;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    .line 39
    iput-object p3, p0, Lcom/squareup/queue/QueueJobCreator;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/String;)Lcom/evernote/android/job/Job;
    .locals 2

    .line 44
    sget-object v0, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->START_TAG:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;->START_WITH_NETWORK_TAG:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    new-instance p1, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;

    iget-object v0, p0, Lcom/squareup/queue/QueueJobCreator;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    iget-object v1, p0, Lcom/squareup/queue/QueueJobCreator;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    invoke-direct {p1, v0, v1}, Lcom/squareup/queue/QueueJobCreator$StartQueueServiceJob;-><init>(Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/queue/QueueServiceStarter;)V

    :goto_1
    return-object p1
.end method
