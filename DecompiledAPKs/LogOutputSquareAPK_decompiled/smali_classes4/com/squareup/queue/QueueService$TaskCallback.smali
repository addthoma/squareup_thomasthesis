.class Lcom/squareup/queue/QueueService$TaskCallback;
.super Lcom/squareup/server/SquareCallback;
.source "QueueService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/QueueService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TaskCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/server/SquareCallback<",
        "Lcom/squareup/server/SimpleResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final task:Lcom/squareup/queue/retrofit/RetrofitTask;

.field private final taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private final taskWatcher:Lcom/squareup/queue/TaskWatcher;

.field final synthetic this$0:Lcom/squareup/queue/QueueService;


# direct methods
.method constructor <init>(Lcom/squareup/queue/QueueService;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/retrofit/RetrofitTask;Lcom/squareup/queue/TaskWatcher;)V
    .locals 0

    .line 341
    iput-object p1, p0, Lcom/squareup/queue/QueueService$TaskCallback;->this$0:Lcom/squareup/queue/QueueService;

    invoke-direct {p0}, Lcom/squareup/server/SquareCallback;-><init>()V

    .line 342
    iput-object p3, p0, Lcom/squareup/queue/QueueService$TaskCallback;->task:Lcom/squareup/queue/retrofit/RetrofitTask;

    .line 343
    iput-object p2, p0, Lcom/squareup/queue/QueueService$TaskCallback;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 344
    iput-object p4, p0, Lcom/squareup/queue/QueueService$TaskCallback;->taskWatcher:Lcom/squareup/queue/TaskWatcher;

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/server/SimpleResponse;)V
    .locals 4

    .line 348
    iget-object v0, p0, Lcom/squareup/queue/QueueService$TaskCallback;->this$0:Lcom/squareup/queue/QueueService;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/squareup/queue/QueueService;->access$200(Lcom/squareup/queue/QueueService;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p1}, Lcom/squareup/server/SimpleResponse;->isSuccessful()Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "succeeded"

    goto :goto_0

    :cond_0
    const-string p1, "failed"

    :goto_0
    const/4 v2, 0x1

    aput-object p1, v1, v2

    const-string p1, "%s %s."

    invoke-static {v0, p1, v1}, Lcom/squareup/queue/QueueService;->access$300(Lcom/squareup/queue/QueueService;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 349
    invoke-virtual {p0}, Lcom/squareup/queue/QueueService$TaskCallback;->removeTask()V

    .line 350
    iget-object p1, p0, Lcom/squareup/queue/QueueService$TaskCallback;->this$0:Lcom/squareup/queue/QueueService;

    const-string v0, "task handled"

    invoke-virtual {p1, v0}, Lcom/squareup/queue/QueueService;->restart(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 336
    check-cast p1, Lcom/squareup/server/SimpleResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/QueueService$TaskCallback;->call(Lcom/squareup/server/SimpleResponse;)V

    return-void
.end method

.method public clientError(Lcom/squareup/server/SimpleResponse;I)V
    .locals 8

    const-string v0, "client error"

    .line 371
    :try_start_0
    iget-object v1, p0, Lcom/squareup/queue/QueueService$TaskCallback;->task:Lcom/squareup/queue/retrofit/RetrofitTask;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 372
    iget-object v2, p0, Lcom/squareup/queue/QueueService$TaskCallback;->this$0:Lcom/squareup/queue/QueueService;

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "got client error"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const-string v4, "Client Error %d executing %s! %s %s"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 373
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v1, v5, v6

    const/4 v1, 0x2

    aput-object p1, v5, v1

    const/4 p1, 0x3

    iget-object v1, p0, Lcom/squareup/queue/QueueService$TaskCallback;->this$0:Lcom/squareup/queue/QueueService;

    invoke-static {v1}, Lcom/squareup/queue/QueueService;->access$200(Lcom/squareup/queue/QueueService;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, p1

    .line 372
    invoke-static {v2, v3, v4, v5}, Lcom/squareup/queue/QueueService;->access$400(Lcom/squareup/queue/QueueService;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 378
    invoke-static {p2}, Lcom/squareup/http/ClientErrors;->isRetryableClientError(I)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 380
    sget-object p1, Lcom/squareup/queue/QueueService$ErrorType;->CLIENT:Lcom/squareup/queue/QueueService$ErrorType;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/QueueService$TaskCallback;->retryLater(Lcom/squareup/queue/QueueService$ErrorType;)V

    goto :goto_0

    .line 383
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/queue/QueueService$TaskCallback;->removeTask()V

    .line 384
    iget-object p1, p0, Lcom/squareup/queue/QueueService$TaskCallback;->this$0:Lcom/squareup/queue/QueueService;

    invoke-virtual {p1, v0}, Lcom/squareup/queue/QueueService;->restart(Ljava/lang/String;)V

    :goto_0
    return-void

    :catchall_0
    move-exception p1

    .line 378
    invoke-static {p2}, Lcom/squareup/http/ClientErrors;->isRetryableClientError(I)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 380
    sget-object p2, Lcom/squareup/queue/QueueService$ErrorType;->CLIENT:Lcom/squareup/queue/QueueService$ErrorType;

    invoke-virtual {p0, p2}, Lcom/squareup/queue/QueueService$TaskCallback;->retryLater(Lcom/squareup/queue/QueueService$ErrorType;)V

    goto :goto_1

    .line 383
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/queue/QueueService$TaskCallback;->removeTask()V

    .line 384
    iget-object p2, p0, Lcom/squareup/queue/QueueService$TaskCallback;->this$0:Lcom/squareup/queue/QueueService;

    invoke-virtual {p2, v0}, Lcom/squareup/queue/QueueService;->restart(Ljava/lang/String;)V

    .line 386
    :goto_1
    throw p1
.end method

.method public bridge synthetic clientError(Ljava/lang/Object;I)V
    .locals 0

    .line 336
    check-cast p1, Lcom/squareup/server/SimpleResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/queue/QueueService$TaskCallback;->clientError(Lcom/squareup/server/SimpleResponse;I)V

    return-void
.end method

.method public networkError()V
    .locals 3

    .line 361
    iget-object v0, p0, Lcom/squareup/queue/QueueService$TaskCallback;->this$0:Lcom/squareup/queue/QueueService;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Got network error."

    invoke-static {v0, v2, v1}, Lcom/squareup/queue/QueueService;->access$300(Lcom/squareup/queue/QueueService;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 362
    sget-object v0, Lcom/squareup/queue/QueueService$ErrorType;->NETWORK:Lcom/squareup/queue/QueueService$ErrorType;

    invoke-virtual {p0, v0}, Lcom/squareup/queue/QueueService$TaskCallback;->retryLater(Lcom/squareup/queue/QueueService$ErrorType;)V

    return-void
.end method

.method removeTask()V
    .locals 1

    .line 404
    iget-object v0, p0, Lcom/squareup/queue/QueueService$TaskCallback;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-interface {v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->remove()V

    .line 405
    iget-object v0, p0, Lcom/squareup/queue/QueueService$TaskCallback;->taskWatcher:Lcom/squareup/queue/TaskWatcher;

    invoke-virtual {v0}, Lcom/squareup/queue/TaskWatcher;->taskCompleted()V

    return-void
.end method

.method retryLater(Lcom/squareup/queue/QueueService$ErrorType;)V
    .locals 1

    .line 409
    iget-object v0, p0, Lcom/squareup/queue/QueueService$TaskCallback;->taskWatcher:Lcom/squareup/queue/TaskWatcher;

    invoke-virtual {v0}, Lcom/squareup/queue/TaskWatcher;->taskRequiresRetry()V

    .line 410
    iget-object v0, p0, Lcom/squareup/queue/QueueService$TaskCallback;->this$0:Lcom/squareup/queue/QueueService;

    invoke-static {v0, p1}, Lcom/squareup/queue/QueueService;->access$500(Lcom/squareup/queue/QueueService;Lcom/squareup/queue/QueueService$ErrorType;)V

    return-void
.end method

.method public serverError(I)V
    .locals 3

    .line 390
    iget-object v0, p0, Lcom/squareup/queue/QueueService$TaskCallback;->this$0:Lcom/squareup/queue/QueueService;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string p1, "got server error %s"

    invoke-static {v0, p1, v1}, Lcom/squareup/queue/QueueService;->access$300(Lcom/squareup/queue/QueueService;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 391
    sget-object p1, Lcom/squareup/queue/QueueService$ErrorType;->SERVER:Lcom/squareup/queue/QueueService$ErrorType;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/QueueService$TaskCallback;->retryLater(Lcom/squareup/queue/QueueService$ErrorType;)V

    return-void
.end method

.method public sessionExpired()V
    .locals 3

    .line 355
    iget-object v0, p0, Lcom/squareup/queue/QueueService$TaskCallback;->this$0:Lcom/squareup/queue/QueueService;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Stopping task queue."

    invoke-static {v0, v2, v1}, Lcom/squareup/queue/QueueService;->access$300(Lcom/squareup/queue/QueueService;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 356
    iget-object v0, p0, Lcom/squareup/queue/QueueService$TaskCallback;->this$0:Lcom/squareup/queue/QueueService;

    iget-object v0, v0, Lcom/squareup/queue/QueueService;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance v1, Lcom/squareup/account/AccountEvents$SessionExpired;

    invoke-direct {v1}, Lcom/squareup/account/AccountEvents$SessionExpired;-><init>()V

    invoke-interface {v0, v1}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    .line 357
    iget-object v0, p0, Lcom/squareup/queue/QueueService$TaskCallback;->this$0:Lcom/squareup/queue/QueueService;

    invoke-virtual {v0}, Lcom/squareup/queue/QueueService;->stopSelf()V

    return-void
.end method

.method public unexpectedError(Ljava/lang/Throwable;)V
    .locals 4

    .line 399
    iget-object v0, p0, Lcom/squareup/queue/QueueService$TaskCallback;->this$0:Lcom/squareup/queue/QueueService;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/squareup/queue/QueueService;->access$200(Lcom/squareup/queue/QueueService;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "Error executing %s!"

    invoke-static {v0, p1, v2, v1}, Lcom/squareup/queue/QueueService;->access$400(Lcom/squareup/queue/QueueService;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 400
    sget-object p1, Lcom/squareup/queue/QueueService$ErrorType;->UNEXPECTED:Lcom/squareup/queue/QueueService$ErrorType;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/QueueService$TaskCallback;->retryLater(Lcom/squareup/queue/QueueService$ErrorType;)V

    return-void
.end method
