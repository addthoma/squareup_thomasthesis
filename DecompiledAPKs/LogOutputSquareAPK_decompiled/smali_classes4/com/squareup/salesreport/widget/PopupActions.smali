.class public final Lcom/squareup/salesreport/widget/PopupActions;
.super Ljava/lang/Object;
.source "PopupActions.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPopupActions.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PopupActions.kt\ncom/squareup/salesreport/widget/PopupActions\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,127:1\n1103#2,7:128\n*E\n*S KotlinDebug\n*F\n+ 1 PopupActions.kt\ncom/squareup/salesreport/widget/PopupActions\n*L\n89#1,7:128\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ6\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u00132\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u00152\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u000e\u0010\u001b\u001a\u00020\u00162\u0006\u0010\u001c\u001a\u00020\u001dR\u0010\u0010\u000c\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/salesreport/widget/PopupActions;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "context",
        "Landroid/content/Context;",
        "actions",
        "",
        "Lcom/squareup/salesreport/widget/PopupAction;",
        "showAsAlertDialog",
        "",
        "(Lcom/squareup/util/Res;Landroid/content/Context;Ljava/util/List;Z)V",
        "alertDialog",
        "Landroid/app/AlertDialog;",
        "popupWindow",
        "Landroid/widget/PopupWindow;",
        "createButton",
        "Lcom/squareup/noho/NohoButton;",
        "label",
        "",
        "action",
        "Lkotlin/Function0;",
        "",
        "type",
        "Lcom/squareup/noho/NohoButtonType;",
        "buttonEdges",
        "",
        "toggle",
        "anchorView",
        "Landroid/view/View;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private alertDialog:Landroid/app/AlertDialog;

.field private popupWindow:Landroid/widget/PopupWindow;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Landroid/content/Context;Ljava/util/List;Z)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/widget/PopupAction;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actions"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    move-object v0, p3

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_6

    .line 36
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 37
    sget v2, Lcom/squareup/salesreport/impl/R$layout;->popup_actions_view:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 38
    sget v2, Lcom/squareup/salesreport/impl/R$id;->popup_actions_container:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const-string v3, "popupView.findViewById(R\u2026.popup_actions_container)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/noho/NohoLinearLayout;

    if-eqz p4, :cond_0

    .line 41
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 42
    invoke-virtual {v1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/salesreport/widget/PopupActions;->alertDialog:Landroid/app/AlertDialog;

    goto :goto_0

    .line 45
    :cond_0
    invoke-virtual {v2, v4, v4, v4, v4}, Lcom/squareup/noho/NohoLinearLayout;->setPadding(IIII)V

    .line 46
    new-instance v3, Landroid/widget/PopupWindow;

    .line 48
    sget v5, Lcom/squareup/salesreport/impl/R$dimen;->popup_actions_view_width:I

    invoke-interface {p1, v5}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v5

    const/4 v6, -0x2

    .line 46
    invoke-direct {v3, v0, v5, v6, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v3, p0, Lcom/squareup/salesreport/widget/PopupActions;->popupWindow:Landroid/widget/PopupWindow;

    .line 54
    :goto_0
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_1
    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/salesreport/widget/PopupAction;

    .line 57
    invoke-virtual {v0}, Lcom/squareup/salesreport/widget/PopupAction;->getTitle()Ljava/lang/String;

    move-result-object v7

    .line 58
    invoke-virtual {v0}, Lcom/squareup/salesreport/widget/PopupAction;->getAction()Lkotlin/jvm/functions/Function0;

    move-result-object v8

    if-eqz p4, :cond_2

    .line 59
    sget-object v0, Lcom/squareup/noho/NohoButtonType;->SECONDARY:Lcom/squareup/noho/NohoButtonType;

    goto :goto_2

    :cond_2
    sget-object v0, Lcom/squareup/noho/NohoButtonType;->TERTIARY:Lcom/squareup/noho/NohoButtonType;

    :goto_2
    move-object v9, v0

    if-eqz p4, :cond_3

    const/4 v10, 0x0

    goto :goto_3

    :cond_3
    const/16 v0, 0xf

    const/16 v10, 0xf

    :goto_3
    move-object v5, p0

    move-object v6, p2

    .line 55
    invoke-direct/range {v5 .. v10}, Lcom/squareup/salesreport/widget/PopupActions;->createButton(Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lcom/squareup/noho/NohoButtonType;I)Lcom/squareup/noho/NohoButton;

    move-result-object v0

    .line 62
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    invoke-virtual {v2, v1}, Lcom/squareup/noho/NohoLinearLayout;->addView(Landroid/view/View;)V

    .line 64
    invoke-virtual {v0}, Lcom/squareup/noho/NohoButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz p4, :cond_1

    .line 65
    instance-of v1, v0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;

    if-eqz v1, :cond_1

    .line 67
    check-cast v0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;

    sget v1, Lcom/squareup/salesreport/impl/R$dimen;->popup_actions_view_button_gap:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;->bottomMargin:I

    goto :goto_1

    :cond_4
    if-eqz p4, :cond_5

    .line 74
    sget p3, Lcom/squareup/salesreport/impl/R$string;->popup_actions_view_dismiss:I

    invoke-interface {p1, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 75
    sget-object p1, Lcom/squareup/salesreport/widget/PopupActions$button$1;->INSTANCE:Lcom/squareup/salesreport/widget/PopupActions$button$1;

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 76
    sget-object v7, Lcom/squareup/noho/NohoButtonType;->TERTIARY:Lcom/squareup/noho/NohoButtonType;

    const/4 v8, 0x0

    move-object v3, p0

    move-object v4, p2

    .line 72
    invoke-direct/range {v3 .. v8}, Lcom/squareup/salesreport/widget/PopupActions;->createButton(Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lcom/squareup/noho/NohoButtonType;I)Lcom/squareup/noho/NohoButton;

    move-result-object p1

    .line 79
    check-cast p1, Landroid/view/View;

    invoke-virtual {v2, p1}, Lcom/squareup/noho/NohoLinearLayout;->addView(Landroid/view/View;)V

    :cond_5
    return-void

    .line 35
    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "List of PopupAction should not be empty."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final synthetic access$getAlertDialog$p(Lcom/squareup/salesreport/widget/PopupActions;)Landroid/app/AlertDialog;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/salesreport/widget/PopupActions;->alertDialog:Landroid/app/AlertDialog;

    return-object p0
.end method

.method public static final synthetic access$getPopupWindow$p(Lcom/squareup/salesreport/widget/PopupActions;)Landroid/widget/PopupWindow;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/salesreport/widget/PopupActions;->popupWindow:Landroid/widget/PopupWindow;

    return-object p0
.end method

.method public static final synthetic access$setAlertDialog$p(Lcom/squareup/salesreport/widget/PopupActions;Landroid/app/AlertDialog;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/squareup/salesreport/widget/PopupActions;->alertDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method public static final synthetic access$setPopupWindow$p(Lcom/squareup/salesreport/widget/PopupActions;Landroid/widget/PopupWindow;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/squareup/salesreport/widget/PopupActions;->popupWindow:Landroid/widget/PopupWindow;

    return-void
.end method

.method private final createButton(Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lcom/squareup/noho/NohoButtonType;I)Lcom/squareup/noho/NohoButton;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/noho/NohoButtonType;",
            "I)",
            "Lcom/squareup/noho/NohoButton;"
        }
    .end annotation

    .line 89
    new-instance v6, Lcom/squareup/noho/NohoButton;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, v6

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 90
    invoke-virtual {v6, p4}, Lcom/squareup/noho/NohoButton;->apply(Lcom/squareup/noho/NohoButtonType;)V

    .line 91
    move-object p1, p2

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v6, p1}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    const/16 p1, 0x11

    .line 92
    invoke-virtual {v6, p1}, Lcom/squareup/noho/NohoButton;->setGravity(I)V

    .line 93
    new-instance p1, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$1;

    move-object v0, p1

    move-object v1, p0

    move-object v2, p4

    move-object v3, p2

    move v4, p5

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$1;-><init>(Lcom/squareup/salesreport/widget/PopupActions;Lcom/squareup/noho/NohoButtonType;Ljava/lang/String;ILkotlin/jvm/functions/Function0;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v6, p1}, Lcom/squareup/noho/NohoButton;->configureEdges(Lkotlin/jvm/functions/Function1;)V

    .line 96
    move-object p1, v6

    check-cast p1, Landroid/view/View;

    .line 128
    new-instance v7, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$2;

    move-object v0, v7

    invoke-direct/range {v0 .. v5}, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$2;-><init>(Lcom/squareup/salesreport/widget/PopupActions;Lcom/squareup/noho/NohoButtonType;Ljava/lang/String;ILkotlin/jvm/functions/Function0;)V

    check-cast v7, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v6
.end method


# virtual methods
.method public final toggle(Landroid/view/View;)V
    .locals 3

    const-string v0, "anchorView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/salesreport/widget/PopupActions;->alertDialog:Landroid/app/AlertDialog;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-ne v0, v1, :cond_0

    .line 105
    iget-object v0, p0, Lcom/squareup/salesreport/widget/PopupActions;->alertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/AlertDialog;->hide()V

    goto :goto_0

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/squareup/salesreport/widget/PopupActions;->alertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 110
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/salesreport/widget/PopupActions;->popupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-ne v0, v1, :cond_2

    .line 111
    iget-object p1, p0, Lcom/squareup/salesreport/widget/PopupActions;->popupWindow:Landroid/widget/PopupWindow;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Landroid/widget/PopupWindow;->dismiss()V

    goto :goto_1

    .line 113
    :cond_2
    iget-object v0, p0, Lcom/squareup/salesreport/widget/PopupActions;->popupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_3

    const v1, 0x800005

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2, v2, v1}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;III)V

    :cond_3
    :goto_1
    return-void
.end method
