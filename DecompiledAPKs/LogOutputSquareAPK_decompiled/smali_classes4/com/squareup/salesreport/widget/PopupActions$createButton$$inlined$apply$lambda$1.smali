.class final Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "PopupActions.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/widget/PopupActions;->createButton(Landroid/content/Context;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lcom/squareup/noho/NohoButtonType;I)Lcom/squareup/noho/NohoButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/noho/EdgePainter;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/noho/EdgePainter;",
        "invoke",
        "com/squareup/salesreport/widget/PopupActions$createButton$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $action$inlined:Lkotlin/jvm/functions/Function0;

.field final synthetic $buttonEdges$inlined:I

.field final synthetic $label$inlined:Ljava/lang/String;

.field final synthetic $type$inlined:Lcom/squareup/noho/NohoButtonType;

.field final synthetic this$0:Lcom/squareup/salesreport/widget/PopupActions;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/widget/PopupActions;Lcom/squareup/noho/NohoButtonType;Ljava/lang/String;ILkotlin/jvm/functions/Function0;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$1;->this$0:Lcom/squareup/salesreport/widget/PopupActions;

    iput-object p2, p0, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$1;->$type$inlined:Lcom/squareup/noho/NohoButtonType;

    iput-object p3, p0, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$1;->$label$inlined:Ljava/lang/String;

    iput p4, p0, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$1;->$buttonEdges$inlined:I

    iput-object p5, p0, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$1;->$action$inlined:Lkotlin/jvm/functions/Function0;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/noho/EdgePainter;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$1;->invoke(Lcom/squareup/noho/EdgePainter;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/noho/EdgePainter;)V
    .locals 1

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    iget v0, p0, Lcom/squareup/salesreport/widget/PopupActions$createButton$$inlined$apply$lambda$1;->$buttonEdges$inlined:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/EdgePainter;->setEdges(I)V

    return-void
.end method
