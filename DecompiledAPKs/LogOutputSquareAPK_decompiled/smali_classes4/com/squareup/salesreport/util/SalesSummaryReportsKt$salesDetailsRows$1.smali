.class final Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SalesSummaryReports.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/util/SalesSummaryReportsKt;->salesDetailsRows(Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/PercentageChangeFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/common/Money;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0004\u0018\u00010\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "format",
        "",
        "Lcom/squareup/protos/common/Money;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $moneyFormatter:Lcom/squareup/text/Formatter;


# direct methods
.method constructor <init>(Lcom/squareup/text/Formatter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$1;->$moneyFormatter:Lcom/squareup/text/Formatter;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$1;->invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/common/Money;)Ljava/lang/String;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesSummaryReportsKt$salesDetailsRows$1;->$moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
