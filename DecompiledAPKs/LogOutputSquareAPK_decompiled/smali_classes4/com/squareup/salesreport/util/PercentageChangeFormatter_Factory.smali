.class public final Lcom/squareup/salesreport/util/PercentageChangeFormatter_Factory;
.super Ljava/lang/Object;
.source "PercentageChangeFormatter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/salesreport/util/PercentageChangeFormatter_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/salesreport/util/PercentageChangeFormatter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;)",
            "Lcom/squareup/salesreport/util/PercentageChangeFormatter_Factory;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/salesreport/util/PercentageChangeFormatter_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/salesreport/util/PercentageChangeFormatter_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/text/Formatter;)Lcom/squareup/salesreport/util/PercentageChangeFormatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)",
            "Lcom/squareup/salesreport/util/PercentageChangeFormatter;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/salesreport/util/PercentageChangeFormatter;

    invoke-direct {v0, p0}, Lcom/squareup/salesreport/util/PercentageChangeFormatter;-><init>(Lcom/squareup/text/Formatter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/salesreport/util/PercentageChangeFormatter;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/salesreport/util/PercentageChangeFormatter_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {v0}, Lcom/squareup/salesreport/util/PercentageChangeFormatter_Factory;->newInstance(Lcom/squareup/text/Formatter;)Lcom/squareup/salesreport/util/PercentageChangeFormatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/PercentageChangeFormatter_Factory;->get()Lcom/squareup/salesreport/util/PercentageChangeFormatter;

    move-result-object v0

    return-object v0
.end method
