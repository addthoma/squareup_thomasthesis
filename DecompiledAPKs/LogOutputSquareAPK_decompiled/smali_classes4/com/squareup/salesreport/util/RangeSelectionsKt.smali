.class public final Lcom/squareup/salesreport/util/RangeSelectionsKt;
.super Ljava/lang/Object;
.source "RangeSelections.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u001c\u0010\r\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000fH\u0001\u001a\u0014\u0010\u0011\u001a\u00020\u0012*\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0014H\u0000\u001a*\u0010\u0015\u001a\u00020\u0012*\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u00142\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018H\u0000\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0018\u0010\u0005\u001a\u00020\u0006*\u00020\u00028AX\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008\"\u001e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00010\n*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u001a"
    }
    d2 = {
        "defaultComparisonRange",
        "Lcom/squareup/customreport/data/ComparisonRange;",
        "Lcom/squareup/customreport/data/RangeSelection;",
        "getDefaultComparisonRange",
        "(Lcom/squareup/customreport/data/RangeSelection;)Lcom/squareup/customreport/data/ComparisonRange;",
        "description",
        "",
        "getDescription",
        "(Lcom/squareup/customreport/data/RangeSelection;)I",
        "possibleComparisonRanges",
        "",
        "getPossibleComparisonRanges",
        "(Lcom/squareup/customreport/data/RangeSelection;)Ljava/util/List;",
        "dateRangeFormatRes",
        "singleDayRange",
        "",
        "allDay",
        "endDateFormatter",
        "Lorg/threeten/bp/format/DateTimeFormatter;",
        "res",
        "Lcom/squareup/util/Res;",
        "startDateFormatter",
        "isSameYear",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final dateRangeFormatRes(Lcom/squareup/customreport/data/RangeSelection;ZZ)I
    .locals 1

    const-string v0, "$this$dateRangeFormatRes"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_date_only:I

    goto/16 :goto_4

    .line 132
    :cond_1
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneWeek;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_1
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_date_range:I

    goto :goto_4

    .line 133
    :cond_3
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_2

    :cond_4
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneMonth;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :goto_2
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_date_only:I

    goto :goto_4

    .line 134
    :cond_5
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    goto :goto_3

    :cond_6
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomThreeMonths;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    :goto_3
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_date_range:I

    goto :goto_4

    .line 135
    :cond_7
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_date_only:I

    goto :goto_4

    .line 136
    :cond_8
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_c

    if-eqz p1, :cond_9

    if-eqz p2, :cond_9

    .line 138
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_date_only:I

    goto :goto_4

    :cond_9
    if-eqz p1, :cond_a

    if-nez p2, :cond_a

    .line 139
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_same_date_time_range:I

    goto :goto_4

    :cond_a
    if-nez p1, :cond_b

    if-eqz p2, :cond_b

    .line 140
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_date_range:I

    goto :goto_4

    .line 141
    :cond_b
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_date_time_range:I

    :goto_4
    return p0

    .line 137
    :cond_c
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method public static final endDateFormatter(Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/util/Res;)Lorg/threeten/bp/format/DateTimeFormatter;
    .locals 1

    const-string v0, "$this$endDateFormatter"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomThreeMonths;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    .line 115
    :cond_0
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_month_day_year:I

    goto :goto_1

    .line 113
    :cond_1
    :goto_0
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_month_year:I

    .line 111
    :goto_1
    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 110
    invoke-static {p0}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p0

    const-string p1, "DateTimeFormatter.ofPatt\u2026r\n          }\n      )\n  )"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getDefaultComparisonRange(Lcom/squareup/customreport/data/RangeSelection;)Lcom/squareup/customreport/data/ComparisonRange;
    .locals 1

    const-string v0, "$this$defaultComparisonRange"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-static {p0}, Lcom/squareup/salesreport/util/RangeSelectionsKt;->getPossibleComparisonRanges(Lcom/squareup/customreport/data/RangeSelection;)Ljava/util/List;

    move-result-object p0

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/customreport/data/ComparisonRange;

    return-object p0
.end method

.method public static final getDescription(Lcom/squareup/customreport/data/RangeSelection;)I
    .locals 1

    const-string v0, "$this$description"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_time_selection_1_day:I

    goto :goto_0

    .line 62
    :cond_0
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_time_selection_1_week:I

    goto :goto_0

    .line 63
    :cond_1
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_time_selection_1_month:I

    goto :goto_0

    .line 64
    :cond_2
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_time_selection_3_months:I

    goto :goto_0

    .line 65
    :cond_3
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_time_selection_1_year:I

    goto :goto_0

    .line 66
    :cond_4
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_time_selection_custom_1_day:I

    goto :goto_0

    .line 67
    :cond_5
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneWeek;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_time_selection_custom_1_week:I

    goto :goto_0

    .line 68
    :cond_6
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneMonth;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_time_selection_custom_1_month:I

    goto :goto_0

    .line 69
    :cond_7
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomThreeMonths;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_time_selection_custom_3_months:I

    goto :goto_0

    .line 70
    :cond_8
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    sget p0, Lcom/squareup/salesreport/impl/R$string;->empty:I

    :goto_0
    return p0

    :cond_9
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method public static final getPossibleComparisonRanges(Lcom/squareup/customreport/data/RangeSelection;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/RangeSelection;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/customreport/data/ComparisonRange;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$possibleComparisonRanges"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v0, :cond_0

    new-array p0, v1, [Lcom/squareup/customreport/data/ComparisonRange;

    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousWeek;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousWeek;

    check-cast v0, Lcom/squareup/customreport/data/ComparisonRange;

    aput-object v0, p0, v4

    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousYear;

    check-cast v0, Lcom/squareup/customreport/data/ComparisonRange;

    aput-object v0, p0, v3

    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$Yesterday;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$Yesterday;

    check-cast v0, Lcom/squareup/customreport/data/ComparisonRange;

    aput-object v0, p0, v2

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    goto/16 :goto_3

    .line 41
    :cond_0
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneWeek;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_0
    new-array p0, v2, [Lcom/squareup/customreport/data/ComparisonRange;

    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousWeek;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousWeek;

    check-cast v0, Lcom/squareup/customreport/data/ComparisonRange;

    aput-object v0, p0, v4

    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameWeekPreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameWeekPreviousYear;

    check-cast v0, Lcom/squareup/customreport/data/ComparisonRange;

    aput-object v0, p0, v3

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    goto/16 :goto_3

    .line 42
    :cond_2
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneMonth;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :goto_1
    new-array p0, v2, [Lcom/squareup/customreport/data/ComparisonRange;

    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousMonth;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousMonth;

    check-cast v0, Lcom/squareup/customreport/data/ComparisonRange;

    aput-object v0, p0, v4

    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameMonthPreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameMonthPreviousYear;

    check-cast v0, Lcom/squareup/customreport/data/ComparisonRange;

    aput-object v0, p0, v3

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    goto :goto_3

    .line 43
    :cond_4
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_2

    :cond_5
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomThreeMonths;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :goto_2
    new-array p0, v2, [Lcom/squareup/customreport/data/ComparisonRange;

    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousThreeMonths;

    check-cast v0, Lcom/squareup/customreport/data/ComparisonRange;

    aput-object v0, p0, v4

    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameThreeMonthsPreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameThreeMonthsPreviousYear;

    check-cast v0, Lcom/squareup/customreport/data/ComparisonRange;

    aput-object v0, p0, v3

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    goto :goto_3

    .line 44
    :cond_6
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object p0, Lcom/squareup/customreport/data/ComparisonRange$PreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousYear;

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    goto :goto_3

    .line 45
    :cond_7
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    new-array p0, v1, [Lcom/squareup/customreport/data/ComparisonRange;

    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousWeek;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousWeek;

    check-cast v0, Lcom/squareup/customreport/data/ComparisonRange;

    aput-object v0, p0, v4

    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousYear;

    check-cast v0, Lcom/squareup/customreport/data/ComparisonRange;

    aput-object v0, p0, v3

    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousDay;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousDay;

    check-cast v0, Lcom/squareup/customreport/data/ComparisonRange;

    aput-object v0, p0, v2

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    goto :goto_3

    .line 46
    :cond_8
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    sget-object p0, Lcom/squareup/customreport/data/ComparisonRange$NoComparison;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$NoComparison;

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    :goto_3
    return-object p0

    :cond_9
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method public static final startDateFormatter(Lcom/squareup/customreport/data/RangeSelection;ZLcom/squareup/util/Res;Ljavax/inject/Provider;)Lorg/threeten/bp/format/DateTimeFormatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/RangeSelection;",
            "Z",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lorg/threeten/bp/format/DateTimeFormatter;"
        }
    .end annotation

    const-string v0, "$this$startDateFormatter"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_month_day_year:I

    goto :goto_4

    .line 87
    :cond_1
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneWeek;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :goto_1
    if-eqz p1, :cond_3

    .line 88
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_month_day:I

    goto :goto_4

    .line 90
    :cond_3
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_month_day_year:I

    goto :goto_4

    .line 92
    :cond_4
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_2

    :cond_5
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneMonth;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :goto_2
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_month_year:I

    goto :goto_4

    .line 93
    :cond_6
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    goto :goto_3

    :cond_7
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomThreeMonths;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :goto_3
    if-eqz p1, :cond_8

    .line 94
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_month_only:I

    goto :goto_4

    .line 96
    :cond_8
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_month_year:I

    goto :goto_4

    .line 98
    :cond_9
    sget-object p1, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_a

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_year_only:I

    goto :goto_4

    .line 99
    :cond_a
    sget-object p1, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_b

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_month_day_year:I

    .line 84
    :goto_4
    invoke-interface {p2, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 83
    invoke-static {p0}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p0

    .line 103
    invoke-interface {p3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Locale;

    invoke-virtual {p0, p1}, Lorg/threeten/bp/format/DateTimeFormatter;->withLocale(Ljava/util/Locale;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p0

    const-string p1, "DateTimeFormatter\n      \u2026ale(localeProvider.get())"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0

    .line 99
    :cond_b
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
