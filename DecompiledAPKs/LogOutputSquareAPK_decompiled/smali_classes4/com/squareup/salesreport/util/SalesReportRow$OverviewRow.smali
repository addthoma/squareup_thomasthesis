.class public final Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;
.super Lcom/squareup/salesreport/util/SalesReportRow;
.source "SalesReportRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/util/SalesReportRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OverviewRow"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0015\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001 B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003JE\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000b\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;",
        "Lcom/squareup/salesreport/util/SalesReportRow;",
        "grossSales",
        "Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;",
        "transactionCount",
        "averageSale",
        "netSales",
        "refunds",
        "discount",
        "(Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;)V",
        "getAverageSale",
        "()Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;",
        "getDiscount",
        "getGrossSales",
        "getNetSales",
        "getRefunds",
        "getTransactionCount",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "OverviewItem",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final averageSale:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

.field private final discount:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

.field private final grossSales:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

.field private final netSales:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

.field private final refunds:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

.field private final transactionCount:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;


# direct methods
.method public constructor <init>(Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;)V
    .locals 1

    const-string v0, "grossSales"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionCount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "averageSale"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "netSales"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refunds"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discount"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 34
    invoke-direct {p0, v0}, Lcom/squareup/salesreport/util/SalesReportRow;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->grossSales:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    iput-object p2, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->transactionCount:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    iput-object p3, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->averageSale:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    iput-object p4, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->netSales:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    iput-object p5, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->refunds:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    iput-object p6, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->discount:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;ILjava/lang/Object;)Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->grossSales:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->transactionCount:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->averageSale:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->netSales:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->refunds:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->discount:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->copy(Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;)Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->grossSales:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    return-object v0
.end method

.method public final component2()Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->transactionCount:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    return-object v0
.end method

.method public final component3()Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->averageSale:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    return-object v0
.end method

.method public final component4()Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->netSales:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    return-object v0
.end method

.method public final component5()Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->refunds:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    return-object v0
.end method

.method public final component6()Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->discount:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    return-object v0
.end method

.method public final copy(Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;)Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;
    .locals 8

    const-string v0, "grossSales"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionCount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "averageSale"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "netSales"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refunds"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discount"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;-><init>(Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->grossSales:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    iget-object v1, p1, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->grossSales:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->transactionCount:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    iget-object v1, p1, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->transactionCount:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->averageSale:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    iget-object v1, p1, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->averageSale:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->netSales:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    iget-object v1, p1, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->netSales:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->refunds:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    iget-object v1, p1, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->refunds:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->discount:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    iget-object p1, p1, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->discount:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAverageSale()Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->averageSale:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    return-object v0
.end method

.method public final getDiscount()Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->discount:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    return-object v0
.end method

.method public final getGrossSales()Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->grossSales:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    return-object v0
.end method

.method public final getNetSales()Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->netSales:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    return-object v0
.end method

.method public final getRefunds()Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->refunds:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    return-object v0
.end method

.method public final getTransactionCount()Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->transactionCount:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->grossSales:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->transactionCount:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->averageSale:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->netSales:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->refunds:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->discount:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OverviewRow(grossSales="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->grossSales:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", transactionCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->transactionCount:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", averageSale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->averageSale:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", netSales="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->netSales:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", refunds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->refunds:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", discount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;->discount:Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow$OverviewItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
