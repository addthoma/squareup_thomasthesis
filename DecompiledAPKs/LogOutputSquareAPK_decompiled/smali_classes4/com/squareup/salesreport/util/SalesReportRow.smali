.class public abstract Lcom/squareup/salesreport/util/SalesReportRow;
.super Ljava/lang/Object;
.source "SalesReportRow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderRow;,
        Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithButtonRow;,
        Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithImageButtonRow;,
        Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;,
        Lcom/squareup/salesreport/util/SalesReportRow$SubsectionHeaderRow;,
        Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;,
        Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodRow;,
        Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;,
        Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodTotalRow;,
        Lcom/squareup/salesreport/util/SalesReportRow$FeesRow;,
        Lcom/squareup/salesreport/util/SalesReportRow$TwoTabToggleRow;,
        Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u000c\u0003\u0004\u0005\u0006\u0007\u0008\t\n\u000b\u000c\r\u000eB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u000c\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/salesreport/util/SalesReportRow;",
        "",
        "()V",
        "FeesRow",
        "OverviewRow",
        "PaymentMethodRow",
        "PaymentMethodTotalRow",
        "SalesChartRow",
        "SalesDetailsRow",
        "SectionHeaderRow",
        "SectionHeaderWithButtonRow",
        "SectionHeaderWithImageButtonRow",
        "SubsectionHeaderRow",
        "TruncatedPaymentMethodRow",
        "TwoTabToggleRow",
        "Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderRow;",
        "Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithButtonRow;",
        "Lcom/squareup/salesreport/util/SalesReportRow$SectionHeaderWithImageButtonRow;",
        "Lcom/squareup/salesreport/util/SalesReportRow$OverviewRow;",
        "Lcom/squareup/salesreport/util/SalesReportRow$SubsectionHeaderRow;",
        "Lcom/squareup/salesreport/util/SalesReportRow$SalesDetailsRow;",
        "Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodRow;",
        "Lcom/squareup/salesreport/util/SalesReportRow$TruncatedPaymentMethodRow;",
        "Lcom/squareup/salesreport/util/SalesReportRow$PaymentMethodTotalRow;",
        "Lcom/squareup/salesreport/util/SalesReportRow$FeesRow;",
        "Lcom/squareup/salesreport/util/SalesReportRow$TwoTabToggleRow;",
        "Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/squareup/salesreport/util/SalesReportRow;-><init>()V

    return-void
.end method
