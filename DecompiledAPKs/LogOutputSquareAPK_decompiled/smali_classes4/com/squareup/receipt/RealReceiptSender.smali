.class public Lcom/squareup/receipt/RealReceiptSender;
.super Ljava/lang/Object;
.source "RealReceiptSender.java"

# interfaces
.implements Lcom/squareup/receipt/ReceiptSender;


# instance fields
.field private final analytics:Lcom/squareup/receipt/ReceiptAnalytics;

.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private final paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

.field private final phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final receiptValidator:Lcom/squareup/receipt/ReceiptValidator;


# direct methods
.method public constructor <init>(Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/receipt/ReceiptAnalytics;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/receipt/ReceiptValidator;Lcom/squareup/print/PrinterStations;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/receipt/RealReceiptSender;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    .line 39
    iput-object p2, p0, Lcom/squareup/receipt/RealReceiptSender;->analytics:Lcom/squareup/receipt/ReceiptAnalytics;

    .line 40
    iput-object p3, p0, Lcom/squareup/receipt/RealReceiptSender;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    .line 41
    iput-object p4, p0, Lcom/squareup/receipt/RealReceiptSender;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    .line 42
    iput-object p5, p0, Lcom/squareup/receipt/RealReceiptSender;->receiptValidator:Lcom/squareup/receipt/ReceiptValidator;

    .line 43
    iput-object p6, p0, Lcom/squareup/receipt/RealReceiptSender;->printerStations:Lcom/squareup/print/PrinterStations;

    return-void
.end method

.method private isValidSmsNumber(Ljava/lang/String;)Z
    .locals 1

    if-eqz p1, :cond_0

    .line 200
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    invoke-interface {v0, p1}, Lcom/squareup/text/PhoneNumberHelper;->isValid(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method static synthetic lambda$onMaybeAutoPrintReceipt$2(Ljava/lang/Throwable;)Ljava/lang/Boolean;
    .locals 0

    const/4 p0, 0x0

    .line 79
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onMaybePrintReceipt$5(Ljava/lang/Throwable;)Ljava/lang/Boolean;
    .locals 0

    const/4 p0, 0x0

    .line 147
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public autoPrintReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Ljava/util/Collection;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/PaymentReceipt;",
            "Lcom/squareup/payment/tender/BaseTender;",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;)V"
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v0}, Lcom/squareup/papersignature/PaperSignatureSettings;->isSignOnPrintedReceiptEnabled()Z

    move-result v0

    .line 85
    iget-object v1, p0, Lcom/squareup/receipt/RealReceiptSender;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v1}, Lcom/squareup/papersignature/PaperSignatureSettings;->isPrintAdditionalAuthSlipEnabled()Z

    move-result v6

    .line 86
    sget-object v1, Lcom/squareup/receipt/PrintedReceiptSettings;->INSTANCE:Lcom/squareup/receipt/PrintedReceiptSettings;

    .line 87
    invoke-virtual {v1, p2}, Lcom/squareup/receipt/PrintedReceiptSettings;->tenderRequiresPrintedReceipt(Lcom/squareup/payment/tender/BaseTender;)Z

    move-result v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 93
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    const/4 v4, 0x0

    move-object v1, p1

    move-object v2, p2

    move v3, v6

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/print/OrderPrintingDispatcher;->printAuthSlip(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;ZZLjava/util/Collection;)V

    if-eqz v6, :cond_0

    .line 97
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    const/4 v3, 0x1

    const/4 v4, 0x1

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/print/OrderPrintingDispatcher;->printAuthSlip(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;ZZLjava/util/Collection;)V

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/print/OrderPrintingDispatcher;->autoPrintItemizedReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Ljava/util/Collection;)V

    return-void
.end method

.method public autoPrintValidatedReceipt(Lcom/squareup/payment/PaymentReceipt;)V
    .locals 3

    .line 51
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->canAutoPrintReceipt()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender;->receiptValidator:Lcom/squareup/receipt/ReceiptValidator;

    sget-object v1, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    new-instance v2, Lcom/squareup/receipt/-$$Lambda$RealReceiptSender$zW-lVe8my3ucO-9kvyFpDHFKOKo;

    invoke-direct {v2, p0}, Lcom/squareup/receipt/-$$Lambda$RealReceiptSender$zW-lVe8my3ucO-9kvyFpDHFKOKo;-><init>(Lcom/squareup/receipt/RealReceiptSender;)V

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receipt/ReceiptValidator;->waitForReceiptNumber(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/receipt/ReceiptValidator$ReceiptNumberReadyCallback;)V

    return-void
.end method

.method public isItemizedReceiptsEnabled()Z
    .locals 2

    .line 196
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender;->printerStations:Lcom/squareup/print/PrinterStations;

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->getEnabledStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public synthetic lambda$autoPrintValidatedReceipt$0$RealReceiptSender(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V
    .locals 1

    .line 57
    iget-object p3, p0, Lcom/squareup/receipt/RealReceiptSender;->printerStations:Lcom/squareup/print/PrinterStations;

    sget-object v0, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    .line 58
    invoke-interface {p3, v0}, Lcom/squareup/print/PrinterStations;->getEnabledStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;

    move-result-object p3

    .line 59
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/receipt/RealReceiptSender;->autoPrintReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Ljava/util/Collection;)V

    return-void
.end method

.method public synthetic lambda$onMaybeAutoPrintReceipt$1$RealReceiptSender(Lcom/squareup/payment/PaymentReceipt;)Ljava/lang/Boolean;
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender;->printerStations:Lcom/squareup/print/PrinterStations;

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    .line 75
    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->getEnabledStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;

    move-result-object v0

    .line 76
    invoke-static {p1}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->getTenderForPrinting(Lcom/squareup/payment/PaymentReceipt;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object v1

    invoke-virtual {p0, p1, v1, v0}, Lcom/squareup/receipt/RealReceiptSender;->autoPrintReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Ljava/util/Collection;)V

    const/4 p1, 0x1

    .line 77
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onMaybePrintReceipt$4$RealReceiptSender(Lcom/squareup/print/payload/ReceiptPayload$RenderType;Ljava/util/Collection;Lcom/squareup/payment/PaymentReceipt;)Ljava/lang/Boolean;
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    invoke-static {p3}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->getTenderForPrinting(Lcom/squareup/payment/PaymentReceipt;)Lcom/squareup/payment/tender/BaseTender;

    move-result-object v1

    invoke-virtual {v0, p3, v1, p1, p2}, Lcom/squareup/print/OrderPrintingDispatcher;->printItemizedReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Ljava/util/Collection;)V

    const/4 p1, 0x1

    .line 145
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$printValidatedReceipt$3$RealReceiptSender(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V
    .locals 2

    .line 113
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender;->printerStations:Lcom/squareup/print/PrinterStations;

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    .line 114
    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->getEnabledStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;

    move-result-object v0

    .line 115
    iget-object v1, p0, Lcom/squareup/receipt/RealReceiptSender;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    invoke-virtual {v1, p1, p2, p3, v0}, Lcom/squareup/print/OrderPrintingDispatcher;->printItemizedReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Ljava/util/Collection;)V

    return-void
.end method

.method public onMaybeAutoPrintReceipt(Lcom/squareup/payment/PaymentReceipt;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/PaymentReceipt;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 69
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->canAutoPrintReceipt()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    .line 70
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender;->receiptValidator:Lcom/squareup/receipt/ReceiptValidator;

    invoke-virtual {v0, p1}, Lcom/squareup/receipt/ReceiptValidator;->waitForReceiptNumber(Lcom/squareup/payment/PaymentReceipt;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/receipt/-$$Lambda$RealReceiptSender$bIvmZxdk-NDV-BVD37W2aRMEmvU;

    invoke-direct {v0, p0}, Lcom/squareup/receipt/-$$Lambda$RealReceiptSender$bIvmZxdk-NDV-BVD37W2aRMEmvU;-><init>(Lcom/squareup/receipt/RealReceiptSender;)V

    .line 73
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/receipt/-$$Lambda$RealReceiptSender$2B_b_XNqfFEK-HtulU7BJZ4EBP8;->INSTANCE:Lcom/squareup/receipt/-$$Lambda$RealReceiptSender$2B_b_XNqfFEK-HtulU7BJZ4EBP8;

    .line 79
    invoke-virtual {p1, v0}, Lrx/Observable;->onErrorReturn(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public onMaybePrintReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/PaymentReceipt;",
            "Lcom/squareup/print/payload/ReceiptPayload$RenderType;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender;->printerStations:Lcom/squareup/print/PrinterStations;

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    .line 129
    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->getEnabledStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;

    move-result-object v0

    .line 130
    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/receipt/RealReceiptSender;->onMaybePrintReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Ljava/util/Collection;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public onMaybePrintReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Ljava/util/Collection;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/PaymentReceipt;",
            "Lcom/squareup/print/payload/ReceiptPayload$RenderType;",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;)",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 141
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender;->receiptValidator:Lcom/squareup/receipt/ReceiptValidator;

    invoke-virtual {v0, p1}, Lcom/squareup/receipt/ReceiptValidator;->waitForReceiptNumber(Lcom/squareup/payment/PaymentReceipt;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/receipt/-$$Lambda$RealReceiptSender$kzha_a3dLuRh-2eKJzw6gAHzcTw;

    invoke-direct {v0, p0, p2, p3}, Lcom/squareup/receipt/-$$Lambda$RealReceiptSender$kzha_a3dLuRh-2eKJzw6gAHzcTw;-><init>(Lcom/squareup/receipt/RealReceiptSender;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Ljava/util/Collection;)V

    .line 142
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    sget-object p2, Lcom/squareup/receipt/-$$Lambda$RealReceiptSender$-0uJoGsUuirBtMvABQXasClu7SI;->INSTANCE:Lcom/squareup/receipt/-$$Lambda$RealReceiptSender$-0uJoGsUuirBtMvABQXasClu7SI;

    .line 147
    invoke-virtual {p1, p2}, Lrx/Observable;->onErrorReturn(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public printValidatedReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender;->receiptValidator:Lcom/squareup/receipt/ReceiptValidator;

    new-instance v1, Lcom/squareup/receipt/-$$Lambda$RealReceiptSender$C8tMo6K9JgtfQGQervE4VHRchNU;

    invoke-direct {v1, p0}, Lcom/squareup/receipt/-$$Lambda$RealReceiptSender$C8tMo6K9JgtfQGQervE4VHRchNU;-><init>(Lcom/squareup/receipt/RealReceiptSender;)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/squareup/receipt/ReceiptValidator;->waitForReceiptNumber(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/receipt/ReceiptValidator$ReceiptNumberReadyCallback;)V

    return-void
.end method

.method public receiptDeclined(Lcom/squareup/payment/PaymentReceipt;)V
    .locals 0

    .line 187
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->decline()V

    .line 188
    iget-object p1, p0, Lcom/squareup/receipt/RealReceiptSender;->analytics:Lcom/squareup/receipt/ReceiptAnalytics;

    invoke-virtual {p1}, Lcom/squareup/receipt/ReceiptAnalytics;->logNone()V

    return-void
.end method

.method public sendEmailReceiptToDefault(Lcom/squareup/payment/PaymentReceipt;)V
    .locals 3

    .line 163
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender;->analytics:Lcom/squareup/receipt/ReceiptAnalytics;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/receipt/ReceiptAnalytics;->logEmail(ZZ)V

    .line 164
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->emailDefault()V

    return-void
.end method

.method public sendSmsReceiptToDefault(Lcom/squareup/payment/PaymentReceipt;)V
    .locals 3

    .line 181
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender;->analytics:Lcom/squareup/receipt/ReceiptAnalytics;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/receipt/ReceiptAnalytics;->logSms(ZZ)V

    .line 182
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->smsDefault()V

    return-void
.end method

.method public trySendEmailReceipt(Lcom/squareup/payment/PaymentReceipt;Ljava/lang/String;)Z
    .locals 2

    .line 154
    invoke-static {p2}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender;->analytics:Lcom/squareup/receipt/ReceiptAnalytics;

    invoke-virtual {v0, v1, v1}, Lcom/squareup/receipt/ReceiptAnalytics;->logEmail(ZZ)V

    .line 156
    invoke-virtual {p1, p2}, Lcom/squareup/payment/PaymentReceipt;->email(Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    return v1
.end method

.method public trySendSmsReceipt(Lcom/squareup/payment/PaymentReceipt;Ljava/lang/String;)Z
    .locals 2

    .line 172
    invoke-direct {p0, p2}, Lcom/squareup/receipt/RealReceiptSender;->isValidSmsNumber(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/squareup/receipt/RealReceiptSender;->analytics:Lcom/squareup/receipt/ReceiptAnalytics;

    invoke-virtual {v0, v1, v1}, Lcom/squareup/receipt/ReceiptAnalytics;->logSms(ZZ)V

    .line 174
    invoke-virtual {p1, p2}, Lcom/squareup/payment/PaymentReceipt;->sms(Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    return v1
.end method
