.class Lcom/squareup/receipt/ReceiptAnalytics$ReceiptEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "ReceiptAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/receipt/ReceiptAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ReceiptEvent"
.end annotation


# instance fields
.field delivery:Ljava/lang/String;

.field isDefault:Z

.field resend:Z


# direct methods
.method constructor <init>(Ljava/lang/String;ZZ)V
    .locals 1

    .line 52
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->SEND_RECEIPT:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 53
    iput-object p1, p0, Lcom/squareup/receipt/ReceiptAnalytics$ReceiptEvent;->delivery:Ljava/lang/String;

    .line 54
    iput-boolean p2, p0, Lcom/squareup/receipt/ReceiptAnalytics$ReceiptEvent;->resend:Z

    .line 55
    iput-boolean p3, p0, Lcom/squareup/receipt/ReceiptAnalytics$ReceiptEvent;->isDefault:Z

    return-void
.end method
