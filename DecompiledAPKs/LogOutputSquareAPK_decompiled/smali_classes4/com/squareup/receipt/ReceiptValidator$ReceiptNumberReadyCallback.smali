.class public interface abstract Lcom/squareup/receipt/ReceiptValidator$ReceiptNumberReadyCallback;
.super Ljava/lang/Object;
.source "ReceiptValidator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/receipt/ReceiptValidator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ReceiptNumberReadyCallback"
.end annotation


# virtual methods
.method public abstract onReceiptNumberReady(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V
.end method
