.class public Lcom/squareup/print/PrinterScoutsProvider$NoPrinterScouts;
.super Ljava/lang/Object;
.source "PrinterScoutsProvider.java"

# interfaces
.implements Lcom/squareup/print/PrinterScoutsProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/PrinterScoutsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NoPrinterScouts"
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/print/PrinterScoutsProvider$NoPrinterScouts;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    new-instance v0, Lcom/squareup/print/PrinterScoutsProvider$NoPrinterScouts;

    invoke-direct {v0}, Lcom/squareup/print/PrinterScoutsProvider$NoPrinterScouts;-><init>()V

    sput-object v0, Lcom/squareup/print/PrinterScoutsProvider$NoPrinterScouts;->INSTANCE:Lcom/squareup/print/PrinterScoutsProvider$NoPrinterScouts;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public availableScouts()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/print/PrinterScout;",
            ">;"
        }
    .end annotation

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
