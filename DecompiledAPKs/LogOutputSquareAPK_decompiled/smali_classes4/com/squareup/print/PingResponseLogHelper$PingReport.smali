.class public Lcom/squareup/print/PingResponseLogHelper$PingReport;
.super Ljava/lang/Object;
.source "PingResponseLogHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/PingResponseLogHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PingReport"
.end annotation


# instance fields
.field private final currentTimeMS:Ljava/lang/Long;

.field final synthetic this$0:Lcom/squareup/print/PingResponseLogHelper;

.field private final timeDelta:Ljava/lang/Long;


# direct methods
.method constructor <init>(Lcom/squareup/print/PingResponseLogHelper;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/print/PingResponseLogHelper$PingReport;->this$0:Lcom/squareup/print/PingResponseLogHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput-object p2, p0, Lcom/squareup/print/PingResponseLogHelper$PingReport;->currentTimeMS:Ljava/lang/Long;

    .line 130
    iput-object p3, p0, Lcom/squareup/print/PingResponseLogHelper$PingReport;->timeDelta:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 6

    .line 136
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/print/PingResponseLogHelper$PingReport;->this$0:Lcom/squareup/print/PingResponseLogHelper;

    .line 137
    invoke-static {v2}, Lcom/squareup/print/PingResponseLogHelper;->access$100(Lcom/squareup/print/PingResponseLogHelper;)Ljava/text/SimpleDateFormat;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    iget-object v4, p0, Lcom/squareup/print/PingResponseLogHelper$PingReport;->currentTimeMS:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/print/PingResponseLogHelper$PingReport;->timeDelta:Ljava/lang/Long;

    if-nez v2, :cond_0

    const-string v2, "null"

    :cond_0
    const/4 v3, 0x1

    aput-object v2, v1, v3

    const-string v2, "[\"%s\", %s]"

    .line 136
    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
