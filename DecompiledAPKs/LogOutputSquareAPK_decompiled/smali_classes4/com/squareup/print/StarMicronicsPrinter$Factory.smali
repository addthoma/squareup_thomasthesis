.class public Lcom/squareup/print/StarMicronicsPrinter$Factory;
.super Ljava/lang/Object;
.source "StarMicronicsPrinter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/StarMicronicsPrinter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# instance fields
.field clock:Lcom/squareup/util/Clock;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 426
    iput-object p1, p0, Lcom/squareup/print/StarMicronicsPrinter$Factory;->clock:Lcom/squareup/util/Clock;

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/ConnectionType;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lcom/squareup/print/StarMicronicsPrinter;
    .locals 9

    .line 432
    new-instance v8, Lcom/squareup/print/StarMicronicsPrinter;

    iget-object v7, p0, Lcom/squareup/print/StarMicronicsPrinter$Factory;->clock:Lcom/squareup/util/Clock;

    move-object v0, v8

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/print/StarMicronicsPrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/ConnectionType;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/squareup/util/Clock;)V

    return-object v8
.end method
