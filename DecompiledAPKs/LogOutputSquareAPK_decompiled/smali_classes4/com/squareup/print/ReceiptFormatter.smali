.class public Lcom/squareup/print/ReceiptFormatter;
.super Ljava/lang/Object;
.source "ReceiptFormatter.java"


# static fields
.field private static final TICKET_ITEM_QUANTITY_UNIT:Ljava/lang/String; = " \u00d7"

.field public static final UNTAXED_ITEM_ANNOTATION:Ljava/lang/String; = "*"


# instance fields
.field private final countryCode:Lcom/squareup/CountryCode;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final locale:Ljava/util/Locale;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

.field private final res:Lcom/squareup/util/Res;

.field private final taxFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/Features;Lcom/squareup/CountryCode;)V
    .locals 1
    .param p4    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/ForTaxPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/CountryCode;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    .line 75
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 76
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getPerUnitFormatter()Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 77
    invoke-virtual {p1}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocale()Ljava/util/Locale;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/ReceiptFormatter;->locale:Ljava/util/Locale;

    .line 78
    iput-object p2, p0, Lcom/squareup/print/ReceiptFormatter;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    .line 79
    iput-object p3, p0, Lcom/squareup/print/ReceiptFormatter;->percentageFormatter:Lcom/squareup/text/Formatter;

    .line 80
    iput-object p4, p0, Lcom/squareup/print/ReceiptFormatter;->taxFormatter:Lcom/squareup/text/Formatter;

    .line 81
    iput-object p5, p0, Lcom/squareup/print/ReceiptFormatter;->features:Lcom/squareup/settings/server/Features;

    .line 82
    iput-object p6, p0, Lcom/squareup/print/ReceiptFormatter;->countryCode:Lcom/squareup/CountryCode;

    return-void
.end method

.method private applicationIdWithSpaces(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 226
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    .line 227
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 228
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 229
    rem-int/lit8 v2, v1, 0x2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const/16 v2, 0xa0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 231
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private formatMoneyOrNull(ILcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 0

    if-eqz p2, :cond_0

    .line 359
    invoke-virtual {p0, p1, p2}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(ILcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private formatMoneyOrNullIfZero(ILcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 5

    if-eqz p2, :cond_0

    .line 347
    iget-object v0, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    .line 348
    invoke-virtual {p0, p1, p2}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(ILcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private formatMoneyOrZeroIfNull(ILcom/squareup/protos/common/Money;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 2

    if-nez p2, :cond_0

    const-wide/16 v0, 0x0

    .line 337
    invoke-static {v0, v1, p3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 340
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(ILcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    return-object p1
.end method

.method private getInclusiveTaxTitle(Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;Ljava/util/List;Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 473
    sget-object v0, Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;->SIMPLE:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    const-string v1, "percentage"

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne p1, v0, :cond_1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_1

    .line 474
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p1

    if-ne p1, v3, :cond_0

    .line 475
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/payment/OrderAdjustment;

    .line 476
    iget-object p2, p0, Lcom/squareup/print/ReceiptFormatter;->taxFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/payment/OrderAdjustment;->decimalPercentage:Lcom/squareup/util/Percentage;

    .line 477
    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 478
    iget-object p2, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/print/R$string;->buyer_printed_receipt_taxes_included_items_format:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 479
    invoke-virtual {p2, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 480
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 481
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 483
    :cond_0
    iget-object p1, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/print/R$string;->buyer_printed_receipt_taxes_included_items:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 486
    :cond_1
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p1

    if-ne p1, v3, :cond_2

    .line 487
    invoke-interface {p3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/payment/OrderAdjustment;

    iget-object p1, p1, Lcom/squareup/payment/OrderAdjustment;->decimalPercentage:Lcom/squareup/util/Percentage;

    .line 488
    iget-object p2, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/print/R$string;->buyer_printed_receipt_taxes_included_total_format:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/print/ReceiptFormatter;->taxFormatter:Lcom/squareup/text/Formatter;

    .line 489
    invoke-interface {p3, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p2, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 490
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 491
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 493
    :cond_2
    iget-object p1, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/print/R$string;->buyer_printed_receipt_taxes_included_total:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public additionsLabelAndUnitPrice(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 572
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->buyer_printed_receipt_additions_label_and_price:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/print/ReceiptFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 574
    invoke-virtual {v1, p1}, Lcom/squareup/quantity/PerUnitFormatter;->money(Lcom/squareup/protos/common/Money;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 575
    invoke-virtual {p1, p2}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 576
    invoke-virtual {p1}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 577
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "price"

    .line 573
    invoke-virtual {v0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 578
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 579
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public addressLines(Lcom/squareup/settings/server/UserSettings;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/UserSettings;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 133
    invoke-virtual {p1}, Lcom/squareup/settings/server/UserSettings;->getReceiptAddressWithUserCountryCode()Lcom/squareup/server/account/protos/User$ReceiptAddress;

    move-result-object p1

    .line 132
    invoke-static {p1}, Lcom/squareup/address/AddressFormatter;->formatForShippingDisplay(Lcom/squareup/server/account/protos/User$ReceiptAddress;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public amountToTender(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 255
    sget v0, Lcom/squareup/print/R$string;->buyer_printed_receipt_tender_amount:I

    invoke-direct {p0, v0, p1}, Lcom/squareup/print/ReceiptFormatter;->formatMoneyOrNull(ILcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    return-object p1
.end method

.method public applicationIdOrNull(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 214
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->buyer_printed_receipt_aid:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 219
    invoke-direct {p0, p1}, Lcom/squareup/print/ReceiptFormatter;->applicationIdWithSpaces(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "aid"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 220
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 221
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public authorizationCodeOrNull(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 277
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "0"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->buyer_printed_receipt_authorization_number:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "authorization_number"

    .line 282
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 283
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 284
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public businessAbn(Lcom/squareup/settings/server/UserSettings;)Ljava/lang/String;
    .locals 4

    .line 142
    invoke-virtual {p1}, Lcom/squareup/settings/server/UserSettings;->getBusinessAbn()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 145
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xb

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 150
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x5

    .line 151
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x8

    .line 152
    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ABN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const-string p1, ""

    return-object p1
.end method

.method public calculatedOption(Lcom/squareup/protos/common/Money;)Ljava/lang/String;
    .locals 2

    .line 648
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->paper_signature_quick_tip_calculated_total_pattern:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 649
    invoke-virtual {p0, p1}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "total"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 650
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 651
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public calculatedOption(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Ljava/lang/String;
    .locals 2

    .line 640
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->paper_signature_quick_tip_calculated_amount_and_total_pattern:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 641
    invoke-virtual {p0, p1}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "tip"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 642
    invoke-virtual {p0, p2}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object p2

    const-string v0, "total"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 643
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 644
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public cardholderVerificationMethodUsedOrNull(Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 240
    :cond_0
    sget-object v1, Lcom/squareup/print/ReceiptFormatter$1;->$SwitchMap$com$squareup$protos$client$bills$CardTender$Emv$CardholderVerificationMethod:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_2

    const/4 v1, 0x3

    if-eq p1, v1, :cond_1

    return-object v0

    .line 245
    :cond_1
    iget-object p1, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/print/R$string;->buyer_printed_receipt_verification_method_on_device:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 243
    :cond_2
    iget-object p1, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/print/R$string;->buyer_printed_receipt_verification_method_pin:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public changeOrZero(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 267
    sget v0, Lcom/squareup/print/R$string;->buyer_printed_receipt_tender_change:I

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/print/ReceiptFormatter;->formatMoneyOrZeroIfNull(ILcom/squareup/protos/common/Money;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    return-object p1
.end method

.method public comp(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 323
    sget v0, Lcom/squareup/print/R$string;->buyer_printed_receipt_comp:I

    invoke-virtual {p0, v0, p1}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(ILcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    return-object p1
.end method

.method public compLabel()Ljava/lang/String;
    .locals 2

    .line 539
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->comp_initial:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public deviceNameOrNull(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 516
    invoke-static {p1}, Lcom/squareup/util/Strings;->nullIfBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public discount(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 319
    sget v0, Lcom/squareup/print/R$string;->buyer_printed_receipt_discount:I

    invoke-virtual {p0, v0, p1}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(ILcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    return-object p1
.end method

.method public employee(Lcom/squareup/permissions/Employee;)Ljava/lang/String;
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 167
    invoke-virtual {p1}, Lcom/squareup/permissions/Employee;->first_name()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/squareup/print/util/PrintRendererUtils;->removeBlanks([Ljava/lang/CharSequence;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object p1

    const-string v0, " "

    invoke-static {p1, v0}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public formalReceiptTotal(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 263
    sget v0, Lcom/squareup/print/R$string;->jp_formal_receipt_total:I

    invoke-virtual {p0, v0, p1}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(ILcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    return-object p1
.end method

.method public formatAdditiveTax(Lcom/squareup/payment/OrderAdjustment;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 4

    .line 378
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->tax_name:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 379
    iget-object v1, p0, Lcom/squareup/print/ReceiptFormatter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->PRINT_TAX_PERCENTAGE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 380
    iget-object v1, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/print/R$string;->tax_breakdown_table_vat_rate_name_and_percentage:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/payment/OrderAdjustment;->name:Ljava/lang/String;

    const-string v3, "name"

    .line 381
    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/print/ReceiptFormatter;->taxFormatter:Lcom/squareup/text/Formatter;

    .line 382
    invoke-virtual {p1, v2}, Lcom/squareup/payment/OrderAdjustment;->getPercentage(Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "percentage"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 383
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 384
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 385
    iget-object v2, p1, Lcom/squareup/payment/OrderAdjustment;->name:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v1

    goto :goto_0

    .line 389
    :cond_1
    iget-object v1, p1, Lcom/squareup/payment/OrderAdjustment;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lcom/squareup/payment/OrderAdjustment;->name:Ljava/lang/String;

    .line 394
    :goto_0
    iget-object p1, p1, Lcom/squareup/payment/OrderAdjustment;->appliedMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, v0, p1}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    return-object p1
.end method

.method public formatBaseAmountInputs(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 413
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 414
    invoke-virtual {v0, p1}, Lcom/squareup/quantity/PerUnitFormatter;->money(Lcom/squareup/protos/common/Money;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p2}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 415
    invoke-virtual {p1, v0, p4}, Lcom/squareup/quantity/PerUnitFormatter;->quantityAndPrecision(Ljava/math/BigDecimal;I)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 416
    invoke-virtual {p1, p3}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 417
    invoke-virtual {p1}, Lcom/squareup/quantity/PerUnitFormatter;->inParentheses()Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 418
    invoke-virtual {p1}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 420
    invoke-static {p5}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public formatDiscount(Lcom/squareup/util/Percentage;)Ljava/lang/String;
    .locals 2

    .line 300
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->discount_receipt_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/print/ReceiptFormatter;->percentageFormatter:Lcom/squareup/text/Formatter;

    .line 301
    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v1, "amount"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 302
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 303
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public formatMoney(ILcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 365
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    return-object p1
.end method

.method public formatMoney(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 369
    new-instance v0, Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {p0, p2}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Lcom/squareup/print/payload/LabelAmountPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public formatMoney(Lcom/squareup/protos/common/Money;)Ljava/lang/String;
    .locals 1

    .line 373
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public formatSurcharge(Lcom/squareup/checkout/Surcharge;Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 3

    .line 398
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/print/ReceiptFormatter;->countryCode:Lcom/squareup/CountryCode;

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2, v1}, Lcom/squareup/checkout/Surcharge;->formattedName(Lcom/squareup/util/Res;ZLcom/squareup/CountryCode;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    return-object p1
.end method

.method public getApprovedDispositionText()Ljava/lang/String;
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->buyer_printed_receipt_disposition_approved_uppercase:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDateDetailString(Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->locale:Ljava/util/Locale;

    const/4 v1, 0x1

    invoke-static {v1, v0}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getEMoneyReprintReceiptLabel()Ljava/lang/String;
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->receipt_felica_reprint:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFormalReceiptItemHeader()Ljava/lang/String;
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->jp_formal_receipt_item_header:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFormalReceiptItemReceivedConfirmation()Ljava/lang/String;
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->jp_formal_receipt_received_confirmation:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFormalReceiptName()Ljava/lang/String;
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->jp_formal_receipt_name:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFormalReceiptTitle()Ljava/lang/String;
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->jp_formal_receipt_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPurchaseTransactionTypeHeader()Ljava/lang/String;
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->receipt_header_purchase_transaction_type:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRefundTransactionTypeHeader()Ljava/lang/String;
    .locals 2

    .line 110
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->receipt_header_refund_transaction_type:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTimeDetailStringWithSeconds(Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->locale:Ljava/util/Locale;

    const/4 v1, 0x2

    invoke-static {v1, v0}, Ljava/text/DateFormat;->getTimeInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getTimeDetailStringWithoutSeconds(Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->locale:Ljava/util/Locale;

    const/4 v1, 0x3

    invoke-static {v1, v0}, Ljava/text/DateFormat;->getTimeInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public inclusiveTaxTitleAndAmount(Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 460
    sget-object v0, Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;->NONE:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    if-eq p1, v0, :cond_1

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 464
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->buyer_printed_receipt_taxes_inclusive_tax_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 465
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/print/ReceiptFormatter;->getInclusiveTaxTitle(Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;Ljava/util/List;Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "tax_label"

    invoke-virtual {v0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/print/ReceiptFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 466
    invoke-interface {p2, p4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string p3, "amount"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 467
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 468
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public invoiceLabel()Ljava/lang/String;
    .locals 2

    .line 288
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->buyer_printed_receipt_invoice:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public itemNameOrCustomIfBlank(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 434
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 435
    iget-object p1, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/print/R$string;->buyer_printed_receipt_keypad_item_name:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public labelAndUnitPrice(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 552
    invoke-static {p2}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->buyer_printed_receipt_label_and_price:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "label"

    .line 554
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 556
    invoke-virtual {v0, p2}, Lcom/squareup/quantity/PerUnitFormatter;->money(Lcom/squareup/protos/common/Money;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p2

    .line 557
    invoke-virtual {p2, p3}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p2

    .line 558
    invoke-virtual {p2}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 559
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "price"

    .line 555
    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 560
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 561
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public nonTaxableItemLabel()Ljava/lang/String;
    .locals 2

    .line 296
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->buyer_printed_receipt_non_taxable:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public noteOrNull(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 543
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    return-object p1
.end method

.method public phoneNumberOrNull(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    invoke-interface {v0, p1}, Lcom/squareup/text/PhoneNumberHelper;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 138
    invoke-static {p1}, Lcom/squareup/util/Strings;->nullIfBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public purchaseSubtotal(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 311
    sget v0, Lcom/squareup/print/R$string;->buyer_printed_receipt_subtotal_purchase:I

    invoke-virtual {p0, v0, p1}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(ILcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    return-object p1
.end method

.method public quantity(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 588
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    new-instance v1, Ljava/math/BigDecimal;

    invoke-direct {v1, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 589
    invoke-virtual {v0, v1}, Lcom/squareup/quantity/PerUnitFormatter;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 590
    invoke-virtual {p1}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 591
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public quickTipCustomTipLabel()Ljava/lang/String;
    .locals 2

    .line 621
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->paper_signature_quick_tip_tip_label:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public quickTipCustomTotalLabel()Ljava/lang/String;
    .locals 2

    .line 625
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->paper_signature_quick_tip_total_label:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public quickTipNoTipLabel()Ljava/lang/String;
    .locals 2

    .line 629
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->paper_signature_quick_tip_no_tip_label:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public quickTipTitle()Ljava/lang/String;
    .locals 2

    .line 617
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->paper_signature_quick_tip_title_label:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public receiptNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 176
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->buyer_printed_receipt_number:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "receipt_number"

    .line 178
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 179
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, ""

    .line 181
    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public receiptNumber(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 185
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, ""

    return-object p1

    .line 189
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    .line 190
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    const-string v3, "#"

    if-ge v1, v2, :cond_1

    .line 191
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", "

    .line 193
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 195
    :cond_1
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    iget-object p1, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->buyer_printed_receipt_number:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v1, "receipt_number"

    .line 199
    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 200
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 201
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public remainingBalance(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 171
    sget v0, Lcom/squareup/print/R$string;->buyer_printed_receipt_remaining_balance:I

    invoke-direct {p0, v0, p1}, Lcom/squareup/print/ReceiptFormatter;->formatMoneyOrNull(ILcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    return-object p1
.end method

.method public reprint()Ljava/lang/String;
    .locals 3

    .line 521
    new-instance v0, Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/squareup/print/ReceiptFormatter;->locale:Ljava/util/Locale;

    const-string v2, "hh:mm:ss aaa"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 522
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 523
    iget-object v1, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/print/R$string;->ticket_reprint_label:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "time"

    .line 524
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 525
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 526
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public returnSubtotal(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 315
    sget v0, Lcom/squareup/print/R$string;->buyer_printed_receipt_subtotal_return:I

    invoke-virtual {p0, v0, p1}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(ILcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    return-object p1
.end method

.method public sequentialTenderNumber(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 205
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, ""

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->buyer_printed_receipt_invoice_number:I

    .line 207
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "invoice_number"

    .line 208
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 209
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 210
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public subtotal(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 307
    sget v0, Lcom/squareup/print/R$string;->buyer_printed_receipt_subtotal:I

    invoke-virtual {p0, v0, p1}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(ILcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    return-object p1
.end method

.method public surchargeDisclosure(Ljava/util/List;I)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Surcharge;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 429
    invoke-static {p1}, Lcom/squareup/checkout/Surcharge;->getAutoGratuity(Ljava/util/List;)Lcom/squareup/checkout/Surcharge$AutoGratuity;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/print/ReceiptFormatter;->countryCode:Lcom/squareup/CountryCode;

    invoke-virtual {p1, v0, p2, v1}, Lcom/squareup/checkout/Surcharge$AutoGratuity;->disclosureText(Lcom/squareup/util/Res;ILcom/squareup/CountryCode;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public swedishRoundingOrNullIfZero(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 331
    sget v0, Lcom/squareup/print/R$string;->buyer_printed_receipt_rounding:I

    invoke-direct {p0, v0, p1}, Lcom/squareup/print/ReceiptFormatter;->formatMoneyOrNullIfZero(ILcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    return-object p1
.end method

.method public taxBreakdownLabelAndPercentage(Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;)Ljava/lang/String;
    .locals 3

    .line 441
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->taxFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, p1, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;->taxPercentage:Lcom/squareup/util/Percentage;

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 442
    iget-object v1, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/print/R$string;->tax_breakdown_table_vat_rate_name_and_percentage:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;->label:Ljava/lang/String;

    const-string v2, "name"

    .line 443
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v1, "percentage"

    .line 444
    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 445
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 446
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public taxId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 450
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->buyer_printed_receipt_tax_id:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "tax_id_name"

    .line 451
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v0, "tax_id_number"

    .line 452
    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 453
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 454
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public taxInvoiceLabel()Ljava/lang/String;
    .locals 2

    .line 292
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->buyer_printed_receipt_tax_invoice:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public ticketNameOrBlank(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 0

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 512
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public ticketNameOrNull(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 2

    .line 499
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 502
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->buyer_printed_receipt_ticket:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "name"

    .line 503
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 504
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 505
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public ticketQuantityX()Ljava/lang/String;
    .locals 1

    const-string v0, " \u00d7"

    return-object v0
.end method

.method public tipOrNullIfZero(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 327
    sget v0, Lcom/squareup/print/R$string;->buyer_printed_receipt_tip:I

    invoke-direct {p0, v0, p1}, Lcom/squareup/print/ReceiptFormatter;->formatMoneyOrNullIfZero(ILcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    return-object p1
.end method

.method public tipPercentage(Lcom/squareup/util/Percentage;)Ljava/lang/String;
    .locals 2

    .line 633
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->paper_signature_quick_tip_percentage_pattern:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 634
    invoke-virtual {p1}, Lcom/squareup/util/Percentage;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "percentage"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 635
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 636
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public total(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 259
    sget v0, Lcom/squareup/print/R$string;->buyer_printed_receipt_total:I

    invoke-virtual {p0, v0, p1}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(ILcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    return-object p1
.end method

.method public traditionalTipLabel(Z)Ljava/lang/String;
    .locals 1

    .line 608
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    if-eqz p1, :cond_0

    sget p1, Lcom/squareup/print/R$string;->paper_signature_traditional_additional_tip_label:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/print/R$string;->paper_signature_traditional_tip_label:I

    :goto_0
    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public traditionalTipTotalLabel()Ljava/lang/String;
    .locals 2

    .line 613
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->paper_signature_traditional_total_label:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public twitterOrNull(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 159
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public unitQuantity(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 600
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    new-instance v1, Ljava/math/BigDecimal;

    invoke-direct {v1, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 601
    invoke-virtual {v0, v1, p2}, Lcom/squareup/quantity/PerUnitFormatter;->quantityAndPrecision(Ljava/math/BigDecimal;I)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 602
    invoke-virtual {p1, p3}, Lcom/squareup/quantity/PerUnitFormatter;->unit(Ljava/lang/String;)Lcom/squareup/quantity/PerUnitFormatter;

    move-result-object p1

    .line 603
    invoke-virtual {p1}, Lcom/squareup/quantity/PerUnitFormatter;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 604
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public voidLabel()Ljava/lang/String;
    .locals 2

    .line 535
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->printed_ticket_void:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public whichCopy(Z)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_0

    .line 655
    iget-object p1, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/print/R$string;->uppercase_receipt_customer_copy:I

    .line 656
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/print/ReceiptFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/print/R$string;->uppercase_receipt_merchant_copy:I

    .line 657
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method
