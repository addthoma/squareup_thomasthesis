.class public interface abstract Lcom/squareup/print/PrinterStations;
.super Ljava/lang/Object;
.source "PrinterStations.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\t\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0014\u0010\u0006\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00080\u0007H&J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000bH&J\u000e\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0008H&J\u000e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0008H&J\u000e\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0008H&J\u0010\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u000bH&J\u0016\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00082\u0006\u0010\u0012\u001a\u00020\u0013H&J\u0016\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00082\u0006\u0010\u0012\u001a\u00020\u0013H&J\u0016\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00082\u0006\u0010\u0012\u001a\u00020\u0013H&J\u0016\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00082\u0006\u0010\u0012\u001a\u00020\u0013H&J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0012\u001a\u00020\u0013H&J\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u001a\u001a\u00020\u000bH&J!\u0010\u001b\u001a\u00020\u001c2\u0012\u0010\u001d\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00130\u001e\"\u00020\u0013H&\u00a2\u0006\u0002\u0010\u001fJ\u0008\u0010 \u001a\u00020\u001cH&J!\u0010!\u001a\u00020\u001c2\u0012\u0010\u001d\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00130\u001e\"\u00020\u0013H&\u00a2\u0006\u0002\u0010\u001fJ\u0008\u0010\"\u001a\u00020\u001cH&J\u0008\u0010#\u001a\u00020\u001cH&J\u0010\u0010$\u001a\u00020\u001c2\u0006\u0010\u0010\u001a\u00020\u000bH&J!\u0010%\u001a\u00020\u001c2\u0012\u0010\u001d\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00130\u001e\"\u00020\u0013H&\u00a2\u0006\u0002\u0010\u001fJ\u0008\u0010&\u001a\u00020\u001cH&\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/print/PrinterStations;",
        "",
        "addNewPrinterStationByUuid",
        "",
        "printerStation",
        "Lcom/squareup/print/PrinterStation;",
        "allStations",
        "Lio/reactivex/Observable;",
        "",
        "deletePrinterStation",
        "stationUuid",
        "",
        "getAllEnabledStations",
        "getAllStations",
        "getDisabledStations",
        "getDisplayableStationsStringForPrinter",
        "hardwarePrinterId",
        "getEnabledExternalStationsFor",
        "role",
        "Lcom/squareup/print/PrinterStation$Role;",
        "getEnabledInternalStationsFor",
        "getEnabledInternalStationsWithoutPaperCutterFor",
        "getEnabledStationsFor",
        "getNumberOfEnabledStationsFor",
        "",
        "getPrinterStationById",
        "uuid",
        "hasEnabledExternalStationsForAnyRoleIn",
        "",
        "roles",
        "",
        "([Lcom/squareup/print/PrinterStation$Role;)Z",
        "hasEnabledInternalStation",
        "hasEnabledInternalStationsForAnyRoleIn",
        "hasEnabledKitchenPrintingStations",
        "hasEnabledStations",
        "hasEnabledStationsFor",
        "hasEnabledStationsForAnyRoleIn",
        "isTicketAutoNumberingEnabled",
        "hardware_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract addNewPrinterStationByUuid(Lcom/squareup/print/PrinterStation;)V
.end method

.method public abstract allStations()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract deletePrinterStation(Ljava/lang/String;)V
.end method

.method public abstract getAllEnabledStations()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAllStations()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getDisabledStations()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getDisplayableStationsStringForPrinter(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getEnabledExternalStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrinterStation$Role;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEnabledInternalStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrinterStation$Role;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEnabledInternalStationsWithoutPaperCutterFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrinterStation$Role;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getEnabledStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrinterStation$Role;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getNumberOfEnabledStationsFor(Lcom/squareup/print/PrinterStation$Role;)I
.end method

.method public abstract getPrinterStationById(Ljava/lang/String;)Lcom/squareup/print/PrinterStation;
.end method

.method public varargs abstract hasEnabledExternalStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z
.end method

.method public abstract hasEnabledInternalStation()Z
.end method

.method public varargs abstract hasEnabledInternalStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z
.end method

.method public abstract hasEnabledKitchenPrintingStations()Z
.end method

.method public abstract hasEnabledStations()Z
.end method

.method public abstract hasEnabledStationsFor(Ljava/lang/String;)Z
.end method

.method public varargs abstract hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z
.end method

.method public abstract isTicketAutoNumberingEnabled()Z
.end method
