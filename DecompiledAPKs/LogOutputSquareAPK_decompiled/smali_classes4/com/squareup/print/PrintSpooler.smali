.class public Lcom/squareup/print/PrintSpooler;
.super Ljava/lang/Object;
.source "PrintSpooler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/PrintSpooler$PrinterActivityListener;,
        Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;
    }
.end annotation


# static fields
.field private static final TEST_JOB_TARGET_NAME:Ljava/lang/String; = "TestTarget"

.field private static final UNKNOWN_SOURCE:Ljava/lang/String; = "Unknown"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final blockedPrinterLogRunner:Lcom/squareup/print/BlockedPrinterLogRunner;

.field private final clock:Lcom/squareup/util/Clock;

.field private final hardwarePrinterExecutor:Lcom/squareup/print/HardwarePrinterExecutor;

.field private final hardwarePrinterTrackerForCashDrawers:Lcom/squareup/print/HardwarePrinterTracker;

.field final hardwarePrintersToOpenCashDrawers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final inFlightPrintJobIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final printRenderer:Lcom/squareup/print/PayloadRenderer;

.field private final printTargetRouter:Lcom/squareup/print/PrintTargetRouter;

.field private final printerActivityListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintSpooler$PrinterActivityListener;",
            ">;"
        }
    .end annotation
.end field

.field private final queueExecutor:Lcom/squareup/print/PrintQueueExecutor;

.field private final statusListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/print/PayloadRenderer;Lcom/squareup/print/PrintTargetRouter;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/PrintQueueExecutor;Lcom/squareup/print/HardwarePrinterExecutor;Lcom/squareup/analytics/Analytics;Lcom/squareup/print/BlockedPrinterLogRunner;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Clock;)V
    .locals 1

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/squareup/print/PrintSpooler;->statusListeners:Ljava/util/List;

    .line 73
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 74
    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/print/PrintSpooler;->printerActivityListeners:Ljava/util/List;

    .line 87
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/print/PrintSpooler;->hardwarePrintersToOpenCashDrawers:Ljava/util/Set;

    .line 96
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/print/PrintSpooler;->inFlightPrintJobIds:Ljava/util/Set;

    .line 111
    iput-object p3, p0, Lcom/squareup/print/PrintSpooler;->hardwarePrinterTrackerForCashDrawers:Lcom/squareup/print/HardwarePrinterTracker;

    .line 112
    iput-object p4, p0, Lcom/squareup/print/PrintSpooler;->queueExecutor:Lcom/squareup/print/PrintQueueExecutor;

    .line 113
    iput-object p5, p0, Lcom/squareup/print/PrintSpooler;->hardwarePrinterExecutor:Lcom/squareup/print/HardwarePrinterExecutor;

    .line 114
    iput-object p1, p0, Lcom/squareup/print/PrintSpooler;->printRenderer:Lcom/squareup/print/PayloadRenderer;

    .line 115
    iput-object p2, p0, Lcom/squareup/print/PrintSpooler;->printTargetRouter:Lcom/squareup/print/PrintTargetRouter;

    .line 116
    iput-object p6, p0, Lcom/squareup/print/PrintSpooler;->analytics:Lcom/squareup/analytics/Analytics;

    .line 117
    iput-object p7, p0, Lcom/squareup/print/PrintSpooler;->blockedPrinterLogRunner:Lcom/squareup/print/BlockedPrinterLogRunner;

    .line 118
    iput-object p8, p0, Lcom/squareup/print/PrintSpooler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 119
    iput-object p9, p0, Lcom/squareup/print/PrintSpooler;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method private doCheckQueues(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/print/PrintJob;",
            ">;)V"
        }
    .end annotation

    .line 290
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 293
    iget-object v2, p0, Lcom/squareup/print/PrintSpooler;->hardwarePrintersToOpenCashDrawers:Ljava/util/Set;

    .line 294
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "[checkQueues_cash] Drawers to attempt to open: %d"

    .line 293
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 295
    iget-object v1, p0, Lcom/squareup/print/PrintSpooler;->hardwarePrintersToOpenCashDrawers:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 296
    iget-object v4, p0, Lcom/squareup/print/PrintSpooler;->hardwarePrinterTrackerForCashDrawers:Lcom/squareup/print/HardwarePrinterTracker;

    .line 297
    invoke-virtual {v4, v2}, Lcom/squareup/print/HardwarePrinterTracker;->getHardwarePrinter(Ljava/lang/String;)Lcom/squareup/print/HardwarePrinter;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 300
    invoke-virtual {p0, v4}, Lcom/squareup/print/PrintSpooler;->attemptToOpenCashDrawer(Lcom/squareup/print/HardwarePrinter;)V

    goto :goto_0

    :cond_0
    new-array v4, v0, [Ljava/lang/Object;

    aput-object v2, v4, v3

    const-string v5, "[checkQueues_cash] Drawer %s not found, dropping open job."

    .line 302
    invoke-static {v5, v4}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 303
    iget-object v4, p0, Lcom/squareup/print/PrintSpooler;->hardwarePrintersToOpenCashDrawers:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 309
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "[checkQueues_Jobs] Jobs to attempt to print: %d"

    .line 308
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 310
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 311
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/PrintJob;

    invoke-virtual {p0, v1, v0}, Lcom/squareup/print/PrintSpooler;->attemptToPrint(Ljava/lang/String;Lcom/squareup/print/PrintJob;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method static synthetic lambda$enqueueDumpQueueToLog$8(Lcom/squareup/print/PrintCallback$PrintCallbackResult;)V
    .locals 4

    .line 262
    invoke-interface {p0}, Lcom/squareup/print/PrintCallback$PrintCallbackResult;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/List;

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 263
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "[Dump_START] %d Jobs"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 264
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrintJob;

    new-array v2, v0, [Ljava/lang/Object;

    .line 265
    invoke-virtual {v1}, Lcom/squareup/print/PrintJob;->generateStatusDebugText()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    const-string v1, "[Dump] %s"

    invoke-static {v1, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-array p0, v3, [Ljava/lang/Object;

    const-string v0, "[DumpToLog_END]"

    .line 267
    invoke-static {v0, p0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$performOpenCashDrawer$10(Lcom/squareup/print/HardwarePrinter;Ljava/lang/String;)V
    .locals 1

    .line 339
    invoke-virtual {p0}, Lcom/squareup/print/HardwarePrinter;->performOpenCashDrawer()V

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p1, p0, v0

    const-string p1, "[PrintSpooler_openCashDrawer] Drawer opened for HardwarePrinter %s"

    .line 340
    invoke-static {p1, p0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$purgeFailedJobs$7(Ljava/util/Collection;Ljava/lang/String;Lcom/squareup/print/PrintJobQueue;)V
    .locals 0

    .line 253
    invoke-interface {p2, p0, p1}, Lcom/squareup/print/PrintJobQueue;->removeFailedJobs(Ljava/util/Collection;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$updateQueueAfterPrintAttempt$14(Lcom/squareup/print/PrintJob;Lcom/squareup/print/PrintJobQueue;)V
    .locals 4

    .line 484
    invoke-virtual {p0}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/print/PrintJob$PrintAttempt;->result:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->SUCCESS:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_0

    new-array v0, v3, [Ljava/lang/Object;

    .line 486
    invoke-virtual {p0}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "[updateStatus] Performing queue work to remove successful %s"

    .line 485
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 487
    invoke-virtual {p0}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object p0

    invoke-interface {p1, p0}, Lcom/squareup/print/PrintJobQueue;->removeSuccessfulJob(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-array v0, v3, [Ljava/lang/Object;

    .line 489
    invoke-virtual {p0}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "[updateStatus] Performing queue work to remove failed %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 490
    invoke-interface {p1, p0}, Lcom/squareup/print/PrintJobQueue;->updateJobAsFailed(Lcom/squareup/print/PrintJob;)V

    :goto_0
    return-void
.end method

.method private notifyPrintJobEnqueued(Lcom/squareup/print/PrintJob;)V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 510
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 511
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getPrintAttemptsCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "[notifyPrintJobEnqueued] Job Id: %s, attempt count: %s"

    .line 510
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 512
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->statusListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;

    .line 513
    invoke-interface {v1, p1}, Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;->onPrintJobEnqueued(Lcom/squareup/print/PrintJob;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private performPrint(Lcom/squareup/print/PrintJob;Lcom/squareup/print/HardwarePrinter;)V
    .locals 8

    .line 437
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 438
    invoke-virtual {p2}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object v5

    .line 440
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getPrintTimingData()Lcom/squareup/print/PrintTimingData;

    move-result-object v4

    .line 442
    iget-object v6, p0, Lcom/squareup/print/PrintSpooler;->hardwarePrinterExecutor:Lcom/squareup/print/HardwarePrinterExecutor;

    new-instance v7, Lcom/squareup/print/-$$Lambda$PrintSpooler$81jRatSknnAn7RGglfrGGETTQ2E;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/-$$Lambda$PrintSpooler$81jRatSknnAn7RGglfrGGETTQ2E;-><init>(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/HardwarePrinter;Lcom/squareup/print/PrintJob;Lcom/squareup/print/PrintTimingData;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/print/-$$Lambda$PrintSpooler$K_siFm9Nt8iGayExxrjQED0Ht64;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/print/-$$Lambda$PrintSpooler$K_siFm9Nt8iGayExxrjQED0Ht64;-><init>(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/HardwarePrinter;Lcom/squareup/print/PrintJob;)V

    invoke-interface {v6, p2, v7, v0}, Lcom/squareup/print/HardwarePrinterExecutor;->claimHardwarePrinterAndExecute(Lcom/squareup/print/HardwarePrinter;Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    return-void
.end method

.method private unlockPrintJob(Lcom/squareup/print/PrintJob;)V
    .locals 1

    .line 506
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->inFlightPrintJobIds:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method private updateListenersAfterPrinterActivity(Lcom/squareup/print/HardwarePrinter;Z)V
    .locals 5

    .line 528
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->printerActivityListeners:Ljava/util/List;

    monitor-enter v0

    :try_start_0
    const-string v1, "[updateListeners] HardwarePrinter %s {active: %s}"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 529
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 530
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    .line 529
    invoke-static {v1, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 531
    iget-object v1, p0, Lcom/squareup/print/PrintSpooler;->printerActivityListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/PrintSpooler$PrinterActivityListener;

    .line 532
    invoke-interface {v2, p1, p2}, Lcom/squareup/print/PrintSpooler$PrinterActivityListener;->onPrinterActive(Lcom/squareup/print/HardwarePrinter;Z)V

    goto :goto_0

    .line 534
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private updateListenersAfterQueueUpdated(Lcom/squareup/print/PrintJob;)V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 518
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 519
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/print/PrintJob$PrintAttempt;->result:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "[updateListeners] Job Id: %s, result: %s"

    .line 518
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 520
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 521
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->statusListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;

    .line 522
    invoke-interface {v1, p1}, Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;->onPrintJobAttempted(Lcom/squareup/print/PrintJob;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public addPrintJobStatusListener(Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;)V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->statusListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addPrinterActivityListener(Lcom/squareup/print/PrintSpooler$PrinterActivityListener;)V
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->printerActivityListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method attemptToOpenCashDrawer(Lcom/squareup/print/HardwarePrinter;)V
    .locals 3

    .line 320
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 321
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object v0

    .line 322
    iget-object v1, p0, Lcom/squareup/print/PrintSpooler;->hardwarePrinterExecutor:Lcom/squareup/print/HardwarePrinterExecutor;

    invoke-interface {v1, p1}, Lcom/squareup/print/HardwarePrinterExecutor;->isHardwarePrinterClaimed(Lcom/squareup/print/HardwarePrinter;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "attemptToOpenCashDrawer"

    aput-object v2, p1, v1

    const/4 v1, 0x1

    aput-object v0, p1, v1

    const-string v0, "[%s] HardwarePrinter %s is occupied, skipping opening drawer for now."

    .line 323
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 327
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/print/PrintSpooler;->performOpenCashDrawer(Lcom/squareup/print/HardwarePrinter;)V

    return-void
.end method

.method attemptToPrint(Ljava/lang/String;Lcom/squareup/print/PrintJob;)V
    .locals 9

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 356
    invoke-virtual {p2}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "[PrintSpooler_attemptToPrint] Job Id: %s."

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 357
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 360
    iget-object v1, p0, Lcom/squareup/print/PrintSpooler;->inFlightPrintJobIds:Ljava/util/Set;

    invoke-virtual {p2}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-array p1, v0, [Ljava/lang/Object;

    .line 362
    invoke-virtual {p2}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v3

    const-string p2, "[PrintSpooler_attemptToPrint_earlyExit] PrintJob %s is already in flight"

    .line 361
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 366
    :cond_0
    iget-object v1, p0, Lcom/squareup/print/PrintSpooler;->blockedPrinterLogRunner:Lcom/squareup/print/BlockedPrinterLogRunner;

    invoke-interface {v1, p2}, Lcom/squareup/print/BlockedPrinterLogRunner;->onJobStarted(Lcom/squareup/print/PrintJob;)V

    .line 369
    iget-object v1, p0, Lcom/squareup/print/PrintSpooler;->inFlightPrintJobIds:Ljava/util/Set;

    invoke-virtual {p2}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 371
    iget-object v1, p0, Lcom/squareup/print/PrintSpooler;->printTargetRouter:Lcom/squareup/print/PrintTargetRouter;

    iget-boolean v2, p2, Lcom/squareup/print/PrintJob;->forTestPrint:Z

    .line 372
    invoke-interface {v1, p1, v2}, Lcom/squareup/print/PrintTargetRouter;->retrieveHardwarePrinterFromTarget(Ljava/lang/String;Z)Lkotlin/Pair;

    move-result-object v1

    .line 373
    invoke-virtual {v1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/PrintTargetRouter$RouteResult;

    .line 374
    invoke-virtual {v1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/HardwarePrinter;

    .line 376
    invoke-virtual {p2}, Lcom/squareup/print/PrintJob;->incrementPrintAttempts()I

    if-nez v1, :cond_1

    const-string v4, "(no hardware printer)"

    goto :goto_0

    .line 379
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object v4

    :goto_0
    const/4 v5, 0x4

    new-array v6, v5, [Ljava/lang/Object;

    const-string v7, "PrintSpooler_attemptToPrint"

    aput-object v7, v6, v3

    aput-object p1, v6, v0

    const/4 p1, 0x2

    aput-object v4, v6, p1

    .line 381
    invoke-virtual {v2}, Lcom/squareup/print/PrintTargetRouter$RouteResult;->name()Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x3

    aput-object v4, v6, v7

    const-string v4, "[%s] Target %s attempting to print to hardware printer %s with route resolution %s."

    .line 380
    invoke-static {v4, v6}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 383
    invoke-virtual {p2}, Lcom/squareup/print/PrintJob;->getPrintTimingData()Lcom/squareup/print/PrintTimingData;

    move-result-object v4

    .line 385
    sget-object v6, Lcom/squareup/print/PrintSpooler$2;->$SwitchMap$com$squareup$print$PrintTargetRouter$RouteResult:[I

    invoke-virtual {v2}, Lcom/squareup/print/PrintTargetRouter$RouteResult;->ordinal()I

    move-result v8

    aget v6, v6, v8

    if-eq v6, v0, :cond_6

    if-eq v6, p1, :cond_5

    if-eq v6, v7, :cond_4

    if-ne v6, v5, :cond_3

    .line 416
    iget-object v2, p0, Lcom/squareup/print/PrintSpooler;->hardwarePrinterExecutor:Lcom/squareup/print/HardwarePrinterExecutor;

    invoke-interface {v2, v1}, Lcom/squareup/print/HardwarePrinterExecutor;->isHardwarePrinterClaimed(Lcom/squareup/print/HardwarePrinter;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-array v2, v7, [Ljava/lang/Object;

    const-string v4, "PrintSpooler_attemptToPrint_exit"

    aput-object v4, v2, v3

    .line 418
    invoke-virtual {v1}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-virtual {p2}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, p1

    const-string p1, "[%s] HardwarePrinter %s in use, skipping job %s for now."

    .line 417
    invoke-static {p1, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 419
    invoke-direct {p0, p2}, Lcom/squareup/print/PrintSpooler;->unlockPrintJob(Lcom/squareup/print/PrintJob;)V

    .line 420
    iget-object p1, p0, Lcom/squareup/print/PrintSpooler;->blockedPrinterLogRunner:Lcom/squareup/print/BlockedPrinterLogRunner;

    invoke-interface {p1, p2}, Lcom/squareup/print/BlockedPrinterLogRunner;->onJobFinished(Lcom/squareup/print/PrintJob;)V

    return-void

    .line 424
    :cond_2
    invoke-direct {p0, p2, v1}, Lcom/squareup/print/PrintSpooler;->performPrint(Lcom/squareup/print/PrintJob;Lcom/squareup/print/HardwarePrinter;)V

    return-void

    .line 428
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Cannot understand RouteResult: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 407
    :cond_4
    iget-object p1, p0, Lcom/squareup/print/PrintSpooler;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p2}, Lcom/squareup/print/PrintFinishedEventKt;->targetHardwarePrinterUnavailableError(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrintFinishedEvent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 408
    invoke-virtual {p2}, Lcom/squareup/print/PrintJob;->getPrintTimingData()Lcom/squareup/print/PrintTimingData;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->clock:Lcom/squareup/util/Clock;

    invoke-virtual {p1, v0}, Lcom/squareup/print/PrintTimingData;->finished(Lcom/squareup/util/Clock;)V

    .line 409
    new-instance p1, Lcom/squareup/print/PrintJob$PrintAttempt;

    sget-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->HARDWARE_PRINTER_NOT_AVAILABLE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    sget-object v1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->NO_PRINTER:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    invoke-direct {p1, v0, v1, v4}, Lcom/squareup/print/PrintJob$PrintAttempt;-><init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Lcom/squareup/print/PrintTimingData;)V

    invoke-virtual {p2, p1}, Lcom/squareup/print/PrintJob;->setPrintAttempt(Lcom/squareup/print/PrintJob$PrintAttempt;)V

    .line 411
    invoke-virtual {p0, p2}, Lcom/squareup/print/PrintSpooler;->updateQueueAfterPrintAttempt(Lcom/squareup/print/PrintJob;)V

    return-void

    .line 398
    :cond_5
    iget-object p1, p0, Lcom/squareup/print/PrintSpooler;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p2}, Lcom/squareup/print/PrintFinishedEventKt;->targetHasNoHardwarePrinterError(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrintFinishedEvent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 399
    invoke-virtual {p2}, Lcom/squareup/print/PrintJob;->getPrintTimingData()Lcom/squareup/print/PrintTimingData;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->clock:Lcom/squareup/util/Clock;

    invoke-virtual {p1, v0}, Lcom/squareup/print/PrintTimingData;->finished(Lcom/squareup/util/Clock;)V

    .line 400
    new-instance p1, Lcom/squareup/print/PrintJob$PrintAttempt;

    sget-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->TARGET_HAS_NO_PRINTER:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    sget-object v1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->NO_PRINTER:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    invoke-direct {p1, v0, v1, v4}, Lcom/squareup/print/PrintJob$PrintAttempt;-><init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Lcom/squareup/print/PrintTimingData;)V

    invoke-virtual {p2, p1}, Lcom/squareup/print/PrintJob;->setPrintAttempt(Lcom/squareup/print/PrintJob$PrintAttempt;)V

    .line 402
    invoke-virtual {p0, p2}, Lcom/squareup/print/PrintSpooler;->updateQueueAfterPrintAttempt(Lcom/squareup/print/PrintJob;)V

    return-void

    .line 389
    :cond_6
    iget-object p1, p0, Lcom/squareup/print/PrintSpooler;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p2}, Lcom/squareup/print/PrintFinishedEventKt;->targetDoesNotExistError(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrintFinishedEvent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 390
    invoke-virtual {p2}, Lcom/squareup/print/PrintJob;->getPrintTimingData()Lcom/squareup/print/PrintTimingData;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->clock:Lcom/squareup/util/Clock;

    invoke-virtual {p1, v0}, Lcom/squareup/print/PrintTimingData;->finished(Lcom/squareup/util/Clock;)V

    .line 391
    new-instance p1, Lcom/squareup/print/PrintJob$PrintAttempt;

    sget-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->TARGET_UNRESOLVABLE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    sget-object v1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->NO_PRINTER:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    invoke-direct {p1, v0, v1, v4}, Lcom/squareup/print/PrintJob$PrintAttempt;-><init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Lcom/squareup/print/PrintTimingData;)V

    invoke-virtual {p2, p1}, Lcom/squareup/print/PrintJob;->setPrintAttempt(Lcom/squareup/print/PrintJob$PrintAttempt;)V

    .line 393
    invoke-virtual {p0, p2}, Lcom/squareup/print/PrintSpooler;->updateQueueAfterPrintAttempt(Lcom/squareup/print/PrintJob;)V

    return-void
.end method

.method public checkQueues()V
    .locals 3

    .line 280
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 281
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->queueExecutor:Lcom/squareup/print/PrintQueueExecutor;

    sget-object v1, Lcom/squareup/print/-$$Lambda$CdsMWwOseFcVBmhONudtXkSSRjc;->INSTANCE:Lcom/squareup/print/-$$Lambda$CdsMWwOseFcVBmhONudtXkSSRjc;

    new-instance v2, Lcom/squareup/print/-$$Lambda$PrintSpooler$8qJLKJhKz0ohr5z6QAevmZvrhSE;

    invoke-direct {v2, p0}, Lcom/squareup/print/-$$Lambda$PrintSpooler$8qJLKJhKz0ohr5z6QAevmZvrhSE;-><init>(Lcom/squareup/print/PrintSpooler;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/print/PrintQueueExecutor;->execute(Lcom/squareup/print/PrintQueueExecutor$PrintTask;Lcom/squareup/print/PrintCallback;)V

    return-void
.end method

.method public enqueueDumpQueueToLog()V
    .locals 3

    .line 261
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->queueExecutor:Lcom/squareup/print/PrintQueueExecutor;

    sget-object v1, Lcom/squareup/print/-$$Lambda$n4U9Y0Rqvn9UDHoinWgmMtqP7F4;->INSTANCE:Lcom/squareup/print/-$$Lambda$n4U9Y0Rqvn9UDHoinWgmMtqP7F4;

    sget-object v2, Lcom/squareup/print/-$$Lambda$PrintSpooler$VMDCZ1lxP_DgirdG1FogFfOGodU;->INSTANCE:Lcom/squareup/print/-$$Lambda$PrintSpooler$VMDCZ1lxP_DgirdG1FogFfOGodU;

    invoke-interface {v0, v1, v2}, Lcom/squareup/print/PrintQueueExecutor;->execute(Lcom/squareup/print/PrintQueueExecutor$PrintTask;Lcom/squareup/print/PrintCallback;)V

    return-void
.end method

.method public enqueueForPrint(Lcom/squareup/print/PrintTarget;Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 158
    new-instance v6, Lcom/squareup/print/PrintJob;

    const/4 v4, 0x0

    move-object v0, v6

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/PrintJob;-><init>(Lcom/squareup/print/PrintTarget;Lcom/squareup/print/PrintablePayload;Ljava/lang/String;ZLjava/lang/String;)V

    .line 163
    iget-object p2, p0, Lcom/squareup/print/PrintSpooler;->printTargetRouter:Lcom/squareup/print/PrintTargetRouter;

    .line 164
    invoke-interface {p1}, Lcom/squareup/print/PrintTarget;->getId()Ljava/lang/String;

    move-result-object p1

    const/4 p3, 0x0

    invoke-interface {p2, p1, p3}, Lcom/squareup/print/PrintTargetRouter;->retrieveHardwarePrinterFromTarget(Ljava/lang/String;Z)Lkotlin/Pair;

    move-result-object p1

    .line 165
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/HardwarePrinter;

    if-eqz p1, :cond_0

    .line 167
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object p1

    invoke-virtual {v6, p1}, Lcom/squareup/print/PrintJob;->setHardwareInfo(Lcom/squareup/print/HardwarePrinter$HardwareInfo;)V

    .line 170
    :cond_0
    invoke-virtual {v6}, Lcom/squareup/print/PrintJob;->resetPrintTimingData()V

    .line 171
    invoke-virtual {v6}, Lcom/squareup/print/PrintJob;->getPrintTimingData()Lcom/squareup/print/PrintTimingData;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/print/PrintSpooler;->clock:Lcom/squareup/util/Clock;

    invoke-virtual {p1, p2}, Lcom/squareup/print/PrintTimingData;->enqueuing(Lcom/squareup/util/Clock;)V

    .line 173
    iget-object p1, p0, Lcom/squareup/print/PrintSpooler;->queueExecutor:Lcom/squareup/print/PrintQueueExecutor;

    new-instance p2, Lcom/squareup/print/-$$Lambda$PrintSpooler$JsEVfESHFwiHKdMq-uFiMtNvv84;

    invoke-direct {p2, p0, v6}, Lcom/squareup/print/-$$Lambda$PrintSpooler$JsEVfESHFwiHKdMq-uFiMtNvv84;-><init>(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/PrintJob;)V

    new-instance p3, Lcom/squareup/print/-$$Lambda$PrintSpooler$nKYCH7nMVInenI2KgbGpYMsRrHc;

    invoke-direct {p3, p0}, Lcom/squareup/print/-$$Lambda$PrintSpooler$nKYCH7nMVInenI2KgbGpYMsRrHc;-><init>(Lcom/squareup/print/PrintSpooler;)V

    invoke-interface {p1, p2, p3}, Lcom/squareup/print/PrintQueueExecutor;->execute(Lcom/squareup/print/PrintQueueExecutor$PrintTask;Lcom/squareup/print/PrintCallback;)V

    return-void
.end method

.method public enqueueForReprint(Lcom/squareup/print/PrintJob;)V
    .locals 2

    .line 223
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->resetPrintTimingData()V

    .line 224
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getPrintTimingData()Lcom/squareup/print/PrintTimingData;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/print/PrintSpooler;->clock:Lcom/squareup/util/Clock;

    invoke-virtual {v0, v1}, Lcom/squareup/print/PrintTimingData;->enqueuing(Lcom/squareup/util/Clock;)V

    .line 226
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->queueExecutor:Lcom/squareup/print/PrintQueueExecutor;

    new-instance v1, Lcom/squareup/print/-$$Lambda$PrintSpooler$2-8OWt-ln8yVp2mSyUotA-FTTGk;

    invoke-direct {v1, p0, p1}, Lcom/squareup/print/-$$Lambda$PrintSpooler$2-8OWt-ln8yVp2mSyUotA-FTTGk;-><init>(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/PrintJob;)V

    new-instance p1, Lcom/squareup/print/-$$Lambda$PrintSpooler$srl7iglfpsdbSQ5By3Oa1TERYGg;

    invoke-direct {p1, p0}, Lcom/squareup/print/-$$Lambda$PrintSpooler$srl7iglfpsdbSQ5By3Oa1TERYGg;-><init>(Lcom/squareup/print/PrintSpooler;)V

    invoke-interface {v0, v1, p1}, Lcom/squareup/print/PrintQueueExecutor;->execute(Lcom/squareup/print/PrintQueueExecutor$PrintTask;Lcom/squareup/print/PrintCallback;)V

    return-void
.end method

.method public enqueueForTestPrint(Lcom/squareup/print/HardwarePrinter;Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 191
    new-instance v6, Lcom/squareup/print/PrintJob;

    new-instance v1, Lcom/squareup/print/PrintSpooler$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/print/PrintSpooler$1;-><init>(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/HardwarePrinter;)V

    const/4 v4, 0x1

    move-object v0, v6

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/PrintJob;-><init>(Lcom/squareup/print/PrintTarget;Lcom/squareup/print/PrintablePayload;Ljava/lang/String;ZLjava/lang/String;)V

    .line 202
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object p1

    invoke-virtual {v6, p1}, Lcom/squareup/print/PrintJob;->setHardwareInfo(Lcom/squareup/print/HardwarePrinter$HardwareInfo;)V

    .line 203
    invoke-virtual {v6}, Lcom/squareup/print/PrintJob;->resetPrintTimingData()V

    .line 204
    invoke-virtual {v6}, Lcom/squareup/print/PrintJob;->getPrintTimingData()Lcom/squareup/print/PrintTimingData;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/print/PrintSpooler;->clock:Lcom/squareup/util/Clock;

    invoke-virtual {p1, p2}, Lcom/squareup/print/PrintTimingData;->enqueuing(Lcom/squareup/util/Clock;)V

    .line 206
    iget-object p1, p0, Lcom/squareup/print/PrintSpooler;->queueExecutor:Lcom/squareup/print/PrintQueueExecutor;

    new-instance p2, Lcom/squareup/print/-$$Lambda$PrintSpooler$_kN1xKiFx9OSbhZHVqRd_Pl1azI;

    invoke-direct {p2, p0, v6}, Lcom/squareup/print/-$$Lambda$PrintSpooler$_kN1xKiFx9OSbhZHVqRd_Pl1azI;-><init>(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/PrintJob;)V

    new-instance p3, Lcom/squareup/print/-$$Lambda$PrintSpooler$WZ9VjXk7xcjFak7BL2zSt2MbZ18;

    invoke-direct {p3, p0}, Lcom/squareup/print/-$$Lambda$PrintSpooler$WZ9VjXk7xcjFak7BL2zSt2MbZ18;-><init>(Lcom/squareup/print/PrintSpooler;)V

    invoke-interface {p1, p2, p3}, Lcom/squareup/print/PrintQueueExecutor;->execute(Lcom/squareup/print/PrintQueueExecutor$PrintTask;Lcom/squareup/print/PrintCallback;)V

    return-void
.end method

.method public enqueuePriorityOpenCashDrawersFor(Ljava/lang/Iterable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;)V"
        }
    .end annotation

    .line 144
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/print/-$$Lambda$PrintSpooler$BiPNXGjqoiMJtjF7JWKoURNHhI4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/print/-$$Lambda$PrintSpooler$BiPNXGjqoiMJtjF7JWKoURNHhI4;-><init>(Lcom/squareup/print/PrintSpooler;Ljava/lang/Iterable;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getFailedPrintJobs(Lcom/squareup/print/PrintCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrintCallback<",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/print/PrintJob;",
            ">;>;)V"
        }
    .end annotation

    .line 244
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->queueExecutor:Lcom/squareup/print/PrintQueueExecutor;

    sget-object v1, Lcom/squareup/print/-$$Lambda$mBjfBSqyhfg697O--vSWClwd_ZM;->INSTANCE:Lcom/squareup/print/-$$Lambda$mBjfBSqyhfg697O--vSWClwd_ZM;

    invoke-interface {v0, v1, p1}, Lcom/squareup/print/PrintQueueExecutor;->execute(Lcom/squareup/print/PrintQueueExecutor$PrintTask;Lcom/squareup/print/PrintCallback;)V

    return-void
.end method

.method public synthetic lambda$checkQueues$9$PrintSpooler(Lcom/squareup/print/PrintCallback$PrintCallbackResult;)V
    .locals 0

    .line 282
    invoke-interface {p1}, Lcom/squareup/print/PrintCallback$PrintCallbackResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/squareup/print/PrintSpooler;->doCheckQueues(Ljava/util/Map;)V

    return-void
.end method

.method public synthetic lambda$enqueueForPrint$1$PrintSpooler(Lcom/squareup/print/PrintJob;Lcom/squareup/print/PrintJobQueue;)Ljava/util/Map;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 175
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getTargetId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "[enqueueForPrint] Enqueuing job id %s to target %s."

    .line 174
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 176
    invoke-direct {p0, p1}, Lcom/squareup/print/PrintSpooler;->notifyPrintJobEnqueued(Lcom/squareup/print/PrintJob;)V

    .line 177
    invoke-interface {p2, p1}, Lcom/squareup/print/PrintJobQueue;->enqueueNewJob(Lcom/squareup/print/PrintJob;)V

    .line 178
    invoke-interface {p2}, Lcom/squareup/print/PrintJobQueue;->retrieveHeadPrintJobPerPrintTarget()Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$enqueueForPrint$2$PrintSpooler(Lcom/squareup/print/PrintCallback$PrintCallbackResult;)V
    .locals 0

    .line 181
    invoke-interface {p1}, Lcom/squareup/print/PrintCallback$PrintCallbackResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/squareup/print/PrintSpooler;->doCheckQueues(Ljava/util/Map;)V

    return-void
.end method

.method public synthetic lambda$enqueueForReprint$5$PrintSpooler(Lcom/squareup/print/PrintJob;Lcom/squareup/print/PrintJobQueue;)Ljava/util/Map;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 228
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getTargetId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "[PrintSpooler_reenqueueForPrint] Reenqueuing job id %s to target %s."

    .line 227
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/print/PrintSpooler;->notifyPrintJobEnqueued(Lcom/squareup/print/PrintJob;)V

    .line 230
    invoke-interface {p2, p1}, Lcom/squareup/print/PrintJobQueue;->reenqueueFailedJob(Lcom/squareup/print/PrintJob;)V

    .line 231
    invoke-interface {p2}, Lcom/squareup/print/PrintJobQueue;->retrieveHeadPrintJobPerPrintTarget()Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$enqueueForReprint$6$PrintSpooler(Lcom/squareup/print/PrintCallback$PrintCallbackResult;)V
    .locals 0

    .line 234
    invoke-interface {p1}, Lcom/squareup/print/PrintCallback$PrintCallbackResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/squareup/print/PrintSpooler;->doCheckQueues(Ljava/util/Map;)V

    return-void
.end method

.method public synthetic lambda$enqueueForTestPrint$3$PrintSpooler(Lcom/squareup/print/PrintJob;Lcom/squareup/print/PrintJobQueue;)Ljava/util/Map;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 208
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getTargetId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "[enqueueForTestPrint] Enqueuing job id %s to printer %s."

    .line 207
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 209
    invoke-direct {p0, p1}, Lcom/squareup/print/PrintSpooler;->notifyPrintJobEnqueued(Lcom/squareup/print/PrintJob;)V

    .line 210
    invoke-interface {p2, p1}, Lcom/squareup/print/PrintJobQueue;->enqueueNewJob(Lcom/squareup/print/PrintJob;)V

    .line 211
    invoke-interface {p2}, Lcom/squareup/print/PrintJobQueue;->retrieveHeadPrintJobPerPrintTarget()Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$enqueueForTestPrint$4$PrintSpooler(Lcom/squareup/print/PrintCallback$PrintCallbackResult;)V
    .locals 0

    .line 214
    invoke-interface {p1}, Lcom/squareup/print/PrintCallback$PrintCallbackResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/squareup/print/PrintSpooler;->doCheckQueues(Ljava/util/Map;)V

    return-void
.end method

.method public synthetic lambda$enqueuePriorityOpenCashDrawersFor$0$PrintSpooler(Ljava/lang/Iterable;)V
    .locals 2

    .line 145
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/HardwarePrinter;

    .line 146
    iget-object v1, p0, Lcom/squareup/print/PrintSpooler;->hardwarePrintersToOpenCashDrawers:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 148
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/print/PrintSpooler;->checkQueues()V

    return-void
.end method

.method public synthetic lambda$performOpenCashDrawer$11$PrintSpooler(Ljava/lang/String;Lcom/squareup/print/HardwarePrinter;)V
    .locals 1

    .line 344
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->hardwarePrintersToOpenCashDrawers:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 347
    iget-object p1, p0, Lcom/squareup/print/PrintSpooler;->hardwarePrinterExecutor:Lcom/squareup/print/HardwarePrinterExecutor;

    invoke-interface {p1, p2}, Lcom/squareup/print/HardwarePrinterExecutor;->releaseHardwarePrinter(Lcom/squareup/print/HardwarePrinter;)V

    .line 350
    invoke-virtual {p0}, Lcom/squareup/print/PrintSpooler;->checkQueues()V

    return-void
.end method

.method public synthetic lambda$performPrint$12$PrintSpooler(Lcom/squareup/print/HardwarePrinter;Lcom/squareup/print/PrintJob;Lcom/squareup/print/PrintTimingData;Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x1

    .line 443
    invoke-direct {p0, p1, v0}, Lcom/squareup/print/PrintSpooler;->updateListenersAfterPrinterActivity(Lcom/squareup/print/HardwarePrinter;Z)V

    .line 447
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v1

    iget-boolean v1, v1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->supportsRasterMode:Z

    if-eqz v1, :cond_0

    .line 448
    iget-object v1, p0, Lcom/squareup/print/PrintSpooler;->printRenderer:Lcom/squareup/print/PayloadRenderer;

    invoke-virtual {p2}, Lcom/squareup/print/PrintJob;->getPayload()Lcom/squareup/print/PrintablePayload;

    move-result-object v2

    .line 449
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getBitmapRenderingSpecs()Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;

    move-result-object v3

    .line 448
    invoke-interface {v1, v2, v3, p2}, Lcom/squareup/print/PayloadRenderer;->renderBitmap(Lcom/squareup/print/PrintablePayload;Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;Lcom/squareup/print/PrintJob;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 450
    invoke-virtual {p1, v1, p3}, Lcom/squareup/print/HardwarePrinter;->performPrint(Landroid/graphics/Bitmap;Lcom/squareup/print/PrintTimingData;)Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object v1

    goto :goto_0

    .line 451
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v1

    iget-boolean v1, v1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->supportsTextMode:Z

    if-eqz v1, :cond_1

    .line 452
    iget-object v1, p0, Lcom/squareup/print/PrintSpooler;->printRenderer:Lcom/squareup/print/PayloadRenderer;

    invoke-virtual {p2}, Lcom/squareup/print/PrintJob;->getPayload()Lcom/squareup/print/PrintablePayload;

    move-result-object v2

    .line 453
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getTextRenderingSpecs()Lcom/squareup/print/HardwarePrinter$TextRenderingSpecs;

    move-result-object v3

    iget v3, v3, Lcom/squareup/print/HardwarePrinter$TextRenderingSpecs;->maxLineLength:I

    .line 452
    invoke-interface {v1, v2, p2, v3}, Lcom/squareup/print/PayloadRenderer;->renderText(Lcom/squareup/print/PrintablePayload;Lcom/squareup/print/PrintJob;I)Lcom/squareup/print/text/RenderedRows;

    move-result-object v1

    .line 454
    invoke-virtual {p1, v1, p3}, Lcom/squareup/print/HardwarePrinter;->performPrint(Lcom/squareup/print/text/RenderedRows;Lcom/squareup/print/PrintTimingData;)Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object v1

    :goto_0
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p4, v2, v3

    .line 460
    invoke-virtual {p2}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object p4

    aput-object p4, v2, v0

    const/4 p4, 0x2

    aput-object p3, v2, p4

    const-string p3, "[PrintSpooler_performPrint] HardwarePrinter %s printed job %s\nPrint timing: %s"

    .line 459
    invoke-static {p3, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 463
    invoke-virtual {p2, v1}, Lcom/squareup/print/PrintJob;->setPrintAttempt(Lcom/squareup/print/PrintJob$PrintAttempt;)V

    .line 464
    invoke-direct {p0, p1, v3}, Lcom/squareup/print/PrintSpooler;->updateListenersAfterPrinterActivity(Lcom/squareup/print/HardwarePrinter;Z)V

    return-void

    .line 456
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Printer does not support bitmap or text modes."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic lambda$performPrint$13$PrintSpooler(Lcom/squareup/print/HardwarePrinter;Lcom/squareup/print/PrintJob;)V
    .locals 1

    .line 467
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->hardwarePrinterExecutor:Lcom/squareup/print/HardwarePrinterExecutor;

    invoke-interface {v0, p1}, Lcom/squareup/print/HardwarePrinterExecutor;->releaseHardwarePrinter(Lcom/squareup/print/HardwarePrinter;)V

    .line 470
    invoke-virtual {p0, p2}, Lcom/squareup/print/PrintSpooler;->updateQueueAfterPrintAttempt(Lcom/squareup/print/PrintJob;)V

    return-void
.end method

.method public synthetic lambda$updateQueueAfterPrintAttempt$15$PrintSpooler(Lcom/squareup/print/PrintJob;)V
    .locals 1

    .line 493
    invoke-direct {p0, p1}, Lcom/squareup/print/PrintSpooler;->unlockPrintJob(Lcom/squareup/print/PrintJob;)V

    .line 496
    invoke-direct {p0, p1}, Lcom/squareup/print/PrintSpooler;->updateListenersAfterQueueUpdated(Lcom/squareup/print/PrintJob;)V

    .line 497
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->blockedPrinterLogRunner:Lcom/squareup/print/BlockedPrinterLogRunner;

    invoke-interface {v0, p1}, Lcom/squareup/print/BlockedPrinterLogRunner;->onJobFinished(Lcom/squareup/print/PrintJob;)V

    .line 500
    invoke-virtual {p0}, Lcom/squareup/print/PrintSpooler;->checkQueues()V

    return-void
.end method

.method performOpenCashDrawer(Lcom/squareup/print/HardwarePrinter;)V
    .locals 4

    .line 335
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 336
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object v0

    .line 338
    iget-object v1, p0, Lcom/squareup/print/PrintSpooler;->hardwarePrinterExecutor:Lcom/squareup/print/HardwarePrinterExecutor;

    new-instance v2, Lcom/squareup/print/-$$Lambda$PrintSpooler$WcGeUlYwoNbYCnWyvELMd1J257k;

    invoke-direct {v2, p1, v0}, Lcom/squareup/print/-$$Lambda$PrintSpooler$WcGeUlYwoNbYCnWyvELMd1J257k;-><init>(Lcom/squareup/print/HardwarePrinter;Ljava/lang/String;)V

    new-instance v3, Lcom/squareup/print/-$$Lambda$PrintSpooler$Shi3IMXEAMWQCYKe8q_d0dK_LWw;

    invoke-direct {v3, p0, v0, p1}, Lcom/squareup/print/-$$Lambda$PrintSpooler$Shi3IMXEAMWQCYKe8q_d0dK_LWw;-><init>(Lcom/squareup/print/PrintSpooler;Ljava/lang/String;Lcom/squareup/print/HardwarePrinter;)V

    invoke-interface {v1, p1, v2, v3}, Lcom/squareup/print/HardwarePrinterExecutor;->claimHardwarePrinterAndExecute(Lcom/squareup/print/HardwarePrinter;Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    return-void
.end method

.method public purgeFailedJobs(Ljava/util/Collection;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrintJob;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 252
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->queueExecutor:Lcom/squareup/print/PrintQueueExecutor;

    new-instance v1, Lcom/squareup/print/-$$Lambda$PrintSpooler$HSRrptXVCD_hk5zX_jxG1kz2Rdg;

    invoke-direct {v1, p1, p2}, Lcom/squareup/print/-$$Lambda$PrintSpooler$HSRrptXVCD_hk5zX_jxG1kz2Rdg;-><init>(Ljava/util/Collection;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/print/PrintQueueExecutor;->execute(Lcom/squareup/print/PrintQueueExecutor$PrintWork;)V

    return-void
.end method

.method public removePrintJobStatusListener(Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;)V
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->statusListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public removePrinterActivityListener(Lcom/squareup/print/PrintSpooler$PrinterActivityListener;)V
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->printerActivityListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method updateQueueAfterPrintAttempt(Lcom/squareup/print/PrintJob;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 479
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->generateStatusDebugText()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "[updateStatus] %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 480
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 483
    iget-object v0, p0, Lcom/squareup/print/PrintSpooler;->queueExecutor:Lcom/squareup/print/PrintQueueExecutor;

    new-instance v1, Lcom/squareup/print/-$$Lambda$PrintSpooler$F_D_kqfiRuvEmGhvkId6TMiFgMo;

    invoke-direct {v1, p1}, Lcom/squareup/print/-$$Lambda$PrintSpooler$F_D_kqfiRuvEmGhvkId6TMiFgMo;-><init>(Lcom/squareup/print/PrintJob;)V

    new-instance v2, Lcom/squareup/print/-$$Lambda$PrintSpooler$8cMufDSCYtw-ciDwmyxaElr9up0;

    invoke-direct {v2, p0, p1}, Lcom/squareup/print/-$$Lambda$PrintSpooler$8cMufDSCYtw-ciDwmyxaElr9up0;-><init>(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/PrintJob;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/print/PrintQueueExecutor;->execute(Lcom/squareup/print/PrintQueueExecutor$PrintWork;Ljava/lang/Runnable;)V

    return-void
.end method
