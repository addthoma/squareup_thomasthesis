.class public Lcom/squareup/print/StarMicronicsPrinter;
.super Lcom/squareup/print/HardwarePrinter;
.source "StarMicronicsPrinter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/StarMicronicsPrinter$Factory;,
        Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;,
        Lcom/squareup/print/StarMicronicsPrinter$StarGetPortResult;
    }
.end annotation


# static fields
.field private static final DEFAULT_PRINTER_COMMAND_TIMEOUT_MS:I

.field private static final GENERIC_STAR_ERROR_MESSAGE:Ljava/lang/String; = "Generic Star printing failure"

.field private static final GET_PORT_RETRY_DELAYS_MS:[I

.field protected static final MANUFACTURER:Ljava/lang/String; = "Star"

.field private static final OPEN_CASH_DRAWERS_BYTES:[B

.field private static final PRINTER_BLOCKED_TIMEOUT_MS:J

.field private static final PRINTER_STATUS_TIMEOUT_MS:I

.field private static final STAR_PRINTER_OFFLINE_EXCEPTION_MESSAGE:Ljava/lang/String; = "Printer is off line"

.field private static final STAR_TIMEOUT_EXCEPTION_MESSAGE:Ljava/lang/String; = "There was no response of the printer within the timeout period."


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final context:Landroid/content/Context;

.field private final starPortName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 131
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3c

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/squareup/print/StarMicronicsPrinter;->PRINTER_BLOCKED_TIMEOUT_MS:J

    .line 134
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0xf

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v1, v0

    sput v1, Lcom/squareup/print/StarMicronicsPrinter;->DEFAULT_PRINTER_COMMAND_TIMEOUT_MS:I

    .line 137
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1e

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v1, v0

    sput v1, Lcom/squareup/print/StarMicronicsPrinter;->PRINTER_STATUS_TIMEOUT_MS:I

    const/4 v0, 0x6

    new-array v0, v0, [I

    .line 143
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    .line 144
    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    long-to-int v2, v1

    const/4 v1, 0x0

    aput v2, v0, v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x3

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    long-to-int v3, v2

    const/4 v2, 0x1

    aput v3, v0, v2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x5

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    long-to-int v3, v2

    const/4 v2, 0x2

    aput v3, v0, v2

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x8

    .line 145
    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v3

    long-to-int v4, v3

    const/4 v3, 0x3

    aput v4, v0, v3

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0xa

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v3

    long-to-int v4, v3

    const/4 v3, 0x4

    aput v4, v0, v3

    const/4 v3, 0x5

    aput v1, v0, v3

    sput-object v0, Lcom/squareup/print/StarMicronicsPrinter;->GET_PORT_RETRY_DELAYS_MS:[I

    new-array v0, v2, [B

    .line 149
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/print/StarMicronicsPrinter;->OPEN_CASH_DRAWERS_BYTES:[B

    return-void

    :array_0
    .array-data 1
        0x7t
        0x1at
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/ConnectionType;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/squareup/util/Clock;)V
    .locals 10

    move-object v9, p0

    .line 169
    invoke-static {p2}, Lcom/squareup/print/StarMicronicsPrinter;->getCleanModelName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, Lcom/squareup/print/StarMicronicsPrinters;->modelSupportsRasterMode(Ljava/lang/String;)Z

    move-result v4

    .line 170
    invoke-static {p2}, Lcom/squareup/print/StarMicronicsPrinters;->modelSupportsTextMode(Ljava/lang/String;)Z

    move-result v5

    invoke-static {p2}, Lcom/squareup/print/StarMicronicsPrinter;->getBitmapRenderingSpecs(Ljava/lang/String;)Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;

    move-result-object v7

    new-instance v8, Lcom/squareup/print/HardwarePrinter$TextRenderingSpecs;

    const/16 v0, 0x15

    invoke-direct {v8, v0}, Lcom/squareup/print/HardwarePrinter$TextRenderingSpecs;-><init>(I)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v6, p4

    .line 169
    invoke-direct/range {v0 .. v8}, Lcom/squareup/print/HardwarePrinter;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/ConnectionType;ZZLjava/lang/String;Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;Lcom/squareup/print/HardwarePrinter$TextRenderingSpecs;)V

    move-object/from16 v0, p6

    .line 172
    iput-object v0, v9, Lcom/squareup/print/StarMicronicsPrinter;->context:Landroid/content/Context;

    move-object v0, p5

    .line 173
    iput-object v0, v9, Lcom/squareup/print/StarMicronicsPrinter;->starPortName:Ljava/lang/String;

    move-object/from16 v0, p7

    .line 174
    iput-object v0, v9, Lcom/squareup/print/StarMicronicsPrinter;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method private static classifyResult(Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;)Lcom/squareup/print/PrintJob$PrintAttempt$Result;
    .locals 2

    .line 372
    iget-boolean v0, p0, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->timedOut:Z

    if-eqz v0, :cond_0

    .line 373
    sget-object p0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->UNRELIABLE_FAILURE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    return-object p0

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->status:Lcom/starmicronics/stario/StarPrinterStatus;

    if-nez v0, :cond_1

    .line 380
    sget-object p0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->UNRELIABLE_FAILURE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    return-object p0

    .line 383
    :cond_1
    iget-boolean v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->cutterError:Z

    if-eqz v1, :cond_2

    sget-object p0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_CUTTER_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    return-object p0

    .line 384
    :cond_2
    iget-boolean v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->presenterPaperJamError:Z

    if-eqz v1, :cond_3

    sget-object p0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_PAPER_JAM_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    return-object p0

    .line 385
    :cond_3
    iget-boolean v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->receiveBufferOverflow:Z

    if-eqz v1, :cond_4

    sget-object p0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_RECEIVE_BUFFER_OVERFLOW_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    return-object p0

    .line 386
    :cond_4
    iget-boolean v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->coverOpen:Z

    if-eqz v1, :cond_5

    sget-object p0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_COVER_OPEN:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    return-object p0

    .line 387
    :cond_5
    iget-boolean v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperEmpty:Z

    if-eqz v1, :cond_6

    sget-object p0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_PAPER_EMPTY:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    return-object p0

    .line 392
    :cond_6
    iget-boolean v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    if-eqz v1, :cond_7

    sget-object p0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_OFFLINE_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    return-object p0

    .line 394
    :cond_7
    iget-boolean v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->mechError:Z

    if-nez v1, :cond_b

    iget-boolean v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->headThermistorError:Z

    if-nez v1, :cond_b

    iget-boolean v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->headUpError:Z

    if-eqz v1, :cond_8

    goto :goto_0

    .line 399
    :cond_8
    iget-boolean v0, v0, Lcom/starmicronics/stario/StarPrinterStatus;->unrecoverableError:Z

    if-eqz v0, :cond_9

    .line 400
    sget-object p0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_UNRECOVERABLE_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    return-object p0

    .line 404
    :cond_9
    iget-object p0, p0, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->errorMessage:Ljava/lang/String;

    if-nez p0, :cond_a

    .line 405
    sget-object p0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->SUCCESS:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    return-object p0

    .line 409
    :cond_a
    sget-object p0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->UNRELIABLE_FAILURE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    return-object p0

    .line 395
    :cond_b
    :goto_0
    sget-object p0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_MECH_ERROR:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    return-object p0
.end method

.method private static getBitmapRenderingSpecs(Ljava/lang/String;)Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;
    .locals 2

    .line 413
    invoke-static {p0}, Lcom/squareup/print/StarMicronicsPrinters;->modelPaperWidth(Ljava/lang/String;)Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;

    move-result-object p0

    .line 414
    new-instance v0, Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;

    invoke-virtual {p0}, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;->getDotsPerLine()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/print/StarMicronicsPrinters$PaperWidth;->getDotsPerInch()I

    move-result p0

    invoke-direct {v0, v1, p0}, Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;-><init>(II)V

    return-object v0
.end method

.method static getCleanModelName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, " "

    .line 418
    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    aget-object p0, p0, v0

    return-object p0
.end method

.method private static getPort(Ljava/lang/String;Landroid/content/Context;Lcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;)Lcom/squareup/print/StarMicronicsPrinter$StarGetPortResult;
    .locals 10

    .line 348
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 353
    sget-object v2, Lcom/squareup/print/StarMicronicsPrinter;->GET_PORT_RETRY_DELAYS_MS:[I

    array-length v3, v2

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v3, :cond_0

    aget v7, v2, v6

    .line 354
    invoke-virtual {p2, p3}, Lcom/squareup/print/PrintTimingData;->connecting(Lcom/squareup/util/Clock;)V

    :try_start_0
    const-string v8, ""

    .line 356
    sget v9, Lcom/squareup/print/StarMicronicsPrinter;->DEFAULT_PRINTER_COMMAND_TIMEOUT_MS:I

    invoke-static {p0, v8, v9, p1}, Lcom/starmicronics/stario/StarIOPort;->getPort(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v5

    .line 357
    sget v8, Lcom/squareup/print/StarMicronicsPrinter;->PRINTER_STATUS_TIMEOUT_MS:I

    invoke-virtual {v5, v8}, Lcom/starmicronics/stario/StarIOPort;->setEndCheckedBlockTimeoutMillis(I)V
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v8

    .line 360
    invoke-static {v8}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;)V

    int-to-long v7, v7

    .line 362
    invoke-static {v7, v8}, Landroid/os/SystemClock;->sleep(J)V

    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 367
    :cond_0
    :goto_1
    new-instance p0, Lcom/squareup/print/StarMicronicsPrinter$StarGetPortResult;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide p1

    sub-long/2addr p1, v0

    invoke-direct {p0, v5, v4, p1, p2}, Lcom/squareup/print/StarMicronicsPrinter$StarGetPortResult;-><init>(Lcom/starmicronics/stario/StarIOPort;IJ)V

    return-object p0
.end method

.method private getPrintableBytes(Landroid/graphics/Bitmap;)[[B
    .locals 9
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 261
    new-instance v8, Lcom/squareup/print/util/RasterDocument;

    sget-object v1, Lcom/squareup/print/util/RasterDocument$RasSpeed;->MEDIUM:Lcom/squareup/print/util/RasterDocument$RasSpeed;

    sget-object v2, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->FEED_AND_FULL_CUT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    sget-object v3, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->FEED_AND_FULL_CUT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    sget-object v4, Lcom/squareup/print/util/RasterDocument$RasTopMargin;->STANDARD:Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/print/util/RasterDocument;-><init>(Lcom/squareup/print/util/RasterDocument$RasSpeed;Lcom/squareup/print/util/RasterDocument$RasPageEndMode;Lcom/squareup/print/util/RasterDocument$RasPageEndMode;Lcom/squareup/print/util/RasterDocument$RasTopMargin;III)V

    .line 263
    new-instance v0, Lcom/squareup/print/util/StarBitmap;

    invoke-virtual {p0}, Lcom/squareup/print/StarMicronicsPrinter;->getBitmapRenderingSpecs()Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;

    move-result-object v1

    iget v1, v1, Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;->dotsPerLine:I

    invoke-direct {v0, p1, v1}, Lcom/squareup/print/util/StarBitmap;-><init>(Landroid/graphics/Bitmap;I)V

    const/4 p1, 0x4

    new-array p1, p1, [[B

    .line 265
    invoke-virtual {v8}, Lcom/squareup/print/util/RasterDocument;->beginDocumentCommandData()[B

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, p1, v2

    .line 266
    invoke-virtual {v0}, Lcom/squareup/print/util/StarBitmap;->getImageRasterDataForPrinting()[B

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, p1, v1

    sget-object v0, Lcom/squareup/util/Strings;->US_ASCII:Ljava/nio/charset/Charset;

    const-string v1, "\u001b*rY10\u0000"

    .line 269
    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    const/4 v1, 0x2

    aput-object v0, p1, v1

    .line 271
    invoke-virtual {v8}, Lcom/squareup/print/util/RasterDocument;->endDocumentCommandData()[B

    move-result-object v0

    const/4 v1, 0x3

    aput-object v0, p1, v1

    return-object p1
.end method

.method private getPrintableBytesWithStarLib(Landroid/graphics/Bitmap;)[[B
    .locals 2

    .line 249
    sget-object v0, Lcom/starmicronics/starioextension/StarIoExt$Emulation;->StarPRNT:Lcom/starmicronics/starioextension/StarIoExt$Emulation;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExt;->createCommandBuilder(Lcom/starmicronics/starioextension/StarIoExt$Emulation;)Lcom/starmicronics/starioextension/ICommandBuilder;

    move-result-object v0

    .line 250
    invoke-interface {v0}, Lcom/starmicronics/starioextension/ICommandBuilder;->beginDocument()V

    const/4 v1, 0x0

    .line 251
    invoke-interface {v0, p1, v1}, Lcom/starmicronics/starioextension/ICommandBuilder;->appendBitmap(Landroid/graphics/Bitmap;Z)V

    .line 252
    sget-object p1, Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;->FullCutWithFeed:Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;

    invoke-interface {v0, p1}, Lcom/starmicronics/starioextension/ICommandBuilder;->appendCutPaper(Lcom/starmicronics/starioextension/ICommandBuilder$CutPaperAction;)V

    .line 253
    invoke-interface {v0}, Lcom/starmicronics/starioextension/ICommandBuilder;->endDocument()V

    const/4 p1, 0x1

    new-array p1, p1, [[B

    .line 255
    invoke-interface {v0}, Lcom/starmicronics/starioextension/ICommandBuilder;->getCommands()[B

    move-result-object v0

    aput-object v0, p1, v1

    return-object p1
.end method

.method private varargs sendCommandsToPrinter(Landroid/content/Context;Ljava/lang/String;ZLcom/squareup/print/PrintTimingData;[[B)Lcom/squareup/print/PrintJob$PrintAttempt;
    .locals 9

    const-string v0, "Don\'t talk to printer from main thread, you naughty person."

    .line 209
    invoke-static {v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkNotMainThread(Ljava/lang/String;)V

    .line 211
    iget-object v0, p0, Lcom/squareup/print/StarMicronicsPrinter;->clock:Lcom/squareup/util/Clock;

    invoke-static {p2, p1, p4, v0}, Lcom/squareup/print/StarMicronicsPrinter;->getPort(Ljava/lang/String;Landroid/content/Context;Lcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;)Lcom/squareup/print/StarMicronicsPrinter$StarGetPortResult;

    move-result-object p1

    .line 212
    iget-object v0, p1, Lcom/squareup/print/StarMicronicsPrinter$StarGetPortResult;->port:Lcom/starmicronics/stario/StarIOPort;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 213
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p5, "Port is null, returning early "

    invoke-virtual {p3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    new-array p3, v1, [Ljava/lang/Object;

    invoke-static {p2, p3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 215
    iget-object p2, p0, Lcom/squareup/print/StarMicronicsPrinter;->clock:Lcom/squareup/util/Clock;

    invoke-virtual {p4, p2}, Lcom/squareup/print/PrintTimingData;->finished(Lcom/squareup/util/Clock;)V

    .line 217
    new-instance p2, Lcom/squareup/print/PrintJob$PrintAttempt;

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->PRINTER_BUSY_FAILURE:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    invoke-virtual {p0}, Lcom/squareup/print/StarMicronicsPrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v2

    iget v3, p1, Lcom/squareup/print/StarMicronicsPrinter$StarGetPortResult;->attempts:I

    iget-wide v4, p1, Lcom/squareup/print/StarMicronicsPrinter$StarGetPortResult;->totalTimeMs:J

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p2

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/squareup/print/PrintJob$PrintAttempt;-><init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Lcom/squareup/print/HardwarePrinter$HardwareInfo;IJLjava/lang/String;Ljava/lang/String;Lcom/squareup/print/PrintTimingData;)V

    return-object p2

    :cond_0
    const-wide/16 v2, 0x64

    .line 222
    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    .line 225
    iget-object p2, p1, Lcom/squareup/print/StarMicronicsPrinter$StarGetPortResult;->port:Lcom/starmicronics/stario/StarIOPort;

    iget-object v0, p0, Lcom/squareup/print/StarMicronicsPrinter;->clock:Lcom/squareup/util/Clock;

    .line 226
    invoke-static {p2, p3, p4, v0, p5}, Lcom/squareup/print/StarMicronicsPrinter;->sendRawBytesToPort(Lcom/starmicronics/stario/StarIOPort;ZLcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;[[B)Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;

    move-result-object p2

    .line 229
    :try_start_0
    iget-object p3, p1, Lcom/squareup/print/StarMicronicsPrinter$StarGetPortResult;->port:Lcom/starmicronics/stario/StarIOPort;

    invoke-static {p3}, Lcom/starmicronics/stario/StarIOPort;->releasePort(Lcom/starmicronics/stario/StarIOPort;)V
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p3

    .line 231
    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Failed to release port: "

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/squareup/print/StarMicronicsPrinter$StarGetPortResult;->port:Lcom/starmicronics/stario/StarIOPort;

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " "

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    new-array p5, v1, [Ljava/lang/Object;

    invoke-static {p3, p5}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/Object;

    .line 234
    invoke-virtual {p2}, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->toString()Ljava/lang/String;

    move-result-object p5

    aput-object p5, p3, v1

    const-string p5, "Send commands to printer result: %s"

    invoke-static {p5, p3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 236
    invoke-static {p2}, Lcom/squareup/print/StarMicronicsPrinter;->classifyResult(Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;)Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    move-result-object v1

    .line 238
    iget-object p3, p0, Lcom/squareup/print/StarMicronicsPrinter;->clock:Lcom/squareup/util/Clock;

    invoke-virtual {p4, p3}, Lcom/squareup/print/PrintTimingData;->finished(Lcom/squareup/util/Clock;)V

    .line 240
    new-instance p3, Lcom/squareup/print/PrintJob$PrintAttempt;

    invoke-virtual {p0}, Lcom/squareup/print/StarMicronicsPrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v2

    iget v3, p1, Lcom/squareup/print/StarMicronicsPrinter$StarGetPortResult;->attempts:I

    iget-wide v4, p1, Lcom/squareup/print/StarMicronicsPrinter$StarGetPortResult;->totalTimeMs:J

    iget-object v6, p2, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->errorMessage:Ljava/lang/String;

    .line 241
    invoke-virtual {p2}, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v0, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/squareup/print/PrintJob$PrintAttempt;-><init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Lcom/squareup/print/HardwarePrinter$HardwareInfo;IJLjava/lang/String;Ljava/lang/String;Lcom/squareup/print/PrintTimingData;)V

    return-object p3
.end method

.method private static varargs sendRawBytesToPort(Lcom/starmicronics/stario/StarIOPort;ZLcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;[[B)Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;
    .locals 8

    const-string v0, "Generic Star printing failure"

    const-string v1, "port must not be null!"

    .line 278
    invoke-static {p0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 280
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    .line 281
    new-instance v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;-><init>(Lcom/squareup/print/StarMicronicsPrinter$1;)V

    .line 282
    iput-boolean p1, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->usedCheckedBlock:Z

    const/4 v4, 0x1

    .line 285
    :try_start_0
    invoke-virtual {p0}, Lcom/starmicronics/stario/StarIOPort;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v5

    .line 286
    iget-boolean v6, v5, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    if-eqz v6, :cond_0

    .line 287
    iput-boolean v4, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->offline:Z

    .line 288
    iput-object v5, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->status:Lcom/starmicronics/stario/StarPrinterStatus;

    return-object v3

    :cond_0
    if-eqz p1, :cond_1

    .line 293
    invoke-virtual {p0}, Lcom/starmicronics/stario/StarIOPort;->beginCheckedBlock()Lcom/starmicronics/stario/StarPrinterStatus;

    .line 294
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    sub-long/2addr v5, v1

    iput-wide v5, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->beginCheckedBlockTime:J

    .line 297
    :cond_1
    invoke-virtual {p2, p3}, Lcom/squareup/print/PrintTimingData;->sendingDataToPrinter(Lcom/squareup/util/Clock;)V

    .line 299
    array-length p2, p4

    const/4 p3, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, p2, :cond_2

    aget-object v6, p4, v5

    .line 300
    array-length v7, v6

    invoke-virtual {p0, v6, p3, v7}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 303
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide p2

    sub-long/2addr p2, v1

    iput-wide p2, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->writeToPortTime:J

    if-eqz p1, :cond_3

    .line 306
    invoke-virtual {p0}, Lcom/starmicronics/stario/StarIOPort;->endCheckedBlock()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object p0

    iput-object p0, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->status:Lcom/starmicronics/stario/StarPrinterStatus;

    .line 307
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide p0

    sub-long/2addr p0, v1

    iput-wide p0, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->endCheckedBlockTime:J

    goto :goto_1

    .line 309
    :cond_3
    invoke-virtual {p0}, Lcom/starmicronics/stario/StarIOPort;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object p0

    iput-object p0, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->status:Lcom/starmicronics/stario/StarPrinterStatus;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    .line 329
    invoke-static {p0}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;)V

    .line 330
    invoke-virtual {p0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->errorMessage:Ljava/lang/String;

    .line 331
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide p0

    sub-long/2addr p0, v1

    iput-wide p0, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->exceptionTime:J

    .line 333
    iget-object p0, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->errorMessage:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 334
    iput-object v0, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->errorMessage:Ljava/lang/String;

    goto :goto_1

    .line 335
    :cond_4
    iget-object p0, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->errorMessage:Ljava/lang/String;

    const-string p1, "There was no response of the printer within the timeout period."

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 336
    iput-boolean v4, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->timedOut:Z

    goto :goto_1

    .line 337
    :cond_5
    iget-object p0, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->errorMessage:Ljava/lang/String;

    const-string p1, "Printer is off line"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 338
    iput-boolean v4, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->offline:Z

    goto :goto_1

    :catch_1
    move-exception p0

    .line 324
    invoke-static {p0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 325
    invoke-static {p0}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;)V

    .line 326
    iput-object v0, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->errorMessage:Ljava/lang/String;

    .line 327
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide p0

    sub-long/2addr p0, v1

    iput-wide p0, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->exceptionTime:J

    .line 342
    :cond_6
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide p0

    sub-long/2addr p0, v1

    iput-wide p0, v3, Lcom/squareup/print/StarMicronicsPrinter$SendBytesResult;->finishTime:J

    return-object v3
.end method


# virtual methods
.method public performOpenCashDrawer()V
    .locals 6

    .line 179
    iget-object v1, p0, Lcom/squareup/print/StarMicronicsPrinter;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/squareup/print/StarMicronicsPrinter;->starPortName:Ljava/lang/String;

    new-instance v4, Lcom/squareup/print/PrintTimingData;

    invoke-direct {v4}, Lcom/squareup/print/PrintTimingData;-><init>()V

    const/4 v0, 0x1

    new-array v5, v0, [[B

    sget-object v0, Lcom/squareup/print/StarMicronicsPrinter;->OPEN_CASH_DRAWERS_BYTES:[B

    const/4 v3, 0x0

    aput-object v0, v5, v3

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/StarMicronicsPrinter;->sendCommandsToPrinter(Landroid/content/Context;Ljava/lang/String;ZLcom/squareup/print/PrintTimingData;[[B)Lcom/squareup/print/PrintJob$PrintAttempt;

    return-void
.end method

.method public performPrint(Landroid/graphics/Bitmap;Lcom/squareup/print/PrintTimingData;)Lcom/squareup/print/PrintJob$PrintAttempt;
    .locals 6

    .line 189
    invoke-virtual {p0}, Lcom/squareup/print/StarMicronicsPrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->model:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/print/StarMicronicsPrinters;->useStarExtensionLib(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 190
    invoke-direct {p0, p1}, Lcom/squareup/print/StarMicronicsPrinter;->getPrintableBytesWithStarLib(Landroid/graphics/Bitmap;)[[B

    move-result-object p1

    goto :goto_0

    .line 192
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/print/StarMicronicsPrinter;->getPrintableBytes(Landroid/graphics/Bitmap;)[[B

    move-result-object p1

    :goto_0
    move-object v5, p1

    .line 195
    iget-object v1, p0, Lcom/squareup/print/StarMicronicsPrinter;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/squareup/print/StarMicronicsPrinter;->starPortName:Ljava/lang/String;

    const/4 v3, 0x1

    move-object v0, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/StarMicronicsPrinter;->sendCommandsToPrinter(Landroid/content/Context;Ljava/lang/String;ZLcom/squareup/print/PrintTimingData;[[B)Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object p1

    return-object p1
.end method

.method public performPrint(Lcom/squareup/print/text/RenderedRows;Lcom/squareup/print/PrintTimingData;)Lcom/squareup/print/PrintJob$PrintAttempt;
    .locals 6

    .line 201
    sget-object v0, Lcom/squareup/print/text/StarMicronicsRenderRowsUtil;->INSTANCE:Lcom/squareup/print/text/StarMicronicsRenderRowsUtil;

    .line 202
    invoke-virtual {v0, p1}, Lcom/squareup/print/text/StarMicronicsRenderRowsUtil;->mapRenderedRowsToString(Lcom/squareup/print/text/RenderedRows;)Ljava/lang/String;

    move-result-object p1

    sget-object v0, Lcom/squareup/util/Strings;->US_ASCII:Ljava/nio/charset/Charset;

    .line 203
    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    .line 204
    iget-object v1, p0, Lcom/squareup/print/StarMicronicsPrinter;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/squareup/print/StarMicronicsPrinter;->starPortName:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v5, v0, [[B

    const/4 v0, 0x0

    aput-object p1, v5, v0

    const/4 v3, 0x1

    move-object v0, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/StarMicronicsPrinter;->sendCommandsToPrinter(Landroid/content/Context;Ljava/lang/String;ZLcom/squareup/print/PrintTimingData;[[B)Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object p1

    return-object p1
.end method
