.class public final Lcom/squareup/print/ReceiptFormatter_Factory;
.super Ljava/lang/Object;
.source "ReceiptFormatter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/ReceiptFormatter;",
        ">;"
    }
.end annotation


# instance fields
.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final localeOverrideFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final phoneNumbersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final taxFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/print/ReceiptFormatter_Factory;->localeOverrideFactoryProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p2, p0, Lcom/squareup/print/ReceiptFormatter_Factory;->phoneNumbersProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p3, p0, Lcom/squareup/print/ReceiptFormatter_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/print/ReceiptFormatter_Factory;->taxFormatterProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p5, p0, Lcom/squareup/print/ReceiptFormatter_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p6, p0, Lcom/squareup/print/ReceiptFormatter_Factory;->countryCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/ReceiptFormatter_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)",
            "Lcom/squareup/print/ReceiptFormatter_Factory;"
        }
    .end annotation

    .line 58
    new-instance v7, Lcom/squareup/print/ReceiptFormatter_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/print/ReceiptFormatter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/Features;Lcom/squareup/CountryCode;)Lcom/squareup/print/ReceiptFormatter;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/CountryCode;",
            ")",
            "Lcom/squareup/print/ReceiptFormatter;"
        }
    .end annotation

    .line 64
    new-instance v7, Lcom/squareup/print/ReceiptFormatter;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/print/ReceiptFormatter;-><init>(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/Features;Lcom/squareup/CountryCode;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/print/ReceiptFormatter;
    .locals 7

    .line 49
    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter_Factory;->localeOverrideFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/locale/LocaleOverrideFactory;

    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter_Factory;->phoneNumbersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/text/PhoneNumberHelper;

    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter_Factory;->taxFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/print/ReceiptFormatter_Factory;->countryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/CountryCode;

    invoke-static/range {v1 .. v6}, Lcom/squareup/print/ReceiptFormatter_Factory;->newInstance(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/Features;Lcom/squareup/CountryCode;)Lcom/squareup/print/ReceiptFormatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/print/ReceiptFormatter_Factory;->get()Lcom/squareup/print/ReceiptFormatter;

    move-result-object v0

    return-object v0
.end method
