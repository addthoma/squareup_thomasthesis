.class public interface abstract Lcom/squareup/print/PrintableOrderItem;
.super Ljava/lang/Object;
.source "PrintableOrderItem.kt"

# interfaces
.implements Lcom/squareup/itemsorter/SortableItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/PrintableOrderItem$DefaultImpls;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/itemsorter/SortableItem<",
        "Lcom/squareup/print/PrintableOrderItem;",
        "Lcom/squareup/checkout/DiningOption;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u000b\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001J\u0010\u00105\u001a\u00020\u00042\u0006\u00106\u001a\u000207H&J\u0010\u00108\u001a\u00020\u001c2\u0006\u00106\u001a\u000207H&J\u0010\u00109\u001a\u00020\u00002\u0006\u0010!\u001a\u00020\u0002H\u0016J\u0010\u0010:\u001a\u00020\u00042\u0006\u00106\u001a\u000207H&R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u000cX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eR\u0012\u0010\u000f\u001a\u00020\u0010X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0011R\u0012\u0010\u0012\u001a\u00020\u0010X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0011R\u0012\u0010\u0013\u001a\u00020\u0010X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0011R\u0012\u0010\u0014\u001a\u00020\u0010X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0011R\u0012\u0010\u0015\u001a\u00020\u0010X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0011R\u0012\u0010\u0016\u001a\u00020\u0010X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0011R\u0014\u0010\u0017\u001a\u0004\u0018\u00010\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u0006R\u0014\u0010\u0019\u001a\u0004\u0018\u00010\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001a\u0010\u0006R\u0012\u0010\u001b\u001a\u00020\u001cX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001d\u0010\u001eR\u0012\u0010\u001f\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008 \u0010\u0006R\u0014\u0010!\u001a\u0004\u0018\u00010\u0002X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\"\u0010#R\u0018\u0010$\u001a\u0008\u0012\u0004\u0012\u00020&0%X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\'\u0010(R\u0014\u0010)\u001a\u0004\u0018\u00010\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008*\u0010\u0006R\u0012\u0010+\u001a\u00020\u0010X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008,\u0010\u0011R\u0012\u0010-\u001a\u00020.X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008/\u00100R\u0014\u00101\u001a\u0004\u0018\u00010.X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u00082\u00100R\u0012\u00103\u001a\u00020.X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u00084\u00100\u00a8\u0006;"
    }
    d2 = {
        "Lcom/squareup/print/PrintableOrderItem;",
        "Lcom/squareup/itemsorter/SortableItem;",
        "Lcom/squareup/checkout/DiningOption;",
        "categoryId",
        "",
        "getCategoryId",
        "()Ljava/lang/String;",
        "destination",
        "Lcom/squareup/checkout/OrderDestination;",
        "getDestination",
        "()Lcom/squareup/checkout/OrderDestination;",
        "idPair",
        "Lcom/squareup/protos/client/IdPair;",
        "getIdPair",
        "()Lcom/squareup/protos/client/IdPair;",
        "isComped",
        "",
        "()Z",
        "isDownConvertedCustomAmount",
        "isTaxed",
        "isUncategorized",
        "isUnitPriced",
        "isVoided",
        "itemName",
        "getItemName",
        "notes",
        "getNotes",
        "quantityAsInt",
        "",
        "getQuantityAsInt",
        "()I",
        "quantityAsString",
        "getQuantityAsString",
        "selectedDiningOption",
        "getSelectedDiningOption",
        "()Lcom/squareup/checkout/DiningOption;",
        "selectedModifiers",
        "",
        "Lcom/squareup/print/PrintableOrderItemModifier;",
        "getSelectedModifiers",
        "()Ljava/util/List;",
        "selectedVariationDisplayName",
        "getSelectedVariationDisplayName",
        "shouldShowVariationName",
        "getShouldShowVariationName",
        "total",
        "Lcom/squareup/protos/common/Money;",
        "getTotal",
        "()Lcom/squareup/protos/common/Money;",
        "unitPrice",
        "getUnitPrice",
        "unitPriceWithModifiers",
        "getUnitPriceWithModifiers",
        "quantityEntrySuffix",
        "res",
        "Lcom/squareup/util/Res;",
        "quantityPrecision",
        "setSelectedDiningOption",
        "unitAbbreviation",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getCategoryId()Ljava/lang/String;
.end method

.method public abstract getDestination()Lcom/squareup/checkout/OrderDestination;
.end method

.method public abstract getIdPair()Lcom/squareup/protos/client/IdPair;
.end method

.method public abstract getItemName()Ljava/lang/String;
.end method

.method public abstract getNotes()Ljava/lang/String;
.end method

.method public abstract getQuantityAsInt()I
.end method

.method public abstract getQuantityAsString()Ljava/lang/String;
.end method

.method public abstract getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;
.end method

.method public abstract getSelectedModifiers()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItemModifier;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSelectedVariationDisplayName()Ljava/lang/String;
.end method

.method public abstract getShouldShowVariationName()Z
.end method

.method public abstract getTotal()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getUnitPrice()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getUnitPriceWithModifiers()Lcom/squareup/protos/common/Money;
.end method

.method public abstract isComped()Z
.end method

.method public abstract isDownConvertedCustomAmount()Z
.end method

.method public abstract isTaxed()Z
.end method

.method public abstract isUncategorized()Z
.end method

.method public abstract isUnitPriced()Z
.end method

.method public abstract isVoided()Z
.end method

.method public abstract quantityEntrySuffix(Lcom/squareup/util/Res;)Ljava/lang/String;
.end method

.method public abstract quantityPrecision(Lcom/squareup/util/Res;)I
.end method

.method public abstract setSelectedDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/print/PrintableOrderItem;
.end method

.method public abstract unitAbbreviation(Lcom/squareup/util/Res;)Ljava/lang/String;
.end method
