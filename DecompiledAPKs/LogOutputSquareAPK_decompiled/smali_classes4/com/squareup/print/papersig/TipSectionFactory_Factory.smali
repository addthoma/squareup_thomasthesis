.class public final Lcom/squareup/print/papersig/TipSectionFactory_Factory;
.super Ljava/lang/Object;
.source "TipSectionFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/papersig/TipSectionFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final historicalTippingCalculatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;",
            ">;"
        }
    .end annotation
.end field

.field private final paperSignatureSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/print/papersig/TipSectionFactory_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/print/papersig/TipSectionFactory_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/print/papersig/TipSectionFactory_Factory;->historicalTippingCalculatorProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/print/papersig/TipSectionFactory_Factory;->transactionProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/papersig/TipSectionFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)",
            "Lcom/squareup/print/papersig/TipSectionFactory_Factory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/print/papersig/TipSectionFactory_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/print/papersig/TipSectionFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;Lcom/squareup/payment/Transaction;)Lcom/squareup/print/papersig/TipSectionFactory;
    .locals 1

    .line 53
    new-instance v0, Lcom/squareup/print/papersig/TipSectionFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/print/papersig/TipSectionFactory;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;Lcom/squareup/payment/Transaction;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/print/papersig/TipSectionFactory;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/print/papersig/TipSectionFactory_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/print/papersig/TipSectionFactory_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/papersignature/PaperSignatureSettings;

    iget-object v2, p0, Lcom/squareup/print/papersig/TipSectionFactory_Factory;->historicalTippingCalculatorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;

    iget-object v3, p0, Lcom/squareup/print/papersig/TipSectionFactory_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/payment/Transaction;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/print/papersig/TipSectionFactory_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;Lcom/squareup/payment/Transaction;)Lcom/squareup/print/papersig/TipSectionFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/print/papersig/TipSectionFactory_Factory;->get()Lcom/squareup/print/papersig/TipSectionFactory;

    move-result-object v0

    return-object v0
.end method
