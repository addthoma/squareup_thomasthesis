.class public Lcom/squareup/print/papersig/TraditionalTipSection;
.super Ljava/lang/Object;
.source "TraditionalTipSection.java"


# instance fields
.field public final tipLabel:Ljava/lang/String;

.field public final totalLabel:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "tipLabel"

    .line 14
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/print/papersig/TraditionalTipSection;->tipLabel:Ljava/lang/String;

    const-string p1, "totalLabel"

    .line 15
    invoke-static {p2, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/print/papersig/TraditionalTipSection;->totalLabel:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_4

    .line 27
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_0

    .line 29
    :cond_1
    check-cast p1, Lcom/squareup/print/papersig/TraditionalTipSection;

    .line 31
    iget-object v2, p0, Lcom/squareup/print/papersig/TraditionalTipSection;->tipLabel:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/print/papersig/TraditionalTipSection;->tipLabel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    return v1

    .line 32
    :cond_2
    iget-object v2, p0, Lcom/squareup/print/papersig/TraditionalTipSection;->totalLabel:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/papersig/TraditionalTipSection;->totalLabel:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3

    return v1

    :cond_3
    return v0

    :cond_4
    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/squareup/print/papersig/TraditionalTipSection;->tipLabel:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 39
    iget-object v1, p0, Lcom/squareup/print/papersig/TraditionalTipSection;->totalLabel:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->EXTRA_LARGE:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->space(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 20
    iget-object v0, p0, Lcom/squareup/print/papersig/TraditionalTipSection;->tipLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->labelledWriteLineHalfWidth(Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 21
    sget-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->EXTRA_LARGE:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->space(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 22
    iget-object v0, p0, Lcom/squareup/print/papersig/TraditionalTipSection;->totalLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->labelledWriteLineHalfWidth(Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method
