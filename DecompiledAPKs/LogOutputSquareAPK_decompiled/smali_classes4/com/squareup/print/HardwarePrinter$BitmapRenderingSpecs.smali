.class public Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;
.super Ljava/lang/Object;
.source "HardwarePrinter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/HardwarePrinter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BitmapRenderingSpecs"
.end annotation


# instance fields
.field public final dotsPerInch:I

.field public final dotsPerLine:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput p1, p0, Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;->dotsPerLine:I

    .line 29
    iput p2, p0, Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;->dotsPerInch:I

    return-void
.end method
