.class public final Lcom/squareup/print/OrderPrintingDispatcher_Factory;
.super Ljava/lang/Object;
.source "OrderPrintingDispatcher_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/OrderPrintingDispatcher;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final billPayloadFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/TicketBillPayloadFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final historicalReceiptPayloadFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentReceiptPayloadFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final printSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final printSpoolerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final stubPayloadFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/StubPayload$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketPayloadFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/TicketPayload$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/TicketBillPayloadFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/StubPayload$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/TicketPayload$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSettings;",
            ">;)V"
        }
    .end annotation

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->printSpoolerProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p2, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p3, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->billPayloadFactoryProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p4, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->paymentReceiptPayloadFactoryProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p5, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->historicalReceiptPayloadFactoryProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p6, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->stubPayloadFactoryProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p7, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->ticketPayloadFactoryProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p8, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->resProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p9, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p10, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p11, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->contextProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p12, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->printSettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/OrderPrintingDispatcher_Factory;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/TicketBillPayloadFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/StubPayload$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/payload/TicketPayload$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSettings;",
            ">;)",
            "Lcom/squareup/print/OrderPrintingDispatcher_Factory;"
        }
    .end annotation

    .line 86
    new-instance v13, Lcom/squareup/print/OrderPrintingDispatcher_Factory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/print/OrderPrintingDispatcher_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v13
.end method

.method public static newInstance(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/payload/TicketBillPayloadFactory;Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;Lcom/squareup/print/payload/StubPayload$Factory;Lcom/squareup/print/payload/TicketPayload$Factory;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Ljavax/inject/Provider;Landroid/app/Application;Lcom/squareup/print/PrintSettings;)Lcom/squareup/print/OrderPrintingDispatcher;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrintSpooler;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/print/payload/TicketBillPayloadFactory;",
            "Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;",
            "Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;",
            "Lcom/squareup/print/payload/StubPayload$Factory;",
            "Lcom/squareup/print/payload/TicketPayload$Factory;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/analytics/Analytics;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Landroid/app/Application;",
            "Lcom/squareup/print/PrintSettings;",
            ")",
            "Lcom/squareup/print/OrderPrintingDispatcher;"
        }
    .end annotation

    .line 96
    new-instance v13, Lcom/squareup/print/OrderPrintingDispatcher;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/print/OrderPrintingDispatcher;-><init>(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/payload/TicketBillPayloadFactory;Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;Lcom/squareup/print/payload/StubPayload$Factory;Lcom/squareup/print/payload/TicketPayload$Factory;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Ljavax/inject/Provider;Landroid/app/Application;Lcom/squareup/print/PrintSettings;)V

    return-object v13
.end method


# virtual methods
.method public get()Lcom/squareup/print/OrderPrintingDispatcher;
    .locals 13

    .line 74
    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->printSpoolerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/print/PrintSpooler;

    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/print/PrinterStations;

    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->billPayloadFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/print/payload/TicketBillPayloadFactory;

    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->paymentReceiptPayloadFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;

    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->historicalReceiptPayloadFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;

    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->stubPayloadFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/print/payload/StubPayload$Factory;

    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->ticketPayloadFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/print/payload/TicketPayload$Factory;

    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/analytics/Analytics;

    iget-object v10, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->localeProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->printSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/print/PrintSettings;

    invoke-static/range {v1 .. v12}, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->newInstance(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/payload/TicketBillPayloadFactory;Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;Lcom/squareup/print/payload/StubPayload$Factory;Lcom/squareup/print/payload/TicketPayload$Factory;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Ljavax/inject/Provider;Landroid/app/Application;Lcom/squareup/print/PrintSettings;)Lcom/squareup/print/OrderPrintingDispatcher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/print/OrderPrintingDispatcher_Factory;->get()Lcom/squareup/print/OrderPrintingDispatcher;

    move-result-object v0

    return-object v0
.end method
