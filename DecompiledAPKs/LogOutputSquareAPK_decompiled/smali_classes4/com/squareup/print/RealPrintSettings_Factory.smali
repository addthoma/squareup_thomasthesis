.class public final Lcom/squareup/print/RealPrintSettings_Factory;
.super Ljava/lang/Object;
.source "RealPrintSettings_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/RealPrintSettings;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final paperSignatureSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final voidCompSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/print/RealPrintSettings_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/print/RealPrintSettings_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/print/RealPrintSettings_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/RealPrintSettings_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;)",
            "Lcom/squareup/print/RealPrintSettings_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/print/RealPrintSettings_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/print/RealPrintSettings_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/tickets/voidcomp/VoidCompSettings;)Lcom/squareup/print/RealPrintSettings;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/print/RealPrintSettings;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/print/RealPrintSettings;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/tickets/voidcomp/VoidCompSettings;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/print/RealPrintSettings;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/print/RealPrintSettings_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/print/RealPrintSettings_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/papersignature/PaperSignatureSettings;

    iget-object v2, p0, Lcom/squareup/print/RealPrintSettings_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-static {v0, v1, v2}, Lcom/squareup/print/RealPrintSettings_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/tickets/voidcomp/VoidCompSettings;)Lcom/squareup/print/RealPrintSettings;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/print/RealPrintSettings_Factory;->get()Lcom/squareup/print/RealPrintSettings;

    move-result-object v0

    return-object v0
.end method
