.class Lcom/squareup/print/sections/CoalescedItem;
.super Ljava/lang/Object;
.source "CoalescedItem.java"


# instance fields
.field final item:Lcom/squareup/checkout/CartItem;

.field quantity:Ljava/math/BigDecimal;


# direct methods
.method constructor <init>(Lcom/squareup/checkout/CartItem;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    .line 26
    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    iput-object p1, p0, Lcom/squareup/print/sections/CoalescedItem;->quantity:Ljava/math/BigDecimal;

    return-void
.end method

.method public static coalesce(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 58
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 59
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 60
    new-instance v2, Lcom/squareup/print/sections/CoalescedItem;

    invoke-direct {v2, v1}, Lcom/squareup/print/sections/CoalescedItem;-><init>(Lcom/squareup/checkout/CartItem;)V

    .line 61
    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/sections/CoalescedItem;

    if-eqz v1, :cond_0

    .line 63
    iget-object v3, v2, Lcom/squareup/print/sections/CoalescedItem;->quantity:Ljava/math/BigDecimal;

    iget-object v1, v1, Lcom/squareup/print/sections/CoalescedItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v3, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    iput-object v1, v2, Lcom/squareup/print/sections/CoalescedItem;->quantity:Ljava/math/BigDecimal;

    .line 65
    :cond_0
    invoke-virtual {v0, v2, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 68
    :cond_1
    new-instance p0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    invoke-direct {p0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 69
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/sections/CoalescedItem;

    .line 70
    iget-object v2, v1, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v2, v2, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    iget-object v3, v1, Lcom/squareup/print/sections/CoalescedItem;->quantity:Ljava/math/BigDecimal;

    invoke-static {v2, v3}, Lcom/squareup/util/BigDecimals;->equalsIgnoringScale(Ljava/math/BigDecimal;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 71
    iget-object v1, v1, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 73
    :cond_2
    iget-object v2, v1, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v2

    iget-object v1, v1, Lcom/squareup/print/sections/CoalescedItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v2, v1}, Lcom/squareup/checkout/CartItem$Builder;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    return-object p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 31
    :cond_0
    instance-of v1, p1, Lcom/squareup/print/sections/CoalescedItem;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 32
    :cond_1
    check-cast p1, Lcom/squareup/print/sections/CoalescedItem;

    .line 33
    iget-object v1, p0, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 34
    iget-object v1, p0, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v3, v3, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    .line 35
    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v3, v3, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    .line 36
    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->variablePrice:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v3, v3, Lcom/squareup/checkout/CartItem;->variablePrice:Lcom/squareup/protos/common/Money;

    .line 37
    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->overridePrice:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v3, v3, Lcom/squareup/checkout/CartItem;->overridePrice:Lcom/squareup/protos/common/Money;

    .line 38
    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    iget-object v3, p1, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v3, v3, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 39
    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    iget-object v3, p1, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v3, v3, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    .line 40
    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    .line 41
    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v1

    iget-object v3, p1, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    .line 42
    invoke-static {v1, p1}, Lcom/squareup/util/Strings;->hasTextDifference(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    .line 46
    iget-object v1, p0, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->variablePrice:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->overridePrice:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    .line 52
    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/CoalescedItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 46
    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
