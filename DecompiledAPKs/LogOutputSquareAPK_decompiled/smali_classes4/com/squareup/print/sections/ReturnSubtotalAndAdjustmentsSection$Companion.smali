.class public final Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection$Companion;
.super Ljava/lang/Object;
.source "ReturnSubtotalAndAdjustmentsSection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReturnSubtotalAndAdjustmentsSection.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReturnSubtotalAndAdjustmentsSection.kt\ncom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,100:1\n1360#2:101\n1429#2,3:102\n1360#2:105\n1429#2,3:106\n704#2:109\n777#2,2:110\n1360#2:112\n1429#2,3:113\n*E\n*S KotlinDebug\n*F\n+ 1 ReturnSubtotalAndAdjustmentsSection.kt\ncom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection$Companion\n*L\n91#1:101\n91#1,3:102\n92#1:105\n92#1,3:106\n95#1:109\n95#1,2:110\n96#1:112\n96#1,3:113\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u0002J4\u0010\u0008\u001a\u0010\u0012\u000c\u0012\n \n*\u0004\u0018\u00010\t0\t0\u00062\u0006\u0010\u000b\u001a\u00020\u000c2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u00062\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u0013\u001a\u00020\u0014H\u0007\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection$Companion;",
        "",
        "()V",
        "calculateSubtotalAmount",
        "",
        "items",
        "",
        "Lcom/squareup/checkout/CartItem;",
        "formatAdditiveTaxes",
        "Lcom/squareup/print/payload/LabelAmountPair;",
        "kotlin.jvm.PlatformType",
        "formatter",
        "Lcom/squareup/print/ReceiptFormatter;",
        "additiveTaxes",
        "Lcom/squareup/checkout/ReturnTax;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "fromReturnCart",
        "Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;",
        "returnCart",
        "Lcom/squareup/checkout/ReturnCart;",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 59
    invoke-direct {p0}, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection$Companion;-><init>()V

    return-void
.end method

.method private final calculateSubtotalAmount(Ljava/util/List;)J
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;)J"
        }
    .end annotation

    .line 94
    check-cast p1, Ljava/lang/Iterable;

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 110
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 95
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 111
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 112
    new-instance p1, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 113
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 114
    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 96
    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->baseAmount()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 115
    :cond_2
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 97
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->sumOfLong(Ljava/lang/Iterable;)J

    move-result-wide v0

    return-wide v0
.end method

.method private final formatAdditiveTaxes(Lcom/squareup/print/ReceiptFormatter;Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/ReceiptFormatter;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/ReturnTax;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation

    .line 90
    check-cast p2, Ljava/lang/Iterable;

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p2, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 102
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 103
    check-cast v2, Lcom/squareup/checkout/ReturnTax;

    .line 91
    invoke-virtual {v2}, Lcom/squareup/checkout/ReturnTax;->getTax()Lcom/squareup/checkout/Tax;

    move-result-object v3

    new-instance v4, Lcom/squareup/protos/common/Money;

    invoke-virtual {v2}, Lcom/squareup/checkout/ReturnTax;->getAppliedAmount()J

    move-result-wide v5

    neg-long v5, v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v4, v2, p3}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-static {v3, v4}, Lcom/squareup/payment/OrderAdjustment;->fromTax(Lcom/squareup/checkout/Tax;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/OrderAdjustment;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 104
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 105
    new-instance p2, Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result p3

    invoke-direct {p2, p3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 106
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 107
    check-cast v0, Lcom/squareup/payment/OrderAdjustment;

    .line 92
    invoke-virtual {p1, v0}, Lcom/squareup/print/ReceiptFormatter;->formatAdditiveTax(Lcom/squareup/payment/OrderAdjustment;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 108
    :cond_1
    check-cast p2, Ljava/util/List;

    return-object p2
.end method


# virtual methods
.method public final fromReturnCart(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/checkout/ReturnCart;)Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;
    .locals 6
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "formatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "returnCart"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    move-object v0, p0

    check-cast v0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection$Companion;

    invoke-virtual {p2}, Lcom/squareup/checkout/ReturnCart;->getAdditiveReturnTaxes()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/checkout/ReturnCart;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection$Companion;->formatAdditiveTaxes(Lcom/squareup/print/ReceiptFormatter;Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Ljava/util/List;

    move-result-object v1

    .line 68
    invoke-virtual {p2}, Lcom/squareup/checkout/ReturnCart;->getReturnItemsAsCartItems()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection$Companion;->calculateSubtotalAmount(Ljava/util/List;)J

    move-result-wide v2

    .line 69
    invoke-virtual {p2}, Lcom/squareup/checkout/ReturnCart;->getReturnDiscountsAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 71
    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v4, "discountAmount.amount"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v2, v4

    neg-long v2, v2

    .line 72
    invoke-virtual {p2}, Lcom/squareup/checkout/ReturnCart;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 74
    invoke-virtual {p2}, Lcom/squareup/checkout/ReturnCart;->getTipAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 76
    new-instance v3, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    .line 77
    invoke-virtual {p1, v0}, Lcom/squareup/print/ReceiptFormatter;->returnSubtotal(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v0

    .line 79
    invoke-virtual {p1, v2}, Lcom/squareup/print/ReceiptFormatter;->tipOrNullIfZero(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v2

    .line 81
    invoke-virtual {p2}, Lcom/squareup/checkout/ReturnCart;->getSwedishRoundingAdjustment()Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 80
    invoke-virtual {p1, p2}, Lcom/squareup/print/ReceiptFormatter;->swedishRoundingOrNullIfZero(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p1

    .line 76
    invoke-direct {v3, v0, v1, v2, p1}, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;-><init>(Lcom/squareup/print/payload/LabelAmountPair;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;)V

    return-object v3
.end method
