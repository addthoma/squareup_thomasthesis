.class public final Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;
.super Ljava/lang/Object;
.source "ReturnSubtotalAndAdjustmentsSection.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0011\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 !2\u00020\u0001:\u0001!B1\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0008J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000f\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005H\u00c6\u0003J\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J=\u0010\u0015\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\u000e\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eJ\t\u0010\u001f\u001a\u00020 H\u00d6\u0001R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u001a\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\nR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000eR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000e\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;",
        "",
        "subtotal",
        "Lcom/squareup/print/payload/LabelAmountPair;",
        "additiveTaxes",
        "",
        "tip",
        "swedishRounding",
        "(Lcom/squareup/print/payload/LabelAmountPair;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;)V",
        "getAdditiveTaxes",
        "()Ljava/util/List;",
        "nonNullAdjustments",
        "getNonNullAdjustments",
        "getSubtotal",
        "()Lcom/squareup/print/payload/LabelAmountPair;",
        "getSwedishRounding",
        "getTip",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "renderBitmap",
        "",
        "builder",
        "Lcom/squareup/print/ThermalBitmapBuilder;",
        "toString",
        "",
        "Companion",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection$Companion;


# instance fields
.field private final additiveTaxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation
.end field

.field private final subtotal:Lcom/squareup/print/payload/LabelAmountPair;

.field private final swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

.field private final tip:Lcom/squareup/print/payload/LabelAmountPair;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->Companion:Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/print/payload/LabelAmountPair;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ")V"
        }
    .end annotation

    const-string v0, "additiveTaxes"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->subtotal:Lcom/squareup/print/payload/LabelAmountPair;

    iput-object p2, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->additiveTaxes:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    iput-object p4, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;Lcom/squareup/print/payload/LabelAmountPair;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;ILjava/lang/Object;)Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->subtotal:Lcom/squareup/print/payload/LabelAmountPair;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->additiveTaxes:Ljava/util/List;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->copy(Lcom/squareup/print/payload/LabelAmountPair;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    move-result-object p0

    return-object p0
.end method

.method public static final fromReturnCart(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/checkout/ReturnCart;)Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->Companion:Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection$Companion;->fromReturnCart(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/checkout/ReturnCart;)Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    move-result-object p0

    return-object p0
.end method

.method private final getNonNullAdjustments()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 35
    iget-object v1, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->additiveTaxes:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 36
    iget-object v1, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v1, :cond_0

    .line 37
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    :cond_0
    iget-object v1, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v1, :cond_1

    .line 40
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    :cond_1
    check-cast v0, Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->subtotal:Lcom/squareup/print/payload/LabelAmountPair;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->additiveTaxes:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    return-object v0
.end method

.method public final component4()Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

    return-object v0
.end method

.method public final copy(Lcom/squareup/print/payload/LabelAmountPair;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ")",
            "Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;"
        }
    .end annotation

    const-string v0, "additiveTaxes"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;-><init>(Lcom/squareup/print/payload/LabelAmountPair;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    iget-object v0, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->subtotal:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v1, p1, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->subtotal:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->additiveTaxes:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->additiveTaxes:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v1, p1, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object p1, p1, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAdditiveTaxes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->additiveTaxes:Ljava/util/List;

    return-object v0
.end method

.method public final getSubtotal()Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->subtotal:Lcom/squareup/print/payload/LabelAmountPair;

    return-object v0
.end method

.method public final getSwedishRounding()Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

    return-object v0
.end method

.method public final getTip()Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->subtotal:Lcom/squareup/print/payload/LabelAmountPair;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->additiveTaxes:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public final renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 2

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->lightDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 48
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 50
    iget-object v0, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->subtotal:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 52
    invoke-direct {p0}, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->getNonNullAdjustments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/payload/LabelAmountPair;

    .line 53
    invoke-virtual {p1, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    .line 56
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReturnSubtotalAndAdjustmentsSection(subtotal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->subtotal:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", additiveTaxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->additiveTaxes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", swedishRounding="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
