.class public Lcom/squareup/print/sections/MultipleTaxBreakdownSection;
.super Ljava/lang/Object;
.source "MultipleTaxBreakdownSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;
    }
.end annotation


# instance fields
.field public final vatRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;",
            ">;)V"
        }
    .end annotation

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object p1, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection;->vatRows:Ljava/util/List;

    return-void
.end method

.method public static fromTaxBreakdown(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/util/Res;)Lcom/squareup/print/sections/MultipleTaxBreakdownSection;
    .locals 8

    .line 78
    iget-object v0, p1, Lcom/squareup/util/TaxBreakdown;->taxBreakdownType:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    sget-object v1, Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;->TABLE:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    if-eq v0, v1, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 82
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 83
    new-instance v7, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;

    const/4 v2, 0x1

    sget v1, Lcom/squareup/print/R$string;->tax_breakdown_table_vat_rate_header:I

    .line 84
    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v1, Lcom/squareup/print/R$string;->tax_breakdown_table_net_header:I

    .line 85
    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v1, Lcom/squareup/print/R$string;->tax_breakdown_table_vat_header:I

    .line 86
    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v1, Lcom/squareup/print/R$string;->tax_breakdown_table_gross_header:I

    .line 87
    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object p1, p1, Lcom/squareup/util/TaxBreakdown;->sortedTaxBreakdownAmountsByTaxId:Ljava/util/SortedMap;

    .line 90
    invoke-interface {p1}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object p1

    .line 91
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;

    .line 92
    invoke-virtual {p0, p2}, Lcom/squareup/print/ReceiptFormatter;->taxBreakdownLabelAndPercentage(Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;)Ljava/lang/String;

    move-result-object v3

    .line 93
    iget-object v1, p2, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;->appliedToAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, v1}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v4

    .line 94
    iget-object v1, p2, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;->appliedAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, v1}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v5

    .line 95
    iget-object p2, p2, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;->afterApplicationTotalAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, p2}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v6

    .line 96
    new-instance p2, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;

    const/4 v2, 0x0

    move-object v1, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 98
    :cond_1
    new-instance p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/print/sections/MultipleTaxBreakdownSection;-><init>(Ljava/util/List;)V

    return-object p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 109
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_0

    .line 111
    :cond_1
    check-cast p1, Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    .line 113
    iget-object v2, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection;->vatRows:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/print/sections/MultipleTaxBreakdownSection;->vatRows:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    return v1

    :cond_2
    return v0

    :cond_3
    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection;->vatRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 8

    .line 123
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 125
    iget-object v0, p0, Lcom/squareup/print/sections/MultipleTaxBreakdownSection;->vatRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;

    .line 126
    iget-boolean v3, v1, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->bold:Z

    iget-object v4, v1, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->name:Ljava/lang/String;

    iget-object v5, v1, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->net:Ljava/lang/String;

    iget-object v6, v1, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->vat:Ljava/lang/String;

    iget-object v7, v1, Lcom/squareup/print/sections/MultipleTaxBreakdownSection$VatRow;->gross:Ljava/lang/String;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Lcom/squareup/print/ThermalBitmapBuilder;->taxBreakdownRow(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    .line 129
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method
