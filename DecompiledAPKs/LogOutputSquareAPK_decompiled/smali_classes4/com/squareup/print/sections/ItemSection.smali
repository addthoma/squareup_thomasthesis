.class public Lcom/squareup/print/sections/ItemSection;
.super Ljava/lang/Object;
.source "ItemSection.java"


# static fields
.field private static final NAME_QUANTITY_SEPARATOR:Ljava/lang/String; = " \u00d7 "


# instance fields
.field public final baseAmountInputs:Ljava/lang/String;

.field public final diningOptionName:Ljava/lang/String;

.field public final nameAndQuantity:Ljava/lang/String;

.field public final note:Ljava/lang/String;

.field public final totalPrice:Ljava/lang/String;

.field public final variationAndModifiers:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    iput-object p1, p0, Lcom/squareup/print/sections/ItemSection;->nameAndQuantity:Ljava/lang/String;

    .line 163
    iput-object p2, p0, Lcom/squareup/print/sections/ItemSection;->totalPrice:Ljava/lang/String;

    .line 164
    iput-object p3, p0, Lcom/squareup/print/sections/ItemSection;->baseAmountInputs:Ljava/lang/String;

    .line 165
    iput-object p4, p0, Lcom/squareup/print/sections/ItemSection;->variationAndModifiers:Ljava/lang/String;

    .line 166
    iput-object p5, p0, Lcom/squareup/print/sections/ItemSection;->note:Ljava/lang/String;

    .line 167
    iput-object p6, p0, Lcom/squareup/print/sections/ItemSection;->diningOptionName:Ljava/lang/String;

    return-void
.end method

.method static buildVariationSeatsAndModifiers(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/print/PrintableOrderItem;ZZLcom/squareup/util/Res;Z)Ljava/lang/String;
    .locals 7

    const/4 v3, 0x1

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v5, p4

    move v6, p5

    .line 102
    invoke-static/range {v0 .. v6}, Lcom/squareup/print/sections/SectionUtils;->getSeatsVariationsAndModifiersToPrint(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/print/PrintableOrderItem;ZZZLcom/squareup/util/Res;Z)Ljava/util/List;

    move-result-object p2

    if-eqz p3, :cond_0

    .line 105
    invoke-interface {p1}, Lcom/squareup/print/PrintableOrderItem;->isComped()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 106
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 107
    invoke-virtual {p0}, Lcom/squareup/print/ReceiptFormatter;->compLabel()Ljava/lang/String;

    move-result-object p0

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move-object p1, p2

    .line 110
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 p0, 0x0

    return-object p0

    .line 115
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object p0

    const-string p1, ", "

    invoke-static {p0, p1}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static createItemSection(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/OrderAdjustment;)Lcom/squareup/print/sections/ItemSection;
    .locals 9

    .line 75
    iget-object v0, p1, Lcom/squareup/payment/OrderAdjustment;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/print/ReceiptFormatter;->itemNameOrCustomIfBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    .line 77
    invoke-static {v0, v1}, Lcom/squareup/print/sections/ItemSection;->formatNameAndQuantity(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 79
    iget-object v0, p1, Lcom/squareup/payment/OrderAdjustment;->appliedMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, v0}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v4

    .line 81
    iget-object v0, p1, Lcom/squareup/payment/OrderAdjustment;->decimalPercentage:Lcom/squareup/util/Percentage;

    if-eqz v0, :cond_0

    .line 82
    iget-object p1, p1, Lcom/squareup/payment/OrderAdjustment;->decimalPercentage:Lcom/squareup/util/Percentage;

    invoke-virtual {p0, p1}, Lcom/squareup/print/ReceiptFormatter;->formatDiscount(Lcom/squareup/util/Percentage;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    move-object v6, p0

    .line 87
    new-instance p0, Lcom/squareup/print/sections/ItemSection;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/squareup/print/sections/ItemSection;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public static createItemSection(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/print/PrintableOrderItem;ZZZZLjava/lang/String;Lcom/squareup/util/Res;Z)Lcom/squareup/print/sections/ItemSection;
    .locals 12

    move-object v6, p0

    move-object v7, p2

    move-object/from16 v8, p8

    .line 34
    invoke-interface {p2}, Lcom/squareup/print/PrintableOrderItem;->getItemName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/print/ReceiptFormatter;->itemNameOrCustomIfBlank(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz p3, :cond_0

    .line 35
    invoke-interface {p2}, Lcom/squareup/print/PrintableOrderItem;->isTaxed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 36
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "*"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    move-object v9, v0

    .line 39
    invoke-interface {p2}, Lcom/squareup/print/PrintableOrderItem;->isUnitPriced()Z

    move-result v0

    const/4 v10, 0x0

    if-eqz v0, :cond_1

    .line 40
    invoke-interface {p2}, Lcom/squareup/print/PrintableOrderItem;->getUnitPrice()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 41
    invoke-interface {p2}, Lcom/squareup/print/PrintableOrderItem;->getQuantityAsString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v8}, Lcom/squareup/print/PrintableOrderItem;->unitAbbreviation(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v8}, Lcom/squareup/print/PrintableOrderItem;->quantityPrecision(Lcom/squareup/util/Res;)I

    move-result v4

    .line 42
    invoke-interface {p2, v8}, Lcom/squareup/print/PrintableOrderItem;->quantityEntrySuffix(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    .line 40
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/print/ReceiptFormatter;->formatBaseAmountInputs(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v11, v0

    goto :goto_0

    .line 46
    :cond_1
    invoke-interface {p2}, Lcom/squareup/print/PrintableOrderItem;->getQuantityAsInt()I

    move-result v0

    .line 47
    invoke-static {v9, v0}, Lcom/squareup/print/sections/ItemSection;->formatNameAndQuantity(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    move-object v11, v10

    :goto_0
    move-object v0, p0

    move-object v1, p2

    move/from16 v2, p5

    move/from16 v3, p6

    move-object/from16 v4, p8

    move/from16 v5, p9

    .line 51
    invoke-static/range {v0 .. v5}, Lcom/squareup/print/sections/ItemSection;->buildVariationSeatsAndModifiers(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/print/PrintableOrderItem;ZZLcom/squareup/util/Res;Z)Ljava/lang/String;

    move-result-object v0

    .line 53
    invoke-interface {p2}, Lcom/squareup/print/PrintableOrderItem;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v1

    if-eqz p4, :cond_2

    .line 56
    instance-of v2, v7, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;

    const-string v3, "Multiple taxes only supported for Payment orders"

    invoke-static {v2, v3}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    move-object v2, p1

    .line 58
    iget-object v2, v2, Lcom/squareup/util/TaxBreakdown;->taxLabelsByCartItemId:Ljava/util/Map;

    invoke-interface {p2}, Lcom/squareup/print/PrintableOrderItem;->getIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 59
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 65
    :cond_2
    invoke-interface {p2}, Lcom/squareup/print/PrintableOrderItem;->isDownConvertedCustomAmount()Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_3
    invoke-interface {p2}, Lcom/squareup/print/PrintableOrderItem;->getNotes()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/squareup/print/ReceiptFormatter;->noteOrNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 67
    :goto_1
    new-instance v2, Lcom/squareup/print/sections/ItemSection;

    move-object p0, v2

    move-object p1, v9

    move-object p2, v1

    move-object p3, v11

    move-object/from16 p4, v0

    move-object/from16 p5, v10

    move-object/from16 p6, p7

    invoke-direct/range {p0 .. p6}, Lcom/squareup/print/sections/ItemSection;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method static formatNameAndQuantity(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 p0, 0x1

    if-le p1, p0, :cond_0

    const-string p0, " \u00d7 "

    .line 93
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 96
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_c

    .line 127
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_5

    .line 129
    :cond_1
    check-cast p1, Lcom/squareup/print/sections/ItemSection;

    .line 131
    iget-object v2, p0, Lcom/squareup/print/sections/ItemSection;->nameAndQuantity:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v3, p1, Lcom/squareup/print/sections/ItemSection;->nameAndQuantity:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/squareup/print/sections/ItemSection;->nameAndQuantity:Ljava/lang/String;

    if-eqz v2, :cond_3

    :goto_0
    return v1

    .line 135
    :cond_3
    iget-object v2, p0, Lcom/squareup/print/sections/ItemSection;->note:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v3, p1, Lcom/squareup/print/sections/ItemSection;->note:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_1

    :cond_4
    iget-object v2, p1, Lcom/squareup/print/sections/ItemSection;->note:Ljava/lang/String;

    if-eqz v2, :cond_5

    :goto_1
    return v1

    .line 136
    :cond_5
    iget-object v2, p0, Lcom/squareup/print/sections/ItemSection;->totalPrice:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v3, p1, Lcom/squareup/print/sections/ItemSection;->totalPrice:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    goto :goto_2

    :cond_6
    iget-object v2, p1, Lcom/squareup/print/sections/ItemSection;->totalPrice:Ljava/lang/String;

    if-eqz v2, :cond_7

    :goto_2
    return v1

    .line 139
    :cond_7
    iget-object v2, p0, Lcom/squareup/print/sections/ItemSection;->baseAmountInputs:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v3, p1, Lcom/squareup/print/sections/ItemSection;->baseAmountInputs:Ljava/lang/String;

    .line 140
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    goto :goto_3

    :cond_8
    iget-object v2, p1, Lcom/squareup/print/sections/ItemSection;->baseAmountInputs:Ljava/lang/String;

    if-eqz v2, :cond_9

    :goto_3
    return v1

    .line 143
    :cond_9
    iget-object v2, p0, Lcom/squareup/print/sections/ItemSection;->variationAndModifiers:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/sections/ItemSection;->variationAndModifiers:Ljava/lang/String;

    if-eqz v2, :cond_a

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_b

    goto :goto_4

    :cond_a
    if-eqz p1, :cond_b

    :goto_4
    return v1

    :cond_b
    return v0

    :cond_c
    :goto_5
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 152
    iget-object v0, p0, Lcom/squareup/print/sections/ItemSection;->nameAndQuantity:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 153
    iget-object v2, p0, Lcom/squareup/print/sections/ItemSection;->totalPrice:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 154
    iget-object v2, p0, Lcom/squareup/print/sections/ItemSection;->baseAmountInputs:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 155
    iget-object v2, p0, Lcom/squareup/print/sections/ItemSection;->variationAndModifiers:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 156
    iget-object v2, p0, Lcom/squareup/print/sections/ItemSection;->note:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method
