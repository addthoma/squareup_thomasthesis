.class public final Lcom/squareup/print/ThermalBitmapBuilder_Factory_Factory;
.super Ljava/lang/Object;
.source "ThermalBitmapBuilder_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/ThermalBitmapBuilder$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final picassoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;"
        }
    .end annotation
.end field

.field private final thumborProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pollexor/Thumbor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pollexor/Thumbor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/print/ThermalBitmapBuilder_Factory_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/print/ThermalBitmapBuilder_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/print/ThermalBitmapBuilder_Factory_Factory;->picassoProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/print/ThermalBitmapBuilder_Factory_Factory;->thumborProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/print/ThermalBitmapBuilder_Factory_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/ThermalBitmapBuilder_Factory_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pollexor/Thumbor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;)",
            "Lcom/squareup/print/ThermalBitmapBuilder_Factory_Factory;"
        }
    .end annotation

    .line 50
    new-instance v6, Lcom/squareup/print/ThermalBitmapBuilder_Factory_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/ThermalBitmapBuilder_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/settings/server/Features;Lcom/squareup/picasso/Picasso;Lcom/squareup/pollexor/Thumbor;Lrx/Scheduler;)Lcom/squareup/print/ThermalBitmapBuilder$Factory;
    .locals 7

    .line 55
    new-instance v6, Lcom/squareup/print/ThermalBitmapBuilder$Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/ThermalBitmapBuilder$Factory;-><init>(Landroid/app/Application;Lcom/squareup/settings/server/Features;Lcom/squareup/picasso/Picasso;Lcom/squareup/pollexor/Thumbor;Lrx/Scheduler;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/print/ThermalBitmapBuilder$Factory;
    .locals 5

    .line 43
    iget-object v0, p0, Lcom/squareup/print/ThermalBitmapBuilder_Factory_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/print/ThermalBitmapBuilder_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    iget-object v2, p0, Lcom/squareup/print/ThermalBitmapBuilder_Factory_Factory;->picassoProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/picasso/Picasso;

    iget-object v3, p0, Lcom/squareup/print/ThermalBitmapBuilder_Factory_Factory;->thumborProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/pollexor/Thumbor;

    iget-object v4, p0, Lcom/squareup/print/ThermalBitmapBuilder_Factory_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lrx/Scheduler;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/print/ThermalBitmapBuilder_Factory_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/settings/server/Features;Lcom/squareup/picasso/Picasso;Lcom/squareup/pollexor/Thumbor;Lrx/Scheduler;)Lcom/squareup/print/ThermalBitmapBuilder$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/print/ThermalBitmapBuilder_Factory_Factory;->get()Lcom/squareup/print/ThermalBitmapBuilder$Factory;

    move-result-object v0

    return-object v0
.end method
