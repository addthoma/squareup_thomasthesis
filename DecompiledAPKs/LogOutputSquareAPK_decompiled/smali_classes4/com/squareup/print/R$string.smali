.class public final Lcom/squareup/print/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final buyer_printed_receipt_additions_label_and_price:I = 0x7f120217

.field public static final buyer_printed_receipt_aid:I = 0x7f120218

.field public static final buyer_printed_receipt_authorization_number:I = 0x7f120219

.field public static final buyer_printed_receipt_comp:I = 0x7f12021a

.field public static final buyer_printed_receipt_discount:I = 0x7f12021b

.field public static final buyer_printed_receipt_disposition_approved_uppercase:I = 0x7f12021c

.field public static final buyer_printed_receipt_invoice:I = 0x7f120222

.field public static final buyer_printed_receipt_invoice_number:I = 0x7f120223

.field public static final buyer_printed_receipt_keypad_item_name:I = 0x7f120224

.field public static final buyer_printed_receipt_label_and_price:I = 0x7f120225

.field public static final buyer_printed_receipt_non_taxable:I = 0x7f120226

.field public static final buyer_printed_receipt_number:I = 0x7f120227

.field public static final buyer_printed_receipt_remaining_balance:I = 0x7f120228

.field public static final buyer_printed_receipt_rounding:I = 0x7f120229

.field public static final buyer_printed_receipt_subtotal:I = 0x7f12022a

.field public static final buyer_printed_receipt_subtotal_purchase:I = 0x7f12022b

.field public static final buyer_printed_receipt_subtotal_return:I = 0x7f12022c

.field public static final buyer_printed_receipt_tax_id:I = 0x7f12022d

.field public static final buyer_printed_receipt_tax_invoice:I = 0x7f12022e

.field public static final buyer_printed_receipt_taxes_included_items:I = 0x7f12022f

.field public static final buyer_printed_receipt_taxes_included_items_format:I = 0x7f120230

.field public static final buyer_printed_receipt_taxes_included_total:I = 0x7f120231

.field public static final buyer_printed_receipt_taxes_included_total_format:I = 0x7f120232

.field public static final buyer_printed_receipt_taxes_inclusive_tax_format:I = 0x7f120233

.field public static final buyer_printed_receipt_tender_amount:I = 0x7f120234

.field public static final buyer_printed_receipt_tender_card:I = 0x7f120235

.field public static final buyer_printed_receipt_tender_card_detail_all:I = 0x7f120236

.field public static final buyer_printed_receipt_tender_card_detail_no_entry_method:I = 0x7f120237

.field public static final buyer_printed_receipt_tender_card_detail_no_suffix:I = 0x7f120238

.field public static final buyer_printed_receipt_tender_change:I = 0x7f12023a

.field public static final buyer_printed_receipt_test_title:I = 0x7f12023d

.field public static final buyer_printed_receipt_test_title_with_printer_name:I = 0x7f12023e

.field public static final buyer_printed_receipt_ticket:I = 0x7f12023f

.field public static final buyer_printed_receipt_tip:I = 0x7f120240

.field public static final buyer_printed_receipt_title:I = 0x7f120241

.field public static final buyer_printed_receipt_total:I = 0x7f120242

.field public static final buyer_printed_receipt_unit_cost_each:I = 0x7f120243

.field public static final buyer_printed_receipt_verification_method_on_device:I = 0x7f120244

.field public static final buyer_printed_receipt_verification_method_pin:I = 0x7f120245

.field public static final comp_initial:I = 0x7f12045c

.field public static final cover_count_plural:I = 0x7f1205ba

.field public static final cover_count_singular:I = 0x7f1205bb

.field public static final discount_receipt_format:I = 0x7f120884

.field public static final jp_formal_receipt_item_header:I = 0x7f120e73

.field public static final jp_formal_receipt_name:I = 0x7f120e74

.field public static final jp_formal_receipt_received_confirmation:I = 0x7f120e75

.field public static final jp_formal_receipt_title:I = 0x7f120e76

.field public static final jp_formal_receipt_total:I = 0x7f120e77

.field public static final paper_signature_quick_tip_calculated_amount_and_total_pattern:I = 0x7f121324

.field public static final paper_signature_quick_tip_calculated_total_pattern:I = 0x7f121325

.field public static final paper_signature_quick_tip_no_tip_label:I = 0x7f121327

.field public static final paper_signature_quick_tip_percentage_pattern:I = 0x7f121328

.field public static final paper_signature_quick_tip_setting_hint:I = 0x7f121329

.field public static final paper_signature_quick_tip_setting_label:I = 0x7f12132a

.field public static final paper_signature_quick_tip_tip_label:I = 0x7f12132b

.field public static final paper_signature_quick_tip_title_label:I = 0x7f12132c

.field public static final paper_signature_quick_tip_total_label:I = 0x7f12132d

.field public static final paper_signature_traditional_additional_tip_label:I = 0x7f12133d

.field public static final paper_signature_traditional_receipt:I = 0x7f12133e

.field public static final paper_signature_traditional_tip_label:I = 0x7f12133f

.field public static final paper_signature_traditional_total_label:I = 0x7f121340

.field public static final print_job_title:I = 0x7f1214a4

.field public static final printed_ticket_void:I = 0x7f1214b6

.field public static final printout_type_auth_slip:I = 0x7f1214e8

.field public static final printout_type_auth_slip_copy:I = 0x7f1214e9

.field public static final printout_type_order_ticket:I = 0x7f1214ea

.field public static final printout_type_order_ticket_stub:I = 0x7f1214eb

.field public static final printout_type_receipt:I = 0x7f1214ec

.field public static final printout_type_void_ticket:I = 0x7f1214ed

.field public static final receipt_felica_reprint:I = 0x7f1215c3

.field public static final receipt_felica_suica_terminal_id:I = 0x7f1215c4

.field public static final receipt_header_purchase:I = 0x7f1215c5

.field public static final receipt_header_purchase_transaction_type:I = 0x7f1215c6

.field public static final receipt_header_refund_transaction_type:I = 0x7f1215c7

.field public static final receipt_header_return:I = 0x7f1215c8

.field public static final receipt_tender_felica_id:I = 0x7f1215ce

.field public static final receipt_tender_felica_id_purchase_location:I = 0x7f1215cf

.field public static final receipt_tender_felica_id_refund_location:I = 0x7f1215d0

.field public static final receipt_tender_felica_qp:I = 0x7f1215d1

.field public static final receipt_tender_felica_suica:I = 0x7f1215d2

.field public static final tax_breakdown_table_gross_header:I = 0x7f12192f

.field public static final tax_breakdown_table_net_header:I = 0x7f121930

.field public static final tax_breakdown_table_vat_header:I = 0x7f121931

.field public static final tax_breakdown_table_vat_rate_header:I = 0x7f121932

.field public static final tax_breakdown_table_vat_rate_name_and_percentage:I = 0x7f121933

.field public static final tax_name:I = 0x7f121948

.field public static final ticket_reprint_label:I = 0x7f12196f

.field public static final timecards_breaks_header:I = 0x7f1219a5

.field public static final timecards_clock_in:I = 0x7f1219a6

.field public static final timecards_clock_out:I = 0x7f1219a7

.field public static final timecards_print_timestamp:I = 0x7f1219a8

.field public static final timecards_receipt_title:I = 0x7f1219a9

.field public static final timecards_shift_default_title:I = 0x7f1219aa

.field public static final timecards_shift_report_header:I = 0x7f1219ab

.field public static final timecards_shifts_header:I = 0x7f1219ac

.field public static final timecards_total_hours:I = 0x7f1219ad

.field public static final uppercase_receipt_customer_copy:I = 0x7f121b6a

.field public static final uppercase_receipt_merchant_copy:I = 0x7f121b70


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
