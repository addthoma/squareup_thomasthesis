.class public final Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;
.super Ljava/lang/Object;
.source "PrintablePaymentOrder.kt"

# interfaces
.implements Lcom/squareup/print/PrintableOrderItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/PrintablePaymentOrder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PrintablePaymentOrderItem"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPrintablePaymentOrder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PrintablePaymentOrder.kt\ncom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,123:1\n1265#2,12:124\n1360#2:136\n1429#2,3:137\n*E\n*S KotlinDebug\n*F\n+ 1 PrintablePaymentOrder.kt\ncom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem\n*L\n57#1,12:124\n58#1:136\n58#1,3:137\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0010\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0019\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010;\u001a\u00020\u00082\u0006\u0010<\u001a\u00020=H\u0016J\u0010\u0010>\u001a\u00020!2\u0006\u0010<\u001a\u00020=H\u0016J\u0010\u0010?\u001a\u00020\u00082\u0006\u0010<\u001a\u00020=H\u0016R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u000cX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\u0010X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0014\u0010\u0015\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0014R\u0014\u0010\u0016\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0014R\u0014\u0010\u0017\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0014R\u0014\u0010\u0018\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0014R\u0014\u0010\u0019\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0014R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0016\u0010\u001c\u001a\u0004\u0018\u00010\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\nR\u0016\u0010\u001e\u001a\u0004\u0018\u00010\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\nR\u0014\u0010 \u001a\u00020!X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010#R\u0014\u0010$\u001a\u00020\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010\nR\u0016\u0010&\u001a\u0004\u0018\u00010\'X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010)R\u001a\u0010*\u001a\u0008\u0012\u0004\u0012\u00020,0+X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008-\u0010.R\u0016\u0010/\u001a\u0004\u0018\u00010\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00080\u0010\nR\u0014\u00101\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00082\u0010\u0014R\u0014\u00103\u001a\u000204X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00085\u00106R\u0016\u00107\u001a\u0004\u0018\u000104X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00088\u00106R\u0014\u00109\u001a\u000204X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008:\u00106\u00a8\u0006@"
    }
    d2 = {
        "Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;",
        "Lcom/squareup/print/PrintableOrderItem;",
        "item",
        "Lcom/squareup/checkout/CartItem;",
        "isReturn",
        "",
        "(Lcom/squareup/checkout/CartItem;Z)V",
        "categoryId",
        "",
        "getCategoryId",
        "()Ljava/lang/String;",
        "destination",
        "Lcom/squareup/checkout/OrderDestination;",
        "getDestination",
        "()Lcom/squareup/checkout/OrderDestination;",
        "idPair",
        "Lcom/squareup/protos/client/IdPair;",
        "getIdPair",
        "()Lcom/squareup/protos/client/IdPair;",
        "isComped",
        "()Z",
        "isDownConvertedCustomAmount",
        "isTaxed",
        "isUncategorized",
        "isUnitPriced",
        "isVoided",
        "getItem$print_release",
        "()Lcom/squareup/checkout/CartItem;",
        "itemName",
        "getItemName",
        "notes",
        "getNotes",
        "quantityAsInt",
        "",
        "getQuantityAsInt",
        "()I",
        "quantityAsString",
        "getQuantityAsString",
        "selectedDiningOption",
        "Lcom/squareup/checkout/DiningOption;",
        "getSelectedDiningOption",
        "()Lcom/squareup/checkout/DiningOption;",
        "selectedModifiers",
        "",
        "Lcom/squareup/print/PrintableOrderItemModifier;",
        "getSelectedModifiers",
        "()Ljava/util/List;",
        "selectedVariationDisplayName",
        "getSelectedVariationDisplayName",
        "shouldShowVariationName",
        "getShouldShowVariationName",
        "total",
        "Lcom/squareup/protos/common/Money;",
        "getTotal",
        "()Lcom/squareup/protos/common/Money;",
        "unitPrice",
        "getUnitPrice",
        "unitPriceWithModifiers",
        "getUnitPriceWithModifiers",
        "quantityEntrySuffix",
        "res",
        "Lcom/squareup/util/Res;",
        "quantityPrecision",
        "unitAbbreviation",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final categoryId:Ljava/lang/String;

.field private final destination:Lcom/squareup/checkout/OrderDestination;

.field private final idPair:Lcom/squareup/protos/client/IdPair;

.field private final isComped:Z

.field private final isDownConvertedCustomAmount:Z

.field private final isTaxed:Z

.field private final isUncategorized:Z

.field private final isUnitPriced:Z

.field private final isVoided:Z

.field private final item:Lcom/squareup/checkout/CartItem;

.field private final itemName:Ljava/lang/String;

.field private final notes:Ljava/lang/String;

.field private final quantityAsInt:I

.field private final quantityAsString:Ljava/lang/String;

.field private final selectedDiningOption:Lcom/squareup/checkout/DiningOption;

.field private final selectedModifiers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItemModifier;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedVariationDisplayName:Ljava/lang/String;

.field private final shouldShowVariationName:Z

.field private final total:Lcom/squareup/protos/common/Money;

.field private final unitPrice:Lcom/squareup/protos/common/Money;

.field private final unitPriceWithModifiers:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Lcom/squareup/checkout/CartItem;)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;-><init>(Lcom/squareup/checkout/CartItem;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/checkout/CartItem;Z)V
    .locals 5

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    .line 50
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isUncategorized()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->isUncategorized:Z

    .line 52
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->destination:Lcom/squareup/checkout/OrderDestination;

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->destination:Lcom/squareup/checkout/OrderDestination;

    .line 54
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const-string v0, "item.selectedVariation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->getDisplayName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->selectedVariationDisplayName:Ljava/lang/String;

    .line 57
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    invoke-interface {p1}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object p1

    const-string v1, "item.selectedModifiers.values"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 124
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 131
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 132
    check-cast v2, Ljava/util/SortedMap;

    .line 57
    invoke-interface {v2}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 133
    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 135
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 136
    new-instance p1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {p1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 137
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 138
    check-cast v2, Lcom/squareup/checkout/OrderModifier;

    .line 58
    new-instance v3, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItemModifier;

    const-string v4, "it"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v2}, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItemModifier;-><init>(Lcom/squareup/checkout/OrderModifier;)V

    invoke-interface {p1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 139
    :cond_1
    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->selectedModifiers:Ljava/util/List;

    .line 60
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->itemName:Ljava/lang/String;

    .line 62
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {p1}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "item.quantity.toPlainString()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->quantityAsString:Ljava/lang/String;

    .line 64
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {p1}, Ljava/math/BigDecimal;->intValue()I

    move-result p1

    iput p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->quantityAsInt:I

    .line 66
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->notes:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->notes:Ljava/lang/String;

    .line 68
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    .line 70
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->isComped:Z

    .line 72
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isTaxed()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->isTaxed:Z

    .line 74
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    const-string v1, "item.idPair"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->idPair:Lcom/squareup/protos/client/IdPair;

    .line 76
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->isVoided:Z

    .line 78
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->categoryId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->categoryId:Ljava/lang/String;

    .line 80
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->shouldShowVariationName()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->shouldShowVariationName:Z

    .line 82
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->isUnitPriced:Z

    .line 91
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->unitPriceOrNull()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->unitPrice:Lcom/squareup/protos/common/Money;

    .line 93
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->unitPriceWithModifiers()Lcom/squareup/protos/common/Money;

    move-result-object p1

    const-string v0, "item.unitPriceWithModifiers()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->unitPriceWithModifiers:Lcom/squareup/protos/common/Money;

    .line 95
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->total()Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-eqz p2, :cond_2

    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    const-string p2, "MoneyMath.negate(item.total())"

    goto :goto_2

    :cond_2
    const-string p2, "item.total()"

    :goto_2
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->total:Lcom/squareup/protos/common/Money;

    .line 98
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    const/4 p2, 0x0

    if-eqz p1, :cond_3

    const/4 v0, 0x2

    const/4 v1, 0x0

    const-string v2, "-downconverted"

    invoke-static {p1, v2, p2, v0, v1}, Lkotlin/text/StringsKt;->endsWith$default(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result p2

    :cond_3
    iput-boolean p2, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->isDownConvertedCustomAmount:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/checkout/CartItem;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 48
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;-><init>(Lcom/squareup/checkout/CartItem;Z)V

    return-void
.end method


# virtual methods
.method public getCategoryId()Ljava/lang/String;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->categoryId:Ljava/lang/String;

    return-object v0
.end method

.method public getDestination()Lcom/squareup/checkout/OrderDestination;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->destination:Lcom/squareup/checkout/OrderDestination;

    return-object v0
.end method

.method public getIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->idPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public final getItem$print_release()Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    return-object v0
.end method

.method public getItemName()Ljava/lang/String;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->itemName:Ljava/lang/String;

    return-object v0
.end method

.method public getNotes()Ljava/lang/String;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->notes:Ljava/lang/String;

    return-object v0
.end method

.method public getQuantityAsInt()I
    .locals 1

    .line 64
    iget v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->quantityAsInt:I

    return v0
.end method

.method public getQuantityAsString()Ljava/lang/String;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->quantityAsString:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->selectedDiningOption:Lcom/squareup/checkout/DiningOption;

    return-object v0
.end method

.method public bridge synthetic getSelectedDiningOption()Lcom/squareup/itemsorter/SortableDiningOption;
    .locals 1

    .line 46
    invoke-virtual {p0}, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    check-cast v0, Lcom/squareup/itemsorter/SortableDiningOption;

    return-object v0
.end method

.method public getSelectedModifiers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItemModifier;",
            ">;"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->selectedModifiers:Ljava/util/List;

    return-object v0
.end method

.method public getSelectedVariationDisplayName()Ljava/lang/String;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->selectedVariationDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getShouldShowVariationName()Z
    .locals 1

    .line 80
    iget-boolean v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->shouldShowVariationName:Z

    return v0
.end method

.method public getTotal()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->total:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getUnitPrice()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->unitPrice:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getUnitPriceWithModifiers()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->unitPriceWithModifiers:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public isComped()Z
    .locals 1

    .line 70
    iget-boolean v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->isComped:Z

    return v0
.end method

.method public isDownConvertedCustomAmount()Z
    .locals 1

    .line 97
    iget-boolean v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->isDownConvertedCustomAmount:Z

    return v0
.end method

.method public isTaxed()Z
    .locals 1

    .line 72
    iget-boolean v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->isTaxed:Z

    return v0
.end method

.method public isUncategorized()Z
    .locals 1

    .line 50
    iget-boolean v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->isUncategorized:Z

    return v0
.end method

.method public isUnitPriced()Z
    .locals 1

    .line 82
    iget-boolean v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->isUnitPriced:Z

    return v0
.end method

.method public isVoided()Z
    .locals 1

    .line 76
    iget-boolean v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->isVoided:Z

    return v0
.end method

.method public quantityEntrySuffix(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    invoke-static {v0, p1}, Lcom/squareup/quantity/ItemQuantities;->toQuantityEntrySuffix(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public quantityPrecision(Lcom/squareup/util/Res;)I
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const-string v0, "item.selectedVariation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->getQuantityPrecision()I

    move-result p1

    return p1
.end method

.method public bridge synthetic setSelectedDiningOption(Lcom/squareup/itemsorter/SortableDiningOption;)Lcom/squareup/itemsorter/SortableItem;
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/checkout/DiningOption;

    invoke-virtual {p0, p1}, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->setSelectedDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/print/PrintableOrderItem;

    move-result-object p1

    check-cast p1, Lcom/squareup/itemsorter/SortableItem;

    return-object p1
.end method

.method public setSelectedDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/print/PrintableOrderItem;
    .locals 1

    const-string v0, "selectedDiningOption"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-static {p0, p1}, Lcom/squareup/print/PrintableOrderItem$DefaultImpls;->setSelectedDiningOption(Lcom/squareup/print/PrintableOrderItem;Lcom/squareup/checkout/DiningOption;)Lcom/squareup/print/PrintableOrderItem;

    move-result-object p1

    return-object p1
.end method

.method public unitAbbreviation(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;->item:Lcom/squareup/checkout/CartItem;

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const-string v0, "item.selectedVariation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object p1

    const-string v0, "item.selectedVariation.unitAbbreviation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
