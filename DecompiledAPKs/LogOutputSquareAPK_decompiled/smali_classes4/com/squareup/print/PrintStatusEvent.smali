.class public Lcom/squareup/print/PrintStatusEvent;
.super Lcom/squareup/print/PrinterEvent;
.source "PrintStatusEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u000b\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u000b\u0008\u0016\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0013\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0013\u0010\r\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000cR\u0013\u0010\u000f\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000cR\u0013\u0010\u0011\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000cR\u0013\u0010\u0013\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u000cR\u0011\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0011\u0010\u0019\u001a\u00020\u001a\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0013\u0010\u001d\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u000cR\u0013\u0010\u001f\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u000cR\u0011\u0010!\u001a\u00020\u001a\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010\u001cR\u0013\u0010#\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010\u000c\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/print/PrintStatusEvent;",
        "Lcom/squareup/print/PrinterEvent;",
        "name",
        "Lcom/squareup/eventstream/v1/EventStream$Name;",
        "value",
        "Lcom/squareup/analytics/RegisterActionName;",
        "job",
        "Lcom/squareup/print/PrintJob;",
        "(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrintJob;)V",
        "errorMessage",
        "",
        "getErrorMessage",
        "()Ljava/lang/String;",
        "jobName",
        "getJobName",
        "jobSourceIdentifier",
        "getJobSourceIdentifier",
        "jobType",
        "getJobType",
        "jobUuid",
        "getJobUuid",
        "portAcquisitionAccumulatedDurationMs",
        "",
        "getPortAcquisitionAccumulatedDurationMs",
        "()J",
        "portAcquisitionAttempts",
        "",
        "getPortAcquisitionAttempts",
        "()I",
        "printTargetId",
        "getPrintTargetId",
        "printTargetName",
        "getPrintTargetName",
        "retries",
        "getRetries",
        "vendorSpecificResult",
        "getVendorSpecificResult",
        "hardware_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final errorMessage:Ljava/lang/String;

.field private final jobName:Ljava/lang/String;

.field private final jobSourceIdentifier:Ljava/lang/String;

.field private final jobType:Ljava/lang/String;

.field private final jobUuid:Ljava/lang/String;

.field private final portAcquisitionAccumulatedDurationMs:J

.field private final portAcquisitionAttempts:I

.field private final printTargetId:Ljava/lang/String;

.field private final printTargetName:Ljava/lang/String;

.field private final retries:I

.field private final vendorSpecificResult:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrintJob;)V
    .locals 12

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "job"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    iget-object v3, p2, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    const-string/jumbo p2, "value.value"

    invoke-static {v3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/print/PrintJob$PrintAttempt;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->hardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object p2

    :goto_0
    move-object v4, p2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xf8

    const/4 v11, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v11}, Lcom/squareup/print/PrinterEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 13
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/print/PrintJob$PrintAttempt;->errorMessage:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/print/PrintStatusEvent;->errorMessage:Ljava/lang/String;

    .line 14
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/print/PrintJob$PrintAttempt;->vendorSpecificResult:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/print/PrintStatusEvent;->vendorSpecificResult:Ljava/lang/String;

    .line 15
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object p1

    iget p1, p1, Lcom/squareup/print/PrintJob$PrintAttempt;->portAcquisitionAttempts:I

    iput p1, p0, Lcom/squareup/print/PrintStatusEvent;->portAcquisitionAttempts:I

    .line 17
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object p1

    iget-wide p1, p1, Lcom/squareup/print/PrintJob$PrintAttempt;->portAcquisitionAccumulatedDurationMs:J

    iput-wide p1, p0, Lcom/squareup/print/PrintStatusEvent;->portAcquisitionAccumulatedDurationMs:J

    .line 19
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getPrintAttemptsCount()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, Lcom/squareup/print/PrintStatusEvent;->retries:I

    .line 20
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getTargetId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintStatusEvent;->printTargetId:Ljava/lang/String;

    .line 21
    iget-object p1, p3, Lcom/squareup/print/PrintJob;->printTargetName:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/print/PrintStatusEvent;->printTargetName:Ljava/lang/String;

    .line 22
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getJobId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintStatusEvent;->jobUuid:Ljava/lang/String;

    .line 23
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getTitle()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintStatusEvent;->jobName:Ljava/lang/String;

    .line 24
    iget-object p1, p3, Lcom/squareup/print/PrintJob;->sourceIdentifier:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/print/PrintStatusEvent;->jobSourceIdentifier:Ljava/lang/String;

    .line 25
    invoke-virtual {p3}, Lcom/squareup/print/PrintJob;->getPayload()Lcom/squareup/print/PrintablePayload;

    move-result-object p1

    const-string p2, "job.payload"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/print/PrintablePayload;->getAnalyticsPrintJobType()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintStatusEvent;->jobType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getErrorMessage()Ljava/lang/String;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/print/PrintStatusEvent;->errorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final getJobName()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/print/PrintStatusEvent;->jobName:Ljava/lang/String;

    return-object v0
.end method

.method public final getJobSourceIdentifier()Ljava/lang/String;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/print/PrintStatusEvent;->jobSourceIdentifier:Ljava/lang/String;

    return-object v0
.end method

.method public final getJobType()Ljava/lang/String;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/print/PrintStatusEvent;->jobType:Ljava/lang/String;

    return-object v0
.end method

.method public final getJobUuid()Ljava/lang/String;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/print/PrintStatusEvent;->jobUuid:Ljava/lang/String;

    return-object v0
.end method

.method public final getPortAcquisitionAccumulatedDurationMs()J
    .locals 2

    .line 16
    iget-wide v0, p0, Lcom/squareup/print/PrintStatusEvent;->portAcquisitionAccumulatedDurationMs:J

    return-wide v0
.end method

.method public final getPortAcquisitionAttempts()I
    .locals 1

    .line 15
    iget v0, p0, Lcom/squareup/print/PrintStatusEvent;->portAcquisitionAttempts:I

    return v0
.end method

.method public final getPrintTargetId()Ljava/lang/String;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/print/PrintStatusEvent;->printTargetId:Ljava/lang/String;

    return-object v0
.end method

.method public final getPrintTargetName()Ljava/lang/String;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/print/PrintStatusEvent;->printTargetName:Ljava/lang/String;

    return-object v0
.end method

.method public final getRetries()I
    .locals 1

    .line 19
    iget v0, p0, Lcom/squareup/print/PrintStatusEvent;->retries:I

    return v0
.end method

.method public final getVendorSpecificResult()Ljava/lang/String;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/print/PrintStatusEvent;->vendorSpecificResult:Ljava/lang/String;

    return-object v0
.end method
