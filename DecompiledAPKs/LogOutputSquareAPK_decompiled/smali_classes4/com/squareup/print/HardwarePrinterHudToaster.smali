.class public Lcom/squareup/print/HardwarePrinterHudToaster;
.super Ljava/lang/Object;
.source "HardwarePrinterHudToaster.java"

# interfaces
.implements Lcom/squareup/print/HardwarePrinterTracker$Listener;


# instance fields
.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/util/Res;Lcom/squareup/print/PrinterStations;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/print/HardwarePrinterHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 22
    iput-object p2, p0, Lcom/squareup/print/HardwarePrinterHudToaster;->res:Lcom/squareup/util/Res;

    .line 23
    iput-object p3, p0, Lcom/squareup/print/HardwarePrinterHudToaster;->printerStations:Lcom/squareup/print/PrinterStations;

    return-void
.end method

.method private getPrinterNameToDisplay(Lcom/squareup/print/HardwarePrinter;)Ljava/lang/String;
    .locals 2

    .line 61
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinterHudToaster;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsFor(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getDisplayableModelName()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private shouldDisplayToastFor(Ljava/lang/String;Lcom/squareup/print/HardwarePrinter;)Z
    .locals 1

    .line 52
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 53
    invoke-virtual {p2}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    sget-object v0, Lcom/squareup/print/ConnectionType;->INTERNAL:Lcom/squareup/print/ConnectionType;

    if-eq p1, v0, :cond_0

    .line 54
    invoke-virtual {p2}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    sget-object p2, Lcom/squareup/print/ConnectionType;->FAKE_INTERNAL:Lcom/squareup/print/ConnectionType;

    if-eq p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public printerConnected(Lcom/squareup/print/HardwarePrinter;)V
    .locals 3

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/print/HardwarePrinterHudToaster;->getPrinterNameToDisplay(Lcom/squareup/print/HardwarePrinter;)Ljava/lang/String;

    move-result-object v0

    .line 29
    invoke-direct {p0, v0, p1}, Lcom/squareup/print/HardwarePrinterHudToaster;->shouldDisplayToastFor(Ljava/lang/String;Lcom/squareup/print/HardwarePrinter;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 31
    iget-object p1, p0, Lcom/squareup/print/HardwarePrinterHudToaster;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/hardware/R$string;->printer_connected_by_name:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v1, "printer"

    .line 32
    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 33
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 34
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinterHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_PRINTER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    :cond_0
    return-void
.end method

.method public printerDisconnected(Lcom/squareup/print/HardwarePrinter;)V
    .locals 3

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/print/HardwarePrinterHudToaster;->getPrinterNameToDisplay(Lcom/squareup/print/HardwarePrinter;)Ljava/lang/String;

    move-result-object v0

    .line 41
    invoke-direct {p0, v0, p1}, Lcom/squareup/print/HardwarePrinterHudToaster;->shouldDisplayToastFor(Ljava/lang/String;Lcom/squareup/print/HardwarePrinter;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 43
    iget-object p1, p0, Lcom/squareup/print/HardwarePrinterHudToaster;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/hardware/R$string;->printer_disconnected_by_name:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v1, "printer"

    .line 44
    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 45
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 46
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinterHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_PRINTER_DISCONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    :cond_0
    return-void
.end method
