.class public interface abstract Lcom/squareup/print/HardwarePrinterExecutor;
.super Ljava/lang/Object;
.source "HardwarePrinterExecutor.java"


# virtual methods
.method public abstract claimHardwarePrinterAndExecute(Lcom/squareup/print/HardwarePrinter;Ljava/lang/Runnable;Ljava/lang/Runnable;)V
.end method

.method public abstract isHardwarePrinterClaimed(Lcom/squareup/print/HardwarePrinter;)Z
.end method

.method public abstract releaseHardwarePrinter(Lcom/squareup/print/HardwarePrinter;)V
.end method
