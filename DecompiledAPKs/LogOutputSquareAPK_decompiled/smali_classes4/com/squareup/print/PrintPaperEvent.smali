.class public Lcom/squareup/print/PrintPaperEvent;
.super Lcom/squareup/print/PrintStatusEvent;
.source "PrintPaperEvent.java"


# instance fields
.field public final paperLengthInchesEstimate:F

.field public final paperLengthLines:I

.field public final paperLengthPixels:I


# direct methods
.method private constructor <init>(Lcom/squareup/analytics/RegisterActionName;IIFLcom/squareup/print/PrintJob;)V
    .locals 1

    .line 27
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-direct {p0, v0, p1, p5}, Lcom/squareup/print/PrintStatusEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrintJob;)V

    .line 28
    iput p2, p0, Lcom/squareup/print/PrintPaperEvent;->paperLengthPixels:I

    .line 29
    iput p3, p0, Lcom/squareup/print/PrintPaperEvent;->paperLengthLines:I

    .line 30
    iput p4, p0, Lcom/squareup/print/PrintPaperEvent;->paperLengthInchesEstimate:F

    return-void
.end method

.method public static forImpactPaper(Lcom/squareup/analytics/RegisterActionName;IFLcom/squareup/print/PrintJob;)Lcom/squareup/print/PrintPaperEvent;
    .locals 7

    .line 22
    new-instance v6, Lcom/squareup/print/PrintPaperEvent;

    const/4 v2, 0x0

    move-object v0, v6

    move-object v1, p0

    move v3, p1

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/PrintPaperEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;IIFLcom/squareup/print/PrintJob;)V

    return-object v6
.end method

.method public static forThermalPaper(Lcom/squareup/analytics/RegisterActionName;IFLcom/squareup/print/PrintJob;)Lcom/squareup/print/PrintPaperEvent;
    .locals 7

    .line 17
    new-instance v6, Lcom/squareup/print/PrintPaperEvent;

    const/4 v3, 0x0

    move-object v0, v6

    move-object v1, p0

    move v2, p1

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/PrintPaperEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;IIFLcom/squareup/print/PrintJob;)V

    return-object v6
.end method
