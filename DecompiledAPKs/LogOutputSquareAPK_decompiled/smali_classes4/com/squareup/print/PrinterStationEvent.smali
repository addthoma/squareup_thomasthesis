.class public Lcom/squareup/print/PrinterStationEvent;
.super Lcom/squareup/print/PrinterEvent;
.source "PrinterStationEvent.java"


# instance fields
.field public final printsReceipts:Z

.field public final printsTicketStubs:Z

.field public final printsTickets:Z

.field public final stationId:Ljava/lang/String;

.field public final stationName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;)V
    .locals 9

    .line 144
    iget-object v2, p2, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    const/4 p2, 0x0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v8}, Lcom/squareup/print/PrinterEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    new-array p4, p1, [Lcom/squareup/print/PrinterStation$Role;

    .line 146
    sget-object p5, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    aput-object p5, p4, p2

    invoke-virtual {p3, p4}, Lcom/squareup/print/PrinterStation;->hasAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result p4

    iput-boolean p4, p0, Lcom/squareup/print/PrinterStationEvent;->printsReceipts:Z

    new-array p4, p1, [Lcom/squareup/print/PrinterStation$Role;

    .line 147
    sget-object p5, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    aput-object p5, p4, p2

    invoke-virtual {p3, p4}, Lcom/squareup/print/PrinterStation;->hasAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result p4

    iput-boolean p4, p0, Lcom/squareup/print/PrinterStationEvent;->printsTickets:Z

    new-array p1, p1, [Lcom/squareup/print/PrinterStation$Role;

    .line 148
    sget-object p4, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    aput-object p4, p1, p2

    invoke-virtual {p3, p1}, Lcom/squareup/print/PrinterStation;->hasAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/print/PrinterStationEvent;->printsTicketStubs:Z

    .line 149
    invoke-virtual {p3}, Lcom/squareup/print/PrinterStation;->getName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrinterStationEvent;->stationName:Ljava/lang/String;

    .line 150
    invoke-virtual {p3}, Lcom/squareup/print/PrinterStation;->getId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrinterStationEvent;->stationId:Ljava/lang/String;

    return-void
.end method

.method public static forAuthSlipCopyPrinted(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrinterEvent;
    .locals 3

    .line 68
    new-instance v0, Lcom/squareup/print/PrintFinishedEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->PRINT_AUTH_SLIP_COPY_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/print/PrintFinishedEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrintJob;)V

    return-object v0
.end method

.method public static forAuthSlipPrinted(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrinterEvent;
    .locals 3

    .line 60
    new-instance v0, Lcom/squareup/print/PrintFinishedEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->PRINT_AUTH_SLIP_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/print/PrintFinishedEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrintJob;)V

    return-object v0
.end method

.method public static forPrintATicketForEachItemToggle(Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Z)Lcom/squareup/print/PrinterStationEvent;
    .locals 7

    .line 128
    new-instance v6, Lcom/squareup/print/PrinterStationEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->PRINTER_PRINT_A_TICKET_FOR_EACH_ITEM_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    .line 129
    invoke-static {p2}, Lcom/squareup/print/PrinterEventKt;->getDetailString(Z)Ljava/lang/String;

    move-result-object v5

    move-object v0, v6

    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/PrinterStationEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;)V

    return-object v6
.end method

.method public static forPrinterCategoryToggle(Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Z)Lcom/squareup/print/PrinterStationEvent;
    .locals 7

    .line 104
    new-instance v6, Lcom/squareup/print/PrinterStationEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->PRINTER_CATEGORY_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 105
    invoke-static {p2}, Lcom/squareup/print/PrinterEventKt;->getDetailString(Z)Ljava/lang/String;

    move-result-object v5

    move-object v0, v6

    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/PrinterStationEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;)V

    return-object v6
.end method

.method public static forPrinterRoleSettingsToggle(Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Lcom/squareup/print/PrinterStation$Role;Z)Lcom/squareup/print/PrinterStationEvent;
    .locals 6

    .line 83
    sget-object v0, Lcom/squareup/print/PrinterStationEvent$1;->$SwitchMap$com$squareup$print$PrinterStation$Role:[I

    invoke-virtual {p2}, Lcom/squareup/print/PrinterStation$Role;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 91
    sget-object p2, Lcom/squareup/analytics/RegisterActionName;->PRINTER_STUBS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    goto :goto_0

    .line 94
    :cond_0
    new-instance p0, Ljava/lang/RuntimeException;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Printer settings toggle logging event not specified for: "

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 88
    :cond_1
    sget-object p2, Lcom/squareup/analytics/RegisterActionName;->PRINTER_TICKETS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    goto :goto_0

    .line 85
    :cond_2
    sget-object p2, Lcom/squareup/analytics/RegisterActionName;->PRINTER_RECEIPTS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    :goto_0
    move-object v2, p2

    .line 98
    new-instance p2, Lcom/squareup/print/PrinterStationEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 99
    invoke-static {p3}, Lcom/squareup/print/PrinterEventKt;->getDetailString(Z)Ljava/lang/String;

    move-result-object v5

    move-object v0, p2

    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/PrinterStationEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;)V

    return-object p2
.end method

.method public static forPrinterUncategorizedItemsToggle(Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Z)Lcom/squareup/print/PrinterStationEvent;
    .locals 7

    .line 110
    new-instance v6, Lcom/squareup/print/PrinterStationEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->PRINTER_UNCATEGORIZED_ITEMS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 111
    invoke-static {p2}, Lcom/squareup/print/PrinterEventKt;->getDetailString(Z)Ljava/lang/String;

    move-result-object v5

    move-object v0, v6

    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/PrinterStationEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;)V

    return-object v6
.end method

.method public static forReceiptPrinted(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrinterEvent;
    .locals 3

    .line 36
    new-instance v0, Lcom/squareup/print/PrintFinishedEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->PRINT_RECEIPT_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/print/PrintFinishedEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrintJob;)V

    return-object v0
.end method

.method public static forStubPrinted(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrinterEvent;
    .locals 3

    .line 52
    new-instance v0, Lcom/squareup/print/PrintFinishedEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->PRINT_STUB_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/print/PrintFinishedEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrintJob;)V

    return-object v0
.end method

.method public static forTicketAutoNumberToggle(Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Z)Lcom/squareup/print/PrinterStationEvent;
    .locals 7

    .line 116
    new-instance v6, Lcom/squareup/print/PrinterStationEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->PRINTER_AUTONUMBER_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    .line 117
    invoke-static {p2}, Lcom/squareup/print/PrinterEventKt;->getDetailString(Z)Ljava/lang/String;

    move-result-object v5

    move-object v0, v6

    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/PrinterStationEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;)V

    return-object v6
.end method

.method public static forTicketAutoNumberToggleCancel(Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Z)Lcom/squareup/print/PrinterStationEvent;
    .locals 7

    .line 122
    new-instance v6, Lcom/squareup/print/PrinterStationEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->PRINTER_AUTONUMBER_TOGGLE_CANCEL:Lcom/squareup/analytics/RegisterActionName;

    .line 123
    invoke-static {p2}, Lcom/squareup/print/PrinterEventKt;->getDetailString(Z)Ljava/lang/String;

    move-result-object v5

    move-object v0, v6

    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/PrinterStationEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;)V

    return-object v6
.end method

.method public static forTicketPrinted(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrinterEvent;
    .locals 3

    .line 44
    new-instance v0, Lcom/squareup/print/PrintFinishedEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->PRINT_TICKET_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/print/PrintFinishedEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrintJob;)V

    return-object v0
.end method

.method public static forVoidTicketPrinted(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrinterEvent;
    .locals 3

    .line 76
    new-instance v0, Lcom/squareup/print/PrintFinishedEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->PRINT_VOID_TICKET_SUCCEEDED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/print/PrintFinishedEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrintJob;)V

    return-object v0
.end method
