.class public abstract enum Lcom/squareup/pos/loggedout/PosLearnMoreTour;
.super Ljava/lang/Enum;
.source "PosLearnMoreTour.kt"

# interfaces
.implements Lcom/squareup/tour/Tour$HasTourPages;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/pos/loggedout/PosLearnMoreTour$LEARN_MORE_BACKOFFICE;,
        Lcom/squareup/pos/loggedout/PosLearnMoreTour$LEARN_MORE_POS;,
        Lcom/squareup/pos/loggedout/PosLearnMoreTour$LEARN_MORE_READER;,
        Lcom/squareup/pos/loggedout/PosLearnMoreTour$LEARN_MORE_SELLING_SIMPLE;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/pos/loggedout/PosLearnMoreTour;",
        ">;",
        "Lcom/squareup/tour/Tour$HasTourPages;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/pos/loggedout/PosLearnMoreTour;",
        "",
        "Lcom/squareup/tour/Tour$HasTourPages;",
        "(Ljava/lang/String;I)V",
        "LEARN_MORE_BACKOFFICE",
        "LEARN_MORE_POS",
        "LEARN_MORE_READER",
        "LEARN_MORE_SELLING_SIMPLE",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/pos/loggedout/PosLearnMoreTour;

.field public static final enum LEARN_MORE_BACKOFFICE:Lcom/squareup/pos/loggedout/PosLearnMoreTour;

.field public static final enum LEARN_MORE_POS:Lcom/squareup/pos/loggedout/PosLearnMoreTour;

.field public static final enum LEARN_MORE_READER:Lcom/squareup/pos/loggedout/PosLearnMoreTour;

.field public static final enum LEARN_MORE_SELLING_SIMPLE:Lcom/squareup/pos/loggedout/PosLearnMoreTour;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/pos/loggedout/PosLearnMoreTour;

    new-instance v1, Lcom/squareup/pos/loggedout/PosLearnMoreTour$LEARN_MORE_BACKOFFICE;

    const/4 v2, 0x0

    const-string v3, "LEARN_MORE_BACKOFFICE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/pos/loggedout/PosLearnMoreTour$LEARN_MORE_BACKOFFICE;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/pos/loggedout/PosLearnMoreTour;->LEARN_MORE_BACKOFFICE:Lcom/squareup/pos/loggedout/PosLearnMoreTour;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/pos/loggedout/PosLearnMoreTour$LEARN_MORE_POS;

    const/4 v2, 0x1

    const-string v3, "LEARN_MORE_POS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/pos/loggedout/PosLearnMoreTour$LEARN_MORE_POS;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/pos/loggedout/PosLearnMoreTour;->LEARN_MORE_POS:Lcom/squareup/pos/loggedout/PosLearnMoreTour;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/pos/loggedout/PosLearnMoreTour$LEARN_MORE_READER;

    const/4 v2, 0x2

    const-string v3, "LEARN_MORE_READER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/pos/loggedout/PosLearnMoreTour$LEARN_MORE_READER;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/pos/loggedout/PosLearnMoreTour;->LEARN_MORE_READER:Lcom/squareup/pos/loggedout/PosLearnMoreTour;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/pos/loggedout/PosLearnMoreTour$LEARN_MORE_SELLING_SIMPLE;

    const/4 v2, 0x3

    const-string v3, "LEARN_MORE_SELLING_SIMPLE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/pos/loggedout/PosLearnMoreTour$LEARN_MORE_SELLING_SIMPLE;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/pos/loggedout/PosLearnMoreTour;->LEARN_MORE_SELLING_SIMPLE:Lcom/squareup/pos/loggedout/PosLearnMoreTour;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/pos/loggedout/PosLearnMoreTour;->$VALUES:[Lcom/squareup/pos/loggedout/PosLearnMoreTour;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1, p2}, Lcom/squareup/pos/loggedout/PosLearnMoreTour;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/pos/loggedout/PosLearnMoreTour;
    .locals 1

    const-class v0, Lcom/squareup/pos/loggedout/PosLearnMoreTour;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/pos/loggedout/PosLearnMoreTour;

    return-object p0
.end method

.method public static values()[Lcom/squareup/pos/loggedout/PosLearnMoreTour;
    .locals 1

    sget-object v0, Lcom/squareup/pos/loggedout/PosLearnMoreTour;->$VALUES:[Lcom/squareup/pos/loggedout/PosLearnMoreTour;

    invoke-virtual {v0}, [Lcom/squareup/pos/loggedout/PosLearnMoreTour;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/pos/loggedout/PosLearnMoreTour;

    return-object v0
.end method
