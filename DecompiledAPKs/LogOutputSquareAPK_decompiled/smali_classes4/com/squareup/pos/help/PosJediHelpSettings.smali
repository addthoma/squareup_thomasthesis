.class public final Lcom/squareup/pos/help/PosJediHelpSettings;
.super Ljava/lang/Object;
.source "PosJediHelpSettings.kt"

# interfaces
.implements Lcom/squareup/jedi/JediHelpSettings;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\u000f\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\tR\u0014\u0010\n\u001a\u00020\u000bX\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0014\u0010\u000e\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\t\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/pos/help/PosJediHelpSettings;",
        "Lcom/squareup/jedi/JediHelpSettings;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/settings/server/Features;)V",
        "getFeatures",
        "()Lcom/squareup/settings/server/Features;",
        "isJediEnabled",
        "",
        "()Z",
        "jediClientKey",
        "",
        "getJediClientKey",
        "()Ljava/lang/String;",
        "showInlineLinks",
        "getShowInlineLinks",
        "spos-help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final isJediEnabled:Z

.field private final jediClientKey:Ljava/lang/String;

.field private final showInlineLinks:Z


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/pos/help/PosJediHelpSettings;->features:Lcom/squareup/settings/server/Features;

    .line 11
    iget-object p1, p0, Lcom/squareup/pos/help/PosJediHelpSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->HELP_APPLET_JEDI:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/pos/help/PosJediHelpSettings;->isJediEnabled:Z

    const-string p1, "android-pos"

    .line 12
    iput-object p1, p0, Lcom/squareup/pos/help/PosJediHelpSettings;->jediClientKey:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCallUsPanelToken()Ljava/lang/String;
    .locals 1

    .line 8
    invoke-static {p0}, Lcom/squareup/jedi/JediHelpSettings$DefaultImpls;->getCallUsPanelToken(Lcom/squareup/jedi/JediHelpSettings;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getFeatures()Lcom/squareup/settings/server/Features;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/pos/help/PosJediHelpSettings;->features:Lcom/squareup/settings/server/Features;

    return-object v0
.end method

.method public getJediClientKey()Ljava/lang/String;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/pos/help/PosJediHelpSettings;->jediClientKey:Ljava/lang/String;

    return-object v0
.end method

.method public getShowInlineLinks()Z
    .locals 1

    .line 13
    iget-boolean v0, p0, Lcom/squareup/pos/help/PosJediHelpSettings;->showInlineLinks:Z

    return v0
.end method

.method public isJediEnabled()Z
    .locals 1

    .line 11
    iget-boolean v0, p0, Lcom/squareup/pos/help/PosJediHelpSettings;->isJediEnabled:Z

    return v0
.end method
