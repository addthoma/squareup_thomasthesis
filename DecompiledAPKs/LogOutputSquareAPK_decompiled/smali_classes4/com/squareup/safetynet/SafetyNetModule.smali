.class public abstract Lcom/squareup/safetynet/SafetyNetModule;
.super Ljava/lang/Object;
.source "SafetyNetModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/gms/common/PlayServicesModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getClient(Landroid/app/Application;)Lcom/google/android/gms/safetynet/SafetyNetClient;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 14
    invoke-static {p0}, Lcom/google/android/gms/safetynet/SafetyNet;->getClient(Landroid/content/Context;)Lcom/google/android/gms/safetynet/SafetyNetClient;

    move-result-object p0

    return-object p0
.end method
