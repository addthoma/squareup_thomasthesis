.class interface abstract Lcom/squareup/persistentbundle/PersistentBundleStore;
.super Ljava/lang/Object;
.source "PersistentBundleStore.java"


# virtual methods
.method public abstract add(Ljava/lang/String;[B)V
.end method

.method public abstract contains(Ljava/lang/String;)Z
.end method

.method public abstract getAndRemove(Ljava/lang/String;)Landroid/os/Bundle;
.end method

.method public abstract logBundleSize(JZ)V
.end method

.method public abstract remove(Ljava/lang/String;)V
.end method
