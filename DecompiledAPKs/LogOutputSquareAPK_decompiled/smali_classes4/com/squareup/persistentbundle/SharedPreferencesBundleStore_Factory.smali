.class public final Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;
.super Ljava/lang/Object;
.source "SharedPreferencesBundleStore_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p6, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 42
    iput-object p7, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;->arg6Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;"
        }
    .end annotation

    .line 54
    new-instance v8, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/analytics/Analytics;Ljava/util/concurrent/Executor;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Clock;I)Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;
    .locals 9

    .line 59
    new-instance v8, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;-><init>(Landroid/app/Application;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/analytics/Analytics;Ljava/util/concurrent/Executor;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Clock;I)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;
    .locals 8

    .line 47
    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/log/OhSnapLogger;

    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static/range {v1 .. v7}, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/analytics/Analytics;Ljava/util/concurrent/Executor;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Clock;I)Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore_Factory;->get()Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;

    move-result-object v0

    return-object v0
.end method
