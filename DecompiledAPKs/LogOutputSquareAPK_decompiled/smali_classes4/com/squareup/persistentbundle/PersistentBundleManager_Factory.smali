.class public final Lcom/squareup/persistentbundle/PersistentBundleManager_Factory;
.super Ljava/lang/Object;
.source "PersistentBundleManager_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/persistentbundle/PersistentBundleManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final sharedPreferencesBundleStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/persistentbundle/PersistentBundleManager_Factory;->sharedPreferencesBundleStoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/persistentbundle/PersistentBundleManager_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;",
            ">;)",
            "Lcom/squareup/persistentbundle/PersistentBundleManager_Factory;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/persistentbundle/PersistentBundleManager_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/persistentbundle/PersistentBundleManager_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljava/lang/Object;)Lcom/squareup/persistentbundle/PersistentBundleManager;
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/persistentbundle/PersistentBundleManager;

    check-cast p0, Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;

    invoke-direct {v0, p0}, Lcom/squareup/persistentbundle/PersistentBundleManager;-><init>(Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/persistentbundle/PersistentBundleManager;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/persistentbundle/PersistentBundleManager_Factory;->sharedPreferencesBundleStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/persistentbundle/PersistentBundleManager_Factory;->newInstance(Ljava/lang/Object;)Lcom/squareup/persistentbundle/PersistentBundleManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/persistentbundle/PersistentBundleManager_Factory;->get()Lcom/squareup/persistentbundle/PersistentBundleManager;

    move-result-object v0

    return-object v0
.end method
