.class Lcom/squareup/payment/RealOfflineModeMonitor$GoingOnlineState;
.super Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;
.source "RealOfflineModeMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/RealOfflineModeMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GoingOnlineState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/RealOfflineModeMonitor;


# direct methods
.method private constructor <init>(Lcom/squareup/payment/RealOfflineModeMonitor;)V
    .locals 0

    .line 341
    iput-object p1, p0, Lcom/squareup/payment/RealOfflineModeMonitor$GoingOnlineState;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/RealOfflineModeMonitor;Lcom/squareup/payment/RealOfflineModeMonitor$1;)V
    .locals 0

    .line 341
    invoke-direct {p0, p1}, Lcom/squareup/payment/RealOfflineModeMonitor$GoingOnlineState;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor;)V

    return-void
.end method


# virtual methods
.method public init()V
    .locals 2

    .line 364
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor$GoingOnlineState;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/payment/RealOfflineModeMonitor;->access$800(Lcom/squareup/payment/RealOfflineModeMonitor;Z)V

    return-void
.end method

.method public isUnavailable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public recordServiceAvailable(Z)Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;
    .locals 2

    .line 345
    iget-object p1, p0, Lcom/squareup/payment/RealOfflineModeMonitor$GoingOnlineState;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    invoke-static {p1}, Lcom/squareup/payment/RealOfflineModeMonitor;->access$600(Lcom/squareup/payment/RealOfflineModeMonitor;)Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 346
    new-instance p1, Lcom/squareup/payment/RealOfflineModeMonitor$WaitingForPaymentToCompleteState;

    iget-object v1, p0, Lcom/squareup/payment/RealOfflineModeMonitor$GoingOnlineState;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    invoke-direct {p1, v1, v0}, Lcom/squareup/payment/RealOfflineModeMonitor$WaitingForPaymentToCompleteState;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor;Lcom/squareup/payment/RealOfflineModeMonitor$1;)V

    return-object p1

    .line 348
    :cond_0
    new-instance p1, Lcom/squareup/payment/RealOfflineModeMonitor$OnlineState;

    iget-object v1, p0, Lcom/squareup/payment/RealOfflineModeMonitor$GoingOnlineState;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    invoke-direct {p1, v1, v0}, Lcom/squareup/payment/RealOfflineModeMonitor$OnlineState;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor;Lcom/squareup/payment/RealOfflineModeMonitor$1;)V

    return-object p1
.end method

.method public recordServiceUnavailable(Z)Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;
    .locals 2

    .line 354
    new-instance p1, Lcom/squareup/payment/RealOfflineModeMonitor$OfflineState;

    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor$GoingOnlineState;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, Lcom/squareup/payment/RealOfflineModeMonitor$OfflineState;-><init>(Lcom/squareup/payment/RealOfflineModeMonitor;Lcom/squareup/payment/RealOfflineModeMonitor$1;)V

    return-object p1
.end method
