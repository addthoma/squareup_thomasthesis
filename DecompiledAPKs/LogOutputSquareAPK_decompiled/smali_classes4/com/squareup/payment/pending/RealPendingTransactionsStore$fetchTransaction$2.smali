.class final synthetic Lcom/squareup/payment/pending/RealPendingTransactionsStore$fetchTransaction$2;
.super Lkotlin/jvm/internal/FunctionReference;
.source "RealPendingTransactionsStore.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/pending/RealPendingTransactionsStore;->fetchTransaction(Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/util/Optional<",
        "+",
        "Lcom/squareup/billhistory/model/BillHistory;",
        ">;",
        "Lcom/squareup/util/Optional<",
        "+",
        "Lcom/squareup/transactionhistory/pending/PendingTransaction;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0001\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/transactionhistory/pending/PendingTransaction;",
        "p1",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/payment/pending/RealPendingTransactionsStore$fetchTransaction$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$fetchTransaction$2;

    invoke-direct {v0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$fetchTransaction$2;-><init>()V

    sput-object v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$fetchTransaction$2;->INSTANCE:Lcom/squareup/payment/pending/RealPendingTransactionsStore$fetchTransaction$2;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/FunctionReference;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "toOptionalPendingTransaction"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 2

    const-class v0, Lcom/squareup/transactionhistory/mappings/pending/PendingTransactionMappersKt;

    const-string v1, "payment_release"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinPackage(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/reflect/KDeclarationContainer;

    move-result-object v0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "toOptionalPendingTransaction(Lcom/squareup/util/Optional;)Lcom/squareup/util/Optional;"

    return-object v0
.end method

.method public final invoke(Lcom/squareup/util/Optional;)Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "+",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;)",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/transactionhistory/pending/PendingTransaction;",
            ">;"
        }
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 332
    invoke-static {p1}, Lcom/squareup/transactionhistory/mappings/pending/PendingTransactionMappersKt;->toOptionalPendingTransaction(Lcom/squareup/util/Optional;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 54
    check-cast p1, Lcom/squareup/util/Optional;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$fetchTransaction$2;->invoke(Lcom/squareup/util/Optional;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method
