.class public final Lcom/squareup/payment/pending/RealPendingTransactionsStoreKt;
.super Ljava/lang/Object;
.source "RealPendingTransactionsStore.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010!\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0000\"2\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00040\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"
    }
    d2 = {
        "EMPTY_PROCESSED_FORWARDED_TRANSACTION_IDS",
        "",
        "",
        "kotlin.jvm.PlatformType",
        "",
        "payment_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final EMPTY_PROCESSED_FORWARDED_TRANSACTION_IDS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 496
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/payment/pending/RealPendingTransactionsStoreKt;->EMPTY_PROCESSED_FORWARDED_TRANSACTION_IDS:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$getEMPTY_PROCESSED_FORWARDED_TRANSACTION_IDS$p()Ljava/util/List;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/payment/pending/RealPendingTransactionsStoreKt;->EMPTY_PROCESSED_FORWARDED_TRANSACTION_IDS:Ljava/util/List;

    return-object v0
.end method
