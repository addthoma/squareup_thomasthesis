.class final Lcom/squareup/payment/pending/RealPendingTransactionsStore$allForwardedTransactionSummaries$1;
.super Ljava/lang/Object;
.source "RealPendingTransactionsStore.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/pending/RealPendingTransactionsStore;->allForwardedTransactionSummaries()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012(\u0010\u0003\u001a$\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0010\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00070\u0004H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
        "it",
        "",
        "Lcom/squareup/payment/offline/ForwardedPayment;",
        "kotlin.jvm.PlatformType",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/pending/RealPendingTransactionsStore;


# direct methods
.method constructor <init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$allForwardedTransactionSummaries$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 54
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$allForwardedTransactionSummaries$1;->apply(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Ljava/util/Collection;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 256
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$allForwardedTransactionSummaries$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsStore;

    invoke-static {v0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore;->access$getRes$p(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)Lcom/squareup/util/Res;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/transactionhistory/mappings/pending/PendingTransactionMappersKt;->toPendingTransactionSummaries(Ljava/util/Collection;Lcom/squareup/util/Res;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
