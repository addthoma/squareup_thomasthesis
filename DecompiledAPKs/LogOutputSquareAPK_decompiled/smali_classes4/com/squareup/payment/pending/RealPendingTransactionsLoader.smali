.class public final Lcom/squareup/payment/pending/RealPendingTransactionsLoader;
.super Ljava/lang/Object;
.source "RealPendingTransactionsLoader.kt"

# interfaces
.implements Lcom/squareup/transactionhistory/pending/PendingTransactionsLoader;
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B!\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0014\u0010\u000e\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000c0\u00100\u000fH\u0016J\u0014\u0010\u001b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00140\u00130\u000fH\u0016J\u0008\u0010\u001c\u001a\u00020\u001dH\u0016J\u000e\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u000fH\u0016J\u000e\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u000fH\u0016J\u0010\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0008\u0010!\u001a\u00020\u001dH\u0016J\u0008\u0010\"\u001a\u00020\u001dH\u0002J\u0008\u0010#\u001a\u00020\u001dH\u0016J \u0010$\u001a\u00020\u001d2\u0016\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\u000c0\u000bj\u0008\u0012\u0004\u0012\u00020\u000c`\rH\u0016J\u0012\u0010%\u001a\u00020&*\u0008\u0012\u0004\u0012\u00020\u00180\u0012H\u0002R\u001e\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\u000c0\u000bj\u0008\u0012\u0004\u0012\u00020\u000c`\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000e\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000c0\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0011\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00140\u00130\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/payment/pending/RealPendingTransactionsLoader;",
        "Lcom/squareup/transactionhistory/pending/PendingTransactionsLoader;",
        "Lmortar/Scoped;",
        "mainThread",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "transactionsStore",
        "Lcom/squareup/payment/pending/PendingTransactionsStore;",
        "(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lio/reactivex/Scheduler;Lcom/squareup/payment/pending/PendingTransactionsStore;)V",
        "comparator",
        "Ljava/util/Comparator;",
        "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
        "Lkotlin/Comparator;",
        "cumulativeSummaries",
        "Lio/reactivex/Observable;",
        "",
        "lastError",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/receiving/FailureMessage;",
        "lastRequest",
        "Lcom/squareup/payment/pending/RequestType;",
        "loadingSequenceState",
        "Lcom/squareup/transactionhistory/LoadingSequenceState;",
        "loadingState",
        "Lcom/squareup/transactionhistory/TransactionsLoaderState;",
        "error",
        "loadNext",
        "",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "resetLoader",
        "restart",
        "setSortOrder",
        "isStart",
        "",
        "payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
            ">;"
        }
    .end annotation
.end field

.field private final cumulativeSummaries:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
            ">;>;"
        }
    .end annotation
.end field

.field private final lastError:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/receiving/FailureMessage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final lastRequest:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/payment/pending/RequestType;",
            ">;"
        }
    .end annotation
.end field

.field private final loadingSequenceState:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/transactionhistory/LoadingSequenceState;",
            ">;"
        }
    .end annotation
.end field

.field private final loadingState:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/transactionhistory/TransactionsLoaderState;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;


# direct methods
.method public constructor <init>(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lio/reactivex/Scheduler;Lcom/squareup/payment/pending/PendingTransactionsStore;)V
    .locals 1
    .param p1    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param

    const-string v0, "mainThread"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionsStore"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iput-object p2, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->mainScheduler:Lio/reactivex/Scheduler;

    .line 70
    sget-object p1, Lcom/squareup/payment/pending/RequestType;->NONE:Lcom/squareup/payment/pending/RequestType;

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(NONE)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->lastRequest:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 84
    iget-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->lastRequest:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 85
    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    .line 86
    new-instance p2, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;

    invoke-direct {p2, p0, p3}, Lcom/squareup/payment/pending/RealPendingTransactionsLoader$cumulativeSummaries$1;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsLoader;Lcom/squareup/payment/pending/PendingTransactionsStore;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "lastRequest\n        .dis\u2026  }\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->cumulativeSummaries:Lio/reactivex/Observable;

    .line 118
    sget-object p1, Lcom/squareup/transactionhistory/LoadingSequenceState;->START:Lcom/squareup/transactionhistory/LoadingSequenceState;

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(START)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->loadingSequenceState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 126
    sget-object p1, Lcom/squareup/transactionhistory/TransactionsLoaderState;->START:Lcom/squareup/transactionhistory/TransactionsLoaderState;

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefa\u2026actionsLoaderState.START)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->loadingState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 133
    invoke-static {}, Lcom/squareup/transactionhistory/TransactionsLoaderKt;->getNO_ERROR()Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(NO_ERROR)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->lastError:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 140
    invoke-static {}, Lcom/squareup/payment/pending/RealPendingTransactionsLoaderKt;->getNO_OP_COMPARATOR()Ljava/util/Comparator;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->comparator:Ljava/util/Comparator;

    return-void
.end method

.method public static final synthetic access$getComparator$p(Lcom/squareup/payment/pending/RealPendingTransactionsLoader;)Ljava/util/Comparator;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->comparator:Ljava/util/Comparator;

    return-object p0
.end method

.method public static final synthetic access$getLoadingSequenceState$p(Lcom/squareup/payment/pending/RealPendingTransactionsLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->loadingSequenceState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getLoadingState$p(Lcom/squareup/payment/pending/RealPendingTransactionsLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->loadingState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$setComparator$p(Lcom/squareup/payment/pending/RealPendingTransactionsLoader;Ljava/util/Comparator;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->comparator:Ljava/util/Comparator;

    return-void
.end method

.method private final isStart(Lcom/jakewharton/rxrelay2/BehaviorRelay;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/transactionhistory/LoadingSequenceState;",
            ">;)Z"
        }
    .end annotation

    .line 207
    invoke-static {p1}, Lcom/squareup/util/rx2/Rx2Kt;->requireValue(Lcom/jakewharton/rxrelay2/BehaviorRelay;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/transactionhistory/LoadingSequenceState;

    sget-object v0, Lcom/squareup/transactionhistory/LoadingSequenceState;->START:Lcom/squareup/transactionhistory/LoadingSequenceState;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final resetLoader()V
    .locals 2

    .line 198
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->loadingState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/transactionhistory/TransactionsLoaderState;->START:Lcom/squareup/transactionhistory/TransactionsLoaderState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 199
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->loadingSequenceState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/transactionhistory/LoadingSequenceState;->START:Lcom/squareup/transactionhistory/LoadingSequenceState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public cumulativeSummaries()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
            ">;>;"
        }
    .end annotation

    .line 184
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->cumulativeSummaries:Lio/reactivex/Observable;

    return-object v0
.end method

.method public error()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/receiving/FailureMessage;",
            ">;>;"
        }
    .end annotation

    .line 219
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->lastError:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 220
    iget-object v1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 221
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "lastError\n        .obser\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public loadNext()V
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 157
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->loadingSequenceState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-direct {p0, v0}, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->isStart(Lcom/jakewharton/rxrelay2/BehaviorRelay;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->lastRequest:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/payment/pending/RequestType;->LOAD_NEXT:Lcom/squareup/payment/pending/RequestType;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public loadingSequenceState()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/transactionhistory/LoadingSequenceState;",
            ">;"
        }
    .end annotation

    .line 203
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->loadingSequenceState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 204
    iget-object v1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 205
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "loadingSequenceState\n   \u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public loadingState()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/transactionhistory/TransactionsLoaderState;",
            ">;"
        }
    .end annotation

    .line 210
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->loadingState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 211
    iget-object v1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 212
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "loadingState\n        .ob\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->cumulativeSummaries:Lio/reactivex/Observable;

    invoke-virtual {v0}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "cumulativeSummaries.subscribe()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public restart()V
    .locals 2

    .line 192
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 193
    invoke-direct {p0}, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->resetLoader()V

    .line 194
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->lastRequest:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/payment/pending/RequestType;->RESTART:Lcom/squareup/payment/pending/RequestType;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public setSortOrder(Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator<",
            "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
            ">;)V"
        }
    .end annotation

    const-string v0, "comparator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->mainThread:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 188
    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsLoader;->comparator:Ljava/util/Comparator;

    return-void
.end method
