.class final Lcom/squareup/payment/PaymentReceipt$RealFactory;
.super Ljava/lang/Object;
.source "PaymentReceipt.java"

# interfaces
.implements Lcom/squareup/payment/PaymentReceipt$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/PaymentReceipt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "RealFactory"
.end annotation


# instance fields
.field private final cartProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/payment/PaymentReceipt$RealFactory;->cartProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private getSnapshotOfOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/OrderSnapshot;
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 66
    :cond_0
    instance-of v0, p1, Lcom/squareup/payment/OrderSnapshot;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/payment/OrderSnapshot;

    goto :goto_0

    .line 68
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->snapshot()Lcom/squareup/payment/OrderSnapshot;

    move-result-object p1

    :goto_0
    return-object p1
.end method


# virtual methods
.method public createTenderReceipt(Lcom/squareup/payment/Obfuscated;Lcom/squareup/payment/Obfuscated;)Lcom/squareup/payment/PaymentReceipt;
    .locals 4

    .line 50
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$RealFactory;->cartProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/Transaction;

    .line 51
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/payment/PaymentReceipt$RealFactory;->getSnapshotOfOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/OrderSnapshot;

    move-result-object v1

    .line 52
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getDisplayNameOrDefault()Ljava/lang/String;

    move-result-object v2

    .line 54
    new-instance v3, Lcom/squareup/payment/PaymentReceipt$Builder;

    invoke-direct {v3}, Lcom/squareup/payment/PaymentReceipt$Builder;-><init>()V

    .line 55
    invoke-virtual {v3, p1}, Lcom/squareup/payment/PaymentReceipt$Builder;->defaultEmail(Lcom/squareup/payment/Obfuscated;)Lcom/squareup/payment/PaymentReceipt$Builder;

    move-result-object p1

    .line 56
    invoke-virtual {p1, p2}, Lcom/squareup/payment/PaymentReceipt$Builder;->defaultSms(Lcom/squareup/payment/Obfuscated;)Lcom/squareup/payment/PaymentReceipt$Builder;

    move-result-object p1

    .line 57
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/payment/PaymentReceipt$Builder;->payment(Lcom/squareup/payment/Payment;)Lcom/squareup/payment/PaymentReceipt$Builder;

    move-result-object p1

    .line 58
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getDisplayName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/payment/PaymentReceipt$Builder;->ticketName(Ljava/lang/CharSequence;)Lcom/squareup/payment/PaymentReceipt$Builder;

    move-result-object p1

    .line 59
    invoke-virtual {p1, v1}, Lcom/squareup/payment/PaymentReceipt$Builder;->orderSnapshot(Lcom/squareup/payment/OrderSnapshot;)Lcom/squareup/payment/PaymentReceipt$Builder;

    move-result-object p1

    .line 60
    invoke-virtual {p1, v2}, Lcom/squareup/payment/PaymentReceipt$Builder;->orderDisplayName(Ljava/lang/String;)Lcom/squareup/payment/PaymentReceipt$Builder;

    move-result-object p1

    .line 61
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt$Builder;->buildTenderReceipt()Lcom/squareup/payment/PaymentReceipt$TenderReceipt;

    move-result-object p1

    return-object p1
.end method

.method public createVoidReceipt()Lcom/squareup/payment/PaymentReceipt;
    .locals 3

    .line 40
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt$RealFactory;->cartProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/Transaction;

    .line 42
    new-instance v1, Lcom/squareup/payment/PaymentReceipt$Builder;

    invoke-direct {v1}, Lcom/squareup/payment/PaymentReceipt$Builder;-><init>()V

    .line 43
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/payment/PaymentReceipt$Builder;->payment(Lcom/squareup/payment/Payment;)Lcom/squareup/payment/PaymentReceipt$Builder;

    move-result-object v1

    .line 44
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/payment/PaymentReceipt$Builder;->ticketName(Ljava/lang/CharSequence;)Lcom/squareup/payment/PaymentReceipt$Builder;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt$Builder;->buildVoidReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    return-object v0
.end method
