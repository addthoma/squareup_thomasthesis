.class public Lcom/squareup/payment/PaymentModule$Prod;
.super Ljava/lang/Object;
.source "PaymentModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/PaymentModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Prod"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideConnectivityCheck(Lretrofit/client/Client;)Lcom/squareup/connectivity/check/ConnectivityCheck;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/connectivity/check/RealConnectivityCheck;

    const-string v1, "https://api-global.squareup.com/"

    invoke-direct {v0, p0, v1}, Lcom/squareup/connectivity/check/RealConnectivityCheck;-><init>(Lretrofit/client/Client;Ljava/lang/String;)V

    return-object v0
.end method
