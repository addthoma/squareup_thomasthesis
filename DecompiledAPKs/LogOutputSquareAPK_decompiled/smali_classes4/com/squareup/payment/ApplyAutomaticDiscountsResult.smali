.class public Lcom/squareup/payment/ApplyAutomaticDiscountsResult;
.super Ljava/lang/Object;
.source "ApplyAutomaticDiscountsResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;
    }
.end annotation


# instance fields
.field private final added:I

.field private final removed:I

.field private final replacements:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private final split:I


# direct methods
.method public constructor <init>(IIILjava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;>;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput p1, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->added:I

    .line 23
    iput p2, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->removed:I

    .line 24
    iput p3, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->split:I

    .line 25
    invoke-static {p4}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->replacements:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public anyChanged()Z
    .locals 1

    .line 61
    iget v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->added:I

    if-gtz v0, :cond_1

    iget v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->removed:I

    if-gtz v0, :cond_1

    iget v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->split:I

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public getAdded()I
    .locals 1

    .line 32
    iget v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->added:I

    return v0
.end method

.method public getRemoved()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->removed:I

    return v0
.end method

.method public getReplacements()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;>;"
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->replacements:Ljava/util/Map;

    return-object v0
.end method

.method public getSplit()I
    .locals 1

    .line 46
    iget v0, p0, Lcom/squareup/payment/ApplyAutomaticDiscountsResult;->split:I

    return v0
.end method
