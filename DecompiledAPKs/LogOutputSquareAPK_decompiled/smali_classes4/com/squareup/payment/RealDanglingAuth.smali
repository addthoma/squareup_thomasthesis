.class public Lcom/squareup/payment/RealDanglingAuth;
.super Ljava/lang/Object;
.source "RealDanglingAuth.java"

# interfaces
.implements Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;,
        Lcom/squareup/payment/RealDanglingAuth$BillPaymentVoidable;,
        Lcom/squareup/payment/RealDanglingAuth$CardPaymentVoidable;,
        Lcom/squareup/payment/RealDanglingAuth$Voidable;
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final lastAuthKey:Lcom/squareup/persistent/AtomicSyncedValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/persistent/AtomicSyncedValue<",
            "Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

.field private final paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

.field private final serverClock:Lcom/squareup/account/ServerClock;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tasksProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

.field private final voidMonitor:Lcom/squareup/payment/VoidMonitor;


# direct methods
.method constructor <init>(Lcom/squareup/account/ServerClock;Lcom/squareup/persistent/AtomicSyncedValue;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/payment/PaymentAccuracyLogger;Lcom/squareup/payment/VoidMonitor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/account/ServerClock;",
            "Lcom/squareup/persistent/AtomicSyncedValue<",
            "Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/log/OhSnapLogger;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            "Lcom/squareup/payment/VoidMonitor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/squareup/payment/RealDanglingAuth;->serverClock:Lcom/squareup/account/ServerClock;

    .line 61
    iput-object p2, p0, Lcom/squareup/payment/RealDanglingAuth;->lastAuthKey:Lcom/squareup/persistent/AtomicSyncedValue;

    .line 62
    iput-object p3, p0, Lcom/squareup/payment/RealDanglingAuth;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 63
    iput-object p4, p0, Lcom/squareup/payment/RealDanglingAuth;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 64
    iput-object p6, p0, Lcom/squareup/payment/RealDanglingAuth;->tasksProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p7, p0, Lcom/squareup/payment/RealDanglingAuth;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    .line 66
    iput-object p5, p0, Lcom/squareup/payment/RealDanglingAuth;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 67
    iput-object p8, p0, Lcom/squareup/payment/RealDanglingAuth;->paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

    .line 68
    iput-object p9, p0, Lcom/squareup/payment/RealDanglingAuth;->voidMonitor:Lcom/squareup/payment/VoidMonitor;

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/payment/RealDanglingAuth;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/payment/RealDanglingAuth;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/payment/RealDanglingAuth;)Lcom/squareup/payment/VoidMonitor;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/payment/RealDanglingAuth;->voidMonitor:Lcom/squareup/payment/VoidMonitor;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/payment/RealDanglingAuth;)Ljavax/inject/Provider;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/payment/RealDanglingAuth;->tasksProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/payment/RealDanglingAuth;)Lcom/squareup/payment/ledger/TransactionLedgerManager;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/payment/RealDanglingAuth;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/payment/RealDanglingAuth;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/payment/RealDanglingAuth;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method

.method private readLastAuth()Lcom/squareup/payment/RealDanglingAuth$Voidable;
    .locals 5

    const/4 v0, 0x0

    .line 73
    :try_start_0
    iget-object v1, p0, Lcom/squareup/payment/RealDanglingAuth;->lastAuthKey:Lcom/squareup/persistent/AtomicSyncedValue;

    invoke-interface {v1}, Lcom/squareup/persistent/AtomicSyncedValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;

    if-nez v1, :cond_0

    return-object v0

    .line 76
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 77
    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 84
    :cond_1
    sget-object v2, Lcom/squareup/payment/RealDanglingAuth$1;->$SwitchMap$com$squareup$PaymentType:[I

    invoke-virtual {v1}, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->getType()Lcom/squareup/PaymentType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/PaymentType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const/4 v3, 0x1

    if-eq v2, v3, :cond_4

    const/4 v3, 0x2

    if-eq v2, v3, :cond_3

    const/4 v3, 0x3

    if-eq v2, v3, :cond_2

    .line 95
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown payment type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->getType()Lcom/squareup/PaymentType;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    :cond_2
    return-object v0

    .line 88
    :cond_3
    new-instance v2, Lcom/squareup/payment/RealDanglingAuth$BillPaymentVoidable;

    invoke-direct {v2, p0, v1}, Lcom/squareup/payment/RealDanglingAuth$BillPaymentVoidable;-><init>(Lcom/squareup/payment/RealDanglingAuth;Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;)V

    return-object v2

    .line 86
    :cond_4
    new-instance v2, Lcom/squareup/payment/RealDanglingAuth$CardPaymentVoidable;

    invoke-direct {v2, p0, v1}, Lcom/squareup/payment/RealDanglingAuth$CardPaymentVoidable;-><init>(Lcom/squareup/payment/RealDanglingAuth;Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;)V

    return-object v2

    .line 78
    :cond_5
    :goto_0
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unexpected empty billId"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v1

    const-string v2, "Error reading last authorization key."

    .line 103
    invoke-static {v1, v2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public clearLastAuth()V
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth;->lastAuthKey:Lcom/squareup/persistent/AtomicSyncedValue;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/persistent/AtomicSyncedValue;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public getDanglingAuthBillId()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth;->lastAuthKey:Lcom/squareup/persistent/AtomicSyncedValue;

    invoke-interface {v0}, Lcom/squareup/persistent/AtomicSyncedValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 114
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public hasDanglingAuth()Z
    .locals 1

    .line 109
    invoke-direct {p0}, Lcom/squareup/payment/RealDanglingAuth;->readLastAuth()Lcom/squareup/payment/RealDanglingAuth$Voidable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onFailedAuth(Lcom/squareup/protos/client/IdPair;)V
    .locals 9

    .line 132
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 133
    invoke-direct {p0}, Lcom/squareup/payment/RealDanglingAuth;->readLastAuth()Lcom/squareup/payment/RealDanglingAuth$Voidable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 134
    iget-object v1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iget-object v2, v0, Lcom/squareup/payment/RealDanglingAuth$Voidable;->billId:Lcom/squareup/protos/client/IdPair;

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 136
    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_OTHER:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    const-string v2, "Overwriting last auth"

    invoke-virtual {p0, v1, v2}, Lcom/squareup/payment/RealDanglingAuth;->voidLastAuth(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/String;)Lcom/squareup/protos/common/Money;

    :cond_0
    if-nez v0, :cond_1

    .line 138
    sget-object v1, Lcom/squareup/PaymentType;->BILL2:Lcom/squareup/PaymentType;

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/squareup/payment/RealDanglingAuth$Voidable;->authorizationInfo:Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;

    invoke-static {v1}, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->access$000(Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;)Lcom/squareup/PaymentType;

    move-result-object v1

    :goto_0
    move-object v6, v1

    if-eqz v0, :cond_2

    .line 139
    iget-object v1, v0, Lcom/squareup/payment/RealDanglingAuth$Voidable;->authorizationInfo:Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;

    invoke-static {v1}, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->access$100(Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    const/4 v8, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    const/4 v8, 0x0

    :goto_1
    if-nez v0, :cond_3

    .line 140
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_2

    :cond_3
    iget-object v0, v0, Lcom/squareup/payment/RealDanglingAuth$Voidable;->authorizationInfo:Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;

    invoke-static {v0}, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;->access$200(Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;)Ljava/util/List;

    move-result-object v0

    :goto_2
    move-object v7, v0

    .line 141
    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth;->lastAuthKey:Lcom/squareup/persistent/AtomicSyncedValue;

    new-instance v1, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;

    const-wide/16 v3, -0x1

    move-object v2, v1

    move-object v5, p1

    invoke-direct/range {v2 .. v8}, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;-><init>(JLcom/squareup/protos/client/IdPair;Lcom/squareup/PaymentType;Ljava/util/List;Z)V

    invoke-interface {v0, v1}, Lcom/squareup/persistent/AtomicSyncedValue;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 178
    invoke-direct {p0}, Lcom/squareup/payment/RealDanglingAuth;->readLastAuth()Lcom/squareup/payment/RealDanglingAuth$Voidable;

    move-result-object v0

    .line 179
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v2, "{"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/payment/RealDanglingAuth$Voidable;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public voidLastAuth(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/String;)Lcom/squareup/protos/common/Money;
    .locals 10

    .line 151
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 153
    invoke-direct {p0}, Lcom/squareup/payment/RealDanglingAuth;->readLastAuth()Lcom/squareup/payment/RealDanglingAuth$Voidable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 155
    iget-object p1, p0, Lcom/squareup/payment/RealDanglingAuth;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object p2, Lcom/squareup/log/OhSnapLogger$EventType;->AUTO_VOID:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v0, "nothing to do"

    invoke-interface {p1, p2, v0}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const-string/jumbo v2, "voiding %s"

    .line 159
    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 160
    iget-object v2, p0, Lcom/squareup/payment/RealDanglingAuth;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v3, Lcom/squareup/log/OhSnapLogger$EventType;->AUTO_VOID:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-interface {v2, v3, v1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 161
    iget-object v4, p0, Lcom/squareup/payment/RealDanglingAuth;->paymentAccuracyLogger:Lcom/squareup/payment/PaymentAccuracyLogger;

    iget-object v7, v0, Lcom/squareup/payment/RealDanglingAuth$Voidable;->billId:Lcom/squareup/protos/client/IdPair;

    iget-object v8, v0, Lcom/squareup/payment/RealDanglingAuth$Voidable;->amountToReport:Lcom/squareup/protos/common/Money;

    .line 162
    invoke-static {v0}, Lcom/squareup/util/Objects;->getHumanClassName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    move-object v5, p1

    move-object v6, p2

    .line 161
    invoke-virtual/range {v4 .. v9}, Lcom/squareup/payment/PaymentAccuracyLogger;->logVoidLastAuth(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Ljava/lang/String;)V

    .line 163
    iget-object v2, p0, Lcom/squareup/payment/RealDanglingAuth;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    iget-object v3, v0, Lcom/squareup/payment/RealDanglingAuth$Voidable;->billId:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v2, v3, p2}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logVoidDanglingAuthorization(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    sget-object p2, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_OTHER:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    if-ne p1, p2, :cond_1

    .line 167
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 170
    :cond_1
    invoke-virtual {v0, p1}, Lcom/squareup/payment/RealDanglingAuth$Voidable;->voidAuth(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 172
    invoke-virtual {p0}, Lcom/squareup/payment/RealDanglingAuth;->clearLastAuth()V

    .line 174
    invoke-virtual {v0}, Lcom/squareup/payment/RealDanglingAuth$Voidable;->getAmountToReport()Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public writeLastAuth(JLcom/squareup/protos/client/IdPair;Lcom/squareup/PaymentType;Ljava/util/List;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/PaymentType;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;Z)V"
        }
    .end annotation

    .line 120
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 121
    invoke-direct {p0}, Lcom/squareup/payment/RealDanglingAuth;->readLastAuth()Lcom/squareup/payment/RealDanglingAuth$Voidable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v1, p3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/payment/RealDanglingAuth$Voidable;->billId:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    sget-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_OTHER:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    const-string v1, "Overwriting last auth"

    invoke-virtual {p0, v0, v1}, Lcom/squareup/payment/RealDanglingAuth;->voidLastAuth(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/String;)Lcom/squareup/protos/common/Money;

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/RealDanglingAuth;->lastAuthKey:Lcom/squareup/persistent/AtomicSyncedValue;

    new-instance v8, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;

    move-object v1, v8

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/payment/RealDanglingAuth$AuthorizationInfo;-><init>(JLcom/squareup/protos/client/IdPair;Lcom/squareup/PaymentType;Ljava/util/List;Z)V

    invoke-interface {v0, v8}, Lcom/squareup/persistent/AtomicSyncedValue;->set(Ljava/lang/Object;)V

    return-void
.end method
