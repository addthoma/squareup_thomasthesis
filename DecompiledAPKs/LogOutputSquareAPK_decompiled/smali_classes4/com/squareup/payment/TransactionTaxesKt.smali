.class public final Lcom/squareup/payment/TransactionTaxesKt;
.super Ljava/lang/Object;
.source "TransactionTaxes.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransactionTaxes.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransactionTaxes.kt\ncom/squareup/payment/TransactionTaxesKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,61:1\n704#2:62\n777#2,2:63\n1642#2,2:65\n*E\n*S KotlinDebug\n*F\n+ 1 TransactionTaxes.kt\ncom/squareup/payment/TransactionTaxesKt\n*L\n50#1:62\n50#1,2:63\n51#1,2:65\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u001a\"\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u001a6\u0010\u0008\u001a\u00020\u0001*\u00020\u00022\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0008\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\u00012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0002\u001a*\u0010\u000c\u001a\u00020\u0001*\u00020\u00022\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\r\u001a\u00020\n2\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a8\u0006\u000e"
    }
    d2 = {
        "isDisabledForCustomItem",
        "",
        "Lcom/squareup/checkout/Tax;",
        "availableTaxRules",
        "",
        "Lcom/squareup/payment/OrderTaxRule;",
        "diningOption",
        "Lcom/squareup/checkout/DiningOption;",
        "isDisabledForItem",
        "itemIdOrNull",
        "",
        "isCustomItem",
        "isDisabledForNonCustomItem",
        "itemId",
        "transaction_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final isDisabledForCustomItem(Lcom/squareup/checkout/Tax;Ljava/util/List;Lcom/squareup/checkout/DiningOption;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/Tax;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;",
            "Lcom/squareup/checkout/DiningOption;",
            ")Z"
        }
    .end annotation

    const-string v0, "$this$isDisabledForCustomItem"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "availableTaxRules"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 14
    invoke-static {p0, p1, v0, v1, p2}, Lcom/squareup/payment/TransactionTaxesKt;->isDisabledForItem(Lcom/squareup/checkout/Tax;Ljava/util/List;Ljava/lang/String;ZLcom/squareup/checkout/DiningOption;)Z

    move-result p0

    return p0
.end method

.method private static final isDisabledForItem(Lcom/squareup/checkout/Tax;Ljava/util/List;Ljava/lang/String;ZLcom/squareup/checkout/DiningOption;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/Tax;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/squareup/checkout/DiningOption;",
            ")Z"
        }
    .end annotation

    const/4 v0, 0x1

    if-eqz p3, :cond_0

    .line 45
    iget-boolean v1, p0, Lcom/squareup/checkout/Tax;->appliesToCustomAmounts:Z

    if-nez v1, :cond_0

    return v0

    :cond_0
    if-eqz p4, :cond_4

    .line 49
    check-cast p1, Ljava/lang/Iterable;

    .line 62
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 63
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/payment/OrderTaxRule;

    .line 50
    invoke-virtual {v3, p2, p3, p0, p4}, Lcom/squareup/payment/OrderTaxRule;->doesTaxRuleApply(Ljava/lang/String;ZLcom/squareup/checkout/Tax;Lcom/squareup/checkout/DiningOption;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 64
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 65
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/payment/OrderTaxRule;

    .line 52
    invoke-virtual {p0}, Lcom/squareup/payment/OrderTaxRule;->getEnablingActionType()Lcom/squareup/api/items/EnablingActionType;

    move-result-object p0

    sget-object p1, Lcom/squareup/api/items/EnablingActionType;->DISABLE:Lcom/squareup/api/items/EnablingActionType;

    if-ne p0, p1, :cond_3

    return v0

    .line 55
    :cond_3
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Point of Sale only supports DISABLE tax rules."

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    :cond_4
    const/4 p0, 0x0

    return p0
.end method

.method public static final isDisabledForNonCustomItem(Lcom/squareup/checkout/Tax;Ljava/util/List;Ljava/lang/String;Lcom/squareup/checkout/DiningOption;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/Tax;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/DiningOption;",
            ")Z"
        }
    .end annotation

    const-string v0, "$this$isDisabledForNonCustomItem"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "availableTaxRules"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 29
    invoke-static {p0, p1, p2, v0, p3}, Lcom/squareup/payment/TransactionTaxesKt;->isDisabledForItem(Lcom/squareup/checkout/Tax;Ljava/util/List;Ljava/lang/String;ZLcom/squareup/checkout/DiningOption;)Z

    move-result p0

    return p0
.end method
