.class public final synthetic Lcom/squareup/payment/-$$Lambda$Order$F1UYrsEg6Se40VXrta5nYGMEpso;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/checkout/CartItem$Transform;


# instance fields
.field private final synthetic f$0:Ljava/util/Set;

.field private final synthetic f$1:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public synthetic constructor <init>(Ljava/util/Set;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/-$$Lambda$Order$F1UYrsEg6Se40VXrta5nYGMEpso;->f$0:Ljava/util/Set;

    iput-object p2, p0, Lcom/squareup/payment/-$$Lambda$Order$F1UYrsEg6Se40VXrta5nYGMEpso;->f$1:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/CartItem;
    .locals 2

    iget-object v0, p0, Lcom/squareup/payment/-$$Lambda$Order$F1UYrsEg6Se40VXrta5nYGMEpso;->f$0:Ljava/util/Set;

    iget-object v1, p0, Lcom/squareup/payment/-$$Lambda$Order$F1UYrsEg6Se40VXrta5nYGMEpso;->f$1:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-static {v0, v1, p1}, Lcom/squareup/payment/Order;->lambda$removeNonRecalledFromTicketDiscounts$1(Ljava/util/Set;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    return-object p1
.end method
