.class public Lcom/squareup/payment/ledger/TransactionLedgerModule$LoggedInUi;
.super Ljava/lang/Object;
.source "TransactionLedgerModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/ledger/TransactionLedgerModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LoggedInUi"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideMaybeTransactionLedgerManager(Lcom/squareup/payment/ledger/TransactionLedgerManager;)Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 57
    check-cast p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;

    return-object p0
.end method

.method static provideScreenChangeLedgerManager(Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;)Lcom/squareup/flowlegacy/ScreenChangeLedgerManager;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    return-object p0
.end method
