.class public final Lcom/squareup/payment/ledger/LedgerUploaderRunner_Factory;
.super Ljava/lang/Object;
.source "LedgerUploaderRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/ledger/LedgerUploaderRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final pushMessageDelegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerUploaderRunner_Factory;->pushMessageDelegateProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/payment/ledger/LedgerUploaderRunner_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/ledger/LedgerUploaderRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;)",
            "Lcom/squareup/payment/ledger/LedgerUploaderRunner_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/payment/ledger/LedgerUploaderRunner_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/payment/ledger/LedgerUploaderRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/pushmessages/PushMessageDelegate;Lcom/squareup/payment/ledger/TransactionLedgerManager;)Lcom/squareup/payment/ledger/LedgerUploaderRunner;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/payment/ledger/LedgerUploaderRunner;

    invoke-direct {v0, p0, p1}, Lcom/squareup/payment/ledger/LedgerUploaderRunner;-><init>(Lcom/squareup/pushmessages/PushMessageDelegate;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/ledger/LedgerUploaderRunner;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/payment/ledger/LedgerUploaderRunner_Factory;->pushMessageDelegateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/pushmessages/PushMessageDelegate;

    iget-object v1, p0, Lcom/squareup/payment/ledger/LedgerUploaderRunner_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-static {v0, v1}, Lcom/squareup/payment/ledger/LedgerUploaderRunner_Factory;->newInstance(Lcom/squareup/pushmessages/PushMessageDelegate;Lcom/squareup/payment/ledger/TransactionLedgerManager;)Lcom/squareup/payment/ledger/LedgerUploaderRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/payment/ledger/LedgerUploaderRunner_Factory;->get()Lcom/squareup/payment/ledger/LedgerUploaderRunner;

    move-result-object v0

    return-object v0
.end method
