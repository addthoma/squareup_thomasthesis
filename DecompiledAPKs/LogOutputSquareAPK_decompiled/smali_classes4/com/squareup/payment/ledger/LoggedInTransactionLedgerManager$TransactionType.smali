.class final enum Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;
.super Ljava/lang/Enum;
.source "LoggedInTransactionLedgerManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "TransactionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum ADD_TENDER:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum ADD_TENDERS_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum ADD_TENDERS_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum ADD_TENDER_BEFORE_AUTH:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum AUTO_VOID:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum CANCEL_BILL_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum CANCEL_BILL_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum CANCEL_ENQUEUED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum CAPTURE_ENQUEUED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum CAPTURE_FAILED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum CAPTURE_PROCESSED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum CAPTURE_TENDER_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum CAPTURE_TENDER_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum COMPLETE_BILL_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum COMPLETE_BILL_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum ISSUE_REFUNDS_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum ISSUE_REFUNDS_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum REMOVE_TENDERS_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum REMOVE_TENDERS_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum SCREEN_TRANSITION:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum STORE_AND_FORWARD_ENQUEUED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum STORE_AND_FORWARD_FAILED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum STORE_AND_FORWARD_PROCESSED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum STORE_AND_FORWARD_READY:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

.field public static final enum STORE_AND_FORWARD_TASK_STATUS:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 93
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/4 v1, 0x0

    const-string v2, "AUTO_VOID"

    invoke-direct {v0, v2, v1}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->AUTO_VOID:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 94
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/4 v2, 0x1

    const-string v3, "CAPTURE_ENQUEUED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CAPTURE_ENQUEUED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 95
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/4 v3, 0x2

    const-string v4, "CAPTURE_PROCESSED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CAPTURE_PROCESSED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 96
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/4 v4, 0x3

    const-string v5, "CAPTURE_FAILED"

    invoke-direct {v0, v5, v4}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CAPTURE_FAILED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 97
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/4 v5, 0x4

    const-string v6, "CANCEL_ENQUEUED"

    invoke-direct {v0, v6, v5}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CANCEL_ENQUEUED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 98
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/4 v6, 0x5

    const-string v7, "STORE_AND_FORWARD_READY"

    invoke-direct {v0, v7, v6}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->STORE_AND_FORWARD_READY:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 99
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/4 v7, 0x6

    const-string v8, "STORE_AND_FORWARD_ENQUEUED"

    invoke-direct {v0, v8, v7}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->STORE_AND_FORWARD_ENQUEUED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 100
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/4 v8, 0x7

    const-string v9, "STORE_AND_FORWARD_PROCESSED"

    invoke-direct {v0, v9, v8}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->STORE_AND_FORWARD_PROCESSED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 101
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v9, 0x8

    const-string v10, "STORE_AND_FORWARD_FAILED"

    invoke-direct {v0, v10, v9}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->STORE_AND_FORWARD_FAILED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 102
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v10, 0x9

    const-string v11, "STORE_AND_FORWARD_TASK_STATUS"

    invoke-direct {v0, v11, v10}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->STORE_AND_FORWARD_TASK_STATUS:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 103
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v11, 0xa

    const-string v12, "ADD_TENDER_BEFORE_AUTH"

    invoke-direct {v0, v12, v11}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->ADD_TENDER_BEFORE_AUTH:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 104
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v12, 0xb

    const-string v13, "ADD_TENDERS_REQUEST"

    invoke-direct {v0, v13, v12}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->ADD_TENDERS_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 105
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v13, 0xc

    const-string v14, "ADD_TENDER"

    invoke-direct {v0, v14, v13}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->ADD_TENDER:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 106
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v14, 0xd

    const-string v15, "ADD_TENDERS_RESPONSE"

    invoke-direct {v0, v15, v14}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->ADD_TENDERS_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 107
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v15, 0xe

    const-string v14, "COMPLETE_BILL_REQUEST"

    invoke-direct {v0, v14, v15}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->COMPLETE_BILL_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 108
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const-string v14, "COMPLETE_BILL_RESPONSE"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v15}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->COMPLETE_BILL_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 109
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const-string v14, "CANCEL_BILL_REQUEST"

    const/16 v15, 0x10

    invoke-direct {v0, v14, v15}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CANCEL_BILL_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 110
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const-string v14, "CANCEL_BILL_RESPONSE"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v15}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CANCEL_BILL_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 111
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const-string v14, "ISSUE_REFUNDS_REQUEST"

    const/16 v15, 0x12

    invoke-direct {v0, v14, v15}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->ISSUE_REFUNDS_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 112
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const-string v14, "ISSUE_REFUNDS_RESPONSE"

    const/16 v15, 0x13

    invoke-direct {v0, v14, v15}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->ISSUE_REFUNDS_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 113
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const-string v14, "SCREEN_TRANSITION"

    const/16 v15, 0x14

    invoke-direct {v0, v14, v15}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->SCREEN_TRANSITION:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 114
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const-string v14, "CAPTURE_TENDER_REQUEST"

    const/16 v15, 0x15

    invoke-direct {v0, v14, v15}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CAPTURE_TENDER_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 115
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const-string v14, "CAPTURE_TENDER_RESPONSE"

    const/16 v15, 0x16

    invoke-direct {v0, v14, v15}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CAPTURE_TENDER_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 116
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const-string v14, "REMOVE_TENDERS_REQUEST"

    const/16 v15, 0x17

    invoke-direct {v0, v14, v15}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->REMOVE_TENDERS_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 117
    new-instance v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const-string v14, "REMOVE_TENDERS_RESPONSE"

    const/16 v15, 0x18

    invoke-direct {v0, v14, v15}, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->REMOVE_TENDERS_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v0, 0x19

    new-array v0, v0, [Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 92
    sget-object v14, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->AUTO_VOID:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    aput-object v14, v0, v1

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CAPTURE_ENQUEUED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CAPTURE_PROCESSED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CAPTURE_FAILED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CANCEL_ENQUEUED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->STORE_AND_FORWARD_READY:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->STORE_AND_FORWARD_ENQUEUED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->STORE_AND_FORWARD_PROCESSED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->STORE_AND_FORWARD_FAILED:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->STORE_AND_FORWARD_TASK_STATUS:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->ADD_TENDER_BEFORE_AUTH:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->ADD_TENDERS_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->ADD_TENDER:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->ADD_TENDERS_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->COMPLETE_BILL_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->COMPLETE_BILL_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CANCEL_BILL_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CANCEL_BILL_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->ISSUE_REFUNDS_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->ISSUE_REFUNDS_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->SCREEN_TRANSITION:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CAPTURE_TENDER_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->CAPTURE_TENDER_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->REMOVE_TENDERS_REQUEST:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->REMOVE_TENDERS_RESPONSE:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->$VALUES:[Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;
    .locals 1

    .line 92
    const-class v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;
    .locals 1

    .line 92
    sget-object v0, Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->$VALUES:[Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    invoke-virtual {v0}, [Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    return-object v0
.end method
