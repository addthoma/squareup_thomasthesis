.class public Lcom/squareup/payment/tender/NoSaleTender;
.super Lcom/squareup/payment/tender/BaseLocalTender;
.source "NoSaleTender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/tender/NoSaleTender$Builder;
    }
.end annotation


# direct methods
.method private constructor <init>(Lcom/squareup/payment/tender/NoSaleTender$Builder;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/payment/tender/BaseLocalTender;-><init>(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/tender/NoSaleTender$Builder;Lcom/squareup/payment/tender/NoSaleTender$1;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1}, Lcom/squareup/payment/tender/NoSaleTender;-><init>(Lcom/squareup/payment/tender/NoSaleTender$Builder;)V

    return-void
.end method


# virtual methods
.method public getDeclinedMessage()Ljava/lang/String;
    .locals 2

    .line 44
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "NoSaleTender cannot be declined."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method getMethod()Lcom/squareup/protos/client/bills/Tender$Method;
    .locals 2

    .line 21
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/bills/NoSaleTender$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/NoSaleTender$Builder;-><init>()V

    .line 23
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/NoSaleTender$Builder;->build()Lcom/squareup/protos/client/bills/NoSaleTender;

    move-result-object v1

    .line 22
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->no_sale_tender(Lcom/squareup/protos/client/bills/NoSaleTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v0

    return-object v0
.end method

.method public getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 28
    sget v0, Lcom/squareup/transaction/R$string;->buyer_printed_receipt_tender_no_sale:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getShortTenderMessage()Ljava/lang/String;
    .locals 2

    .line 36
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "NoSaleTender is never part of a split tender payment"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;
    .locals 1

    .line 32
    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->NO_SALE:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    return-object v0
.end method

.method public getTenderTypeGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 40
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->OTHER_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public supportsPaperSigAndTip()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
