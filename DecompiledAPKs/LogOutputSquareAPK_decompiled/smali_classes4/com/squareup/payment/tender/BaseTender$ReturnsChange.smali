.class public interface abstract Lcom/squareup/payment/tender/BaseTender$ReturnsChange;
.super Ljava/lang/Object;
.source "BaseTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/tender/BaseTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ReturnsChange"
.end annotation


# virtual methods
.method public abstract getChange()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getTendered()Lcom/squareup/protos/common/Money;
.end method
