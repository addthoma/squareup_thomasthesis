.class public Lcom/squareup/payment/tender/DefaultTenderProtoFactory;
.super Ljava/lang/Object;
.source "DefaultTenderProtoFactory.java"

# interfaces
.implements Lcom/squareup/payment/tender/TenderProtoFactory;


# instance fields
.field private connectedPeripheralsLogger:Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;


# direct methods
.method public constructor <init>(Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/payment/tender/DefaultTenderProtoFactory;->connectedPeripheralsLogger:Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;

    return-void
.end method


# virtual methods
.method public startTender()Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 2

    .line 20
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/payment/tender/DefaultTenderProtoFactory;->connectedPeripheralsLogger:Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;

    .line 21
    invoke-virtual {v1}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->getPeripheralMetaData()Lcom/squareup/protos/common/payment/PeripheralMetadata;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Builder;->peripheral_metadata(Lcom/squareup/protos/common/payment/PeripheralMetadata;)Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v0

    return-object v0
.end method
