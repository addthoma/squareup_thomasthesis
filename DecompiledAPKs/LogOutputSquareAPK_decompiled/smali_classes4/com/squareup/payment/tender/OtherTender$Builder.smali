.class public Lcom/squareup/payment/tender/OtherTender$Builder;
.super Lcom/squareup/payment/tender/BaseLocalTender$Builder;
.source "OtherTender.java"

# interfaces
.implements Lcom/squareup/payment/AcceptsTips;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/tender/OtherTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private note:Ljava/lang/String;

.field private tip:Lcom/squareup/protos/common/Money;

.field private tipPercentage:Lcom/squareup/util/Percentage;

.field private type:Lcom/squareup/server/account/protos/OtherTenderType;


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/TenderFactory;)V
    .locals 2

    .line 130
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->OTHER_TENDER_PAYMENT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->OTHER:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/payment/tender/BaseLocalTender$Builder;-><init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Lcom/squareup/protos/client/bills/Tender$Type;)V

    .line 132
    iget-object p1, p1, Lcom/squareup/payment/tender/TenderFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p1, p0, Lcom/squareup/payment/tender/OtherTender$Builder;->moneyFormatter:Lcom/squareup/text/Formatter;

    const/4 p1, 0x0

    .line 134
    iput-object p1, p0, Lcom/squareup/payment/tender/OtherTender$Builder;->tip:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/payment/tender/OtherTender$Builder;)Lcom/squareup/util/Percentage;
    .locals 0

    .line 122
    iget-object p0, p0, Lcom/squareup/payment/tender/OtherTender$Builder;->tipPercentage:Lcom/squareup/util/Percentage;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/payment/tender/OtherTender$Builder;)Lcom/squareup/server/account/protos/OtherTenderType;
    .locals 0

    .line 122
    iget-object p0, p0, Lcom/squareup/payment/tender/OtherTender$Builder;->type:Lcom/squareup/server/account/protos/OtherTenderType;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/payment/tender/OtherTender$Builder;)Ljava/lang/String;
    .locals 0

    .line 122
    iget-object p0, p0, Lcom/squareup/payment/tender/OtherTender$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/payment/tender/OtherTender$Builder;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 122
    iget-object p0, p0, Lcom/squareup/payment/tender/OtherTender$Builder;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/payment/tender/OtherTender$Builder;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 122
    invoke-direct {p0}, Lcom/squareup/payment/tender/OtherTender$Builder;->getTipForBuild()Lcom/squareup/protos/common/Money;

    move-result-object p0

    return-object p0
.end method

.method private getTipForBuild()Lcom/squareup/protos/common/Money;
    .locals 5

    .line 182
    invoke-virtual {p0}, Lcom/squareup/payment/tender/OtherTender$Builder;->isThirdPartyCardTender()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/payment/tender/OtherTender$Builder;->type:Lcom/squareup/server/account/protos/OtherTenderType;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/OtherTenderType;->accepts_tips:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/tender/OtherTender$Builder;->tip:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_1

    goto :goto_0

    .line 186
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This OtherTenderType doesn\'t accept tips."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 183
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/squareup/payment/tender/OtherTender$Builder;->tip:Lcom/squareup/protos/common/Money;

    return-object v0
.end method


# virtual methods
.method public askForTip()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic build()Lcom/squareup/payment/tender/BaseTender;
    .locals 1

    .line 122
    invoke-virtual {p0}, Lcom/squareup/payment/tender/OtherTender$Builder;->build()Lcom/squareup/payment/tender/OtherTender;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/squareup/payment/tender/OtherTender;
    .locals 2

    .line 169
    new-instance v0, Lcom/squareup/payment/tender/OtherTender;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/payment/tender/OtherTender;-><init>(Lcom/squareup/payment/tender/OtherTender$Builder;Lcom/squareup/payment/tender/OtherTender$1;)V

    return-object v0
.end method

.method public canBeZeroAmount()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getAmountForTipping()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 210
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getCustomTipMaxMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 198
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getNote()Ljava/lang/String;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/payment/tender/OtherTender$Builder;->note:Ljava/lang/String;

    return-object v0
.end method

.method public getTip()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/squareup/payment/tender/OtherTender$Builder;->tip:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getTipOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation

    .line 194
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType()Lcom/squareup/server/account/protos/OtherTenderType;
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/squareup/payment/tender/OtherTender$Builder;->type:Lcom/squareup/server/account/protos/OtherTenderType;

    return-object v0
.end method

.method public isThirdPartyCardTender()Z
    .locals 2

    .line 178
    iget-object v0, p0, Lcom/squareup/payment/tender/OtherTender$Builder;->type:Lcom/squareup/server/account/protos/OtherTenderType;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->THIRD_PARTY_CARD:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->getValue()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setNote(Ljava/lang/String;)Lcom/squareup/payment/tender/OtherTender$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/payment/tender/OtherTender$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method public setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/payment/tender/OtherTender$Builder;->tip:Lcom/squareup/protos/common/Money;

    .line 161
    iput-object p2, p0, Lcom/squareup/payment/tender/OtherTender$Builder;->tipPercentage:Lcom/squareup/util/Percentage;

    return-void
.end method

.method public setType(Lcom/squareup/server/account/protos/OtherTenderType;)Lcom/squareup/payment/tender/OtherTender$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/payment/tender/OtherTender$Builder;->type:Lcom/squareup/server/account/protos/OtherTenderType;

    return-object p0
.end method

.method public tipOnPrintedReceipt()Z
    .locals 1

    .line 206
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public useSeparateTippingScreen()Z
    .locals 1

    .line 202
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
