.class Lcom/squareup/payment/tender/MagStripeTender$1;
.super Ljava/lang/Object;
.source "MagStripeTender.java"

# interfaces
.implements Lcom/squareup/Card$InputType$InputTypeHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/tender/MagStripeTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/Card$InputType$InputTypeHandler<",
        "Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/tender/MagStripeTender;


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/MagStripeTender;)V
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/payment/tender/MagStripeTender$1;->this$0:Lcom/squareup/payment/tender/MagStripeTender;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleA10(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 0

    .line 114
    sget-object p1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p1
.end method

.method public bridge synthetic handleA10(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 92
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/MagStripeTender$1;->handleA10(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object p1

    return-object p1
.end method

.method public handleGen2(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 0

    .line 98
    sget-object p1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p1
.end method

.method public bridge synthetic handleGen2(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 92
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/MagStripeTender$1;->handleGen2(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object p1

    return-object p1
.end method

.method public handleManual(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 0

    .line 94
    sget-object p1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->KEYED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p1
.end method

.method public bridge synthetic handleManual(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 92
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/MagStripeTender$1;->handleManual(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object p1

    return-object p1
.end method

.method public handleO1(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 0

    .line 102
    sget-object p1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p1
.end method

.method public bridge synthetic handleO1(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 92
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/MagStripeTender$1;->handleO1(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object p1

    return-object p1
.end method

.method public handleR4(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 0

    .line 106
    sget-object p1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p1
.end method

.method public bridge synthetic handleR4(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 92
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/MagStripeTender$1;->handleR4(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object p1

    return-object p1
.end method

.method public handleR6(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 0

    .line 110
    sget-object p1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p1
.end method

.method public bridge synthetic handleR6(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 92
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/MagStripeTender$1;->handleR6(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object p1

    return-object p1
.end method

.method public handleT2(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 0

    .line 122
    sget-object p1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p1
.end method

.method public bridge synthetic handleT2(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 92
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/MagStripeTender$1;->handleT2(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object p1

    return-object p1
.end method

.method public handleX2(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .locals 0

    .line 118
    sget-object p1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p1
.end method

.method public bridge synthetic handleX2(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 92
    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/MagStripeTender$1;->handleX2(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object p1

    return-object p1
.end method
