.class public final enum Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;
.super Ljava/lang/Enum;
.source "BaseTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/tender/BaseTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReceiptDestinationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

.field public static final enum DECLINE:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

.field public static final enum EMAIL:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

.field public static final enum EMAIL_ID:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

.field public static final enum SMS:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

.field public static final enum SMS_ID:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 90
    new-instance v0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    const/4 v1, 0x0

    const-string v2, "DECLINE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->DECLINE:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    new-instance v0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    const/4 v2, 0x1

    const-string v3, "EMAIL"

    invoke-direct {v0, v3, v2}, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->EMAIL:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    new-instance v0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    const/4 v3, 0x2

    const-string v4, "EMAIL_ID"

    invoke-direct {v0, v4, v3}, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->EMAIL_ID:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    new-instance v0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    const/4 v4, 0x3

    const-string v5, "SMS"

    invoke-direct {v0, v5, v4}, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->SMS:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    new-instance v0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    const/4 v5, 0x4

    const-string v6, "SMS_ID"

    invoke-direct {v0, v6, v5}, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->SMS_ID:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    .line 89
    sget-object v6, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->DECLINE:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->EMAIL:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->EMAIL_ID:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->SMS:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->SMS_ID:Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->$VALUES:[Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 89
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;
    .locals 1

    .line 89
    const-class v0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;
    .locals 1

    .line 89
    sget-object v0, Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->$VALUES:[Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    invoke-virtual {v0}, [Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/payment/tender/BaseTender$ReceiptDestinationType;

    return-object v0
.end method
