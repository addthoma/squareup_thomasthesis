.class public Lcom/squareup/payment/tender/InstrumentTender$Builder;
.super Lcom/squareup/payment/tender/BaseCardTender$Builder;
.source "InstrumentTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/tender/InstrumentTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private card:Lcom/squareup/Card;

.field private contactToken:Ljava/lang/String;

.field private instrument:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/tender/TenderFactory;)V
    .locals 1

    const/4 v0, 0x0

    .line 28
    invoke-direct {p0, p1, v0}, Lcom/squareup/payment/tender/BaseCardTender$Builder;-><init>(Lcom/squareup/payment/tender/TenderFactory;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/payment/tender/InstrumentTender$Builder;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/payment/tender/InstrumentTender$Builder;->instrument:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/payment/tender/InstrumentTender$Builder;)Ljava/lang/String;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/payment/tender/InstrumentTender$Builder;->contactToken:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/payment/tender/InstrumentTender$Builder;)Lcom/squareup/Card;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/payment/tender/InstrumentTender$Builder;->card:Lcom/squareup/Card;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/payment/tender/BaseTender;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/payment/tender/InstrumentTender$Builder;->build()Lcom/squareup/payment/tender/InstrumentTender;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/squareup/payment/tender/InstrumentTender;
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/payment/tender/InstrumentTender;

    invoke-direct {v0, p0}, Lcom/squareup/payment/tender/InstrumentTender;-><init>(Lcom/squareup/payment/tender/InstrumentTender$Builder;)V

    return-object v0
.end method

.method public getCard()Lcom/squareup/Card;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/payment/tender/InstrumentTender$Builder;->card:Lcom/squareup/Card;

    return-object v0
.end method

.method public getInstrument()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/payment/tender/InstrumentTender$Builder;->instrument:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    return-object v0
.end method

.method public setContactToken(Ljava/lang/String;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/payment/tender/InstrumentTender$Builder;->contactToken:Ljava/lang/String;

    return-void
.end method

.method public setInstrument(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;)V
    .locals 2

    .line 40
    iput-object p1, p0, Lcom/squareup/payment/tender/InstrumentTender$Builder;->instrument:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    .line 41
    new-instance v0, Lcom/squareup/Card$Builder;

    invoke-direct {v0}, Lcom/squareup/Card$Builder;-><init>()V

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->cardholder_name:Ljava/lang/String;

    .line 42
    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->name(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 43
    invoke-static {v1}, Lcom/squareup/card/CardBrands;->toCardBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/Card$Brand;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->last_four:Ljava/lang/String;

    .line 44
    invoke-virtual {v0, p1}, Lcom/squareup/Card$Builder;->pan(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/Card$InputType;->MANUAL:Lcom/squareup/Card$InputType;

    .line 45
    invoke-virtual {p1, v0}, Lcom/squareup/Card$Builder;->inputType(Lcom/squareup/Card$InputType;)Lcom/squareup/Card$Builder;

    move-result-object p1

    .line 46
    invoke-virtual {p1}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/tender/InstrumentTender$Builder;->card:Lcom/squareup/Card;

    return-void
.end method
