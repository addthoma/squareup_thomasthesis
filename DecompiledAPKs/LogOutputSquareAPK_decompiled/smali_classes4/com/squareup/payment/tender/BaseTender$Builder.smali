.class public abstract Lcom/squareup/payment/tender/BaseTender$Builder;
.super Ljava/lang/Object;
.source "BaseTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/tender/BaseTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# instance fields
.field private amount:Lcom/squareup/protos/common/Money;

.field private amountWithoutTax:Lcom/squareup/protos/common/Money;

.field private autoGratuityAmount:Lcom/squareup/protos/common/Money;

.field protected buyerSelectedLocale:Ljava/util/Locale;

.field protected final clientId:Ljava/lang/String;

.field private customerContact:Lcom/squareup/protos/client/rolodex/Contact;

.field private customerInstruments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;"
        }
    .end annotation
.end field

.field private customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

.field private deviceCredential:Lcom/squareup/server/account/protos/DeviceCredential;

.field protected employeeToken:Ljava/lang/String;

.field protected final locationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private merchantToken:Ljava/lang/String;

.field protected final res:Lcom/squareup/util/Res;

.field private surchargeAmount:Lcom/squareup/protos/common/Money;

.field protected final taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field protected final tenderProtoFactory:Lcom/squareup/payment/tender/TenderProtoFactory;

.field protected final tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

.field protected final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method protected constructor <init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/protos/client/bills/Tender$Type;Ljava/lang/String;)V
    .locals 1

    .line 617
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 618
    iget-object v0, p1, Lcom/squareup/payment/tender/TenderFactory;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 619
    iget-object v0, p1, Lcom/squareup/payment/tender/TenderFactory;->locationProvider:Ljavax/inject/Provider;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->locationProvider:Ljavax/inject/Provider;

    .line 620
    iget-object v0, p1, Lcom/squareup/payment/tender/TenderFactory;->tenderProtoFactory:Lcom/squareup/payment/tender/TenderProtoFactory;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->tenderProtoFactory:Lcom/squareup/payment/tender/TenderProtoFactory;

    .line 621
    iget-object v0, p1, Lcom/squareup/payment/tender/TenderFactory;->res:Lcom/squareup/util/Res;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->res:Lcom/squareup/util/Res;

    .line 622
    iget-object v0, p1, Lcom/squareup/payment/tender/TenderFactory;->transaction:Lcom/squareup/payment/Transaction;

    iput-object v0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->transaction:Lcom/squareup/payment/Transaction;

    .line 623
    iput-object p2, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 624
    iput-object p3, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->clientId:Ljava/lang/String;

    .line 625
    iget-object p2, p1, Lcom/squareup/payment/tender/TenderFactory;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {p2}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->employeeToken:Ljava/lang/String;

    .line 626
    iget-object p2, p1, Lcom/squareup/payment/tender/TenderFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p2}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->merchantToken:Ljava/lang/String;

    .line 627
    iget-object p1, p1, Lcom/squareup/payment/tender/TenderFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getDeviceCredential()Lcom/squareup/server/account/protos/DeviceCredential;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->deviceCredential:Lcom/squareup/server/account/protos/DeviceCredential;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 594
    iget-object p0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 594
    iget-object p0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->amountWithoutTax:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 594
    iget-object p0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->autoGratuityAmount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 594
    iget-object p0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->surchargeAmount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/payment/tender/BaseTender$Builder;)Ljava/lang/String;
    .locals 0

    .line 594
    iget-object p0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->merchantToken:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/server/account/protos/DeviceCredential;
    .locals 0

    .line 594
    iget-object p0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->deviceCredential:Lcom/squareup/server/account/protos/DeviceCredential;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 0

    .line 594
    iget-object p0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;
    .locals 0

    .line 594
    iget-object p0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/payment/tender/BaseTender$Builder;)Ljava/util/List;
    .locals 0

    .line 594
    iget-object p0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->customerInstruments:Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public abstract build()Lcom/squareup/payment/tender/BaseTender;
.end method

.method public buyerSelectedLocale()Ljava/util/Locale;
    .locals 1

    .line 699
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->buyerSelectedLocale:Ljava/util/Locale;

    return-object v0
.end method

.method public canBeZeroAmount()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 631
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->amount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getAmountWithoutTax()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 635
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->amountWithoutTax:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getMaxAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 686
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getMinAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTenderType()Lcom/squareup/protos/client/bills/Tender$Type;
    .locals 1

    .line 639
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object v0
.end method

.method public isAmountWithinRange()Z
    .locals 3

    .line 703
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->amount:Lcom/squareup/protos/common/Money;

    if-nez v0, :cond_0

    .line 704
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender$Builder;->canBeZeroAmount()Z

    move-result v0

    return v0

    .line 707
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender$Builder;->getMinAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender$Builder;->getMinAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->amount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v2}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    .line 711
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender$Builder;->getMaxAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0}, Lcom/squareup/payment/tender/BaseTender$Builder;->getMaxAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_2

    return v1

    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method public setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;
    .locals 2

    .line 643
    iput-object p1, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->amount:Lcom/squareup/protos/common/Money;

    .line 650
    iget-object v0, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->transaction:Lcom/squareup/payment/Transaction;

    .line 651
    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getAllTaxes()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 650
    invoke-static {v0, p1, v1}, Lcom/squareup/money/MoneyMath;->prorateAmountNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 656
    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1, v0}, Lcom/squareup/money/MoneyMath;->greaterThanNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 658
    invoke-static {p1, v0}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 660
    :cond_0
    iput-object p1, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->amountWithoutTax:Lcom/squareup/protos/common/Money;

    .line 662
    iget-object p1, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->hasAutoGratuity()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 663
    iget-object p1, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getAutoGratuityAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->autoGratuityAmount:Lcom/squareup/protos/common/Money;

    .line 666
    :cond_1
    iget-object p1, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getAllSurchargeAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->surchargeAmount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public setBuyerSelectedLocale(Ljava/util/Locale;)V
    .locals 0

    .line 695
    iput-object p1, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->buyerSelectedLocale:Ljava/util/Locale;

    return-void
.end method

.method public setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)Lcom/squareup/payment/tender/BaseTender$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;)",
            "Lcom/squareup/payment/tender/BaseTender$Builder;"
        }
    .end annotation

    .line 673
    iput-object p1, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 674
    iput-object p2, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    .line 675
    iput-object p3, p0, Lcom/squareup/payment/tender/BaseTender$Builder;->customerInstruments:Ljava/util/List;

    return-object p0
.end method
