.class public Lcom/squareup/payment/PaymentAccuracyLogger;
.super Ljava/lang/Object;
.source "PaymentAccuracyLogger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/PaymentAccuracyLogger$VoidLastAuthEvent;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final giftCards:Lcom/squareup/giftcard/GiftCards;


# direct methods
.method public constructor <init>(Lcom/squareup/giftcard/GiftCards;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/payment/PaymentAccuracyLogger;->giftCards:Lcom/squareup/giftcard/GiftCards;

    .line 44
    iput-object p2, p0, Lcom/squareup/payment/PaymentAccuracyLogger;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private accuracyEventForBillPayment(Lcom/squareup/payment/BillPayment;Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;)Lcom/squareup/log/accuracy/PaymentAccuracyEvent;
    .locals 1

    .line 121
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    .line 120
    invoke-direct {p0, p2, p3, p1, v0}, Lcom/squareup/payment/PaymentAccuracyLogger;->accuracyEventForBillPaymentAndTender(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/log/accuracy/PaymentAccuracyEvent;

    move-result-object p1

    return-object p1
.end method

.method private accuracyEventForBillPaymentAndTender(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/log/accuracy/PaymentAccuracyEvent;
    .locals 1

    .line 126
    instance-of v0, p4, Lcom/squareup/payment/tender/EmoneyTender;

    if-eqz v0, :cond_0

    .line 127
    new-instance v0, Lcom/squareup/log/accuracy/EmoneyTenderEvent;

    invoke-direct {v0, p1, p2, p4, p3}, Lcom/squareup/log/accuracy/EmoneyTenderEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/payment/BillPayment;)V

    return-object v0

    .line 129
    :cond_0
    instance-of v0, p4, Lcom/squareup/payment/tender/BaseCardTender;

    if-eqz v0, :cond_1

    .line 130
    check-cast p4, Lcom/squareup/payment/tender/BaseCardTender;

    invoke-direct {p0, p3, p1, p2, p4}, Lcom/squareup/payment/PaymentAccuracyLogger;->accuracyForCardTender(Lcom/squareup/payment/BillPayment;Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/BaseCardTender;)Lcom/squareup/log/accuracy/PaymentAccuracyEvent;

    move-result-object p1

    return-object p1

    .line 132
    :cond_1
    instance-of v0, p4, Lcom/squareup/payment/tender/CashTender;

    if-eqz v0, :cond_2

    .line 133
    new-instance v0, Lcom/squareup/log/accuracy/CashTenderEvent;

    check-cast p4, Lcom/squareup/payment/tender/CashTender;

    invoke-direct {v0, p1, p2, p4, p3}, Lcom/squareup/log/accuracy/CashTenderEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/CashTender;Lcom/squareup/payment/BillPayment;)V

    return-object v0

    .line 135
    :cond_2
    instance-of v0, p4, Lcom/squareup/payment/tender/OtherTender;

    if-eqz v0, :cond_3

    .line 136
    new-instance v0, Lcom/squareup/log/accuracy/OtherTenderEvent;

    check-cast p4, Lcom/squareup/payment/tender/OtherTender;

    invoke-direct {v0, p1, p2, p4, p3}, Lcom/squareup/log/accuracy/OtherTenderEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/OtherTender;Lcom/squareup/payment/BillPayment;)V

    return-object v0

    .line 138
    :cond_3
    instance-of v0, p4, Lcom/squareup/payment/tender/ZeroTender;

    if-eqz v0, :cond_4

    .line 139
    new-instance v0, Lcom/squareup/log/accuracy/ZeroTenderEvent;

    check-cast p4, Lcom/squareup/payment/tender/ZeroTender;

    invoke-direct {v0, p1, p2, p4, p3}, Lcom/squareup/log/accuracy/ZeroTenderEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/ZeroTender;Lcom/squareup/payment/BillPayment;)V

    return-object v0

    .line 141
    :cond_4
    instance-of v0, p4, Lcom/squareup/payment/tender/InstrumentTender;

    if-eqz v0, :cond_5

    .line 142
    new-instance v0, Lcom/squareup/log/accuracy/InstrumentTenderEvent;

    invoke-direct {v0, p1, p2, p4, p3}, Lcom/squareup/log/accuracy/InstrumentTenderEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/payment/BillPayment;)V

    return-object v0

    .line 144
    :cond_5
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Unsupported new tender type: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private accuracyEventForPayment(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/Payment;)Lcom/squareup/log/accuracy/PaymentAccuracyEvent;
    .locals 1

    .line 109
    instance-of v0, p3, Lcom/squareup/payment/BillPayment;

    if-eqz v0, :cond_0

    .line 110
    check-cast p3, Lcom/squareup/payment/BillPayment;

    invoke-direct {p0, p3, p1, p2}, Lcom/squareup/payment/PaymentAccuracyLogger;->accuracyEventForBillPayment(Lcom/squareup/payment/BillPayment;Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;)Lcom/squareup/log/accuracy/PaymentAccuracyEvent;

    move-result-object p1

    return-object p1

    .line 111
    :cond_0
    instance-of p1, p3, Lcom/squareup/payment/InvoicePayment;

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    return-object p1

    .line 114
    :cond_1
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unsupported new payment type: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private accuracyForCardTender(Lcom/squareup/payment/BillPayment;Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/BaseCardTender;)Lcom/squareup/log/accuracy/PaymentAccuracyEvent;
    .locals 2

    .line 150
    instance-of v0, p4, Lcom/squareup/payment/tender/SmartCardTender;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/PaymentAccuracyLogger;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {p4}, Lcom/squareup/payment/tender/BaseCardTender;->getCard()Lcom/squareup/Card;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    new-instance v0, Lcom/squareup/log/accuracy/GiftCardTenderEvent;

    invoke-direct {v0, p2, p3, p4, p1}, Lcom/squareup/log/accuracy/GiftCardTenderEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/BaseCardTender;Lcom/squareup/payment/BillPayment;)V

    return-object v0

    .line 153
    :cond_0
    new-instance v0, Lcom/squareup/log/accuracy/CardTenderEvent;

    invoke-direct {v0, p2, p3, p4, p1}, Lcom/squareup/log/accuracy/CardTenderEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/BaseCardTender;Lcom/squareup/payment/BillPayment;)V

    return-object v0
.end method

.method private logAccuracyEvent(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/Payment;)V
    .locals 0

    .line 98
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/payment/PaymentAccuracyLogger;->accuracyEventForPayment(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/Payment;)Lcom/squareup/log/accuracy/PaymentAccuracyEvent;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/PaymentAccuracyLogger;->logAccuracyEvent(Lcom/squareup/log/accuracy/PaymentAccuracyEvent;)V

    return-void
.end method

.method private logAccuracyEvent(Lcom/squareup/log/accuracy/PaymentAccuracyEvent;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 103
    iget-object v0, p0, Lcom/squareup/payment/PaymentAccuracyLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public logCancelBillAccuracyEvent(Lcom/squareup/payment/Payment;)V
    .locals 3

    .line 71
    new-instance v0, Lcom/squareup/log/accuracy/PaymentAccuracyEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->CANCEL_BILL:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/log/accuracy/PaymentAccuracyEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/Payment;)V

    invoke-direct {p0, v0}, Lcom/squareup/payment/PaymentAccuracyLogger;->logAccuracyEvent(Lcom/squareup/log/accuracy/PaymentAccuracyEvent;)V

    return-void
.end method

.method public logCancelTenderAccuracyEvent(Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;)V
    .locals 2

    .line 67
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->CANCEL_TENDER:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/squareup/payment/PaymentAccuracyLogger;->accuracyEventForBillPaymentAndTender(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/log/accuracy/PaymentAccuracyEvent;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/PaymentAccuracyLogger;->logAccuracyEvent(Lcom/squareup/log/accuracy/PaymentAccuracyEvent;)V

    return-void
.end method

.method public logOfflineCheckmarkForAuthorization(Lcom/squareup/payment/Payment;)V
    .locals 2

    .line 55
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->OFFLINE_CHECKMARK_VIEW_FOR_AUTHORIZED:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/payment/PaymentAccuracyLogger;->logAccuracyEvent(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/Payment;)V

    return-void
.end method

.method public logOnlineCheckmarkForAuthorization(Lcom/squareup/payment/Payment;)V
    .locals 2

    .line 59
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->ONLINE_CHECKMARK_VIEW_FOR_AUTH:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/payment/PaymentAccuracyLogger;->logAccuracyEvent(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/Payment;)V

    return-void
.end method

.method public logReceiptAccuracyEvent(Lcom/squareup/payment/Payment;)V
    .locals 2

    .line 63
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->VIEW:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->RECEIPT_SCREEN:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/payment/PaymentAccuracyLogger;->logAccuracyEvent(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/Payment;)V

    return-void
.end method

.method public logStoredTransaction(Lcom/squareup/payment/BillPayment;)V
    .locals 4

    .line 48
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->getTendersForLogging()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/tender/BaseTender;

    .line 49
    sget-object v2, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v3, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->STORED_TRANSACTION:Lcom/squareup/analytics/RegisterPaymentAccuracyName;

    .line 50
    invoke-direct {p0, v2, v3, p1, v1}, Lcom/squareup/payment/PaymentAccuracyLogger;->accuracyEventForBillPaymentAndTender(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/BillPayment;Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/log/accuracy/PaymentAccuracyEvent;

    move-result-object v1

    .line 49
    invoke-direct {p0, v1}, Lcom/squareup/payment/PaymentAccuracyLogger;->logAccuracyEvent(Lcom/squareup/log/accuracy/PaymentAccuracyEvent;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public logVoidLastAuth(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Ljava/lang/String;)V
    .locals 8

    .line 76
    new-instance v7, Lcom/squareup/payment/PaymentAccuracyLogger$VoidLastAuthEvent;

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/PaymentAccuracyLogger$VoidLastAuthEvent;-><init>(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/payment/PaymentAccuracyLogger$1;)V

    invoke-direct {p0, v7}, Lcom/squareup/payment/PaymentAccuracyLogger;->logAccuracyEvent(Lcom/squareup/log/accuracy/PaymentAccuracyEvent;)V

    return-void
.end method
