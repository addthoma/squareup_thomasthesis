.class public Lcom/squareup/payment/CardConverterUtils;
.super Ljava/lang/Object;
.source "CardConverterUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static buildR12Card([B)Lcom/squareup/protos/client/bills/CardData$R12Card;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;-><init>()V

    .line 47
    invoke-static {p0}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;->encrypted_reader_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;

    .line 48
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$R12Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$R12Card;

    move-result-object p0

    return-object p0
.end method

.method private static buildR6CardForDip([B)Lcom/squareup/protos/client/bills/CardData$R6Card;
    .locals 1

    .line 52
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;-><init>()V

    .line 53
    invoke-static {p0}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;->encrypted_reader_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;

    .line 54
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$R6Card;

    move-result-object p0

    return-object p0
.end method

.method private static buildT2Card([B)Lcom/squareup/protos/client/bills/CardData$T2Card;
    .locals 1

    .line 64
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;-><init>()V

    .line 65
    invoke-static {p0}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;->encrypted_reader_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;

    .line 66
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$T2Card;

    move-result-object p0

    return-object p0
.end method

.method private static buildX2Card([B)Lcom/squareup/protos/client/bills/CardData$X2Card;
    .locals 1

    .line 58
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;-><init>()V

    .line 59
    invoke-static {p0}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;->encrypted_reader_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;

    .line 60
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$X2Card;

    move-result-object p0

    return-object p0
.end method

.method public static createCardDataFromBytes(Lcom/squareup/protos/client/bills/CardData$ReaderType;[B)Lcom/squareup/protos/client/bills/CardData;
    .locals 2

    .line 18
    sget-object v0, Lcom/squareup/payment/CardConverterUtils$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 35
    new-instance p0, Lcom/squareup/protos/client/bills/CardData$Builder;

    invoke-direct {p0}, Lcom/squareup/protos/client/bills/CardData$Builder;-><init>()V

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 36
    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p0

    .line 37
    invoke-static {p1}, Lcom/squareup/payment/CardConverterUtils;->buildR6CardForDip([B)Lcom/squareup/protos/client/bills/CardData$R6Card;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->r6_card(Lcom/squareup/protos/client/bills/CardData$R6Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p0

    .line 38
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object p0

    return-object p0

    .line 40
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getPaymentInstrument: Unknown reader type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 30
    :cond_1
    new-instance p0, Lcom/squareup/protos/client/bills/CardData$Builder;

    invoke-direct {p0}, Lcom/squareup/protos/client/bills/CardData$Builder;-><init>()V

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 31
    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p0

    .line 32
    invoke-static {p1}, Lcom/squareup/payment/CardConverterUtils;->buildT2Card([B)Lcom/squareup/protos/client/bills/CardData$T2Card;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->t2_card(Lcom/squareup/protos/client/bills/CardData$T2Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p0

    .line 33
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object p0

    return-object p0

    .line 25
    :cond_2
    new-instance p0, Lcom/squareup/protos/client/bills/CardData$Builder;

    invoke-direct {p0}, Lcom/squareup/protos/client/bills/CardData$Builder;-><init>()V

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->X2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 26
    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p0

    .line 27
    invoke-static {p1}, Lcom/squareup/payment/CardConverterUtils;->buildX2Card([B)Lcom/squareup/protos/client/bills/CardData$X2Card;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->x2_card(Lcom/squareup/protos/client/bills/CardData$X2Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p0

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object p0

    return-object p0

    .line 20
    :cond_3
    new-instance p0, Lcom/squareup/protos/client/bills/CardData$Builder;

    invoke-direct {p0}, Lcom/squareup/protos/client/bills/CardData$Builder;-><init>()V

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 21
    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p0

    .line 22
    invoke-static {p1}, Lcom/squareup/payment/CardConverterUtils;->buildR12Card([B)Lcom/squareup/protos/client/bills/CardData$R12Card;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->r12_card(Lcom/squareup/protos/client/bills/CardData$R12Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p0

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object p0

    return-object p0
.end method
