.class public final Lcom/squareup/payment/DefaultCartSort;
.super Ljava/lang/Object;
.source "DefaultCartSort.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/DefaultCartSort$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"
    }
    d2 = {
        "Lcom/squareup/payment/DefaultCartSort;",
        "",
        "()V",
        "Companion",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/payment/DefaultCartSort$Companion;

.field public static final Comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/payment/DefaultCartSort$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/payment/DefaultCartSort$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/payment/DefaultCartSort;->Companion:Lcom/squareup/payment/DefaultCartSort$Companion;

    .line 10
    sget-object v0, Lcom/squareup/payment/DefaultCartSort$Companion$Comparator$1;->INSTANCE:Lcom/squareup/payment/DefaultCartSort$Companion$Comparator$1;

    check-cast v0, Ljava/util/Comparator;

    sput-object v0, Lcom/squareup/payment/DefaultCartSort;->Comparator:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
