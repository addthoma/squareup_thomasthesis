.class public Lcom/squareup/payment/InvoicePayment;
.super Lcom/squareup/payment/Payment;
.source "InvoicePayment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/InvoicePayment$Factory;
    }
.end annotation


# static fields
.field private static final ID_PAIR:Ljava/lang/String; = "idPair"

.field private static final IS_DRAFT:Ljava/lang/String; = "isDraft"

.field private static final IS_SCHEDULED:Ljava/lang/String; = "isScheduled"

.field private static final IS_SHARE_LINK:Ljava/lang/String; = "isShareLink"

.field private static final SENT:Ljava/lang/String; = "sent"


# instance fields
.field private buyerName:Ljava/lang/String;

.field private customerContact:Lcom/squareup/protos/client/rolodex/Contact;

.field private customerInstruments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;"
        }
    .end annotation
.end field

.field private customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

.field private email:Ljava/lang/String;

.field private idPair:Lcom/squareup/protos/client/IdPair;

.field private instrument:Lcom/squareup/protos/client/instruments/InstrumentSummary;

.field private isChargedInvoice:Z

.field private isDraft:Z

.field private isScheduled:Z

.field private isShareLinkInvoice:Z

.field private final onCustomerChanged:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final receiptFactory:Lcom/squareup/payment/PaymentReceipt$Factory;

.field private sent:Z


# direct methods
.method constructor <init>(Lcom/squareup/payment/InvoicePayment$Factory;Lcom/squareup/payment/Order;)V
    .locals 1

    .line 65
    invoke-static {p1}, Lcom/squareup/payment/InvoicePayment$Factory;->access$000(Lcom/squareup/payment/InvoicePayment$Factory;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/squareup/payment/Payment;-><init>(Lcom/squareup/payment/Order;Lcom/squareup/analytics/Analytics;)V

    .line 49
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/InvoicePayment;->onCustomerChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 66
    invoke-static {p1}, Lcom/squareup/payment/InvoicePayment$Factory;->access$100(Lcom/squareup/payment/InvoicePayment$Factory;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/payment/PaymentReceipt$Factory;

    iput-object p1, p0, Lcom/squareup/payment/InvoicePayment;->receiptFactory:Lcom/squareup/payment/PaymentReceipt$Factory;

    .line 67
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/InvoicePayment;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 68
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/InvoicePayment;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    .line 69
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getCustomerInstrumentDetails()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/InvoicePayment;->customerInstruments:Ljava/util/List;

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "Started a new Invoice payment."

    .line 70
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public endCurrentTenderAndCreateReceipt()Lcom/squareup/payment/PaymentReceipt;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/payment/InvoicePayment;->receiptFactory:Lcom/squareup/payment/PaymentReceipt$Factory;

    invoke-interface {v0}, Lcom/squareup/payment/PaymentReceipt$Factory;->createVoidReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    return-object v0
.end method

.method public enqueueAttachContactTask()V
    .locals 0

    return-void
.end method

.method public getBuyerName()Ljava/lang/String;
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/squareup/payment/InvoicePayment;->buyerName:Ljava/lang/String;

    return-object v0
.end method

.method public getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 214
    iget-object v0, p0, Lcom/squareup/payment/InvoicePayment;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public getCustomerId()Ljava/lang/String;
    .locals 1

    .line 210
    iget-object v0, p0, Lcom/squareup/payment/InvoicePayment;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->customer_id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getCustomerInstrumentDetails()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;"
        }
    .end annotation

    .line 222
    iget-object v0, p0, Lcom/squareup/payment/InvoicePayment;->customerInstruments:Ljava/util/List;

    return-object v0
.end method

.method public getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;
    .locals 1

    .line 218
    iget-object v0, p0, Lcom/squareup/payment/InvoicePayment;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/squareup/payment/InvoicePayment;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/payment/InvoicePayment;->idPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public getInstrumentToCharge()Lcom/squareup/protos/client/instruments/InstrumentSummary;
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/payment/InvoicePayment;->instrument:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    return-object v0
.end method

.method public getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 178
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "An invoice has no receipt."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;
    .locals 1

    .line 182
    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->INVOICE:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    return-object v0
.end method

.method public hasCustomer()Z
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/squareup/payment/InvoicePayment;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->customer_id:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isCaptured()Z
    .locals 1

    .line 78
    iget-boolean v0, p0, Lcom/squareup/payment/InvoicePayment;->sent:Z

    return v0
.end method

.method public isChargedInvoice()Z
    .locals 1

    .line 126
    iget-boolean v0, p0, Lcom/squareup/payment/InvoicePayment;->isChargedInvoice:Z

    return v0
.end method

.method public isDraft()Z
    .locals 1

    .line 102
    iget-boolean v0, p0, Lcom/squareup/payment/InvoicePayment;->isDraft:Z

    return v0
.end method

.method public isLocalPayment()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isScheduled()Z
    .locals 1

    .line 110
    iget-boolean v0, p0, Lcom/squareup/payment/InvoicePayment;->isScheduled:Z

    return v0
.end method

.method public isShareLinkInvoice()Z
    .locals 1

    .line 118
    iget-boolean v0, p0, Lcom/squareup/payment/InvoicePayment;->isShareLinkInvoice:Z

    return v0
.end method

.method public onCustomerChanged()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 226
    iget-object v0, p0, Lcom/squareup/payment/InvoicePayment;->onCustomerChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public restore(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "idPair"

    .line 164
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    :try_start_0
    sget-object v1, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p0, Lcom/squareup/payment/InvoicePayment;->idPair:Lcom/squareup/protos/client/IdPair;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    .line 168
    iput-object v0, p0, Lcom/squareup/payment/InvoicePayment;->idPair:Lcom/squareup/protos/client/IdPair;

    :cond_0
    :goto_0
    const-string v0, "sent"

    .line 171
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/InvoicePayment;->sent:Z

    const-string v0, "isDraft"

    .line 172
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/InvoicePayment;->isDraft:Z

    const-string v0, "isScheduled"

    .line 173
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/InvoicePayment;->isScheduled:Z

    const-string v0, "isShareLink"

    .line 174
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/payment/InvoicePayment;->isShareLinkInvoice:Z

    return-void
.end method

.method public save(Landroid/os/Bundle;)V
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/squareup/payment/InvoicePayment;->idPair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p0, Lcom/squareup/payment/InvoicePayment;->idPair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object v0

    const-string v1, "idPair"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 157
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/payment/InvoicePayment;->sent:Z

    const-string v1, "sent"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 158
    iget-boolean v0, p0, Lcom/squareup/payment/InvoicePayment;->isDraft:Z

    const-string v1, "isDraft"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 159
    iget-boolean v0, p0, Lcom/squareup/payment/InvoicePayment;->isScheduled:Z

    const-string v1, "isScheduled"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 160
    iget-boolean v0, p0, Lcom/squareup/payment/InvoicePayment;->isShareLinkInvoice:Z

    const-string v1, "isShareLink"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public setBuyerName(Ljava/lang/String;)V
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/payment/InvoicePayment;->buyerName:Ljava/lang/String;

    return-void
.end method

.method public setCaptured(Z)V
    .locals 0

    .line 82
    iput-boolean p1, p0, Lcom/squareup/payment/InvoicePayment;->sent:Z

    return-void
.end method

.method public setChargedInvoice(ZLcom/squareup/protos/client/instruments/InstrumentSummary;)V
    .locals 0

    .line 134
    iput-object p2, p0, Lcom/squareup/payment/InvoicePayment;->instrument:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    .line 135
    iput-boolean p1, p0, Lcom/squareup/payment/InvoicePayment;->isChargedInvoice:Z

    return-void
.end method

.method public setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;)V"
        }
    .end annotation

    .line 195
    iput-object p1, p0, Lcom/squareup/payment/InvoicePayment;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 196
    iput-object p2, p0, Lcom/squareup/payment/InvoicePayment;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    .line 197
    invoke-static {p1, p3}, Lcom/squareup/cardonfile/StoredInstrumentHelper;->getInstrumentFromContactUnlessNull(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/InvoicePayment;->customerInstruments:Ljava/util/List;

    .line 200
    iget-object p1, p0, Lcom/squareup/payment/InvoicePayment;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    const/4 p2, 0x1

    const/4 p3, 0x0

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/payment/InvoicePayment;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 201
    :goto_0
    iget-object v0, p0, Lcom/squareup/payment/InvoicePayment;->customerInstruments:Ljava/util/List;

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :goto_1
    if-nez p1, :cond_2

    if-eqz p2, :cond_3

    .line 203
    :cond_2
    iget-object p1, p0, Lcom/squareup/payment/InvoicePayment;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object p2, p0, Lcom/squareup/payment/InvoicePayment;->customerInstruments:Ljava/util/List;

    invoke-static {p1, p2}, Lcom/squareup/crm/util/RolodexContactHelper;->toBuyerInfo(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/InvoicePayment;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    .line 206
    :cond_3
    iget-object p1, p0, Lcom/squareup/payment/InvoicePayment;->onCustomerChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setDraft(Z)V
    .locals 0

    .line 106
    iput-boolean p1, p0, Lcom/squareup/payment/InvoicePayment;->isDraft:Z

    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/payment/InvoicePayment;->email:Ljava/lang/String;

    return-void
.end method

.method public setIdPair(Lcom/squareup/protos/client/IdPair;)V
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/payment/InvoicePayment;->idPair:Lcom/squareup/protos/client/IdPair;

    return-void
.end method

.method public setScheduled(Z)V
    .locals 0

    .line 114
    iput-boolean p1, p0, Lcom/squareup/payment/InvoicePayment;->isScheduled:Z

    return-void
.end method

.method public setShareLinkInvoice(Z)V
    .locals 0

    .line 122
    iput-boolean p1, p0, Lcom/squareup/payment/InvoicePayment;->isShareLinkInvoice:Z

    return-void
.end method

.method public shouldSkipReceipt()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
