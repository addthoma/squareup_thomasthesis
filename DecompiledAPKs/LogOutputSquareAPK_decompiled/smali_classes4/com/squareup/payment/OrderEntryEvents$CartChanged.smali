.class public Lcom/squareup/payment/OrderEntryEvents$CartChanged;
.super Ljava/lang/Object;
.source "OrderEntryEvents.java"

# interfaces
.implements Lcom/squareup/log/OhSnapLoggable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/OrderEntryEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CartChanged"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;
    }
.end annotation


# static fields
.field public static final ADDITIVE:Z = true

.field public static final VIA_TICKET:Z = true


# instance fields
.field public final animate:Z

.field public final cardChanged:Z

.field public final diningOptionChanged:Z

.field public final discountsChanged:Z

.field public final itemsChanged:Z

.field public final keypadCommitted:Z

.field public final paymentChanged:Z

.field public final returnChanged:Z

.field public final source:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

.field public final status:Lcom/squareup/payment/OrderEntryEvents$ItemStatus;

.field public final viaTicket:Z


# direct methods
.method private constructor <init>(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)V
    .locals 1

    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243
    invoke-static {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->access$100(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->cardChanged:Z

    .line 244
    invoke-static {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->access$200(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->diningOptionChanged:Z

    .line 245
    invoke-static {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->access$300(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->discountsChanged:Z

    .line 246
    invoke-static {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->access$400(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->itemsChanged:Z

    .line 247
    invoke-static {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->access$500(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->keypadCommitted:Z

    .line 248
    invoke-static {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->access$600(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->returnChanged:Z

    .line 249
    invoke-static {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->access$700(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->paymentChanged:Z

    .line 250
    invoke-static {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->access$800(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->animate:Z

    .line 251
    invoke-static {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->access$900(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->viaTicket:Z

    .line 252
    invoke-static {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->access$1000(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Lcom/squareup/payment/OrderEntryEvents$ItemStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->status:Lcom/squareup/payment/OrderEntryEvents$ItemStatus;

    .line 253
    invoke-static {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->access$1100(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->source:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;Lcom/squareup/payment/OrderEntryEvents$1;)V
    .locals 0

    .line 89
    invoke-direct {p0, p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;-><init>(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)V

    return-void
.end method

.method public static cardChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 1

    .line 124
    sget-object v0, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->MANUAL:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-static {v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->cardChanged(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object v0

    return-object v0
.end method

.method public static cardChanged(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 2

    .line 128
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;-><init>(Lcom/squareup/payment/OrderEntryEvents$1;)V

    invoke-virtual {v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->cardChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->source(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->build()Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method

.method public static diningOptionChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 1

    .line 132
    sget-object v0, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->MANUAL:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-static {v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->diningOptionChanged(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object v0

    return-object v0
.end method

.method public static diningOptionChanged(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 2

    .line 136
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;-><init>(Lcom/squareup/payment/OrderEntryEvents$1;)V

    invoke-virtual {v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->diningOptionChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->source(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->build()Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method

.method public static discountsChanged(Z)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 1

    .line 140
    sget-object v0, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->MANUAL:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-static {p0, v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->discountsChanged(ZLcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method

.method public static discountsChanged(ZLcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 2

    .line 144
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;-><init>(Lcom/squareup/payment/OrderEntryEvents$1;)V

    invoke-virtual {v0, p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->animate(Z)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->discountsChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->source(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->build()Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method

.method public static discountsChanged(ZLcom/squareup/payment/OrderEntryEvents$ItemChangeType;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 1

    .line 148
    sget-object v0, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->MANUAL:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-static {p0, p1, v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->discountsChanged(ZLcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method

.method public static discountsChanged(ZLcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 2

    .line 155
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;-><init>(Lcom/squareup/payment/OrderEntryEvents$1;)V

    .line 156
    invoke-virtual {v0, p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->animate(Z)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    .line 157
    invoke-virtual {p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->discountsChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    sget-object v0, Lcom/squareup/payment/OrderEntryEvents$ItemType;->DISCOUNT:Lcom/squareup/payment/OrderEntryEvents$ItemType;

    .line 158
    invoke-virtual {p0, p1, v1, v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->status(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/checkout/CartItem;Lcom/squareup/payment/OrderEntryEvents$ItemType;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    .line 159
    invoke-virtual {p0, p2}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->source(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    .line 160
    invoke-virtual {p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->build()Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method

.method public static everythingChanged(Z)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 1

    .line 116
    sget-object v0, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->MANUAL:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-static {p0, v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->everythingChanged(ZLcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method

.method public static everythingChanged(ZLcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 2

    .line 120
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;-><init>(Lcom/squareup/payment/OrderEntryEvents$1;)V

    invoke-virtual {v0, p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->viaTicket(Z)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->everythingChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->source(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->build()Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method

.method public static itemAdded(ZLcom/squareup/checkout/CartItem;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 1

    .line 164
    sget-object v0, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->MANUAL:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-static {p0, p1, v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->itemAdded(ZLcom/squareup/checkout/CartItem;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method

.method public static itemAdded(ZLcom/squareup/checkout/CartItem;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 2

    .line 171
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;-><init>(Lcom/squareup/payment/OrderEntryEvents$1;)V

    .line 172
    invoke-virtual {v0, p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->animate(Z)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    .line 173
    invoke-virtual {p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->itemsChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    sget-object v0, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->ADD:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    .line 174
    invoke-virtual {p0, v0, p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->status(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/checkout/CartItem;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    .line 175
    invoke-virtual {p0, p2}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->source(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    .line 176
    invoke-virtual {p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->build()Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method

.method public static itemEdited(Lcom/squareup/checkout/CartItem;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 1

    .line 180
    sget-object v0, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->MANUAL:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-static {p0, v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->itemEdited(Lcom/squareup/checkout/CartItem;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method

.method public static itemEdited(Lcom/squareup/checkout/CartItem;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 2

    .line 187
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;-><init>(Lcom/squareup/payment/OrderEntryEvents$1;)V

    invoke-virtual {v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->itemsChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->EDIT:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    invoke-virtual {v0, v1, p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->status(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/checkout/CartItem;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->source(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->build()Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method

.method public static itemRemoved(Lcom/squareup/checkout/CartItem;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 1

    .line 191
    sget-object v0, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->MANUAL:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-static {p0, v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->itemRemoved(Lcom/squareup/checkout/CartItem;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method

.method public static itemRemoved(Lcom/squareup/checkout/CartItem;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 2

    .line 198
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;-><init>(Lcom/squareup/payment/OrderEntryEvents$1;)V

    const/4 v1, 0x1

    .line 199
    invoke-virtual {v0, v1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->animate(Z)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object v0

    .line 200
    invoke-virtual {v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->itemsChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->DELETE:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    .line 201
    invoke-virtual {v0, v1, p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->status(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/checkout/CartItem;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    .line 202
    invoke-virtual {p0, p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->source(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    .line 203
    invoke-virtual {p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->build()Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method

.method public static itemsChanged(Z)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 1

    .line 207
    sget-object v0, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->MANUAL:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-static {p0, v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->itemsChanged(ZLcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method

.method public static itemsChanged(ZLcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 2

    .line 211
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;-><init>(Lcom/squareup/payment/OrderEntryEvents$1;)V

    invoke-virtual {v0, p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->animate(Z)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->itemsChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->source(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->build()Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method

.method public static keypadCommitted()Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 2

    .line 215
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;-><init>(Lcom/squareup/payment/OrderEntryEvents$1;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->animate(Z)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->keypadCommitted()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->MANUAL:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->source(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->build()Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object v0

    return-object v0
.end method

.method public static paymentChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 1

    .line 219
    sget-object v0, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->MANUAL:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-static {v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->paymentChanged(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object v0

    return-object v0
.end method

.method public static paymentChanged(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 2

    .line 223
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;-><init>(Lcom/squareup/payment/OrderEntryEvents$1;)V

    invoke-virtual {v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->paymentChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->source(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->build()Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method

.method public static returnChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 2

    .line 239
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;-><init>(Lcom/squareup/payment/OrderEntryEvents$1;)V

    invoke-virtual {v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->returnChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->MANUAL:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->source(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->build()Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object v0

    return-object v0
.end method

.method public static taxesChanged(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 1

    .line 227
    sget-object v0, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->MANUAL:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    invoke-static {p0, v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->taxesChanged(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method

.method public static taxesChanged(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 3

    .line 231
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;-><init>(Lcom/squareup/payment/OrderEntryEvents$1;)V

    .line 232
    invoke-virtual {v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->everythingChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object v0

    sget-object v2, Lcom/squareup/payment/OrderEntryEvents$ItemType;->TAX:Lcom/squareup/payment/OrderEntryEvents$ItemType;

    .line 233
    invoke-virtual {v0, p0, v1, v2}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->status(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/checkout/CartItem;Lcom/squareup/payment/OrderEntryEvents$ItemType;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    .line 234
    invoke-virtual {p0, p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->source(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p0

    .line 235
    invoke-virtual {p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->build()Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public everythingChanged()Z
    .locals 1

    .line 284
    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->cardChanged:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->diningOptionChanged:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->discountsChanged:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->itemsChanged:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->paymentChanged:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getOhSnapMessage()Ljava/lang/String;
    .locals 3

    .line 296
    iget-object v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->status:Lcom/squareup/payment/OrderEntryEvents$ItemStatus;

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ", status.changeType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->status:Lcom/squareup/payment/OrderEntryEvents$ItemStatus;

    iget-object v1, v1, Lcom/squareup/payment/OrderEntryEvents$ItemStatus;->changeType:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", status.itemType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->status:Lcom/squareup/payment/OrderEntryEvents$ItemStatus;

    iget-object v1, v1, Lcom/squareup/payment/OrderEntryEvents$ItemStatus;->itemType:Lcom/squareup/payment/OrderEntryEvents$ItemType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 299
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v2, "{cardChanged="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->cardChanged:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ", diningOptionChanged="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->diningOptionChanged:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ", discountsChanged="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->discountsChanged:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ", itemsChanged="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->itemsChanged:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ", paymentChanged="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->paymentChanged:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ", keypadCommitted="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->keypadCommitted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ", changeSource="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->source:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    .line 306
    invoke-virtual {v2}, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onlyDiscountChanged()Z
    .locals 1

    .line 257
    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->cardChanged:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->diningOptionChanged:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->discountsChanged:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->itemsChanged:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->paymentChanged:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->keypadCommitted:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onlyItemChanged()Z
    .locals 1

    .line 266
    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->cardChanged:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->diningOptionChanged:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->discountsChanged:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->itemsChanged:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->paymentChanged:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->keypadCommitted:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onlyKeypadCommitted()Z
    .locals 1

    .line 275
    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->cardChanged:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->diningOptionChanged:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->discountsChanged:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->itemsChanged:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->paymentChanged:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->keypadCommitted:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public viaPricingEngine()Z
    .locals 2

    .line 292
    iget-object v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->source:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    sget-object v1, Lcom/squareup/payment/OrderEntryEvents$ChangeSource;->PRICING_RULE:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
