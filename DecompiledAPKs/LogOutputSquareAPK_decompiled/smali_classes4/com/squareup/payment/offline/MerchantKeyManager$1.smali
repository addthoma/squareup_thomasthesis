.class final Lcom/squareup/payment/offline/MerchantKeyManager$1;
.super Ljava/lang/Object;
.source "MerchantKeyManager.java"

# interfaces
.implements Lcom/squareup/encryption/CryptoKeyAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/offline/MerchantKeyManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/encryption/CryptoKeyAdapter<",
        "Lcom/squareup/server/account/protos/User$MerchantKey;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getKeyId(Lcom/squareup/server/account/protos/User$MerchantKey;)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic getKeyId(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/server/account/protos/User$MerchantKey;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/offline/MerchantKeyManager$1;->getKeyId(Lcom/squareup/server/account/protos/User$MerchantKey;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getRawKey(Lcom/squareup/server/account/protos/User$MerchantKey;)[B
    .locals 0

    .line 22
    iget-object p1, p1, Lcom/squareup/server/account/protos/User$MerchantKey;->key:Ljava/lang/String;

    invoke-static {p1}, Lokio/ByteString;->decodeBase64(Ljava/lang/String;)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getRawKey(Ljava/lang/Object;)[B
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/server/account/protos/User$MerchantKey;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/offline/MerchantKeyManager$1;->getRawKey(Lcom/squareup/server/account/protos/User$MerchantKey;)[B

    move-result-object p1

    return-object p1
.end method

.method public isExpired(Lcom/squareup/server/account/protos/User$MerchantKey;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic isExpired(Ljava/lang/Object;)Z
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/server/account/protos/User$MerchantKey;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/offline/MerchantKeyManager$1;->isExpired(Lcom/squareup/server/account/protos/User$MerchantKey;)Z

    move-result p1

    return p1
.end method
