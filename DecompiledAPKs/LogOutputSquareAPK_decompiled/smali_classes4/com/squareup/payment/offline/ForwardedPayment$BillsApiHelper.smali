.class Lcom/squareup/payment/offline/ForwardedPayment$BillsApiHelper;
.super Ljava/lang/Object;
.source "ForwardedPayment.java"

# interfaces
.implements Lcom/squareup/payment/offline/ForwardedPayment$Helper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/offline/ForwardedPayment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BillsApiHelper"
.end annotation


# instance fields
.field private final billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

.field private final completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;


# direct methods
.method constructor <init>(Lcom/squareup/queue/bills/CompleteBill;Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;)V
    .locals 0

    .line 219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220
    iput-object p1, p0, Lcom/squareup/payment/offline/ForwardedPayment$BillsApiHelper;->completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

    .line 221
    iput-object p2, p0, Lcom/squareup/payment/offline/ForwardedPayment$BillsApiHelper;->billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    return-void
.end method


# virtual methods
.method public asBill(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 3

    .line 241
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment$BillsApiHelper;->completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

    invoke-virtual {p0}, Lcom/squareup/payment/offline/ForwardedPayment$BillsApiHelper;->getBillId()Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/queue/bills/CompleteBill;->asBillBuilder(Lcom/squareup/util/Res;Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->FORWARDED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    .line 242
    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setStoreAndForward(Lcom/squareup/transactionhistory/pending/StoreAndForwardState;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    .line 243
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment$BillsApiHelper;->billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    if-eqz v0, :cond_2

    .line 244
    sget-object v0, Lcom/squareup/payment/offline/ForwardedPayment$1;->$SwitchMap$com$squareup$protos$client$store_and_forward$bills$BillProcessingResult$Status:[I

    iget-object v1, p0, Lcom/squareup/payment/offline/ForwardedPayment$BillsApiHelper;->billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    iget-object v1, v1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->status:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    sget-object v2, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->STATUS_UNKNOWN:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    invoke-static {v1, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    invoke-virtual {v1}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 249
    :cond_0
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$State;->LOST:Lcom/squareup/protos/client/bills/Tender$State;

    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setPaymentState(Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    goto :goto_0

    .line 246
    :cond_1
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$State;->SETTLED:Lcom/squareup/protos/client/bills/Tender$State;

    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setPaymentState(Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    .line 257
    :cond_2
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    return-object p1
.end method

.method public getBillId()Lcom/squareup/billhistory/model/BillHistoryId;
    .locals 1

    .line 233
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment$BillsApiHelper;->billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->bill_or_payment_id:Lcom/squareup/protos/client/IdPair;

    invoke-static {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->forBillIdPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    return-object v0

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment$BillsApiHelper;->completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

    invoke-virtual {v0}, Lcom/squareup/queue/bills/CompleteBill;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->forBill(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    return-object v0
.end method

.method public getUniqueKey()Ljava/lang/String;
    .locals 1

    .line 261
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment$BillsApiHelper;->billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->bill_or_payment_id:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public isPending()Z
    .locals 3

    .line 225
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment$BillsApiHelper;->billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    .line 228
    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->status:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    sget-object v2, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->UNPROCESSED:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPayment$BillsApiHelper;->billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    iget-object v0, v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->status:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    sget-object v2, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->PROCESSING:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    if-ne v0, v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_0
    return v1
.end method

.method public withResult(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;)Lcom/squareup/payment/offline/ForwardedPayment;
    .locals 8

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 270
    iget-object v1, p0, Lcom/squareup/payment/offline/ForwardedPayment$BillsApiHelper;->billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    iget-object v1, v1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->bill_or_payment_id:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/payment/offline/ForwardedPayment$BillsApiHelper;->billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    iget-object v1, v1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->status:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    .line 271
    invoke-virtual {v1}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->name()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "Bill %s now in state %s"

    .line 270
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 272
    new-instance v0, Lcom/squareup/payment/offline/ForwardedPayment;

    iget-object v4, p0, Lcom/squareup/payment/offline/ForwardedPayment$BillsApiHelper;->completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v2, v0

    move-object v6, p1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/payment/offline/ForwardedPayment;-><init>(Lcom/squareup/queue/Capture;Lcom/squareup/queue/bills/CompleteBill;Lcom/squareup/protos/queuebert/model/PaymentResult;Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;Lcom/squareup/payment/offline/ForwardedPayment$1;)V

    return-object v0
.end method

.method public withResult(Lcom/squareup/protos/queuebert/model/PaymentResult;)Lcom/squareup/payment/offline/ForwardedPayment;
    .locals 1

    .line 266
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Can\'t add a PaymentResult to a non-V1 payment"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
