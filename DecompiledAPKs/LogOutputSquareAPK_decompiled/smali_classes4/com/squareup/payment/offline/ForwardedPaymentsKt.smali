.class public final Lcom/squareup/payment/offline/ForwardedPaymentsKt;
.super Ljava/lang/Object;
.source "ForwardedPayments.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0000\n\u0002\u0010\u000e\n\u0000\u001a*\u0010\u0000\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u0001*\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00040\u00012\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "maybeForwardedPaymentWithId",
        "Lio/reactivex/Single;",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/payment/offline/ForwardedPayment;",
        "",
        "transactionId",
        "",
        "transaction_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final maybeForwardedPaymentWithId(Lio/reactivex/Single;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Single<",
            "Ljava/util/Collection<",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;>;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;>;"
        }
    .end annotation

    const-string v0, "$this$maybeForwardedPaymentWithId"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transactionId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    new-instance v0, Lcom/squareup/payment/offline/ForwardedPaymentsKt$maybeForwardedPaymentWithId$1;

    invoke-direct {v0, p1}, Lcom/squareup/payment/offline/ForwardedPaymentsKt$maybeForwardedPaymentWithId$1;-><init>(Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p0, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p0

    const-string p1, "map { list ->\n  ofNullab\u2026Key == transactionId })\n}"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
