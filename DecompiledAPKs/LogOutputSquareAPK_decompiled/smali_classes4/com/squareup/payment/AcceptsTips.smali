.class public interface abstract Lcom/squareup/payment/AcceptsTips;
.super Ljava/lang/Object;
.source "AcceptsTips.java"


# virtual methods
.method public abstract askForTip()Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getAmountForTipping()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getCustomTipMaxMoney()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getTip()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getTipOptions()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V
.end method

.method public abstract tipOnPrintedReceipt()Z
.end method

.method public abstract useSeparateTippingScreen()Z
.end method
