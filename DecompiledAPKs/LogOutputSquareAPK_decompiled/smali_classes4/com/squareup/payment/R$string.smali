.class public final Lcom/squareup/payment/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final any_amount_over:I = 0x7f1200cd

.field public static final card_on_file_charge_confirmation_title:I = 0x7f12031b

.field public static final change_hud_amount_change:I = 0x7f1203ef

.field public static final change_hud_no_change:I = 0x7f1203f0

.field public static final change_hud_out_of_total:I = 0x7f1203f1

.field public static final change_hud_payment_complete:I = 0x7f1203f2

.field public static final pending_payments_notification_content_plural:I = 0x7f12142a

.field public static final pending_payments_notification_content_singular:I = 0x7f12142b

.field public static final pending_payments_notification_title_plural:I = 0x7f12142c

.field public static final pending_payments_notification_title_singular:I = 0x7f12142d

.field public static final third_party_card_disclaimer:I = 0x7f12196d


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
