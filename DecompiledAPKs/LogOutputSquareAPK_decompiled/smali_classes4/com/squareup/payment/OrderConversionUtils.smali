.class public final Lcom/squareup/payment/OrderConversionUtils;
.super Ljava/lang/Object;
.source "OrderConversionUtils.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderConversionUtils.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderConversionUtils.kt\ncom/squareup/payment/OrderConversionUtils\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,95:1\n1360#2:96\n1429#2,3:97\n1360#2:100\n1429#2,3:101\n*E\n*S KotlinDebug\n*F\n+ 1 OrderConversionUtils.kt\ncom/squareup/payment/OrderConversionUtils\n*L\n67#1:96\n67#1,3:97\n72#1:100\n72#1,3:101\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J*\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0007J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J \u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u00082\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J\u0015\u0010\u0012\u001a\u00020\u000e*\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u000eH\u0082\u0002\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/payment/OrderConversionUtils;",
        "",
        "()V",
        "getAmountDetails",
        "Lcom/squareup/protos/client/bills/Cart$AmountDetails;",
        "saleAdjuster",
        "Lcom/squareup/calc/Adjuster;",
        "saleTip",
        "Lcom/squareup/protos/common/Money;",
        "returnCart",
        "Lcom/squareup/checkout/ReturnCart;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "getReturnAmounts",
        "Lcom/squareup/protos/client/bills/Cart$Amounts;",
        "getSaleAmounts",
        "adjuster",
        "tipAmount",
        "minus",
        "other",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/payment/OrderConversionUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/payment/OrderConversionUtils;

    invoke-direct {v0}, Lcom/squareup/payment/OrderConversionUtils;-><init>()V

    sput-object v0, Lcom/squareup/payment/OrderConversionUtils;->INSTANCE:Lcom/squareup/payment/OrderConversionUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final getAmountDetails(Lcom/squareup/calc/Adjuster;Lcom/squareup/protos/common/Money;Lcom/squareup/checkout/ReturnCart;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/bills/Cart$AmountDetails;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "saleAdjuster"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "returnCart"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    new-instance v0, Lcom/squareup/protos/common/Money;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 28
    sget-object v1, Lcom/squareup/payment/OrderConversionUtils;->INSTANCE:Lcom/squareup/payment/OrderConversionUtils;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    invoke-direct {v1, p0, p1, p3}, Lcom/squareup/payment/OrderConversionUtils;->getSaleAmounts(Lcom/squareup/calc/Adjuster;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/bills/Cart$Amounts;

    move-result-object p0

    .line 34
    sget-object p1, Lcom/squareup/payment/OrderConversionUtils;->INSTANCE:Lcom/squareup/payment/OrderConversionUtils;

    invoke-direct {p1, p2, p3}, Lcom/squareup/payment/OrderConversionUtils;->getReturnAmounts(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/bills/Cart$Amounts;

    move-result-object p1

    .line 39
    new-instance p2, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;-><init>()V

    .line 40
    invoke-virtual {p2, p0}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->sale(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;

    move-result-object p2

    .line 41
    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->return_(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;

    move-result-object p2

    .line 42
    sget-object p3, Lcom/squareup/payment/OrderConversionUtils;->INSTANCE:Lcom/squareup/payment/OrderConversionUtils;

    invoke-direct {p3, p0, p1}, Lcom/squareup/payment/OrderConversionUtils;->minus(Lcom/squareup/protos/client/bills/Cart$Amounts;Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$Amounts;

    move-result-object p0

    invoke-virtual {p2, p0}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->net(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;

    move-result-object p0

    .line 43
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    move-result-object p0

    const-string p1, "AmountDetails.Builder()\n\u2026Amounts)\n        .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private final getReturnAmounts(Lcom/squareup/checkout/ReturnCart;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/bills/Cart$Amounts;
    .locals 7

    .line 65
    new-instance v0, Lcom/squareup/protos/common/Money;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 67
    invoke-virtual {p1}, Lcom/squareup/checkout/ReturnCart;->getReturnTaxes()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 96
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 97
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 98
    check-cast v4, Lcom/squareup/checkout/ReturnTax;

    .line 67
    invoke-virtual {v4}, Lcom/squareup/checkout/ReturnTax;->getAppliedAmount()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 99
    :cond_0
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 67
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->sumOfLong(Ljava/lang/Iterable;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 66
    new-instance v2, Lcom/squareup/protos/common/Money;

    invoke-direct {v2, v1, p2}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 72
    invoke-virtual {p1}, Lcom/squareup/checkout/ReturnCart;->getReturnDiscounts()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 100
    new-instance v4, Ljava/util/ArrayList;

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 101
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 102
    check-cast v3, Lcom/squareup/checkout/ReturnDiscount;

    .line 72
    invoke-virtual {v3}, Lcom/squareup/checkout/ReturnDiscount;->getAppliedAmount()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 103
    :cond_1
    check-cast v4, Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 72
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->sumOfLong(Ljava/lang/Iterable;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 71
    new-instance v3, Lcom/squareup/protos/common/Money;

    invoke-direct {v3, v1, p2}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 76
    new-instance p2, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;-><init>()V

    .line 77
    invoke-virtual {p1}, Lcom/squareup/checkout/ReturnCart;->getTotalReturnAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object p2

    .line 78
    invoke-virtual {p2, v2}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object p2

    .line 79
    invoke-virtual {p2, v3}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->discount_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object p2

    .line 80
    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->surcharge_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object p2

    .line 81
    invoke-virtual {p1}, Lcom/squareup/checkout/ReturnCart;->getTipAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object p1

    .line 82
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Cart$Amounts;

    move-result-object p1

    const-string p2, "Builder()\n        .total\u2026mount())\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getSaleAmounts(Lcom/squareup/calc/Adjuster;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/bills/Cart$Amounts;
    .locals 4

    .line 51
    new-instance v0, Lcom/squareup/protos/common/Money;

    invoke-virtual {p1}, Lcom/squareup/calc/Adjuster;->getTotal()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-static {v0, p2}, Lcom/squareup/money/MoneyMathOperatorsKt;->plus(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 52
    new-instance v1, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;-><init>()V

    .line 53
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object v0

    .line 54
    new-instance v1, Lcom/squareup/protos/common/Money;

    invoke-virtual {p1}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForAllTaxes()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v1, v2, p3}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object v0

    .line 55
    new-instance v1, Lcom/squareup/protos/common/Money;

    invoke-virtual {p1}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForAllDiscounts()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v1, v2, p3}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->discount_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object v0

    .line 56
    new-instance v1, Lcom/squareup/protos/common/Money;

    invoke-virtual {p1}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForAllSurcharges()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-direct {v1, p1, p3}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->surcharge_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object p1

    .line 57
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object p1

    .line 58
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Cart$Amounts;

    move-result-object p1

    const-string p2, "Builder()\n        .total\u2026pAmount)\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final minus(Lcom/squareup/protos/client/bills/Cart$Amounts;Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$Amounts;
    .locals 3

    const-string v0, "$this$minus"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;-><init>()V

    .line 87
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$Amounts;->discount_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p2, Lcom/squareup/protos/client/bills/Cart$Amounts;->discount_money:Lcom/squareup/protos/common/Money;

    invoke-static {v1, v2}, Lcom/squareup/money/MoneyMath;->subtractNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->discount_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object v0

    .line 88
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p2, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    invoke-static {v1, v2}, Lcom/squareup/money/MoneyMath;->subtractNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object v0

    .line 89
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$Amounts;->tax_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p2, Lcom/squareup/protos/client/bills/Cart$Amounts;->tax_money:Lcom/squareup/protos/common/Money;

    invoke-static {v1, v2}, Lcom/squareup/money/MoneyMath;->subtractNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object v0

    .line 90
    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p2, Lcom/squareup/protos/client/bills/Cart$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-static {v1, v2}, Lcom/squareup/money/MoneyMath;->subtractNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object v0

    .line 91
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$Amounts;->surcharge_money:Lcom/squareup/protos/common/Money;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$Amounts;->surcharge_money:Lcom/squareup/protos/common/Money;

    invoke-static {p1, p2}, Lcom/squareup/money/MoneyMath;->subtractNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->surcharge_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object p1

    .line 92
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Cart$Amounts;

    move-result-object p1

    const-string p2, "Amounts.Builder()\n      \u2026_money))\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
