.class Lcom/squareup/payment/CardConverter$FallbackKeyAdapter;
.super Ljava/lang/Object;
.source "CardConverter.java"

# interfaces
.implements Lcom/squareup/encryption/CryptoKeyAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/CardConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FallbackKeyAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/encryption/CryptoKeyAdapter<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field static final KEY_DATA:Ljava/lang/String; = "MIID0TCCArmgAwIBAgIQZonJJJMdmP2mtw+jZAexTzANBgkqhkiG9w0BAQ0FADA8MSYwJAYDVQQDEx1CbGV0Y2hsZXkgU2lnbmluZyBDZXJ0aWZpY2F0ZTESMBAGA1UECxMJYmxldGNobGV5MB4XDTE1MDIwNTAwMzYyN1oXDTE2MDQwNTAwMzYyN1owUjEdMBsGA1UEChMUQmxldGNobGV5IFB1YmxpYyBLZXkxMTAvBgNVBAMTKDkwYTdjNDQ3MDI2YmRkNTlkMmUwMzZiOTg5ODIxNDQyYzBiZGVlYjQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDjtIr/i7CkDxcP+GZKR6L+x0WSnNRlESrT0XvOpec9emQfI4rN7sINnRG1zkq1Nm0upWxPx+795kOjoq7u9pwX06lB80TAdFcHs6zHXqacqXItnM0QfsAKmhGXDtzGsg4wrGmYaLSZeAJ7BHLG/hvS1gQElnZjTCtdKBqwYBIqxVC2zPgMD96z9swliXt5zgHY48jJcLJjNLD3UPSIp76XjbbHEXsggCe+0sJGo4RX7xeJ0sP0/AZLpGXYhiwpL8ZynjebVS7mbliiDGDweN5daT1Vug3U+NB5/xCEwKx5hwCQonl3vx4gV9Twdj57+U+ldfK1bTexq3hEFu9k3LghAgMBAAGjgbgwgbUwHQYDVR0OBBYEFGGn0gWsWhW4cpNVGC7tVMof10e0MA4GA1UdDwEB/wQEAwID+DAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwEwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBRoBIiOKk7QO6Sef1K0ksQTpqYUaTA2BgNVHREBAf8ELDAqgig5MGE3YzQ0NzAyNmJkZDU5ZDJlMDM2Yjk4OTgyMTQ0MmMwYmRlZWI0MA0GCSqGSIb3DQEBDQUAA4IBAQCC5D3MVJwM3FwNuyYR2TQSLi3xe6LiJ8OhcIHX48xuCSJ5ixG5cHhYccd/Bfy7ZAosdrRLnvLQeE/Isuyup+hD0MYxj6wFWw7ZYtk77qCrg5qZRDA/a7hSbVXJ3XKiKsYrKkNkv9fz1DBz3FZl3te3hlZPFM4WgVsXRa8kWv0yKigIVDNRIZzmoYEjDbbA448dCR/yKZwsh7ccl0WE2smZfTG3DW6MQD2WBuy301wnUb/P7SB3wxbLCPcoW/eIMSVkVwXeBrAELyDUrxwLH2NKHDbPWUOARJtTKsjiL/IAVkYchaHKVib2ycD5C7udG+qDnXHs1LvrsLpwgAg8joav"

.field static final KEY_ID:Ljava/lang/String; = "OTBhN2M0NDcwMjZiZGQ1OWQyZTAzNmI5ODk4MjE0NDJjMGJkZWViNHxwYW5oYW5kbGVyfF9uY1FWN3pQdUhsbGlfVW9iNWlrTGJWQWdfbGlYYTlwTElGVEpyMEptNHc"


# direct methods
.method constructor <init>()V
    .locals 0

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic getKeyId(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 144
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/CardConverter$FallbackKeyAdapter;->getKeyId(Ljava/lang/Void;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getKeyId(Ljava/lang/Void;)Ljava/lang/String;
    .locals 0

    const-string p1, "OTBhN2M0NDcwMjZiZGQ1OWQyZTAzNmI5ODk4MjE0NDJjMGJkZWViNHxwYW5oYW5kbGVyfF9uY1FWN3pQdUhsbGlfVW9iNWlrTGJWQWdfbGlYYTlwTElGVEpyMEptNHc"

    return-object p1
.end method

.method public bridge synthetic getRawKey(Ljava/lang/Object;)[B
    .locals 0

    .line 144
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/CardConverter$FallbackKeyAdapter;->getRawKey(Ljava/lang/Void;)[B

    move-result-object p1

    return-object p1
.end method

.method public getRawKey(Ljava/lang/Void;)[B
    .locals 1

    const-string p1, "MIID0TCCArmgAwIBAgIQZonJJJMdmP2mtw+jZAexTzANBgkqhkiG9w0BAQ0FADA8MSYwJAYDVQQDEx1CbGV0Y2hsZXkgU2lnbmluZyBDZXJ0aWZpY2F0ZTESMBAGA1UECxMJYmxldGNobGV5MB4XDTE1MDIwNTAwMzYyN1oXDTE2MDQwNTAwMzYyN1owUjEdMBsGA1UEChMUQmxldGNobGV5IFB1YmxpYyBLZXkxMTAvBgNVBAMTKDkwYTdjNDQ3MDI2YmRkNTlkMmUwMzZiOTg5ODIxNDQyYzBiZGVlYjQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDjtIr/i7CkDxcP+GZKR6L+x0WSnNRlESrT0XvOpec9emQfI4rN7sINnRG1zkq1Nm0upWxPx+795kOjoq7u9pwX06lB80TAdFcHs6zHXqacqXItnM0QfsAKmhGXDtzGsg4wrGmYaLSZeAJ7BHLG/hvS1gQElnZjTCtdKBqwYBIqxVC2zPgMD96z9swliXt5zgHY48jJcLJjNLD3UPSIp76XjbbHEXsggCe+0sJGo4RX7xeJ0sP0/AZLpGXYhiwpL8ZynjebVS7mbliiDGDweN5daT1Vug3U+NB5/xCEwKx5hwCQonl3vx4gV9Twdj57+U+ldfK1bTexq3hEFu9k3LghAgMBAAGjgbgwgbUwHQYDVR0OBBYEFGGn0gWsWhW4cpNVGC7tVMof10e0MA4GA1UdDwEB/wQEAwID+DAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwEwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBRoBIiOKk7QO6Sef1K0ksQTpqYUaTA2BgNVHREBAf8ELDAqgig5MGE3YzQ0NzAyNmJkZDU5ZDJlMDM2Yjk4OTgyMTQ0MmMwYmRlZWI0MA0GCSqGSIb3DQEBDQUAA4IBAQCC5D3MVJwM3FwNuyYR2TQSLi3xe6LiJ8OhcIHX48xuCSJ5ixG5cHhYccd/Bfy7ZAosdrRLnvLQeE/Isuyup+hD0MYxj6wFWw7ZYtk77qCrg5qZRDA/a7hSbVXJ3XKiKsYrKkNkv9fz1DBz3FZl3te3hlZPFM4WgVsXRa8kWv0yKigIVDNRIZzmoYEjDbbA448dCR/yKZwsh7ccl0WE2smZfTG3DW6MQD2WBuy301wnUb/P7SB3wxbLCPcoW/eIMSVkVwXeBrAELyDUrxwLH2NKHDbPWUOARJtTKsjiL/IAVkYchaHKVib2ycD5C7udG+qDnXHs1LvrsLpwgAg8joav"

    const/4 v0, 0x0

    .line 184
    invoke-static {p1, v0}, Lcom/squareup/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic isExpired(Ljava/lang/Object;)Z
    .locals 0

    .line 144
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/CardConverter$FallbackKeyAdapter;->isExpired(Ljava/lang/Void;)Z

    move-result p1

    return p1
.end method

.method public isExpired(Ljava/lang/Void;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method
