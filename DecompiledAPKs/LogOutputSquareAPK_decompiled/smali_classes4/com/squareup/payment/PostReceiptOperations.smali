.class public Lcom/squareup/payment/PostReceiptOperations;
.super Ljava/lang/Object;
.source "PostReceiptOperations.java"


# static fields
.field public static final FINISHED_RECEIPT:Ljava/lang/String; = "Finished with receipt"


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final tutorialApi:Lcom/squareup/register/tutorial/TutorialApi;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/analytics/Analytics;Lcom/squareup/register/tutorial/TutorialApi;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/payment/PostReceiptOperations;->transaction:Lcom/squareup/payment/Transaction;

    .line 35
    iput-object p2, p0, Lcom/squareup/payment/PostReceiptOperations;->analytics:Lcom/squareup/analytics/Analytics;

    .line 36
    iput-object p3, p0, Lcom/squareup/payment/PostReceiptOperations;->tutorialApi:Lcom/squareup/register/tutorial/TutorialApi;

    .line 37
    iput-object p4, p0, Lcom/squareup/payment/PostReceiptOperations;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    .line 38
    iput-object p5, p0, Lcom/squareup/payment/PostReceiptOperations;->adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

    .line 39
    iput-object p6, p0, Lcom/squareup/payment/PostReceiptOperations;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method


# virtual methods
.method public resetTransactionBeforeReceiptScreen()V
    .locals 5

    .line 53
    iget-object v0, p0, Lcom/squareup/payment/PostReceiptOperations;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->isComplete()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 56
    iget-object v1, p0, Lcom/squareup/payment/PostReceiptOperations;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 57
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->isStoreAndForward()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->OFFLINE_CHARGE:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->CHARGE:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    .line 60
    :goto_0
    iget-object v2, p0, Lcom/squareup/payment/PostReceiptOperations;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/squareup/payment/Transaction;->closeCurrentTicketBeforeReset(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;)V

    .line 64
    :cond_1
    iget-object v1, p0, Lcom/squareup/payment/PostReceiptOperations;->tutorialApi:Lcom/squareup/register/tutorial/TutorialApi;

    invoke-virtual {v1}, Lcom/squareup/register/tutorial/TutorialApi;->onFinishedReceipt()V

    .line 66
    iget-object v1, p0, Lcom/squareup/payment/PostReceiptOperations;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v2, "Finished with receipt"

    invoke-interface {v1, v2}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 67
    iget-object v1, p0, Lcom/squareup/payment/PostReceiptOperations;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->fullNameForLogging()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logPaymentFinished(Ljava/lang/String;)V

    .line 70
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 71
    iget-object v1, p0, Lcom/squareup/payment/PostReceiptOperations;->adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

    iget-object v2, p0, Lcom/squareup/payment/PostReceiptOperations;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/UserSettings;->getMerchantToken()Ljava/lang/String;

    move-result-object v2

    .line 72
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 71
    invoke-interface {v1, v2, v3, v4}, Lcom/squareup/adanalytics/AdAnalytics;->recordTakePayment(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 73
    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/OrderSnapshot;->getItemizationsForRequest()Ljava/util/List;

    move-result-object v0

    .line 74
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/payment/Itemization;

    iget-object v0, v0, Lcom/squareup/server/payment/Itemization;->item_id:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 75
    iget-object v0, p0, Lcom/squareup/payment/PostReceiptOperations;->adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

    iget-object v1, p0, Lcom/squareup/payment/PostReceiptOperations;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    .line 76
    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getMerchantToken()Ljava/lang/String;

    move-result-object v1

    .line 75
    invoke-interface {v0, v1}, Lcom/squareup/adanalytics/AdAnalytics;->recordTakeItemizedPayment(Ljava/lang/String;)V

    .line 79
    :cond_2
    iget-object v0, p0, Lcom/squareup/payment/PostReceiptOperations;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->reset()V

    goto :goto_1

    .line 82
    :cond_3
    iget-object v0, p0, Lcom/squareup/payment/PostReceiptOperations;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setCard(Lcom/squareup/Card;)V

    :goto_1
    return-void
.end method
