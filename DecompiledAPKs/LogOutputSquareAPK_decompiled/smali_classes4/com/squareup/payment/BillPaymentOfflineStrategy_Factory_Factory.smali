.class public final Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;
.super Ljava/lang/Object;
.source "BillPaymentOfflineStrategy_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final backgroundCaptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BackgroundCaptor;",
            ">;"
        }
    .end annotation
.end field

.field private final locationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentAccuracyLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final skipReceiptScreenSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAndForwardAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAndForwardPaymentServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/StoreAndForwardPaymentService;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLazyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/StoreAndForwardPaymentService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BackgroundCaptor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ">;)V"
        }
    .end annotation

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p2, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p3, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->transactionLazyProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p4, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->locationProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p5, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->storeAndForwardAnalyticsProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p6, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->paymentAccuracyLoggerProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p7, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->storeAndForwardPaymentServiceProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p8, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->backgroundCaptorProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p9, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->skipReceiptScreenSettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/StoreAndForwardPaymentService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BackgroundCaptor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ">;)",
            "Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;"
        }
    .end annotation

    .line 74
    new-instance v10, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Ldagger/Lazy;Ljavax/inject/Provider;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/payment/PaymentAccuracyLogger;Lcom/squareup/payment/offline/StoreAndForwardPaymentService;Ljava/lang/Object;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;)Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/util/Res;",
            "Ldagger/Lazy<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            "Lcom/squareup/payment/offline/StoreAndForwardPaymentService;",
            "Ljava/lang/Object;",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ")",
            "Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;"
        }
    .end annotation

    .line 83
    new-instance v10, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;

    move-object/from16 v8, p7

    check-cast v8, Lcom/squareup/payment/BackgroundCaptor;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Ldagger/Lazy;Ljavax/inject/Provider;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/payment/PaymentAccuracyLogger;Lcom/squareup/payment/offline/StoreAndForwardPaymentService;Lcom/squareup/payment/BackgroundCaptor;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;
    .locals 10

    .line 63
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->transactionLazyProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->locationProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->storeAndForwardAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/analytics/StoreAndForwardAnalytics;

    iget-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->paymentAccuracyLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/payment/PaymentAccuracyLogger;

    iget-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->storeAndForwardPaymentServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

    iget-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->backgroundCaptorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v8

    iget-object v0, p0, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->skipReceiptScreenSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    invoke-static/range {v1 .. v9}, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Ldagger/Lazy;Ljavax/inject/Provider;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/payment/PaymentAccuracyLogger;Lcom/squareup/payment/offline/StoreAndForwardPaymentService;Ljava/lang/Object;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;)Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/payment/BillPaymentOfflineStrategy_Factory_Factory;->get()Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;

    move-result-object v0

    return-object v0
.end method
