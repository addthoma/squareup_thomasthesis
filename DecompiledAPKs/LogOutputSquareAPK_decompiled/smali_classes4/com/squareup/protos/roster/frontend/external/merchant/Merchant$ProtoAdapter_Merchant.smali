.class final Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$ProtoAdapter_Merchant;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Merchant.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Merchant"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 294
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 321
    new-instance v0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;-><init>()V

    .line 322
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 323
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 353
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 346
    :pswitch_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->status(Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;)Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 348
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 338
    :pswitch_1
    :try_start_1
    sget-object v4, Lcom/squareup/protos/connect/v2/common/Currency;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/connect/v2/common/Currency;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->currency(Lcom/squareup/protos/connect/v2/common/Currency;)Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 340
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 335
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->language_code(Ljava/lang/String;)Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;

    goto :goto_0

    .line 329
    :pswitch_3
    :try_start_2
    sget-object v4, Lcom/squareup/protos/connect/v2/resources/Country;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/connect/v2/resources/Country;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->country(Lcom/squareup/protos/connect/v2/resources/Country;)Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;
    :try_end_2
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v4

    .line 331
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 326
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->business_name(Ljava/lang/String;)Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;

    goto :goto_0

    .line 325
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->id(Ljava/lang/String;)Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;

    goto :goto_0

    .line 357
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 358
    invoke-virtual {v0}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->build()Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 292
    invoke-virtual {p0, p1}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$ProtoAdapter_Merchant;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 310
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 311
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->business_name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 312
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 313
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->language_code:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 314
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 315
    sget-object v0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->status:Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 316
    invoke-virtual {p2}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 292
    check-cast p2, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$ProtoAdapter_Merchant;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;)I
    .locals 4

    .line 299
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->business_name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 300
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    const/4 v3, 0x3

    .line 301
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->language_code:Ljava/lang/String;

    const/4 v3, 0x4

    .line 302
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    const/4 v3, 0x5

    .line 303
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->status:Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;

    const/4 v3, 0x6

    .line 304
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 305
    invoke-virtual {p1}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 292
    check-cast p1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$ProtoAdapter_Merchant;->encodedSize(Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;)Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;
    .locals 1

    .line 363
    invoke-virtual {p1}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->newBuilder()Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 364
    iput-object v0, p1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->business_name:Ljava/lang/String;

    .line 365
    invoke-virtual {p1}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 366
    invoke-virtual {p1}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->build()Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 292
    check-cast p1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$ProtoAdapter_Merchant;->redact(Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;)Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;

    move-result-object p1

    return-object p1
.end method
