.class public final Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListMerchantsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;",
        "Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cursor:Ljava/lang/Integer;

.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field

.field public merchant:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 125
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 126
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;->errors:Ljava/util/List;

    .line 127
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;->merchant:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;
    .locals 5

    .line 158
    new-instance v0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;

    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;->errors:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;->merchant:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;->cursor:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;-><init>(Ljava/util/List;Ljava/util/List;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;->build()Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse;

    move-result-object v0

    return-object v0
.end method

.method public cursor(Ljava/lang/Integer;)Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;->cursor:Ljava/lang/Integer;

    return-object p0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;)",
            "Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;"
        }
    .end annotation

    .line 134
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;->errors:Ljava/util/List;

    return-object p0
.end method

.method public merchant(Ljava/util/List;)Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;",
            ">;)",
            "Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;"
        }
    .end annotation

    .line 143
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/ListMerchantsResponse$Builder;->merchant:Ljava/util/List;

    return-object p0
.end method
