.class public final Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SubscribeRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;",
        "Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact_token:Ljava/lang/String;

.field public payment_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 226
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;
    .locals 4

    .line 241
    new-instance v0, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata$Builder;->contact_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata$Builder;->payment_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 221
    invoke-virtual {p0}, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata$Builder;->build()Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata;

    move-result-object v0

    return-object v0
.end method

.method public contact_token(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata$Builder;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method public payment_token(Ljava/lang/String;)Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata$Builder;
    .locals 0

    .line 235
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/SubscribeRequest$Metadata$Builder;->payment_token:Ljava/lang/String;

    return-object p0
.end method
