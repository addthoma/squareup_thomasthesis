.class public final enum Lcom/squareup/protos/sub2/common/SubscriptionStatus;
.super Ljava/lang/Enum;
.source "SubscriptionStatus.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/sub2/common/SubscriptionStatus$ProtoAdapter_SubscriptionStatus;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/sub2/common/SubscriptionStatus;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/sub2/common/SubscriptionStatus;

.field public static final enum ACTIVE:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

.field public static final enum ACTIVE_PENDING_CANCELLATION:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/sub2/common/SubscriptionStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CANCELED:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

.field public static final enum DELINQUENT:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

.field public static final enum FREE_TRIAL:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

.field public static final enum FREE_TRIAL_NO_OBLIGATION:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

.field public static final enum FREE_TRIAL_PENDING_CANCELLATION:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

.field public static final enum INVALID:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

.field public static final enum IN_GRACE:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

.field public static final enum NO_OBLIGATION_FREE_TRIAL_EXPIRED:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

.field public static final enum PENDING:Lcom/squareup/protos/sub2/common/SubscriptionStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 19
    new-instance v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    const/4 v1, 0x0

    const-string v2, "INVALID"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/sub2/common/SubscriptionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->INVALID:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    .line 25
    new-instance v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    const/4 v2, 0x1

    const-string v3, "PENDING"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/sub2/common/SubscriptionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->PENDING:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    .line 30
    new-instance v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    const/4 v3, 0x2

    const-string v4, "FREE_TRIAL"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/sub2/common/SubscriptionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->FREE_TRIAL:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    .line 36
    new-instance v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    const/4 v4, 0x3

    const-string v5, "FREE_TRIAL_PENDING_CANCELLATION"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/sub2/common/SubscriptionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->FREE_TRIAL_PENDING_CANCELLATION:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    .line 47
    new-instance v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    const/16 v5, 0x9

    const/4 v6, 0x4

    const-string v7, "FREE_TRIAL_NO_OBLIGATION"

    invoke-direct {v0, v7, v6, v5}, Lcom/squareup/protos/sub2/common/SubscriptionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->FREE_TRIAL_NO_OBLIGATION:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    .line 57
    new-instance v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    const/16 v7, 0xa

    const/4 v8, 0x5

    const-string v9, "NO_OBLIGATION_FREE_TRIAL_EXPIRED"

    invoke-direct {v0, v9, v8, v7}, Lcom/squareup/protos/sub2/common/SubscriptionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->NO_OBLIGATION_FREE_TRIAL_EXPIRED:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    .line 62
    new-instance v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    const/4 v9, 0x6

    const-string v10, "ACTIVE"

    invoke-direct {v0, v10, v9, v6}, Lcom/squareup/protos/sub2/common/SubscriptionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->ACTIVE:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    .line 67
    new-instance v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    const/4 v10, 0x7

    const-string v11, "ACTIVE_PENDING_CANCELLATION"

    invoke-direct {v0, v11, v10, v8}, Lcom/squareup/protos/sub2/common/SubscriptionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->ACTIVE_PENDING_CANCELLATION:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    .line 72
    new-instance v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    const/16 v11, 0x8

    const-string v12, "CANCELED"

    invoke-direct {v0, v12, v11, v9}, Lcom/squareup/protos/sub2/common/SubscriptionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->CANCELED:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    .line 77
    new-instance v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    const-string v12, "IN_GRACE"

    invoke-direct {v0, v12, v5, v11}, Lcom/squareup/protos/sub2/common/SubscriptionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->IN_GRACE:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    .line 82
    new-instance v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    const-string v12, "DELINQUENT"

    invoke-direct {v0, v12, v7, v10}, Lcom/squareup/protos/sub2/common/SubscriptionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->DELINQUENT:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    .line 15
    sget-object v12, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->INVALID:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    aput-object v12, v0, v1

    sget-object v1, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->PENDING:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->FREE_TRIAL:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->FREE_TRIAL_PENDING_CANCELLATION:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->FREE_TRIAL_NO_OBLIGATION:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->NO_OBLIGATION_FREE_TRIAL_EXPIRED:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->ACTIVE:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->ACTIVE_PENDING_CANCELLATION:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->CANCELED:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->IN_GRACE:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->DELINQUENT:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->$VALUES:[Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    .line 84
    new-instance v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus$ProtoAdapter_SubscriptionStatus;

    invoke-direct {v0}, Lcom/squareup/protos/sub2/common/SubscriptionStatus$ProtoAdapter_SubscriptionStatus;-><init>()V

    sput-object v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 89
    iput p3, p0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/sub2/common/SubscriptionStatus;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 102
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->NO_OBLIGATION_FREE_TRIAL_EXPIRED:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    return-object p0

    .line 101
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->FREE_TRIAL_NO_OBLIGATION:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    return-object p0

    .line 106
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->IN_GRACE:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    return-object p0

    .line 107
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->DELINQUENT:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    return-object p0

    .line 105
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->CANCELED:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    return-object p0

    .line 104
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->ACTIVE_PENDING_CANCELLATION:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    return-object p0

    .line 103
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->ACTIVE:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    return-object p0

    .line 100
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->FREE_TRIAL_PENDING_CANCELLATION:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    return-object p0

    .line 99
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->FREE_TRIAL:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    return-object p0

    .line 98
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->PENDING:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    return-object p0

    .line 97
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->INVALID:Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/sub2/common/SubscriptionStatus;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/sub2/common/SubscriptionStatus;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->$VALUES:[Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    invoke-virtual {v0}, [Lcom/squareup/protos/sub2/common/SubscriptionStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/sub2/common/SubscriptionStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 114
    iget v0, p0, Lcom/squareup/protos/sub2/common/SubscriptionStatus;->value:I

    return v0
.end method
