.class public final Lcom/squareup/protos/eventstream/v1/Sim;
.super Lcom/squareup/wire/Message;
.source "Sim.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/eventstream/v1/Sim$ProtoAdapter_Sim;,
        Lcom/squareup/protos/eventstream/v1/Sim$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/eventstream/v1/Sim;",
        "Lcom/squareup/protos/eventstream/v1/Sim$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/eventstream/v1/Sim;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COUNTRY_ISO:Ljava/lang/String; = ""

.field public static final DEFAULT_MCC:Ljava/lang/Integer;

.field public static final DEFAULT_MNC:Ljava/lang/Integer;

.field public static final DEFAULT_OPERATOR_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_SERIAL_NUMBER:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final country_iso:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final mcc:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field

.field public final mnc:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field

.field public final operator_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final serial_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/eventstream/v1/Sim$ProtoAdapter_Sim;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/Sim$ProtoAdapter_Sim;-><init>()V

    sput-object v0, Lcom/squareup/protos/eventstream/v1/Sim;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/eventstream/v1/Sim;->DEFAULT_MCC:Ljava/lang/Integer;

    .line 29
    sput-object v0, Lcom/squareup/protos/eventstream/v1/Sim;->DEFAULT_MNC:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 67
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/eventstream/v1/Sim;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 72
    sget-object v0, Lcom/squareup/protos/eventstream/v1/Sim;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 73
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->country_iso:Ljava/lang/String;

    .line 74
    iput-object p2, p0, Lcom/squareup/protos/eventstream/v1/Sim;->mcc:Ljava/lang/Integer;

    .line 75
    iput-object p3, p0, Lcom/squareup/protos/eventstream/v1/Sim;->mnc:Ljava/lang/Integer;

    .line 76
    iput-object p4, p0, Lcom/squareup/protos/eventstream/v1/Sim;->operator_name:Ljava/lang/String;

    .line 77
    iput-object p5, p0, Lcom/squareup/protos/eventstream/v1/Sim;->serial_number:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 95
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/eventstream/v1/Sim;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 96
    :cond_1
    check-cast p1, Lcom/squareup/protos/eventstream/v1/Sim;

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Sim;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/eventstream/v1/Sim;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->country_iso:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Sim;->country_iso:Ljava/lang/String;

    .line 98
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->mcc:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Sim;->mcc:Ljava/lang/Integer;

    .line 99
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->mnc:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Sim;->mnc:Ljava/lang/Integer;

    .line 100
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->operator_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Sim;->operator_name:Ljava/lang/String;

    .line 101
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->serial_number:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/eventstream/v1/Sim;->serial_number:Ljava/lang/String;

    .line 102
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 107
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Sim;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->country_iso:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->mcc:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->mnc:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->operator_name:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->serial_number:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 115
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/eventstream/v1/Sim$Builder;
    .locals 2

    .line 82
    new-instance v0, Lcom/squareup/protos/eventstream/v1/Sim$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/Sim$Builder;-><init>()V

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->country_iso:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->country_iso:Ljava/lang/String;

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->mcc:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->mcc:Ljava/lang/Integer;

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->mnc:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->mnc:Ljava/lang/Integer;

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->operator_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->operator_name:Ljava/lang/String;

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->serial_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->serial_number:Ljava/lang/String;

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Sim;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/eventstream/v1/Sim$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Sim;->newBuilder()Lcom/squareup/protos/eventstream/v1/Sim$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->country_iso:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", country_iso="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->country_iso:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->mcc:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", mcc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->mcc:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->mnc:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", mnc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->mnc:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->operator_name:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", operator_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->operator_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->serial_number:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", serial_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Sim;->serial_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Sim{"

    .line 128
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
