.class public final Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListTransactionsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount:Lcom/squareup/protos/common/Money;

.field public bill_id_pair:Lcom/squareup/protos/client/IdPair;

.field public date:Lcom/squareup/protos/common/time/DateTime;

.field public description:Ljava/lang/String;

.field public has_error:Ljava/lang/Boolean;

.field public has_related_transactions:Ljava/lang/Boolean;

.field public is_awaiting_tip:Ljava/lang/Boolean;

.field public is_fully_voided:Ljava/lang/Boolean;

.field public is_tip_expiring:Ljava/lang/Boolean;

.field public search_match:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;",
            ">;"
        }
    .end annotation
.end field

.field public tender_information:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;",
            ">;"
        }
    .end annotation
.end field

.field public type:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 404
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 405
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->tender_information:Ljava/util/List;

    .line 406
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->search_match:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;
    .locals 0

    .line 437
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;
    .locals 0

    .line 413
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;
    .locals 15

    .line 514
    new-instance v14, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->type:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    iget-object v3, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->date:Lcom/squareup/protos/common/time/DateTime;

    iget-object v4, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->amount:Lcom/squareup/protos/common/Money;

    iget-object v5, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->description:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->tender_information:Ljava/util/List;

    iget-object v7, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->has_related_transactions:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->has_error:Ljava/lang/Boolean;

    iget-object v9, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->is_fully_voided:Ljava/lang/Boolean;

    iget-object v10, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->is_awaiting_tip:Ljava/lang/Boolean;

    iget-object v11, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->is_tip_expiring:Ljava/lang/Boolean;

    iget-object v12, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->search_match:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v13

    move-object v0, v14

    invoke-direct/range {v0 .. v13}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V

    return-object v14
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 379
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;

    move-result-object v0

    return-object v0
.end method

.method public date(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;
    .locals 0

    .line 429
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->date:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;
    .locals 0

    .line 446
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public has_error(Ljava/lang/Boolean;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;
    .locals 0

    .line 474
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->has_error:Ljava/lang/Boolean;

    return-object p0
.end method

.method public has_related_transactions(Ljava/lang/Boolean;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;
    .locals 0

    .line 465
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->has_related_transactions:Ljava/lang/Boolean;

    return-object p0
.end method

.method public is_awaiting_tip(Ljava/lang/Boolean;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;
    .locals 0

    .line 490
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->is_awaiting_tip:Ljava/lang/Boolean;

    return-object p0
.end method

.method public is_fully_voided(Ljava/lang/Boolean;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;
    .locals 0

    .line 482
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->is_fully_voided:Ljava/lang/Boolean;

    return-object p0
.end method

.method public is_tip_expiring(Ljava/lang/Boolean;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;
    .locals 0

    .line 499
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->is_tip_expiring:Ljava/lang/Boolean;

    return-object p0
.end method

.method public search_match(Ljava/util/List;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;",
            ">;)",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;"
        }
    .end annotation

    .line 507
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 508
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->search_match:Ljava/util/List;

    return-object p0
.end method

.method public tender_information(Ljava/util/List;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;",
            ">;)",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;"
        }
    .end annotation

    .line 454
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 455
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->tender_information:Ljava/util/List;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;
    .locals 0

    .line 421
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->type:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    return-object p0
.end method
