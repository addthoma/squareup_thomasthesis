.class public final Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListDeviceCodesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;",
        "Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cursor:Ljava/lang/String;

.field public location_id:Ljava/lang/String;

.field public product_type:Lcom/squareup/protos/devicesettings/ProductType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 131
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;
    .locals 5

    .line 171
    new-instance v0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;->cursor:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;->location_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/devicesettings/ProductType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;->build()Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;

    move-result-object v0

    return-object v0
.end method

.method public cursor(Ljava/lang/String;)Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;->cursor:Ljava/lang/String;

    return-object p0
.end method

.method public location_id(Ljava/lang/String;)Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;->location_id:Ljava/lang/String;

    return-object p0
.end method

.method public product_type(Lcom/squareup/protos/devicesettings/ProductType;)Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    return-object p0
.end method
