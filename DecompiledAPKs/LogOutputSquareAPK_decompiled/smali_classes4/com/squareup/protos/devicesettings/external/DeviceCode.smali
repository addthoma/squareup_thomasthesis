.class public final Lcom/squareup/protos/devicesettings/external/DeviceCode;
.super Lcom/squareup/wire/Message;
.source "DeviceCode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/devicesettings/external/DeviceCode$ProtoAdapter_DeviceCode;,
        Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;,
        Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/devicesettings/external/DeviceCode;",
        "Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/devicesettings/external/DeviceCode;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_CREATED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_DEVICE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_LOCATION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PAIRED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_PAIR_BY:Ljava/lang/String; = ""

.field public static final DEFAULT_PRODUCT_TYPE:Lcom/squareup/protos/devicesettings/ProductType;

.field public static final DEFAULT_STATUS:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

.field public static final DEFAULT_STATUS_CHANGED_AT:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final created_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final device_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final location_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final pair_by:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final paired_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final product_type:Lcom/squareup/protos/devicesettings/ProductType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.devicesettings.ProductType#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.devicesettings.external.DeviceCode$Status#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final status_changed_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$ProtoAdapter_DeviceCode;

    invoke-direct {v0}, Lcom/squareup/protos/devicesettings/external/DeviceCode$ProtoAdapter_DeviceCode;-><init>()V

    sput-object v0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 38
    sget-object v0, Lcom/squareup/protos/devicesettings/ProductType;->UNKNOWN:Lcom/squareup/protos/devicesettings/ProductType;

    sput-object v0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->DEFAULT_PRODUCT_TYPE:Lcom/squareup/protos/devicesettings/ProductType;

    .line 42
    sget-object v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;->UNPAIRED:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    sput-object v0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->DEFAULT_STATUS:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/devicesettings/ProductType;Ljava/lang/String;Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13

    .line 177
    sget-object v12, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/devicesettings/external/DeviceCode;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/devicesettings/ProductType;Ljava/lang/String;Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/devicesettings/ProductType;Ljava/lang/String;Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 183
    sget-object v0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p12}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->id:Ljava/lang/String;

    .line 185
    iput-object p2, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->name:Ljava/lang/String;

    .line 186
    iput-object p3, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->code:Ljava/lang/String;

    .line 187
    iput-object p4, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->device_id:Ljava/lang/String;

    .line 188
    iput-object p5, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    .line 189
    iput-object p6, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->location_id:Ljava/lang/String;

    .line 190
    iput-object p7, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->status:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    .line 191
    iput-object p8, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->pair_by:Ljava/lang/String;

    .line 192
    iput-object p9, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->created_at:Ljava/lang/String;

    .line 193
    iput-object p10, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->status_changed_at:Ljava/lang/String;

    .line 194
    iput-object p11, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->paired_at:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 218
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/devicesettings/external/DeviceCode;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 219
    :cond_1
    check-cast p1, Lcom/squareup/protos/devicesettings/external/DeviceCode;

    .line 220
    invoke-virtual {p0}, Lcom/squareup/protos/devicesettings/external/DeviceCode;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/devicesettings/external/DeviceCode;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/devicesettings/external/DeviceCode;->id:Ljava/lang/String;

    .line 221
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/devicesettings/external/DeviceCode;->name:Ljava/lang/String;

    .line 222
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/devicesettings/external/DeviceCode;->code:Ljava/lang/String;

    .line 223
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->device_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/devicesettings/external/DeviceCode;->device_id:Ljava/lang/String;

    .line 224
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    iget-object v3, p1, Lcom/squareup/protos/devicesettings/external/DeviceCode;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    .line 225
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->location_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/devicesettings/external/DeviceCode;->location_id:Ljava/lang/String;

    .line 226
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->status:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    iget-object v3, p1, Lcom/squareup/protos/devicesettings/external/DeviceCode;->status:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    .line 227
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->pair_by:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/devicesettings/external/DeviceCode;->pair_by:Ljava/lang/String;

    .line 228
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->created_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/devicesettings/external/DeviceCode;->created_at:Ljava/lang/String;

    .line 229
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->status_changed_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/devicesettings/external/DeviceCode;->status_changed_at:Ljava/lang/String;

    .line 230
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->paired_at:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/devicesettings/external/DeviceCode;->paired_at:Ljava/lang/String;

    .line 231
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 236
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_b

    .line 238
    invoke-virtual {p0}, Lcom/squareup/protos/devicesettings/external/DeviceCode;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 239
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 240
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 241
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->code:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 242
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->device_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 243
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/devicesettings/ProductType;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 245
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->status:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 246
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->pair_by:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 247
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->created_at:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 248
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->status_changed_at:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 249
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->paired_at:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_a
    add-int/2addr v0, v2

    .line 250
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_b
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;
    .locals 2

    .line 199
    new-instance v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;-><init>()V

    .line 200
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->id:Ljava/lang/String;

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->name:Ljava/lang/String;

    .line 202
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->code:Ljava/lang/String;

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->device_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->device_id:Ljava/lang/String;

    .line 204
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    iput-object v1, v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->location_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->location_id:Ljava/lang/String;

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->status:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    iput-object v1, v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->status:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->pair_by:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->pair_by:Ljava/lang/String;

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->created_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->created_at:Ljava/lang/String;

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->status_changed_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->status_changed_at:Ljava/lang/String;

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->paired_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->paired_at:Ljava/lang/String;

    .line 211
    invoke-virtual {p0}, Lcom/squareup/protos/devicesettings/external/DeviceCode;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/devicesettings/external/DeviceCode;->newBuilder()Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 258
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->code:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", code=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->device_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", device_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->device_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    if-eqz v1, :cond_4

    const-string v1, ", product_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 263
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", location_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->location_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->status:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    if-eqz v1, :cond_6

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->status:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 265
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->pair_by:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", pair_by="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->pair_by:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->created_at:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->created_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->status_changed_at:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", status_changed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->status_changed_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->paired_at:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", paired_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode;->paired_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DeviceCode{"

    .line 269
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
