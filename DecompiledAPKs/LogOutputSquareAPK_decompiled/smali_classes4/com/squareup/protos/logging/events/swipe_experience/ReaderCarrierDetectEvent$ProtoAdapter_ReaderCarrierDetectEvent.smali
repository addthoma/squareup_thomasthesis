.class final Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$ProtoAdapter_ReaderCarrierDetectEvent;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderCarrierDetectEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ReaderCarrierDetectEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 242
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 263
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;-><init>()V

    .line 264
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 265
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 278
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 276
    :cond_0
    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->signal_found(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;)Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    goto :goto_0

    .line 275
    :cond_1
    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->carrier_detect_info(Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;)Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    goto :goto_0

    .line 269
    :cond_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->event(Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;)Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 271
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 282
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 283
    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 240
    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$ProtoAdapter_ReaderCarrierDetectEvent;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 255
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 256
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 257
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 258
    invoke-virtual {p2}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 240
    check-cast p2, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$ProtoAdapter_ReaderCarrierDetectEvent;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;)I
    .locals 4

    .line 247
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->event:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    const/4 v3, 0x2

    .line 248
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    const/4 v3, 0x3

    .line 249
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 250
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 240
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$ProtoAdapter_ReaderCarrierDetectEvent;->encodedSize(Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;)Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;
    .locals 2

    .line 288
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    move-result-object p1

    .line 289
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    iput-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    .line 290
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    iput-object v0, p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->signal_found:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;

    .line 291
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 292
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 240
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$ProtoAdapter_ReaderCarrierDetectEvent;->redact(Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;)Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent;

    move-result-object p1

    return-object p1
.end method
