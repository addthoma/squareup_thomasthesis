.class public final Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;
.super Lcom/squareup/wire/Message;
.source "CarrierDetectInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$ProtoAdapter_CarrierDetectInfo;,
        Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;",
        "Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALLOW_RESTART:Ljava/lang/Boolean;

.field public static final DEFAULT_END_AVG:Ljava/lang/Integer;

.field public static final DEFAULT_END_THRESHOLD:Ljava/lang/Integer;

.field public static final DEFAULT_IN_PACKET_RUNTIME_US:Ljava/lang/Integer;

.field public static final DEFAULT_IS_EARLY_PACKET:Ljava/lang/Boolean;

.field public static final DEFAULT_NUM_SAMPLES:Ljava/lang/Integer;

.field public static final DEFAULT_START_AVG:Ljava/lang/Integer;

.field public static final DEFAULT_START_SAMPLE:Ljava/lang/Long;

.field public static final DEFAULT_START_THRESHOLD:Ljava/lang/Integer;

.field public static final DEFAULT_TOTAL_RUNTIME_IN_US:Ljava/lang/Long;

.field public static final DEFAULT_TOTAL_TIME_IN_US:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final allow_restart:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final early_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.ClassifyInfo#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final end_avg:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x9
    .end annotation
.end field

.field public final end_threshold:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x8
    .end annotation
.end field

.field public final in_packet_runtime_us:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xd
    .end annotation
.end field

.field public final is_early_packet:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final late_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.ClassifyInfo#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final num_samples:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field

.field public final start_avg:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x7
    .end annotation
.end field

.field public final start_sample:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x1
    .end annotation
.end field

.field public final start_threshold:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x6
    .end annotation
.end field

.field public final total_runtime_in_us:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x4
    .end annotation
.end field

.field public final total_time_in_us:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 26
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$ProtoAdapter_CarrierDetectInfo;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$ProtoAdapter_CarrierDetectInfo;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 30
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->DEFAULT_START_SAMPLE:Ljava/lang/Long;

    const/4 v1, 0x0

    .line 32
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->DEFAULT_NUM_SAMPLES:Ljava/lang/Integer;

    .line 34
    sput-object v2, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->DEFAULT_IS_EARLY_PACKET:Ljava/lang/Boolean;

    .line 36
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->DEFAULT_TOTAL_RUNTIME_IN_US:Ljava/lang/Long;

    .line 38
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->DEFAULT_TOTAL_TIME_IN_US:Ljava/lang/Long;

    .line 40
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->DEFAULT_START_THRESHOLD:Ljava/lang/Integer;

    .line 42
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->DEFAULT_START_AVG:Ljava/lang/Integer;

    .line 44
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->DEFAULT_END_THRESHOLD:Ljava/lang/Integer;

    .line 46
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->DEFAULT_END_AVG:Ljava/lang/Integer;

    .line 48
    sput-object v2, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->DEFAULT_ALLOW_RESTART:Ljava/lang/Boolean;

    .line 50
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->DEFAULT_IN_PACKET_RUNTIME_US:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;Ljava/lang/Integer;)V
    .locals 15

    .line 191
    sget-object v14, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 199
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p14}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 200
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_sample:Ljava/lang/Long;

    .line 201
    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->num_samples:Ljava/lang/Integer;

    .line 202
    iput-object p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->is_early_packet:Ljava/lang/Boolean;

    .line 203
    iput-object p4, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->total_runtime_in_us:Ljava/lang/Long;

    .line 204
    iput-object p5, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->total_time_in_us:Ljava/lang/Long;

    .line 205
    iput-object p6, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_threshold:Ljava/lang/Integer;

    .line 206
    iput-object p7, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_avg:Ljava/lang/Integer;

    .line 207
    iput-object p8, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->end_threshold:Ljava/lang/Integer;

    .line 208
    iput-object p9, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->end_avg:Ljava/lang/Integer;

    .line 209
    iput-object p10, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->allow_restart:Ljava/lang/Boolean;

    .line 210
    iput-object p11, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->early_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    .line 211
    iput-object p12, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->late_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    .line 212
    iput-object p13, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->in_packet_runtime_us:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 238
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 239
    :cond_1
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    .line 240
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_sample:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_sample:Ljava/lang/Long;

    .line 241
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->num_samples:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->num_samples:Ljava/lang/Integer;

    .line 242
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->is_early_packet:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->is_early_packet:Ljava/lang/Boolean;

    .line 243
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->total_runtime_in_us:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->total_runtime_in_us:Ljava/lang/Long;

    .line 244
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->total_time_in_us:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->total_time_in_us:Ljava/lang/Long;

    .line 245
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_threshold:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_threshold:Ljava/lang/Integer;

    .line 246
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_avg:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_avg:Ljava/lang/Integer;

    .line 247
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->end_threshold:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->end_threshold:Ljava/lang/Integer;

    .line 248
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->end_avg:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->end_avg:Ljava/lang/Integer;

    .line 249
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->allow_restart:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->allow_restart:Ljava/lang/Boolean;

    .line 250
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->early_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->early_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    .line 251
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->late_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->late_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    .line 252
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->in_packet_runtime_us:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->in_packet_runtime_us:Ljava/lang/Integer;

    .line 253
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 258
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_d

    .line 260
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 261
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_sample:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 262
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->num_samples:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 263
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->is_early_packet:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 264
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->total_runtime_in_us:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 265
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->total_time_in_us:Ljava/lang/Long;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 266
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_threshold:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 267
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_avg:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 268
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->end_threshold:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 269
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->end_avg:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 270
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->allow_restart:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 271
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->early_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 272
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->late_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 273
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->in_packet_runtime_us:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_c
    add-int/2addr v0, v2

    .line 274
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_d
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;
    .locals 2

    .line 217
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;-><init>()V

    .line 218
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_sample:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->start_sample:Ljava/lang/Long;

    .line 219
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->num_samples:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->num_samples:Ljava/lang/Integer;

    .line 220
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->is_early_packet:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->is_early_packet:Ljava/lang/Boolean;

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->total_runtime_in_us:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->total_runtime_in_us:Ljava/lang/Long;

    .line 222
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->total_time_in_us:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->total_time_in_us:Ljava/lang/Long;

    .line 223
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_threshold:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->start_threshold:Ljava/lang/Integer;

    .line 224
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_avg:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->start_avg:Ljava/lang/Integer;

    .line 225
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->end_threshold:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->end_threshold:Ljava/lang/Integer;

    .line 226
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->end_avg:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->end_avg:Ljava/lang/Integer;

    .line 227
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->allow_restart:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->allow_restart:Ljava/lang/Boolean;

    .line 228
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->early_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->early_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    .line 229
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->late_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->late_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    .line 230
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->in_packet_runtime_us:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->in_packet_runtime_us:Ljava/lang/Integer;

    .line 231
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 281
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 282
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_sample:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", start_sample="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_sample:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 283
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->num_samples:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", num_samples="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->num_samples:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 284
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->is_early_packet:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", is_early_packet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->is_early_packet:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 285
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->total_runtime_in_us:Ljava/lang/Long;

    if-eqz v1, :cond_3

    const-string v1, ", total_runtime_in_us="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->total_runtime_in_us:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 286
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->total_time_in_us:Ljava/lang/Long;

    if-eqz v1, :cond_4

    const-string v1, ", total_time_in_us="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->total_time_in_us:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 287
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_threshold:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    const-string v1, ", start_threshold="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_threshold:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 288
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_avg:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    const-string v1, ", start_avg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->start_avg:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 289
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->end_threshold:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    const-string v1, ", end_threshold="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->end_threshold:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 290
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->end_avg:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    const-string v1, ", end_avg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->end_avg:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 291
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->allow_restart:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", allow_restart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->allow_restart:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 292
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->early_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    if-eqz v1, :cond_a

    const-string v1, ", early_classify_stats="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->early_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 293
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->late_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    if-eqz v1, :cond_b

    const-string v1, ", late_classify_stats="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->late_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 294
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->in_packet_runtime_us:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    const-string v1, ", in_packet_runtime_us="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->in_packet_runtime_us:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_c
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CarrierDetectInfo{"

    .line 295
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
