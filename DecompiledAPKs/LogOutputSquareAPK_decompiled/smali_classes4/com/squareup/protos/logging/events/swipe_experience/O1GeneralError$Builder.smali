.class public final Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "O1GeneralError.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public end_period:Ljava/lang/Integer;

.field public error_code:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

.field public resets:Ljava/lang/Integer;

.field public start_period:Ljava/lang/Integer;

.field public status:Ljava/lang/Integer;

.field public wakeups:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 202
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;
    .locals 9

    .line 277
    new-instance v8, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->error_code:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    iget-object v2, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->resets:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->wakeups:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->status:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->start_period:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->end_period:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;-><init>(Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 189
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    move-result-object v0

    return-object v0
.end method

.method public end_period(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;
    .locals 0

    .line 271
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->end_period:Ljava/lang/Integer;

    return-object p0
.end method

.method public error_code(Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->error_code:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    return-object p0
.end method

.method public resets(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;
    .locals 0

    .line 226
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->resets:Ljava/lang/Integer;

    return-object p0
.end method

.method public start_period(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;
    .locals 0

    .line 260
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->start_period:Ljava/lang/Integer;

    return-object p0
.end method

.method public status(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;
    .locals 0

    .line 249
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->status:Ljava/lang/Integer;

    return-object p0
.end method

.method public wakeups(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;
    .locals 0

    .line 236
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->wakeups:Ljava/lang/Integer;

    return-object p0
.end method
