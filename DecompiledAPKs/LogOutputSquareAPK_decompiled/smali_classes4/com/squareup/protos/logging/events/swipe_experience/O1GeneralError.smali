.class public final Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;
.super Lcom/squareup/wire/Message;
.source "O1GeneralError.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ProtoAdapter_O1GeneralError;,
        Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;,
        Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_END_PERIOD:Ljava/lang/Integer;

.field public static final DEFAULT_ERROR_CODE:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

.field public static final DEFAULT_RESETS:Ljava/lang/Integer;

.field public static final DEFAULT_START_PERIOD:Ljava/lang/Integer;

.field public static final DEFAULT_STATUS:Ljava/lang/Integer;

.field public static final DEFAULT_WAKEUPS:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final end_period:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x6
    .end annotation
.end field

.field public final error_code:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.O1GeneralError$ErrorCode#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final resets:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field

.field public final start_period:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x5
    .end annotation
.end field

.field public final status:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field

.field public final wakeups:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ProtoAdapter_O1GeneralError;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ProtoAdapter_O1GeneralError;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->CARD_DATA_TOO_LONG:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->DEFAULT_ERROR_CODE:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    const/4 v0, 0x0

    .line 32
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->DEFAULT_RESETS:Ljava/lang/Integer;

    .line 34
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->DEFAULT_WAKEUPS:Ljava/lang/Integer;

    .line 36
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->DEFAULT_STATUS:Ljava/lang/Integer;

    .line 38
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->DEFAULT_START_PERIOD:Ljava/lang/Integer;

    .line 40
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->DEFAULT_END_PERIOD:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 8

    .line 120
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;-><init>(Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 125
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->error_code:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    .line 127
    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->resets:Ljava/lang/Integer;

    .line 128
    iput-object p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->wakeups:Ljava/lang/Integer;

    .line 129
    iput-object p4, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->status:Ljava/lang/Integer;

    .line 130
    iput-object p5, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->start_period:Ljava/lang/Integer;

    .line 131
    iput-object p6, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->end_period:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 150
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 151
    :cond_1
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    .line 152
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->error_code:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->error_code:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    .line 153
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->resets:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->resets:Ljava/lang/Integer;

    .line 154
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->wakeups:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->wakeups:Ljava/lang/Integer;

    .line 155
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->status:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->status:Ljava/lang/Integer;

    .line 156
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->start_period:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->start_period:Ljava/lang/Integer;

    .line 157
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->end_period:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->end_period:Ljava/lang/Integer;

    .line 158
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 163
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 165
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 166
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->error_code:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 167
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->resets:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 168
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->wakeups:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 169
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->status:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 170
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->start_period:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 171
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->end_period:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 172
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;
    .locals 2

    .line 136
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;-><init>()V

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->error_code:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->error_code:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->resets:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->resets:Ljava/lang/Integer;

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->wakeups:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->wakeups:Ljava/lang/Integer;

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->status:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->status:Ljava/lang/Integer;

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->start_period:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->start_period:Ljava/lang/Integer;

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->end_period:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->end_period:Ljava/lang/Integer;

    .line 143
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 180
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->error_code:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    if-eqz v1, :cond_0

    const-string v1, ", error_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->error_code:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 181
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->resets:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", resets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->resets:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 182
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->wakeups:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", wakeups="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->wakeups:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 183
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->status:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->status:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 184
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->start_period:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", start_period="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->start_period:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 185
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->end_period:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    const-string v1, ", end_period="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->end_period:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "O1GeneralError{"

    .line 186
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
