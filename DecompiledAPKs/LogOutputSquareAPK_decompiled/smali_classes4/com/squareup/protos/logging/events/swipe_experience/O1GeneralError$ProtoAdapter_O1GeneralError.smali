.class final Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ProtoAdapter_O1GeneralError;
.super Lcom/squareup/wire/ProtoAdapter;
.source "O1GeneralError.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_O1GeneralError"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 379
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 406
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;-><init>()V

    .line 407
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 408
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 424
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 422
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->end_period(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;

    goto :goto_0

    .line 421
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->start_period(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;

    goto :goto_0

    .line 420
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->status(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;

    goto :goto_0

    .line 419
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->wakeups(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;

    goto :goto_0

    .line 418
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->resets(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;

    goto :goto_0

    .line 412
    :pswitch_5
    :try_start_0
    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->error_code(Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 414
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 428
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 429
    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 377
    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ProtoAdapter_O1GeneralError;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 395
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->error_code:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 396
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->resets:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 397
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->wakeups:Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 398
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->status:Ljava/lang/Integer;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 399
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->start_period:Ljava/lang/Integer;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 400
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->end_period:Ljava/lang/Integer;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 401
    invoke-virtual {p2}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 377
    check-cast p2, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ProtoAdapter_O1GeneralError;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;)I
    .locals 4

    .line 384
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->error_code:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ErrorCode;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->resets:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 385
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->wakeups:Ljava/lang/Integer;

    const/4 v3, 0x3

    .line 386
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->status:Ljava/lang/Integer;

    const/4 v3, 0x4

    .line 387
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->start_period:Ljava/lang/Integer;

    const/4 v3, 0x5

    .line 388
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->end_period:Ljava/lang/Integer;

    const/4 v3, 0x6

    .line 389
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 390
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 377
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ProtoAdapter_O1GeneralError;->encodedSize(Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;
    .locals 0

    .line 434
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;

    move-result-object p1

    .line 435
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 436
    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 377
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError$ProtoAdapter_O1GeneralError;->redact(Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;)Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    move-result-object p1

    return-object p1
.end method
