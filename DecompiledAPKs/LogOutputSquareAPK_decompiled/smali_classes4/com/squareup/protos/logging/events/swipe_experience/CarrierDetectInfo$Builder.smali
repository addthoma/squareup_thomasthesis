.class public final Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CarrierDetectInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;",
        "Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allow_restart:Ljava/lang/Boolean;

.field public early_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

.field public end_avg:Ljava/lang/Integer;

.field public end_threshold:Ljava/lang/Integer;

.field public in_packet_runtime_us:Ljava/lang/Integer;

.field public is_early_packet:Ljava/lang/Boolean;

.field public late_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

.field public num_samples:Ljava/lang/Integer;

.field public start_avg:Ljava/lang/Integer;

.field public start_sample:Ljava/lang/Long;

.field public start_threshold:Ljava/lang/Integer;

.field public total_runtime_in_us:Ljava/lang/Long;

.field public total_time_in_us:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 325
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public allow_restart(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;
    .locals 0

    .line 417
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->allow_restart:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;
    .locals 17

    move-object/from16 v0, p0

    .line 451
    new-instance v16, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    iget-object v2, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->start_sample:Ljava/lang/Long;

    iget-object v3, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->num_samples:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->is_early_packet:Ljava/lang/Boolean;

    iget-object v5, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->total_runtime_in_us:Ljava/lang/Long;

    iget-object v6, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->total_time_in_us:Ljava/lang/Long;

    iget-object v7, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->start_threshold:Ljava/lang/Integer;

    iget-object v8, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->start_avg:Ljava/lang/Integer;

    iget-object v9, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->end_threshold:Ljava/lang/Integer;

    iget-object v10, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->end_avg:Ljava/lang/Integer;

    iget-object v11, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->allow_restart:Ljava/lang/Boolean;

    iget-object v12, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->early_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    iget-object v13, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->late_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    iget-object v14, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->in_packet_runtime_us:Ljava/lang/Integer;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v15

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v16
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 298
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    move-result-object v0

    return-object v0
.end method

.method public early_classify_stats(Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;
    .locals 0

    .line 426
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->early_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    return-object p0
.end method

.method public end_avg(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;
    .locals 0

    .line 408
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->end_avg:Ljava/lang/Integer;

    return-object p0
.end method

.method public end_threshold(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;
    .locals 0

    .line 399
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->end_threshold:Ljava/lang/Integer;

    return-object p0
.end method

.method public in_packet_runtime_us(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;
    .locals 0

    .line 445
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->in_packet_runtime_us:Ljava/lang/Integer;

    return-object p0
.end method

.method public is_early_packet(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;
    .locals 0

    .line 354
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->is_early_packet:Ljava/lang/Boolean;

    return-object p0
.end method

.method public late_classify_stats(Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;
    .locals 0

    .line 436
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->late_classify_stats:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    return-object p0
.end method

.method public num_samples(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;
    .locals 0

    .line 342
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->num_samples:Ljava/lang/Integer;

    return-object p0
.end method

.method public start_avg(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;
    .locals 0

    .line 390
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->start_avg:Ljava/lang/Integer;

    return-object p0
.end method

.method public start_sample(Ljava/lang/Long;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;
    .locals 0

    .line 333
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->start_sample:Ljava/lang/Long;

    return-object p0
.end method

.method public start_threshold(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;
    .locals 0

    .line 381
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->start_threshold:Ljava/lang/Integer;

    return-object p0
.end method

.method public total_runtime_in_us(Ljava/lang/Long;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;
    .locals 0

    .line 363
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->total_runtime_in_us:Ljava/lang/Long;

    return-object p0
.end method

.method public total_time_in_us(Ljava/lang/Long;)Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;
    .locals 0

    .line 372
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo$Builder;->total_time_in_us:Ljava/lang/Long;

    return-object p0
.end method
