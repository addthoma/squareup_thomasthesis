.class public final Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "O1FlashError.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public counter:Ljava/lang/Integer;

.field public entropy:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 113
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;
    .locals 4

    .line 143
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError$Builder;->counter:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError$Builder;->entropy:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 108
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    move-result-object v0

    return-object v0
.end method

.method public counter(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError$Builder;
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError$Builder;->counter:Ljava/lang/Integer;

    return-object p0
.end method

.method public entropy(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError$Builder;->entropy:Ljava/lang/Integer;

    return-object p0
.end method
