.class public final Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SqLinkDemodInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;",
        "Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public deconvolve_bit_errors:Ljava/lang/Integer;

.field public deconvolve_num_bits:Ljava/lang/Integer;

.field public deconvolve_runtime:Ljava/lang/Integer;

.field public demodulate_packets_runtime:Ljava/lang/Integer;

.field public deprecated_runtime_in_us:Ljava/lang/Integer;

.field public find_preamble_freq_runtime:Ljava/lang/Integer;

.field public first_find_sync_runtime:Ljava/lang/Integer;

.field public inverted:Ljava/lang/Boolean;

.field public is_fast:Ljava/lang/Boolean;

.field public last4_status:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;

.field public low_pass_filter_runtime:Ljava/lang/Integer;

.field public packet_frequency0:Ljava/lang/Float;

.field public packet_frequency1:Ljava/lang/Float;

.field public packet_frequency2:Ljava/lang/Float;

.field public packet_frequency3:Ljava/lang/Float;

.field public packet_frequency4:Ljava/lang/Float;

.field public preamble_freq:Ljava/lang/Float;

.field public rest_find_sync_runtime:Ljava/lang/Integer;

.field public result:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

.field public sync_index0:Ljava/lang/Integer;

.field public sync_index1:Ljava/lang/Integer;

.field public sync_index2:Ljava/lang/Integer;

.field public sync_index3:Ljava/lang/Integer;

.field public sync_index4:Ljava/lang/Integer;

.field public viterbi_runtime:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 522
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;
    .locals 2

    .line 731
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;-><init>(Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 471
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    move-result-object v0

    return-object v0
.end method

.method public deconvolve_bit_errors(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 717
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->deconvolve_bit_errors:Ljava/lang/Integer;

    return-object p0
.end method

.method public deconvolve_num_bits(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 725
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->deconvolve_num_bits:Ljava/lang/Integer;

    return-object p0
.end method

.method public deconvolve_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 604
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->deconvolve_runtime:Ljava/lang/Integer;

    return-object p0
.end method

.method public demodulate_packets_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 596
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->demodulate_packets_runtime:Ljava/lang/Integer;

    return-object p0
.end method

.method public deprecated_runtime_in_us(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 556
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->deprecated_runtime_in_us:Ljava/lang/Integer;

    return-object p0
.end method

.method public find_preamble_freq_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 572
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->find_preamble_freq_runtime:Ljava/lang/Integer;

    return-object p0
.end method

.method public first_find_sync_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 580
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->first_find_sync_runtime:Ljava/lang/Integer;

    return-object p0
.end method

.method public inverted(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 621
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->inverted:Ljava/lang/Boolean;

    return-object p0
.end method

.method public is_fast(Ljava/lang/Boolean;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 546
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->is_fast:Ljava/lang/Boolean;

    return-object p0
.end method

.method public last4_status(Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 537
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->last4_status:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;

    return-object p0
.end method

.method public low_pass_filter_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 564
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->low_pass_filter_runtime:Ljava/lang/Integer;

    return-object p0
.end method

.method public packet_frequency0(Ljava/lang/Float;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 677
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency0:Ljava/lang/Float;

    return-object p0
.end method

.method public packet_frequency1(Ljava/lang/Float;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 685
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency1:Ljava/lang/Float;

    return-object p0
.end method

.method public packet_frequency2(Ljava/lang/Float;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 693
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency2:Ljava/lang/Float;

    return-object p0
.end method

.method public packet_frequency3(Ljava/lang/Float;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 701
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency3:Ljava/lang/Float;

    return-object p0
.end method

.method public packet_frequency4(Ljava/lang/Float;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 709
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency4:Ljava/lang/Float;

    return-object p0
.end method

.method public preamble_freq(Ljava/lang/Float;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 629
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->preamble_freq:Ljava/lang/Float;

    return-object p0
.end method

.method public rest_find_sync_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 588
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->rest_find_sync_runtime:Ljava/lang/Integer;

    return-object p0
.end method

.method public result(Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 529
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->result:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    return-object p0
.end method

.method public sync_index0(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 637
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index0:Ljava/lang/Integer;

    return-object p0
.end method

.method public sync_index1(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 645
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index1:Ljava/lang/Integer;

    return-object p0
.end method

.method public sync_index2(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 653
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index2:Ljava/lang/Integer;

    return-object p0
.end method

.method public sync_index3(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 661
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index3:Ljava/lang/Integer;

    return-object p0
.end method

.method public sync_index4(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 669
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index4:Ljava/lang/Integer;

    return-object p0
.end method

.method public viterbi_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 0

    .line 612
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->viterbi_runtime:Ljava/lang/Integer;

    return-object p0
.end method
