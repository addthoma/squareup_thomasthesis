.class public final Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;
.super Lcom/squareup/wire/Message;
.source "NotificationMessageFormat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$ProtoAdapter_NotificationMessageFormat;,
        Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;",
        "Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AGGREGATABLE:Ljava/lang/Boolean;

.field public static final DEFAULT_AGGREGATED_BODY:Ljava/lang/String; = ""

.field public static final DEFAULT_AGGREGATED_BROWSER_DIALOG_BODY:Ljava/lang/String; = ""

.field public static final DEFAULT_AGGREGATED_PRIMARY_CTA:Ljava/lang/String; = ""

.field public static final DEFAULT_AGGREGATED_TITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_AGGREGATED_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_BODY:Ljava/lang/String; = ""

.field public static final DEFAULT_BROWSER_DIALOG_BODY:Ljava/lang/String; = ""

.field public static final DEFAULT_PRIMARY_CTA:Ljava/lang/String; = ""

.field public static final DEFAULT_TITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_URL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final aggregatable:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final aggregated_body:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final aggregated_browser_dialog_body:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field

.field public final aggregated_client_action:Lcom/squareup/protos/client/ClientAction;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final aggregated_primary_cta:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final aggregated_title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final aggregated_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final body:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final browser_dialog_body:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final client_action:Lcom/squareup/protos/client/ClientAction;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ClientAction#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final primary_cta:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$ProtoAdapter_NotificationMessageFormat;

    invoke-direct {v0}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$ProtoAdapter_NotificationMessageFormat;-><init>()V

    sput-object v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 36
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->DEFAULT_AGGREGATABLE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;)V
    .locals 15

    .line 173
    sget-object v14, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 181
    sget-object v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p14}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 182
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->title:Ljava/lang/String;

    .line 183
    iput-object p2, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->body:Ljava/lang/String;

    .line 184
    iput-object p3, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->primary_cta:Ljava/lang/String;

    .line 185
    iput-object p4, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->url:Ljava/lang/String;

    .line 186
    iput-object p5, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->client_action:Lcom/squareup/protos/client/ClientAction;

    .line 187
    iput-object p6, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->browser_dialog_body:Ljava/lang/String;

    .line 188
    iput-object p7, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregatable:Ljava/lang/Boolean;

    .line 189
    iput-object p8, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_title:Ljava/lang/String;

    .line 190
    iput-object p9, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_body:Ljava/lang/String;

    .line 191
    iput-object p10, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_primary_cta:Ljava/lang/String;

    .line 192
    iput-object p11, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_url:Ljava/lang/String;

    .line 193
    iput-object p12, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_client_action:Lcom/squareup/protos/client/ClientAction;

    .line 194
    iput-object p13, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_browser_dialog_body:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 220
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 221
    :cond_1
    check-cast p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    .line 222
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->title:Ljava/lang/String;

    .line 223
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->body:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->body:Ljava/lang/String;

    .line 224
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->primary_cta:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->primary_cta:Ljava/lang/String;

    .line 225
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->url:Ljava/lang/String;

    .line 226
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->client_action:Lcom/squareup/protos/client/ClientAction;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->client_action:Lcom/squareup/protos/client/ClientAction;

    .line 227
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->browser_dialog_body:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->browser_dialog_body:Ljava/lang/String;

    .line 228
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregatable:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregatable:Ljava/lang/Boolean;

    .line 229
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_title:Ljava/lang/String;

    .line 230
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_body:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_body:Ljava/lang/String;

    .line 231
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_primary_cta:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_primary_cta:Ljava/lang/String;

    .line 232
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_url:Ljava/lang/String;

    .line 233
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_client_action:Lcom/squareup/protos/client/ClientAction;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_client_action:Lcom/squareup/protos/client/ClientAction;

    .line 234
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_browser_dialog_body:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_browser_dialog_body:Ljava/lang/String;

    .line 235
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 240
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_d

    .line 242
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 243
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->title:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->body:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 245
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->primary_cta:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 246
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->url:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 247
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->client_action:Lcom/squareup/protos/client/ClientAction;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 248
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->browser_dialog_body:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 249
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregatable:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 250
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_title:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 251
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_body:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 252
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_primary_cta:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 253
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_url:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 254
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_client_action:Lcom/squareup/protos/client/ClientAction;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/client/ClientAction;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 255
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_browser_dialog_body:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_c
    add-int/2addr v0, v2

    .line 256
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_d
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;
    .locals 2

    .line 199
    new-instance v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;-><init>()V

    .line 200
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->title:Ljava/lang/String;

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->body:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->body:Ljava/lang/String;

    .line 202
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->primary_cta:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->primary_cta:Ljava/lang/String;

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->url:Ljava/lang/String;

    .line 204
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->client_action:Lcom/squareup/protos/client/ClientAction;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->client_action:Lcom/squareup/protos/client/ClientAction;

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->browser_dialog_body:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->browser_dialog_body:Ljava/lang/String;

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregatable:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregatable:Ljava/lang/Boolean;

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_title:Ljava/lang/String;

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_body:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_body:Ljava/lang/String;

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_primary_cta:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_primary_cta:Ljava/lang/String;

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_url:Ljava/lang/String;

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_client_action:Lcom/squareup/protos/client/ClientAction;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_client_action:Lcom/squareup/protos/client/ClientAction;

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_browser_dialog_body:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->aggregated_browser_dialog_body:Ljava/lang/String;

    .line 213
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->newBuilder()Lcom/squareup/protos/messageservice/service/NotificationMessageFormat$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 263
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 264
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->title:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->body:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", body="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->body:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->primary_cta:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", primary_cta="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->primary_cta:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->url:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->client_action:Lcom/squareup/protos/client/ClientAction;

    if-eqz v1, :cond_4

    const-string v1, ", client_action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->client_action:Lcom/squareup/protos/client/ClientAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 269
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->browser_dialog_body:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", browser_dialog_body="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->browser_dialog_body:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregatable:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", aggregatable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregatable:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 271
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_title:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", aggregated_title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_body:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", aggregated_body="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_body:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_primary_cta:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", aggregated_primary_cta="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_primary_cta:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_url:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", aggregated_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_client_action:Lcom/squareup/protos/client/ClientAction;

    if-eqz v1, :cond_b

    const-string v1, ", aggregated_client_action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_client_action:Lcom/squareup/protos/client/ClientAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 276
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_browser_dialog_body:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", aggregated_browser_dialog_body="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_browser_dialog_body:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_c
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "NotificationMessageFormat{"

    .line 277
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
