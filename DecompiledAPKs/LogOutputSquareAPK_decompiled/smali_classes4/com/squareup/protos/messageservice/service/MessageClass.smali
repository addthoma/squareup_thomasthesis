.class public final enum Lcom/squareup/protos/messageservice/service/MessageClass;
.super Ljava/lang/Enum;
.source "MessageClass.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/messageservice/service/MessageClass$ProtoAdapter_MessageClass;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/messageservice/service/MessageClass;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/messageservice/service/MessageClass;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/messageservice/service/MessageClass;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ALERT_COMPLETE_ORDERS:Lcom/squareup/protos/messageservice/service/MessageClass;

.field public static final enum ALERT_CUSTOMER_COMMS:Lcom/squareup/protos/messageservice/service/MessageClass;

.field public static final enum ALERT_HIGH_PRIORITY:Lcom/squareup/protos/messageservice/service/MessageClass;

.field public static final enum ALERT_RESOLVE_DISPUTE:Lcom/squareup/protos/messageservice/service/MessageClass;

.field public static final enum ALERT_SETUP_POS:Lcom/squareup/protos/messageservice/service/MessageClass;

.field public static final enum ALERT_SUPPORT_CENTER:Lcom/squareup/protos/messageservice/service/MessageClass;

.field public static final enum BUSINESS_CRITICAL:Lcom/squareup/protos/messageservice/service/MessageClass;

.field public static final enum DO_NOT_USE_MESSAGE_CLASS:Lcom/squareup/protos/messageservice/service/MessageClass;

.field public static final enum MARKETING_TO_CUSTOMERS_YOU_WANT_TO_ACQUIRE:Lcom/squareup/protos/messageservice/service/MessageClass;

.field public static final enum MARKETING_TO_YOUR_EXISTING_CUSTOMERS:Lcom/squareup/protos/messageservice/service/MessageClass;

.field public static final enum NON_URGENT_BUSINESS_NOTICES_FOR_SELLERS:Lcom/squareup/protos/messageservice/service/MessageClass;

.field public static final enum SPONSORED:Lcom/squareup/protos/messageservice/service/MessageClass;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 14
    new-instance v0, Lcom/squareup/protos/messageservice/service/MessageClass;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE_MESSAGE_CLASS"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/messageservice/service/MessageClass;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/MessageClass;->DO_NOT_USE_MESSAGE_CLASS:Lcom/squareup/protos/messageservice/service/MessageClass;

    .line 20
    new-instance v0, Lcom/squareup/protos/messageservice/service/MessageClass;

    const/4 v2, 0x1

    const/4 v3, 0x5

    const-string v4, "BUSINESS_CRITICAL"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/messageservice/service/MessageClass;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/MessageClass;->BUSINESS_CRITICAL:Lcom/squareup/protos/messageservice/service/MessageClass;

    .line 25
    new-instance v0, Lcom/squareup/protos/messageservice/service/MessageClass;

    const/4 v4, 0x2

    const/4 v5, 0x6

    const-string v6, "ALERT_SETUP_POS"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/messageservice/service/MessageClass;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_SETUP_POS:Lcom/squareup/protos/messageservice/service/MessageClass;

    .line 31
    new-instance v0, Lcom/squareup/protos/messageservice/service/MessageClass;

    const/4 v6, 0x3

    const/16 v7, 0x8

    const-string v8, "ALERT_COMPLETE_ORDERS"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/protos/messageservice/service/MessageClass;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_COMPLETE_ORDERS:Lcom/squareup/protos/messageservice/service/MessageClass;

    .line 37
    new-instance v0, Lcom/squareup/protos/messageservice/service/MessageClass;

    const/4 v8, 0x4

    const/16 v9, 0xa

    const-string v10, "NON_URGENT_BUSINESS_NOTICES_FOR_SELLERS"

    invoke-direct {v0, v10, v8, v9}, Lcom/squareup/protos/messageservice/service/MessageClass;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/MessageClass;->NON_URGENT_BUSINESS_NOTICES_FOR_SELLERS:Lcom/squareup/protos/messageservice/service/MessageClass;

    .line 42
    new-instance v0, Lcom/squareup/protos/messageservice/service/MessageClass;

    const/16 v10, 0xb

    const-string v11, "MARKETING_TO_YOUR_EXISTING_CUSTOMERS"

    invoke-direct {v0, v11, v3, v10}, Lcom/squareup/protos/messageservice/service/MessageClass;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/MessageClass;->MARKETING_TO_YOUR_EXISTING_CUSTOMERS:Lcom/squareup/protos/messageservice/service/MessageClass;

    .line 47
    new-instance v0, Lcom/squareup/protos/messageservice/service/MessageClass;

    const/16 v11, 0xc

    const-string v12, "MARKETING_TO_CUSTOMERS_YOU_WANT_TO_ACQUIRE"

    invoke-direct {v0, v12, v5, v11}, Lcom/squareup/protos/messageservice/service/MessageClass;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/MessageClass;->MARKETING_TO_CUSTOMERS_YOU_WANT_TO_ACQUIRE:Lcom/squareup/protos/messageservice/service/MessageClass;

    .line 52
    new-instance v0, Lcom/squareup/protos/messageservice/service/MessageClass;

    const/4 v12, 0x7

    const-string v13, "SPONSORED"

    const/16 v14, 0xd

    invoke-direct {v0, v13, v12, v14}, Lcom/squareup/protos/messageservice/service/MessageClass;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/MessageClass;->SPONSORED:Lcom/squareup/protos/messageservice/service/MessageClass;

    .line 57
    new-instance v0, Lcom/squareup/protos/messageservice/service/MessageClass;

    const-string v13, "ALERT_HIGH_PRIORITY"

    const/16 v14, 0xe

    invoke-direct {v0, v13, v7, v14}, Lcom/squareup/protos/messageservice/service/MessageClass;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_HIGH_PRIORITY:Lcom/squareup/protos/messageservice/service/MessageClass;

    .line 62
    new-instance v0, Lcom/squareup/protos/messageservice/service/MessageClass;

    const/16 v13, 0x9

    const-string v14, "ALERT_RESOLVE_DISPUTE"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v13, v15}, Lcom/squareup/protos/messageservice/service/MessageClass;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_RESOLVE_DISPUTE:Lcom/squareup/protos/messageservice/service/MessageClass;

    .line 67
    new-instance v0, Lcom/squareup/protos/messageservice/service/MessageClass;

    const-string v14, "ALERT_SUPPORT_CENTER"

    const/16 v15, 0x10

    invoke-direct {v0, v14, v9, v15}, Lcom/squareup/protos/messageservice/service/MessageClass;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_SUPPORT_CENTER:Lcom/squareup/protos/messageservice/service/MessageClass;

    .line 72
    new-instance v0, Lcom/squareup/protos/messageservice/service/MessageClass;

    const-string v14, "ALERT_CUSTOMER_COMMS"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v10, v15}, Lcom/squareup/protos/messageservice/service/MessageClass;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_CUSTOMER_COMMS:Lcom/squareup/protos/messageservice/service/MessageClass;

    new-array v0, v11, [Lcom/squareup/protos/messageservice/service/MessageClass;

    .line 10
    sget-object v11, Lcom/squareup/protos/messageservice/service/MessageClass;->DO_NOT_USE_MESSAGE_CLASS:Lcom/squareup/protos/messageservice/service/MessageClass;

    aput-object v11, v0, v1

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->BUSINESS_CRITICAL:Lcom/squareup/protos/messageservice/service/MessageClass;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_SETUP_POS:Lcom/squareup/protos/messageservice/service/MessageClass;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_COMPLETE_ORDERS:Lcom/squareup/protos/messageservice/service/MessageClass;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->NON_URGENT_BUSINESS_NOTICES_FOR_SELLERS:Lcom/squareup/protos/messageservice/service/MessageClass;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->MARKETING_TO_YOUR_EXISTING_CUSTOMERS:Lcom/squareup/protos/messageservice/service/MessageClass;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->MARKETING_TO_CUSTOMERS_YOU_WANT_TO_ACQUIRE:Lcom/squareup/protos/messageservice/service/MessageClass;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->SPONSORED:Lcom/squareup/protos/messageservice/service/MessageClass;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_HIGH_PRIORITY:Lcom/squareup/protos/messageservice/service/MessageClass;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_RESOLVE_DISPUTE:Lcom/squareup/protos/messageservice/service/MessageClass;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_SUPPORT_CENTER:Lcom/squareup/protos/messageservice/service/MessageClass;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_CUSTOMER_COMMS:Lcom/squareup/protos/messageservice/service/MessageClass;

    aput-object v1, v0, v10

    sput-object v0, Lcom/squareup/protos/messageservice/service/MessageClass;->$VALUES:[Lcom/squareup/protos/messageservice/service/MessageClass;

    .line 74
    new-instance v0, Lcom/squareup/protos/messageservice/service/MessageClass$ProtoAdapter_MessageClass;

    invoke-direct {v0}, Lcom/squareup/protos/messageservice/service/MessageClass$ProtoAdapter_MessageClass;-><init>()V

    sput-object v0, Lcom/squareup/protos/messageservice/service/MessageClass;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 78
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 79
    iput p3, p0, Lcom/squareup/protos/messageservice/service/MessageClass;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/messageservice/service/MessageClass;
    .locals 1

    if-eqz p0, :cond_3

    const/16 v0, 0x8

    if-eq p0, v0, :cond_2

    const/4 v0, 0x5

    if-eq p0, v0, :cond_1

    const/4 v0, 0x6

    if-eq p0, v0, :cond_0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 98
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_CUSTOMER_COMMS:Lcom/squareup/protos/messageservice/service/MessageClass;

    return-object p0

    .line 97
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_SUPPORT_CENTER:Lcom/squareup/protos/messageservice/service/MessageClass;

    return-object p0

    .line 96
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_RESOLVE_DISPUTE:Lcom/squareup/protos/messageservice/service/MessageClass;

    return-object p0

    .line 95
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_HIGH_PRIORITY:Lcom/squareup/protos/messageservice/service/MessageClass;

    return-object p0

    .line 94
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/messageservice/service/MessageClass;->SPONSORED:Lcom/squareup/protos/messageservice/service/MessageClass;

    return-object p0

    .line 93
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/messageservice/service/MessageClass;->MARKETING_TO_CUSTOMERS_YOU_WANT_TO_ACQUIRE:Lcom/squareup/protos/messageservice/service/MessageClass;

    return-object p0

    .line 92
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/messageservice/service/MessageClass;->MARKETING_TO_YOUR_EXISTING_CUSTOMERS:Lcom/squareup/protos/messageservice/service/MessageClass;

    return-object p0

    .line 91
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/messageservice/service/MessageClass;->NON_URGENT_BUSINESS_NOTICES_FOR_SELLERS:Lcom/squareup/protos/messageservice/service/MessageClass;

    return-object p0

    .line 89
    :cond_0
    sget-object p0, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_SETUP_POS:Lcom/squareup/protos/messageservice/service/MessageClass;

    return-object p0

    .line 88
    :cond_1
    sget-object p0, Lcom/squareup/protos/messageservice/service/MessageClass;->BUSINESS_CRITICAL:Lcom/squareup/protos/messageservice/service/MessageClass;

    return-object p0

    .line 90
    :cond_2
    sget-object p0, Lcom/squareup/protos/messageservice/service/MessageClass;->ALERT_COMPLETE_ORDERS:Lcom/squareup/protos/messageservice/service/MessageClass;

    return-object p0

    .line 87
    :cond_3
    sget-object p0, Lcom/squareup/protos/messageservice/service/MessageClass;->DO_NOT_USE_MESSAGE_CLASS:Lcom/squareup/protos/messageservice/service/MessageClass;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/MessageClass;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/messageservice/service/MessageClass;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/messageservice/service/MessageClass;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/messageservice/service/MessageClass;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/messageservice/service/MessageClass;->$VALUES:[Lcom/squareup/protos/messageservice/service/MessageClass;

    invoke-virtual {v0}, [Lcom/squareup/protos/messageservice/service/MessageClass;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/messageservice/service/MessageClass;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 105
    iget v0, p0, Lcom/squareup/protos/messageservice/service/MessageClass;->value:I

    return v0
.end method
