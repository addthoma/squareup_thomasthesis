.class final Lcom/squareup/protos/messageservice/service/MessageUnit$ProtoAdapter_MessageUnit;
.super Lcom/squareup/wire/ProtoAdapter;
.source "MessageUnit.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messageservice/service/MessageUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_MessageUnit"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/messageservice/service/MessageUnit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 305
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/messageservice/service/MessageUnit;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/messageservice/service/MessageUnit;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 338
    new-instance v0, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;-><init>()V

    .line 339
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 340
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 366
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 364
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->message_id(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;

    goto :goto_0

    .line 363
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->notification_message_format(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;

    goto :goto_0

    .line 362
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/messageservice/service/DefaultFormat;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/messageservice/service/DefaultFormat;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->default_format(Lcom/squareup/protos/messageservice/service/DefaultFormat;)Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;

    goto :goto_0

    .line 361
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->created_at(Ljava/lang/Long;)Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;

    goto :goto_0

    .line 355
    :pswitch_4
    :try_start_0
    sget-object v4, Lcom/squareup/protos/messageservice/service/MessageClass;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/messageservice/service/MessageClass;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->message_class(Lcom/squareup/protos/messageservice/service/MessageClass;)Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 357
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 352
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->message_unit_token(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;

    goto :goto_0

    .line 346
    :pswitch_6
    :try_start_1
    sget-object v4, Lcom/squareup/protos/messageservice/service/State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/messageservice/service/State;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->state(Lcom/squareup/protos/messageservice/service/State;)Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 348
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 343
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->message_format_id(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;

    goto/16 :goto_0

    .line 342
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->request_token(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;

    goto/16 :goto_0

    .line 370
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 371
    invoke-virtual {v0}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->build()Lcom/squareup/protos/messageservice/service/MessageUnit;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 303
    invoke-virtual {p0, p1}, Lcom/squareup/protos/messageservice/service/MessageUnit$ProtoAdapter_MessageUnit;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/messageservice/service/MessageUnit;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/messageservice/service/MessageUnit;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 324
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/MessageUnit;->request_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 325
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_format_id:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 326
    sget-object v0, Lcom/squareup/protos/messageservice/service/State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/MessageUnit;->state:Lcom/squareup/protos/messageservice/service/State;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 327
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_unit_token:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 328
    sget-object v0, Lcom/squareup/protos/messageservice/service/MessageClass;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_class:Lcom/squareup/protos/messageservice/service/MessageClass;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 329
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/MessageUnit;->created_at:Ljava/lang/Long;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 330
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_id:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 331
    sget-object v0, Lcom/squareup/protos/messageservice/service/DefaultFormat;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/MessageUnit;->default_format:Lcom/squareup/protos/messageservice/service/DefaultFormat;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 332
    sget-object v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messageservice/service/MessageUnit;->notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 333
    invoke-virtual {p2}, Lcom/squareup/protos/messageservice/service/MessageUnit;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 303
    check-cast p2, Lcom/squareup/protos/messageservice/service/MessageUnit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/messageservice/service/MessageUnit$ProtoAdapter_MessageUnit;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/messageservice/service/MessageUnit;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/messageservice/service/MessageUnit;)I
    .locals 4

    .line 310
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;->request_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_format_id:Ljava/lang/String;

    const/4 v3, 0x2

    .line 311
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/messageservice/service/State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;->state:Lcom/squareup/protos/messageservice/service/State;

    const/4 v3, 0x3

    .line 312
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_unit_token:Ljava/lang/String;

    const/4 v3, 0x4

    .line 313
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/messageservice/service/MessageClass;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_class:Lcom/squareup/protos/messageservice/service/MessageClass;

    const/4 v3, 0x5

    .line 314
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;->created_at:Ljava/lang/Long;

    const/4 v3, 0x6

    .line 315
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_id:Ljava/lang/String;

    const/16 v3, 0x9

    .line 316
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/messageservice/service/DefaultFormat;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;->default_format:Lcom/squareup/protos/messageservice/service/DefaultFormat;

    const/4 v3, 0x7

    .line 317
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messageservice/service/MessageUnit;->notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    const/16 v3, 0x8

    .line 318
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 319
    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/MessageUnit;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 303
    check-cast p1, Lcom/squareup/protos/messageservice/service/MessageUnit;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/messageservice/service/MessageUnit$ProtoAdapter_MessageUnit;->encodedSize(Lcom/squareup/protos/messageservice/service/MessageUnit;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/messageservice/service/MessageUnit;)Lcom/squareup/protos/messageservice/service/MessageUnit;
    .locals 2

    .line 376
    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/MessageUnit;->newBuilder()Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;

    move-result-object p1

    .line 377
    iget-object v0, p1, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->default_format:Lcom/squareup/protos/messageservice/service/DefaultFormat;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/messageservice/service/DefaultFormat;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->default_format:Lcom/squareup/protos/messageservice/service/DefaultFormat;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/messageservice/service/DefaultFormat;

    iput-object v0, p1, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->default_format:Lcom/squareup/protos/messageservice/service/DefaultFormat;

    .line 378
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    iput-object v0, p1, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    .line 379
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 380
    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/MessageUnit$Builder;->build()Lcom/squareup/protos/messageservice/service/MessageUnit;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 303
    check-cast p1, Lcom/squareup/protos/messageservice/service/MessageUnit;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/messageservice/service/MessageUnit$ProtoAdapter_MessageUnit;->redact(Lcom/squareup/protos/messageservice/service/MessageUnit;)Lcom/squareup/protos/messageservice/service/MessageUnit;

    move-result-object p1

    return-object p1
.end method
