.class public final Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SupportResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;",
        "Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public phone:Ljava/lang/String;

.field public type:Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 128
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;
    .locals 5

    .line 159
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;->type:Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;

    iget-object v2, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;->url:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;->phone:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;-><init>(Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;

    move-result-object v0

    return-object v0
.end method

.method public phone(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;->phone:Ljava/lang/String;

    const/4 p1, 0x0

    .line 153
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;->url:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;)Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;->type:Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;

    return-object p0
.end method

.method public url(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;->url:Ljava/lang/String;

    const/4 p1, 0x0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;->phone:Ljava/lang/String;

    return-object p0
.end method
