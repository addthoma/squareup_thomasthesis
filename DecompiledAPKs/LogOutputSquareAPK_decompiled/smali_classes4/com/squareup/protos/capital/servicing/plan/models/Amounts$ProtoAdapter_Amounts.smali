.class final Lcom/squareup/protos/capital/servicing/plan/models/Amounts$ProtoAdapter_Amounts;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Amounts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Amounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Amounts"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Amounts;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 539
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 562
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;-><init>()V

    .line 563
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 564
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 571
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 569
    :cond_0
    sget-object v3, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->detail_amounts_pending_applied(Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;

    goto :goto_0

    .line 568
    :cond_1
    sget-object v3, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->detail_amounts(Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;

    goto :goto_0

    .line 567
    :cond_2
    sget-object v3, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->base_amounts(Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;

    goto :goto_0

    .line 566
    :cond_3
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;

    goto :goto_0

    .line 575
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 576
    invoke-virtual {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 537
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$ProtoAdapter_Amounts;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/Amounts;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 553
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->pending_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 554
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->base_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 555
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->detail_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 556
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->detail_amounts_pending_applied:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 557
    invoke-virtual {p2}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 537
    check-cast p2, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$ProtoAdapter_Amounts;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/Amounts;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/Amounts;)I
    .locals 4

    .line 544
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->pending_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->base_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    const/4 v3, 0x2

    .line 545
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->detail_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    const/4 v3, 0x3

    .line 546
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->detail_amounts_pending_applied:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    const/4 v3, 0x4

    .line 547
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 548
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 537
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$ProtoAdapter_Amounts;->encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/Amounts;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/capital/servicing/plan/models/Amounts;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts;
    .locals 2

    .line 581
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;

    move-result-object p1

    .line 582
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->pending_money:Lcom/squareup/protos/common/Money;

    .line 583
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->base_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->base_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->base_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    .line 584
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->detail_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->detail_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->detail_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    .line 585
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->detail_amounts_pending_applied:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->detail_amounts_pending_applied:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->detail_amounts_pending_applied:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    .line 586
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 587
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 537
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$ProtoAdapter_Amounts;->redact(Lcom/squareup/protos/capital/servicing/plan/models/Amounts;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    move-result-object p1

    return-object p1
.end method
