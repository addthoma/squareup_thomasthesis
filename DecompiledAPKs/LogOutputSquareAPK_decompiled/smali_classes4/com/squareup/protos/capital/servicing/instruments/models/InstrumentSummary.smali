.class public final Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;
.super Lcom/squareup/wire/Message;
.source "InstrumentSummary.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$ProtoAdapter_InstrumentSummary;,
        Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;",
        "Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_IS_ACTIVE:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final bank_account_summary:Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.instruments.models.BankAccountSummary#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final card_summary:Lcom/squareup/protos/simple_instrument_store/api/CardSummary;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.simple_instrument_store.api.CardSummary#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final is_active:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$ProtoAdapter_InstrumentSummary;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$ProtoAdapter_InstrumentSummary;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 34
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->DEFAULT_IS_ACTIVE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;Lcom/squareup/protos/simple_instrument_store/api/CardSummary;)V
    .locals 6

    .line 72
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;Lcom/squareup/protos/simple_instrument_store/api/CardSummary;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;Lcom/squareup/protos/simple_instrument_store/api/CardSummary;Lokio/ByteString;)V
    .locals 1

    .line 77
    sget-object v0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 78
    invoke-static {p3, p4}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p5

    const/4 v0, 0x1

    if-gt p5, v0, :cond_0

    .line 81
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->id:Ljava/lang/String;

    .line 82
    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->is_active:Ljava/lang/Boolean;

    .line 83
    iput-object p3, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->bank_account_summary:Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    .line 84
    iput-object p4, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->card_summary:Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    return-void

    .line 79
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of bank_account_summary, card_summary may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 101
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 102
    :cond_1
    check-cast p1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->id:Ljava/lang/String;

    .line 104
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->is_active:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->is_active:Ljava/lang/Boolean;

    .line 105
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->bank_account_summary:Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->bank_account_summary:Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    .line 106
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->card_summary:Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->card_summary:Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    .line 107
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 112
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->is_active:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->bank_account_summary:Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->card_summary:Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 119
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;
    .locals 2

    .line 89
    new-instance v0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;-><init>()V

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->id:Ljava/lang/String;

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->is_active:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->is_active:Ljava/lang/Boolean;

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->bank_account_summary:Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->bank_account_summary:Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->card_summary:Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->card_summary:Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->newBuilder()Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->is_active:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", is_active="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->is_active:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 129
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->bank_account_summary:Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    if-eqz v1, :cond_2

    const-string v1, ", bank_account_summary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->bank_account_summary:Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 130
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->card_summary:Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    if-eqz v1, :cond_3

    const-string v1, ", card_summary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;->card_summary:Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InstrumentSummary{"

    .line 131
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
