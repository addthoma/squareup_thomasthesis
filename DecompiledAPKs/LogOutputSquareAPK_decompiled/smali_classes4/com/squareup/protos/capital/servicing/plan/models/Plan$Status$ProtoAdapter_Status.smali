.class final Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status$ProtoAdapter_Status;
.super Lcom/squareup/wire/EnumAdapter;
.source "Plan.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 798
    const-class v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;
    .locals 0

    .line 803
    invoke-static {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;->fromValue(I)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 796
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status$ProtoAdapter_Status;->fromValue(I)Lcom/squareup/protos/capital/servicing/plan/models/Plan$Status;

    move-result-object p1

    return-object p1
.end method
