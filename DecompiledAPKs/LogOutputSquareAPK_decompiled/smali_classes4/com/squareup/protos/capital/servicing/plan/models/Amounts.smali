.class public final Lcom/squareup/protos/capital/servicing/plan/models/Amounts;
.super Lcom/squareup/wire/Message;
.source "Amounts.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/Amounts$ProtoAdapter_Amounts;,
        Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;,
        Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;,
        Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Amounts;",
        "Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Amounts;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final base_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.Amounts$BaseAmounts#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final detail_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.Amounts$DetailAmounts#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final detail_amounts_pending_applied:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.Amounts$DetailAmounts#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final pending_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$ProtoAdapter_Amounts;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$ProtoAdapter_Amounts;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;)V
    .locals 6

    .line 62
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;Lokio/ByteString;)V
    .locals 1

    .line 67
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 68
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->pending_money:Lcom/squareup/protos/common/Money;

    .line 69
    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->base_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    .line 70
    iput-object p3, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->detail_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    .line 71
    iput-object p4, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->detail_amounts_pending_applied:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 88
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 89
    :cond_1
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->pending_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->pending_money:Lcom/squareup/protos/common/Money;

    .line 91
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->base_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->base_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    .line 92
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->detail_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->detail_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    .line 93
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->detail_amounts_pending_applied:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->detail_amounts_pending_applied:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    .line 94
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 99
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->pending_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->base_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->detail_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->detail_amounts_pending_applied:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 106
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;
    .locals 2

    .line 76
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;-><init>()V

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->pending_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->pending_money:Lcom/squareup/protos/common/Money;

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->base_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->base_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    .line 79
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->detail_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->detail_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    .line 80
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->detail_amounts_pending_applied:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->detail_amounts_pending_applied:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    .line 81
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/Amounts$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    const-string v1, ", pending_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 115
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->base_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    if-eqz v1, :cond_1

    const-string v1, ", base_amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->base_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 116
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->detail_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    if-eqz v1, :cond_2

    const-string v1, ", detail_amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->detail_amounts:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 117
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->detail_amounts_pending_applied:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    if-eqz v1, :cond_3

    const-string v1, ", detail_amounts_pending_applied="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts;->detail_amounts_pending_applied:Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Amounts{"

    .line 118
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
