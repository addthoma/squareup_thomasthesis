.class public final Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ScheduledPayment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;",
        "Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public debit_money:Lcom/squareup/protos/common/Money;

.field public debited_at:Ljava/lang/String;

.field public late_fee_applied_at:Ljava/lang/String;

.field public late_fee_money:Lcom/squareup/protos/common/Money;

.field public remaining_due_money:Lcom/squareup/protos/common/Money;

.field public remaining_late_fee_money:Lcom/squareup/protos/common/Money;

.field public remaining_non_due_money:Lcom/squareup/protos/common/Money;

.field public remaining_past_due_money:Lcom/squareup/protos/common/Money;

.field public status:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 225
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;
    .locals 12

    .line 302
    new-instance v11, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->status:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    iget-object v2, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->debited_at:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->debit_money:Lcom/squareup/protos/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->late_fee_applied_at:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->late_fee_money:Lcom/squareup/protos/common/Money;

    iget-object v6, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_non_due_money:Lcom/squareup/protos/common/Money;

    iget-object v7, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_due_money:Lcom/squareup/protos/common/Money;

    iget-object v8, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_past_due_money:Lcom/squareup/protos/common/Money;

    iget-object v9, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_late_fee_money:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;-><init>(Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 206
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment;

    move-result-object v0

    return-object v0
.end method

.method public debit_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;
    .locals 0

    .line 248
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->debit_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public debited_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->debited_at:Ljava/lang/String;

    return-object p0
.end method

.method public late_fee_applied_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;
    .locals 0

    .line 256
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->late_fee_applied_at:Ljava/lang/String;

    return-object p0
.end method

.method public late_fee_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;
    .locals 0

    .line 264
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->late_fee_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public remaining_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;
    .locals 0

    .line 280
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_due_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public remaining_late_fee_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;
    .locals 0

    .line 296
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_late_fee_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public remaining_non_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;
    .locals 0

    .line 272
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_non_due_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public remaining_past_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;
    .locals 0

    .line 288
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->remaining_past_due_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;)Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Builder;->status:Lcom/squareup/protos/capital/servicing/plan/models/ScheduledPayment$Status;

    return-object p0
.end method
