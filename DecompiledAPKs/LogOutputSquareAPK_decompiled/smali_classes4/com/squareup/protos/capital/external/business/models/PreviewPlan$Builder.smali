.class public final Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PreviewPlan.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/external/business/models/PreviewPlan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/external/business/models/PreviewPlan;",
        "Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public legacy_plan_id:Ljava/lang/String;

.field public plan_id:Ljava/lang/String;

.field public plan_servicing_url:Ljava/lang/String;

.field public product_type:Lcom/squareup/protos/capital/external/business/models/ProductType;

.field public rfc3986_plan_servicing_url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 155
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/external/business/models/PreviewPlan;
    .locals 8

    .line 198
    new-instance v7, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;->plan_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;->plan_servicing_url:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;->legacy_plan_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;->product_type:Lcom/squareup/protos/capital/external/business/models/ProductType;

    iget-object v5, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;->rfc3986_plan_servicing_url:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/capital/external/business/models/ProductType;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 144
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;->build()Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    move-result-object v0

    return-object v0
.end method

.method public legacy_plan_id(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 177
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;->legacy_plan_id:Ljava/lang/String;

    return-object p0
.end method

.method public plan_id(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;->plan_id:Ljava/lang/String;

    return-object p0
.end method

.method public plan_servicing_url(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;->plan_servicing_url:Ljava/lang/String;

    return-object p0
.end method

.method public product_type(Lcom/squareup/protos/capital/external/business/models/ProductType;)Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;->product_type:Lcom/squareup/protos/capital/external/business/models/ProductType;

    return-object p0
.end method

.method public rfc3986_plan_servicing_url(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;
    .locals 0

    .line 192
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewPlan$Builder;->rfc3986_plan_servicing_url:Ljava/lang/String;

    return-object p0
.end method
