.class public final enum Lcom/squareup/protos/precog/OnboardingEntryPoint;
.super Ljava/lang/Enum;
.source "OnboardingEntryPoint.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/precog/OnboardingEntryPoint$ProtoAdapter_OnboardingEntryPoint;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/precog/OnboardingEntryPoint;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/precog/OnboardingEntryPoint;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum OEP_APPLE_PAY_PROMO:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_APPOINTMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_APP_MARKETPLACE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_CAPITAL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_CONTACT_SALES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_CUSTOMER_DIRECTORY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_CUSTOMER_ENGAGEMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_DEPOSITS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_DEVELOPERS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_ECOMMERCE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_ECOM_ONLINE_STORE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_EMPLOYEES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_GIFT_CARDS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_GUIDES_APPLE_PAY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_GUIDES_CREDIT_CARDS_PROCESSING:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_GUIDES_GIFT_CARDS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_GUIDES_INVOICES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_GUIDES_LIABILITY_SHIFT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_GUIDES_MOBILE_PAYMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_GUIDES_NFC:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_GUIDES_PAYMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_GUIDES_PAYMENT_GATEWAY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_GUIDES_PCI:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_GUIDES_POS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_GUIDES_RESTAURANT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_GUIDES_RUN_BUSINESS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_GUIDES_SALON:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_GUIDES_SCHEDULING_SOFTWARE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_GUIDES_START_BUSINESS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_HOMEPAGE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_INVOICES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_LOGIN:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_ONLINE_STORE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_OVER_250:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_PAYMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_PAYMENTS_ENTERTAINMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_PAYMENTS_ORGANIZATIONS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_PAYROLL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_ANALYTICS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_APPAREL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_BAKERY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_BARS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_BEAUTY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_COFFEE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_DASHBOARD:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_ENTERTAINMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_FOOD:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_FOOD_TRUCK:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_GIFT_CARDS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_GROCERY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_HEALTH_AND_FITNESS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_HEALTH_AND_FITNESS_APOS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_HEALTH_AND_FITNESS_POS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_INVENTORY_MANAGEMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_LOCATION_MANAGEMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_LOYALTY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_MARKETING:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_ORGANIZATIONS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_PAYMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_PROFESSIONAL_SERVICES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_QUICK_SERVICE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_QUICK_SERVICE_POS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_QUICK_SERVICE_RPOS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_RESTAURANT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_RETAIL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_SALON:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_SPA:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_SPOS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_TAXI:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_TOUCH_BISTRO:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_POS_VEND:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_PRICING:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_R12:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_R4:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_REDEMPTION:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_SECURE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_SQUARE_ONE_BUSINESS_MANAGEMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_SQUARE_ONE_QUICK_SERVICE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_SQUARE_ONE_RETAIL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_SQUARE_ONE_ROUTING:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_SQUARE_ONE_SERVICES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_TOWN_SQUARE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_UNDER_250:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_UNKNOWN:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_VIRTUAL_TERMINAL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final enum OEP_WEEBLY:Lcom/squareup/protos/precog/OnboardingEntryPoint;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 11
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/4 v1, 0x0

    const-string v2, "OEP_UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_UNKNOWN:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 13
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/4 v2, 0x1

    const-string v3, "OEP_APPLE_PAY_PROMO"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_APPLE_PAY_PROMO:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 15
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/4 v3, 0x2

    const-string v4, "OEP_APPOINTMENTS"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_APPOINTMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 17
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/4 v4, 0x3

    const-string v5, "OEP_APP_MARKETPLACE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_APP_MARKETPLACE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 19
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/4 v5, 0x4

    const-string v6, "OEP_CONTACT_SALES"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_CONTACT_SALES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 21
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/4 v6, 0x5

    const-string v7, "OEP_CUSTOMER_DIRECTORY"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_CUSTOMER_DIRECTORY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 23
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/4 v7, 0x6

    const-string v8, "OEP_CUSTOMER_ENGAGEMENT"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_CUSTOMER_ENGAGEMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 25
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/4 v8, 0x7

    const-string v9, "OEP_DEPOSITS"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_DEPOSITS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 27
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v9, 0x8

    const-string v10, "OEP_ECOMMERCE"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_ECOMMERCE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 29
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v10, 0x9

    const-string v11, "OEP_EMPLOYEES"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_EMPLOYEES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 31
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v11, 0xa

    const-string v12, "OEP_GIFT_CARDS"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GIFT_CARDS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 33
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v12, 0xb

    const-string v13, "OEP_GUIDES_APPLE_PAY"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_APPLE_PAY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 35
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v13, 0xc

    const-string v14, "OEP_GUIDES_CREDIT_CARDS_PROCESSING"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_CREDIT_CARDS_PROCESSING:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 37
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v14, 0xd

    const-string v15, "OEP_GUIDES_GIFT_CARDS"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_GIFT_CARDS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 39
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v15, 0xe

    const-string v14, "OEP_GUIDES_INVOICES"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_INVOICES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 41
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v14, "OEP_GUIDES_LIABILITY_SHIFT"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_LIABILITY_SHIFT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 43
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_GUIDES_MOBILE_PAYMENTS"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_MOBILE_PAYMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 45
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_GUIDES_NFC"

    const/16 v14, 0x11

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_NFC:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 47
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_GUIDES_PAYMENTS"

    const/16 v14, 0x12

    const/16 v15, 0x12

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_PAYMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 49
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_GUIDES_PAYMENT_GATEWAY"

    const/16 v14, 0x13

    const/16 v15, 0x13

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_PAYMENT_GATEWAY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 51
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_GUIDES_PCI"

    const/16 v14, 0x14

    const/16 v15, 0x14

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_PCI:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 53
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_GUIDES_POS"

    const/16 v14, 0x15

    const/16 v15, 0x15

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_POS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 55
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_GUIDES_RESTAURANT"

    const/16 v14, 0x16

    const/16 v15, 0x16

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_RESTAURANT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 57
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_GUIDES_RUN_BUSINESS"

    const/16 v14, 0x17

    const/16 v15, 0x17

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_RUN_BUSINESS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 59
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_GUIDES_SALON"

    const/16 v14, 0x18

    const/16 v15, 0x18

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_SALON:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 61
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_GUIDES_SCHEDULING_SOFTWARE"

    const/16 v14, 0x19

    const/16 v15, 0x19

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_SCHEDULING_SOFTWARE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 63
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_GUIDES_START_BUSINESS"

    const/16 v14, 0x1a

    const/16 v15, 0x1a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_START_BUSINESS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 65
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_HOMEPAGE"

    const/16 v14, 0x1b

    const/16 v15, 0x1b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_HOMEPAGE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 67
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_INVOICES"

    const/16 v14, 0x1c

    const/16 v15, 0x1c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_INVOICES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 69
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_LOGIN"

    const/16 v14, 0x1d

    const/16 v15, 0x1d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_LOGIN:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 71
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_ONLINE_STORE"

    const/16 v14, 0x1e

    const/16 v15, 0x1e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_ONLINE_STORE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 76
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_OVER_250"

    const/16 v14, 0x1f

    const/16 v15, 0x1f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_OVER_250:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 78
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_PAYROLL"

    const/16 v14, 0x20

    const/16 v15, 0x20

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_PAYROLL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 80
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS"

    const/16 v14, 0x21

    const/16 v15, 0x21

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 82
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_ANALYTICS"

    const/16 v14, 0x22

    const/16 v15, 0x22

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_ANALYTICS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 87
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_APPAREL"

    const/16 v14, 0x23

    const/16 v15, 0x23

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_APPAREL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 92
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_BAKERY"

    const/16 v14, 0x24

    const/16 v15, 0x24

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_BAKERY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 94
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_BEAUTY"

    const/16 v14, 0x25

    const/16 v15, 0x25

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_BEAUTY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 96
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_COFFEE"

    const/16 v14, 0x26

    const/16 v15, 0x26

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_COFFEE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 98
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_DASHBOARD"

    const/16 v14, 0x27

    const/16 v15, 0x27

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_DASHBOARD:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 103
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_ENTERTAINMENT"

    const/16 v14, 0x28

    const/16 v15, 0x28

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_ENTERTAINMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 108
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_FOOD"

    const/16 v14, 0x29

    const/16 v15, 0x29

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_FOOD:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 113
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_FOOD_TRUCK"

    const/16 v14, 0x2a

    const/16 v15, 0x2a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_FOOD_TRUCK:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 118
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_GROCERY"

    const/16 v14, 0x2b

    const/16 v15, 0x2b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_GROCERY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 120
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_HEALTH_AND_FITNESS"

    const/16 v14, 0x2c

    const/16 v15, 0x2c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_HEALTH_AND_FITNESS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 125
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_ORGANIZATIONS"

    const/16 v14, 0x2d

    const/16 v15, 0x2d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_ORGANIZATIONS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 130
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_PAYMENTS"

    const/16 v14, 0x2e

    const/16 v15, 0x2e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_PAYMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 132
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_PROFESSIONAL_SERVICES"

    const/16 v14, 0x2f

    const/16 v15, 0x2f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_PROFESSIONAL_SERVICES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 134
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_RESTAURANT"

    const/16 v14, 0x30

    const/16 v15, 0x30

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_RESTAURANT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 136
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_RETAIL"

    const/16 v14, 0x31

    const/16 v15, 0x31

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_RETAIL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 141
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_SALON"

    const/16 v14, 0x32

    const/16 v15, 0x32

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_SALON:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 146
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_SPA"

    const/16 v14, 0x33

    const/16 v15, 0x33

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_SPA:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 151
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_TAXI"

    const/16 v14, 0x34

    const/16 v15, 0x34

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_TAXI:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 153
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_TOUCH_BISTRO"

    const/16 v14, 0x35

    const/16 v15, 0x35

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_TOUCH_BISTRO:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 155
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_VEND"

    const/16 v14, 0x36

    const/16 v15, 0x36

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_VEND:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 157
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_PRICING"

    const/16 v14, 0x37

    const/16 v15, 0x37

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_PRICING:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 159
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_R12"

    const/16 v14, 0x38

    const/16 v15, 0x38

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_R12:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 161
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_R4"

    const/16 v14, 0x39

    const/16 v15, 0x39

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_R4:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 163
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_REDEMPTION"

    const/16 v14, 0x3a

    const/16 v15, 0x3a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_REDEMPTION:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 165
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_SECURE"

    const/16 v14, 0x3b

    const/16 v15, 0x3b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_SECURE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 167
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_TOWN_SQUARE"

    const/16 v14, 0x3c

    const/16 v15, 0x3c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_TOWN_SQUARE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 172
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_UNDER_250"

    const/16 v14, 0x3d

    const/16 v15, 0x3d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_UNDER_250:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 174
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_VIRTUAL_TERMINAL"

    const/16 v14, 0x3e

    const/16 v15, 0x3e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_VIRTUAL_TERMINAL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 176
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_CAPITAL"

    const/16 v14, 0x3f

    const/16 v15, 0x3f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_CAPITAL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 178
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_DEVELOPERS"

    const/16 v14, 0x40

    const/16 v15, 0x40

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_DEVELOPERS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 183
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_PAYMENTS_ENTERTAINMENT"

    const/16 v14, 0x41

    const/16 v15, 0x41

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_PAYMENTS_ENTERTAINMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 185
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_PAYMENTS_ORGANIZATIONS"

    const/16 v14, 0x42

    const/16 v15, 0x42

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_PAYMENTS_ORGANIZATIONS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 187
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_BARS"

    const/16 v14, 0x43

    const/16 v15, 0x43

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_BARS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 189
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_HEALTH_AND_FITNESS_APOS"

    const/16 v14, 0x44

    const/16 v15, 0x44

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_HEALTH_AND_FITNESS_APOS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 191
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_HEALTH_AND_FITNESS_POS"

    const/16 v14, 0x45

    const/16 v15, 0x45

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_HEALTH_AND_FITNESS_POS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 193
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_INVENTORY_MANAGEMENT"

    const/16 v14, 0x46

    const/16 v15, 0x46

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_INVENTORY_MANAGEMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 198
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_LOCATION_MANAGEMENT"

    const/16 v14, 0x47

    const/16 v15, 0x47

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_LOCATION_MANAGEMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 200
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_QUICK_SERVICE"

    const/16 v14, 0x48

    const/16 v15, 0x48

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_QUICK_SERVICE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 202
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_QUICK_SERVICE_POS"

    const/16 v14, 0x49

    const/16 v15, 0x49

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_QUICK_SERVICE_POS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 204
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_QUICK_SERVICE_RPOS"

    const/16 v14, 0x4a

    const/16 v15, 0x4a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_QUICK_SERVICE_RPOS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 206
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_WEEBLY"

    const/16 v14, 0x4b

    const/16 v15, 0x4b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_WEEBLY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 208
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_PAYMENTS"

    const/16 v14, 0x4c

    const/16 v15, 0x4c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_PAYMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 210
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_GIFT_CARDS"

    const/16 v14, 0x4d

    const/16 v15, 0x4d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_GIFT_CARDS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 212
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_LOYALTY"

    const/16 v14, 0x4e

    const/16 v15, 0x4e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_LOYALTY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 214
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_MARKETING"

    const/16 v14, 0x4f

    const/16 v15, 0x4f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_MARKETING:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 216
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_POS_SPOS"

    const/16 v14, 0x50

    const/16 v15, 0x50

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_SPOS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 218
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_SQUARE_ONE_BUSINESS_MANAGEMENT"

    const/16 v14, 0x51

    const/16 v15, 0x51

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_SQUARE_ONE_BUSINESS_MANAGEMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 220
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_SQUARE_ONE_QUICK_SERVICE"

    const/16 v14, 0x52

    const/16 v15, 0x52

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_SQUARE_ONE_QUICK_SERVICE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 222
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_SQUARE_ONE_RETAIL"

    const/16 v14, 0x53

    const/16 v15, 0x53

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_SQUARE_ONE_RETAIL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 224
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_SQUARE_ONE_ROUTING"

    const/16 v14, 0x54

    const/16 v15, 0x54

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_SQUARE_ONE_ROUTING:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 226
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_SQUARE_ONE_SERVICES"

    const/16 v14, 0x55

    const/16 v15, 0x55

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_SQUARE_ONE_SERVICES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 228
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const-string v13, "OEP_ECOM_ONLINE_STORE"

    const/16 v14, 0x56

    const/16 v15, 0x56

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/precog/OnboardingEntryPoint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_ECOM_ONLINE_STORE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v0, 0x57

    new-array v0, v0, [Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 10
    sget-object v13, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_UNKNOWN:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_APPLE_PAY_PROMO:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_APPOINTMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_APP_MARKETPLACE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_CONTACT_SALES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_CUSTOMER_DIRECTORY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_CUSTOMER_ENGAGEMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_DEPOSITS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_ECOMMERCE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_EMPLOYEES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GIFT_CARDS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_APPLE_PAY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_CREDIT_CARDS_PROCESSING:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_GIFT_CARDS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_INVOICES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_LIABILITY_SHIFT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_MOBILE_PAYMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_NFC:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_PAYMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_PAYMENT_GATEWAY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_PCI:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_POS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_RESTAURANT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_RUN_BUSINESS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_SALON:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_SCHEDULING_SOFTWARE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_START_BUSINESS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_HOMEPAGE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_INVOICES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_LOGIN:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_ONLINE_STORE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_OVER_250:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_PAYROLL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_ANALYTICS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_APPAREL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_BAKERY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_BEAUTY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_COFFEE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_DASHBOARD:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_ENTERTAINMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_FOOD:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_FOOD_TRUCK:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_GROCERY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_HEALTH_AND_FITNESS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_ORGANIZATIONS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_PAYMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_PROFESSIONAL_SERVICES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_RESTAURANT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_RETAIL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_SALON:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_SPA:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_TAXI:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_TOUCH_BISTRO:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_VEND:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_PRICING:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_R12:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_R4:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_REDEMPTION:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_SECURE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_TOWN_SQUARE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_UNDER_250:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_VIRTUAL_TERMINAL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_CAPITAL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_DEVELOPERS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_PAYMENTS_ENTERTAINMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_PAYMENTS_ORGANIZATIONS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_BARS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_HEALTH_AND_FITNESS_APOS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_HEALTH_AND_FITNESS_POS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_INVENTORY_MANAGEMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_LOCATION_MANAGEMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_QUICK_SERVICE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_QUICK_SERVICE_POS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_QUICK_SERVICE_RPOS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_WEEBLY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_PAYMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_GIFT_CARDS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_LOYALTY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_MARKETING:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_SPOS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_SQUARE_ONE_BUSINESS_MANAGEMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_SQUARE_ONE_QUICK_SERVICE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_SQUARE_ONE_RETAIL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_SQUARE_ONE_ROUTING:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_SQUARE_ONE_SERVICES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_ECOM_ONLINE_STORE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->$VALUES:[Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 230
    new-instance v0, Lcom/squareup/protos/precog/OnboardingEntryPoint$ProtoAdapter_OnboardingEntryPoint;

    invoke-direct {v0}, Lcom/squareup/protos/precog/OnboardingEntryPoint$ProtoAdapter_OnboardingEntryPoint;-><init>()V

    sput-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 234
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 235
    iput p3, p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/precog/OnboardingEntryPoint;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 329
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_ECOM_ONLINE_STORE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 328
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_SQUARE_ONE_SERVICES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 327
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_SQUARE_ONE_ROUTING:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 326
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_SQUARE_ONE_RETAIL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 325
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_SQUARE_ONE_QUICK_SERVICE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 324
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_SQUARE_ONE_BUSINESS_MANAGEMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 323
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_SPOS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 322
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_MARKETING:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 321
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_LOYALTY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 320
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_GIFT_CARDS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 319
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_PAYMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 318
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_WEEBLY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 317
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_QUICK_SERVICE_RPOS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 316
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_QUICK_SERVICE_POS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 315
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_QUICK_SERVICE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 314
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_LOCATION_MANAGEMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 313
    :pswitch_10
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_INVENTORY_MANAGEMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 312
    :pswitch_11
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_HEALTH_AND_FITNESS_POS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 311
    :pswitch_12
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_HEALTH_AND_FITNESS_APOS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 310
    :pswitch_13
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_BARS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 309
    :pswitch_14
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_PAYMENTS_ORGANIZATIONS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 308
    :pswitch_15
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_PAYMENTS_ENTERTAINMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 307
    :pswitch_16
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_DEVELOPERS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 306
    :pswitch_17
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_CAPITAL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 305
    :pswitch_18
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_VIRTUAL_TERMINAL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 304
    :pswitch_19
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_UNDER_250:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 303
    :pswitch_1a
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_TOWN_SQUARE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 302
    :pswitch_1b
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_SECURE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 301
    :pswitch_1c
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_REDEMPTION:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 300
    :pswitch_1d
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_R4:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 299
    :pswitch_1e
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_R12:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 298
    :pswitch_1f
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_PRICING:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 297
    :pswitch_20
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_VEND:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 296
    :pswitch_21
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_TOUCH_BISTRO:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 295
    :pswitch_22
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_TAXI:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 294
    :pswitch_23
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_SPA:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 293
    :pswitch_24
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_SALON:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 292
    :pswitch_25
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_RETAIL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 291
    :pswitch_26
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_RESTAURANT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 290
    :pswitch_27
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_PROFESSIONAL_SERVICES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 289
    :pswitch_28
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_PAYMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 288
    :pswitch_29
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_ORGANIZATIONS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 287
    :pswitch_2a
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_HEALTH_AND_FITNESS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 286
    :pswitch_2b
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_GROCERY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 285
    :pswitch_2c
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_FOOD_TRUCK:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 284
    :pswitch_2d
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_FOOD:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 283
    :pswitch_2e
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_ENTERTAINMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 282
    :pswitch_2f
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_DASHBOARD:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 281
    :pswitch_30
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_COFFEE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 280
    :pswitch_31
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_BEAUTY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 279
    :pswitch_32
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_BAKERY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 278
    :pswitch_33
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_APPAREL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 277
    :pswitch_34
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS_ANALYTICS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 276
    :pswitch_35
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_POS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 275
    :pswitch_36
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_PAYROLL:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 274
    :pswitch_37
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_OVER_250:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 273
    :pswitch_38
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_ONLINE_STORE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 272
    :pswitch_39
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_LOGIN:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 271
    :pswitch_3a
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_INVOICES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 270
    :pswitch_3b
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_HOMEPAGE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 269
    :pswitch_3c
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_START_BUSINESS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 268
    :pswitch_3d
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_SCHEDULING_SOFTWARE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 267
    :pswitch_3e
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_SALON:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 266
    :pswitch_3f
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_RUN_BUSINESS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 265
    :pswitch_40
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_RESTAURANT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 264
    :pswitch_41
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_POS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 263
    :pswitch_42
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_PCI:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 262
    :pswitch_43
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_PAYMENT_GATEWAY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 261
    :pswitch_44
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_PAYMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 260
    :pswitch_45
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_NFC:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 259
    :pswitch_46
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_MOBILE_PAYMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 258
    :pswitch_47
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_LIABILITY_SHIFT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 257
    :pswitch_48
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_INVOICES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 256
    :pswitch_49
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_GIFT_CARDS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 255
    :pswitch_4a
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_CREDIT_CARDS_PROCESSING:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 254
    :pswitch_4b
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GUIDES_APPLE_PAY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 253
    :pswitch_4c
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_GIFT_CARDS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 252
    :pswitch_4d
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_EMPLOYEES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 251
    :pswitch_4e
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_ECOMMERCE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 250
    :pswitch_4f
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_DEPOSITS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 249
    :pswitch_50
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_CUSTOMER_ENGAGEMENT:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 248
    :pswitch_51
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_CUSTOMER_DIRECTORY:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 247
    :pswitch_52
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_CONTACT_SALES:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 246
    :pswitch_53
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_APP_MARKETPLACE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 245
    :pswitch_54
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_APPOINTMENTS:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 244
    :pswitch_55
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_APPLE_PAY_PROMO:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    .line 243
    :pswitch_56
    sget-object p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_UNKNOWN:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_56
        :pswitch_55
        :pswitch_54
        :pswitch_53
        :pswitch_52
        :pswitch_51
        :pswitch_50
        :pswitch_4f
        :pswitch_4e
        :pswitch_4d
        :pswitch_4c
        :pswitch_4b
        :pswitch_4a
        :pswitch_49
        :pswitch_48
        :pswitch_47
        :pswitch_46
        :pswitch_45
        :pswitch_44
        :pswitch_43
        :pswitch_42
        :pswitch_41
        :pswitch_40
        :pswitch_3f
        :pswitch_3e
        :pswitch_3d
        :pswitch_3c
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/precog/OnboardingEntryPoint;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/precog/OnboardingEntryPoint;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->$VALUES:[Lcom/squareup/protos/precog/OnboardingEntryPoint;

    invoke-virtual {v0}, [Lcom/squareup/protos/precog/OnboardingEntryPoint;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/precog/OnboardingEntryPoint;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 336
    iget v0, p0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->value:I

    return v0
.end method
