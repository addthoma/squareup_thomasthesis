.class public final enum Lcom/squareup/protos/client/Device$Product;
.super Ljava/lang/Enum;
.source "Device.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/Device;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Product"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/Device$Product$ProtoAdapter_Product;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/Device$Product;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/Device$Product;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/Device$Product;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum APPOINTMENTS:Lcom/squareup/protos/client/Device$Product;

.field public static final enum DASHBOARD:Lcom/squareup/protos/client/Device$Product;

.field public static final enum INVOICES:Lcom/squareup/protos/client/Device$Product;

.field public static final enum KDS:Lcom/squareup/protos/client/Device$Product;

.field public static final enum NONE:Lcom/squareup/protos/client/Device$Product;

.field public static final enum READERSDK:Lcom/squareup/protos/client/Device$Product;

.field public static final enum REGISTER:Lcom/squareup/protos/client/Device$Product;

.field public static final enum RESTAURANT:Lcom/squareup/protos/client/Device$Product;

.field public static final enum RETAIL:Lcom/squareup/protos/client/Device$Product;

.field public static final enum SIGNIN:Lcom/squareup/protos/client/Device$Product;

.field public static final enum SINGLE_BINARY:Lcom/squareup/protos/client/Device$Product;

.field public static final enum SQUARE:Lcom/squareup/protos/client/Device$Product;

.field public static final enum TERMINAL_API:Lcom/squareup/protos/client/Device$Product;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/Device$Product;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 20
    new-instance v0, Lcom/squareup/protos/client/Device$Product;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/Device$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Device$Product;->UNKNOWN:Lcom/squareup/protos/client/Device$Product;

    .line 22
    new-instance v0, Lcom/squareup/protos/client/Device$Product;

    const/4 v2, 0x1

    const-string v3, "NONE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/Device$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Device$Product;->NONE:Lcom/squareup/protos/client/Device$Product;

    .line 27
    new-instance v0, Lcom/squareup/protos/client/Device$Product;

    const/4 v3, 0x2

    const-string v4, "SINGLE_BINARY"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/Device$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Device$Product;->SINGLE_BINARY:Lcom/squareup/protos/client/Device$Product;

    .line 32
    new-instance v0, Lcom/squareup/protos/client/Device$Product;

    const/4 v4, 0x3

    const-string v5, "SQUARE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/Device$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Device$Product;->SQUARE:Lcom/squareup/protos/client/Device$Product;

    .line 37
    new-instance v0, Lcom/squareup/protos/client/Device$Product;

    const/4 v5, 0x4

    const-string v6, "REGISTER"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/Device$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Device$Product;->REGISTER:Lcom/squareup/protos/client/Device$Product;

    .line 39
    new-instance v0, Lcom/squareup/protos/client/Device$Product;

    const/4 v6, 0x5

    const-string v7, "KDS"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/Device$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Device$Product;->KDS:Lcom/squareup/protos/client/Device$Product;

    .line 41
    new-instance v0, Lcom/squareup/protos/client/Device$Product;

    const/4 v7, 0x6

    const-string v8, "RETAIL"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/Device$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Device$Product;->RETAIL:Lcom/squareup/protos/client/Device$Product;

    .line 43
    new-instance v0, Lcom/squareup/protos/client/Device$Product;

    const/4 v8, 0x7

    const-string v9, "RESTAURANT"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/Device$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Device$Product;->RESTAURANT:Lcom/squareup/protos/client/Device$Product;

    .line 45
    new-instance v0, Lcom/squareup/protos/client/Device$Product;

    const/16 v9, 0x8

    const-string v10, "DASHBOARD"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/Device$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Device$Product;->DASHBOARD:Lcom/squareup/protos/client/Device$Product;

    .line 47
    new-instance v0, Lcom/squareup/protos/client/Device$Product;

    const/16 v10, 0x9

    const-string v11, "APPOINTMENTS"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/Device$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Device$Product;->APPOINTMENTS:Lcom/squareup/protos/client/Device$Product;

    .line 49
    new-instance v0, Lcom/squareup/protos/client/Device$Product;

    const/16 v11, 0xa

    const-string v12, "INVOICES"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/client/Device$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Device$Product;->INVOICES:Lcom/squareup/protos/client/Device$Product;

    .line 51
    new-instance v0, Lcom/squareup/protos/client/Device$Product;

    const/16 v12, 0xb

    const-string v13, "READERSDK"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/client/Device$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Device$Product;->READERSDK:Lcom/squareup/protos/client/Device$Product;

    .line 53
    new-instance v0, Lcom/squareup/protos/client/Device$Product;

    const/16 v13, 0xc

    const-string v14, "SIGNIN"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/client/Device$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Device$Product;->SIGNIN:Lcom/squareup/protos/client/Device$Product;

    .line 55
    new-instance v0, Lcom/squareup/protos/client/Device$Product;

    const/16 v14, 0xd

    const-string v15, "TERMINAL_API"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/client/Device$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/Device$Product;->TERMINAL_API:Lcom/squareup/protos/client/Device$Product;

    const/16 v0, 0xe

    new-array v0, v0, [Lcom/squareup/protos/client/Device$Product;

    .line 19
    sget-object v15, Lcom/squareup/protos/client/Device$Product;->UNKNOWN:Lcom/squareup/protos/client/Device$Product;

    aput-object v15, v0, v1

    sget-object v1, Lcom/squareup/protos/client/Device$Product;->NONE:Lcom/squareup/protos/client/Device$Product;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/Device$Product;->SINGLE_BINARY:Lcom/squareup/protos/client/Device$Product;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/Device$Product;->SQUARE:Lcom/squareup/protos/client/Device$Product;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/Device$Product;->REGISTER:Lcom/squareup/protos/client/Device$Product;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/Device$Product;->KDS:Lcom/squareup/protos/client/Device$Product;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/Device$Product;->RETAIL:Lcom/squareup/protos/client/Device$Product;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/Device$Product;->RESTAURANT:Lcom/squareup/protos/client/Device$Product;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/Device$Product;->DASHBOARD:Lcom/squareup/protos/client/Device$Product;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/Device$Product;->APPOINTMENTS:Lcom/squareup/protos/client/Device$Product;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/Device$Product;->INVOICES:Lcom/squareup/protos/client/Device$Product;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/client/Device$Product;->READERSDK:Lcom/squareup/protos/client/Device$Product;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/client/Device$Product;->SIGNIN:Lcom/squareup/protos/client/Device$Product;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/protos/client/Device$Product;->TERMINAL_API:Lcom/squareup/protos/client/Device$Product;

    aput-object v1, v0, v14

    sput-object v0, Lcom/squareup/protos/client/Device$Product;->$VALUES:[Lcom/squareup/protos/client/Device$Product;

    .line 57
    new-instance v0, Lcom/squareup/protos/client/Device$Product$ProtoAdapter_Product;

    invoke-direct {v0}, Lcom/squareup/protos/client/Device$Product$ProtoAdapter_Product;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/Device$Product;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 62
    iput p3, p0, Lcom/squareup/protos/client/Device$Product;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/Device$Product;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 83
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/Device$Product;->TERMINAL_API:Lcom/squareup/protos/client/Device$Product;

    return-object p0

    .line 82
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/Device$Product;->SIGNIN:Lcom/squareup/protos/client/Device$Product;

    return-object p0

    .line 81
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/Device$Product;->READERSDK:Lcom/squareup/protos/client/Device$Product;

    return-object p0

    .line 80
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/Device$Product;->INVOICES:Lcom/squareup/protos/client/Device$Product;

    return-object p0

    .line 79
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/Device$Product;->APPOINTMENTS:Lcom/squareup/protos/client/Device$Product;

    return-object p0

    .line 78
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/Device$Product;->DASHBOARD:Lcom/squareup/protos/client/Device$Product;

    return-object p0

    .line 77
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/Device$Product;->RESTAURANT:Lcom/squareup/protos/client/Device$Product;

    return-object p0

    .line 76
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/Device$Product;->RETAIL:Lcom/squareup/protos/client/Device$Product;

    return-object p0

    .line 75
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/Device$Product;->KDS:Lcom/squareup/protos/client/Device$Product;

    return-object p0

    .line 74
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/Device$Product;->REGISTER:Lcom/squareup/protos/client/Device$Product;

    return-object p0

    .line 73
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/Device$Product;->SQUARE:Lcom/squareup/protos/client/Device$Product;

    return-object p0

    .line 72
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/client/Device$Product;->SINGLE_BINARY:Lcom/squareup/protos/client/Device$Product;

    return-object p0

    .line 71
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/client/Device$Product;->NONE:Lcom/squareup/protos/client/Device$Product;

    return-object p0

    .line 70
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/client/Device$Product;->UNKNOWN:Lcom/squareup/protos/client/Device$Product;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/Device$Product;
    .locals 1

    .line 19
    const-class v0, Lcom/squareup/protos/client/Device$Product;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/Device$Product;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/Device$Product;
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/protos/client/Device$Product;->$VALUES:[Lcom/squareup/protos/client/Device$Product;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/Device$Product;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/Device$Product;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 90
    iget v0, p0, Lcom/squareup/protos/client/Device$Product;->value:I

    return v0
.end method
