.class public final Lcom/squareup/protos/client/settlements/SettlementReportLite$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SettlementReportLite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettlementReportLite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/settlements/SettlementReportLite;",
        "Lcom/squareup/protos/client/settlements/SettlementReportLite$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public created_at:Lcom/squareup/protos/common/time/DateTime;

.field public settlement_money:Lcom/squareup/protos/common/Money;

.field public settlement_type:Lcom/squareup/protos/client/settlements/SettlementType;

.field public success:Ljava/lang/Boolean;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 155
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/settlements/SettlementReportLite;
    .locals 8

    .line 200
    new-instance v7, Lcom/squareup/protos/client/settlements/SettlementReportLite;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLite$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/settlements/SettlementReportLite$Builder;->settlement_type:Lcom/squareup/protos/client/settlements/SettlementType;

    iget-object v3, p0, Lcom/squareup/protos/client/settlements/SettlementReportLite$Builder;->settlement_money:Lcom/squareup/protos/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/client/settlements/SettlementReportLite$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v5, p0, Lcom/squareup/protos/client/settlements/SettlementReportLite$Builder;->success:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/settlements/SettlementReportLite;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/settlements/SettlementType;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 144
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReportLite$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportLite;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/settlements/SettlementReportLite$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLite$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public settlement_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/settlements/SettlementReportLite$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLite$Builder;->settlement_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public settlement_type(Lcom/squareup/protos/client/settlements/SettlementType;)Lcom/squareup/protos/client/settlements/SettlementReportLite$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLite$Builder;->settlement_type:Lcom/squareup/protos/client/settlements/SettlementType;

    return-object p0
.end method

.method public success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/settlements/SettlementReportLite$Builder;
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLite$Builder;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/SettlementReportLite$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLite$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
