.class public final Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;
.super Lcom/squareup/wire/Message;
.source "SettlementReportLiteResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$ProtoAdapter_SettlementReportLiteResponse;,
        Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;,
        Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final batch_response:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.settlements.SettlementReportLiteResponse$BatchResponse#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final settlements:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.settlements.SettlementReportLite#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettlementReportLite;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$ProtoAdapter_SettlementReportLiteResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$ProtoAdapter_SettlementReportLiteResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettlementReportLite;",
            ">;",
            "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;",
            ")V"
        }
    .end annotation

    .line 43
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettlementReportLite;",
            ">;",
            "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 48
    sget-object v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p3, "settlements"

    .line 49
    invoke-static {p3, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->settlements:Ljava/util/List;

    .line 50
    iput-object p2, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->batch_response:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 65
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 66
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    .line 67
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->settlements:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->settlements:Ljava/util/List;

    .line 68
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->batch_response:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->batch_response:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    .line 69
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 74
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->settlements:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->batch_response:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 79
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;
    .locals 2

    .line 55
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;-><init>()V

    .line 56
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->settlements:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->settlements:Ljava/util/List;

    .line 57
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->batch_response:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    iput-object v1, v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->batch_response:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    .line 58
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->newBuilder()Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->settlements:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", settlements="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->settlements:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->batch_response:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    if-eqz v1, :cond_1

    const-string v1, ", batch_response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;->batch_response:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SettlementReportLiteResponse{"

    .line 89
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
