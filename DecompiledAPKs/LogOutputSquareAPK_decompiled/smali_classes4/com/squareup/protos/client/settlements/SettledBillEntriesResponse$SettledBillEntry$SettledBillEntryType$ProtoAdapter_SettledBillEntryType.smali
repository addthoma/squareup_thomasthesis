.class final Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType$ProtoAdapter_SettledBillEntryType;
.super Lcom/squareup/wire/EnumAdapter;
.source "SettledBillEntriesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SettledBillEntryType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 350
    const-class v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;
    .locals 0

    .line 355
    invoke-static {p1}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;->fromValue(I)Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 348
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType$ProtoAdapter_SettledBillEntryType;->fromValue(I)Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry$SettledBillEntryType;

    move-result-object p1

    return-object p1
.end method
