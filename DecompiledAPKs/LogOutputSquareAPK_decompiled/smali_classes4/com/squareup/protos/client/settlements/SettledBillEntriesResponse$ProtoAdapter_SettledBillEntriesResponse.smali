.class final Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$ProtoAdapter_SettledBillEntriesResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SettledBillEntriesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SettledBillEntriesResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 431
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 448
    new-instance v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$Builder;-><init>()V

    .line 449
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 450
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 454
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 452
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$Builder;->settled_bill_entry:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 458
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 459
    invoke-virtual {v0}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$Builder;->build()Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 429
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$ProtoAdapter_SettledBillEntriesResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 442
    sget-object v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;->settled_bill_entry:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 443
    invoke-virtual {p2}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 429
    check-cast p2, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$ProtoAdapter_SettledBillEntriesResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;)I
    .locals 3

    .line 436
    sget-object v0, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;->settled_bill_entry:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 437
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 429
    check-cast p1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$ProtoAdapter_SettledBillEntriesResponse;->encodedSize(Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;)Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;
    .locals 2

    .line 464
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;->newBuilder()Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$Builder;

    move-result-object p1

    .line 465
    iget-object v0, p1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$Builder;->settled_bill_entry:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$SettledBillEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 466
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 467
    invoke-virtual {p1}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$Builder;->build()Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 429
    check-cast p1, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse$ProtoAdapter_SettledBillEntriesResponse;->redact(Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;)Lcom/squareup/protos/client/settlements/SettledBillEntriesResponse;

    move-result-object p1

    return-object p1
.end method
