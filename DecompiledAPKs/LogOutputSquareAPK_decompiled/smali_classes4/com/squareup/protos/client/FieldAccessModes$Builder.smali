.class public final Lcom/squareup/protos/client/FieldAccessModes$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FieldAccessModes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/FieldAccessModes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/FieldAccessModes;",
        "Lcom/squareup/protos/client/FieldAccessModes$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public read:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/Flow;",
            ">;"
        }
    .end annotation
.end field

.field public write:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/Flow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 102
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 103
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/FieldAccessModes$Builder;->read:Ljava/util/List;

    .line 104
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/FieldAccessModes$Builder;->write:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/FieldAccessModes;
    .locals 4

    .line 127
    new-instance v0, Lcom/squareup/protos/client/FieldAccessModes;

    iget-object v1, p0, Lcom/squareup/protos/client/FieldAccessModes$Builder;->read:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/FieldAccessModes$Builder;->write:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/FieldAccessModes;-><init>(Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/client/FieldAccessModes$Builder;->build()Lcom/squareup/protos/client/FieldAccessModes;

    move-result-object v0

    return-object v0
.end method

.method public read(Ljava/util/List;)Lcom/squareup/protos/client/FieldAccessModes$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/Flow;",
            ">;)",
            "Lcom/squareup/protos/client/FieldAccessModes$Builder;"
        }
    .end annotation

    .line 111
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 112
    iput-object p1, p0, Lcom/squareup/protos/client/FieldAccessModes$Builder;->read:Ljava/util/List;

    return-object p0
.end method

.method public write(Ljava/util/List;)Lcom/squareup/protos/client/FieldAccessModes$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/Flow;",
            ">;)",
            "Lcom/squareup/protos/client/FieldAccessModes$Builder;"
        }
    .end annotation

    .line 120
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/client/FieldAccessModes$Builder;->write:Ljava/util/List;

    return-object p0
.end method
