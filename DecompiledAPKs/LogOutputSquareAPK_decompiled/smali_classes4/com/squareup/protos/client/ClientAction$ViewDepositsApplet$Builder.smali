.class public final Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ClientAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;",
        "Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet$Builder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2588
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;
    .locals 2

    .line 2593
    new-instance v0, Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;-><init>(Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2587
    invoke-virtual {p0}, Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    move-result-object v0

    return-object v0
.end method
