.class public final Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;
.super Lcom/squareup/wire/Message;
.source "DiagnosticReportRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$ProtoAdapter_DiagnosticReportRequest;,
        Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;,
        Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;,
        Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Destination;,
        Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;",
        "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_NUM_XMIT_RETRIES:Ljava/lang/Integer;

.field public static final DEFAULT_PRIMARY_UUID:Ljava/lang/String; = ""

.field public static final DEFAULT_REPORTER:Ljava/lang/String; = ""

.field public static final DEFAULT_REPORT_GENERATED_LOCALTIME_SEC:Ljava/lang/Long;

.field public static final DEFAULT_REPORT_TRIGGERED_LOCALTIME_SEC:Ljava/lang/Long;

.field public static final DEFAULT_SOURCE:Ljava/lang/String; = ""

.field public static final DEFAULT_TITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_UUID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final destinations:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tarkin.DiagnosticReportRequest$Destination#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xd
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Destination;",
            ">;"
        }
    .end annotation
.end field

.field public final device_metadata:Lcom/squareup/protos/client/tarkin/SquidProductManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tarkin.SquidProductManifest#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final links:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tarkin.DiagnosticReportRequest$Item#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xc
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;",
            ">;"
        }
    .end annotation
.end field

.field public final num_xmit_retries:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x7
    .end annotation
.end field

.field public final primary_uuid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final report_generated_localtime_sec:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x9
    .end annotation
.end field

.field public final report_triggered_localtime_sec:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x8
    .end annotation
.end field

.field public final reporter:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xe
    .end annotation
.end field

.field public final source:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final uuid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final zip:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tarkin.DiagnosticReportRequest$Zip#ADAPTER"
        tag = 0xb
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 25
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$ProtoAdapter_DiagnosticReportRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$ProtoAdapter_DiagnosticReportRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 39
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->DEFAULT_NUM_XMIT_RETRIES:Ljava/lang/Integer;

    const-wide/16 v0, 0x0

    .line 41
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->DEFAULT_REPORT_TRIGGERED_LOCALTIME_SEC:Ljava/lang/Long;

    .line 43
    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->DEFAULT_REPORT_GENERATED_LOCALTIME_SEC:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/tarkin/SquidProductManifest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/tarkin/SquidProductManifest;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Destination;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 171
    sget-object v14, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/tarkin/SquidProductManifest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/tarkin/SquidProductManifest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/tarkin/SquidProductManifest;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Destination;",
            ">;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 179
    sget-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p14}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 180
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->uuid:Ljava/lang/String;

    .line 181
    iput-object p2, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->primary_uuid:Ljava/lang/String;

    .line 182
    iput-object p3, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->device_metadata:Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    .line 183
    iput-object p4, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->source:Ljava/lang/String;

    .line 184
    iput-object p5, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->title:Ljava/lang/String;

    .line 185
    iput-object p6, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->description:Ljava/lang/String;

    .line 186
    iput-object p7, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->num_xmit_retries:Ljava/lang/Integer;

    .line 187
    iput-object p8, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->report_triggered_localtime_sec:Ljava/lang/Long;

    .line 188
    iput-object p9, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->report_generated_localtime_sec:Ljava/lang/Long;

    .line 189
    iput-object p10, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->zip:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    const-string p1, "links"

    .line 190
    invoke-static {p1, p11}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->links:Ljava/util/List;

    const-string p1, "destinations"

    .line 191
    invoke-static {p1, p12}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->destinations:Ljava/util/List;

    .line 192
    iput-object p13, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->reporter:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 218
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 219
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;

    .line 220
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->uuid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->uuid:Ljava/lang/String;

    .line 221
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->primary_uuid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->primary_uuid:Ljava/lang/String;

    .line 222
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->device_metadata:Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->device_metadata:Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    .line 223
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->source:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->source:Ljava/lang/String;

    .line 224
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->title:Ljava/lang/String;

    .line 225
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->description:Ljava/lang/String;

    .line 226
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->num_xmit_retries:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->num_xmit_retries:Ljava/lang/Integer;

    .line 227
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->report_triggered_localtime_sec:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->report_triggered_localtime_sec:Ljava/lang/Long;

    .line 228
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->report_generated_localtime_sec:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->report_generated_localtime_sec:Ljava/lang/Long;

    .line 229
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->zip:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->zip:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    .line 230
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->links:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->links:Ljava/util/List;

    .line 231
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->destinations:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->destinations:Ljava/util/List;

    .line 232
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->reporter:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->reporter:Ljava/lang/String;

    .line 233
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 238
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_b

    .line 240
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 241
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->uuid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 242
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->primary_uuid:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 243
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->device_metadata:Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/tarkin/SquidProductManifest;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->source:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 245
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->title:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 246
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->description:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 247
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->num_xmit_retries:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 248
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->report_triggered_localtime_sec:Ljava/lang/Long;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 249
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->report_generated_localtime_sec:Ljava/lang/Long;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 250
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->zip:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 251
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->links:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 252
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->destinations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 253
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->reporter:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_a
    add-int/2addr v0, v2

    .line 254
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_b
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;
    .locals 2

    .line 197
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;-><init>()V

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->uuid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->uuid:Ljava/lang/String;

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->primary_uuid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->primary_uuid:Ljava/lang/String;

    .line 200
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->device_metadata:Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->device_metadata:Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->source:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->source:Ljava/lang/String;

    .line 202
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->title:Ljava/lang/String;

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->description:Ljava/lang/String;

    .line 204
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->num_xmit_retries:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->num_xmit_retries:Ljava/lang/Integer;

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->report_triggered_localtime_sec:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->report_triggered_localtime_sec:Ljava/lang/Long;

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->report_generated_localtime_sec:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->report_generated_localtime_sec:Ljava/lang/Long;

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->zip:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->zip:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->links:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->links:Ljava/util/List;

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->destinations:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->destinations:Ljava/util/List;

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->reporter:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->reporter:Ljava/lang/String;

    .line 211
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->newBuilder()Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 262
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->uuid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->uuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->primary_uuid:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", primary_uuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->primary_uuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->device_metadata:Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    if-eqz v1, :cond_2

    const-string v1, ", device_metadata="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->device_metadata:Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 265
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->source:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", source="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->source:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->title:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->description:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->num_xmit_retries:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    const-string v1, ", num_xmit_retries="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->num_xmit_retries:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 269
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->report_triggered_localtime_sec:Ljava/lang/Long;

    if-eqz v1, :cond_7

    const-string v1, ", report_triggered_localtime_sec="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->report_triggered_localtime_sec:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 270
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->report_generated_localtime_sec:Ljava/lang/Long;

    if-eqz v1, :cond_8

    const-string v1, ", report_generated_localtime_sec="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->report_generated_localtime_sec:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 271
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->zip:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    if-eqz v1, :cond_9

    const-string v1, ", zip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->zip:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Zip;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 272
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->links:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, ", links="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->links:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 273
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->destinations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, ", destinations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->destinations:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 274
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->reporter:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", reporter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest;->reporter:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_c
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DiagnosticReportRequest{"

    .line 275
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
