.class public final Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;
.super Lcom/squareup/wire/Message;
.source "AssetUpdateRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CommsProtocolVersion"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$ProtoAdapter_CommsProtocolVersion;,
        Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;",
        "Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APP:Ljava/lang/Integer;

.field public static final DEFAULT_EP:Ljava/lang/Integer;

.field public static final DEFAULT_TRANSPORT:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final app:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x2
    .end annotation
.end field

.field public final ep:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x3
    .end annotation
.end field

.field public final transport:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 143
    new-instance v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$ProtoAdapter_CommsProtocolVersion;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$ProtoAdapter_CommsProtocolVersion;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 147
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->DEFAULT_TRANSPORT:Ljava/lang/Integer;

    .line 149
    sput-object v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->DEFAULT_APP:Ljava/lang/Integer;

    .line 151
    sput-object v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->DEFAULT_EP:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1

    .line 172
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 177
    sget-object v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->transport:Ljava/lang/Integer;

    .line 179
    iput-object p2, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->app:Ljava/lang/Integer;

    .line 180
    iput-object p3, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->ep:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 196
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 197
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    .line 198
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->transport:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->transport:Ljava/lang/Integer;

    .line 199
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->app:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->app:Ljava/lang/Integer;

    .line 200
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->ep:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->ep:Ljava/lang/Integer;

    .line 201
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 206
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 208
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->transport:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->app:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->ep:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 212
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;
    .locals 2

    .line 185
    new-instance v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;-><init>()V

    .line 186
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->transport:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->transport:Ljava/lang/Integer;

    .line 187
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->app:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->app:Ljava/lang/Integer;

    .line 188
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->ep:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->ep:Ljava/lang/Integer;

    .line 189
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 142
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->newBuilder()Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 219
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 220
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->transport:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", transport="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->transport:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 221
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->app:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", app="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->app:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 222
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->ep:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", ep="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;->ep:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CommsProtocolVersion{"

    .line 223
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
