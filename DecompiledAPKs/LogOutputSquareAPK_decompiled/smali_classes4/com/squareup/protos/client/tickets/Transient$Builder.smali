.class public final Lcom/squareup/protos/client/tickets/Transient$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Transient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tickets/Transient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tickets/Transient;",
        "Lcom/squareup/protos/client/tickets/Transient$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public coupon:Lcom/squareup/protos/client/coupons/Coupon;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 82
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/tickets/Transient;
    .locals 3

    .line 92
    new-instance v0, Lcom/squareup/protos/client/tickets/Transient;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/Transient$Builder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/tickets/Transient;-><init>(Lcom/squareup/protos/client/coupons/Coupon;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/Transient$Builder;->build()Lcom/squareup/protos/client/tickets/Transient;

    move-result-object v0

    return-object v0
.end method

.method public coupon(Lcom/squareup/protos/client/coupons/Coupon;)Lcom/squareup/protos/client/tickets/Transient$Builder;
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/Transient$Builder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    return-object p0
.end method
