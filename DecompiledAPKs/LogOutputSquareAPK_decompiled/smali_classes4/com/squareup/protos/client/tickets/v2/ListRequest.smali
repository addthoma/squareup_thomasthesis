.class public final Lcom/squareup/protos/client/tickets/v2/ListRequest;
.super Lcom/squareup/wire/Message;
.source "ListRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/tickets/v2/ListRequest$ProtoAdapter_ListRequest;,
        Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/tickets/v2/ListRequest;",
        "Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/tickets/v2/ListRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DATA_CENTER_HINT:Ljava/lang/String; = ""

.field public static final DEFAULT_INCLUDE_OMITTED_OPEN_TICKETS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final data_center_hint:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final include_omitted_open_tickets:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final known_ticket_info:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tickets.v2.TicketInfo#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tickets/v2/TicketInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/tickets/v2/ListRequest$ProtoAdapter_ListRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/tickets/v2/ListRequest$ProtoAdapter_ListRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 26
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->DEFAULT_INCLUDE_OMITTED_OPEN_TICKETS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tickets/v2/TicketInfo;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 71
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/tickets/v2/ListRequest;-><init>(Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/tickets/v2/TicketInfo;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 76
    sget-object v0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p4, "known_ticket_info"

    .line 77
    invoke-static {p4, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->known_ticket_info:Ljava/util/List;

    .line 78
    iput-object p2, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->include_omitted_open_tickets:Ljava/lang/Boolean;

    .line 79
    iput-object p3, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->data_center_hint:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 95
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/tickets/v2/ListRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 96
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/tickets/v2/ListRequest;

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/v2/ListRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/v2/ListRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->known_ticket_info:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/tickets/v2/ListRequest;->known_ticket_info:Ljava/util/List;

    .line 98
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->include_omitted_open_tickets:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/tickets/v2/ListRequest;->include_omitted_open_tickets:Ljava/lang/Boolean;

    .line 99
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->data_center_hint:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/tickets/v2/ListRequest;->data_center_hint:Ljava/lang/String;

    .line 100
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 105
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/v2/ListRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->known_ticket_info:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->include_omitted_open_tickets:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->data_center_hint:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 111
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;
    .locals 2

    .line 84
    new-instance v0, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;-><init>()V

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->known_ticket_info:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;->known_ticket_info:Ljava/util/List;

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->include_omitted_open_tickets:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;->include_omitted_open_tickets:Ljava/lang/Boolean;

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->data_center_hint:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;->data_center_hint:Ljava/lang/String;

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/v2/ListRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/v2/ListRequest;->newBuilder()Lcom/squareup/protos/client/tickets/v2/ListRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->known_ticket_info:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", known_ticket_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->known_ticket_info:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 120
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->include_omitted_open_tickets:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", include_omitted_open_tickets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->include_omitted_open_tickets:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 121
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->data_center_hint:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", data_center_hint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/v2/ListRequest;->data_center_hint:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ListRequest{"

    .line 122
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
