.class public final Lcom/squareup/protos/client/timecards/SwitchJobsResponse;
.super Lcom/squareup/wire/Message;
.source "SwitchJobsResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/timecards/SwitchJobsResponse$ProtoAdapter_SwitchJobsResponse;,
        Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/timecards/SwitchJobsResponse;",
        "Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/timecards/SwitchJobsResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_VALID:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final employee_job_infos:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.employeejobs.EmployeeJobInfo#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final new_timecard:Lcom/squareup/protos/client/timecards/Timecard;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.Timecard#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final old_timecard:Lcom/squareup/protos/client/timecards/Timecard;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.Timecard#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final old_timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.TimecardBreak#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final valid:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$ProtoAdapter_SwitchJobsResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$ProtoAdapter_SwitchJobsResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->DEFAULT_VALID:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/util/List;Ljava/lang/Boolean;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/timecards/Timecard;",
            "Lcom/squareup/protos/client/timecards/Timecard;",
            "Lcom/squareup/protos/client/timecards/TimecardBreak;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 75
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;-><init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/util/List;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/util/List;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/timecards/Timecard;",
            "Lcom/squareup/protos/client/timecards/Timecard;",
            "Lcom/squareup/protos/client/timecards/TimecardBreak;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 81
    sget-object v0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 82
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->new_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 83
    iput-object p2, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->old_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 84
    iput-object p3, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->old_timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    const-string p1, "employee_job_infos"

    .line 85
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->employee_job_infos:Ljava/util/List;

    .line 86
    iput-object p5, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->valid:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 104
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 105
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->new_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->new_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 107
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->old_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->old_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 108
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->old_timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->old_timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    .line 109
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->employee_job_infos:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->employee_job_infos:Ljava/util/List;

    .line 110
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->valid:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->valid:Ljava/lang/Boolean;

    .line 111
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 116
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->new_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/Timecard;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->old_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/Timecard;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->old_timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/TimecardBreak;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->employee_job_infos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->valid:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 124
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;
    .locals 2

    .line 91
    new-instance v0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;-><init>()V

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->new_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;->new_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->old_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;->old_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->old_timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;->old_timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->employee_job_infos:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;->employee_job_infos:Ljava/util/List;

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->valid:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;->valid:Ljava/lang/Boolean;

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->newBuilder()Lcom/squareup/protos/client/timecards/SwitchJobsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->new_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-eqz v1, :cond_0

    const-string v1, ", new_timecard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->new_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 133
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->old_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-eqz v1, :cond_1

    const-string v1, ", old_timecard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->old_timecard:Lcom/squareup/protos/client/timecards/Timecard;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 134
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->old_timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    if-eqz v1, :cond_2

    const-string v1, ", old_timecard_break="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->old_timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 135
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->employee_job_infos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", employee_job_infos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->employee_job_infos:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 136
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->valid:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", valid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SwitchJobsResponse;->valid:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SwitchJobsResponse{"

    .line 137
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
