.class final Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$ProtoAdapter_ListTimecardBreakDefinitionsResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ListTimecardBreakDefinitionsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ListTimecardBreakDefinitionsResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 100
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 118
    new-instance v0, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$Builder;-><init>()V

    .line 119
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 120
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 124
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 122
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$Builder;->timecard_break_definition:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 128
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 129
    invoke-virtual {v0}, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$Builder;->build()Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 98
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$ProtoAdapter_ListTimecardBreakDefinitionsResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 112
    sget-object v0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;->timecard_break_definition:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 113
    invoke-virtual {p2}, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 98
    check-cast p2, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$ProtoAdapter_ListTimecardBreakDefinitionsResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;)I
    .locals 3

    .line 105
    sget-object v0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;->timecard_break_definition:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 106
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 98
    check-cast p1, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$ProtoAdapter_ListTimecardBreakDefinitionsResponse;->encodedSize(Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;)Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;
    .locals 2

    .line 134
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;->newBuilder()Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$Builder;

    move-result-object p1

    .line 135
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$Builder;->timecard_break_definition:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 136
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 137
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$Builder;->build()Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 98
    check-cast p1, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$ProtoAdapter_ListTimecardBreakDefinitionsResponse;->redact(Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;)Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;

    move-result-object p1

    return-object p1
.end method
