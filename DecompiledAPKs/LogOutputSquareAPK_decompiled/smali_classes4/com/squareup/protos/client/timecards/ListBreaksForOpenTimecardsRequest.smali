.class public final Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;
.super Lcom/squareup/wire/Message;
.source "ListBreaksForOpenTimecardsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$ProtoAdapter_ListBreaksForOpenTimecardsRequest;,
        Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;",
        "Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EMPLOYEE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_INCLUDE_CLOSED_BREAKS:Ljava/lang/Boolean;

.field public static final DEFAULT_LIMIT:Ljava/lang/Integer;

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_PAGINATION_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_UNIT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final employee_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final include_closed_breaks:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final limit:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final pagination_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 25
    new-instance v0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$ProtoAdapter_ListBreaksForOpenTimecardsRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$ProtoAdapter_ListBreaksForOpenTimecardsRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 35
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->DEFAULT_LIMIT:Ljava/lang/Integer;

    .line 39
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->DEFAULT_INCLUDE_CLOSED_BREAKS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 8

    .line 85
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 91
    sget-object v0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 92
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->merchant_token:Ljava/lang/String;

    .line 93
    iput-object p2, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->unit_token:Ljava/lang/String;

    .line 94
    iput-object p3, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->employee_token:Ljava/lang/String;

    .line 95
    iput-object p4, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->limit:Ljava/lang/Integer;

    .line 96
    iput-object p5, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->pagination_token:Ljava/lang/String;

    .line 97
    iput-object p6, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->include_closed_breaks:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 116
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 117
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->merchant_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->merchant_token:Ljava/lang/String;

    .line 119
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->unit_token:Ljava/lang/String;

    .line 120
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->employee_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->employee_token:Ljava/lang/String;

    .line 121
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->limit:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->limit:Ljava/lang/Integer;

    .line 122
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->pagination_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->pagination_token:Ljava/lang/String;

    .line 123
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->include_closed_breaks:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->include_closed_breaks:Ljava/lang/Boolean;

    .line 124
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 129
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 131
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->employee_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->limit:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->pagination_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->include_closed_breaks:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 138
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;
    .locals 2

    .line 102
    new-instance v0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;-><init>()V

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->merchant_token:Ljava/lang/String;

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->unit_token:Ljava/lang/String;

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->employee_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->employee_token:Ljava/lang/String;

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->limit:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->limit:Ljava/lang/Integer;

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->pagination_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->pagination_token:Ljava/lang/String;

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->include_closed_breaks:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->include_closed_breaks:Ljava/lang/Boolean;

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->newBuilder()Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->employee_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", employee_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->employee_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->limit:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->limit:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 150
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->pagination_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", pagination_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->pagination_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->include_closed_breaks:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", include_closed_breaks="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListBreaksForOpenTimecardsRequest;->include_closed_breaks:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ListBreaksForOpenTimecardsRequest{"

    .line 152
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
