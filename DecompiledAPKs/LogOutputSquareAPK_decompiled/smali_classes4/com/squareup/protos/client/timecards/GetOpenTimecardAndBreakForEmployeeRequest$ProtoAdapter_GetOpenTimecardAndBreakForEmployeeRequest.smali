.class final Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$ProtoAdapter_GetOpenTimecardAndBreakForEmployeeRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetOpenTimecardAndBreakForEmployeeRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetOpenTimecardAndBreakForEmployeeRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 96
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 114
    new-instance v0, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;-><init>()V

    .line 115
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 116
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 120
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 118
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;

    goto :goto_0

    .line 124
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 125
    invoke-virtual {v0}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;->build()Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 94
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$ProtoAdapter_GetOpenTimecardAndBreakForEmployeeRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 108
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;->employee_token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 109
    invoke-virtual {p2}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 94
    check-cast p2, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$ProtoAdapter_GetOpenTimecardAndBreakForEmployeeRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;)I
    .locals 3

    .line 101
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;->employee_token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 102
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 94
    check-cast p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$ProtoAdapter_GetOpenTimecardAndBreakForEmployeeRequest;->encodedSize(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;
    .locals 0

    .line 131
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;->newBuilder()Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;

    move-result-object p1

    .line 132
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 133
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$Builder;->build()Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 94
    check-cast p1, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest$ProtoAdapter_GetOpenTimecardAndBreakForEmployeeRequest;->redact(Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;)Lcom/squareup/protos/client/timecards/GetOpenTimecardAndBreakForEmployeeRequest;

    move-result-object p1

    return-object p1
.end method
