.class public final Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;
.super Lcom/squareup/wire/Message;
.source "StopTimecardBreakResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$ProtoAdapter_StopTimecardBreakResponse;,
        Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;",
        "Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_VALID:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final timecard:Lcom/squareup/protos/client/timecards/Timecard;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.Timecard#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.TimecardBreak#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final valid:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$ProtoAdapter_StopTimecardBreakResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$ProtoAdapter_StopTimecardBreakResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 25
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->DEFAULT_VALID:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/lang/Boolean;)V
    .locals 1

    .line 46
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;-><init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 51
    sget-object v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 52
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 53
    iput-object p2, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    .line 54
    iput-object p3, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->valid:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 70
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 71
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;

    .line 72
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 73
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    .line 74
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->valid:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->valid:Ljava/lang/Boolean;

    .line 75
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 80
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/Timecard;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/TimecardBreak;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->valid:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 86
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;
    .locals 2

    .line 59
    new-instance v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;-><init>()V

    .line 60
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 61
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    .line 62
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->valid:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;->valid:Ljava/lang/Boolean;

    .line 63
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->newBuilder()Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-eqz v1, :cond_0

    const-string v1, ", timecard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 95
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    if-eqz v1, :cond_1

    const-string v1, ", timecard_break="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 96
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->valid:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", valid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;->valid:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "StopTimecardBreakResponse{"

    .line 97
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
