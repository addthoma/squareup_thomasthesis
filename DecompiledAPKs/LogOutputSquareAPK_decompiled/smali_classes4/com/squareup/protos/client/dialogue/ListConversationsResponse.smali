.class public final Lcom/squareup/protos/client/dialogue/ListConversationsResponse;
.super Lcom/squareup/wire/Message;
.source "ListConversationsResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/dialogue/ListConversationsResponse$ProtoAdapter_ListConversationsResponse;,
        Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/dialogue/ListConversationsResponse;",
        "Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/dialogue/ListConversationsResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PAGING_KEY:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final items:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.dialogue.ConversationListItem#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/dialogue/ConversationListItem;",
            ">;"
        }
    .end annotation
.end field

.field public final list_options:Lcom/squareup/protos/client/dialogue/ListOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.dialogue.ListOptions#ADAPTER"
        tag = 0x3
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final paging_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$ProtoAdapter_ListConversationsResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$ProtoAdapter_ListConversationsResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lcom/squareup/protos/client/dialogue/ListOptions;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/dialogue/ConversationListItem;",
            ">;",
            "Lcom/squareup/protos/client/dialogue/ListOptions;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 63
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lcom/squareup/protos/client/dialogue/ListOptions;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lcom/squareup/protos/client/dialogue/ListOptions;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/dialogue/ConversationListItem;",
            ">;",
            "Lcom/squareup/protos/client/dialogue/ListOptions;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 68
    sget-object v0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 69
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->status:Lcom/squareup/protos/client/Status;

    const-string p1, "items"

    .line 70
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->items:Ljava/util/List;

    .line 71
    iput-object p3, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    .line 72
    iput-object p4, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->paging_key:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 89
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 90
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->status:Lcom/squareup/protos/client/Status;

    .line 92
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->items:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->items:Ljava/util/List;

    .line 93
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    iget-object v3, p1, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    .line 94
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->paging_key:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->paging_key:Ljava/lang/String;

    .line 95
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 100
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/dialogue/ListOptions;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->paging_key:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 107
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;
    .locals 2

    .line 77
    new-instance v0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;-><init>()V

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 79
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->items:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;->items:Ljava/util/List;

    .line 80
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->paging_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;->paging_key:Ljava/lang/String;

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->newBuilder()Lcom/squareup/protos/client/dialogue/ListConversationsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 116
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->items:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 117
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    if-eqz v1, :cond_2

    const-string v1, ", list_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->list_options:Lcom/squareup/protos/client/dialogue/ListOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 118
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->paging_key:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", paging_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/ListConversationsResponse;->paging_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ListConversationsResponse{"

    .line 119
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
