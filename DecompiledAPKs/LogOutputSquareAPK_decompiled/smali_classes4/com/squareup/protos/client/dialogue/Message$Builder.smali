.class public final Lcom/squareup/protos/client/dialogue/Message$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Message.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/dialogue/Message;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/dialogue/Message;",
        "Lcom/squareup/protos/client/dialogue/Message$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public created_at:Lcom/squareup/protos/common/time/DateTime;

.field public data:Lcom/squareup/protos/client/dialogue/Message$Data;

.field public type:Lcom/squareup/protos/client/dialogue/Message$Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/dialogue/Message;
    .locals 5

    .line 131
    new-instance v0, Lcom/squareup/protos/client/dialogue/Message;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Message$Builder;->type:Lcom/squareup/protos/client/dialogue/Message$Type;

    iget-object v2, p0, Lcom/squareup/protos/client/dialogue/Message$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p0, Lcom/squareup/protos/client/dialogue/Message$Builder;->data:Lcom/squareup/protos/client/dialogue/Message$Data;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/dialogue/Message;-><init>(Lcom/squareup/protos/client/dialogue/Message$Type;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/client/dialogue/Message$Data;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/Message$Builder;->build()Lcom/squareup/protos/client/dialogue/Message;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/dialogue/Message$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Message$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public data(Lcom/squareup/protos/client/dialogue/Message$Data;)Lcom/squareup/protos/client/dialogue/Message$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Message$Builder;->data:Lcom/squareup/protos/client/dialogue/Message$Data;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/dialogue/Message$Type;)Lcom/squareup/protos/client/dialogue/Message$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Message$Builder;->type:Lcom/squareup/protos/client/dialogue/Message$Type;

    return-object p0
.end method
