.class public final enum Lcom/squareup/protos/client/dialogue/Sentiment;
.super Ljava/lang/Enum;
.source "Sentiment.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/dialogue/Sentiment$ProtoAdapter_Sentiment;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/dialogue/Sentiment;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/dialogue/Sentiment;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/dialogue/Sentiment;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum INVALID_SENTIMENT:Lcom/squareup/protos/client/dialogue/Sentiment;

.field public static final enum NEGATIVE:Lcom/squareup/protos/client/dialogue/Sentiment;

.field public static final enum POSITIVE:Lcom/squareup/protos/client/dialogue/Sentiment;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/dialogue/Sentiment;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 18
    new-instance v0, Lcom/squareup/protos/client/dialogue/Sentiment;

    const/4 v1, 0x0

    const-string v2, "INVALID_SENTIMENT"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/dialogue/Sentiment;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/dialogue/Sentiment;->INVALID_SENTIMENT:Lcom/squareup/protos/client/dialogue/Sentiment;

    .line 20
    new-instance v0, Lcom/squareup/protos/client/dialogue/Sentiment;

    const/4 v2, 0x1

    const-string v3, "POSITIVE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/dialogue/Sentiment;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/dialogue/Sentiment;->POSITIVE:Lcom/squareup/protos/client/dialogue/Sentiment;

    .line 22
    new-instance v0, Lcom/squareup/protos/client/dialogue/Sentiment;

    const/4 v3, 0x2

    const-string v4, "NEGATIVE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/dialogue/Sentiment;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/dialogue/Sentiment;->NEGATIVE:Lcom/squareup/protos/client/dialogue/Sentiment;

    .line 27
    new-instance v0, Lcom/squareup/protos/client/dialogue/Sentiment;

    const/4 v4, 0x3

    const-string v5, "UNKNOWN"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/dialogue/Sentiment;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/dialogue/Sentiment;->UNKNOWN:Lcom/squareup/protos/client/dialogue/Sentiment;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/client/dialogue/Sentiment;

    .line 13
    sget-object v5, Lcom/squareup/protos/client/dialogue/Sentiment;->INVALID_SENTIMENT:Lcom/squareup/protos/client/dialogue/Sentiment;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/client/dialogue/Sentiment;->POSITIVE:Lcom/squareup/protos/client/dialogue/Sentiment;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/dialogue/Sentiment;->NEGATIVE:Lcom/squareup/protos/client/dialogue/Sentiment;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/dialogue/Sentiment;->UNKNOWN:Lcom/squareup/protos/client/dialogue/Sentiment;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/client/dialogue/Sentiment;->$VALUES:[Lcom/squareup/protos/client/dialogue/Sentiment;

    .line 29
    new-instance v0, Lcom/squareup/protos/client/dialogue/Sentiment$ProtoAdapter_Sentiment;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/Sentiment$ProtoAdapter_Sentiment;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/dialogue/Sentiment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput p3, p0, Lcom/squareup/protos/client/dialogue/Sentiment;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/dialogue/Sentiment;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 45
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/dialogue/Sentiment;->UNKNOWN:Lcom/squareup/protos/client/dialogue/Sentiment;

    return-object p0

    .line 44
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/dialogue/Sentiment;->NEGATIVE:Lcom/squareup/protos/client/dialogue/Sentiment;

    return-object p0

    .line 43
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/dialogue/Sentiment;->POSITIVE:Lcom/squareup/protos/client/dialogue/Sentiment;

    return-object p0

    .line 42
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/dialogue/Sentiment;->INVALID_SENTIMENT:Lcom/squareup/protos/client/dialogue/Sentiment;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/Sentiment;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/client/dialogue/Sentiment;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/dialogue/Sentiment;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/dialogue/Sentiment;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/client/dialogue/Sentiment;->$VALUES:[Lcom/squareup/protos/client/dialogue/Sentiment;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/dialogue/Sentiment;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/dialogue/Sentiment;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 52
    iget v0, p0, Lcom/squareup/protos/client/dialogue/Sentiment;->value:I

    return v0
.end method
