.class public final enum Lcom/squareup/protos/client/dialogue/Commenter;
.super Ljava/lang/Enum;
.source "Commenter.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/dialogue/Commenter$ProtoAdapter_Commenter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/dialogue/Commenter;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/dialogue/Commenter;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/dialogue/Commenter;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BUYER:Lcom/squareup/protos/client/dialogue/Commenter;

.field public static final enum INVALID_COMMENTER:Lcom/squareup/protos/client/dialogue/Commenter;

.field public static final enum MERCHANT:Lcom/squareup/protos/client/dialogue/Commenter;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 15
    new-instance v0, Lcom/squareup/protos/client/dialogue/Commenter;

    const/4 v1, 0x0

    const-string v2, "INVALID_COMMENTER"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/dialogue/Commenter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/dialogue/Commenter;->INVALID_COMMENTER:Lcom/squareup/protos/client/dialogue/Commenter;

    .line 17
    new-instance v0, Lcom/squareup/protos/client/dialogue/Commenter;

    const/4 v2, 0x1

    const-string v3, "MERCHANT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/dialogue/Commenter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/dialogue/Commenter;->MERCHANT:Lcom/squareup/protos/client/dialogue/Commenter;

    .line 19
    new-instance v0, Lcom/squareup/protos/client/dialogue/Commenter;

    const/4 v3, 0x2

    const-string v4, "BUYER"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/dialogue/Commenter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/dialogue/Commenter;->BUYER:Lcom/squareup/protos/client/dialogue/Commenter;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/client/dialogue/Commenter;

    .line 14
    sget-object v4, Lcom/squareup/protos/client/dialogue/Commenter;->INVALID_COMMENTER:Lcom/squareup/protos/client/dialogue/Commenter;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/dialogue/Commenter;->MERCHANT:Lcom/squareup/protos/client/dialogue/Commenter;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/dialogue/Commenter;->BUYER:Lcom/squareup/protos/client/dialogue/Commenter;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/dialogue/Commenter;->$VALUES:[Lcom/squareup/protos/client/dialogue/Commenter;

    .line 21
    new-instance v0, Lcom/squareup/protos/client/dialogue/Commenter$ProtoAdapter_Commenter;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/Commenter$ProtoAdapter_Commenter;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/dialogue/Commenter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26
    iput p3, p0, Lcom/squareup/protos/client/dialogue/Commenter;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/dialogue/Commenter;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 36
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/dialogue/Commenter;->BUYER:Lcom/squareup/protos/client/dialogue/Commenter;

    return-object p0

    .line 35
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/dialogue/Commenter;->MERCHANT:Lcom/squareup/protos/client/dialogue/Commenter;

    return-object p0

    .line 34
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/dialogue/Commenter;->INVALID_COMMENTER:Lcom/squareup/protos/client/dialogue/Commenter;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/Commenter;
    .locals 1

    .line 14
    const-class v0, Lcom/squareup/protos/client/dialogue/Commenter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/dialogue/Commenter;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/dialogue/Commenter;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/protos/client/dialogue/Commenter;->$VALUES:[Lcom/squareup/protos/client/dialogue/Commenter;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/dialogue/Commenter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/dialogue/Commenter;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 43
    iget v0, p0, Lcom/squareup/protos/client/dialogue/Commenter;->value:I

    return v0
.end method
