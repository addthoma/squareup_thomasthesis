.class public final enum Lcom/squareup/protos/client/State;
.super Ljava/lang/Enum;
.source "State.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/State$ProtoAdapter_State;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/State;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/State;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/State;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DO_NOT_USE_STATE:Lcom/squareup/protos/client/State;

.field public static final enum IN_STOCK:Lcom/squareup/protos/client/State;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 16
    new-instance v0, Lcom/squareup/protos/client/State;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE_STATE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/State;->DO_NOT_USE_STATE:Lcom/squareup/protos/client/State;

    .line 18
    new-instance v0, Lcom/squareup/protos/client/State;

    const/4 v2, 0x2

    const/4 v3, 0x1

    const-string v4, "IN_STOCK"

    invoke-direct {v0, v4, v3, v2}, Lcom/squareup/protos/client/State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/State;->IN_STOCK:Lcom/squareup/protos/client/State;

    new-array v0, v2, [Lcom/squareup/protos/client/State;

    .line 15
    sget-object v2, Lcom/squareup/protos/client/State;->DO_NOT_USE_STATE:Lcom/squareup/protos/client/State;

    aput-object v2, v0, v1

    sget-object v1, Lcom/squareup/protos/client/State;->IN_STOCK:Lcom/squareup/protos/client/State;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/State;->$VALUES:[Lcom/squareup/protos/client/State;

    .line 20
    new-instance v0, Lcom/squareup/protos/client/State$ProtoAdapter_State;

    invoke-direct {v0}, Lcom/squareup/protos/client/State$ProtoAdapter_State;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput p3, p0, Lcom/squareup/protos/client/State;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/State;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 34
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/State;->IN_STOCK:Lcom/squareup/protos/client/State;

    return-object p0

    .line 33
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/State;->DO_NOT_USE_STATE:Lcom/squareup/protos/client/State;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/State;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/protos/client/State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/State;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/State;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/protos/client/State;->$VALUES:[Lcom/squareup/protos/client/State;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/State;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 41
    iget v0, p0, Lcom/squareup/protos/client/State;->value:I

    return v0
.end method
