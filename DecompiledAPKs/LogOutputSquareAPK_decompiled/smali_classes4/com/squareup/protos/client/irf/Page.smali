.class public final Lcom/squareup/protos/client/irf/Page;
.super Lcom/squareup/wire/Message;
.source "Page.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/irf/Page$ProtoAdapter_Page;,
        Lcom/squareup/protos/client/irf/Page$PageType;,
        Lcom/squareup/protos/client/irf/Page$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/irf/Page;",
        "Lcom/squareup/protos/client/irf/Page$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/irf/Page;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PAGE_TYPE:Lcom/squareup/protos/client/irf/Page$PageType;

.field public static final DEFAULT_TITLE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final page_type:Lcom/squareup/protos/client/irf/Page$PageType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.irf.Page$PageType#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final section:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.irf.Section#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Section;",
            ">;"
        }
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/irf/Page$ProtoAdapter_Page;

    invoke-direct {v0}, Lcom/squareup/protos/client/irf/Page$ProtoAdapter_Page;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/irf/Page;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 27
    sget-object v0, Lcom/squareup/protos/client/irf/Page$PageType;->UNKNOWN:Lcom/squareup/protos/client/irf/Page$PageType;

    sput-object v0, Lcom/squareup/protos/client/irf/Page;->DEFAULT_PAGE_TYPE:Lcom/squareup/protos/client/irf/Page$PageType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/irf/Page$PageType;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/irf/Page$PageType;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Section;",
            ">;)V"
        }
    .end annotation

    .line 51
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/irf/Page;-><init>(Lcom/squareup/protos/client/irf/Page$PageType;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/irf/Page$PageType;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/irf/Page$PageType;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Section;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 55
    sget-object v0, Lcom/squareup/protos/client/irf/Page;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 56
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Page;->page_type:Lcom/squareup/protos/client/irf/Page$PageType;

    .line 57
    iput-object p2, p0, Lcom/squareup/protos/client/irf/Page;->title:Ljava/lang/String;

    const-string p1, "section"

    .line 58
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/irf/Page;->section:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 74
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/irf/Page;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 75
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/irf/Page;

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/Page;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/irf/Page;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Page;->page_type:Lcom/squareup/protos/client/irf/Page$PageType;

    iget-object v3, p1, Lcom/squareup/protos/client/irf/Page;->page_type:Lcom/squareup/protos/client/irf/Page$PageType;

    .line 77
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Page;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/irf/Page;->title:Ljava/lang/String;

    .line 78
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Page;->section:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/irf/Page;->section:Ljava/util/List;

    .line 79
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 84
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/Page;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Page;->page_type:Lcom/squareup/protos/client/irf/Page$PageType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/irf/Page$PageType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Page;->title:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Page;->section:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/irf/Page$Builder;
    .locals 2

    .line 63
    new-instance v0, Lcom/squareup/protos/client/irf/Page$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/irf/Page$Builder;-><init>()V

    .line 64
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Page;->page_type:Lcom/squareup/protos/client/irf/Page$PageType;

    iput-object v1, v0, Lcom/squareup/protos/client/irf/Page$Builder;->page_type:Lcom/squareup/protos/client/irf/Page$PageType;

    .line 65
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Page;->title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/irf/Page$Builder;->title:Ljava/lang/String;

    .line 66
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Page;->section:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/irf/Page$Builder;->section:Ljava/util/List;

    .line 67
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/Page;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/irf/Page$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/Page;->newBuilder()Lcom/squareup/protos/client/irf/Page$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Page;->page_type:Lcom/squareup/protos/client/irf/Page$PageType;

    if-eqz v1, :cond_0

    const-string v1, ", page_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Page;->page_type:Lcom/squareup/protos/client/irf/Page$PageType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 99
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Page;->title:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Page;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Page;->section:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", section="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Page;->section:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Page{"

    .line 101
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
