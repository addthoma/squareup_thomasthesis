.class public final Lcom/squareup/protos/client/irf/SaveFormRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SaveFormRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/irf/SaveFormRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/irf/SaveFormRequest;",
        "Lcom/squareup/protos/client/irf/SaveFormRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public answer:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Answer;",
            ">;"
        }
    .end annotation
.end field

.field public complete:Ljava/lang/Boolean;

.field public target_token:Ljava/lang/String;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 126
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 127
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/irf/SaveFormRequest$Builder;->answer:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public answer(Ljava/util/List;)Lcom/squareup/protos/client/irf/SaveFormRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Answer;",
            ">;)",
            "Lcom/squareup/protos/client/irf/SaveFormRequest$Builder;"
        }
    .end annotation

    .line 136
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/client/irf/SaveFormRequest$Builder;->answer:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/irf/SaveFormRequest;
    .locals 7

    .line 153
    new-instance v6, Lcom/squareup/protos/client/irf/SaveFormRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/SaveFormRequest$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/irf/SaveFormRequest$Builder;->answer:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/irf/SaveFormRequest$Builder;->complete:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/client/irf/SaveFormRequest$Builder;->target_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/irf/SaveFormRequest;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/SaveFormRequest$Builder;->build()Lcom/squareup/protos/client/irf/SaveFormRequest;

    move-result-object v0

    return-object v0
.end method

.method public complete(Ljava/lang/Boolean;)Lcom/squareup/protos/client/irf/SaveFormRequest$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/irf/SaveFormRequest$Builder;->complete:Ljava/lang/Boolean;

    return-object p0
.end method

.method public target_token(Ljava/lang/String;)Lcom/squareup/protos/client/irf/SaveFormRequest$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/client/irf/SaveFormRequest$Builder;->target_token:Ljava/lang/String;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/irf/SaveFormRequest$Builder;
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/client/irf/SaveFormRequest$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
