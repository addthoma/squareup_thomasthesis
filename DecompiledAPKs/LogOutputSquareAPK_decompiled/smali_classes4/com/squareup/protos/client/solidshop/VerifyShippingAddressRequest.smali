.class public final Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;
.super Lcom/squareup/wire/Message;
.source "VerifyShippingAddressRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$ProtoAdapter_VerifyShippingAddressRequest;,
        Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FIRST_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_LAST_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_SAVE:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final first_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final last_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final phone:Lcom/squareup/protos/common/location/Phone;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.location.Phone#ADAPTER"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final save:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final shipping_address:Lcom/squareup/protos/common/location/GlobalAddress;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.location.GlobalAddress#ADAPTER"
        redacted = true
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$ProtoAdapter_VerifyShippingAddressRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$ProtoAdapter_VerifyShippingAddressRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->DEFAULT_SAVE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/location/GlobalAddress;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/Phone;Ljava/lang/Boolean;)V
    .locals 7

    .line 84
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;-><init>(Lcom/squareup/protos/common/location/GlobalAddress;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/Phone;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/location/GlobalAddress;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/Phone;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 89
    sget-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 90
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->shipping_address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 91
    iput-object p2, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->first_name:Ljava/lang/String;

    .line 92
    iput-object p3, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->last_name:Ljava/lang/String;

    .line 93
    iput-object p4, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->phone:Lcom/squareup/protos/common/location/Phone;

    .line 94
    iput-object p5, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->save:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 112
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 113
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->shipping_address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v3, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->shipping_address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 115
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->first_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->first_name:Ljava/lang/String;

    .line 116
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->last_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->last_name:Ljava/lang/String;

    .line 117
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->phone:Lcom/squareup/protos/common/location/Phone;

    iget-object v3, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->phone:Lcom/squareup/protos/common/location/Phone;

    .line 118
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->save:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->save:Ljava/lang/Boolean;

    .line 119
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 124
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 126
    invoke-virtual {p0}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->shipping_address:Lcom/squareup/protos/common/location/GlobalAddress;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/location/GlobalAddress;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->first_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->last_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->phone:Lcom/squareup/protos/common/location/Phone;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/location/Phone;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->save:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 132
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;
    .locals 2

    .line 99
    new-instance v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;-><init>()V

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->shipping_address:Lcom/squareup/protos/common/location/GlobalAddress;

    iput-object v1, v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->shipping_address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->first_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->first_name:Ljava/lang/String;

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->last_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->last_name:Ljava/lang/String;

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->phone:Lcom/squareup/protos/common/location/Phone;

    iput-object v1, v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->phone:Lcom/squareup/protos/common/location/Phone;

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->save:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->save:Ljava/lang/Boolean;

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->newBuilder()Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->shipping_address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v1, :cond_0

    const-string v1, ", shipping_address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->first_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", first_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->last_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", last_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->phone:Lcom/squareup/protos/common/location/Phone;

    if-eqz v1, :cond_3

    const-string v1, ", phone=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->save:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", save="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;->save:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "VerifyShippingAddressRequest{"

    .line 145
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
