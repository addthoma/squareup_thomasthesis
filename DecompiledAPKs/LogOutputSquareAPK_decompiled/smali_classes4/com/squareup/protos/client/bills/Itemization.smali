.class public final Lcom/squareup/protos/client/bills/Itemization;
.super Lcom/squareup/wire/Message;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Itemization$ProtoAdapter_Itemization;,
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;,
        Lcom/squareup/protos/client/bills/Itemization$Event;,
        Lcom/squareup/protos/client/bills/Itemization$Amounts;,
        Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;,
        Lcom/squareup/protos/client/bills/Itemization$BackingDetails;,
        Lcom/squareup/protos/client/bills/Itemization$Configuration;,
        Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;,
        Lcom/squareup/protos/client/bills/Itemization$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Itemization;",
        "Lcom/squareup/protos/client/bills/Itemization$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CUSTOM_NOTE:Ljava/lang/String; = ""

.field public static final DEFAULT_QUANTITY:Ljava/lang/String; = ""

.field public static final DEFAULT_QUANTITY_ENTRY_TYPE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

.field public static final DEFAULT_WRITE_ONLY_DELETED:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$Amounts#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$Configuration#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final created_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final custom_note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final event:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$Event#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;"
        }
    .end annotation
.end field

.field public final feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$FeatureDetails#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final itemization_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$QuantityUnit#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final quantity:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final quantity_entry_type:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$QuantityEntryType#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$DisplayDetails#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization$BackingDetails#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final write_only_deleted:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 51
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$ProtoAdapter_Itemization;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$ProtoAdapter_Itemization;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 57
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->MANUALLY_ENTERED:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization;->DEFAULT_QUANTITY_ENTRY_TYPE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    const/4 v0, 0x0

    .line 61
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization;->DEFAULT_WRITE_ONLY_DELETED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lcom/squareup/orders/model/Order$QuantityUnit;Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization$Configuration;Lcom/squareup/protos/client/bills/Itemization$BackingDetails;Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;Lcom/squareup/protos/client/bills/Itemization$Amounts;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$QuantityUnit;",
            "Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Itemization$Configuration;",
            "Lcom/squareup/protos/client/bills/Itemization$BackingDetails;",
            "Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;",
            "Lcom/squareup/protos/client/bills/Itemization$Amounts;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;",
            ")V"
        }
    .end annotation

    .line 184
    sget-object v14, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/protos/client/bills/Itemization;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lcom/squareup/orders/model/Order$QuantityUnit;Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization$Configuration;Lcom/squareup/protos/client/bills/Itemization$BackingDetails;Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;Lcom/squareup/protos/client/bills/Itemization$Amounts;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lcom/squareup/orders/model/Order$QuantityUnit;Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization$Configuration;Lcom/squareup/protos/client/bills/Itemization$BackingDetails;Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;Lcom/squareup/protos/client/bills/Itemization$Amounts;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$QuantityUnit;",
            "Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Itemization$Configuration;",
            "Lcom/squareup/protos/client/bills/Itemization$BackingDetails;",
            "Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;",
            "Lcom/squareup/protos/client/bills/Itemization$Amounts;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 193
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p14}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 194
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 195
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    .line 196
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Itemization;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    .line 197
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Itemization;->quantity_entry_type:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    .line 198
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Itemization;->custom_note:Ljava/lang/String;

    .line 199
    iput-object p6, p0, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    .line 200
    iput-object p7, p0, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    .line 201
    iput-object p8, p0, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    .line 202
    iput-object p9, p0, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    .line 203
    iput-object p10, p0, Lcom/squareup/protos/client/bills/Itemization;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 204
    iput-object p11, p0, Lcom/squareup/protos/client/bills/Itemization;->write_only_deleted:Ljava/lang/Boolean;

    const-string p1, "event"

    .line 205
    invoke-static {p1, p12}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization;->event:Ljava/util/List;

    .line 206
    iput-object p13, p0, Lcom/squareup/protos/client/bills/Itemization;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 232
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Itemization;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 233
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization;

    .line 234
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 235
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    .line 236
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    .line 237
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->quantity_entry_type:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization;->quantity_entry_type:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    .line 238
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->custom_note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization;->custom_note:Ljava/lang/String;

    .line 239
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    .line 240
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    .line 241
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    .line 242
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    .line 243
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 244
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->write_only_deleted:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization;->write_only_deleted:Ljava/lang/Boolean;

    .line 245
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->event:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization;->event:Ljava/util/List;

    .line 246
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    .line 247
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 252
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_c

    .line 254
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 255
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 256
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 257
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$QuantityUnit;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 258
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->quantity_entry_type:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 259
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->custom_note:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 260
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 261
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 262
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 263
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$Amounts;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 264
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 265
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->write_only_deleted:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 266
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->event:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 267
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->hashCode()I

    move-result v2

    :cond_b
    add-int/2addr v0, v2

    .line 268
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_c
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Itemization$Builder;
    .locals 2

    .line 211
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Builder;-><init>()V

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 213
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->quantity:Ljava/lang/String;

    .line 214
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    .line 215
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->quantity_entry_type:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->quantity_entry_type:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    .line 216
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->custom_note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->custom_note:Ljava/lang/String;

    .line 217
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    .line 218
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    .line 219
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    .line 220
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 222
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->write_only_deleted:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->write_only_deleted:Ljava/lang/Boolean;

    .line 223
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->event:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->event:Ljava/util/List;

    .line 224
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    .line 225
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 50
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 275
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 276
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", itemization_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 277
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    if-eqz v1, :cond_2

    const-string v1, ", measurement_unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 279
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->quantity_entry_type:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    if-eqz v1, :cond_3

    const-string v1, ", quantity_entry_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->quantity_entry_type:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 280
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->custom_note:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", custom_note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->custom_note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    if-eqz v1, :cond_5

    const-string v1, ", configuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 282
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    if-eqz v1, :cond_6

    const-string v1, ", write_only_backing_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 283
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    if-eqz v1, :cond_7

    const-string v1, ", read_only_display_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 284
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    if-eqz v1, :cond_8

    const-string v1, ", amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 285
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_9

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 286
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->write_only_deleted:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", write_only_deleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->write_only_deleted:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 287
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->event:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, ", event="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->event:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 288
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    if-eqz v1, :cond_c

    const-string v1, ", feature_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_c
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Itemization{"

    .line 289
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
