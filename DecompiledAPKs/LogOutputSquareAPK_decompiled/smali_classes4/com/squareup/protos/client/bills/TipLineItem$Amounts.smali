.class public final Lcom/squareup/protos/client/bills/TipLineItem$Amounts;
.super Lcom/squareup/wire/Message;
.source "TipLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/TipLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Amounts"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/TipLineItem$Amounts$ProtoAdapter_Amounts;,
        Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/TipLineItem$Amounts;",
        "Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/TipLineItem$Amounts;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final applied_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 149
    new-instance v0, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$ProtoAdapter_Amounts;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$ProtoAdapter_Amounts;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 160
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;-><init>(Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 164
    sget-object v0, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 165
    iput-object p1, p0, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 179
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 180
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    .line 181
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    .line 182
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 187
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 189
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 190
    iget-object v1, p0, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 191
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;
    .locals 2

    .line 170
    new-instance v0, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;-><init>()V

    .line 171
    iget-object v1, p0, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;->applied_money:Lcom/squareup/protos/common/Money;

    .line 172
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 148
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->newBuilder()Lcom/squareup/protos/client/bills/TipLineItem$Amounts$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 198
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    const-string v1, ", applied_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Amounts{"

    .line 200
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
