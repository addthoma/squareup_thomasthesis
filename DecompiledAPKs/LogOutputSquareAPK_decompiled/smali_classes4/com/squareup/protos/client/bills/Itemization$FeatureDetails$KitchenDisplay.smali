.class public final Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;
.super Lcom/squareup/wire/Message;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "KitchenDisplay"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$ProtoAdapter_KitchenDisplay;,
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EXPEDITE_COMPLETE:Ljava/lang/Boolean;

.field public static final DEFAULT_PREP_COMPLETE:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final expedite_complete:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final prep_complete:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 4160
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$ProtoAdapter_KitchenDisplay;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$ProtoAdapter_KitchenDisplay;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 4164
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->DEFAULT_PREP_COMPLETE:Ljava/lang/Boolean;

    .line 4166
    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->DEFAULT_EXPEDITE_COMPLETE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 1

    .line 4181
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 4186
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 4187
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->prep_complete:Ljava/lang/Boolean;

    .line 4188
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->expedite_complete:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 4203
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 4204
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    .line 4205
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->prep_complete:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->prep_complete:Ljava/lang/Boolean;

    .line 4206
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->expedite_complete:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->expedite_complete:Ljava/lang/Boolean;

    .line 4207
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 4212
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 4214
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 4215
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->prep_complete:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4216
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->expedite_complete:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 4217
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;
    .locals 2

    .line 4193
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;-><init>()V

    .line 4194
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->prep_complete:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;->prep_complete:Ljava/lang/Boolean;

    .line 4195
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->expedite_complete:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;->expedite_complete:Ljava/lang/Boolean;

    .line 4196
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 4159
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 4224
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4225
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->prep_complete:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", prep_complete="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->prep_complete:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4226
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->expedite_complete:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", expedite_complete="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->expedite_complete:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "KitchenDisplay{"

    .line 4227
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
