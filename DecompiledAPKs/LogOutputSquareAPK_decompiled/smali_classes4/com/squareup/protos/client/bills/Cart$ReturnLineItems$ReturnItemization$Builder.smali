.class public final Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
        "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public itemization:Lcom/squareup/protos/client/bills/Itemization;

.field public source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8249
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;
    .locals 4

    .line 8277
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Itemization;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 8244
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->build()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    move-result-object v0

    return-object v0
.end method

.method public itemization(Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;
    .locals 0

    .line 8271
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    return-object p0
.end method

.method public source_itemization_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;
    .locals 0

    .line 8263
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization$Builder;->source_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method
