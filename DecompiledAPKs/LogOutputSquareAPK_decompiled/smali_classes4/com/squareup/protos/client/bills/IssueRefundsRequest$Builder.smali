.class public final Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "IssueRefundsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/IssueRefundsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/IssueRefundsRequest;",
        "Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public creator_details:Lcom/squareup/protos/client/CreatorDetails;

.field public refund_request:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/RefundRequest;",
            ">;"
        }
    .end annotation
.end field

.field public request_uuid:Ljava/lang/String;

.field public return_cart_details:Lcom/squareup/protos/client/bills/ReturnCartDetails;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 155
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 156
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->refund_request:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/IssueRefundsRequest;
    .locals 8

    .line 200
    new-instance v7, Lcom/squareup/protos/client/bills/IssueRefundsRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->request_uuid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->unit_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->refund_request:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->return_cart_details:Lcom/squareup/protos/client/bills/ReturnCartDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/IssueRefundsRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/CreatorDetails;Ljava/util/List;Lcom/squareup/protos/client/bills/ReturnCartDetails;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 144
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->build()Lcom/squareup/protos/client/bills/IssueRefundsRequest;

    move-result-object v0

    return-object v0
.end method

.method public creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;
    .locals 0

    .line 180
    iput-object p1, p0, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    return-object p0
.end method

.method public refund_request(Ljava/util/List;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/RefundRequest;",
            ">;)",
            "Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;"
        }
    .end annotation

    .line 185
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 186
    iput-object p1, p0, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->refund_request:Ljava/util/List;

    return-object p0
.end method

.method public request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->request_uuid:Ljava/lang/String;

    return-object p0
.end method

.method public return_cart_details(Lcom/squareup/protos/client/bills/ReturnCartDetails;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->return_cart_details:Lcom/squareup/protos/client/bills/ReturnCartDetails;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/client/bills/IssueRefundsRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
