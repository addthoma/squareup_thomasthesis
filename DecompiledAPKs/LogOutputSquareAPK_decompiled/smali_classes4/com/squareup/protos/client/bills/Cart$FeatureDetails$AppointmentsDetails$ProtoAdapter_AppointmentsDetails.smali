.class final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$ProtoAdapter_AppointmentsDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AppointmentsDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 3234
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3253
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;-><init>()V

    .line 3254
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 3255
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 3260
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 3258
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;->appointment_staff(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;

    goto :goto_0

    .line 3257
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;->appointment_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;

    goto :goto_0

    .line 3264
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 3265
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3232
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$ProtoAdapter_AppointmentsDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3246
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;->appointment_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3247
    sget-object v0, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;->appointment_staff:Lcom/squareup/protos/client/CreatorDetails;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3248
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3232
    check-cast p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$ProtoAdapter_AppointmentsDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;)I
    .locals 4

    .line 3239
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;->appointment_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;->appointment_staff:Lcom/squareup/protos/client/CreatorDetails;

    const/4 v3, 0x2

    .line 3240
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3241
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 3232
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$ProtoAdapter_AppointmentsDetails;->encodedSize(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;
    .locals 2

    .line 3270
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;

    move-result-object p1

    .line 3271
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;->appointment_staff:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;->appointment_staff:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/CreatorDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;->appointment_staff:Lcom/squareup/protos/client/CreatorDetails;

    .line 3272
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 3273
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 3232
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$ProtoAdapter_AppointmentsDetails;->redact(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    move-result-object p1

    return-object p1
.end method
