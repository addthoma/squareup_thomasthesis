.class final Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType$ProtoAdapter_CancelBillType;
.super Lcom/squareup/wire/EnumAdapter;
.source "CancelBillRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CancelBillType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 373
    const-class v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;
    .locals 0

    .line 378
    invoke-static {p1}, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->fromValue(I)Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 371
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType$ProtoAdapter_CancelBillType;->fromValue(I)Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    move-result-object p1

    return-object p1
.end method
