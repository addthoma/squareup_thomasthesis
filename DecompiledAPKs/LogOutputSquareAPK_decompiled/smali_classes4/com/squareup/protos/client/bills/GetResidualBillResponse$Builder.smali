.class public final Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetResidualBillResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetResidualBillResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse;",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amendment_token:Ljava/lang/String;

.field public can_not_issue_exchange:Ljava/lang/Boolean;

.field public can_only_issue_amount_based_refund:Ljava/lang/Boolean;

.field public refund_constraints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;",
            ">;"
        }
    .end annotation
.end field

.field public residual_itemization:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
            ">;"
        }
    .end annotation
.end field

.field public residual_surcharge:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;",
            ">;"
        }
    .end annotation
.end field

.field public residual_tender:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;",
            ">;"
        }
    .end annotation
.end field

.field public residual_tip:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;",
            ">;"
        }
    .end annotation
.end field

.field public sold_from_unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 260
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 261
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->residual_itemization:Ljava/util/List;

    .line 262
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->residual_tender:Ljava/util/List;

    .line 263
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->residual_tip:Ljava/util/List;

    .line 264
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->residual_surcharge:Ljava/util/List;

    .line 265
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->refund_constraints:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public amendment_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;
    .locals 0

    .line 317
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->amendment_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/GetResidualBillResponse;
    .locals 12

    .line 364
    new-instance v11, Lcom/squareup/protos/client/bills/GetResidualBillResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->can_only_issue_amount_based_refund:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->residual_itemization:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->residual_tender:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->residual_tip:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->residual_surcharge:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->amendment_token:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->can_not_issue_exchange:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->sold_from_unit_token:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->refund_constraints:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/bills/GetResidualBillResponse;-><init>(Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 241
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->build()Lcom/squareup/protos/client/bills/GetResidualBillResponse;

    move-result-object v0

    return-object v0
.end method

.method public can_not_issue_exchange(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;
    .locals 0

    .line 331
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->can_not_issue_exchange:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_only_issue_amount_based_refund(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;
    .locals 0

    .line 284
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->can_only_issue_amount_based_refund:Ljava/lang/Boolean;

    return-object p0
.end method

.method public refund_constraints(Ljava/util/List;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;",
            ">;)",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;"
        }
    .end annotation

    .line 357
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 358
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->refund_constraints:Ljava/util/List;

    return-object p0
.end method

.method public residual_itemization(Ljava/util/List;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
            ">;)",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;"
        }
    .end annotation

    .line 289
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 290
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->residual_itemization:Ljava/util/List;

    return-object p0
.end method

.method public residual_surcharge(Ljava/util/List;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualSurcharge;",
            ">;)",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;"
        }
    .end annotation

    .line 307
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 308
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->residual_surcharge:Ljava/util/List;

    return-object p0
.end method

.method public residual_tender(Ljava/util/List;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;",
            ">;)",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;"
        }
    .end annotation

    .line 295
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 296
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->residual_tender:Ljava/util/List;

    return-object p0
.end method

.method public residual_tip(Ljava/util/List;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;",
            ">;)",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;"
        }
    .end annotation

    .line 301
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 302
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->residual_tip:Ljava/util/List;

    return-object p0
.end method

.method public sold_from_unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;
    .locals 0

    .line 342
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$Builder;->sold_from_unit_token:Ljava/lang/String;

    return-object p0
.end method
