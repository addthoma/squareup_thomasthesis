.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TenderDisplayDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$ProtoAdapter_TenderDisplayDetails;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/bills/Tender$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final instrument_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$InstrumentDetails$DisplayDetails#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final tender_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final total_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/bills/Tender$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender$Type#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 4856
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$ProtoAdapter_TenderDisplayDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$ProtoAdapter_TenderDisplayDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 4860
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->UNKNOWN:Lcom/squareup/protos/client/bills/Tender$Type;

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->DEFAULT_TYPE:Lcom/squareup/protos/client/bills/Tender$Type;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;Lcom/squareup/protos/client/IdPair;)V
    .locals 6

    .line 4894
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;-><init>(Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;Lcom/squareup/protos/client/IdPair;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;Lcom/squareup/protos/client/IdPair;Lokio/ByteString;)V
    .locals 1

    .line 4900
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 4901
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 4902
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->total_money:Lcom/squareup/protos/common/Money;

    .line 4903
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->instrument_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    .line 4904
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 4921
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 4922
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;

    .line 4923
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 4924
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->total_money:Lcom/squareup/protos/common/Money;

    .line 4925
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->instrument_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->instrument_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    .line 4926
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 4927
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 4932
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 4934
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 4935
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$Type;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4936
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->total_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4937
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->instrument_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4938
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 4939
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;
    .locals 2

    .line 4909
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;-><init>()V

    .line 4910
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 4911
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->total_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;->total_money:Lcom/squareup/protos/common/Money;

    .line 4912
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->instrument_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;->instrument_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    .line 4913
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 4914
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 4855
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 4946
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4947
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4948
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->total_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", total_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->total_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4949
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->instrument_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    if-eqz v1, :cond_2

    const-string v1, ", instrument_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->instrument_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4950
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_3

    const-string v1, ", tender_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "TenderDisplayDetails{"

    .line 4951
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
