.class final Lcom/squareup/protos/client/bills/Itemization$Amounts$ProtoAdapter_Amounts;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$Amounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Amounts"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Itemization$Amounts;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 3030
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Itemization$Amounts;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$Amounts;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3059
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;-><init>()V

    .line 3060
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 3061
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 3071
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 3069
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->taxable_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    goto :goto_0

    .line 3068
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->gross_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    goto :goto_0

    .line 3067
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    goto :goto_0

    .line 3066
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    goto :goto_0

    .line 3065
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->discount_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    goto :goto_0

    .line 3064
    :cond_0
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->item_variation_price_times_quantity_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    goto :goto_0

    .line 3063
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->item_variation_price_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    goto :goto_0

    .line 3075
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 3076
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Amounts;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3028
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Amounts$ProtoAdapter_Amounts;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$Amounts;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$Amounts;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3047
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Amounts;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3048
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Amounts;->item_variation_price_times_quantity_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3049
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Amounts;->discount_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3050
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Amounts;->tax_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3051
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3052
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Amounts;->gross_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3053
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$Amounts;->taxable_amount:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3054
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Itemization$Amounts;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3028
    check-cast p2, Lcom/squareup/protos/client/bills/Itemization$Amounts;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Itemization$Amounts$ProtoAdapter_Amounts;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$Amounts;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Itemization$Amounts;)I
    .locals 4

    .line 3035
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts;->item_variation_price_times_quantity_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x2

    .line 3036
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts;->discount_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x5

    .line 3037
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts;->tax_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x6

    .line 3038
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x7

    .line 3039
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts;->gross_sales_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x8

    .line 3040
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts;->taxable_amount:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x9

    .line 3041
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3042
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Amounts;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 3028
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$Amounts;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Amounts$ProtoAdapter_Amounts;->encodedSize(Lcom/squareup/protos/client/bills/Itemization$Amounts;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Itemization$Amounts;)Lcom/squareup/protos/client/bills/Itemization$Amounts;
    .locals 2

    .line 3081
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Amounts;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;

    move-result-object p1

    .line 3082
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    .line 3083
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->item_variation_price_times_quantity_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->item_variation_price_times_quantity_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->item_variation_price_times_quantity_money:Lcom/squareup/protos/common/Money;

    .line 3084
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->discount_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->discount_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->discount_money:Lcom/squareup/protos/common/Money;

    .line 3085
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    .line 3086
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->total_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->total_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->total_money:Lcom/squareup/protos/common/Money;

    .line 3087
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->gross_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 3088
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->taxable_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->taxable_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->taxable_amount:Lcom/squareup/protos/common/Money;

    .line 3089
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 3090
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Amounts;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 3028
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$Amounts;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$Amounts$ProtoAdapter_Amounts;->redact(Lcom/squareup/protos/client/bills/Itemization$Amounts;)Lcom/squareup/protos/client/bills/Itemization$Amounts;

    move-result-object p1

    return-object p1
.end method
