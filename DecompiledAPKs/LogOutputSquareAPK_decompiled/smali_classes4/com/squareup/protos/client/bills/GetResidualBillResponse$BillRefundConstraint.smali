.class public final enum Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;
.super Ljava/lang/Enum;
.source "GetResidualBillResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetResidualBillResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BillRefundConstraint"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint$ProtoAdapter_BillRefundConstraint;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CARD_MUST_BE_PRESENT:Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

.field public static final enum FULL_REFUND_ONLY:Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 369
    new-instance v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;->UNKNOWN:Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

    .line 374
    new-instance v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

    const/4 v2, 0x1

    const-string v3, "CARD_MUST_BE_PRESENT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;->CARD_MUST_BE_PRESENT:Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

    .line 379
    new-instance v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

    const/4 v3, 0x2

    const-string v4, "FULL_REFUND_ONLY"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;->FULL_REFUND_ONLY:Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

    .line 368
    sget-object v4, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;->UNKNOWN:Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;->CARD_MUST_BE_PRESENT:Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;->FULL_REFUND_ONLY:Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;->$VALUES:[Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

    .line 381
    new-instance v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint$ProtoAdapter_BillRefundConstraint;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint$ProtoAdapter_BillRefundConstraint;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 385
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 386
    iput p3, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 396
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;->FULL_REFUND_ONLY:Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

    return-object p0

    .line 395
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;->CARD_MUST_BE_PRESENT:Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

    return-object p0

    .line 394
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;->UNKNOWN:Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;
    .locals 1

    .line 368
    const-class v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;
    .locals 1

    .line 368
    sget-object v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;->$VALUES:[Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 403
    iget v0, p0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$BillRefundConstraint;->value:I

    return v0
.end method
