.class public final enum Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;
.super Ljava/lang/Enum;
.source "Itemization.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CreateReason"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason$ProtoAdapter_CreateReason;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

.field public static final enum REOPEN:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 5103
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;->DO_NOT_USE:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    .line 5108
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    const/4 v2, 0x1

    const-string v3, "REOPEN"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;->REOPEN:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    .line 5102
    sget-object v3, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;->DO_NOT_USE:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;->REOPEN:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;->$VALUES:[Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    .line 5110
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason$ProtoAdapter_CreateReason;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason$ProtoAdapter_CreateReason;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 5114
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 5115
    iput p3, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 5124
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;->REOPEN:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    return-object p0

    .line 5123
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;->DO_NOT_USE:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;
    .locals 1

    .line 5102
    const-class v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;
    .locals 1

    .line 5102
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;->$VALUES:[Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 5131
    iget v0, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;->value:I

    return v0
.end method
