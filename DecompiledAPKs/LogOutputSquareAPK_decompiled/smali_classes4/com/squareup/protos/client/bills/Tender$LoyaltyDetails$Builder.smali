.class public final Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;",
        "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public account_contact:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

.field public new_enrollment:Ljava/lang/Boolean;

.field public reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

.field public reward_name:Ljava/lang/String;

.field public rewards_earned:Ljava/lang/Integer;

.field public stars_earned:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2169
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public account_contact(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;
    .locals 0

    .line 2216
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->account_contact:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;
    .locals 9

    .line 2222
    new-instance v8, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->stars_earned:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->rewards_earned:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->new_enrollment:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->reward_name:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->account_contact:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2156
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->build()Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    move-result-object v0

    return-object v0
.end method

.method public new_enrollment(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;
    .locals 0

    .line 2192
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->new_enrollment:Ljava/lang/Boolean;

    return-object p0
.end method

.method public reason(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;
    .locals 0

    .line 2208
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->reason:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$ReasonForNoStars;

    return-object p0
.end method

.method public reward_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;
    .locals 0

    .line 2200
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->reward_name:Ljava/lang/String;

    return-object p0
.end method

.method public rewards_earned(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;
    .locals 0

    .line 2184
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->rewards_earned:Ljava/lang/Integer;

    return-object p0
.end method

.method public stars_earned(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;
    .locals 0

    .line 2176
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$Builder;->stars_earned:Ljava/lang/Integer;

    return-object p0
.end method
