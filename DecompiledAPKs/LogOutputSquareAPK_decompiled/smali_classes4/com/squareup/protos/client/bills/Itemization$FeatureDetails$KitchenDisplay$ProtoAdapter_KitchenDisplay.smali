.class final Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$ProtoAdapter_KitchenDisplay;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_KitchenDisplay"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 4256
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4275
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;-><init>()V

    .line 4276
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 4277
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 4282
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 4280
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;->expedite_complete(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;

    goto :goto_0

    .line 4279
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;->prep_complete(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;

    goto :goto_0

    .line 4286
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 4287
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4254
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$ProtoAdapter_KitchenDisplay;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4268
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->prep_complete:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4269
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->expedite_complete:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4270
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4254
    check-cast p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$ProtoAdapter_KitchenDisplay;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;)I
    .locals 4

    .line 4261
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->prep_complete:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->expedite_complete:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 4262
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4263
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 4254
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$ProtoAdapter_KitchenDisplay;->encodedSize(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;
    .locals 0

    .line 4292
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;

    move-result-object p1

    .line 4293
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 4294
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 4254
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$ProtoAdapter_KitchenDisplay;->redact(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    move-result-object p1

    return-object p1
.end method
