.class final Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$ProtoAdapter_EncryptedKeyedCard;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_EncryptedKeyedCard"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1502
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1519
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;-><init>()V

    .line 1520
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1521
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 1525
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1523
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;->encrypted_keyed_card_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;

    goto :goto_0

    .line 1529
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1530
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;->build()Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1500
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$ProtoAdapter_EncryptedKeyedCard;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1513
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;->encrypted_keyed_card_data:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1514
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1500
    check-cast p2, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$ProtoAdapter_EncryptedKeyedCard;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;)I
    .locals 3

    .line 1507
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;->encrypted_keyed_card_data:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 1508
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1500
    check-cast p1, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$ProtoAdapter_EncryptedKeyedCard;->encodedSize(Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;)Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;
    .locals 1

    .line 1535
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;->newBuilder()Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 1536
    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;->encrypted_keyed_card_data:Lokio/ByteString;

    .line 1537
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1538
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$Builder;->build()Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1500
    check-cast p1, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard$ProtoAdapter_EncryptedKeyedCard;->redact(Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;)Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    move-result-object p1

    return-object p1
.end method
