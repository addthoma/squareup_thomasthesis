.class public final Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FeeLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;",
        "Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public after_application_total_money:Lcom/squareup/protos/common/Money;

.field public applied_money:Lcom/squareup/protos/common/Money;

.field public applied_to_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 928
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public after_application_total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;
    .locals 0

    .line 970
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;->after_application_total_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public applied_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;
    .locals 0

    .line 935
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;->applied_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public applied_to_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;
    .locals 0

    .line 948
    iput-object p1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;->applied_to_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;
    .locals 5

    .line 976
    new-instance v0, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;->applied_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;->applied_to_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;->after_application_total_money:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 921
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    move-result-object v0

    return-object v0
.end method
