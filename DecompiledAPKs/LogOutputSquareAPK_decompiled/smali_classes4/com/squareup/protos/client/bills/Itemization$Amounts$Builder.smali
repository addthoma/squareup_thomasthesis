.class public final Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$Amounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$Amounts;",
        "Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public discount_money:Lcom/squareup/protos/common/Money;

.field public gross_sales_money:Lcom/squareup/protos/common/Money;

.field public item_variation_price_money:Lcom/squareup/protos/common/Money;

.field public item_variation_price_times_quantity_money:Lcom/squareup/protos/common/Money;

.field public tax_money:Lcom/squareup/protos/common/Money;

.field public taxable_amount:Lcom/squareup/protos/common/Money;

.field public total_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2937
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Itemization$Amounts;
    .locals 10

    .line 3024
    new-instance v9, Lcom/squareup/protos/client/bills/Itemization$Amounts;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->item_variation_price_times_quantity_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->discount_money:Lcom/squareup/protos/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->gross_sales_money:Lcom/squareup/protos/common/Money;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->taxable_amount:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/Itemization$Amounts;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2922
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Amounts;

    move-result-object v0

    return-object v0
.end method

.method public discount_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;
    .locals 0

    .line 2968
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->discount_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public gross_sales_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;
    .locals 0

    .line 3007
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->gross_sales_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public item_variation_price_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;
    .locals 0

    .line 2946
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->item_variation_price_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public item_variation_price_times_quantity_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;
    .locals 0

    .line 2960
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->item_variation_price_times_quantity_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;
    .locals 0

    .line 2976
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public taxable_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;
    .locals 0

    .line 3018
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->taxable_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;
    .locals 0

    .line 2984
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$Amounts$Builder;->total_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
