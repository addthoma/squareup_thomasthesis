.class public final Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;
.super Lcom/squareup/wire/Message;
.source "GetBillFamiliesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Contact"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$ProtoAdapter_Contact;,
        Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CONTACT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_DISPLAY_NAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final contact_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final display_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final tender_tokens:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 382
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$ProtoAdapter_Contact;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$ProtoAdapter_Contact;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 413
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 418
    sget-object v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 419
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->contact_token:Ljava/lang/String;

    .line 420
    iput-object p2, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->display_name:Ljava/lang/String;

    const-string p1, "tender_tokens"

    .line 421
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->tender_tokens:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 437
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 438
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;

    .line 439
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->contact_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->contact_token:Ljava/lang/String;

    .line 440
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->display_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->display_name:Ljava/lang/String;

    .line 441
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->tender_tokens:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->tender_tokens:Ljava/util/List;

    .line 442
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 447
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 449
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 450
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->contact_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 451
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 452
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->tender_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 453
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;
    .locals 2

    .line 426
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;-><init>()V

    .line 427
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->contact_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;->contact_token:Ljava/lang/String;

    .line 428
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->display_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;->display_name:Ljava/lang/String;

    .line 429
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->tender_tokens:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;->tender_tokens:Ljava/util/List;

    .line 430
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 381
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->newBuilder()Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 460
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 461
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->contact_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", contact_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->contact_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 462
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", display_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->display_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 463
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->tender_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", tender_tokens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;->tender_tokens:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Contact{"

    .line 464
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
