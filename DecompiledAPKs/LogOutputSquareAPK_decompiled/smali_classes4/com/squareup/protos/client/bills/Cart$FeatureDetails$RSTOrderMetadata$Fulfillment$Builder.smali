.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public fulfillment_entry:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;",
            ">;"
        }
    .end annotation
.end field

.field public fulfillment_id:Ljava/lang/String;

.field public fulfillment_type_display_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 5808
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 5809
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;->fulfillment_entry:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;
    .locals 5

    .line 5838
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;->fulfillment_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;->fulfillment_type_display_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;->fulfillment_entry:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 5801
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment;

    move-result-object v0

    return-object v0
.end method

.method public fulfillment_entry(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;"
        }
    .end annotation

    .line 5831
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 5832
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;->fulfillment_entry:Ljava/util/List;

    return-object p0
.end method

.method public fulfillment_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;
    .locals 0

    .line 5813
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;->fulfillment_id:Ljava/lang/String;

    return-object p0
.end method

.method public fulfillment_type_display_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;
    .locals 0

    .line 5822
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$Builder;->fulfillment_type_display_name:Ljava/lang/String;

    return-object p0
.end method
