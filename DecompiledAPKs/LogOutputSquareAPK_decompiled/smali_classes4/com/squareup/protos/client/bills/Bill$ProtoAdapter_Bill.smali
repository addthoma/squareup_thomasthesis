.class final Lcom/squareup/protos/client/bills/Bill$ProtoAdapter_Bill;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Bill.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Bill;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Bill"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Bill;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 608
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Bill;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Bill;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 649
    new-instance v0, Lcom/squareup/protos/client/bills/Bill$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Bill$Builder;-><init>()V

    .line 650
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 651
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 674
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 672
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_loyalty_details(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;)Lcom/squareup/protos/client/bills/Bill$Builder;

    goto :goto_0

    .line 671
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Bill$Builder;->current_amendment_client_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Bill$Builder;

    goto :goto_0

    .line 670
    :pswitch_3
    sget-object v3, Lcom/squareup/orders/model/Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Bill$Builder;->order(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/bills/Bill$Builder;

    goto :goto_0

    .line 669
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_electronic_signature_details(Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;)Lcom/squareup/protos/client/bills/Bill$Builder;

    goto :goto_0

    .line 668
    :pswitch_5
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->refund:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/Refund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 662
    :pswitch_6
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/SquareProduct;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/SquareProduct;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/Bill$Builder;->square_product(Lcom/squareup/protos/client/bills/SquareProduct;)Lcom/squareup/protos/client/bills/Bill$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 664
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/Bill$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 659
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Bill$Builder;->square_product_attributes(Lcom/squareup/protos/client/bills/SquareProductAttributes;)Lcom/squareup/protos/client/bills/Bill$Builder;

    goto :goto_0

    .line 658
    :pswitch_8
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_refund:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/RefundV1;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 657
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/client/bills/Bill$Dates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Bill$Dates;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Bill$Builder;->dates(Lcom/squareup/protos/client/bills/Bill$Dates;)Lcom/squareup/protos/client/bills/Bill$Builder;

    goto/16 :goto_0

    .line 656
    :pswitch_a
    sget-object v3, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Bill$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/bills/Bill$Builder;

    goto/16 :goto_0

    .line 655
    :pswitch_b
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Bill$Builder;->tender:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 654
    :pswitch_c
    sget-object v3, Lcom/squareup/protos/client/Merchant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Merchant;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Bill$Builder;->merchant(Lcom/squareup/protos/client/Merchant;)Lcom/squareup/protos/client/bills/Bill$Builder;

    goto/16 :goto_0

    .line 653
    :pswitch_d
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Bill$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Bill$Builder;

    goto/16 :goto_0

    .line 678
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Bill$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 679
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Bill$Builder;->build()Lcom/squareup/protos/client/bills/Bill;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_d
        :pswitch_c
        :pswitch_0
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_0
        :pswitch_8
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 606
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Bill$ProtoAdapter_Bill;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Bill;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Bill;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 631
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 632
    sget-object v0, Lcom/squareup/protos/client/Merchant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Bill;->merchant:Lcom/squareup/protos/client/Merchant;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 633
    sget-object v0, Lcom/squareup/protos/client/bills/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Bill;->tender:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 634
    sget-object v0, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 635
    sget-object v0, Lcom/squareup/protos/client/bills/Bill$Dates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Bill;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 636
    sget-object v0, Lcom/squareup/protos/client/bills/RefundV1;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Bill;->read_only_refund:Ljava/util/List;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 637
    sget-object v0, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Bill;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 638
    sget-object v0, Lcom/squareup/protos/client/bills/SquareProduct;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Bill;->square_product:Lcom/squareup/protos/client/bills/SquareProduct;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 639
    sget-object v0, Lcom/squareup/protos/client/bills/Refund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 640
    sget-object v0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Bill;->read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 641
    sget-object v0, Lcom/squareup/orders/model/Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Bill;->order:Lcom/squareup/orders/model/Order;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 642
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Bill;->current_amendment_client_token:Ljava/lang/String;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 643
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Bill;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 644
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Bill;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 606
    check-cast p2, Lcom/squareup/protos/client/bills/Bill;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Bill$ProtoAdapter_Bill;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Bill;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Bill;)I
    .locals 4

    .line 613
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/Merchant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Bill;->merchant:Lcom/squareup/protos/client/Merchant;

    const/4 v3, 0x2

    .line 614
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 615
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Bill;->tender:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    const/4 v3, 0x5

    .line 616
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Bill$Dates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Bill;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    const/4 v3, 0x6

    .line 617
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/RefundV1;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 618
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Bill;->read_only_refund:Ljava/util/List;

    const/16 v3, 0x8

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Bill;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    const/16 v3, 0x9

    .line 619
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProduct;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Bill;->square_product:Lcom/squareup/protos/client/bills/SquareProduct;

    const/16 v3, 0xc

    .line 620
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Refund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 621
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Bill;->refund:Ljava/util/List;

    const/16 v3, 0xe

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Bill;->read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    const/16 v3, 0xf

    .line 622
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Bill;->order:Lcom/squareup/orders/model/Order;

    const/16 v3, 0x10

    .line 623
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Bill;->current_amendment_client_token:Ljava/lang/String;

    const/16 v3, 0x11

    .line 624
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Bill;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    const/16 v3, 0x12

    .line 625
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 626
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Bill;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 606
    check-cast p1, Lcom/squareup/protos/client/bills/Bill;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Bill$ProtoAdapter_Bill;->encodedSize(Lcom/squareup/protos/client/bills/Bill;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/protos/client/bills/Bill;
    .locals 2

    .line 684
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Bill;->newBuilder()Lcom/squareup/protos/client/bills/Bill$Builder;

    move-result-object p1

    .line 685
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 686
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/Merchant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Merchant;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    .line 687
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->tender:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 688
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 689
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/bills/Bill$Dates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Bill$Dates;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    .line 690
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_refund:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/RefundV1;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 691
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 692
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->refund:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/Refund;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 693
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_electronic_signature_details:Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    .line 694
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->order:Lcom/squareup/orders/model/Order;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/orders/model/Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->order:Lcom/squareup/orders/model/Order;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->order:Lcom/squareup/orders/model/Order;

    .line 695
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Builder;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    .line 696
    :cond_7
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Bill$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 697
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Bill$Builder;->build()Lcom/squareup/protos/client/bills/Bill;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 606
    check-cast p1, Lcom/squareup/protos/client/bills/Bill;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Bill$ProtoAdapter_Bill;->redact(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/protos/client/bills/Bill;

    move-result-object p1

    return-object p1
.end method
