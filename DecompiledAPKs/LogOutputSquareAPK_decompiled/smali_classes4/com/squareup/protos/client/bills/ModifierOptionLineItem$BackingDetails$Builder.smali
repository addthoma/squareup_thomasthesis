.class public final Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ModifierOptionLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

.field public modifier_list:Lcom/squareup/api/items/ItemModifierList;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 662
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public backing_modifier_option(Lcom/squareup/api/items/ItemModifierOption;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;
    .locals 0

    .line 669
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;
    .locals 4

    .line 683
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->backing_modifier_option:Lcom/squareup/api/items/ItemModifierOption;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;-><init>(Lcom/squareup/api/items/ItemModifierOption;Lcom/squareup/api/items/ItemModifierList;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 657
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    move-result-object v0

    return-object v0
.end method

.method public modifier_list(Lcom/squareup/api/items/ItemModifierList;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;
    .locals 0

    .line 677
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->modifier_list:Lcom/squareup/api/items/ItemModifierList;

    return-object p0
.end method
