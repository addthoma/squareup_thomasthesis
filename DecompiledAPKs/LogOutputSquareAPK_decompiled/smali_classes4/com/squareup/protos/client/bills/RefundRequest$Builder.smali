.class public final Lcom/squareup/protos/client/bills/RefundRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RefundRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/RefundRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/RefundRequest;",
        "Lcom/squareup/protos/client/bills/RefundRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amendment_token:Ljava/lang/String;

.field public presented_card_details:Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;

.field public refund:Lcom/squareup/protos/client/bills/Refund;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amendment_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/RefundRequest$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RefundRequest$Builder;->amendment_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/RefundRequest;
    .locals 5

    .line 150
    new-instance v0, Lcom/squareup/protos/client/bills/RefundRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RefundRequest$Builder;->refund:Lcom/squareup/protos/client/bills/Refund;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/RefundRequest$Builder;->presented_card_details:Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/RefundRequest$Builder;->amendment_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/RefundRequest;-><init>(Lcom/squareup/protos/client/bills/Refund;Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RefundRequest$Builder;->build()Lcom/squareup/protos/client/bills/RefundRequest;

    move-result-object v0

    return-object v0
.end method

.method public presented_card_details(Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;)Lcom/squareup/protos/client/bills/RefundRequest$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RefundRequest$Builder;->presented_card_details:Lcom/squareup/protos/client/bills/RefundRequest$PresentedCardDetails;

    return-object p0
.end method

.method public refund(Lcom/squareup/protos/client/bills/Refund;)Lcom/squareup/protos/client/bills/RefundRequest$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RefundRequest$Builder;->refund:Lcom/squareup/protos/client/bills/Refund;

    return-object p0
.end method
