.class final Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$ProtoAdapter_QueryParams;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetBillsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_QueryParams"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 739
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 758
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;-><init>()V

    .line 759
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 760
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 765
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 763
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->instrument_search(Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;)Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;

    goto :goto_0

    .line 762
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->query_string(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;

    goto :goto_0

    .line 769
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 770
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->build()Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 737
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$ProtoAdapter_QueryParams;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 751
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->query_string:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 752
    sget-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->instrument_search:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 753
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 737
    check-cast p2, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$ProtoAdapter_QueryParams;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;)I
    .locals 4

    .line 744
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->query_string:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->instrument_search:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    const/4 v3, 0x2

    .line 745
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 746
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 737
    check-cast p1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$ProtoAdapter_QueryParams;->encodedSize(Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;)Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;
    .locals 2

    .line 775
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->newBuilder()Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;

    move-result-object p1

    .line 776
    iget-object v0, p1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->instrument_search:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->instrument_search:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->instrument_search:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    .line 777
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 778
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->build()Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 737
    check-cast p1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$ProtoAdapter_QueryParams;->redact(Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;)Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    move-result-object p1

    return-object p1
.end method
