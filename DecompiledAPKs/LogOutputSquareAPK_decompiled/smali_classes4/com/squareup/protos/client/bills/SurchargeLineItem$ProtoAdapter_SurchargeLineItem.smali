.class final Lcom/squareup/protos/client/bills/SurchargeLineItem$ProtoAdapter_SurchargeLineItem;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SurchargeLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/SurchargeLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SurchargeLineItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/SurchargeLineItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 496
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/SurchargeLineItem;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/SurchargeLineItem;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 521
    new-instance v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;-><init>()V

    .line 522
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 523
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 531
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 529
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->fee:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/FeeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 528
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->write_only_deleted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;

    goto :goto_0

    .line 527
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;)Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;

    goto :goto_0

    .line 526
    :cond_3
    sget-object v3, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->backing_details(Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;

    goto :goto_0

    .line 525
    :cond_4
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->surcharge_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;

    goto :goto_0

    .line 535
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 536
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->build()Lcom/squareup/protos/client/bills/SurchargeLineItem;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 494
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$ProtoAdapter_SurchargeLineItem;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/SurchargeLineItem;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/SurchargeLineItem;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 511
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/SurchargeLineItem;->surcharge_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 512
    sget-object v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/SurchargeLineItem;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 513
    sget-object v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/SurchargeLineItem;->amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 514
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/SurchargeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 515
    sget-object v0, Lcom/squareup/protos/client/bills/FeeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/SurchargeLineItem;->fee:Ljava/util/List;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 516
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/SurchargeLineItem;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 494
    check-cast p2, Lcom/squareup/protos/client/bills/SurchargeLineItem;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/SurchargeLineItem$ProtoAdapter_SurchargeLineItem;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/SurchargeLineItem;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/SurchargeLineItem;)I
    .locals 4

    .line 501
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem;->surcharge_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    const/4 v3, 0x2

    .line 502
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem;->amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    const/4 v3, 0x3

    .line 503
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    const/4 v3, 0x4

    .line 504
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/FeeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 505
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem;->fee:Ljava/util/List;

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 506
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SurchargeLineItem;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 494
    check-cast p1, Lcom/squareup/protos/client/bills/SurchargeLineItem;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$ProtoAdapter_SurchargeLineItem;->encodedSize(Lcom/squareup/protos/client/bills/SurchargeLineItem;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/SurchargeLineItem;)Lcom/squareup/protos/client/bills/SurchargeLineItem;
    .locals 2

    .line 541
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SurchargeLineItem;->newBuilder()Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;

    move-result-object p1

    .line 542
    iget-object v0, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->surcharge_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->surcharge_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->surcharge_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 543
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->backing_details:Lcom/squareup/protos/client/bills/SurchargeLineItem$BackingDetails;

    .line 544
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/SurchargeLineItem$Amounts;

    .line 545
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->fee:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/FeeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 546
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 547
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$Builder;->build()Lcom/squareup/protos/client/bills/SurchargeLineItem;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 494
    check-cast p1, Lcom/squareup/protos/client/bills/SurchargeLineItem;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/SurchargeLineItem$ProtoAdapter_SurchargeLineItem;->redact(Lcom/squareup/protos/client/bills/SurchargeLineItem;)Lcom/squareup/protos/client/bills/SurchargeLineItem;

    move-result-object p1

    return-object p1
.end method
