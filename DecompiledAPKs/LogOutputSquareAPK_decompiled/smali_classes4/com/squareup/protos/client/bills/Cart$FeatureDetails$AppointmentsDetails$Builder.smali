.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public appointment_id:Ljava/lang/String;

.field public appointment_staff:Lcom/squareup/protos/client/CreatorDetails;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3210
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public appointment_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;
    .locals 0

    .line 3214
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;->appointment_id:Ljava/lang/String;

    return-object p0
.end method

.method public appointment_staff(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;
    .locals 0

    .line 3222
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;->appointment_staff:Lcom/squareup/protos/client/CreatorDetails;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;
    .locals 4

    .line 3228
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;->appointment_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;->appointment_staff:Lcom/squareup/protos/client/CreatorDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/CreatorDetails;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 3205
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    move-result-object v0

    return-object v0
.end method
