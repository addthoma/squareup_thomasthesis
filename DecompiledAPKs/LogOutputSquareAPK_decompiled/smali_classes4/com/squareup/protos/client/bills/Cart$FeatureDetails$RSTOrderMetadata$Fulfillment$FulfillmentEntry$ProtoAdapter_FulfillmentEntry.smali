.class final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$ProtoAdapter_FulfillmentEntry;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FulfillmentEntry"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 5972
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5993
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;-><init>()V

    .line 5994
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 5995
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 6001
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 5999
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;

    goto :goto_0

    .line 5998
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->itemization_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;

    goto :goto_0

    .line 5997
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->fulfillment_entry_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;

    goto :goto_0

    .line 6005
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 6006
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5970
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$ProtoAdapter_FulfillmentEntry;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5985
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->fulfillment_entry_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5986
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5987
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->quantity:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5988
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5970
    check-cast p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$ProtoAdapter_FulfillmentEntry;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;)I
    .locals 4

    .line 5977
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->fulfillment_entry_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v3, 0x2

    .line 5978
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->quantity:Ljava/lang/String;

    const/4 v3, 0x3

    .line 5979
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5980
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 5970
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$ProtoAdapter_FulfillmentEntry;->encodedSize(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;
    .locals 2

    .line 6011
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;

    move-result-object p1

    .line 6012
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 6013
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 6014
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 5970
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry$ProtoAdapter_FulfillmentEntry;->redact(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$RSTOrderMetadata$Fulfillment$FulfillmentEntry;

    move-result-object p1

    return-object p1
.end method
