.class final Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$ProtoAdapter_GetBillFamiliesRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetBillFamiliesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetBillFamiliesRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 678
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 707
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;-><init>()V

    .line 708
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 709
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 719
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 717
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->populate_tender_contact_data(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;

    goto :goto_0

    .line 716
    :pswitch_1
    iget-object v3, v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->unit_token:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 715
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->exclude_related_bills(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;

    goto :goto_0

    .line 714
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;

    goto :goto_0

    .line 713
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->pagination_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;

    goto :goto_0

    .line 712
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;

    goto :goto_0

    .line 711
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->query(Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;

    goto :goto_0

    .line 723
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 724
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 676
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$ProtoAdapter_GetBillFamiliesRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 695
    sget-object v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;->query:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 696
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;->limit:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 697
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;->pagination_token:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 698
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 699
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;->exclude_related_bills:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 700
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;->unit_token:Ljava/util/List;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 701
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;->populate_tender_contact_data:Ljava/lang/Boolean;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 702
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 676
    check-cast p2, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$ProtoAdapter_GetBillFamiliesRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;)I
    .locals 4

    .line 683
    sget-object v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;->query:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;->limit:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 684
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;->pagination_token:Ljava/lang/String;

    const/4 v3, 0x3

    .line 685
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;->merchant_token:Ljava/lang/String;

    const/4 v3, 0x4

    .line 686
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;->exclude_related_bills:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 687
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 688
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;->unit_token:Ljava/util/List;

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;->populate_tender_contact_data:Ljava/lang/Boolean;

    const/4 v3, 0x7

    .line 689
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 690
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 676
    check-cast p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$ProtoAdapter_GetBillFamiliesRequest;->encodedSize(Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;
    .locals 2

    .line 729
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;->newBuilder()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;

    move-result-object p1

    .line 730
    iget-object v0, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->query:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->query:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->query:Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    .line 731
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 732
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 676
    check-cast p1, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$ProtoAdapter_GetBillFamiliesRequest;->redact(Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;

    move-result-object p1

    return-object p1
.end method
