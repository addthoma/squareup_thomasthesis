.class public final Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetBillsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;",
        "Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public instrument_search:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

.field public query_string:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 710
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;
    .locals 4

    .line 733
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->query_string:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->instrument_search:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 705
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->build()Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    move-result-object v0

    return-object v0
.end method

.method public instrument_search(Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;)Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;
    .locals 0

    .line 726
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->instrument_search:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    const/4 p1, 0x0

    .line 727
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->query_string:Ljava/lang/String;

    return-object p0
.end method

.method public query_string(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;
    .locals 0

    .line 717
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->query_string:Ljava/lang/String;

    const/4 p1, 0x0

    .line 718
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->instrument_search:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    return-object p0
.end method
