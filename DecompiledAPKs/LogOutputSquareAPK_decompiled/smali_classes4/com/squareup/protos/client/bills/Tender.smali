.class public final Lcom/squareup/protos/client/bills/Tender;
.super Lcom/squareup/wire/Message;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Tender$ProtoAdapter_Tender;,
        Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;,
        Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;,
        Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;,
        Lcom/squareup/protos/client/bills/Tender$Amounts;,
        Lcom/squareup/protos/client/bills/Tender$Method;,
        Lcom/squareup/protos/client/bills/Tender$State;,
        Lcom/squareup/protos/client/bills/Tender$Type;,
        Lcom/squareup/protos/client/bills/Tender$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Tender;",
        "Lcom/squareup/protos/client/bills/Tender$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Tender;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CHECKOUT_APP_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CHECKOUT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CREATE_PAYMENT_NOTE:Ljava/lang/String; = ""

.field public static final DEFAULT_READ_ONLY_CONTACT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_READ_ONLY_RECEIPT_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_READ_ONLY_REFUND_CARD_PRESENCE_REQUIREMENT:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

.field public static final DEFAULT_READ_ONLY_SEQUENTIAL_TENDER_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_READ_ONLY_STATE:Lcom/squareup/protos/client/bills/Tender$State;

.field public static final DEFAULT_REFERENCE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/bills/Tender$Type;

.field public static final DEFAULT_WRITE_ONLY_CLIENT_CASH_DRAWER_SHIFT_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender$Amounts#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final checkout_app_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1b
    .end annotation
.end field

.field public final checkout_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x18
    .end annotation
.end field

.field public final client_details:Lcom/squareup/protos/client/bills/ClientDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.ClientDetails#ADAPTER"
        tag = 0x15
    .end annotation
.end field

.field public final create_payment_note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1a
    .end annotation
.end field

.field public final creator_details:Lcom/squareup/protos/client/CreatorDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.CreatorDetails#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender$CompleteTenderDetails#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final method:Lcom/squareup/protos/client/bills/Tender$Method;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender$Method#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final open_ticket_owner:Lcom/squareup/protos/client/CreatorDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.CreatorDetails#ADAPTER"
        tag = 0x16
    .end annotation
.end field

.field public final peripheral_metadata:Lcom/squareup/protos/common/payment/PeripheralMetadata;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.payment.PeripheralMetadata#ADAPTER"
        tag = 0x17
    .end annotation
.end field

.field public final read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Buyer#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final read_only_contact_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1c
    .end annotation
.end field

.field public final read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender$LoyaltyDetails#ADAPTER"
        tag = 0x14
    .end annotation
.end field

.field public final read_only_receipt_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final read_only_refund_card_presence_requirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender$RefundCardPresenceRequirement#ADAPTER"
        tag = 0x12
    .end annotation
.end field

.field public final read_only_sequential_tender_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x11
    .end annotation
.end field

.field public final read_only_state:Lcom/squareup/protos/client/bills/Tender$State;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender$State#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final reference_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x19
    .end annotation
.end field

.field public final tender_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final tender_tag:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.TenderTag#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x13
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/TenderTag;",
            ">;"
        }
    .end annotation
.end field

.field public final tendered_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/bills/Tender$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender$Type#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final write_only_client_cash_drawer_shift_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xe
    .end annotation
.end field

.field public final write_only_location:Lcom/squareup/protos/client/GeoLocation;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.GeoLocation#ADAPTER"
        tag = 0x7
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$ProtoAdapter_Tender;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$ProtoAdapter_Tender;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 39
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->UNKNOWN:Lcom/squareup/protos/client/bills/Tender$Type;

    sput-object v0, Lcom/squareup/protos/client/bills/Tender;->DEFAULT_TYPE:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 41
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$State;->UNKNOWN_STATE:Lcom/squareup/protos/client/bills/Tender$State;

    sput-object v0, Lcom/squareup/protos/client/bills/Tender;->DEFAULT_READ_ONLY_STATE:Lcom/squareup/protos/client/bills/Tender$State;

    .line 49
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->REFUND_CARD_PRESENCE_NOT_REQUIRED:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    sput-object v0, Lcom/squareup/protos/client/bills/Tender;->DEFAULT_READ_ONLY_REFUND_CARD_PRESENCE_REQUIREMENT:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/Tender$Builder;Lokio/ByteString;)V
    .locals 1

    .line 290
    sget-object v0, Lcom/squareup/protos/client/bills/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 291
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 292
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 293
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_state:Lcom/squareup/protos/client/bills/Tender$State;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_state:Lcom/squareup/protos/client/bills/Tender$State;

    .line 294
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    .line 295
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 296
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    .line 297
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->write_only_location:Lcom/squareup/protos/client/GeoLocation;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->write_only_location:Lcom/squareup/protos/client/GeoLocation;

    .line 298
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

    .line 299
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_receipt_number:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_receipt_number:Ljava/lang/String;

    .line 300
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 301
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 302
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->write_only_client_cash_drawer_shift_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->write_only_client_cash_drawer_shift_id:Ljava/lang/String;

    .line 303
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    .line 304
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_sequential_tender_number:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_sequential_tender_number:Ljava/lang/String;

    .line 305
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_refund_card_presence_requirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_refund_card_presence_requirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    .line 306
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->tender_tag:Ljava/util/List;

    const-string v0, "tender_tag"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->tender_tag:Ljava/util/List;

    .line 307
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    .line 308
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->client_details:Lcom/squareup/protos/client/bills/ClientDetails;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->client_details:Lcom/squareup/protos/client/bills/ClientDetails;

    .line 309
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->open_ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->open_ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    .line 310
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->peripheral_metadata:Lcom/squareup/protos/common/payment/PeripheralMetadata;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->peripheral_metadata:Lcom/squareup/protos/common/payment/PeripheralMetadata;

    .line 311
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->checkout_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->checkout_id:Ljava/lang/String;

    .line 312
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->reference_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->reference_id:Ljava/lang/String;

    .line 313
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->create_payment_note:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->create_payment_note:Ljava/lang/String;

    .line 314
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->checkout_app_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/bills/Tender;->checkout_app_id:Ljava/lang/String;

    .line 315
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_contact_token:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_contact_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 353
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Tender;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 354
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Tender;

    .line 355
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 356
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 357
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_state:Lcom/squareup/protos/client/bills/Tender$State;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->read_only_state:Lcom/squareup/protos/client/bills/Tender$State;

    .line 358
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    .line 359
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 360
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    .line 361
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->write_only_location:Lcom/squareup/protos/client/GeoLocation;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->write_only_location:Lcom/squareup/protos/client/GeoLocation;

    .line 362
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

    .line 363
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_receipt_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->read_only_receipt_number:Ljava/lang/String;

    .line 364
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 365
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 366
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->write_only_client_cash_drawer_shift_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->write_only_client_cash_drawer_shift_id:Ljava/lang/String;

    .line 367
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    .line 368
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_sequential_tender_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->read_only_sequential_tender_number:Ljava/lang/String;

    .line 369
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_refund_card_presence_requirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->read_only_refund_card_presence_requirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    .line 370
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->tender_tag:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->tender_tag:Ljava/util/List;

    .line 371
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    .line 372
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->client_details:Lcom/squareup/protos/client/bills/ClientDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->client_details:Lcom/squareup/protos/client/bills/ClientDetails;

    .line 373
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->open_ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->open_ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    .line 374
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->peripheral_metadata:Lcom/squareup/protos/common/payment/PeripheralMetadata;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->peripheral_metadata:Lcom/squareup/protos/common/payment/PeripheralMetadata;

    .line 375
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->checkout_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->checkout_id:Ljava/lang/String;

    .line 376
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->reference_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->reference_id:Ljava/lang/String;

    .line 377
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->create_payment_note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->create_payment_note:Ljava/lang/String;

    .line 378
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->checkout_app_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Tender;->checkout_app_id:Ljava/lang/String;

    .line 379
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_contact_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Tender;->read_only_contact_token:Ljava/lang/String;

    .line 380
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 385
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_18

    .line 387
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 388
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 389
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$Type;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 390
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_state:Lcom/squareup/protos/client/bills/Tender$State;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$State;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 391
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$Method;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 392
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 393
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$Amounts;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 394
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->write_only_location:Lcom/squareup/protos/client/GeoLocation;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/GeoLocation;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 395
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/client/Buyer;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 396
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_receipt_number:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 397
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 398
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 399
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->write_only_client_cash_drawer_shift_id:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 400
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 401
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_sequential_tender_number:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 402
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_refund_card_presence_requirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 403
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->tender_tag:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 404
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 405
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->client_details:Lcom/squareup/protos/client/bills/ClientDetails;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/ClientDetails;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 406
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->open_ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 407
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->peripheral_metadata:Lcom/squareup/protos/common/payment/PeripheralMetadata;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/squareup/protos/common/payment/PeripheralMetadata;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 408
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->checkout_id:Ljava/lang/String;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 409
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->reference_id:Ljava/lang/String;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 410
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->create_payment_note:Ljava/lang/String;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 411
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->checkout_app_id:Ljava/lang/String;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 412
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_contact_token:Ljava/lang/String;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_17
    add-int/2addr v0, v2

    .line 413
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_18
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Tender$Builder;
    .locals 2

    .line 320
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Builder;-><init>()V

    .line 321
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 322
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 323
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_state:Lcom/squareup/protos/client/bills/Tender$State;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_state:Lcom/squareup/protos/client/bills/Tender$State;

    .line 324
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    .line 325
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 326
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    .line 327
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->write_only_location:Lcom/squareup/protos/client/GeoLocation;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->write_only_location:Lcom/squareup/protos/client/GeoLocation;

    .line 328
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

    .line 329
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_receipt_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_receipt_number:Ljava/lang/String;

    .line 330
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 331
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 332
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->write_only_client_cash_drawer_shift_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->write_only_client_cash_drawer_shift_id:Ljava/lang/String;

    .line 333
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    .line 334
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_sequential_tender_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_sequential_tender_number:Ljava/lang/String;

    .line 335
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_refund_card_presence_requirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_refund_card_presence_requirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    .line 336
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->tender_tag:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->tender_tag:Ljava/util/List;

    .line 337
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    .line 338
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->client_details:Lcom/squareup/protos/client/bills/ClientDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->client_details:Lcom/squareup/protos/client/bills/ClientDetails;

    .line 339
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->open_ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->open_ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    .line 340
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->peripheral_metadata:Lcom/squareup/protos/common/payment/PeripheralMetadata;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->peripheral_metadata:Lcom/squareup/protos/common/payment/PeripheralMetadata;

    .line 341
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->checkout_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->checkout_id:Ljava/lang/String;

    .line 342
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->reference_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->reference_id:Ljava/lang/String;

    .line 343
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->create_payment_note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->create_payment_note:Ljava/lang/String;

    .line 344
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->checkout_app_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->checkout_app_id:Ljava/lang/String;

    .line 345
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_contact_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Tender$Builder;->read_only_contact_token:Ljava/lang/String;

    .line 346
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender;->newBuilder()Lcom/squareup/protos/client/bills/Tender$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 420
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 421
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", tender_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 422
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    if-eqz v1, :cond_1

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 423
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_state:Lcom/squareup/protos/client/bills/Tender$State;

    if-eqz v1, :cond_2

    const-string v1, ", read_only_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_state:Lcom/squareup/protos/client/bills/Tender$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 424
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    if-eqz v1, :cond_3

    const-string v1, ", method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 425
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_4

    const-string v1, ", tendered_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->tendered_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 426
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    if-eqz v1, :cond_5

    const-string v1, ", amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 427
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->write_only_location:Lcom/squareup/protos/client/GeoLocation;

    if-eqz v1, :cond_6

    const-string v1, ", write_only_location="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->write_only_location:Lcom/squareup/protos/client/GeoLocation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 428
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

    if-eqz v1, :cond_7

    const-string v1, ", read_only_buyer_profile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_buyer_profile:Lcom/squareup/protos/client/Buyer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 429
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_receipt_number:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", read_only_receipt_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_receipt_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 430
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_9

    const-string v1, ", read_only_last_refundable_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_last_refundable_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 431
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_a

    const-string v1, ", creator_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 432
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->write_only_client_cash_drawer_shift_id:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", write_only_client_cash_drawer_shift_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->write_only_client_cash_drawer_shift_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 433
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    if-eqz v1, :cond_c

    const-string v1, ", extra_tender_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 434
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_sequential_tender_number:Ljava/lang/String;

    if-eqz v1, :cond_d

    const-string v1, ", read_only_sequential_tender_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_sequential_tender_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 435
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_refund_card_presence_requirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    if-eqz v1, :cond_e

    const-string v1, ", read_only_refund_card_presence_requirement="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_refund_card_presence_requirement:Lcom/squareup/protos/client/bills/Tender$RefundCardPresenceRequirement;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 436
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->tender_tag:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_f

    const-string v1, ", tender_tag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->tender_tag:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 437
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    if-eqz v1, :cond_10

    const-string v1, ", read_only_loyalty_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_loyalty_details:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 438
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->client_details:Lcom/squareup/protos/client/bills/ClientDetails;

    if-eqz v1, :cond_11

    const-string v1, ", client_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->client_details:Lcom/squareup/protos/client/bills/ClientDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 439
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->open_ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_12

    const-string v1, ", open_ticket_owner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->open_ticket_owner:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 440
    :cond_12
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->peripheral_metadata:Lcom/squareup/protos/common/payment/PeripheralMetadata;

    if-eqz v1, :cond_13

    const-string v1, ", peripheral_metadata="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->peripheral_metadata:Lcom/squareup/protos/common/payment/PeripheralMetadata;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 441
    :cond_13
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->checkout_id:Ljava/lang/String;

    if-eqz v1, :cond_14

    const-string v1, ", checkout_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->checkout_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 442
    :cond_14
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->reference_id:Ljava/lang/String;

    if-eqz v1, :cond_15

    const-string v1, ", reference_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->reference_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 443
    :cond_15
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->create_payment_note:Ljava/lang/String;

    if-eqz v1, :cond_16

    const-string v1, ", create_payment_note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->create_payment_note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 444
    :cond_16
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->checkout_app_id:Ljava/lang/String;

    if-eqz v1, :cond_17

    const-string v1, ", checkout_app_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->checkout_app_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    :cond_17
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_contact_token:Ljava/lang/String;

    if-eqz v1, :cond_18

    const-string v1, ", read_only_contact_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender;->read_only_contact_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_18
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Tender{"

    .line 446
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
