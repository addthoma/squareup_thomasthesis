.class public final Lcom/squareup/protos/client/bills/AddedTender;
.super Lcom/squareup/wire/Message;
.source "AddedTender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/AddedTender$ProtoAdapter_AddedTender;,
        Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;,
        Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;,
        Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;,
        Lcom/squareup/protos/client/bills/AddedTender$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/AddedTender;",
        "Lcom/squareup/protos/client/bills/AddedTender$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/AddedTender;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final cart_punch_status:Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.AddedTender$CartPunchStatus#ADAPTER"
        tag = 0x6
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final coupon:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.coupons.Coupon#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;"
        }
    .end annotation
.end field

.field public final loyalty_status:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.AddedTender$LoyaltyStatus#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;",
            ">;"
        }
    .end annotation
.end field

.field public final receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.AddedTender$ReceiptDetails#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final remaining_balance_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final tender:Lcom/squareup/protos/client/bills/Tender;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Tender#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$ProtoAdapter_AddedTender;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddedTender$ProtoAdapter_AddedTender;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;Lcom/squareup/protos/common/Money;Ljava/util/List;Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Lcom/squareup/protos/client/bills/Tender;",
            "Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;",
            "Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;",
            ">;)V"
        }
    .end annotation

    .line 108
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/AddedTender;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;Lcom/squareup/protos/common/Money;Ljava/util/List;Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;Lcom/squareup/protos/common/Money;Ljava/util/List;Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/Status;",
            "Lcom/squareup/protos/client/bills/Tender;",
            "Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;",
            "Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 114
    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender;->status:Lcom/squareup/protos/client/Status;

    .line 116
    iput-object p2, p0, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    .line 117
    iput-object p3, p0, Lcom/squareup/protos/client/bills/AddedTender;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    .line 118
    iput-object p4, p0, Lcom/squareup/protos/client/bills/AddedTender;->remaining_balance_money:Lcom/squareup/protos/common/Money;

    const-string p1, "coupon"

    .line 119
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender;->coupon:Ljava/util/List;

    .line 120
    iput-object p6, p0, Lcom/squareup/protos/client/bills/AddedTender;->cart_punch_status:Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;

    const-string p1, "loyalty_status"

    .line 121
    invoke-static {p1, p7}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender;->loyalty_status:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 141
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/AddedTender;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 142
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/AddedTender;

    .line 143
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddedTender;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddedTender;->status:Lcom/squareup/protos/client/Status;

    .line 144
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    .line 145
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddedTender;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    .line 146
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->remaining_balance_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddedTender;->remaining_balance_money:Lcom/squareup/protos/common/Money;

    .line 147
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->coupon:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddedTender;->coupon:Ljava/util/List;

    .line 148
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->cart_punch_status:Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddedTender;->cart_punch_status:Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;

    .line 149
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->loyalty_status:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddedTender;->loyalty_status:Ljava/util/List;

    .line 150
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 155
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 157
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Tender;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 160
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->remaining_balance_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->coupon:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->cart_punch_status:Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 164
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->loyalty_status:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 165
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/AddedTender$Builder;
    .locals 2

    .line 126
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddedTender$Builder;-><init>()V

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->tender:Lcom/squareup/protos/client/bills/Tender;

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->remaining_balance_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->remaining_balance_money:Lcom/squareup/protos/common/Money;

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->coupon:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->coupon:Ljava/util/List;

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->cart_punch_status:Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->cart_punch_status:Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->loyalty_status:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/AddedTender$Builder;->loyalty_status:Ljava/util/List;

    .line 134
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/AddedTender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender;->newBuilder()Lcom/squareup/protos/client/bills/AddedTender$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 174
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    if-eqz v1, :cond_1

    const-string v1, ", tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 175
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    if-eqz v1, :cond_2

    const-string v1, ", receipt_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->receipt_details:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 176
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->remaining_balance_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", remaining_balance_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->remaining_balance_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 177
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->coupon:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", coupon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->coupon:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 178
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->cart_punch_status:Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;

    if-eqz v1, :cond_5

    const-string v1, ", cart_punch_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->cart_punch_status:Lcom/squareup/protos/client/bills/AddedTender$CartPunchStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 179
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->loyalty_status:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ", loyalty_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender;->loyalty_status:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AddedTender{"

    .line 180
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
