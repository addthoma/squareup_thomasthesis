.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public reopened_at:Lcom/squareup/protos/client/ISO8601Date;

.field public tender_display_details:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 4824
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 4825
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;->tender_display_details:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;
    .locals 4

    .line 4848
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;->reopened_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;->tender_display_details:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;-><init>(Lcom/squareup/protos/client/ISO8601Date;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4819
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails;

    move-result-object v0

    return-object v0
.end method

.method public reopened_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;
    .locals 0

    .line 4832
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;->reopened_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public tender_display_details(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$TenderDisplayDetails;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;"
        }
    .end annotation

    .line 4841
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 4842
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$ReopenDetails$Builder;->tender_display_details:Ljava/util/List;

    return-object p0
.end method
