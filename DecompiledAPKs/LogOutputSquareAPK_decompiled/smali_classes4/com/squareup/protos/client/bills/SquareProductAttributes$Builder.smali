.class public final Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SquareProductAttributes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/SquareProductAttributes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/SquareProductAttributes;",
        "Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public register:Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

.field public retail_point_of_sale:Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/SquareProductAttributes;
    .locals 4

    .line 112
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;->register:Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;->retail_point_of_sale:Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/SquareProductAttributes;-><init>(Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;->build()Lcom/squareup/protos/client/bills/SquareProductAttributes;

    move-result-object v0

    return-object v0
.end method

.method public register(Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;)Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;->register:Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    return-object p0
.end method

.method public retail_point_of_sale(Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;)Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;->retail_point_of_sale:Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;

    return-object p0
.end method
