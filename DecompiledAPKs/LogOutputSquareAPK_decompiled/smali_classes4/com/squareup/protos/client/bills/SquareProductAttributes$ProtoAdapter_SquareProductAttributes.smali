.class final Lcom/squareup/protos/client/bills/SquareProductAttributes$ProtoAdapter_SquareProductAttributes;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SquareProductAttributes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/SquareProductAttributes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SquareProductAttributes"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/SquareProductAttributes;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 357
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/SquareProductAttributes;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 376
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;-><init>()V

    .line 377
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 378
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 383
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 381
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;->retail_point_of_sale(Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;)Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;

    goto :goto_0

    .line 380
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;->register(Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;)Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;

    goto :goto_0

    .line 387
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 388
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;->build()Lcom/squareup/protos/client/bills/SquareProductAttributes;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 355
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/SquareProductAttributes$ProtoAdapter_SquareProductAttributes;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/SquareProductAttributes;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/SquareProductAttributes;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 369
    sget-object v0, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/SquareProductAttributes;->register:Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 370
    sget-object v0, Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/SquareProductAttributes;->retail_point_of_sale:Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 371
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/SquareProductAttributes;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 355
    check-cast p2, Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/SquareProductAttributes$ProtoAdapter_SquareProductAttributes;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/SquareProductAttributes;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/SquareProductAttributes;)I
    .locals 4

    .line 362
    sget-object v0, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/SquareProductAttributes;->register:Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/SquareProductAttributes;->retail_point_of_sale:Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;

    const/4 v3, 0x2

    .line 363
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 364
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SquareProductAttributes;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 355
    check-cast p1, Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/SquareProductAttributes$ProtoAdapter_SquareProductAttributes;->encodedSize(Lcom/squareup/protos/client/bills/SquareProductAttributes;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/SquareProductAttributes;)Lcom/squareup/protos/client/bills/SquareProductAttributes;
    .locals 2

    .line 393
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SquareProductAttributes;->newBuilder()Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;

    move-result-object p1

    .line 394
    iget-object v0, p1, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;->register:Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;->register:Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;->register:Lcom/squareup/protos/client/bills/SquareProductAttributes$Register;

    .line 395
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;->retail_point_of_sale:Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;->retail_point_of_sale:Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;->retail_point_of_sale:Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;

    .line 396
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 397
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/SquareProductAttributes$Builder;->build()Lcom/squareup/protos/client/bills/SquareProductAttributes;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 355
    check-cast p1, Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/SquareProductAttributes$ProtoAdapter_SquareProductAttributes;->redact(Lcom/squareup/protos/client/bills/SquareProductAttributes;)Lcom/squareup/protos/client/bills/SquareProductAttributes;

    move-result-object p1

    return-object p1
.end method
