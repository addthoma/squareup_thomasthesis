.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CoursingOptions"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$ProtoAdapter_CoursingOptions;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final course:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$FeatureDetails$CoursingOptions$Course#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 3424
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$ProtoAdapter_CoursingOptions;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$ProtoAdapter_CoursingOptions;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;",
            ">;)V"
        }
    .end annotation

    .line 3436
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 3440
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p2, "course"

    .line 3441
    invoke-static {p2, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->course:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 3455
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 3456
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    .line 3457
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->course:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->course:Ljava/util/List;

    .line 3458
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 3463
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_0

    .line 3465
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 3466
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->course:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3467
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_0
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;
    .locals 2

    .line 3446
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;-><init>()V

    .line 3447
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->course:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;->course:Ljava/util/List;

    .line 3448
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 3423
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 3474
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 3475
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->course:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", course="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;->course:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CoursingOptions{"

    .line 3476
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
