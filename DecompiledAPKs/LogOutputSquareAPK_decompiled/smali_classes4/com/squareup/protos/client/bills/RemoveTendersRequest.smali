.class public final Lcom/squareup/protos/client/bills/RemoveTendersRequest;
.super Lcom/squareup/wire/Message;
.source "RemoveTendersRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/RemoveTendersRequest$ProtoAdapter_RemoveTendersRequest;,
        Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;,
        Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/RemoveTendersRequest;",
        "Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/RemoveTendersRequest;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final bill_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final merchant:Lcom/squareup/protos/client/Merchant;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Merchant#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final tenders_to_remove:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.RemoveTendersRequest$RemoveTender#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$ProtoAdapter_RemoveTendersRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$ProtoAdapter_RemoveTendersRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/client/Merchant;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;",
            ">;)V"
        }
    .end annotation

    .line 55
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/client/Merchant;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 60
    sget-object v0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 61
    iput-object p1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 62
    iput-object p2, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    const-string p1, "tenders_to_remove"

    .line 63
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->tenders_to_remove:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 79
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/RemoveTendersRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 80
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/RemoveTendersRequest;

    .line 81
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 82
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    .line 83
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->tenders_to_remove:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->tenders_to_remove:Ljava/util/List;

    .line 84
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 89
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/Merchant;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->tenders_to_remove:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;
    .locals 2

    .line 68
    new-instance v0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;-><init>()V

    .line 69
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 70
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;->merchant:Lcom/squareup/protos/client/Merchant;

    .line 71
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->tenders_to_remove:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;->tenders_to_remove:Ljava/util/List;

    .line 72
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->newBuilder()Lcom/squareup/protos/client/bills/RemoveTendersRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", bill_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 104
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    if-eqz v1, :cond_1

    const-string v1, ", merchant="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->merchant:Lcom/squareup/protos/client/Merchant;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 105
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->tenders_to_remove:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", tenders_to_remove="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/RemoveTendersRequest;->tenders_to_remove:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "RemoveTendersRequest{"

    .line 106
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
