.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public transfer_from_employee:Lcom/squareup/protos/client/Employee;

.field public transfer_to_employee:Lcom/squareup/protos/client/Employee;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6424
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;
    .locals 4

    .line 6445
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->transfer_from_employee:Lcom/squareup/protos/client/Employee;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->transfer_to_employee:Lcom/squareup/protos/client/Employee;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;-><init>(Lcom/squareup/protos/client/Employee;Lcom/squareup/protos/client/Employee;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 6419
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails;

    move-result-object v0

    return-object v0
.end method

.method public transfer_from_employee(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;
    .locals 0

    .line 6431
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->transfer_from_employee:Lcom/squareup/protos/client/Employee;

    return-object p0
.end method

.method public transfer_to_employee(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;
    .locals 0

    .line 6439
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$LifecycleEvent$OwnershipTransferDetails$Builder;->transfer_to_employee:Lcom/squareup/protos/client/Employee;

    return-object p0
.end method
