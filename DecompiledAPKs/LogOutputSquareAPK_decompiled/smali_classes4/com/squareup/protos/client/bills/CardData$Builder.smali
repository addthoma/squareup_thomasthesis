.class public final Lcom/squareup/protos/client/bills/CardData$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CardData;",
        "Lcom/squareup/protos/client/bills/CardData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public a10_card:Lcom/squareup/protos/client/bills/CardData$A10Card;

.field public encrypted_keyed_card:Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

.field public keyed_card:Lcom/squareup/protos/client/bills/CardData$KeyedCard;

.field public mcr_card:Lcom/squareup/protos/client/bills/CardData$MCRCard;

.field public o1_card:Lcom/squareup/protos/client/bills/CardData$O1Card;

.field public r12_card:Lcom/squareup/protos/client/bills/CardData$R12Card;

.field public r4_card:Lcom/squareup/protos/client/bills/CardData$R4Card;

.field public r6_card:Lcom/squareup/protos/client/bills/CardData$R6Card;

.field public reader_type:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field public s1_card:Lcom/squareup/protos/client/bills/CardData$S1Card;

.field public s3_card:Lcom/squareup/protos/client/bills/CardData$S3Card;

.field public server_completed:Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

.field public t2_card:Lcom/squareup/protos/client/bills/CardData$T2Card;

.field public unencrypted_card:Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

.field public x2_card:Lcom/squareup/protos/client/bills/CardData$X2Card;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 280
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public a10_card(Lcom/squareup/protos/client/bills/CardData$A10Card;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 0

    .line 333
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$Builder;->a10_card:Lcom/squareup/protos/client/bills/CardData$A10Card;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/CardData;
    .locals 20

    move-object/from16 v0, p0

    .line 369
    new-instance v18, Lcom/squareup/protos/client/bills/CardData;

    move-object/from16 v1, v18

    iget-object v2, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    iget-object v3, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->keyed_card:Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    iget-object v4, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->unencrypted_card:Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    iget-object v5, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->o1_card:Lcom/squareup/protos/client/bills/CardData$O1Card;

    iget-object v6, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->s1_card:Lcom/squareup/protos/client/bills/CardData$S1Card;

    iget-object v7, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->r4_card:Lcom/squareup/protos/client/bills/CardData$R4Card;

    iget-object v8, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->r6_card:Lcom/squareup/protos/client/bills/CardData$R6Card;

    iget-object v9, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->encrypted_keyed_card:Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    iget-object v10, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->a10_card:Lcom/squareup/protos/client/bills/CardData$A10Card;

    iget-object v11, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->r12_card:Lcom/squareup/protos/client/bills/CardData$R12Card;

    iget-object v12, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->x2_card:Lcom/squareup/protos/client/bills/CardData$X2Card;

    iget-object v13, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->mcr_card:Lcom/squareup/protos/client/bills/CardData$MCRCard;

    iget-object v14, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->t2_card:Lcom/squareup/protos/client/bills/CardData$T2Card;

    iget-object v15, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->server_completed:Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    move-object/from16 v19, v1

    iget-object v1, v0, Lcom/squareup/protos/client/bills/CardData$Builder;->s3_card:Lcom/squareup/protos/client/bills/CardData$S3Card;

    move-object/from16 v16, v1

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v17

    move-object/from16 v1, v19

    invoke-direct/range {v1 .. v17}, Lcom/squareup/protos/client/bills/CardData;-><init>(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/protos/client/bills/CardData$KeyedCard;Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;Lcom/squareup/protos/client/bills/CardData$O1Card;Lcom/squareup/protos/client/bills/CardData$S1Card;Lcom/squareup/protos/client/bills/CardData$R4Card;Lcom/squareup/protos/client/bills/CardData$R6Card;Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;Lcom/squareup/protos/client/bills/CardData$A10Card;Lcom/squareup/protos/client/bills/CardData$R12Card;Lcom/squareup/protos/client/bills/CardData$X2Card;Lcom/squareup/protos/client/bills/CardData$MCRCard;Lcom/squareup/protos/client/bills/CardData$T2Card;Lcom/squareup/protos/client/bills/CardData$ServerCompleted;Lcom/squareup/protos/client/bills/CardData$S3Card;Lokio/ByteString;)V

    return-object v18
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 249
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object v0

    return-object v0
.end method

.method public encrypted_keyed_card(Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 0

    .line 325
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$Builder;->encrypted_keyed_card:Lcom/squareup/protos/client/bills/CardData$EncryptedKeyedCard;

    return-object p0
.end method

.method public keyed_card(Lcom/squareup/protos/client/bills/CardData$KeyedCard;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 0

    .line 295
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$Builder;->keyed_card:Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    return-object p0
.end method

.method public mcr_card(Lcom/squareup/protos/client/bills/CardData$MCRCard;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 0

    .line 348
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$Builder;->mcr_card:Lcom/squareup/protos/client/bills/CardData$MCRCard;

    return-object p0
.end method

.method public o1_card(Lcom/squareup/protos/client/bills/CardData$O1Card;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 0

    .line 305
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$Builder;->o1_card:Lcom/squareup/protos/client/bills/CardData$O1Card;

    return-object p0
.end method

.method public r12_card(Lcom/squareup/protos/client/bills/CardData$R12Card;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 0

    .line 338
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$Builder;->r12_card:Lcom/squareup/protos/client/bills/CardData$R12Card;

    return-object p0
.end method

.method public r4_card(Lcom/squareup/protos/client/bills/CardData$R4Card;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 0

    .line 315
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$Builder;->r4_card:Lcom/squareup/protos/client/bills/CardData$R4Card;

    return-object p0
.end method

.method public r6_card(Lcom/squareup/protos/client/bills/CardData$R6Card;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 0

    .line 320
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$Builder;->r6_card:Lcom/squareup/protos/client/bills/CardData$R6Card;

    return-object p0
.end method

.method public reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 0

    .line 287
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0
.end method

.method public s1_card(Lcom/squareup/protos/client/bills/CardData$S1Card;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$Builder;->s1_card:Lcom/squareup/protos/client/bills/CardData$S1Card;

    return-object p0
.end method

.method public s3_card(Lcom/squareup/protos/client/bills/CardData$S3Card;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 0

    .line 363
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$Builder;->s3_card:Lcom/squareup/protos/client/bills/CardData$S3Card;

    return-object p0
.end method

.method public server_completed(Lcom/squareup/protos/client/bills/CardData$ServerCompleted;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 0

    .line 358
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$Builder;->server_completed:Lcom/squareup/protos/client/bills/CardData$ServerCompleted;

    return-object p0
.end method

.method public t2_card(Lcom/squareup/protos/client/bills/CardData$T2Card;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 0

    .line 353
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$Builder;->t2_card:Lcom/squareup/protos/client/bills/CardData$T2Card;

    return-object p0
.end method

.method public unencrypted_card(Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 0

    .line 300
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$Builder;->unencrypted_card:Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    return-object p0
.end method

.method public x2_card(Lcom/squareup/protos/client/bills/CardData$X2Card;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 0

    .line 343
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$Builder;->x2_card:Lcom/squareup/protos/client/bills/CardData$X2Card;

    return-object p0
.end method
