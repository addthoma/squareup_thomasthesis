.class public final Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetBillFamiliesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill:Lcom/squareup/protos/client/bills/Bill;

.field public related_bill:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Bill;",
            ">;"
        }
    .end annotation
.end field

.field public related_bill_tokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 300
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 301
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->related_bill:Ljava/util/List;

    .line 302
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->related_bill_tokens:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bill(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;
    .locals 0

    .line 306
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->bill:Lcom/squareup/protos/client/bills/Bill;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;
    .locals 5

    .line 327
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->bill:Lcom/squareup/protos/client/bills/Bill;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->related_bill:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->related_bill_tokens:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;-><init>(Lcom/squareup/protos/client/bills/Bill;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 293
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;

    move-result-object v0

    return-object v0
.end method

.method public related_bill(Ljava/util/List;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Bill;",
            ">;)",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;"
        }
    .end annotation

    .line 314
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 315
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->related_bill:Ljava/util/List;

    return-object p0
.end method

.method public related_bill_tokens(Ljava/util/List;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;"
        }
    .end annotation

    .line 320
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 321
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->related_bill_tokens:Ljava/util/List;

    return-object p0
.end method
