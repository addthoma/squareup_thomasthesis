.class public final Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData$T2Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CardData$T2Card;",
        "Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public encrypted_reader_data:Lokio/ByteString;

.field public encrypted_swipe_data:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2286
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/CardData$T2Card;
    .locals 4

    .line 2307
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$T2Card;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;->encrypted_reader_data:Lokio/ByteString;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;->encrypted_swipe_data:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/CardData$T2Card;-><init>(Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2281
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$T2Card;

    move-result-object v0

    return-object v0
.end method

.method public encrypted_reader_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;
    .locals 0

    .line 2293
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;->encrypted_reader_data:Lokio/ByteString;

    return-object p0
.end method

.method public encrypted_swipe_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;
    .locals 0

    .line 2301
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;->encrypted_swipe_data:Lokio/ByteString;

    return-object p0
.end method
