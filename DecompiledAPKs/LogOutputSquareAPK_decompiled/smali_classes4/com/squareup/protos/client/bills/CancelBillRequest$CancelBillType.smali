.class public final enum Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;
.super Ljava/lang/Enum;
.source "CancelBillRequest.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CancelBillRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CancelBillType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType$ProtoAdapter_CancelBillType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CANCEL_BILL_CLIENT_INITIATED_APP_TERMINATION:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

.field public static final enum CANCEL_BILL_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

.field public static final enum CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

.field public static final enum CANCEL_BILL_OTHER:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

.field public static final enum CANCEL_BILL_READER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

.field public static final enum DEFAULT_CANCEL_BILL_DO_NOT_USE:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 313
    new-instance v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "DEFAULT_CANCEL_BILL_DO_NOT_USE"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->DEFAULT_CANCEL_BILL_DO_NOT_USE:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    .line 319
    new-instance v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    const/4 v3, 0x2

    const-string v4, "CANCEL_BILL_HUMAN_INITIATED"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    .line 324
    new-instance v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    const/4 v4, 0x3

    const-string v5, "CANCEL_BILL_OTHER"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_OTHER:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    .line 330
    new-instance v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    const/4 v5, 0x4

    const-string v6, "CANCEL_BILL_CLIENT_INITIATED_TIMEOUT"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    .line 335
    new-instance v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    const/4 v6, 0x5

    const-string v7, "CANCEL_BILL_CLIENT_INITIATED_APP_TERMINATION"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_CLIENT_INITIATED_APP_TERMINATION:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    .line 341
    new-instance v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    const/4 v7, 0x6

    const-string v8, "CANCEL_BILL_READER_INITIATED"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_READER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    new-array v0, v7, [Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    .line 312
    sget-object v7, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->DEFAULT_CANCEL_BILL_DO_NOT_USE:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_OTHER:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_CLIENT_INITIATED_APP_TERMINATION:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_READER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->$VALUES:[Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    .line 343
    new-instance v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType$ProtoAdapter_CancelBillType;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType$ProtoAdapter_CancelBillType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 347
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 348
    iput p3, p0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 361
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_READER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    return-object p0

    .line 360
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_CLIENT_INITIATED_APP_TERMINATION:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    return-object p0

    .line 359
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    return-object p0

    .line 358
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_OTHER:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    return-object p0

    .line 357
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    return-object p0

    .line 356
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->DEFAULT_CANCEL_BILL_DO_NOT_USE:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;
    .locals 1

    .line 312
    const-class v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;
    .locals 1

    .line 312
    sget-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->$VALUES:[Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 368
    iget v0, p0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->value:I

    return v0
.end method
