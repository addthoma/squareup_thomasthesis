.class public final Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$BackingDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$BackingDetails;",
        "Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

.field public category:Lcom/squareup/api/items/MenuCategory;

.field public item:Lcom/squareup/api/items/Item;

.field public item_image:Lcom/squareup/api/items/ItemImage;

.field public menu:Lcom/squareup/api/items/Menu;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1975
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public available_options(Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 2010
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails;
    .locals 8

    .line 2025
    new-instance v7, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->item:Lcom/squareup/api/items/Item;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->item_image:Lcom/squareup/api/items/ItemImage;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->category:Lcom/squareup/api/items/MenuCategory;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->available_options:Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->menu:Lcom/squareup/api/items/Menu;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;-><init>(Lcom/squareup/api/items/Item;Lcom/squareup/api/items/ItemImage;Lcom/squareup/api/items/MenuCategory;Lcom/squareup/protos/client/bills/Itemization$BackingDetails$AvailableOptions;Lcom/squareup/api/items/Menu;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1964
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    move-result-object v0

    return-object v0
.end method

.method public category(Lcom/squareup/api/items/MenuCategory;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;
    .locals 0

    .line 1998
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->category:Lcom/squareup/api/items/MenuCategory;

    return-object p0
.end method

.method public item(Lcom/squareup/api/items/Item;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;
    .locals 0

    .line 1982
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->item:Lcom/squareup/api/items/Item;

    return-object p0
.end method

.method public item_image(Lcom/squareup/api/items/ItemImage;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;
    .locals 0

    .line 1990
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->item_image:Lcom/squareup/api/items/ItemImage;

    return-object p0
.end method

.method public menu(Lcom/squareup/api/items/Menu;)Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;
    .locals 0

    .line 2019
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails$Builder;->menu:Lcom/squareup/api/items/Menu;

    return-object p0
.end method
