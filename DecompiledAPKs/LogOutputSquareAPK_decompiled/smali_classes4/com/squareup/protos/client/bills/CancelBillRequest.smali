.class public final Lcom/squareup/protos/client/bills/CancelBillRequest;
.super Lcom/squareup/wire/Message;
.source "CancelBillRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CancelBillRequest$ProtoAdapter_CancelBillRequest;,
        Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;,
        Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;,
        Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/CancelBillRequest;",
        "Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CancelBillRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CANCEL_BILL_TYPE:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

.field public static final DEFAULT_IS_AMENDING:Ljava/lang/Boolean;

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_REASON_DEPRECATED:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

.field private static final serialVersionUID:J


# instance fields
.field public final bill_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final cancel_bill_type:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CancelBillRequest$CancelBillType#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final canceled_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x3
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final is_amending:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final reason_deprecated:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CancelBillRequest$Reason#ADAPTER"
        tag = 0x4
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/client/bills/CancelBillRequest$ProtoAdapter_CancelBillRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CancelBillRequest$ProtoAdapter_CancelBillRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->UNKNOWN:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest;->DEFAULT_REASON_DEPRECATED:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    .line 34
    sget-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->DEFAULT_CANCEL_BILL_DO_NOT_USE:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest;->DEFAULT_CANCEL_BILL_TYPE:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    const/4 v0, 0x0

    .line 36
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest;->DEFAULT_IS_AMENDING:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;Ljava/lang/String;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/Boolean;)V
    .locals 8

    .line 96
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bills/CancelBillRequest;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;Ljava/lang/String;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;Ljava/lang/String;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 102
    sget-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 104
    iput-object p2, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 105
    iput-object p3, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->reason_deprecated:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    .line 106
    iput-object p4, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->merchant_token:Ljava/lang/String;

    .line 107
    iput-object p5, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->cancel_bill_type:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    .line 108
    iput-object p6, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->is_amending:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 127
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/CancelBillRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 128
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/CancelBillRequest;

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CancelBillRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CancelBillRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CancelBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 130
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CancelBillRequest;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 131
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->reason_deprecated:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CancelBillRequest;->reason_deprecated:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    .line 132
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->merchant_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CancelBillRequest;->merchant_token:Ljava/lang/String;

    .line 133
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->cancel_bill_type:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/CancelBillRequest;->cancel_bill_type:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    .line 134
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->is_amending:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CancelBillRequest;->is_amending:Ljava/lang/Boolean;

    .line 135
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 140
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 142
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CancelBillRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->reason_deprecated:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->cancel_bill_type:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->is_amending:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 149
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;
    .locals 2

    .line 113
    new-instance v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;-><init>()V

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->reason_deprecated:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->reason_deprecated:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->merchant_token:Ljava/lang/String;

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->cancel_bill_type:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->cancel_bill_type:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->is_amending:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->is_amending:Ljava/lang/Boolean;

    .line 120
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CancelBillRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CancelBillRequest;->newBuilder()Lcom/squareup/protos/client/bills/CancelBillRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", bill_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 158
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    const-string v1, ", canceled_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->canceled_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 159
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->reason_deprecated:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    if-eqz v1, :cond_2

    const-string v1, ", reason_deprecated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->reason_deprecated:Lcom/squareup/protos/client/bills/CancelBillRequest$Reason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 160
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->cancel_bill_type:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    if-eqz v1, :cond_4

    const-string v1, ", cancel_bill_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->cancel_bill_type:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 162
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->is_amending:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", is_amending="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CancelBillRequest;->is_amending:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CancelBillRequest{"

    .line 163
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
