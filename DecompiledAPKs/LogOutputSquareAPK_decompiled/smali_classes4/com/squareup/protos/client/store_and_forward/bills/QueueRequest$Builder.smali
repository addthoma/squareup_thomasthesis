.class public final Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "QueueRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;",
        "Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;",
            ">;"
        }
    .end annotation
.end field

.field public payment:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/store_and_forward/payments/Payment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 101
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 102
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;->payment:Ljava/util/List;

    .line 103
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;->bill:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bill(Ljava/util/List;)Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;",
            ">;)",
            "Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;"
        }
    .end annotation

    .line 119
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;->bill:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;
    .locals 4

    .line 126
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;->payment:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;->bill:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;-><init>(Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;->build()Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;

    move-result-object v0

    return-object v0
.end method

.method public payment(Ljava/util/List;)Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/store_and_forward/payments/Payment;",
            ">;)",
            "Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;"
        }
    .end annotation

    .line 110
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;->payment:Ljava/util/List;

    return-object p0
.end method
