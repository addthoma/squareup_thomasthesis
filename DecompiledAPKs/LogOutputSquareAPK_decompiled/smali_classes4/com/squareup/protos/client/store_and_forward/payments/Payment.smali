.class public final Lcom/squareup/protos/client/store_and_forward/payments/Payment;
.super Lcom/squareup/wire/Message;
.source "Payment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/store_and_forward/payments/Payment$ProtoAdapter_Payment;,
        Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/store_and_forward/payments/Payment;",
        "Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/store_and_forward/payments/Payment;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BLETCHLEY_KEY_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CARD_BRAND:Ljava/lang/String; = ""

.field public static final DEFAULT_CARD_LAST_FOUR:Ljava/lang/String; = ""

.field public static final DEFAULT_CLIENT_TIMESTAMP:Ljava/lang/String; = ""

.field public static final DEFAULT_ENCRYPTED_PAYLOAD:Lokio/ByteString;

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_UNIQUE_KEY:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.crypto.merchantsecret.MerchantAuthCode#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final bletchley_key_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final card_brand:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x6
    .end annotation
.end field

.field public final card_last_four:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x7
    .end annotation
.end field

.field public final client_timestamp:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final encrypted_payload:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final total_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final unique_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment$ProtoAdapter_Payment;

    invoke-direct {v0}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$ProtoAdapter_Payment;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 39
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->DEFAULT_ENCRYPTED_PAYLOAD:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;)V
    .locals 11

    .line 150
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/store_and_forward/payments/Payment;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;Lokio/ByteString;)V
    .locals 1

    .line 156
    sget-object v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 157
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->unique_key:Ljava/lang/String;

    .line 158
    iput-object p2, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->bletchley_key_id:Ljava/lang/String;

    .line 159
    iput-object p3, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->encrypted_payload:Lokio/ByteString;

    .line 160
    iput-object p4, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->client_timestamp:Ljava/lang/String;

    .line 161
    iput-object p5, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->total_money:Lcom/squareup/protos/common/Money;

    .line 162
    iput-object p6, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->card_brand:Ljava/lang/String;

    .line 163
    iput-object p7, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->card_last_four:Ljava/lang/String;

    .line 164
    iput-object p8, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->merchant_token:Ljava/lang/String;

    .line 165
    iput-object p9, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 187
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 188
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    .line 189
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->unique_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->unique_key:Ljava/lang/String;

    .line 190
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->bletchley_key_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->bletchley_key_id:Ljava/lang/String;

    .line 191
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->encrypted_payload:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->encrypted_payload:Lokio/ByteString;

    .line 192
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->client_timestamp:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->client_timestamp:Ljava/lang/String;

    .line 193
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->total_money:Lcom/squareup/protos/common/Money;

    .line 194
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->card_brand:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->card_brand:Ljava/lang/String;

    .line 195
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->card_last_four:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->card_last_four:Ljava/lang/String;

    .line 196
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->merchant_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->merchant_token:Ljava/lang/String;

    .line 197
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    iget-object p1, p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    .line 198
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 203
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 205
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->unique_key:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->bletchley_key_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->encrypted_payload:Lokio/ByteString;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->client_timestamp:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->total_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->card_brand:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->card_last_four:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 213
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 214
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 215
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;
    .locals 2

    .line 170
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;-><init>()V

    .line 171
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->unique_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->unique_key:Ljava/lang/String;

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->bletchley_key_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->bletchley_key_id:Ljava/lang/String;

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->encrypted_payload:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->encrypted_payload:Lokio/ByteString;

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->client_timestamp:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->client_timestamp:Ljava/lang/String;

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->total_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->total_money:Lcom/squareup/protos/common/Money;

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->card_brand:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->card_brand:Ljava/lang/String;

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->card_last_four:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->card_last_four:Ljava/lang/String;

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->merchant_token:Ljava/lang/String;

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    iput-object v1, v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    .line 180
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->newBuilder()Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 222
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 223
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->unique_key:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", unique_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->unique_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->bletchley_key_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", bletchley_key_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->bletchley_key_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->encrypted_payload:Lokio/ByteString;

    if-eqz v1, :cond_2

    const-string v1, ", encrypted_payload=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->client_timestamp:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", client_timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->client_timestamp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->total_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", total_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->total_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 228
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->card_brand:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", card_brand=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->card_last_four:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", card_last_four=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    if-eqz v1, :cond_8

    const-string v1, ", auth_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Payment{"

    .line 232
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
