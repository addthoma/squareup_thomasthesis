.class public final Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;
.super Lcom/squareup/wire/Message;
.source "BillEncryptionPayload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ExtraTenderDetail"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$ProtoAdapter_ExtraTenderDetail;,
        Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;",
        "Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SIGN_ON_PAPER:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final receipt_option:Lcom/squareup/protos/client/ReceiptOption;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ReceiptOption#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final sign_on_paper:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final signature:Lcom/squareup/protos/client/Signature;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Signature#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final tender_id:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 288
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$ProtoAdapter_ExtraTenderDetail;

    invoke-direct {v0}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$ProtoAdapter_ExtraTenderDetail;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 292
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->DEFAULT_SIGN_ON_PAPER:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/ReceiptOption;Lcom/squareup/protos/client/Signature;Ljava/lang/Boolean;)V
    .locals 6

    .line 323
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/ReceiptOption;Lcom/squareup/protos/client/Signature;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/ReceiptOption;Lcom/squareup/protos/client/Signature;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 328
    sget-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 329
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->tender_id:Lcom/squareup/protos/client/IdPair;

    .line 330
    iput-object p2, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->receipt_option:Lcom/squareup/protos/client/ReceiptOption;

    .line 331
    iput-object p3, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->signature:Lcom/squareup/protos/client/Signature;

    .line 332
    iput-object p4, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->sign_on_paper:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 349
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 350
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;

    .line 351
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->tender_id:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->tender_id:Lcom/squareup/protos/client/IdPair;

    .line 352
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->receipt_option:Lcom/squareup/protos/client/ReceiptOption;

    iget-object v3, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->receipt_option:Lcom/squareup/protos/client/ReceiptOption;

    .line 353
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->signature:Lcom/squareup/protos/client/Signature;

    iget-object v3, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->signature:Lcom/squareup/protos/client/Signature;

    .line 354
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->sign_on_paper:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->sign_on_paper:Ljava/lang/Boolean;

    .line 355
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 360
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 362
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 363
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->tender_id:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 364
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->receipt_option:Lcom/squareup/protos/client/ReceiptOption;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/ReceiptOption;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 365
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->signature:Lcom/squareup/protos/client/Signature;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/Signature;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 366
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->sign_on_paper:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 367
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;
    .locals 2

    .line 337
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;-><init>()V

    .line 338
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->tender_id:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;->tender_id:Lcom/squareup/protos/client/IdPair;

    .line 339
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->receipt_option:Lcom/squareup/protos/client/ReceiptOption;

    iput-object v1, v0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;->receipt_option:Lcom/squareup/protos/client/ReceiptOption;

    .line 340
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->signature:Lcom/squareup/protos/client/Signature;

    iput-object v1, v0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;->signature:Lcom/squareup/protos/client/Signature;

    .line 341
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->sign_on_paper:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;->sign_on_paper:Ljava/lang/Boolean;

    .line 342
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 287
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->newBuilder()Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 374
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 375
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->tender_id:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", tender_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->tender_id:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 376
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->receipt_option:Lcom/squareup/protos/client/ReceiptOption;

    if-eqz v1, :cond_1

    const-string v1, ", receipt_option="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->receipt_option:Lcom/squareup/protos/client/ReceiptOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 377
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->signature:Lcom/squareup/protos/client/Signature;

    if-eqz v1, :cond_2

    const-string v1, ", signature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->signature:Lcom/squareup/protos/client/Signature;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 378
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->sign_on_paper:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", sign_on_paper="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillEncryptionPayload$ExtraTenderDetail;->sign_on_paper:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ExtraTenderDetail{"

    .line 379
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
