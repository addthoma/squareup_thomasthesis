.class final Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$ProtoAdapter_DayOfWeekDepositSettings;
.super Lcom/squareup/wire/ProtoAdapter;
.source "DayOfWeekDepositSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_DayOfWeekDepositSettings"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 134
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 153
    new-instance v0, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;-><init>()V

    .line 154
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 155
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 167
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 165
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/depositsettings/DepositInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/depositsettings/DepositInterval;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->deposit_interval(Lcom/squareup/protos/client/depositsettings/DepositInterval;)Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;

    goto :goto_0

    .line 159
    :cond_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/common/time/DayOfWeek;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/common/time/DayOfWeek;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->day_of_week(Lcom/squareup/protos/common/time/DayOfWeek;)Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 161
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 171
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 172
    invoke-virtual {v0}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->build()Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 132
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$ProtoAdapter_DayOfWeekDepositSettings;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 146
    sget-object v0, Lcom/squareup/protos/common/time/DayOfWeek;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->day_of_week:Lcom/squareup/protos/common/time/DayOfWeek;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 147
    sget-object v0, Lcom/squareup/protos/client/depositsettings/DepositInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 148
    invoke-virtual {p2}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 132
    check-cast p2, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$ProtoAdapter_DayOfWeekDepositSettings;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;)I
    .locals 4

    .line 139
    sget-object v0, Lcom/squareup/protos/common/time/DayOfWeek;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->day_of_week:Lcom/squareup/protos/common/time/DayOfWeek;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/depositsettings/DepositInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    const/4 v3, 0x2

    .line 140
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 132
    check-cast p1, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$ProtoAdapter_DayOfWeekDepositSettings;->encodedSize(Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;)Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;
    .locals 2

    .line 177
    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;->newBuilder()Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;

    move-result-object p1

    .line 178
    iget-object v0, p1, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/depositsettings/DepositInterval;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/depositsettings/DepositInterval;

    iput-object v0, p1, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->deposit_interval:Lcom/squareup/protos/client/depositsettings/DepositInterval;

    .line 179
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 180
    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$Builder;->build()Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 132
    check-cast p1, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings$ProtoAdapter_DayOfWeekDepositSettings;->redact(Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;)Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;

    move-result-object p1

    return-object p1
.end method
