.class public final Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetDepositScheduleResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;",
        "Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

.field public default_cutoff_time:Lcom/squareup/protos/common/time/LocalTime;

.field public next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

.field public potential_deposit_schedule:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;",
            ">;"
        }
    .end annotation
.end field

.field public settlement_frequency:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 155
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 156
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->potential_deposit_schedule:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;
    .locals 8

    .line 205
    new-instance v7, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    iget-object v2, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    iget-object v3, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->potential_deposit_schedule:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->default_cutoff_time:Lcom/squareup/protos/common/time/LocalTime;

    iget-object v5, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->settlement_frequency:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;-><init>(Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;Ljava/util/List;Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/protos/client/depositsettings/SettlementFrequency;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 144
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->build()Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse;

    move-result-object v0

    return-object v0
.end method

.method public current_weekly_deposit_schedule(Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;)Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->current_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    return-object p0
.end method

.method public default_cutoff_time(Lcom/squareup/protos/common/time/LocalTime;)Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->default_cutoff_time:Lcom/squareup/protos/common/time/LocalTime;

    return-object p0
.end method

.method public next_weekly_deposit_schedule(Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;)Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->next_weekly_deposit_schedule:Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    return-object p0
.end method

.method public potential_deposit_schedule(Ljava/util/List;)Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;",
            ">;)",
            "Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;"
        }
    .end annotation

    .line 182
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 183
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->potential_deposit_schedule:Ljava/util/List;

    return-object p0
.end method

.method public settlement_frequency(Lcom/squareup/protos/client/depositsettings/SettlementFrequency;)Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/GetDepositScheduleResponse$Builder;->settlement_frequency:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    return-object p0
.end method
