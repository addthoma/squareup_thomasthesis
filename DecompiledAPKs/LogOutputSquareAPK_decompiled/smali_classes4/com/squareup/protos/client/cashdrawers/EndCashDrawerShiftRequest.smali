.class public final Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;
.super Lcom/squareup/wire/Message;
.source "EndCashDrawerShiftRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$ProtoAdapter_EndCashDrawerShiftRequest;,
        Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;",
        "Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CLIENT_CASH_DRAWER_SHIFT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CLIENT_UNIQUE_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_ENDING_EMPLOYEE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_ENTERED_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_MERCHANT_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final cash_paid_in_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final cash_paid_out_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final cash_payment_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final cash_refunds_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final client_cash_drawer_shift_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final client_unique_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cashdrawers.DeviceInfo#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final employee_id:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final ended_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final ending_employee_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final entered_description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x5
    .end annotation
.end field

.field public final events:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cashdrawers.CashDrawerShiftEvent#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xd
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;"
        }
    .end annotation
.end field

.field public final expected_cash_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final merchant_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$ProtoAdapter_EndCashDrawerShiftRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$ProtoAdapter_EndCashDrawerShiftRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Lcom/squareup/protos/client/cashdrawers/DeviceInfo;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;",
            "Lcom/squareup/protos/client/cashdrawers/DeviceInfo;",
            ")V"
        }
    .end annotation

    .line 165
    sget-object v15, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Lcom/squareup/protos/client/cashdrawers/DeviceInfo;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Lcom/squareup/protos/client/cashdrawers/DeviceInfo;Lokio/ByteString;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;",
            "Lcom/squareup/protos/client/cashdrawers/DeviceInfo;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    .line 174
    sget-object v1, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p15

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 175
    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    move-object v1, p2

    .line 176
    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    move-object v1, p3

    .line 177
    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    const-string v1, "employee_id"

    move-object v2, p4

    .line 178
    invoke-static {v1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->employee_id:Ljava/util/List;

    move-object v1, p5

    .line 179
    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->entered_description:Ljava/lang/String;

    move-object v1, p6

    .line 180
    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->ending_employee_id:Ljava/lang/String;

    move-object v1, p7

    .line 181
    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    move-object v1, p8

    .line 182
    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_payment_money:Lcom/squareup/protos/common/Money;

    move-object v1, p9

    .line 183
    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    move-object v1, p10

    .line 184
    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    move-object v1, p11

    .line 185
    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    move-object v1, p12

    .line 186
    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->expected_cash_money:Lcom/squareup/protos/common/Money;

    const-string v1, "events"

    move-object/from16 v2, p13

    .line 187
    invoke-static {v1, v2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->events:Ljava/util/List;

    move-object/from16 v1, p14

    .line 188
    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 215
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 216
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;

    .line 217
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    .line 218
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    .line 219
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    .line 220
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->employee_id:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->employee_id:Ljava/util/List;

    .line 221
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->entered_description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->entered_description:Ljava/lang/String;

    .line 222
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->ending_employee_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->ending_employee_id:Ljava/lang/String;

    .line 223
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 224
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_payment_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_payment_money:Lcom/squareup/protos/common/Money;

    .line 225
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    .line 226
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    .line 227
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    .line 228
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->expected_cash_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->expected_cash_money:Lcom/squareup/protos/common/Money;

    .line 229
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->events:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->events:Ljava/util/List;

    .line 230
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 231
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 236
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_c

    .line 238
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 239
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 240
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 241
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 242
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->employee_id:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 243
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->entered_description:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->ending_employee_id:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 245
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 246
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_payment_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 247
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 248
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 249
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 250
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->expected_cash_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 251
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->events:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 252
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->hashCode()I

    move-result v2

    :cond_b
    add-int/2addr v0, v2

    .line 253
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_c
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;
    .locals 2

    .line 193
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;-><init>()V

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->client_unique_key:Ljava/lang/String;

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->merchant_id:Ljava/lang/String;

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->client_cash_drawer_shift_id:Ljava/lang/String;

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->employee_id:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->employee_id:Ljava/util/List;

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->entered_description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->entered_description:Ljava/lang/String;

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->ending_employee_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->ending_employee_id:Ljava/lang/String;

    .line 200
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_payment_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->cash_payment_money:Lcom/squareup/protos/common/Money;

    .line 202
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    .line 204
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->expected_cash_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->events:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->events:Ljava/util/List;

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iput-object v1, v0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 208
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->newBuilder()Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 261
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", client_unique_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->client_unique_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", merchant_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->merchant_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", client_cash_drawer_shift_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->employee_id:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", employee_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->employee_id:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 265
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->entered_description:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", entered_description=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->ending_employee_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", ending_employee_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->ending_employee_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_6

    const-string v1, ", ended_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 268
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_payment_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_7

    const-string v1, ", cash_payment_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_payment_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 269
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_8

    const-string v1, ", cash_refunds_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 270
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_9

    const-string v1, ", cash_paid_in_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 271
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_a

    const-string v1, ", cash_paid_out_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 272
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->expected_cash_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_b

    const-string v1, ", expected_cash_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->expected_cash_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 273
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->events:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, ", events="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->events:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 274
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v1, :cond_d

    const-string v1, ", device_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EndCashDrawerShiftRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_d
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "EndCashDrawerShiftRequest{"

    .line 275
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
