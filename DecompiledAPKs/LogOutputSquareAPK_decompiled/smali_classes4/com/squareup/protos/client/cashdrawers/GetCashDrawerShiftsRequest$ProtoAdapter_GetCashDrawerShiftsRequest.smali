.class final Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$ProtoAdapter_GetCashDrawerShiftsRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetCashDrawerShiftsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetCashDrawerShiftsRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 320
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 353
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;-><init>()V

    .line 354
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 355
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 374
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 372
    :pswitch_0
    iget-object v3, v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->selected_client_cash_drawer_shift_id:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 371
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->ignore_device_header_for_filter(Ljava/lang/Boolean;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    goto :goto_0

    .line 370
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->device_info(Lcom/squareup/protos/client/cashdrawers/DeviceInfo;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    goto :goto_0

    .line 364
    :pswitch_3
    :try_start_0
    iget-object v4, v0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->include_state:Ljava/util/List;

    sget-object v5, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v5, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 366
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 361
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->business_day_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    goto :goto_0

    .line 360
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->client_virtual_register_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    goto :goto_0

    .line 359
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->end_date(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    goto :goto_0

    .line 358
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->start_date(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    goto :goto_0

    .line 357
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->merchant_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    goto/16 :goto_0

    .line 378
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 379
    invoke-virtual {v0}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 318
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$ProtoAdapter_GetCashDrawerShiftsRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 339
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->merchant_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 340
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->start_date:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 341
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->end_date:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 342
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->client_virtual_register_id:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 343
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->business_day_id:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 344
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->include_state:Ljava/util/List;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 345
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 346
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->ignore_device_header_for_filter:Ljava/lang/Boolean;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 347
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->selected_client_cash_drawer_shift_id:Ljava/util/List;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 348
    invoke-virtual {p2}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 318
    check-cast p2, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$ProtoAdapter_GetCashDrawerShiftsRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;)I
    .locals 4

    .line 325
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->merchant_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->start_date:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x4

    .line 326
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->end_date:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x5

    .line 327
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->client_virtual_register_id:Ljava/lang/String;

    const/4 v3, 0x6

    .line 328
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->business_day_id:Ljava/lang/String;

    const/4 v3, 0x7

    .line 329
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 330
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->include_state:Ljava/util/List;

    const/16 v3, 0x8

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    const/16 v3, 0x9

    .line 331
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->ignore_device_header_for_filter:Ljava/lang/Boolean;

    const/16 v3, 0xa

    .line 332
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 333
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->selected_client_cash_drawer_shift_id:Ljava/util/List;

    const/16 v3, 0xb

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 334
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 318
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$ProtoAdapter_GetCashDrawerShiftsRequest;->encodedSize(Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;
    .locals 2

    .line 384
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;->newBuilder()Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    move-result-object p1

    .line 385
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->start_date:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->start_date:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->start_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 386
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->end_date:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->end_date:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->end_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 387
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 388
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 389
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 318
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$ProtoAdapter_GetCashDrawerShiftsRequest;->redact(Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;

    move-result-object p1

    return-object p1
.end method
