.class public final Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EmailCashDrawerShiftReportResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse;",
        "Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error_message:Ljava/lang/String;

.field public error_title:Ljava/lang/String;

.field public status:Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse;
    .locals 5

    .line 145
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Builder;->status:Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Status;

    iget-object v2, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Builder;->error_title:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Builder;->error_message:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse;-><init>(Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Status;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 112
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Builder;->build()Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse;

    move-result-object v0

    return-object v0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method

.method public error_title(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Builder;
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Builder;->error_title:Ljava/lang/String;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Status;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Builder;->status:Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportResponse$Status;

    return-object p0
.end method
