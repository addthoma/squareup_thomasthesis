.class public final Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetCashDrawerShiftsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;",
        "Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public business_day_id:Ljava/lang/String;

.field public client_virtual_register_id:Ljava/lang/String;

.field public device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

.field public end_date:Lcom/squareup/protos/client/ISO8601Date;

.field public ignore_device_header_for_filter:Ljava/lang/Boolean;

.field public include_state:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;",
            ">;"
        }
    .end annotation
.end field

.field public merchant_id:Ljava/lang/String;

.field public selected_client_cash_drawer_shift_id:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public start_date:Lcom/squareup/protos/client/ISO8601Date;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 232
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 233
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->include_state:Ljava/util/List;

    .line 234
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->selected_client_cash_drawer_shift_id:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;
    .locals 12

    .line 314
    new-instance v11, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->merchant_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->start_date:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->end_date:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v4, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->client_virtual_register_id:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->business_day_id:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->include_state:Ljava/util/List;

    iget-object v7, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iget-object v8, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->ignore_device_header_for_filter:Ljava/lang/Boolean;

    iget-object v9, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->selected_client_cash_drawer_shift_id:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/cashdrawers/DeviceInfo;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 213
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;

    move-result-object v0

    return-object v0
.end method

.method public business_day_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;
    .locals 0

    .line 273
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->business_day_id:Ljava/lang/String;

    return-object p0
.end method

.method public client_virtual_register_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;
    .locals 0

    .line 265
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->client_virtual_register_id:Ljava/lang/String;

    return-object p0
.end method

.method public device_info(Lcom/squareup/protos/client/cashdrawers/DeviceInfo;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;
    .locals 0

    .line 290
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    return-object p0
.end method

.method public end_date(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;
    .locals 0

    .line 257
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->end_date:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public ignore_device_header_for_filter(Ljava/lang/Boolean;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;
    .locals 0

    .line 298
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->ignore_device_header_for_filter:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_state(Ljava/util/List;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;",
            ">;)",
            "Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;"
        }
    .end annotation

    .line 281
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 282
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->include_state:Ljava/util/List;

    return-object p0
.end method

.method public merchant_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;
    .locals 0

    .line 241
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->merchant_id:Ljava/lang/String;

    return-object p0
.end method

.method public selected_client_cash_drawer_shift_id(Ljava/util/List;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;"
        }
    .end annotation

    .line 307
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 308
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->selected_client_cash_drawer_shift_id:Ljava/util/List;

    return-object p0
.end method

.method public start_date(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;
    .locals 0

    .line 249
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->start_date:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method
