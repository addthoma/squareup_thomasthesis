.class public final Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EmailCashDrawerShiftReportRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;",
        "Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public client_cash_drawer_shift_id:Ljava/lang/String;

.field public client_unique_key:Ljava/lang/String;

.field public email_address:Ljava/lang/String;

.field public merchant_id:Ljava/lang/String;

.field public rendering_options:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 163
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;
    .locals 8

    .line 211
    new-instance v7, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->client_unique_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->merchant_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->client_cash_drawer_shift_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->email_address:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->rendering_options:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 152
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;

    move-result-object v0

    return-object v0
.end method

.method public client_cash_drawer_shift_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->client_cash_drawer_shift_id:Ljava/lang/String;

    return-object p0
.end method

.method public client_unique_key(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->client_unique_key:Ljava/lang/String;

    return-object p0
.end method

.method public email_address(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->email_address:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->merchant_id:Ljava/lang/String;

    return-object p0
.end method

.method public rendering_options(Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->rendering_options:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    return-object p0
.end method
