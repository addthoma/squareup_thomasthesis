.class public final enum Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;
.super Ljava/lang/Enum;
.source "EligibilityBlocker.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker$ProtoAdapter_EligibilityBlocker;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FUNDING_SOURCE_UNSUPPORTED:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

.field public static final enum FUNDING_SOURCE_VERIFICATION_EXPIRED:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

.field public static final enum INSTANT_DEPOSIT_UNAVAILABLE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

.field public static final enum INSUFFICIENT_FUNDS:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

.field public static final enum NOT_ONBOARDED_TO_INSTANT_DEPOSIT:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

.field public static final enum NO_FUNDING_SOURCE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

.field public static final enum NO_LINKED_BANK_ACCOUNT:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

.field public static final enum OVER_DAILY_DEPOSIT_LIMIT:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

.field public static final enum OVER_DEPOSIT_LIMIT:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

.field public static final enum RISK_FLAGGED:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

.field public static final enum UNVERIFIED_FUNDING_SOURCE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .line 17
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->UNKNOWN:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    .line 22
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    const/4 v2, 0x1

    const-string v3, "NOT_ONBOARDED_TO_INSTANT_DEPOSIT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->NOT_ONBOARDED_TO_INSTANT_DEPOSIT:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    .line 27
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    const/4 v3, 0x2

    const-string v4, "NO_FUNDING_SOURCE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->NO_FUNDING_SOURCE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    .line 32
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    const/4 v4, 0x3

    const-string v5, "INSUFFICIENT_FUNDS"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->INSUFFICIENT_FUNDS:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    .line 37
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    const/4 v5, 0x4

    const-string v6, "RISK_FLAGGED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->RISK_FLAGGED:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    .line 42
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    const/4 v6, 0x5

    const-string v7, "NO_LINKED_BANK_ACCOUNT"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->NO_LINKED_BANK_ACCOUNT:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    .line 47
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    const/4 v7, 0x6

    const-string v8, "OVER_DEPOSIT_LIMIT"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->OVER_DEPOSIT_LIMIT:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    .line 52
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    const/4 v8, 0x7

    const-string v9, "UNVERIFIED_FUNDING_SOURCE"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->UNVERIFIED_FUNDING_SOURCE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    .line 57
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    const/16 v9, 0x8

    const-string v10, "FUNDING_SOURCE_VERIFICATION_EXPIRED"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->FUNDING_SOURCE_VERIFICATION_EXPIRED:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    .line 64
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    const/16 v10, 0x9

    const-string v11, "FUNDING_SOURCE_UNSUPPORTED"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->FUNDING_SOURCE_UNSUPPORTED:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    .line 70
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    const/16 v11, 0xa

    const-string v12, "INSTANT_DEPOSIT_UNAVAILABLE"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->INSTANT_DEPOSIT_UNAVAILABLE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    .line 75
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    const/16 v12, 0xb

    const-string v13, "OVER_DAILY_DEPOSIT_LIMIT"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->OVER_DAILY_DEPOSIT_LIMIT:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    .line 13
    sget-object v13, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->UNKNOWN:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->NOT_ONBOARDED_TO_INSTANT_DEPOSIT:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->NO_FUNDING_SOURCE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->INSUFFICIENT_FUNDS:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->RISK_FLAGGED:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->NO_LINKED_BANK_ACCOUNT:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->OVER_DEPOSIT_LIMIT:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->UNVERIFIED_FUNDING_SOURCE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->FUNDING_SOURCE_VERIFICATION_EXPIRED:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->FUNDING_SOURCE_UNSUPPORTED:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->INSTANT_DEPOSIT_UNAVAILABLE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->OVER_DAILY_DEPOSIT_LIMIT:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    aput-object v1, v0, v12

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->$VALUES:[Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    .line 77
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker$ProtoAdapter_EligibilityBlocker;

    invoke-direct {v0}, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker$ProtoAdapter_EligibilityBlocker;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 81
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 82
    iput p3, p0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 101
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->OVER_DAILY_DEPOSIT_LIMIT:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    return-object p0

    .line 100
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->INSTANT_DEPOSIT_UNAVAILABLE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    return-object p0

    .line 99
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->FUNDING_SOURCE_UNSUPPORTED:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    return-object p0

    .line 98
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->FUNDING_SOURCE_VERIFICATION_EXPIRED:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    return-object p0

    .line 97
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->UNVERIFIED_FUNDING_SOURCE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    return-object p0

    .line 96
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->OVER_DEPOSIT_LIMIT:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    return-object p0

    .line 95
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->NO_LINKED_BANK_ACCOUNT:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    return-object p0

    .line 94
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->RISK_FLAGGED:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    return-object p0

    .line 93
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->INSUFFICIENT_FUNDS:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    return-object p0

    .line 92
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->NO_FUNDING_SOURCE:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    return-object p0

    .line 91
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->NOT_ONBOARDED_TO_INSTANT_DEPOSIT:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    return-object p0

    .line 90
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->UNKNOWN:Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->$VALUES:[Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 108
    iget v0, p0, Lcom/squareup/protos/client/instantdeposits/EligibilityBlocker;->value:I

    return v0
.end method
