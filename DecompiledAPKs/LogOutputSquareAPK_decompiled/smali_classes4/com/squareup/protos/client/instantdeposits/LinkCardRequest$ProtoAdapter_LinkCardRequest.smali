.class final Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$ProtoAdapter_LinkCardRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "LinkCardRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_LinkCardRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 340
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 365
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;-><init>()V

    .line 366
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 367
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 375
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 373
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->skip_verification_email(Ljava/lang/Boolean;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;

    goto :goto_0

    .line 372
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->authentication_detail(Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;

    goto :goto_0

    .line 371
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/bills/CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/CardData;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;

    goto :goto_0

    .line 370
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;

    goto :goto_0

    .line 369
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;

    goto :goto_0

    .line 379
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 380
    invoke-virtual {v0}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->build()Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 338
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$ProtoAdapter_LinkCardRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 355
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->request_uuid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 356
    sget-object v0, Lcom/squareup/protos/client/bills/CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->card_data:Lcom/squareup/protos/client/bills/CardData;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 357
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->skip_verification_email:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 358
    sget-object v0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->authentication_detail:Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 359
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 360
    invoke-virtual {p2}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 338
    check-cast p2, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$ProtoAdapter_LinkCardRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;)I
    .locals 4

    .line 345
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->request_uuid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->card_data:Lcom/squareup/protos/client/bills/CardData;

    const/4 v3, 0x3

    .line 346
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->skip_verification_email:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 347
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->authentication_detail:Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    const/4 v3, 0x4

    .line 348
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->merchant_token:Ljava/lang/String;

    const/4 v3, 0x2

    .line 349
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 350
    invoke-virtual {p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 338
    check-cast p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$ProtoAdapter_LinkCardRequest;->encodedSize(Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;
    .locals 2

    .line 385
    invoke-virtual {p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->newBuilder()Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;

    move-result-object p1

    .line 386
    iget-object v0, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData;

    iput-object v0, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    .line 387
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->authentication_detail:Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->authentication_detail:Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    iput-object v0, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->authentication_detail:Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    .line 388
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 389
    invoke-virtual {p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->build()Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 338
    check-cast p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$ProtoAdapter_LinkCardRequest;->redact(Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;)Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;

    move-result-object p1

    return-object p1
.end method
