.class public final Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;
.super Lcom/squareup/wire/Message;
.source "LinkCardRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$ProtoAdapter_LinkCardRequest;,
        Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;,
        Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;",
        "Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_REQUEST_UUID:Ljava/lang/String; = ""

.field public static final DEFAULT_SKIP_VERIFICATION_EMAIL:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final authentication_detail:Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.instantdeposits.LinkCardRequest$LinkCardAuthenticationDetail#ADAPTER"
        tag = 0x4
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final card_data:Lcom/squareup/protos/client/bills/CardData;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final request_uuid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final skip_verification_email:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$ProtoAdapter_LinkCardRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$ProtoAdapter_LinkCardRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->DEFAULT_SKIP_VERIFICATION_EMAIL:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/CardData;Ljava/lang/Boolean;Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;Ljava/lang/String;)V
    .locals 7

    .line 83
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/CardData;Ljava/lang/Boolean;Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/CardData;Ljava/lang/Boolean;Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 89
    sget-object v0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 90
    iput-object p1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->request_uuid:Ljava/lang/String;

    .line 91
    iput-object p2, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->card_data:Lcom/squareup/protos/client/bills/CardData;

    .line 92
    iput-object p3, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->skip_verification_email:Ljava/lang/Boolean;

    .line 93
    iput-object p4, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->authentication_detail:Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    .line 94
    iput-object p5, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->merchant_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 112
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 113
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->request_uuid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->request_uuid:Ljava/lang/String;

    .line 115
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->card_data:Lcom/squareup/protos/client/bills/CardData;

    iget-object v3, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->card_data:Lcom/squareup/protos/client/bills/CardData;

    .line 116
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->skip_verification_email:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->skip_verification_email:Ljava/lang/Boolean;

    .line 117
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->authentication_detail:Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    iget-object v3, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->authentication_detail:Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    .line 118
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->merchant_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->merchant_token:Ljava/lang/String;

    .line 119
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 124
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 126
    invoke-virtual {p0}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->request_uuid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->card_data:Lcom/squareup/protos/client/bills/CardData;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->skip_verification_email:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->authentication_detail:Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 132
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;
    .locals 2

    .line 99
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;-><init>()V

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->request_uuid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->request_uuid:Ljava/lang/String;

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->card_data:Lcom/squareup/protos/client/bills/CardData;

    iput-object v1, v0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->skip_verification_email:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->skip_verification_email:Ljava/lang/Boolean;

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->authentication_detail:Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    iput-object v1, v0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->authentication_detail:Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->merchant_token:Ljava/lang/String;

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->newBuilder()Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->request_uuid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", request_uuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->request_uuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->card_data:Lcom/squareup/protos/client/bills/CardData;

    if-eqz v1, :cond_1

    const-string v1, ", card_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->card_data:Lcom/squareup/protos/client/bills/CardData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 142
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->skip_verification_email:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", skip_verification_email="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->skip_verification_email:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 143
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->authentication_detail:Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    if-eqz v1, :cond_3

    const-string v1, ", authentication_detail="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->authentication_detail:Lcom/squareup/protos/client/instantdeposits/LinkCardRequest$LinkCardAuthenticationDetail;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 144
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LinkCardRequest{"

    .line 145
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
