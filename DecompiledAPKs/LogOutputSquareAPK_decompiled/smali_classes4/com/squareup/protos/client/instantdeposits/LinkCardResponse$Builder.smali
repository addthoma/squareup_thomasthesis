.class public final Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LinkCardResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/instantdeposits/LinkCardResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/instantdeposits/LinkCardResponse;",
        "Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public state:Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$State;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/instantdeposits/LinkCardResponse;
    .locals 4

    .line 116
    new-instance v0, Lcom/squareup/protos/client/instantdeposits/LinkCardResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$Builder;->state:Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$State;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/instantdeposits/LinkCardResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$State;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$Builder;->build()Lcom/squareup/protos/client/instantdeposits/LinkCardResponse;

    move-result-object v0

    return-object v0
.end method

.method public state(Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$State;)Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$Builder;
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$Builder;->state:Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$State;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$Builder;
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
