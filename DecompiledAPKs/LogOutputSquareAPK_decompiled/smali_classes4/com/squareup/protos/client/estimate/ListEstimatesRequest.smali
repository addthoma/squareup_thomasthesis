.class public final Lcom/squareup/protos/client/estimate/ListEstimatesRequest;
.super Lcom/squareup/wire/Message;
.source "ListEstimatesRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/estimate/ListEstimatesRequest$ProtoAdapter_ListEstimatesRequest;,
        Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;,
        Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/estimate/ListEstimatesRequest;",
        "Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/estimate/ListEstimatesRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CHECK_ARCHIVE_FOR_MATCHES:Ljava/lang/Boolean;

.field public static final DEFAULT_CONTACT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_INCLUDE_ARCHIVED:Ljava/lang/Boolean;

.field public static final DEFAULT_LIMIT:Ljava/lang/Integer;

.field public static final DEFAULT_PAGING_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_QUERY:Ljava/lang/String; = ""

.field public static final DEFAULT_STATE_FILTER:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

.field private static final serialVersionUID:J


# instance fields
.field public final check_archive_for_matches:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final contact_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final date_range:Lcom/squareup/protos/common/time/DateTimeInterval;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTimeInterval#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final include_archived:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final limit:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field

.field public final paging_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final query:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final state_filter:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.estimate.ListEstimatesRequest$StateFilter#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 25
    new-instance v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$ProtoAdapter_ListEstimatesRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$ProtoAdapter_ListEstimatesRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->DEFAULT_LIMIT:Ljava/lang/Integer;

    .line 35
    sget-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->UNKNOWN:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    sput-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->DEFAULT_STATE_FILTER:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    .line 39
    sput-object v1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->DEFAULT_CHECK_ARCHIVE_FOR_MATCHES:Ljava/lang/Boolean;

    .line 41
    sput-object v1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->DEFAULT_INCLUDE_ARCHIVED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 10

    .line 124
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 130
    sget-object v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->paging_key:Ljava/lang/String;

    .line 132
    iput-object p2, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->limit:Ljava/lang/Integer;

    .line 133
    iput-object p3, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->query:Ljava/lang/String;

    .line 134
    iput-object p4, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 135
    iput-object p5, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->state_filter:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    .line 136
    iput-object p6, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->contact_token:Ljava/lang/String;

    .line 137
    iput-object p7, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->check_archive_for_matches:Ljava/lang/Boolean;

    .line 138
    iput-object p8, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->include_archived:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 159
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 160
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;

    .line 161
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->paging_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->paging_key:Ljava/lang/String;

    .line 162
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->limit:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->limit:Ljava/lang/Integer;

    .line 163
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->query:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->query:Ljava/lang/String;

    .line 164
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 165
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->state_filter:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->state_filter:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    .line 166
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->contact_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->contact_token:Ljava/lang/String;

    .line 167
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->check_archive_for_matches:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->check_archive_for_matches:Ljava/lang/Boolean;

    .line 168
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->include_archived:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->include_archived:Ljava/lang/Boolean;

    .line 169
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 174
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 176
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->paging_key:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->limit:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->query:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 180
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTimeInterval;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 181
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->state_filter:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 182
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->contact_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 183
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->check_archive_for_matches:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 184
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->include_archived:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 185
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;
    .locals 2

    .line 143
    new-instance v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;-><init>()V

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->paging_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->paging_key:Ljava/lang/String;

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->limit:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->limit:Ljava/lang/Integer;

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->query:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->query:Ljava/lang/String;

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->state_filter:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->state_filter:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    .line 149
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->contact_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->contact_token:Ljava/lang/String;

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->check_archive_for_matches:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->check_archive_for_matches:Ljava/lang/Boolean;

    .line 151
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->include_archived:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->include_archived:Ljava/lang/Boolean;

    .line 152
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->newBuilder()Lcom/squareup/protos/client/estimate/ListEstimatesRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 192
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->paging_key:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", paging_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->paging_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->limit:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->limit:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 195
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->query:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", query="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->query:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v1, :cond_3

    const-string v1, ", date_range="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 197
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->state_filter:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    if-eqz v1, :cond_4

    const-string v1, ", state_filter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->state_filter:Lcom/squareup/protos/client/estimate/ListEstimatesRequest$StateFilter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 198
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->contact_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", contact_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->contact_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->check_archive_for_matches:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", check_archive_for_matches="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->check_archive_for_matches:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 200
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->include_archived:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", include_archived="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ListEstimatesRequest;->include_archived:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ListEstimatesRequest{"

    .line 201
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
