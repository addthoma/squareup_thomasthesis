.class public final Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ConvertEstimateToInvoiceRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceRequest;",
        "Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public package_token:Ljava/lang/String;

.field public token_pair:Lcom/squareup/protos/client/IdPair;

.field public version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 116
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceRequest;
    .locals 5

    .line 142
    new-instance v0, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceRequest$Builder;->token_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceRequest$Builder;->version:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceRequest$Builder;->package_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceRequest;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceRequest$Builder;->build()Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceRequest;

    move-result-object v0

    return-object v0
.end method

.method public package_token(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceRequest$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceRequest$Builder;->package_token:Ljava/lang/String;

    return-object p0
.end method

.method public token_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceRequest$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceRequest$Builder;->token_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public version(Ljava/lang/Integer;)Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceRequest$Builder;
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/ConvertEstimateToInvoiceRequest$Builder;->version:Ljava/lang/Integer;

    return-object p0
.end method
