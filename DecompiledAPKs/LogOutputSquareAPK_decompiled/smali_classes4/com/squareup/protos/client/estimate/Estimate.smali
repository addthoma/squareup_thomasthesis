.class public final Lcom/squareup/protos/client/estimate/Estimate;
.super Lcom/squareup/wire/Message;
.source "Estimate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/estimate/Estimate$ProtoAdapter_Estimate;,
        Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;,
        Lcom/squareup/protos/client/estimate/Estimate$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/estimate/Estimate;",
        "Lcom/squareup/protos/client/estimate/Estimate$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/estimate/Estimate;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DELIVERY_METHOD:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_ESTIMATE_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_MERCHANT_ESTIMATE_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_VERSION:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final additional_recipient_email:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        redacted = true
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final attachment:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.FileAttachmentMetadata#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xd
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;"
        }
    .end annotation
.end field

.field public final cart:Lcom/squareup/protos/client/bills/Cart;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final creator_details:Lcom/squareup/protos/client/CreatorDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.CreatorDetails#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.estimate.Estimate$DeliveryMethod#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final estimate_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final expires_on:Lcom/squareup/protos/common/time/YearMonthDay;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.YearMonthDay#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final merchant_estimate_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final packages:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.estimate.EstimatePackage#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xe
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/estimate/EstimatePackage;",
            ">;"
        }
    .end annotation
.end field

.field public final payer:Lcom/squareup/protos/client/invoice/InvoiceContact;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceContact#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final scheduled_on:Lcom/squareup/protos/common/time/YearMonthDay;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.YearMonthDay#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xb
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/protos/client/estimate/Estimate$ProtoAdapter_Estimate;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/Estimate$ProtoAdapter_Estimate;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/estimate/Estimate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 44
    sget-object v0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->UNKNOWN_METHOD:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    sput-object v0, Lcom/squareup/protos/client/estimate/Estimate;->DEFAULT_DELIVERY_METHOD:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    const/4 v0, 0x0

    .line 46
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/estimate/Estimate;->DEFAULT_VERSION:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/client/invoice/InvoiceContact;Ljava/util/List;Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/CreatorDetails;Ljava/lang/Integer;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;Ljava/util/List;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Lcom/squareup/protos/client/invoice/InvoiceContact;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Lcom/squareup/protos/client/CreatorDetails;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/estimate/EstimatePackage;",
            ">;)V"
        }
    .end annotation

    .line 186
    sget-object v15, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/protos/client/estimate/Estimate;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/client/invoice/InvoiceContact;Ljava/util/List;Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/CreatorDetails;Ljava/lang/Integer;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/client/invoice/InvoiceContact;Ljava/util/List;Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/CreatorDetails;Ljava/lang/Integer;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Lcom/squareup/protos/client/invoice/InvoiceContact;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Lcom/squareup/protos/client/CreatorDetails;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/estimate/EstimatePackage;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    .line 195
    sget-object v1, Lcom/squareup/protos/client/estimate/Estimate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p15

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 196
    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate;->id_pair:Lcom/squareup/protos/client/IdPair;

    move-object v1, p2

    .line 197
    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate;->description:Ljava/lang/String;

    move-object v1, p3

    .line 198
    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate;->merchant_estimate_number:Ljava/lang/String;

    move-object v1, p4

    .line 199
    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate;->estimate_name:Ljava/lang/String;

    move-object v1, p5

    .line 200
    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate;->expires_on:Lcom/squareup/protos/common/time/YearMonthDay;

    move-object v1, p6

    .line 201
    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    const-string v1, "additional_recipient_email"

    move-object v2, p7

    .line 202
    invoke-static {v1, p7}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate;->additional_recipient_email:Ljava/util/List;

    move-object v1, p8

    .line 203
    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    move-object v1, p9

    .line 204
    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate;->cart:Lcom/squareup/protos/client/bills/Cart;

    move-object v1, p10

    .line 205
    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    move-object v1, p11

    .line 206
    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate;->version:Ljava/lang/Integer;

    move-object v1, p12

    .line 207
    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate;->scheduled_on:Lcom/squareup/protos/common/time/YearMonthDay;

    const-string v1, "attachment"

    move-object/from16 v2, p13

    .line 208
    invoke-static {v1, v2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate;->attachment:Ljava/util/List;

    const-string v1, "packages"

    move-object/from16 v2, p14

    .line 209
    invoke-static {v1, v2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate;->packages:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 236
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/estimate/Estimate;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 237
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/estimate/Estimate;

    .line 238
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/Estimate;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/Estimate;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/Estimate;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 239
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/Estimate;->description:Ljava/lang/String;

    .line 240
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->merchant_estimate_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/Estimate;->merchant_estimate_number:Ljava/lang/String;

    .line 241
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->estimate_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/Estimate;->estimate_name:Ljava/lang/String;

    .line 242
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->expires_on:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/Estimate;->expires_on:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 243
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/Estimate;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    .line 244
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->additional_recipient_email:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/Estimate;->additional_recipient_email:Ljava/util/List;

    .line 245
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/Estimate;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    .line 246
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/Estimate;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 247
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/Estimate;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 248
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->version:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/Estimate;->version:Ljava/lang/Integer;

    .line 249
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->scheduled_on:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/Estimate;->scheduled_on:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 250
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->attachment:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/Estimate;->attachment:Ljava/util/List;

    .line 251
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->packages:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/estimate/Estimate;->packages:Ljava/util/List;

    .line 252
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 257
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_b

    .line 259
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/Estimate;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 260
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 261
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->description:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 262
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->merchant_estimate_number:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 263
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->estimate_name:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 264
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->expires_on:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/YearMonthDay;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 265
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/InvoiceContact;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 266
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->additional_recipient_email:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 267
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 268
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 269
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 270
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->version:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 271
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->scheduled_on:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/YearMonthDay;->hashCode()I

    move-result v2

    :cond_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 272
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->attachment:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 273
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->packages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 274
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_b
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/estimate/Estimate$Builder;
    .locals 2

    .line 214
    new-instance v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/Estimate$Builder;-><init>()V

    .line 215
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 216
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->description:Ljava/lang/String;

    .line 217
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->merchant_estimate_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->merchant_estimate_number:Ljava/lang/String;

    .line 218
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->estimate_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->estimate_name:Ljava/lang/String;

    .line 219
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->expires_on:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->expires_on:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 220
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->additional_recipient_email:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->additional_recipient_email:Ljava/util/List;

    .line 222
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    .line 223
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->cart:Lcom/squareup/protos/client/bills/Cart;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 224
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 225
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->version:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->version:Ljava/lang/Integer;

    .line 226
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->scheduled_on:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->scheduled_on:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 227
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->attachment:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->attachment:Ljava/util/List;

    .line 228
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->packages:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/Estimate$Builder;->packages:Ljava/util/List;

    .line 229
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/Estimate;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/estimate/Estimate$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/Estimate;->newBuilder()Lcom/squareup/protos/client/estimate/Estimate$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 281
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 282
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 283
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->description:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", description=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->merchant_estimate_number:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", merchant_estimate_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->merchant_estimate_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->estimate_name:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", estimate_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->expires_on:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_4

    const-string v1, ", expires_on="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->expires_on:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 287
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    if-eqz v1, :cond_5

    const-string v1, ", payer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 288
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->additional_recipient_email:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ", additional_recipient_email=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    if-eqz v1, :cond_7

    const-string v1, ", delivery_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->delivery_method:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 290
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_8

    const-string v1, ", cart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 291
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_9

    const-string v1, ", creator_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 292
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->version:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 293
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->scheduled_on:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_b

    const-string v1, ", scheduled_on="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->scheduled_on:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 294
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->attachment:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, ", attachment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->attachment:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 295
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->packages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, ", packages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->packages:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_d
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Estimate{"

    .line 296
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
