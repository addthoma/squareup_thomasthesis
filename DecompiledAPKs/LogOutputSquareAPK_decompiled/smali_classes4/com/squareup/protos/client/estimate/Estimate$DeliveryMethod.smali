.class public final enum Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;
.super Ljava/lang/Enum;
.source "Estimate.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/Estimate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DeliveryMethod"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod$ProtoAdapter_DeliveryMethod;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum EMAIL:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

.field public static final enum SHARE_LINK:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

.field public static final enum UNKNOWN_METHOD:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

.field public static final enum UNSELECTED:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 463
    new-instance v0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_METHOD"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->UNKNOWN_METHOD:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    .line 465
    new-instance v0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    const/4 v2, 0x1

    const-string v3, "UNSELECTED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->UNSELECTED:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    .line 467
    new-instance v0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    const/4 v3, 0x2

    const-string v4, "EMAIL"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->EMAIL:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    .line 469
    new-instance v0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    const/4 v4, 0x3

    const-string v5, "SHARE_LINK"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->SHARE_LINK:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    .line 459
    sget-object v5, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->UNKNOWN_METHOD:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->UNSELECTED:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->EMAIL:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->SHARE_LINK:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->$VALUES:[Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    .line 471
    new-instance v0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod$ProtoAdapter_DeliveryMethod;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod$ProtoAdapter_DeliveryMethod;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 475
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 476
    iput p3, p0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 487
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->SHARE_LINK:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    return-object p0

    .line 486
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->EMAIL:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    return-object p0

    .line 485
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->UNSELECTED:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    return-object p0

    .line 484
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->UNKNOWN_METHOD:Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;
    .locals 1

    .line 459
    const-class v0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;
    .locals 1

    .line 459
    sget-object v0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->$VALUES:[Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 494
    iget v0, p0, Lcom/squareup/protos/client/estimate/Estimate$DeliveryMethod;->value:I

    return v0
.end method
