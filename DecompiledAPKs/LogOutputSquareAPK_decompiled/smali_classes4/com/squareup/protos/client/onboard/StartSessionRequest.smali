.class public final Lcom/squareup/protos/client/onboard/StartSessionRequest;
.super Lcom/squareup/wire/Message;
.source "StartSessionRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/onboard/StartSessionRequest$ProtoAdapter_StartSessionRequest;,
        Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/onboard/StartSessionRequest;",
        "Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/onboard/StartSessionRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CLIENT_REQUEST_UUID:Ljava/lang/String; = ""

.field public static final DEFAULT_COUNTRY_CODE:Lcom/squareup/protos/common/countries/Country;

.field public static final DEFAULT_FLOW_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_LANGUAGE_CODE:Lcom/squareup/protos/common/languages/Language;

.field public static final DEFAULT_SPEC_VERSION:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final client_request_uuid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final country_code:Lcom/squareup/protos/common/countries/Country;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.countries.Country#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final flow_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final language_code:Lcom/squareup/protos/common/languages/Language;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.languages.Language#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final spec_version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/onboard/StartSessionRequest$ProtoAdapter_StartSessionRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/onboard/StartSessionRequest$ProtoAdapter_StartSessionRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 27
    sget-object v0, Lcom/squareup/protos/common/countries/Country;->US:Lcom/squareup/protos/common/countries/Country;

    sput-object v0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->DEFAULT_COUNTRY_CODE:Lcom/squareup/protos/common/countries/Country;

    .line 29
    sget-object v0, Lcom/squareup/protos/common/languages/Language;->EN:Lcom/squareup/protos/common/languages/Language;

    sput-object v0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->DEFAULT_LANGUAGE_CODE:Lcom/squareup/protos/common/languages/Language;

    const/4 v0, 0x0

    .line 33
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->DEFAULT_SPEC_VERSION:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/countries/Country;Lcom/squareup/protos/common/languages/Language;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 7

    .line 73
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/onboard/StartSessionRequest;-><init>(Lcom/squareup/protos/common/countries/Country;Lcom/squareup/protos/common/languages/Language;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/countries/Country;Lcom/squareup/protos/common/languages/Language;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 78
    sget-object v0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 79
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->country_code:Lcom/squareup/protos/common/countries/Country;

    .line 80
    iput-object p2, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->language_code:Lcom/squareup/protos/common/languages/Language;

    .line 81
    iput-object p3, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->flow_name:Ljava/lang/String;

    .line 82
    iput-object p4, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->spec_version:Ljava/lang/Integer;

    .line 83
    iput-object p5, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->client_request_uuid:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 101
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/onboard/StartSessionRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 102
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/onboard/StartSessionRequest;

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/StartSessionRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/StartSessionRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->country_code:Lcom/squareup/protos/common/countries/Country;

    iget-object v3, p1, Lcom/squareup/protos/client/onboard/StartSessionRequest;->country_code:Lcom/squareup/protos/common/countries/Country;

    .line 104
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->language_code:Lcom/squareup/protos/common/languages/Language;

    iget-object v3, p1, Lcom/squareup/protos/client/onboard/StartSessionRequest;->language_code:Lcom/squareup/protos/common/languages/Language;

    .line 105
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->flow_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/onboard/StartSessionRequest;->flow_name:Ljava/lang/String;

    .line 106
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->spec_version:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/onboard/StartSessionRequest;->spec_version:Ljava/lang/Integer;

    .line 107
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->client_request_uuid:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/onboard/StartSessionRequest;->client_request_uuid:Ljava/lang/String;

    .line 108
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 113
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 115
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/StartSessionRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->country_code:Lcom/squareup/protos/common/countries/Country;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/countries/Country;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->language_code:Lcom/squareup/protos/common/languages/Language;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/languages/Language;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->flow_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->spec_version:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->client_request_uuid:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 121
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;
    .locals 2

    .line 88
    new-instance v0, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;-><init>()V

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->country_code:Lcom/squareup/protos/common/countries/Country;

    iput-object v1, v0, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->country_code:Lcom/squareup/protos/common/countries/Country;

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->language_code:Lcom/squareup/protos/common/languages/Language;

    iput-object v1, v0, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->language_code:Lcom/squareup/protos/common/languages/Language;

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->flow_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->flow_name:Ljava/lang/String;

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->spec_version:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->spec_version:Ljava/lang/Integer;

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->client_request_uuid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->client_request_uuid:Ljava/lang/String;

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/StartSessionRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/StartSessionRequest;->newBuilder()Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->country_code:Lcom/squareup/protos/common/countries/Country;

    if-eqz v1, :cond_0

    const-string v1, ", country_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->country_code:Lcom/squareup/protos/common/countries/Country;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 130
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->language_code:Lcom/squareup/protos/common/languages/Language;

    if-eqz v1, :cond_1

    const-string v1, ", language_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->language_code:Lcom/squareup/protos/common/languages/Language;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 131
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->flow_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", flow_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->flow_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->spec_version:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", spec_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->spec_version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 133
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->client_request_uuid:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", client_request_uuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest;->client_request_uuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "StartSessionRequest{"

    .line 134
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
