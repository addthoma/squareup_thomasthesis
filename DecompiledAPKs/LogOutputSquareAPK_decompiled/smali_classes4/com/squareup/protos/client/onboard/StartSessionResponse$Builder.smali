.class public final Lcom/squareup/protos/client/onboard/StartSessionResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StartSessionResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/StartSessionResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/onboard/StartSessionResponse;",
        "Lcom/squareup/protos/client/onboard/StartSessionResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public session_token:Ljava/lang/String;

.field public status:Lcom/squareup/protos/client/Status;

.field public step:Lcom/squareup/protos/client/onboard/Step;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 107
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/onboard/StartSessionResponse;
    .locals 5

    .line 127
    new-instance v0, Lcom/squareup/protos/client/onboard/StartSessionResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/onboard/StartSessionResponse$Builder;->session_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/onboard/StartSessionResponse$Builder;->step:Lcom/squareup/protos/client/onboard/Step;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/onboard/StartSessionResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/lang/String;Lcom/squareup/protos/client/onboard/Step;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/StartSessionResponse$Builder;->build()Lcom/squareup/protos/client/onboard/StartSessionResponse;

    move-result-object v0

    return-object v0
.end method

.method public session_token(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/StartSessionResponse$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/StartSessionResponse$Builder;->session_token:Ljava/lang/String;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/onboard/StartSessionResponse$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/StartSessionResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public step(Lcom/squareup/protos/client/onboard/Step;)Lcom/squareup/protos/client/onboard/StartSessionResponse$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/StartSessionResponse$Builder;->step:Lcom/squareup/protos/client/onboard/Step;

    return-object p0
.end method
