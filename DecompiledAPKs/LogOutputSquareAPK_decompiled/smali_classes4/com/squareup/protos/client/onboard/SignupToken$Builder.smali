.class public final Lcom/squareup/protos/client/onboard/SignupToken$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SignupToken.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/SignupToken;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/onboard/SignupToken;",
        "Lcom/squareup/protos/client/onboard/SignupToken$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public free_processing_days:Ljava/lang/Integer;

.field public free_processing_money:Lcom/squareup/protos/common/Money;

.field public referral_url:Ljava/lang/String;

.field public slug:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 139
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/onboard/SignupToken;
    .locals 7

    .line 177
    new-instance v6, Lcom/squareup/protos/client/onboard/SignupToken;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/SignupToken$Builder;->free_processing_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/client/onboard/SignupToken$Builder;->free_processing_days:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/onboard/SignupToken$Builder;->referral_url:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/onboard/SignupToken$Builder;->slug:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/onboard/SignupToken;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/SignupToken$Builder;->build()Lcom/squareup/protos/client/onboard/SignupToken;

    move-result-object v0

    return-object v0
.end method

.method public free_processing_days(Ljava/lang/Integer;)Lcom/squareup/protos/client/onboard/SignupToken$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/SignupToken$Builder;->free_processing_days:Ljava/lang/Integer;

    return-object p0
.end method

.method public free_processing_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/onboard/SignupToken$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/SignupToken$Builder;->free_processing_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public referral_url(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/SignupToken$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/SignupToken$Builder;->referral_url:Ljava/lang/String;

    return-object p0
.end method

.method public slug(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/SignupToken$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/SignupToken$Builder;->slug:Ljava/lang/String;

    return-object p0
.end method
