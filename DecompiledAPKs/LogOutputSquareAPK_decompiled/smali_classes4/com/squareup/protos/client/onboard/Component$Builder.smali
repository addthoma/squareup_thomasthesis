.class public final Lcom/squareup/protos/client/onboard/Component$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Component.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/Component;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/onboard/Component;",
        "Lcom/squareup/protos/client/onboard/Component$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error_message:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public properties:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/onboard/PropertyMapEntry;",
            ">;"
        }
    .end annotation
.end field

.field public type:Lcom/squareup/protos/client/onboard/ComponentType;

.field public validators:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Validator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 141
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 142
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/onboard/Component$Builder;->properties:Ljava/util/Map;

    .line 143
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/onboard/Component$Builder;->validators:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/onboard/Component;
    .locals 8

    .line 175
    new-instance v7, Lcom/squareup/protos/client/onboard/Component;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/Component$Builder;->type:Lcom/squareup/protos/client/onboard/ComponentType;

    iget-object v2, p0, Lcom/squareup/protos/client/onboard/Component$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/onboard/Component$Builder;->properties:Ljava/util/Map;

    iget-object v4, p0, Lcom/squareup/protos/client/onboard/Component$Builder;->validators:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/onboard/Component$Builder;->error_message:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/onboard/Component;-><init>(Lcom/squareup/protos/client/onboard/ComponentType;Ljava/lang/String;Ljava/util/Map;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/Component$Builder;->build()Lcom/squareup/protos/client/onboard/Component;

    move-result-object v0

    return-object v0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Component$Builder;
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Component$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Component$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Component$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public properties(Ljava/util/Map;)Lcom/squareup/protos/client/onboard/Component$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/onboard/PropertyMapEntry;",
            ">;)",
            "Lcom/squareup/protos/client/onboard/Component$Builder;"
        }
    .end annotation

    .line 157
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/Map;)V

    .line 158
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Component$Builder;->properties:Ljava/util/Map;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/onboard/ComponentType;)Lcom/squareup/protos/client/onboard/Component$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Component$Builder;->type:Lcom/squareup/protos/client/onboard/ComponentType;

    return-object p0
.end method

.method public validators(Ljava/util/List;)Lcom/squareup/protos/client/onboard/Component$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Validator;",
            ">;)",
            "Lcom/squareup/protos/client/onboard/Component$Builder;"
        }
    .end annotation

    .line 163
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Component$Builder;->validators:Ljava/util/List;

    return-object p0
.end method
