.class public final Lcom/squareup/protos/client/onboard/Panel$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Panel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/Panel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/onboard/Panel;",
        "Lcom/squareup/protos/client/onboard/Panel$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public components:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Component;",
            ">;"
        }
    .end annotation
.end field

.field public name:Ljava/lang/String;

.field public navigation:Lcom/squareup/protos/client/onboard/Navigation;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 123
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 124
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/onboard/Panel$Builder;->components:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/onboard/Panel;
    .locals 7

    .line 150
    new-instance v6, Lcom/squareup/protos/client/onboard/Panel;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/Panel$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/onboard/Panel$Builder;->title:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/onboard/Panel$Builder;->navigation:Lcom/squareup/protos/client/onboard/Navigation;

    iget-object v4, p0, Lcom/squareup/protos/client/onboard/Panel$Builder;->components:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/onboard/Panel;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/onboard/Navigation;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/Panel$Builder;->build()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object v0

    return-object v0
.end method

.method public components(Ljava/util/List;)Lcom/squareup/protos/client/onboard/Panel$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Component;",
            ">;)",
            "Lcom/squareup/protos/client/onboard/Panel$Builder;"
        }
    .end annotation

    .line 143
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Panel$Builder;->components:Ljava/util/List;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Panel$Builder;
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Panel$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public navigation(Lcom/squareup/protos/client/onboard/Navigation;)Lcom/squareup/protos/client/onboard/Panel$Builder;
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Panel$Builder;->navigation:Lcom/squareup/protos/client/onboard/Navigation;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Panel$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Panel$Builder;->title:Ljava/lang/String;

    return-object p0
.end method
