.class public final Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TenderSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/devicesettings/TenderSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings;",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public disabled_tender:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;"
        }
    .end annotation
.end field

.field public primary_tender:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;"
        }
    .end annotation
.end field

.field public secondary_tender:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 116
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 117
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->primary_tender:Ljava/util/List;

    .line 118
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->secondary_tender:Ljava/util/List;

    .line 119
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->disabled_tender:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 5

    .line 142
    new-instance v0, Lcom/squareup/protos/client/devicesettings/TenderSettings;

    iget-object v1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->primary_tender:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->secondary_tender:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->disabled_tender:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/devicesettings/TenderSettings;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->build()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object v0

    return-object v0
.end method

.method public disabled_tender(Ljava/util/List;)Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;)",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;"
        }
    .end annotation

    .line 135
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->disabled_tender:Ljava/util/List;

    return-object p0
.end method

.method public primary_tender(Ljava/util/List;)Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;)",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;"
        }
    .end annotation

    .line 123
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->primary_tender:Ljava/util/List;

    return-object p0
.end method

.method public secondary_tender(Ljava/util/List;)Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
            ">;)",
            "Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;"
        }
    .end annotation

    .line 129
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/client/devicesettings/TenderSettings$Builder;->secondary_tender:Ljava/util/List;

    return-object p0
.end method
