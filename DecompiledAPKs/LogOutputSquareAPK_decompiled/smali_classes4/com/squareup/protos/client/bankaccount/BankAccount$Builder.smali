.class public final Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BankAccount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bankaccount/BankAccount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bankaccount/BankAccount;",
        "Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public account_name:Ljava/lang/String;

.field public account_number_suffix:Ljava/lang/String;

.field public account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

.field public bank_account_token:Ljava/lang/String;

.field public bank_name:Ljava/lang/String;

.field public primary_institution_number:Ljava/lang/String;

.field public routing_number:Ljava/lang/String;

.field public secondary_institution_number:Ljava/lang/String;

.field public supports_instant_deposit:Ljava/lang/Boolean;

.field public verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

.field public verified_by:Lcom/squareup/protos/common/time/DateTime;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 277
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public account_name(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;
    .locals 0

    .line 308
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->account_name:Ljava/lang/String;

    return-object p0
.end method

.method public account_number_suffix(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;
    .locals 0

    .line 316
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->account_number_suffix:Ljava/lang/String;

    return-object p0
.end method

.method public account_type(Lcom/squareup/protos/client/bankaccount/BankAccountType;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;
    .locals 0

    .line 300
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    return-object p0
.end method

.method public bank_account_token(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;
    .locals 0

    .line 366
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->bank_account_token:Ljava/lang/String;

    return-object p0
.end method

.method public bank_name(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;
    .locals 0

    .line 292
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->bank_name:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bankaccount/BankAccount;
    .locals 14

    .line 372
    new-instance v13, Lcom/squareup/protos/client/bankaccount/BankAccount;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    iget-object v2, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->bank_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    iget-object v4, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->account_name:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->account_number_suffix:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->routing_number:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->verified_by:Lcom/squareup/protos/common/time/DateTime;

    iget-object v8, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->primary_institution_number:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->secondary_institution_number:Ljava/lang/String;

    iget-object v10, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->supports_instant_deposit:Ljava/lang/Boolean;

    iget-object v11, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->bank_account_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/client/bankaccount/BankAccount;-><init>(Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;Ljava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lokio/ByteString;)V

    return-object v13
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 254
    invoke-virtual {p0}, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->build()Lcom/squareup/protos/client/bankaccount/BankAccount;

    move-result-object v0

    return-object v0
.end method

.method public primary_institution_number(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;
    .locals 0

    .line 341
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->primary_institution_number:Ljava/lang/String;

    return-object p0
.end method

.method public routing_number(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 325
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->routing_number:Ljava/lang/String;

    return-object p0
.end method

.method public secondary_institution_number(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;
    .locals 0

    .line 349
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->secondary_institution_number:Ljava/lang/String;

    return-object p0
.end method

.method public supports_instant_deposit(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;
    .locals 0

    .line 357
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->supports_instant_deposit:Ljava/lang/Boolean;

    return-object p0
.end method

.method public verification_state(Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;
    .locals 0

    .line 284
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    return-object p0
.end method

.method public verified_by(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;
    .locals 0

    .line 333
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/BankAccount$Builder;->verified_by:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method
