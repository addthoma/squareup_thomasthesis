.class final Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$ProtoAdapter_GetBankAccountsResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetBankAccountsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetBankAccountsResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 204
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 229
    new-instance v0, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;-><init>()V

    .line 230
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 231
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 239
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 237
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->password_required_to_link(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;

    goto :goto_0

    .line 236
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->recently_failed_verification_bank_account(Lcom/squareup/protos/client/bankaccount/BankAccount;)Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;

    goto :goto_0

    .line 235
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->recently_failed_bank_account(Lcom/squareup/protos/client/bankaccount/BankAccount;)Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;

    goto :goto_0

    .line 234
    :cond_3
    sget-object v3, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->pending_bank_account(Lcom/squareup/protos/client/bankaccount/BankAccount;)Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;

    goto :goto_0

    .line 233
    :cond_4
    sget-object v3, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->active_bank_account(Lcom/squareup/protos/client/bankaccount/BankAccount;)Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;

    goto :goto_0

    .line 243
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 244
    invoke-virtual {v0}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->build()Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 202
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$ProtoAdapter_GetBankAccountsResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 219
    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;->active_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 220
    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;->pending_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 221
    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;->recently_failed_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 222
    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;->recently_failed_verification_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 223
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;->password_required_to_link:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 224
    invoke-virtual {p2}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 202
    check-cast p2, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$ProtoAdapter_GetBankAccountsResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;)I
    .locals 4

    .line 209
    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;->active_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;->pending_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    const/4 v3, 0x2

    .line 210
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;->recently_failed_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    const/4 v3, 0x3

    .line 211
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;->recently_failed_verification_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    const/4 v3, 0x4

    .line 212
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;->password_required_to_link:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 213
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 214
    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 202
    check-cast p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$ProtoAdapter_GetBankAccountsResponse;->encodedSize(Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;)Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;
    .locals 2

    .line 249
    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;->newBuilder()Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;

    move-result-object p1

    .line 250
    iget-object v0, p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->active_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->active_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bankaccount/BankAccount;

    iput-object v0, p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->active_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    .line 251
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->pending_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->pending_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bankaccount/BankAccount;

    iput-object v0, p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->pending_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    .line 252
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->recently_failed_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->recently_failed_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bankaccount/BankAccount;

    iput-object v0, p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->recently_failed_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    .line 253
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->recently_failed_verification_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->recently_failed_verification_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bankaccount/BankAccount;

    iput-object v0, p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->recently_failed_verification_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    .line 254
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 255
    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$Builder;->build()Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 202
    check-cast p1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse$ProtoAdapter_GetBankAccountsResponse;->redact(Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;)Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;

    move-result-object p1

    return-object p1
.end method
