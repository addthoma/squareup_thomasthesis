.class public final enum Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;
.super Ljava/lang/Enum;
.source "BankAccountVerificationState.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState$ProtoAdapter_BankAccountVerificationState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AWAITING_DEBIT_AUTHORIZATION:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

.field public static final enum AWAITING_EMAIL_CONFIRMATION:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

.field public static final enum BANK_ACCOUNT_VERIFICATION_STATE_DO_NOT_USE:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

.field public static final enum FAILED:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

.field public static final enum VERIFICATION_CANCELED:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

.field public static final enum VERIFICATION_IN_PROGRESS:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

.field public static final enum VERIFIED:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 17
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    const/4 v1, 0x0

    const-string v2, "BANK_ACCOUNT_VERIFICATION_STATE_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->BANK_ACCOUNT_VERIFICATION_STATE_DO_NOT_USE:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    .line 22
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    const/4 v2, 0x1

    const-string v3, "AWAITING_DEBIT_AUTHORIZATION"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->AWAITING_DEBIT_AUTHORIZATION:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    .line 27
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    const/4 v3, 0x2

    const-string v4, "AWAITING_EMAIL_CONFIRMATION"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->AWAITING_EMAIL_CONFIRMATION:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    .line 34
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    const/4 v4, 0x3

    const-string v5, "VERIFIED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->VERIFIED:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    .line 39
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    const/4 v5, 0x4

    const-string v6, "VERIFICATION_CANCELED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->VERIFICATION_CANCELED:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    .line 44
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    const/4 v6, 0x5

    const-string v7, "VERIFICATION_IN_PROGRESS"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->VERIFICATION_IN_PROGRESS:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    .line 49
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    const/4 v7, 0x6

    const-string v8, "FAILED"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->FAILED:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    .line 13
    sget-object v8, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->BANK_ACCOUNT_VERIFICATION_STATE_DO_NOT_USE:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->AWAITING_DEBIT_AUTHORIZATION:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->AWAITING_EMAIL_CONFIRMATION:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->VERIFIED:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->VERIFICATION_CANCELED:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->VERIFICATION_IN_PROGRESS:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->FAILED:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->$VALUES:[Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    .line 51
    new-instance v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState$ProtoAdapter_BankAccountVerificationState;

    invoke-direct {v0}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState$ProtoAdapter_BankAccountVerificationState;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    iput p3, p0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 70
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->FAILED:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    return-object p0

    .line 69
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->VERIFICATION_IN_PROGRESS:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    return-object p0

    .line 68
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->VERIFICATION_CANCELED:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    return-object p0

    .line 67
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->VERIFIED:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    return-object p0

    .line 66
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->AWAITING_EMAIL_CONFIRMATION:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    return-object p0

    .line 65
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->AWAITING_DEBIT_AUTHORIZATION:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    return-object p0

    .line 64
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->BANK_ACCOUNT_VERIFICATION_STATE_DO_NOT_USE:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->$VALUES:[Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 77
    iget v0, p0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->value:I

    return v0
.end method
