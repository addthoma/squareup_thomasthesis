.class final Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType$ProtoAdapter_AggregateCohortType;
.super Lcom/squareup/wire/EnumAdapter;
.source "GetLoyaltyReportResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AggregateCohortType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 298
    const-class v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;
    .locals 0

    .line 303
    invoke-static {p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->fromValue(I)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 296
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType$ProtoAdapter_AggregateCohortType;->fromValue(I)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    move-result-object p1

    return-object p1
.end method
