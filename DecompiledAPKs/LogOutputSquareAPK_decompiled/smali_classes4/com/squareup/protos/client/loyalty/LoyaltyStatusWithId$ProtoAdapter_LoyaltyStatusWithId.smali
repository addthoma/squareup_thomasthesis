.class final Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$ProtoAdapter_LoyaltyStatusWithId;
.super Lcom/squareup/wire/ProtoAdapter;
.source "LoyaltyStatusWithId.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_LoyaltyStatusWithId"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 117
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 136
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;-><init>()V

    .line 137
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 138
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 143
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 141
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;->status(Lcom/squareup/protos/client/loyalty/LoyaltyStatus;)Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;

    goto :goto_0

    .line 140
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;->customer_identifier(Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;)Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;

    goto :goto_0

    .line 147
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 148
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 115
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$ProtoAdapter_LoyaltyStatusWithId;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 129
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 130
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;->status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 131
    invoke-virtual {p2}, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 115
    check-cast p2, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$ProtoAdapter_LoyaltyStatusWithId;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;)I
    .locals 4

    .line 122
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;->status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    const/4 v3, 0x2

    .line 123
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 115
    check-cast p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$ProtoAdapter_LoyaltyStatusWithId;->encodedSize(Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;)Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;
    .locals 2

    .line 153
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;->newBuilder()Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;

    move-result-object p1

    .line 154
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    .line 155
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;->status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;->status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;->status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    .line 156
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 157
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 115
    check-cast p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId$ProtoAdapter_LoyaltyStatusWithId;->redact(Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;)Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;

    move-result-object p1

    return-object p1
.end method
