.class public final Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;
.super Lcom/squareup/wire/Message;
.source "AdjustPunchesRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ProtoAdapter_AdjustPunchesRequest;,
        Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;,
        Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;,
        Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;",
        "Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ADJUSTMENT:Ljava/lang/Integer;

.field public static final DEFAULT_BALANCE_BEFORE_ADJUSTMENT:Ljava/lang/Integer;

.field public static final DEFAULT_CURRENT_STARS:Ljava/lang/Long;

.field public static final DEFAULT_DECREMENT_REASON:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

.field public static final DEFAULT_INCREMENT_REASON:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

.field public static final DEFAULT_LOYALTY_ACCOUNT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final adjustment:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field

.field public final balance_before_adjustment:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x8
    .end annotation
.end field

.field public final creator_details:Lcom/squareup/protos/client/CreatorDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.CreatorDetails#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final current_stars:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x3
    .end annotation
.end field

.field public final customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyCustomerIdentifier#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final decrement_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.AdjustPunchesRequest$ManualDecrementReason#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final increment_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.AdjustPunchesRequest$ManualIncrementReason#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final loyalty_account_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 25
    new-instance v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ProtoAdapter_AdjustPunchesRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ProtoAdapter_AdjustPunchesRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->DEFAULT_ADJUSTMENT:Ljava/lang/Integer;

    const-wide/16 v1, 0x0

    .line 31
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->DEFAULT_CURRENT_STARS:Ljava/lang/Long;

    .line 33
    sput-object v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->DEFAULT_BALANCE_BEFORE_ADJUSTMENT:Ljava/lang/Integer;

    .line 37
    sget-object v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;->DECREMENT_REASON_UNKNOWN:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    sput-object v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->DEFAULT_DECREMENT_REASON:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    .line 39
    sget-object v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->INCREMENT_REASON_UNKNOWN:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    sput-object v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->DEFAULT_INCREMENT_REASON:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Long;Lcom/squareup/protos/client/CreatorDetails;Ljava/lang/Integer;Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;)V
    .locals 10

    .line 111
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;-><init>(Ljava/lang/Integer;Ljava/lang/Long;Lcom/squareup/protos/client/CreatorDetails;Ljava/lang/Integer;Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Long;Lcom/squareup/protos/client/CreatorDetails;Ljava/lang/Integer;Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;Lokio/ByteString;)V
    .locals 1

    .line 119
    sget-object v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 120
    invoke-static {p5, p6}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p9

    const/4 v0, 0x1

    if-gt p9, v0, :cond_1

    .line 123
    invoke-static {p7, p8}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p9

    if-gt p9, v0, :cond_0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->adjustment:Ljava/lang/Integer;

    .line 127
    iput-object p2, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->current_stars:Ljava/lang/Long;

    .line 128
    iput-object p3, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 129
    iput-object p4, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->balance_before_adjustment:Ljava/lang/Integer;

    .line 130
    iput-object p5, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    .line 131
    iput-object p6, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->loyalty_account_token:Ljava/lang/String;

    .line 132
    iput-object p7, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->decrement_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    .line 133
    iput-object p8, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->increment_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    return-void

    .line 124
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of decrement_reason, increment_reason may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 121
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of customer_identifier, loyalty_account_token may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 154
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 155
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;

    .line 156
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->adjustment:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->adjustment:Ljava/lang/Integer;

    .line 157
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->current_stars:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->current_stars:Ljava/lang/Long;

    .line 158
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->balance_before_adjustment:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->balance_before_adjustment:Ljava/lang/Integer;

    .line 160
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->loyalty_account_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->loyalty_account_token:Ljava/lang/String;

    .line 162
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->decrement_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->decrement_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    .line 163
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->increment_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->increment_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    .line 164
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 169
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 171
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->adjustment:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->current_stars:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->balance_before_adjustment:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->loyalty_account_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->decrement_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->increment_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 180
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;
    .locals 2

    .line 138
    new-instance v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;-><init>()V

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->adjustment:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->adjustment:Ljava/lang/Integer;

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->current_stars:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->current_stars:Ljava/lang/Long;

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->balance_before_adjustment:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->balance_before_adjustment:Ljava/lang/Integer;

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->loyalty_account_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->loyalty_account_token:Ljava/lang/String;

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->decrement_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->decrement_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->increment_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->increment_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    .line 147
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->newBuilder()Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 188
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->adjustment:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", adjustment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->adjustment:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 189
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->current_stars:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", current_stars="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->current_stars:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 190
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_2

    const-string v1, ", creator_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 191
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->balance_before_adjustment:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", balance_before_adjustment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->balance_before_adjustment:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 192
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    if-eqz v1, :cond_4

    const-string v1, ", customer_identifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 193
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->loyalty_account_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", loyalty_account_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->loyalty_account_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->decrement_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    if-eqz v1, :cond_6

    const-string v1, ", decrement_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->decrement_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 195
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->increment_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    if-eqz v1, :cond_7

    const-string v1, ", increment_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;->increment_reason:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualIncrementReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AdjustPunchesRequest{"

    .line 196
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
