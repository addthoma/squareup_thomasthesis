.class public final Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;
.super Lcom/squareup/wire/Message;
.source "UpdateLoyaltyAccountMappingResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$ProtoAdapter_UpdateLoyaltyAccountMappingResponse;,
        Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;",
        "Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_STATUS:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

.field private static final serialVersionUID:J


# instance fields
.field public final conflicting_contacts:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Contact#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field public final errors:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyRequestError#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;",
            ">;"
        }
    .end annotation
.end field

.field public final loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyAccountMapping#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyRequestStatus#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$ProtoAdapter_UpdateLoyaltyAccountMappingResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$ProtoAdapter_UpdateLoyaltyAccountMappingResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 29
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_INVALID:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    sput-object v0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->DEFAULT_STATUS:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)V"
        }
    .end annotation

    .line 74
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;-><init>(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 80
    sget-object v0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 81
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    .line 82
    iput-object p2, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    const-string p1, "errors"

    .line 83
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->errors:Ljava/util/List;

    const-string p1, "conflicting_contacts"

    .line 84
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->conflicting_contacts:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 101
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 102
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    .line 104
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    .line 105
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->errors:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->errors:Ljava/util/List;

    .line 106
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->conflicting_contacts:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->conflicting_contacts:Ljava/util/List;

    .line 107
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 112
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->conflicting_contacts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 119
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;
    .locals 2

    .line 89
    new-instance v0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;-><init>()V

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->errors:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->errors:Ljava/util/List;

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->conflicting_contacts:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->conflicting_contacts:Ljava/util/List;

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->newBuilder()Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    if-eqz v1, :cond_0

    const-string v1, ", loyalty_account_mapping="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 128
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    if-eqz v1, :cond_1

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 129
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", errors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->errors:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 130
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->conflicting_contacts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", conflicting_contacts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->conflicting_contacts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "UpdateLoyaltyAccountMappingResponse{"

    .line 131
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
