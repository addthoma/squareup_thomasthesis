.class public final Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LoyaltyOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;",
        "Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public include_raw_phone:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 385
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;
    .locals 3

    .line 400
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;->include_raw_phone:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 382
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions;

    move-result-object v0

    return-object v0
.end method

.method public include_raw_phone(Ljava/lang/Boolean;)Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;
    .locals 0

    .line 394
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyOptions$LoyaltyAccountMappingOptions$Builder;->include_raw_phone:Ljava/lang/Boolean;

    return-object p0
.end method
