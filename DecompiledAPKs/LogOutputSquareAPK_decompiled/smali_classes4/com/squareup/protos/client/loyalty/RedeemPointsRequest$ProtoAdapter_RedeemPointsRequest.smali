.class final Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$ProtoAdapter_RedeemPointsRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "RedeemPointsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_RedeemPointsRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 200
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 225
    new-instance v0, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;-><init>()V

    .line 226
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 227
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 235
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 233
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->loyalty_account_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;

    goto :goto_0

    .line 232
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->idempotence_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;

    goto :goto_0

    .line 231
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->return_coupon_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;

    goto :goto_0

    .line 230
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->coupon_definition_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;

    goto :goto_0

    .line 229
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->phone_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;

    goto :goto_0

    .line 239
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 240
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 198
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$ProtoAdapter_RedeemPointsRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 215
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->coupon_definition_token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 216
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->return_coupon_token:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 217
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->idempotence_token:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 218
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->phone_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 219
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->loyalty_account_token:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 220
    invoke-virtual {p2}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 198
    check-cast p2, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$ProtoAdapter_RedeemPointsRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;)I
    .locals 4

    .line 205
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->coupon_definition_token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->return_coupon_token:Ljava/lang/String;

    const/4 v3, 0x3

    .line 206
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->idempotence_token:Ljava/lang/String;

    const/4 v3, 0x4

    .line 207
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->phone_token:Ljava/lang/String;

    const/4 v3, 0x1

    .line 208
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->loyalty_account_token:Ljava/lang/String;

    const/4 v3, 0x5

    .line 209
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 210
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 198
    check-cast p1, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$ProtoAdapter_RedeemPointsRequest;->encodedSize(Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;)Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;
    .locals 0

    .line 245
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;->newBuilder()Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;

    move-result-object p1

    .line 246
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 247
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 198
    check-cast p1, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsRequest$ProtoAdapter_RedeemPointsRequest;->redact(Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;)Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;

    move-result-object p1

    return-object p1
.end method
