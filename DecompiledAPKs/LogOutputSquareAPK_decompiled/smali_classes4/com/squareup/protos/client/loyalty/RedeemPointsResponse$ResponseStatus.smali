.class public final enum Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;
.super Ljava/lang/Enum;
.source "RedeemPointsResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ResponseStatus"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus$ProtoAdapter_ResponseStatus;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum INACTIVE_LOCATION:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

.field public static final enum INSUFFICIENT_POINTS:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

.field public static final enum SUCCESS:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

.field public static final enum UNKNOWN_STATUS:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 166
    new-instance v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_STATUS"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->UNKNOWN_STATUS:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    .line 171
    new-instance v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    const/4 v2, 0x1

    const-string v3, "SUCCESS"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->SUCCESS:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    .line 176
    new-instance v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    const/4 v3, 0x2

    const-string v4, "INSUFFICIENT_POINTS"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->INSUFFICIENT_POINTS:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    .line 183
    new-instance v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    const/4 v4, 0x3

    const-string v5, "INACTIVE_LOCATION"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->INACTIVE_LOCATION:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    .line 165
    sget-object v5, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->UNKNOWN_STATUS:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->SUCCESS:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->INSUFFICIENT_POINTS:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->INACTIVE_LOCATION:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->$VALUES:[Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    .line 185
    new-instance v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus$ProtoAdapter_ResponseStatus;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus$ProtoAdapter_ResponseStatus;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 189
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 190
    iput p3, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 201
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->INACTIVE_LOCATION:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    return-object p0

    .line 200
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->INSUFFICIENT_POINTS:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    return-object p0

    .line 199
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->SUCCESS:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    return-object p0

    .line 198
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->UNKNOWN_STATUS:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;
    .locals 1

    .line 165
    const-class v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;
    .locals 1

    .line 165
    sget-object v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->$VALUES:[Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 208
    iget v0, p0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->value:I

    return v0
.end method
