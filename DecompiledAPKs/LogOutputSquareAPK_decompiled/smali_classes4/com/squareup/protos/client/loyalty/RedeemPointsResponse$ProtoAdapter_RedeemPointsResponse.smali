.class final Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ProtoAdapter_RedeemPointsResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "RedeemPointsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_RedeemPointsResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 225
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 248
    new-instance v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;-><init>()V

    .line 249
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 250
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 264
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 262
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->loyalty_status(Lcom/squareup/protos/client/loyalty/LoyaltyStatus;)Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;

    goto :goto_0

    .line 261
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/coupons/Coupon;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->coupon(Lcom/squareup/protos/client/coupons/Coupon;)Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;

    goto :goto_0

    .line 255
    :cond_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->response_status(Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;)Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 257
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 252
    :cond_3
    sget-object v3, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;

    goto :goto_0

    .line 268
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 269
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 223
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ProtoAdapter_RedeemPointsResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 239
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 240
    sget-object v0, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->response_status:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 241
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 242
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 243
    invoke-virtual {p2}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 223
    check-cast p2, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ProtoAdapter_RedeemPointsResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;)I
    .locals 4

    .line 230
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->response_status:Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ResponseStatus;

    const/4 v3, 0x2

    .line 231
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/coupons/Coupon;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    const/4 v3, 0x3

    .line 232
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    const/4 v3, 0x4

    .line 233
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 234
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 223
    check-cast p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ProtoAdapter_RedeemPointsResponse;->encodedSize(Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;)Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;
    .locals 2

    .line 274
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->newBuilder()Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;

    move-result-object p1

    .line 275
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Status;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 276
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/coupons/Coupon;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    .line 277
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    iput-object v0, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    .line 278
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 279
    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 223
    check-cast p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse$ProtoAdapter_RedeemPointsResponse;->redact(Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;)Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;

    move-result-object p1

    return-object p1
.end method
