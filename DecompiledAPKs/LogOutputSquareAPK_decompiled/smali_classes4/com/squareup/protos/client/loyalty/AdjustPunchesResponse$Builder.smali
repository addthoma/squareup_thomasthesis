.class public final Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AdjustPunchesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse;",
        "Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public coupon:Lcom/squareup/protos/client/coupons/Coupon;

.field public loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 109
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse;
    .locals 5

    .line 132
    new-instance v0, Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse$Builder;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse$Builder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    iget-object v3, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse;-><init>(Lcom/squareup/protos/client/loyalty/LoyaltyStatus;Lcom/squareup/protos/client/coupons/Coupon;Lcom/squareup/protos/client/Status;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse;

    move-result-object v0

    return-object v0
.end method

.method public coupon(Lcom/squareup/protos/client/coupons/Coupon;)Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse$Builder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    return-object p0
.end method

.method public loyalty_status(Lcom/squareup/protos/client/loyalty/LoyaltyStatus;)Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse$Builder;->loyalty_status:Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
