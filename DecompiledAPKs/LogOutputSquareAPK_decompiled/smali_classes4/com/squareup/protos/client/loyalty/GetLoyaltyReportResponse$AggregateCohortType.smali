.class public final enum Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;
.super Ljava/lang/Enum;
.source "GetLoyaltyReportResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AggregateCohortType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType$ProtoAdapter_AggregateCohortType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum COHORT_ALL:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

.field public static final enum COHORT_LOYALTY:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

.field public static final enum COHORT_NON_LOYALTY:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 265
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "COHORT_ALL"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->COHORT_ALL:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    .line 267
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    const/4 v3, 0x2

    const-string v4, "COHORT_LOYALTY"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->COHORT_LOYALTY:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    .line 269
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    const/4 v4, 0x3

    const-string v5, "COHORT_NON_LOYALTY"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->COHORT_NON_LOYALTY:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    new-array v0, v4, [Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    .line 264
    sget-object v4, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->COHORT_ALL:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->COHORT_LOYALTY:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->COHORT_NON_LOYALTY:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->$VALUES:[Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    .line 271
    new-instance v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType$ProtoAdapter_AggregateCohortType;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType$ProtoAdapter_AggregateCohortType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 275
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 276
    iput p3, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 286
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->COHORT_NON_LOYALTY:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    return-object p0

    .line 285
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->COHORT_LOYALTY:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    return-object p0

    .line 284
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->COHORT_ALL:Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;
    .locals 1

    .line 264
    const-class v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;
    .locals 1

    .line 264
    sget-object v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->$VALUES:[Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 293
    iget v0, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCohortType;->value:I

    return v0
.end method
