.class public final Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;
.super Lcom/squareup/wire/Message;
.source "RecordMissedLoyaltyOpportunityRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$ProtoAdapter_RecordMissedLoyaltyOpportunityRequest;,
        Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;,
        Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;,
        Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;",
        "Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BILL_SERVER_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_SERVER_PAYMENT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final bill_server_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final missed_loyalty_opportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final receipt_email_address:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.RecordMissedLoyaltyOpportunityRequest$Email#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final server_payment_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$ProtoAdapter_RecordMissedLoyaltyOpportunityRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$ProtoAdapter_RecordMissedLoyaltyOpportunityRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;)V
    .locals 6

    .line 68
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;Ljava/lang/String;Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;Lokio/ByteString;)V
    .locals 1

    .line 74
    sget-object v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 75
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->server_payment_token:Ljava/lang/String;

    .line 76
    iput-object p2, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->missed_loyalty_opportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    .line 77
    iput-object p3, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->bill_server_token:Ljava/lang/String;

    .line 78
    iput-object p4, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->receipt_email_address:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 95
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 96
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->server_payment_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->server_payment_token:Ljava/lang/String;

    .line 98
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->missed_loyalty_opportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->missed_loyalty_opportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    .line 99
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->bill_server_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->bill_server_token:Ljava/lang/String;

    .line 100
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->receipt_email_address:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->receipt_email_address:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    .line 101
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 106
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 108
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->server_payment_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->missed_loyalty_opportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->bill_server_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->receipt_email_address:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 113
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;
    .locals 2

    .line 83
    new-instance v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;-><init>()V

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->server_payment_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->server_payment_token:Ljava/lang/String;

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->missed_loyalty_opportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->missed_loyalty_opportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->bill_server_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->bill_server_token:Ljava/lang/String;

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->receipt_email_address:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->receipt_email_address:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->newBuilder()Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->server_payment_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", server_payment_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->server_payment_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->missed_loyalty_opportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    if-eqz v1, :cond_1

    const-string v1, ", missed_loyalty_opportunity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->missed_loyalty_opportunity:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$MissedLoyaltyOpportunity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 123
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->bill_server_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", bill_server_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->bill_server_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->receipt_email_address:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    if-eqz v1, :cond_3

    const-string v1, ", receipt_email_address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;->receipt_email_address:Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest$Email;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "RecordMissedLoyaltyOpportunityRequest{"

    .line 125
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
