.class public final Lcom/squareup/protos/client/loyalty/RewardTier$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RewardTier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/RewardTier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/RewardTier;",
        "Lcom/squareup/protos/client/loyalty/RewardTier$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public coupon_definition_token:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public points:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 123
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/RewardTier;
    .locals 5

    .line 152
    new-instance v0, Lcom/squareup/protos/client/loyalty/RewardTier;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/RewardTier$Builder;->coupon_definition_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/RewardTier$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/loyalty/RewardTier$Builder;->points:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/loyalty/RewardTier;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 116
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/RewardTier$Builder;->build()Lcom/squareup/protos/client/loyalty/RewardTier;

    move-result-object v0

    return-object v0
.end method

.method public coupon_definition_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RewardTier$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RewardTier$Builder;->coupon_definition_token:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/RewardTier$Builder;
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RewardTier$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public points(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/RewardTier$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/RewardTier$Builder;->points:Ljava/lang/Long;

    return-object p0
.end method
