.class public final enum Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;
.super Ljava/lang/Enum;
.source "DisputedPayment.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cbms/DisputedPayment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Resolution"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution$ProtoAdapter_Resolution;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

.field public static final enum ACCEPTED:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DISPUTE_REOPENED:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

.field public static final enum LOST:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

.field public static final enum NEW_DISPUTE:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

.field public static final enum PENDING_RESOLUTION:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

.field public static final enum R_UNKNOWN:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

.field public static final enum UNDER_REVIEW:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

.field public static final enum WON:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 511
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    const/4 v1, 0x0

    const-string v2, "R_UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->R_UNKNOWN:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    .line 513
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    const/4 v2, 0x1

    const-string v3, "NEW_DISPUTE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->NEW_DISPUTE:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    .line 515
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    const/4 v3, 0x2

    const-string v4, "UNDER_REVIEW"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->UNDER_REVIEW:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    .line 517
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    const/4 v4, 0x3

    const-string v5, "ACCEPTED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ACCEPTED:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    .line 519
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    const/4 v5, 0x4

    const-string v6, "PENDING_RESOLUTION"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->PENDING_RESOLUTION:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    .line 521
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    const/4 v6, 0x5

    const-string v7, "LOST"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->LOST:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    .line 523
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    const/4 v7, 0x6

    const-string v8, "WON"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->WON:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    .line 525
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    const/4 v8, 0x7

    const-string v9, "DISPUTE_REOPENED"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->DISPUTE_REOPENED:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    .line 510
    sget-object v9, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->R_UNKNOWN:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->NEW_DISPUTE:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->UNDER_REVIEW:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ACCEPTED:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->PENDING_RESOLUTION:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->LOST:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->WON:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->DISPUTE_REOPENED:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    aput-object v1, v0, v8

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->$VALUES:[Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    .line 527
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution$ProtoAdapter_Resolution;

    invoke-direct {v0}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution$ProtoAdapter_Resolution;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 531
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 532
    iput p3, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 547
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->DISPUTE_REOPENED:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    return-object p0

    .line 546
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->WON:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    return-object p0

    .line 545
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->LOST:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    return-object p0

    .line 544
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->PENDING_RESOLUTION:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    return-object p0

    .line 543
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ACCEPTED:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    return-object p0

    .line 542
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->UNDER_REVIEW:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    return-object p0

    .line 541
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->NEW_DISPUTE:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    return-object p0

    .line 540
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->R_UNKNOWN:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;
    .locals 1

    .line 510
    const-class v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;
    .locals 1

    .line 510
    sget-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->$VALUES:[Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 554
    iget v0, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->value:I

    return v0
.end method
