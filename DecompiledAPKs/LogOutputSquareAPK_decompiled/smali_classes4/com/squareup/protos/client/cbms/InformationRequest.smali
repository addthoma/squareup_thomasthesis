.class public final Lcom/squareup/protos/client/cbms/InformationRequest;
.super Lcom/squareup/wire/Message;
.source "InformationRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/cbms/InformationRequest$ProtoAdapter_InformationRequest;,
        Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;,
        Lcom/squareup/protos/client/cbms/InformationRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/cbms/InformationRequest;",
        "Lcom/squareup/protos/client/cbms/InformationRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/cbms/InformationRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACTIONABLE_STATUS:Lcom/squareup/protos/client/cbms/ActionableStatus;

.field public static final DEFAULT_DASHBOARD_FORM:Ljava/lang/Boolean;

.field public static final DEFAULT_FORM_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_FORM_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_REQUEST_TYPE:Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;

.field private static final serialVersionUID:J


# instance fields
.field public final actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cbms.ActionableStatus#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final completed_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final customer_due_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final dashboard_form:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final form_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final form_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final request_type:Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cbms.InformationRequest$RequestType#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final requested_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/cbms/InformationRequest$ProtoAdapter_InformationRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/cbms/InformationRequest$ProtoAdapter_InformationRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/cbms/InformationRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;->UNKNOWN:Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;

    sput-object v0, Lcom/squareup/protos/client/cbms/InformationRequest;->DEFAULT_REQUEST_TYPE:Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;

    .line 34
    sget-object v0, Lcom/squareup/protos/client/cbms/ActionableStatus;->UNKNOWN:Lcom/squareup/protos/client/cbms/ActionableStatus;

    sput-object v0, Lcom/squareup/protos/client/cbms/InformationRequest;->DEFAULT_ACTIONABLE_STATUS:Lcom/squareup/protos/client/cbms/ActionableStatus;

    const/4 v0, 0x0

    .line 36
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/cbms/InformationRequest;->DEFAULT_DASHBOARD_FORM:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;Lcom/squareup/protos/client/cbms/ActionableStatus;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Boolean;)V
    .locals 10

    .line 98
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/cbms/InformationRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;Lcom/squareup/protos/client/cbms/ActionableStatus;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;Lcom/squareup/protos/client/cbms/ActionableStatus;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 104
    sget-object v0, Lcom/squareup/protos/client/cbms/InformationRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->form_token:Ljava/lang/String;

    .line 106
    iput-object p2, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->form_url:Ljava/lang/String;

    .line 107
    iput-object p3, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->request_type:Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;

    .line 108
    iput-object p4, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    .line 109
    iput-object p5, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->requested_at:Lcom/squareup/protos/common/time/DateTime;

    .line 110
    iput-object p6, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->customer_due_at:Lcom/squareup/protos/common/time/DateTime;

    .line 111
    iput-object p7, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->completed_at:Lcom/squareup/protos/common/time/DateTime;

    .line 112
    iput-object p8, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->dashboard_form:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 133
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/cbms/InformationRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 134
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/cbms/InformationRequest;

    .line 135
    invoke-virtual {p0}, Lcom/squareup/protos/client/cbms/InformationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/cbms/InformationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->form_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/InformationRequest;->form_token:Ljava/lang/String;

    .line 136
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->form_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/InformationRequest;->form_url:Ljava/lang/String;

    .line 137
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->request_type:Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/InformationRequest;->request_type:Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;

    .line 138
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/InformationRequest;->actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    .line 139
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->requested_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/InformationRequest;->requested_at:Lcom/squareup/protos/common/time/DateTime;

    .line 140
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->customer_due_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/InformationRequest;->customer_due_at:Lcom/squareup/protos/common/time/DateTime;

    .line 141
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->completed_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/InformationRequest;->completed_at:Lcom/squareup/protos/common/time/DateTime;

    .line 142
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->dashboard_form:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/cbms/InformationRequest;->dashboard_form:Ljava/lang/Boolean;

    .line 143
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 148
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 150
    invoke-virtual {p0}, Lcom/squareup/protos/client/cbms/InformationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 151
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->form_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->form_url:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->request_type:Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/ActionableStatus;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->requested_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->customer_due_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->completed_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->dashboard_form:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 159
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/cbms/InformationRequest$Builder;
    .locals 2

    .line 117
    new-instance v0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;-><init>()V

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->form_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->form_token:Ljava/lang/String;

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->form_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->form_url:Ljava/lang/String;

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->request_type:Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->request_type:Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->requested_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->requested_at:Lcom/squareup/protos/common/time/DateTime;

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->customer_due_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->customer_due_at:Lcom/squareup/protos/common/time/DateTime;

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->completed_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->completed_at:Lcom/squareup/protos/common/time/DateTime;

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->dashboard_form:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->dashboard_form:Ljava/lang/Boolean;

    .line 126
    invoke-virtual {p0}, Lcom/squareup/protos/client/cbms/InformationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cbms/InformationRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/cbms/InformationRequest;->newBuilder()Lcom/squareup/protos/client/cbms/InformationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 167
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->form_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", form_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->form_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->form_url:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", form_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->form_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->request_type:Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;

    if-eqz v1, :cond_2

    const-string v1, ", request_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->request_type:Lcom/squareup/protos/client/cbms/InformationRequest$RequestType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 170
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    if-eqz v1, :cond_3

    const-string v1, ", actionable_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 171
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->requested_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_4

    const-string v1, ", requested_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->requested_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 172
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->customer_due_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_5

    const-string v1, ", customer_due_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->customer_due_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 173
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->completed_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_6

    const-string v1, ", completed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->completed_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 174
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->dashboard_form:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", dashboard_form="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/InformationRequest;->dashboard_form:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InformationRequest{"

    .line 175
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
