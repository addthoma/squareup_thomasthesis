.class public final enum Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;
.super Ljava/lang/Enum;
.source "DisputedPayment.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cbms/DisputedPayment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ProtectionState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState$ProtoAdapter_ProtectionState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum MANUAL_VIRGIL:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

.field public static final enum NOT_APPLICABLE:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

.field public static final enum NOT_PROTECTED:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

.field public static final enum PENDING_PROTECTION_DECISION:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

.field public static final enum PROTECTED:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

.field public static final enum PROTECTION_WAIVED:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

.field public static final enum PS_UNKNOWN:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

.field public static final enum VIRGIL:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

.field public static final enum VIRGIL_EXPIRED:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 570
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    const/4 v1, 0x0

    const-string v2, "PS_UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->PS_UNKNOWN:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    .line 572
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    const/4 v2, 0x1

    const-string v3, "PROTECTED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->PROTECTED:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    .line 574
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    const/4 v3, 0x2

    const-string v4, "NOT_PROTECTED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->NOT_PROTECTED:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    .line 576
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    const/4 v4, 0x3

    const-string v5, "PROTECTION_WAIVED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->PROTECTION_WAIVED:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    .line 578
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    const/4 v5, 0x4

    const-string v6, "PENDING_PROTECTION_DECISION"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->PENDING_PROTECTION_DECISION:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    .line 580
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    const/4 v6, 0x5

    const-string v7, "NOT_APPLICABLE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->NOT_APPLICABLE:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    .line 585
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    const/4 v7, 0x6

    const-string v8, "VIRGIL"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->VIRGIL:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    .line 590
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    const/4 v8, 0x7

    const-string v9, "MANUAL_VIRGIL"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->MANUAL_VIRGIL:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    .line 595
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    const/16 v9, 0x8

    const-string v10, "VIRGIL_EXPIRED"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->VIRGIL_EXPIRED:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    .line 569
    sget-object v10, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->PS_UNKNOWN:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->PROTECTED:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->NOT_PROTECTED:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->PROTECTION_WAIVED:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->PENDING_PROTECTION_DECISION:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->NOT_APPLICABLE:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->VIRGIL:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->MANUAL_VIRGIL:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->VIRGIL_EXPIRED:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->$VALUES:[Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    .line 597
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState$ProtoAdapter_ProtectionState;

    invoke-direct {v0}, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState$ProtoAdapter_ProtectionState;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 601
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 602
    iput p3, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 618
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->VIRGIL_EXPIRED:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    return-object p0

    .line 617
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->MANUAL_VIRGIL:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    return-object p0

    .line 616
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->VIRGIL:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    return-object p0

    .line 615
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->NOT_APPLICABLE:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    return-object p0

    .line 614
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->PENDING_PROTECTION_DECISION:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    return-object p0

    .line 613
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->PROTECTION_WAIVED:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    return-object p0

    .line 612
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->NOT_PROTECTED:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    return-object p0

    .line 611
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->PROTECTED:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    return-object p0

    .line 610
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->PS_UNKNOWN:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;
    .locals 1

    .line 569
    const-class v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;
    .locals 1

    .line 569
    sget-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->$VALUES:[Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 625
    iget v0, p0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->value:I

    return v0
.end method
