.class public final Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SafetyNetAttestation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/flipper/SafetyNetAttestation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/flipper/SafetyNetAttestation;",
        "Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attestation_result:Lokio/ByteString;

.field public gms_api_status:Ljava/lang/Integer;

.field public safetynet_api_status:Ljava/lang/Integer;

.field public safetynet_wrapper_status:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 153
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public attestation_result(Lokio/ByteString;)Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->attestation_result:Lokio/ByteString;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/flipper/SafetyNetAttestation;
    .locals 7

    .line 190
    new-instance v6, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->attestation_result:Lokio/ByteString;

    iget-object v2, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->safetynet_wrapper_status:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    iget-object v3, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->safetynet_api_status:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->gms_api_status:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;-><init>(Lokio/ByteString;Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 144
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->build()Lcom/squareup/protos/client/flipper/SafetyNetAttestation;

    move-result-object v0

    return-object v0
.end method

.method public gms_api_status(Ljava/lang/Integer;)Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->gms_api_status:Ljava/lang/Integer;

    return-object p0
.end method

.method public safetynet_api_status(Ljava/lang/Integer;)Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->safetynet_api_status:Ljava/lang/Integer;

    return-object p0
.end method

.method public safetynet_wrapper_status(Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;)Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->safetynet_wrapper_status:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    return-object p0
.end method
