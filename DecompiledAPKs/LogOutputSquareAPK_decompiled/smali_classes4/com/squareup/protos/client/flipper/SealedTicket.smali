.class public final Lcom/squareup/protos/client/flipper/SealedTicket;
.super Lcom/squareup/wire/Message;
.source "SealedTicket.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/flipper/SealedTicket$ProtoAdapter_SealedTicket;,
        Lcom/squareup/protos/client/flipper/SealedTicket$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/flipper/SealedTicket;",
        "Lcom/squareup/protos/client/flipper/SealedTicket$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/flipper/SealedTicket;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CIPHERTEXT:Lokio/ByteString;

.field public static final DEFAULT_CREATION:Ljava/lang/Long;

.field public static final DEFAULT_EXPIRATION:Ljava/lang/Long;

.field public static final DEFAULT_KEY_INDEX:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final ciphertext:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x2
    .end annotation
.end field

.field public final creation:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x3
    .end annotation
.end field

.field public final expiration:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x1
    .end annotation
.end field

.field public final key_index:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 31
    new-instance v0, Lcom/squareup/protos/client/flipper/SealedTicket$ProtoAdapter_SealedTicket;

    invoke-direct {v0}, Lcom/squareup/protos/client/flipper/SealedTicket$ProtoAdapter_SealedTicket;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/flipper/SealedTicket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 35
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/flipper/SealedTicket;->DEFAULT_EXPIRATION:Ljava/lang/Long;

    .line 37
    sget-object v1, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v1, Lcom/squareup/protos/client/flipper/SealedTicket;->DEFAULT_CIPHERTEXT:Lokio/ByteString;

    .line 39
    sput-object v0, Lcom/squareup/protos/client/flipper/SealedTicket;->DEFAULT_CREATION:Ljava/lang/Long;

    .line 41
    sput-object v0, Lcom/squareup/protos/client/flipper/SealedTicket;->DEFAULT_KEY_INDEX:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Lokio/ByteString;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 6

    .line 87
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/flipper/SealedTicket;-><init>(Ljava/lang/Long;Lokio/ByteString;Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Lokio/ByteString;Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1

    .line 92
    sget-object v0, Lcom/squareup/protos/client/flipper/SealedTicket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 93
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->expiration:Ljava/lang/Long;

    .line 94
    iput-object p2, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->ciphertext:Lokio/ByteString;

    .line 95
    iput-object p3, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->creation:Ljava/lang/Long;

    .line 96
    iput-object p4, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->key_index:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 113
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/flipper/SealedTicket;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 114
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/flipper/SealedTicket;

    .line 115
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/SealedTicket;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/flipper/SealedTicket;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->expiration:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/flipper/SealedTicket;->expiration:Ljava/lang/Long;

    .line 116
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->ciphertext:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/flipper/SealedTicket;->ciphertext:Lokio/ByteString;

    .line 117
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->creation:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/flipper/SealedTicket;->creation:Ljava/lang/Long;

    .line 118
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->key_index:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/protos/client/flipper/SealedTicket;->key_index:Ljava/lang/Long;

    .line 119
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 124
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 126
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/SealedTicket;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->expiration:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->ciphertext:Lokio/ByteString;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->creation:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->key_index:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 131
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/flipper/SealedTicket$Builder;
    .locals 2

    .line 101
    new-instance v0, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;-><init>()V

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->expiration:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->expiration:Ljava/lang/Long;

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->ciphertext:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->ciphertext:Lokio/ByteString;

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->creation:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->creation:Ljava/lang/Long;

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->key_index:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->key_index:Ljava/lang/Long;

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/SealedTicket;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/flipper/SealedTicket$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/SealedTicket;->newBuilder()Lcom/squareup/protos/client/flipper/SealedTicket$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->expiration:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", expiration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->expiration:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 140
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->ciphertext:Lokio/ByteString;

    if-eqz v1, :cond_1

    const-string v1, ", ciphertext="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->ciphertext:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 141
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->creation:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", creation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->creation:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 142
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->key_index:Ljava/lang/Long;

    if-eqz v1, :cond_3

    const-string v1, ", key_index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SealedTicket;->key_index:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SealedTicket{"

    .line 143
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
