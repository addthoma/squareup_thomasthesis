.class public final Lcom/squareup/protos/client/flipper/SafetyNetAttestation;
.super Lcom/squareup/wire/Message;
.source "SafetyNetAttestation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/flipper/SafetyNetAttestation$ProtoAdapter_SafetyNetAttestation;,
        Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;,
        Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/flipper/SafetyNetAttestation;",
        "Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/flipper/SafetyNetAttestation;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ATTESTATION_RESULT:Lokio/ByteString;

.field public static final DEFAULT_GMS_API_STATUS:Ljava/lang/Integer;

.field public static final DEFAULT_SAFETYNET_API_STATUS:Ljava/lang/Integer;

.field public static final DEFAULT_SAFETYNET_WRAPPER_STATUS:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

.field private static final serialVersionUID:J


# instance fields
.field public final attestation_result:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x1
    .end annotation
.end field

.field public final gms_api_status:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field

.field public final safetynet_api_status:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field

.field public final safetynet_wrapper_status:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.flipper.SafetyNetAttestation$SafetyNetWrapperStatusCode#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$ProtoAdapter_SafetyNetAttestation;

    invoke-direct {v0}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$ProtoAdapter_SafetyNetAttestation;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 37
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->DEFAULT_ATTESTATION_RESULT:Lokio/ByteString;

    .line 39
    sget-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->INIT:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->DEFAULT_SAFETYNET_WRAPPER_STATUS:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    const/4 v0, 0x0

    .line 41
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->DEFAULT_SAFETYNET_API_STATUS:Ljava/lang/Integer;

    .line 43
    sput-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->DEFAULT_GMS_API_STATUS:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 6

    .line 84
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;-><init>(Lokio/ByteString;Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 90
    sget-object v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 91
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->attestation_result:Lokio/ByteString;

    .line 92
    iput-object p2, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->safetynet_wrapper_status:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 93
    iput-object p3, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->safetynet_api_status:Ljava/lang/Integer;

    .line 94
    iput-object p4, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->gms_api_status:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 111
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 112
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;

    .line 113
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->attestation_result:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->attestation_result:Lokio/ByteString;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->safetynet_wrapper_status:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    iget-object v3, p1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->safetynet_wrapper_status:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 115
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->safetynet_api_status:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->safetynet_api_status:Ljava/lang/Integer;

    .line 116
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->gms_api_status:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->gms_api_status:Ljava/lang/Integer;

    .line 117
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 122
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->attestation_result:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->safetynet_wrapper_status:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->safetynet_api_status:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->gms_api_status:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 129
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;
    .locals 2

    .line 99
    new-instance v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;-><init>()V

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->attestation_result:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->attestation_result:Lokio/ByteString;

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->safetynet_wrapper_status:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    iput-object v1, v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->safetynet_wrapper_status:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->safetynet_api_status:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->safetynet_api_status:Ljava/lang/Integer;

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->gms_api_status:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->gms_api_status:Ljava/lang/Integer;

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->newBuilder()Lcom/squareup/protos/client/flipper/SafetyNetAttestation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->attestation_result:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", attestation_result="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->attestation_result:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 138
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->safetynet_wrapper_status:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    if-eqz v1, :cond_1

    const-string v1, ", safetynet_wrapper_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->safetynet_wrapper_status:Lcom/squareup/protos/client/flipper/SafetyNetAttestation$SafetyNetWrapperStatusCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 139
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->safetynet_api_status:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", safetynet_api_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->safetynet_api_status:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 140
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->gms_api_status:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", gms_api_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/SafetyNetAttestation;->gms_api_status:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SafetyNetAttestation{"

    .line 141
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
