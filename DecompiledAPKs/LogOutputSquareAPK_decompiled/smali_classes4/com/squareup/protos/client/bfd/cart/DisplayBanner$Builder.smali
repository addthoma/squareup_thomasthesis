.class public final Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DisplayBanner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bfd/cart/DisplayBanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bfd/cart/DisplayBanner;",
        "Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public item_client_id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public price:Ljava/lang/String;

.field public quantity:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 126
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bfd/cart/DisplayBanner;
    .locals 7

    .line 151
    new-instance v6, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;->price:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;->item_client_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;->quantity:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bfd/cart/DisplayBanner;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;->build()Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    move-result-object v0

    return-object v0
.end method

.method public item_client_id(Ljava/lang/String;)Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;->item_client_id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public price(Ljava/lang/String;)Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;->price:Ljava/lang/String;

    return-object p0
.end method

.method public quantity(Ljava/lang/Integer;)Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayBanner$Builder;->quantity:Ljava/lang/Integer;

    return-object p0
.end method
