.class public final Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InventoryProductIdentifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;",
        "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public token:Ljava/lang/String;

.field public type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 101
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;
    .locals 4

    .line 122
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier$Builder;->type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier;

    move-result-object v0

    return-object v0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier$Builder;->token:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductIdentifier$Builder;->type:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    return-object p0
.end method
