.class public final Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;",
        "Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public object:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;",
            ">;"
        }
    .end annotation
.end field

.field public server_version:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 99
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 100
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;->object:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;
    .locals 4

    .line 120
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;->object:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;->server_version:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;-><init>(Ljava/util/List;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;->build()Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse;

    move-result-object v0

    return-object v0
.end method

.method public object(Ljava/util/List;)Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryObject;",
            ">;)",
            "Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;"
        }
    .end annotation

    .line 108
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 109
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;->object:Ljava/util/List;

    return-object p0
.end method

.method public server_version(Ljava/lang/Long;)Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/GetResponse$Builder;->server_version:Ljava/lang/Long;

    return-object p0
.end method
