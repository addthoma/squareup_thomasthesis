.class public final Lcom/squareup/protos/client/orders/FulfillmentAction;
.super Lcom/squareup/wire/Message;
.source "FulfillmentAction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/orders/FulfillmentAction$ProtoAdapter_FulfillmentAction;,
        Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/orders/FulfillmentAction;",
        "Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/orders/FulfillmentAction;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FULFILLMENT_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final accept:Lcom/squareup/protos/client/orders/AcceptAction;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.AcceptAction#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final cancel:Lcom/squareup/protos/client/orders/CancelAction;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.CancelAction#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final complete:Lcom/squareup/protos/client/orders/CompleteAction;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.CompleteAction#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final fulfillment_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final line_items:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.LineItemQuantity#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/LineItemQuantity;",
            ">;"
        }
    .end annotation
.end field

.field public final mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.MarkInProgressAction#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.MarkPickedUpAction#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.MarkReadyAction#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.MarkShippedAction#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/orders/FulfillmentAction$ProtoAdapter_FulfillmentAction;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/FulfillmentAction$ProtoAdapter_FulfillmentAction;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/orders/FulfillmentAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/orders/AcceptAction;Lcom/squareup/protos/client/orders/MarkPickedUpAction;Lcom/squareup/protos/client/orders/MarkReadyAction;Lcom/squareup/protos/client/orders/CancelAction;Lcom/squareup/protos/client/orders/MarkShippedAction;Lcom/squareup/protos/client/orders/MarkInProgressAction;Lcom/squareup/protos/client/orders/CompleteAction;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/LineItemQuantity;",
            ">;",
            "Lcom/squareup/protos/client/orders/AcceptAction;",
            "Lcom/squareup/protos/client/orders/MarkPickedUpAction;",
            "Lcom/squareup/protos/client/orders/MarkReadyAction;",
            "Lcom/squareup/protos/client/orders/CancelAction;",
            "Lcom/squareup/protos/client/orders/MarkShippedAction;",
            "Lcom/squareup/protos/client/orders/MarkInProgressAction;",
            "Lcom/squareup/protos/client/orders/CompleteAction;",
            ")V"
        }
    .end annotation

    .line 90
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/orders/FulfillmentAction;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/orders/AcceptAction;Lcom/squareup/protos/client/orders/MarkPickedUpAction;Lcom/squareup/protos/client/orders/MarkReadyAction;Lcom/squareup/protos/client/orders/CancelAction;Lcom/squareup/protos/client/orders/MarkShippedAction;Lcom/squareup/protos/client/orders/MarkInProgressAction;Lcom/squareup/protos/client/orders/CompleteAction;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/orders/AcceptAction;Lcom/squareup/protos/client/orders/MarkPickedUpAction;Lcom/squareup/protos/client/orders/MarkReadyAction;Lcom/squareup/protos/client/orders/CancelAction;Lcom/squareup/protos/client/orders/MarkShippedAction;Lcom/squareup/protos/client/orders/MarkInProgressAction;Lcom/squareup/protos/client/orders/CompleteAction;Lokio/ByteString;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/LineItemQuantity;",
            ">;",
            "Lcom/squareup/protos/client/orders/AcceptAction;",
            "Lcom/squareup/protos/client/orders/MarkPickedUpAction;",
            "Lcom/squareup/protos/client/orders/MarkReadyAction;",
            "Lcom/squareup/protos/client/orders/CancelAction;",
            "Lcom/squareup/protos/client/orders/MarkShippedAction;",
            "Lcom/squareup/protos/client/orders/MarkInProgressAction;",
            "Lcom/squareup/protos/client/orders/CompleteAction;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 97
    sget-object v0, Lcom/squareup/protos/client/orders/FulfillmentAction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const/4 p10, 0x3

    new-array p10, p10, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p7, p10, v0

    const/4 v0, 0x1

    aput-object p8, p10, v0

    const/4 v1, 0x2

    aput-object p9, p10, v1

    .line 98
    invoke-static {p3, p4, p5, p6, p10}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)I

    move-result p10

    if-gt p10, v0, :cond_0

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->fulfillment_id:Ljava/lang/String;

    const-string p1, "line_items"

    .line 102
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->line_items:Ljava/util/List;

    .line 103
    iput-object p3, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    .line 104
    iput-object p4, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    .line 105
    iput-object p5, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    .line 106
    iput-object p6, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    .line 107
    iput-object p7, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    .line 108
    iput-object p8, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    .line 109
    iput-object p9, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    return-void

    .line 99
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of accept, mark_picked_up, mark_ready, cancel, mark_shipped, mark_in_progress, complete may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 131
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 132
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/orders/FulfillmentAction;

    .line 133
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/FulfillmentAction;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/FulfillmentAction;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->fulfillment_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;->fulfillment_id:Ljava/lang/String;

    .line 134
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->line_items:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;->line_items:Ljava/util/List;

    .line 135
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    .line 136
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    .line 137
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    .line 138
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    .line 139
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    .line 140
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    .line 141
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    iget-object p1, p1, Lcom/squareup/protos/client/orders/FulfillmentAction;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    .line 142
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 147
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 149
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/FulfillmentAction;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->fulfillment_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 151
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->line_items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/AcceptAction;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/MarkPickedUpAction;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/MarkReadyAction;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/CancelAction;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/MarkShippedAction;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/MarkInProgressAction;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/CompleteAction;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 159
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;
    .locals 2

    .line 114
    new-instance v0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;-><init>()V

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->fulfillment_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->fulfillment_id:Ljava/lang/String;

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->line_items:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->line_items:Ljava/util/List;

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/FulfillmentAction;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/FulfillmentAction;->newBuilder()Lcom/squareup/protos/client/orders/FulfillmentAction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 167
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->fulfillment_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", fulfillment_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->fulfillment_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->line_items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", line_items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->line_items:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 169
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    if-eqz v1, :cond_2

    const-string v1, ", accept="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->accept:Lcom/squareup/protos/client/orders/AcceptAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 170
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    if-eqz v1, :cond_3

    const-string v1, ", mark_picked_up="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_picked_up:Lcom/squareup/protos/client/orders/MarkPickedUpAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 171
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    if-eqz v1, :cond_4

    const-string v1, ", mark_ready="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_ready:Lcom/squareup/protos/client/orders/MarkReadyAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 172
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    if-eqz v1, :cond_5

    const-string v1, ", cancel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->cancel:Lcom/squareup/protos/client/orders/CancelAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 173
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    if-eqz v1, :cond_6

    const-string v1, ", mark_shipped="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_shipped:Lcom/squareup/protos/client/orders/MarkShippedAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 174
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    if-eqz v1, :cond_7

    const-string v1, ", mark_in_progress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->mark_in_progress:Lcom/squareup/protos/client/orders/MarkInProgressAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 175
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    if-eqz v1, :cond_8

    const-string v1, ", complete="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentAction;->complete:Lcom/squareup/protos/client/orders/CompleteAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FulfillmentAction{"

    .line 176
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
