.class public final Lcom/squareup/protos/client/IdPair$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "IdPair.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/IdPair;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/IdPair;",
        "Lcom/squareup/protos/client/IdPair$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public client_id:Ljava/lang/String;

.field public server_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 115
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/IdPair;
    .locals 4

    .line 140
    new-instance v0, Lcom/squareup/protos/client/IdPair;

    iget-object v1, p0, Lcom/squareup/protos/client/IdPair$Builder;->server_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/IdPair$Builder;->client_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 110
    invoke-virtual {p0}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    return-object v0
.end method

.method public client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/client/IdPair$Builder;->client_id:Ljava/lang/String;

    return-object p0
.end method

.method public server_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/client/IdPair$Builder;->server_id:Ljava/lang/String;

    return-object p0
.end method
