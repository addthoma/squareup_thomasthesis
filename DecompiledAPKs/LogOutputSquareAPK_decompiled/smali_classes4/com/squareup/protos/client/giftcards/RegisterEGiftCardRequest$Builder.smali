.class public final Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RegisterEGiftCardRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;",
        "Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public client_request_uuid:Ljava/lang/String;

.field public delivery_date:Lcom/squareup/protos/common/time/DateTime;

.field public egift_theme_token:Ljava/lang/String;

.field public itemization_id_pair:Lcom/squareup/protos/client/IdPair;

.field public recipient_email:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 156
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;
    .locals 8

    .line 201
    new-instance v7, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->delivery_date:Lcom/squareup/protos/common/time/DateTime;

    iget-object v2, p0, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->recipient_email:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->egift_theme_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->client_request_uuid:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;-><init>(Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 145
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;

    move-result-object v0

    return-object v0
.end method

.method public client_request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->client_request_uuid:Ljava/lang/String;

    return-object p0
.end method

.method public delivery_date(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->delivery_date:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public egift_theme_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->egift_theme_token:Ljava/lang/String;

    return-object p0
.end method

.method public itemization_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;
    .locals 0

    .line 195
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public recipient_email(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->recipient_email:Ljava/lang/String;

    return-object p0
.end method
