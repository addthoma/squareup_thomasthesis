.class public final Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RegisterGiftCardRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;",
        "Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public gift_card_data:Lcom/squareup/protos/client/bills/CardData;

.field public itemization_id_pair:Lcom/squareup/protos/client/IdPair;

.field public merchant_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 128
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;
    .locals 7

    .line 156
    new-instance v6, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;->gift_card_data:Lcom/squareup/protos/client/bills/CardData;

    iget-object v4, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 119
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;

    move-result-object v0

    return-object v0
.end method

.method public entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0
.end method

.method public gift_card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;->gift_card_data:Lcom/squareup/protos/client/bills/CardData;

    return-object p0
.end method

.method public itemization_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method
