.class public final Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;
.super Lcom/squareup/wire/Message;
.source "RegisterGiftCardRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$ProtoAdapter_RegisterGiftCardRequest;,
        Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;",
        "Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Card$EntryMethod#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final gift_card_data:Lcom/squareup/protos/client/bills/CardData;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final itemization_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$ProtoAdapter_RegisterGiftCardRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$ProtoAdapter_RegisterGiftCardRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 29
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->UNKNOWN_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sput-object v0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V
    .locals 6

    .line 60
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lokio/ByteString;)V
    .locals 1

    .line 65
    sget-object v0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 66
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->merchant_token:Ljava/lang/String;

    .line 67
    iput-object p2, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 68
    iput-object p3, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->gift_card_data:Lcom/squareup/protos/client/bills/CardData;

    .line 69
    iput-object p4, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 86
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 87
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->merchant_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->merchant_token:Ljava/lang/String;

    .line 89
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 90
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->gift_card_data:Lcom/squareup/protos/client/bills/CardData;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->gift_card_data:Lcom/squareup/protos/client/bills/CardData;

    .line 91
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 92
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 97
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->gift_card_data:Lcom/squareup/protos/client/bills/CardData;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 104
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;
    .locals 2

    .line 74
    new-instance v0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;-><init>()V

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;->merchant_token:Ljava/lang/String;

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->gift_card_data:Lcom/squareup/protos/client/bills/CardData;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;->gift_card_data:Lcom/squareup/protos/client/bills/CardData;

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 79
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->newBuilder()Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_1

    const-string v1, ", itemization_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 114
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->gift_card_data:Lcom/squareup/protos/client/bills/CardData;

    if-eqz v1, :cond_2

    const-string v1, ", gift_card_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->gift_card_data:Lcom/squareup/protos/client/bills/CardData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 115
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-eqz v1, :cond_3

    const-string v1, ", entry_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "RegisterGiftCardRequest{"

    .line 116
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
