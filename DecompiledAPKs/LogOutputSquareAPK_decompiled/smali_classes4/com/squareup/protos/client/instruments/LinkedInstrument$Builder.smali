.class public final Lcom/squareup/protos/client/instruments/LinkedInstrument$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LinkedInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/instruments/LinkedInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/instruments/LinkedInstrument;",
        "Lcom/squareup/protos/client/instruments/LinkedInstrument$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card_data:Lcom/squareup/protos/client/bills/CardData;

.field public tender_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 102
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/instruments/LinkedInstrument;
    .locals 4

    .line 125
    new-instance v0, Lcom/squareup/protos/client/instruments/LinkedInstrument;

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/LinkedInstrument$Builder;->tender_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/instruments/LinkedInstrument$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/instruments/LinkedInstrument;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/CardData;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/client/instruments/LinkedInstrument$Builder;->build()Lcom/squareup/protos/client/instruments/LinkedInstrument;

    move-result-object v0

    return-object v0
.end method

.method public card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/instruments/LinkedInstrument$Builder;
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/LinkedInstrument$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    const/4 p1, 0x0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/LinkedInstrument$Builder;->tender_token:Ljava/lang/String;

    return-object p0
.end method

.method public tender_token(Ljava/lang/String;)Lcom/squareup/protos/client/instruments/LinkedInstrument$Builder;
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/LinkedInstrument$Builder;->tender_token:Ljava/lang/String;

    const/4 p1, 0x0

    .line 110
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/LinkedInstrument$Builder;->card_data:Lcom/squareup/protos/client/bills/CardData;

    return-object p0
.end method
