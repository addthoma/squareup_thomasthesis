.class final Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$ProtoAdapter_VerifyAndLinkInstrumentRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "VerifyAndLinkInstrumentRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_VerifyAndLinkInstrumentRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 260
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 290
    new-instance v0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;-><init>()V

    .line 291
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 292
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 309
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 307
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;

    goto :goto_0

    .line 306
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;

    goto :goto_0

    .line 305
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/instruments/LinkedInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/instruments/LinkedInstrument;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->linked_instrument(Lcom/squareup/protos/client/instruments/LinkedInstrument;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;

    goto :goto_0

    .line 299
    :pswitch_3
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 301
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 296
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/instruments/ValidationInformation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/instruments/ValidationInformation;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->validation_information(Lcom/squareup/protos/client/instruments/ValidationInformation;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;

    goto :goto_0

    .line 295
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->cardholder_name(Ljava/lang/String;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;

    goto :goto_0

    .line 294
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->unique_key(Ljava/lang/String;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;

    goto :goto_0

    .line 313
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 314
    invoke-virtual {v0}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->build()Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 258
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$ProtoAdapter_VerifyAndLinkInstrumentRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 278
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->unique_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 279
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->cardholder_name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 280
    sget-object v0, Lcom/squareup/protos/client/instruments/LinkedInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->linked_instrument:Lcom/squareup/protos/client/instruments/LinkedInstrument;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 281
    sget-object v0, Lcom/squareup/protos/client/instruments/ValidationInformation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->validation_information:Lcom/squareup/protos/client/instruments/ValidationInformation;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 282
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 283
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->contact_token:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 284
    sget-object v0, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 285
    invoke-virtual {p2}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 258
    check-cast p2, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$ProtoAdapter_VerifyAndLinkInstrumentRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;)I
    .locals 4

    .line 265
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->unique_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->cardholder_name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 266
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/instruments/LinkedInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->linked_instrument:Lcom/squareup/protos/client/instruments/LinkedInstrument;

    const/4 v3, 0x7

    .line 267
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/instruments/ValidationInformation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->validation_information:Lcom/squareup/protos/client/instruments/ValidationInformation;

    const/4 v3, 0x5

    .line 268
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v3, 0x6

    .line 269
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->contact_token:Ljava/lang/String;

    const/16 v3, 0x8

    .line 270
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    const/16 v3, 0x9

    .line 271
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 272
    invoke-virtual {p1}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 258
    check-cast p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$ProtoAdapter_VerifyAndLinkInstrumentRequest;->encodedSize(Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;
    .locals 2

    .line 319
    invoke-virtual {p1}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;->newBuilder()Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 320
    iput-object v0, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->cardholder_name:Ljava/lang/String;

    .line 321
    iget-object v0, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->linked_instrument:Lcom/squareup/protos/client/instruments/LinkedInstrument;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/instruments/LinkedInstrument;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->linked_instrument:Lcom/squareup/protos/client/instruments/LinkedInstrument;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/instruments/LinkedInstrument;

    iput-object v0, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->linked_instrument:Lcom/squareup/protos/client/instruments/LinkedInstrument;

    .line 322
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->validation_information:Lcom/squareup/protos/client/instruments/ValidationInformation;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/instruments/ValidationInformation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->validation_information:Lcom/squareup/protos/client/instruments/ValidationInformation;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/instruments/ValidationInformation;

    iput-object v0, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->validation_information:Lcom/squareup/protos/client/instruments/ValidationInformation;

    .line 323
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/CreatorDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/CreatorDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 324
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 325
    invoke-virtual {p1}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->build()Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 258
    check-cast p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$ProtoAdapter_VerifyAndLinkInstrumentRequest;->redact(Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;

    move-result-object p1

    return-object p1
.end method
