.class public final enum Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;
.super Ljava/lang/Enum;
.source "CardActivityEvent.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/CardActivityEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TransactionState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState$ProtoAdapter_TransactionState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DECLINED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

.field public static final enum DISPUTED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

.field public static final enum DISPUTE_REVERSED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

.field public static final enum DISPUTE_WON:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

.field public static final enum IN_REVERSAL:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

.field public static final enum IN_REVIEW:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

.field public static final enum PENDING:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

.field public static final enum REFUNDED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

.field public static final enum REVERSED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

.field public static final enum SETTLED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

.field public static final enum VOIDED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

.field public static final enum WAITING_ON_EXTERNAL:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 373
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "PENDING"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->PENDING:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    .line 378
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    const/4 v3, 0x2

    const-string v4, "SETTLED"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->SETTLED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    .line 384
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    const/4 v4, 0x3

    const-string v5, "VOIDED"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->VOIDED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    .line 389
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    const/4 v5, 0x4

    const-string v6, "DECLINED"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->DECLINED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    .line 394
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    const/4 v6, 0x5

    const-string v7, "REFUNDED"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->REFUNDED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    .line 399
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    const/4 v7, 0x6

    const-string v8, "REVERSED"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->REVERSED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    .line 404
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    const/4 v8, 0x7

    const-string v9, "DISPUTED"

    invoke-direct {v0, v9, v7, v8}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->DISPUTED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    .line 411
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    const/16 v9, 0x8

    const-string v10, "DISPUTE_REVERSED"

    invoke-direct {v0, v10, v8, v9}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->DISPUTE_REVERSED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    .line 416
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    const/16 v10, 0x9

    const-string v11, "DISPUTE_WON"

    invoke-direct {v0, v11, v9, v10}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->DISPUTE_WON:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    .line 422
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    const/16 v11, 0xa

    const-string v12, "IN_REVIEW"

    invoke-direct {v0, v12, v10, v11}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->IN_REVIEW:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    .line 428
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    const/16 v12, 0xb

    const-string v13, "IN_REVERSAL"

    invoke-direct {v0, v13, v11, v12}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->IN_REVERSAL:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    .line 435
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    const/16 v13, 0xc

    const-string v14, "WAITING_ON_EXTERNAL"

    invoke-direct {v0, v14, v12, v13}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->WAITING_ON_EXTERNAL:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    new-array v0, v13, [Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    .line 368
    sget-object v13, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->PENDING:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->SETTLED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->VOIDED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->DECLINED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->REFUNDED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->REVERSED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->DISPUTED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->DISPUTE_REVERSED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->DISPUTE_WON:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->IN_REVIEW:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->IN_REVERSAL:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->WAITING_ON_EXTERNAL:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    aput-object v1, v0, v12

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->$VALUES:[Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    .line 437
    new-instance v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState$ProtoAdapter_TransactionState;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState$ProtoAdapter_TransactionState;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 441
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 442
    iput p3, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 461
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->WAITING_ON_EXTERNAL:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    return-object p0

    .line 460
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->IN_REVERSAL:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    return-object p0

    .line 459
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->IN_REVIEW:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    return-object p0

    .line 458
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->DISPUTE_WON:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    return-object p0

    .line 457
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->DISPUTE_REVERSED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    return-object p0

    .line 456
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->DISPUTED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    return-object p0

    .line 455
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->REVERSED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    return-object p0

    .line 454
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->REFUNDED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    return-object p0

    .line 453
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->DECLINED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    return-object p0

    .line 452
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->VOIDED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    return-object p0

    .line 451
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->SETTLED:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    return-object p0

    .line 450
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->PENDING:Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;
    .locals 1

    .line 368
    const-class v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;
    .locals 1

    .line 368
    sget-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->$VALUES:[Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 468
    iget v0, p0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->value:I

    return v0
.end method
