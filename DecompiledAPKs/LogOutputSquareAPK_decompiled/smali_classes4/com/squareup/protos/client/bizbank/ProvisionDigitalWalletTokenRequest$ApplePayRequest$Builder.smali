.class public final Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ProvisionDigitalWalletTokenRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public certificates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lokio/ByteString;",
            ">;"
        }
    .end annotation
.end field

.field public nonce:Lokio/ByteString;

.field public nonce_signature:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 314
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 315
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;->certificates:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;
    .locals 5

    .line 345
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;->certificates:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;->nonce:Lokio/ByteString;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;->nonce_signature:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;-><init>(Ljava/util/List;Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 307
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;

    move-result-object v0

    return-object v0
.end method

.method public certificates(Ljava/util/List;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lokio/ByteString;",
            ">;)",
            "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;"
        }
    .end annotation

    .line 322
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 323
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;->certificates:Ljava/util/List;

    return-object p0
.end method

.method public nonce(Lokio/ByteString;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;
    .locals 0

    .line 331
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;->nonce:Lokio/ByteString;

    return-object p0
.end method

.method public nonce_signature(Lokio/ByteString;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;
    .locals 0

    .line 339
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest$Builder;->nonce_signature:Lokio/ByteString;

    return-object p0
.end method
