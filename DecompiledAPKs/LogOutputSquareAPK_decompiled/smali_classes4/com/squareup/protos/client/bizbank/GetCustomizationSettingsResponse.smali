.class public final Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;
.super Lcom/squareup/wire/Message;
.source "GetCustomizationSettingsResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$ProtoAdapter_GetCustomizationSettingsResponse;,
        Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;",
        "Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BUSINESS_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_CAN_RETRY_IDV:Ljava/lang/Boolean;

.field public static final DEFAULT_IDV_NEEDS_SSN:Ljava/lang/Boolean;

.field public static final DEFAULT_IDV_RETRIES_REMAINING:Ljava/lang/Integer;

.field public static final DEFAULT_IDV_STATE:Lcom/squareup/protos/client/bizbank/IdvState;

.field public static final DEFAULT_MAX_INK_COVERAGE:Ljava/lang/Float;

.field public static final DEFAULT_MIN_INK_COVERAGE:Ljava/lang/Float;

.field public static final DEFAULT_OWNER_NAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final business_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final can_retry_idv:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final idv_needs_ssn:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final idv_retries_remaining:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xb
    .end annotation
.end field

.field public final idv_state:Lcom/squareup/protos/client/bizbank/IdvState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.IdvState#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final max_ink_coverage:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#FLOAT"
        tag = 0x4
    .end annotation
.end field

.field public final min_ink_coverage:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#FLOAT"
        tag = 0x3
    .end annotation
.end field

.field public final owner_address:Lcom/squareup/protos/common/location/GlobalAddress;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.location.GlobalAddress#ADAPTER"
        redacted = true
        tag = 0x9
    .end annotation
.end field

.field public final owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.YearMonthDay#ADAPTER"
        redacted = true
        tag = 0xa
    .end annotation
.end field

.field public final owner_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 26
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$ProtoAdapter_GetCustomizationSettingsResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$ProtoAdapter_GetCustomizationSettingsResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 34
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->DEFAULT_MIN_INK_COVERAGE:Ljava/lang/Float;

    .line 36
    sput-object v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->DEFAULT_MAX_INK_COVERAGE:Ljava/lang/Float;

    .line 38
    sget-object v0, Lcom/squareup/protos/client/bizbank/IdvState;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/IdvState;

    sput-object v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->DEFAULT_IDV_STATE:Lcom/squareup/protos/client/bizbank/IdvState;

    const/4 v0, 0x0

    .line 40
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->DEFAULT_IDV_NEEDS_SSN:Ljava/lang/Boolean;

    .line 42
    sput-object v1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->DEFAULT_CAN_RETRY_IDV:Ljava/lang/Boolean;

    .line 44
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->DEFAULT_IDV_RETRIES_REMAINING:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/bizbank/IdvState;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/protos/common/time/YearMonthDay;)V
    .locals 13

    .line 130
    sget-object v12, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/bizbank/IdvState;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/protos/common/time/YearMonthDay;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/Float;Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/bizbank/IdvState;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/protos/common/time/YearMonthDay;Lokio/ByteString;)V
    .locals 1

    .line 137
    sget-object v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p12}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_name:Ljava/lang/String;

    .line 139
    iput-object p2, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->business_name:Ljava/lang/String;

    .line 140
    iput-object p3, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->min_ink_coverage:Ljava/lang/Float;

    .line 141
    iput-object p4, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->max_ink_coverage:Ljava/lang/Float;

    .line 142
    iput-object p5, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->status:Lcom/squareup/protos/client/Status;

    .line 143
    iput-object p6, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    .line 144
    iput-object p7, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_needs_ssn:Ljava/lang/Boolean;

    .line 145
    iput-object p8, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->can_retry_idv:Ljava/lang/Boolean;

    .line 146
    iput-object p9, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_retries_remaining:Ljava/lang/Integer;

    .line 147
    iput-object p10, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 148
    iput-object p11, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 172
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 173
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;

    .line 174
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_name:Ljava/lang/String;

    .line 175
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->business_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->business_name:Ljava/lang/String;

    .line 176
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->min_ink_coverage:Ljava/lang/Float;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->min_ink_coverage:Ljava/lang/Float;

    .line 177
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->max_ink_coverage:Ljava/lang/Float;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->max_ink_coverage:Ljava/lang/Float;

    .line 178
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->status:Lcom/squareup/protos/client/Status;

    .line 179
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    .line 180
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_needs_ssn:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_needs_ssn:Ljava/lang/Boolean;

    .line 181
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->can_retry_idv:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->can_retry_idv:Ljava/lang/Boolean;

    .line 182
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_retries_remaining:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_retries_remaining:Ljava/lang/Integer;

    .line 183
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 184
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 185
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 190
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_b

    .line 192
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->business_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->min_ink_coverage:Ljava/lang/Float;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Float;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->max_ink_coverage:Ljava/lang/Float;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Float;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/IdvState;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_needs_ssn:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 200
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->can_retry_idv:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_retries_remaining:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 202
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/common/location/GlobalAddress;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/YearMonthDay;->hashCode()I

    move-result v2

    :cond_a
    add-int/2addr v0, v2

    .line 204
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_b
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;
    .locals 2

    .line 153
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;-><init>()V

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->owner_name:Ljava/lang/String;

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->business_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->business_name:Ljava/lang/String;

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->min_ink_coverage:Ljava/lang/Float;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->min_ink_coverage:Ljava/lang/Float;

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->max_ink_coverage:Ljava/lang/Float;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->max_ink_coverage:Ljava/lang/Float;

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    .line 160
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_needs_ssn:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->idv_needs_ssn:Ljava/lang/Boolean;

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->can_retry_idv:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->can_retry_idv:Ljava/lang/Boolean;

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_retries_remaining:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->idv_retries_remaining:Ljava/lang/Integer;

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 164
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 165
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->newBuilder()Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 211
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", owner_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->business_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", business_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->min_ink_coverage:Ljava/lang/Float;

    if-eqz v1, :cond_2

    const-string v1, ", min_ink_coverage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->min_ink_coverage:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 215
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->max_ink_coverage:Ljava/lang/Float;

    if-eqz v1, :cond_3

    const-string v1, ", max_ink_coverage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->max_ink_coverage:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 216
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_4

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 217
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    if-eqz v1, :cond_5

    const-string v1, ", idv_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 218
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_needs_ssn:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", idv_needs_ssn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_needs_ssn:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 219
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->can_retry_idv:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", can_retry_idv="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->can_retry_idv:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 220
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_retries_remaining:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    const-string v1, ", idv_retries_remaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_retries_remaining:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 221
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v1, :cond_9

    const-string v1, ", owner_address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_a

    const-string v1, ", owner_birth_date=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetCustomizationSettingsResponse{"

    .line 223
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
