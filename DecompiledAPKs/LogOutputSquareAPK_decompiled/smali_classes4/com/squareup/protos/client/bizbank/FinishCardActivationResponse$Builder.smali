.class public final Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FinishCardActivationResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse;",
        "Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card_token:Ljava/lang/String;

.field public result:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 96
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse;
    .locals 4

    .line 111
    new-instance v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Builder;->result:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Builder;->card_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse;-><init>(Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse;

    move-result-object v0

    return-object v0
.end method

.method public card_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Builder;
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Builder;->card_token:Ljava/lang/String;

    return-object p0
.end method

.method public result(Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;)Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Builder;
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Builder;->result:Lcom/squareup/protos/client/bizbank/FinishCardActivationResponse$Result;

    return-object p0
.end method
