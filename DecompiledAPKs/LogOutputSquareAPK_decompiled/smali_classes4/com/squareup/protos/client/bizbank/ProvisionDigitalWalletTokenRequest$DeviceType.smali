.class public final enum Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;
.super Ljava/lang/Enum;
.source "ProvisionDigitalWalletTokenRequest.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DeviceType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType$ProtoAdapter_DeviceType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum MOBILE_PHONE:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

.field public static final enum WATCH:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 168
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->UNKNOWN:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    .line 170
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    const/4 v3, 0x2

    const-string v4, "MOBILE_PHONE"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->MOBILE_PHONE:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    .line 172
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    const/4 v4, 0x3

    const-string v5, "WATCH"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->WATCH:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    new-array v0, v4, [Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    .line 167
    sget-object v4, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->UNKNOWN:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->MOBILE_PHONE:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->WATCH:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->$VALUES:[Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    .line 174
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType$ProtoAdapter_DeviceType;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType$ProtoAdapter_DeviceType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 178
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 179
    iput p3, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 189
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->WATCH:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    return-object p0

    .line 188
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->MOBILE_PHONE:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    return-object p0

    .line 187
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->UNKNOWN:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;
    .locals 1

    .line 167
    const-class v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;
    .locals 1

    .line 167
    sget-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->$VALUES:[Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 196
    iget v0, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->value:I

    return v0
.end method
