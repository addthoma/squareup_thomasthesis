.class public final Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;
.super Lcom/squareup/wire/Message;
.source "FinishCardActivationRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ProtoAdapter_FinishCardActivationRequest;,
        Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;,
        Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;",
        "Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACTIVATION_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final activation_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final card_track_data:Lcom/squareup/protos/client/bills/CardData;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardData#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final manual_entry_cvv_expiration:Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.FinishCardActivationRequest$ActivationCvvExpiration#ADAPTER"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ProtoAdapter_FinishCardActivationRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ProtoAdapter_FinishCardActivationRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;Lcom/squareup/protos/client/bills/CardData;)V
    .locals 1

    .line 54
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;Lcom/squareup/protos/client/bills/CardData;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;Lcom/squareup/protos/client/bills/CardData;Lokio/ByteString;)V
    .locals 1

    .line 60
    sget-object v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 61
    invoke-static {p2, p3}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p4

    const/4 v0, 0x1

    if-gt p4, v0, :cond_0

    .line 64
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->activation_token:Ljava/lang/String;

    .line 65
    iput-object p2, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->manual_entry_cvv_expiration:Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    .line 66
    iput-object p3, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->card_track_data:Lcom/squareup/protos/client/bills/CardData;

    return-void

    .line 62
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of manual_entry_cvv_expiration, card_track_data may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 82
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 83
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->activation_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->activation_token:Ljava/lang/String;

    .line 85
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->manual_entry_cvv_expiration:Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->manual_entry_cvv_expiration:Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    .line 86
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->card_track_data:Lcom/squareup/protos/client/bills/CardData;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->card_track_data:Lcom/squareup/protos/client/bills/CardData;

    .line 87
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 92
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->activation_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->manual_entry_cvv_expiration:Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->card_track_data:Lcom/squareup/protos/client/bills/CardData;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 98
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;
    .locals 2

    .line 71
    new-instance v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;-><init>()V

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->activation_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->activation_token:Ljava/lang/String;

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->manual_entry_cvv_expiration:Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->manual_entry_cvv_expiration:Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->card_track_data:Lcom/squareup/protos/client/bills/CardData;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->card_track_data:Lcom/squareup/protos/client/bills/CardData;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->newBuilder()Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->activation_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", activation_token=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->manual_entry_cvv_expiration:Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    if-eqz v1, :cond_1

    const-string v1, ", manual_entry_cvv_expiration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->manual_entry_cvv_expiration:Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest$ActivationCvvExpiration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 108
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->card_track_data:Lcom/squareup/protos/client/bills/CardData;

    if-eqz v1, :cond_2

    const-string v1, ", card_track_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;->card_track_data:Lcom/squareup/protos/client/bills/CardData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FinishCardActivationRequest{"

    .line 109
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
