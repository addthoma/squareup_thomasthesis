.class public final Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AddTipAndSettleResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;",
        "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public processing_status:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

.field public tender_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 105
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;
    .locals 4

    .line 126
    new-instance v0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$Builder;->tender_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$Builder;->processing_status:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$Builder;->build()Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;

    move-result-object v0

    return-object v0
.end method

.method public processing_status(Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;)Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$Builder;->processing_status:Lcom/squareup/protos/client/paper_signature/TenderAwaitingMerchantTipProcessingStatus;

    return-object p0
.end method

.method public tender_id(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$Builder;
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse$Builder;->tender_id:Ljava/lang/String;

    return-object p0
.end method
