.class public final Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TenderStatusRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;",
        "Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_and_tender_list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/BillAndTender;",
            ">;"
        }
    .end annotation
.end field

.field public tender_id_list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 112
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 113
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;->tender_id_list:Ljava/util/List;

    .line 114
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;->bill_and_tender_list:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bill_and_tender_list(Ljava/util/List;)Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/BillAndTender;",
            ">;)",
            "Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;"
        }
    .end annotation

    .line 138
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;->bill_and_tender_list:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;
    .locals 4

    .line 145
    new-instance v0, Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;->tender_id_list:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;->bill_and_tender_list:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;-><init>(Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;->build()Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;

    move-result-object v0

    return-object v0
.end method

.method public tender_id_list(Ljava/util/List;)Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;"
        }
    .end annotation

    .line 123
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/TenderStatusRequest$Builder;->tender_id_list:Ljava/util/List;

    return-object p0
.end method
