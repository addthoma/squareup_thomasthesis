.class public final Lcom/squareup/protos/client/paper_signature/TenderWithBillId$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TenderWithBillId.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/paper_signature/TenderWithBillId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/paper_signature/TenderWithBillId;",
        "Lcom/squareup/protos/client/paper_signature/TenderWithBillId$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_id_pair:Lcom/squareup/protos/client/IdPair;

.field public current_amendment_token:Ljava/lang/String;

.field public tender:Lcom/squareup/protos/client/bills/Tender;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 125
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/paper_signature/TenderWithBillId$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/TenderWithBillId$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/paper_signature/TenderWithBillId;
    .locals 5

    .line 158
    new-instance v0, Lcom/squareup/protos/client/paper_signature/TenderWithBillId;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/TenderWithBillId$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/paper_signature/TenderWithBillId$Builder;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v3, p0, Lcom/squareup/protos/client/paper_signature/TenderWithBillId$Builder;->current_amendment_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/paper_signature/TenderWithBillId;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/TenderWithBillId$Builder;->build()Lcom/squareup/protos/client/paper_signature/TenderWithBillId;

    move-result-object v0

    return-object v0
.end method

.method public current_amendment_token(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/TenderWithBillId$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/TenderWithBillId$Builder;->current_amendment_token:Ljava/lang/String;

    return-object p0
.end method

.method public tender(Lcom/squareup/protos/client/bills/Tender;)Lcom/squareup/protos/client/paper_signature/TenderWithBillId$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/TenderWithBillId$Builder;->tender:Lcom/squareup/protos/client/bills/Tender;

    return-object p0
.end method
