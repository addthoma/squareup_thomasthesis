.class public final enum Lcom/squareup/protos/client/rolodex/GroupType;
.super Ljava/lang/Enum;
.source "GroupType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/GroupType$ProtoAdapter_GroupType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/rolodex/GroupType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/rolodex/GroupType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/GroupType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AUDIENCE_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

.field public static final enum MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 17
    new-instance v0, Lcom/squareup/protos/client/rolodex/GroupType;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "MANUAL_GROUP"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/client/rolodex/GroupType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    .line 22
    new-instance v0, Lcom/squareup/protos/client/rolodex/GroupType;

    const/4 v3, 0x2

    const-string v4, "AUDIENCE_GROUP"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/client/rolodex/GroupType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/GroupType;->AUDIENCE_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    new-array v0, v3, [Lcom/squareup/protos/client/rolodex/GroupType;

    .line 13
    sget-object v3, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/GroupType;->AUDIENCE_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/rolodex/GroupType;->$VALUES:[Lcom/squareup/protos/client/rolodex/GroupType;

    .line 24
    new-instance v0, Lcom/squareup/protos/client/rolodex/GroupType$ProtoAdapter_GroupType;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/GroupType$ProtoAdapter_GroupType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/GroupType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    iput p3, p0, Lcom/squareup/protos/client/rolodex/GroupType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/rolodex/GroupType;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 38
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/rolodex/GroupType;->AUDIENCE_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    return-object p0

    .line 37
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/GroupType;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/client/rolodex/GroupType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/rolodex/GroupType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/rolodex/GroupType;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/client/rolodex/GroupType;->$VALUES:[Lcom/squareup/protos/client/rolodex/GroupType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/rolodex/GroupType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/rolodex/GroupType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 45
    iget v0, p0, Lcom/squareup/protos/client/rolodex/GroupType;->value:I

    return v0
.end method
