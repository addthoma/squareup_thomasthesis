.class public final Lcom/squareup/protos/client/rolodex/Event$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Event.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/Event;",
        "Lcom/squareup/protos/client/rolodex/Event$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public event_token:Ljava/lang/String;

.field public link_relative_url:Ljava/lang/String;

.field public occurred_at:Lcom/squareup/protos/common/time/DateTime;

.field public subtitle:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public type:Lcom/squareup/protos/client/rolodex/EventType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 155
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/Event;
    .locals 9

    .line 190
    new-instance v8, Lcom/squareup/protos/client/rolodex/Event;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Event$Builder;->event_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/Event$Builder;->title:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/Event$Builder;->subtitle:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/Event$Builder;->link_relative_url:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/rolodex/Event$Builder;->occurred_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v6, p0, Lcom/squareup/protos/client/rolodex/Event$Builder;->type:Lcom/squareup/protos/client/rolodex/EventType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/rolodex/Event;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/client/rolodex/EventType;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 142
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Event$Builder;->build()Lcom/squareup/protos/client/rolodex/Event;

    move-result-object v0

    return-object v0
.end method

.method public event_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Event$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Event$Builder;->event_token:Ljava/lang/String;

    return-object p0
.end method

.method public link_relative_url(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Event$Builder;
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Event$Builder;->link_relative_url:Ljava/lang/String;

    return-object p0
.end method

.method public occurred_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/rolodex/Event$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Event$Builder;->occurred_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public subtitle(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Event$Builder;
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Event$Builder;->subtitle:Ljava/lang/String;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Event$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Event$Builder;->title:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/rolodex/EventType;)Lcom/squareup/protos/client/rolodex/Event$Builder;
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Event$Builder;->type:Lcom/squareup/protos/client/rolodex/EventType;

    return-object p0
.end method
