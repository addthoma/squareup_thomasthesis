.class final Lcom/squareup/protos/client/rolodex/Merchant$ProtoAdapter_Merchant;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Merchant.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/Merchant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Merchant"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/rolodex/Merchant;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 187
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/rolodex/Merchant;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/Merchant;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 212
    new-instance v0, Lcom/squareup/protos/client/rolodex/Merchant$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Merchant$Builder;-><init>()V

    .line 213
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 214
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 222
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 220
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/rolodex/MerchantSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/MerchantSettings;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->merchant_settings(Lcom/squareup/protos/client/rolodex/MerchantSettings;)Lcom/squareup/protos/client/rolodex/Merchant$Builder;

    goto :goto_0

    .line 219
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->num_customers(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/Merchant$Builder;

    goto :goto_0

    .line 218
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/rolodex/AttributeSchema;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/AttributeSchema;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->attribute_schema(Lcom/squareup/protos/client/rolodex/AttributeSchema;)Lcom/squareup/protos/client/rolodex/Merchant$Builder;

    goto :goto_0

    .line 217
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->unread_messages_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/Merchant$Builder;

    goto :goto_0

    .line 216
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->manual_customer_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/Merchant$Builder;

    goto :goto_0

    .line 226
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 227
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->build()Lcom/squareup/protos/client/rolodex/Merchant;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 185
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/Merchant$ProtoAdapter_Merchant;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/Merchant;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/Merchant;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 202
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Merchant;->manual_customer_count:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 203
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Merchant;->unread_messages_count:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 204
    sget-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Merchant;->attribute_schema:Lcom/squareup/protos/client/rolodex/AttributeSchema;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 205
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Merchant;->num_customers:Ljava/lang/Integer;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 206
    sget-object v0, Lcom/squareup/protos/client/rolodex/MerchantSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Merchant;->merchant_settings:Lcom/squareup/protos/client/rolodex/MerchantSettings;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 207
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/Merchant;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 185
    check-cast p2, Lcom/squareup/protos/client/rolodex/Merchant;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/rolodex/Merchant$ProtoAdapter_Merchant;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/Merchant;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/rolodex/Merchant;)I
    .locals 4

    .line 192
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Merchant;->manual_customer_count:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Merchant;->unread_messages_count:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 193
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/AttributeSchema;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Merchant;->attribute_schema:Lcom/squareup/protos/client/rolodex/AttributeSchema;

    const/4 v3, 0x3

    .line 194
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Merchant;->num_customers:Ljava/lang/Integer;

    const/4 v3, 0x4

    .line 195
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/MerchantSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Merchant;->merchant_settings:Lcom/squareup/protos/client/rolodex/MerchantSettings;

    const/4 v3, 0x5

    .line 196
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 197
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Merchant;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 185
    check-cast p1, Lcom/squareup/protos/client/rolodex/Merchant;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/Merchant$ProtoAdapter_Merchant;->encodedSize(Lcom/squareup/protos/client/rolodex/Merchant;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/rolodex/Merchant;)Lcom/squareup/protos/client/rolodex/Merchant;
    .locals 2

    .line 232
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Merchant;->newBuilder()Lcom/squareup/protos/client/rolodex/Merchant$Builder;

    move-result-object p1

    .line 233
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->attribute_schema:Lcom/squareup/protos/client/rolodex/AttributeSchema;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->attribute_schema:Lcom/squareup/protos/client/rolodex/AttributeSchema;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/AttributeSchema;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->attribute_schema:Lcom/squareup/protos/client/rolodex/AttributeSchema;

    .line 234
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->merchant_settings:Lcom/squareup/protos/client/rolodex/MerchantSettings;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/rolodex/MerchantSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->merchant_settings:Lcom/squareup/protos/client/rolodex/MerchantSettings;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/MerchantSettings;

    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->merchant_settings:Lcom/squareup/protos/client/rolodex/MerchantSettings;

    .line 235
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 236
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Merchant$Builder;->build()Lcom/squareup/protos/client/rolodex/Merchant;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 185
    check-cast p1, Lcom/squareup/protos/client/rolodex/Merchant;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/Merchant$ProtoAdapter_Merchant;->redact(Lcom/squareup/protos/client/rolodex/Merchant;)Lcom/squareup/protos/client/rolodex/Merchant;

    move-result-object p1

    return-object p1
.end method
