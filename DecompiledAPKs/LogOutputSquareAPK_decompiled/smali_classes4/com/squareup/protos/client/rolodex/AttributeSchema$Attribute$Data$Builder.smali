.class public final Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AttributeSchema.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public address:Lcom/squareup/protos/common/location/GlobalAddress;

.field public boolean_:Ljava/lang/Boolean;

.field public date:Lcom/squareup/protos/common/time/DateTime;

.field public email:Ljava/lang/String;

.field public enum_values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public number:Ljava/lang/Float;

.field public number_text:Ljava/lang/String;

.field public phone:Ljava/lang/String;

.field public text:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 986
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 987
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->enum_values:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;
    .locals 0

    .line 1032
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object p0
.end method

.method public boolean_(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;
    .locals 0

    .line 1001
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->boolean_:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;
    .locals 12

    .line 1043
    new-instance v11, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->number:Ljava/lang/Float;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->boolean_:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->text:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->number_text:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->enum_values:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->phone:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->email:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v9, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->date:Lcom/squareup/protos/common/time/DateTime;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;-><init>(Ljava/lang/Float;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 967
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    move-result-object v0

    return-object v0
.end method

.method public date(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;
    .locals 0

    .line 1037
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->date:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public email(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;
    .locals 0

    .line 1027
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->email:Ljava/lang/String;

    return-object p0
.end method

.method public enum_values(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;"
        }
    .end annotation

    .line 1016
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1017
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->enum_values:Ljava/util/List;

    return-object p0
.end method

.method public number(Ljava/lang/Float;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 996
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->number:Ljava/lang/Float;

    return-object p0
.end method

.method public number_text(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;
    .locals 0

    .line 1011
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->number_text:Ljava/lang/String;

    return-object p0
.end method

.method public phone(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;
    .locals 0

    .line 1022
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->phone:Ljava/lang/String;

    return-object p0
.end method

.method public text(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;
    .locals 0

    .line 1006
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->text:Ljava/lang/String;

    return-object p0
.end method
