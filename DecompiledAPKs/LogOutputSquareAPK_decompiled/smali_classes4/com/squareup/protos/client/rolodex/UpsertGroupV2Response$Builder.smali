.class public final Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpsertGroupV2Response.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;",
        "Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public group:Lcom/squareup/protos/client/rolodex/GroupV2;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 91
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;
    .locals 4

    .line 106
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response$Builder;->group:Lcom/squareup/protos/client/rolodex/GroupV2;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/rolodex/GroupV2;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response$Builder;->build()Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;

    move-result-object v0

    return-object v0
.end method

.method public group(Lcom/squareup/protos/client/rolodex/GroupV2;)Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response$Builder;
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response$Builder;->group:Lcom/squareup/protos/client/rolodex/GroupV2;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response$Builder;
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
