.class public final Lcom/squareup/protos/client/rolodex/MergeProposal$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MergeProposal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/MergeProposal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/MergeProposal;",
        "Lcom/squareup/protos/client/rolodex/MergeProposal$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public duplicate_contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field public new_contact:Lcom/squareup/protos/client/rolodex/Contact;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 93
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 94
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/MergeProposal$Builder;->duplicate_contacts:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/MergeProposal;
    .locals 4

    .line 110
    new-instance v0, Lcom/squareup/protos/client/rolodex/MergeProposal;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/MergeProposal$Builder;->duplicate_contacts:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/MergeProposal$Builder;->new_contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/MergeProposal;-><init>(Ljava/util/List;Lcom/squareup/protos/client/rolodex/Contact;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/MergeProposal$Builder;->build()Lcom/squareup/protos/client/rolodex/MergeProposal;

    move-result-object v0

    return-object v0
.end method

.method public duplicate_contacts(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/MergeProposal$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/MergeProposal$Builder;"
        }
    .end annotation

    .line 98
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 99
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/MergeProposal$Builder;->duplicate_contacts:Ljava/util/List;

    return-object p0
.end method

.method public new_contact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/protos/client/rolodex/MergeProposal$Builder;
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/MergeProposal$Builder;->new_contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object p0
.end method
