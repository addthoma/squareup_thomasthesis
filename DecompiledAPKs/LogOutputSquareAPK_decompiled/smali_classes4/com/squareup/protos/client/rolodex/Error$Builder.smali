.class public final Lcom/squareup/protos/client/rolodex/Error$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Error.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/Error;",
        "Lcom/squareup/protos/client/rolodex/Error$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public category:Lcom/squareup/protos/client/rolodex/Error$Category;

.field public code:Lcom/squareup/protos/client/rolodex/Error$Code;

.field public detail:Ljava/lang/String;

.field public field:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 139
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/Error;
    .locals 7

    .line 176
    new-instance v6, Lcom/squareup/protos/client/rolodex/Error;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Error$Builder;->category:Lcom/squareup/protos/client/rolodex/Error$Category;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/Error$Builder;->code:Lcom/squareup/protos/client/rolodex/Error$Code;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/Error$Builder;->detail:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/Error$Builder;->field:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/rolodex/Error;-><init>(Lcom/squareup/protos/client/rolodex/Error$Category;Lcom/squareup/protos/client/rolodex/Error$Code;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Error$Builder;->build()Lcom/squareup/protos/client/rolodex/Error;

    move-result-object v0

    return-object v0
.end method

.method public category(Lcom/squareup/protos/client/rolodex/Error$Category;)Lcom/squareup/protos/client/rolodex/Error$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Error$Builder;->category:Lcom/squareup/protos/client/rolodex/Error$Category;

    return-object p0
.end method

.method public code(Lcom/squareup/protos/client/rolodex/Error$Code;)Lcom/squareup/protos/client/rolodex/Error$Builder;
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Error$Builder;->code:Lcom/squareup/protos/client/rolodex/Error$Code;

    return-object p0
.end method

.method public detail(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Error$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Error$Builder;->detail:Ljava/lang/String;

    return-object p0
.end method

.method public field(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Error$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/Error$Builder;->field:Ljava/lang/String;

    return-object p0
.end method
