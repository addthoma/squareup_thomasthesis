.class public final Lcom/squareup/protos/client/rolodex/GroupV2;
.super Lcom/squareup/wire/Message;
.source "GroupV2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/GroupV2$ProtoAdapter_GroupV2;,
        Lcom/squareup/protos/client/rolodex/GroupV2$Type;,
        Lcom/squareup/protos/client/rolodex/GroupV2$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/GroupV2;",
        "Lcom/squareup/protos/client/rolodex/GroupV2$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/GroupV2;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.GroupV2Counts#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final filters:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.Filter#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field

.field public final group_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/rolodex/GroupV2$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.GroupV2$Type#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/protos/client/rolodex/GroupV2$ProtoAdapter_GroupV2;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/GroupV2$ProtoAdapter_GroupV2;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/GroupV2;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/protos/client/rolodex/GroupV2$Type;->INVALID:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    sput-object v0, Lcom/squareup/protos/client/rolodex/GroupV2;->DEFAULT_TYPE:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/rolodex/GroupV2$Type;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/rolodex/GroupV2Counts;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/GroupV2$Type;",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;",
            "Lcom/squareup/protos/client/rolodex/GroupV2Counts;",
            ")V"
        }
    .end annotation

    .line 106
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/rolodex/GroupV2;-><init>(Lcom/squareup/protos/client/rolodex/GroupV2$Type;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/rolodex/GroupV2Counts;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/rolodex/GroupV2$Type;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/rolodex/GroupV2Counts;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/GroupV2$Type;",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;",
            "Lcom/squareup/protos/client/rolodex/GroupV2Counts;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 111
    sget-object v0, Lcom/squareup/protos/client/rolodex/GroupV2;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 112
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->type:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    .line 113
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->group_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 114
    iput-object p3, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->merchant_token:Ljava/lang/String;

    .line 115
    iput-object p4, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->name:Ljava/lang/String;

    const-string p1, "filters"

    .line 116
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->filters:Ljava/util/List;

    .line 117
    iput-object p6, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 136
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/GroupV2;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 137
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/GroupV2;

    .line 138
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GroupV2;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GroupV2;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->type:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/GroupV2;->type:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    .line 139
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->group_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/GroupV2;->group_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 140
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->merchant_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/GroupV2;->merchant_token:Ljava/lang/String;

    .line 141
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/GroupV2;->name:Ljava/lang/String;

    .line 142
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->filters:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/GroupV2;->filters:Ljava/util/List;

    .line 143
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/GroupV2;->counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    .line 144
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 149
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 151
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GroupV2;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->type:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/GroupV2$Type;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->group_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->filters:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/GroupV2Counts;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 158
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/GroupV2$Builder;
    .locals 2

    .line 122
    new-instance v0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;-><init>()V

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->type:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->type:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->group_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->group_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->merchant_token:Ljava/lang/String;

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->name:Ljava/lang/String;

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->filters:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->filters:Ljava/util/List;

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GroupV2;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GroupV2;->newBuilder()Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 166
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->type:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->type:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 167
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->group_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_1

    const-string v1, ", group_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->group_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 168
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->filters:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", filters="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->filters:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 171
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    if-eqz v1, :cond_5

    const-string v1, ", counts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GroupV2{"

    .line 172
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
