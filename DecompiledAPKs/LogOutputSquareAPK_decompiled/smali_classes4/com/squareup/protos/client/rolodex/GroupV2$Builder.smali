.class public final Lcom/squareup/protos/client/rolodex/GroupV2$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/GroupV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/GroupV2;",
        "Lcom/squareup/protos/client/rolodex/GroupV2$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

.field public filters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field

.field public group_id_pair:Lcom/squareup/protos/client/IdPair;

.field public merchant_token:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public type:Lcom/squareup/protos/client/rolodex/GroupV2$Type;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 188
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 189
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->filters:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/GroupV2;
    .locals 9

    .line 254
    new-instance v8, Lcom/squareup/protos/client/rolodex/GroupV2;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->type:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->group_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->merchant_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->filters:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/rolodex/GroupV2;-><init>(Lcom/squareup/protos/client/rolodex/GroupV2$Type;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/rolodex/GroupV2Counts;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 175
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->build()Lcom/squareup/protos/client/rolodex/GroupV2;

    move-result-object v0

    return-object v0
.end method

.method public counts(Lcom/squareup/protos/client/rolodex/GroupV2Counts;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;
    .locals 0

    .line 248
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    return-object p0
.end method

.method public filters(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/GroupV2$Builder;"
        }
    .end annotation

    .line 239
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 240
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->filters:Ljava/util/List;

    return-object p0
.end method

.method public group_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->group_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;
    .locals 0

    .line 217
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/rolodex/GroupV2$Type;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->type:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    return-object p0
.end method
