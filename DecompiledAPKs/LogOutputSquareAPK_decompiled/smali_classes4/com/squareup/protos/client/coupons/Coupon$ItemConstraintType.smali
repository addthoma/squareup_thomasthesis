.class public final enum Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;
.super Ljava/lang/Enum;
.source "Coupon.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/coupons/Coupon;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ItemConstraintType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType$ProtoAdapter_ItemConstraintType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CATEGORY:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

.field public static final enum VARIATION:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 348
    new-instance v0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->UNKNOWN:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    .line 353
    new-instance v0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    const/4 v2, 0x1

    const-string v3, "CATEGORY"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->CATEGORY:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    .line 358
    new-instance v0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    const/4 v3, 0x2

    const-string v4, "VARIATION"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->VARIATION:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    .line 344
    sget-object v4, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->UNKNOWN:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->CATEGORY:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->VARIATION:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->$VALUES:[Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    .line 360
    new-instance v0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType$ProtoAdapter_ItemConstraintType;

    invoke-direct {v0}, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType$ProtoAdapter_ItemConstraintType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 364
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 365
    iput p3, p0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 375
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->VARIATION:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    return-object p0

    .line 374
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->CATEGORY:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    return-object p0

    .line 373
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->UNKNOWN:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;
    .locals 1

    .line 344
    const-class v0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;
    .locals 1

    .line 344
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->$VALUES:[Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 382
    iget v0, p0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->value:I

    return v0
.end method
