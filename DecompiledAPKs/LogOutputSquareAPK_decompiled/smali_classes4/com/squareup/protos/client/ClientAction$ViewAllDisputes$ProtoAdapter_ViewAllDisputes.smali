.class final Lcom/squareup/protos/client/ClientAction$ViewAllDisputes$ProtoAdapter_ViewAllDisputes;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ClientAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ViewAllDisputes"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2389
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2404
    new-instance v0, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes$Builder;-><init>()V

    .line 2405
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2406
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 2409
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2413
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2414
    invoke-virtual {v0}, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2387
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes$ProtoAdapter_ViewAllDisputes;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2399
    invoke-virtual {p2}, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2387
    check-cast p2, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes$ProtoAdapter_ViewAllDisputes;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;)I
    .locals 0

    .line 2394
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2387
    check-cast p1, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes$ProtoAdapter_ViewAllDisputes;->encodedSize(Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;)Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;
    .locals 0

    .line 2419
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;->newBuilder()Lcom/squareup/protos/client/ClientAction$ViewAllDisputes$Builder;

    move-result-object p1

    .line 2420
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2421
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2387
    check-cast p1, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewAllDisputes$ProtoAdapter_ViewAllDisputes;->redact(Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;)Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    move-result-object p1

    return-object p1
.end method
