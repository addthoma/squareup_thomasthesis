.class public final Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UnitMetadataDisplayDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allowed_mime_type:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;",
            ">;"
        }
    .end annotation
.end field

.field public max_count:Ljava/lang/Integer;

.field public max_total_size_bytes:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 333
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 334
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->allowed_mime_type:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public allowed_mime_type(Ljava/util/List;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$MimeType;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;"
        }
    .end annotation

    .line 357
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 358
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->allowed_mime_type:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;
    .locals 5

    .line 364
    new-instance v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->max_count:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->max_total_size_bytes:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->allowed_mime_type:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 326
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->build()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    move-result-object v0

    return-object v0
.end method

.method public max_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;
    .locals 0

    .line 341
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->max_count:Ljava/lang/Integer;

    return-object p0
.end method

.method public max_total_size_bytes(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;
    .locals 0

    .line 349
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits$Builder;->max_total_size_bytes:Ljava/lang/Integer;

    return-object p0
.end method
