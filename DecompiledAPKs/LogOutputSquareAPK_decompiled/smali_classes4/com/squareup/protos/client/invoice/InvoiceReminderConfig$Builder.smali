.class public final Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InvoiceReminderConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public custom_message:Ljava/lang/String;

.field public relative_days:Ljava/lang/Integer;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 123
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;
    .locals 5

    .line 152
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;->relative_days:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;->custom_message:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 116
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    move-result-object v0

    return-object v0
.end method

.method public custom_message(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;->custom_message:Ljava/lang/String;

    return-object p0
.end method

.method public relative_days(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;->relative_days:Ljava/lang/Integer;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
