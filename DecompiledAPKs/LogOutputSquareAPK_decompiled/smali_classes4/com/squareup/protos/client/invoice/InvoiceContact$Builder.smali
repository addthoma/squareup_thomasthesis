.class public final Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InvoiceContact.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceContact;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/InvoiceContact;",
        "Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public buyer_billing_address:Lcom/squareup/protos/common/location/GlobalAddress;

.field public buyer_company_name:Ljava/lang/String;

.field public buyer_email:Ljava/lang/String;

.field public buyer_name:Ljava/lang/String;

.field public buyer_phone_number:Ljava/lang/String;

.field public contact_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 180
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/InvoiceContact;
    .locals 9

    .line 234
    new-instance v8, Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->contact_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->buyer_email:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->buyer_name:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->buyer_phone_number:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->buyer_company_name:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->buyer_billing_address:Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/invoice/InvoiceContact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 167
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceContact;

    move-result-object v0

    return-object v0
.end method

.method public buyer_billing_address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->buyer_billing_address:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object p0
.end method

.method public buyer_company_name(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->buyer_company_name:Ljava/lang/String;

    return-object p0
.end method

.method public buyer_email(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;
    .locals 0

    .line 195
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->buyer_email:Ljava/lang/String;

    return-object p0
.end method

.method public buyer_name(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->buyer_name:Ljava/lang/String;

    return-object p0
.end method

.method public buyer_phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;
    .locals 0

    .line 212
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->buyer_phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceContact$Builder;->contact_token:Ljava/lang/String;

    return-object p0
.end method
