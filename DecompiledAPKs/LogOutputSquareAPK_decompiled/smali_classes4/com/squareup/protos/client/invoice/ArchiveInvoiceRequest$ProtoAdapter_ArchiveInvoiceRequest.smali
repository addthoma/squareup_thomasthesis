.class final Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$ProtoAdapter_ArchiveInvoiceRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ArchiveInvoiceRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ArchiveInvoiceRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 114
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 133
    new-instance v0, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;-><init>()V

    .line 134
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 135
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 140
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 138
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;->version(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;

    goto :goto_0

    .line 137
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;->id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;

    goto :goto_0

    .line 144
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 145
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;->build()Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 112
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$ProtoAdapter_ArchiveInvoiceRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 126
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 127
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;->version:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 128
    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 112
    check-cast p2, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$ProtoAdapter_ArchiveInvoiceRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;)I
    .locals 4

    .line 119
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;->id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;->version:Ljava/lang/String;

    const/4 v3, 0x2

    .line 120
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 121
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$ProtoAdapter_ArchiveInvoiceRequest;->encodedSize(Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;)Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;
    .locals 2

    .line 150
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;->newBuilder()Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;

    move-result-object p1

    .line 151
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 152
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 153
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$Builder;->build()Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest$ProtoAdapter_ArchiveInvoiceRequest;->redact(Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;)Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;

    move-result-object p1

    return-object p1
.end method
