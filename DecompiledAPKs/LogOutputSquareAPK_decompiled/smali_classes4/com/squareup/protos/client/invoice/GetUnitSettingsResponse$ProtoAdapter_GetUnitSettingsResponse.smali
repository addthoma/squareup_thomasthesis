.class final Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$ProtoAdapter_GetUnitSettingsResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetUnitSettingsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetUnitSettingsResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 148
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 169
    new-instance v0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;-><init>()V

    .line 170
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 171
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 177
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 175
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->invoice_defaults:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 174
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/invoice/UnitSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/UnitSettings;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->unit_settings(Lcom/squareup/protos/client/invoice/UnitSettings;)Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;

    goto :goto_0

    .line 173
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;

    goto :goto_0

    .line 181
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 182
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->build()Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 146
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$ProtoAdapter_GetUnitSettingsResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 161
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 162
    sget-object v0, Lcom/squareup/protos/client/invoice/UnitSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->unit_settings:Lcom/squareup/protos/client/invoice/UnitSettings;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 163
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->invoice_defaults:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 164
    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 146
    check-cast p2, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$ProtoAdapter_GetUnitSettingsResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;)I
    .locals 4

    .line 153
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/invoice/UnitSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->unit_settings:Lcom/squareup/protos/client/invoice/UnitSettings;

    const/4 v3, 0x2

    .line 154
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 155
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->invoice_defaults:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 156
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 146
    check-cast p1, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$ProtoAdapter_GetUnitSettingsResponse;->encodedSize(Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;)Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;
    .locals 2

    .line 187
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;->newBuilder()Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;

    move-result-object p1

    .line 188
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Status;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 189
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->unit_settings:Lcom/squareup/protos/client/invoice/UnitSettings;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/invoice/UnitSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->unit_settings:Lcom/squareup/protos/client/invoice/UnitSettings;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/UnitSettings;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->unit_settings:Lcom/squareup/protos/client/invoice/UnitSettings;

    .line 190
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->invoice_defaults:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 191
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 192
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$Builder;->build()Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 146
    check-cast p1, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse$ProtoAdapter_GetUnitSettingsResponse;->redact(Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;)Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;

    move-result-object p1

    return-object p1
.end method
