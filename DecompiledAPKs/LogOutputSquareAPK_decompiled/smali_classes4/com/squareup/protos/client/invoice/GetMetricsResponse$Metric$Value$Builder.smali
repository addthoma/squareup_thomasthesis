.class public final Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetMetricsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public money_value:Lcom/squareup/protos/common/Money;

.field public value_type:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 340
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;
    .locals 4

    .line 355
    new-instance v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;->value_type:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;->money_value:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;-><init>(Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 335
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    move-result-object v0

    return-object v0
.end method

.method public money_value(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;
    .locals 0

    .line 349
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;->money_value:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public value_type(Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;
    .locals 0

    .line 344
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value$Builder;->value_type:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$ValueType;

    return-object p0
.end method
