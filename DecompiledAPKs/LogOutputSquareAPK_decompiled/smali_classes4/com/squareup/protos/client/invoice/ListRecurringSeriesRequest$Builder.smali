.class public final Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListRecurringSeriesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;",
        "Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

.field public limit:Ljava/lang/Integer;

.field public paging_key:Ljava/lang/String;

.field public query:Ljava/lang/String;

.field public state_filter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 156
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;
    .locals 8

    .line 199
    new-instance v7, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->paging_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->limit:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->state_filter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    iget-object v4, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    iget-object v5, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->query:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;-><init>(Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;Lcom/squareup/protos/common/time/DateTimeInterval;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 145
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->build()Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;

    move-result-object v0

    return-object v0
.end method

.method public date_range(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    return-object p0
.end method

.method public limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->limit:Ljava/lang/Integer;

    return-object p0
.end method

.method public paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->paging_key:Ljava/lang/String;

    return-object p0
.end method

.method public query(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->query:Ljava/lang/String;

    return-object p0
.end method

.method public state_filter(Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;)Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$Builder;->state_filter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    return-object p0
.end method
