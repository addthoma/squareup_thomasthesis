.class public final Lcom/squareup/protos/client/invoice/ListInvoicesResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListInvoicesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/ListInvoicesResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/ListInvoicesResponse;",
        "Lcom/squareup/protos/client/invoice/ListInvoicesResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public invoice:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            ">;"
        }
    .end annotation
.end field

.field public matches_in_archive_count:Ljava/lang/Integer;

.field public paging_key:Ljava/lang/String;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 134
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 135
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/ListInvoicesResponse$Builder;->invoice:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/ListInvoicesResponse;
    .locals 7

    .line 169
    new-instance v6, Lcom/squareup/protos/client/invoice/ListInvoicesResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/ListInvoicesResponse$Builder;->invoice:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/ListInvoicesResponse$Builder;->paging_key:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/invoice/ListInvoicesResponse$Builder;->matches_in_archive_count:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/invoice/ListInvoicesResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 125
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/ListInvoicesResponse$Builder;->build()Lcom/squareup/protos/client/invoice/ListInvoicesResponse;

    move-result-object v0

    return-object v0
.end method

.method public invoice(Ljava/util/List;)Lcom/squareup/protos/client/invoice/ListInvoicesResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/ListInvoicesResponse$Builder;"
        }
    .end annotation

    .line 144
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesResponse$Builder;->invoice:Ljava/util/List;

    return-object p0
.end method

.method public matches_in_archive_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/ListInvoicesResponse$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesResponse$Builder;->matches_in_archive_count:Ljava/lang/Integer;

    return-object p0
.end method

.method public paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/ListInvoicesResponse$Builder;
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesResponse$Builder;->paging_key:Ljava/lang/String;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/invoice/ListInvoicesResponse$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/ListInvoicesResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
