.class final Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$ProtoAdapter_GetUnitMetadataResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetUnitMetadataResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetUnitMetadataResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 141
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 162
    new-instance v0, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;-><init>()V

    .line 163
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 164
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 170
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 168
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->unit_metadata(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;)Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;

    goto :goto_0

    .line 167
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/invoice/UnitMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/UnitMetadata;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->metadata(Lcom/squareup/protos/client/invoice/UnitMetadata;)Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;

    goto :goto_0

    .line 166
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;

    goto :goto_0

    .line 174
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 175
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->build()Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 139
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$ProtoAdapter_GetUnitMetadataResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 154
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 155
    sget-object v0, Lcom/squareup/protos/client/invoice/UnitMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;->metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 156
    sget-object v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 157
    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 139
    check-cast p2, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$ProtoAdapter_GetUnitMetadataResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;)I
    .locals 4

    .line 146
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/invoice/UnitMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;->metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    const/4 v3, 0x2

    .line 147
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    const/4 v3, 0x3

    .line 148
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 139
    check-cast p1, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$ProtoAdapter_GetUnitMetadataResponse;->encodedSize(Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;)Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;
    .locals 2

    .line 180
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;->newBuilder()Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;

    move-result-object p1

    .line 181
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Status;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 182
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/invoice/UnitMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/UnitMetadata;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    .line 183
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    .line 184
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 185
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$Builder;->build()Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 139
    check-cast p1, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse$ProtoAdapter_GetUnitMetadataResponse;->redact(Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;)Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;

    move-result-object p1

    return-object p1
.end method
