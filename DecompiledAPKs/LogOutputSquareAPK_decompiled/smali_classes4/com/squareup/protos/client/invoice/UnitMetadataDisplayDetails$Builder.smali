.class public final Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UnitMetadataDisplayDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public analytics:Lcom/squareup/protos/client/invoice/UnitAnalytics;

.field public has_estimates:Ljava/lang/Boolean;

.field public has_invoices:Ljava/lang/Boolean;

.field public invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

.field public limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

.field public unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 175
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public analytics(Lcom/squareup/protos/client/invoice/UnitAnalytics;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->analytics:Lcom/squareup/protos/client/invoice/UnitAnalytics;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;
    .locals 9

    .line 229
    new-instance v8, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->has_invoices:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->analytics:Lcom/squareup/protos/client/invoice/UnitAnalytics;

    iget-object v4, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    iget-object v5, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    iget-object v6, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->has_estimates:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;-><init>(Lcom/squareup/protos/client/invoice/UnitMetadata;Ljava/lang/Boolean;Lcom/squareup/protos/client/invoice/UnitAnalytics;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 162
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->build()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    move-result-object v0

    return-object v0
.end method

.method public has_estimates(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->has_estimates:Ljava/lang/Boolean;

    return-object p0
.end method

.method public has_invoices(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;
    .locals 0

    .line 190
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->has_invoices:Ljava/lang/Boolean;

    return-object p0
.end method

.method public invoice_automatic_reminder_settings(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    return-object p0
.end method

.method public limits(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;
    .locals 0

    .line 206
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    return-object p0
.end method

.method public unit_metadata(Lcom/squareup/protos/client/invoice/UnitMetadata;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    return-object p0
.end method
