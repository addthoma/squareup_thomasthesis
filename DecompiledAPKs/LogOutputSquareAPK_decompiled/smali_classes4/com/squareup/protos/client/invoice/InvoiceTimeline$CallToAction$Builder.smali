.class public final Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InvoiceTimeline.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;",
        "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

.field public target:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

.field public target_url:Ljava/lang/String;

.field public text:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 484
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;
    .locals 7

    .line 509
    new-instance v6, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->target:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->text:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->target_url:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;-><init>(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 475
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    move-result-object v0

    return-object v0
.end method

.method public call_to_action_link(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;)Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;
    .locals 0

    .line 503
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    return-object p0
.end method

.method public target(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;)Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;
    .locals 0

    .line 488
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->target:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    return-object p0
.end method

.method public target_url(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;
    .locals 0

    .line 498
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->target_url:Ljava/lang/String;

    return-object p0
.end method

.method public text(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;
    .locals 0

    .line 493
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction$Builder;->text:Ljava/lang/String;

    return-object p0
.end method
