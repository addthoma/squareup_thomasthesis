.class public final Lcom/squareup/protos/client/retail/inventory/Hours$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Hours.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/retail/inventory/Hours;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/retail/inventory/Hours;",
        "Lcom/squareup/protos/client/retail/inventory/Hours$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public day_of_week:Ljava/lang/Integer;

.field public open_intervals:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/retail/inventory/Hours$Interval;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 102
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 103
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Builder;->open_intervals:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/retail/inventory/Hours;
    .locals 4

    .line 122
    new-instance v0, Lcom/squareup/protos/client/retail/inventory/Hours;

    iget-object v1, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Builder;->day_of_week:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Builder;->open_intervals:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/retail/inventory/Hours;-><init>(Ljava/lang/Integer;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/client/retail/inventory/Hours$Builder;->build()Lcom/squareup/protos/client/retail/inventory/Hours;

    move-result-object v0

    return-object v0
.end method

.method public day_of_week(Ljava/lang/Integer;)Lcom/squareup/protos/client/retail/inventory/Hours$Builder;
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Builder;->day_of_week:Ljava/lang/Integer;

    return-object p0
.end method

.method public open_intervals(Ljava/util/List;)Lcom/squareup/protos/client/retail/inventory/Hours$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/retail/inventory/Hours$Interval;",
            ">;)",
            "Lcom/squareup/protos/client/retail/inventory/Hours$Builder;"
        }
    .end annotation

    .line 115
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/retail/inventory/Hours$Builder;->open_intervals:Ljava/util/List;

    return-object p0
.end method
