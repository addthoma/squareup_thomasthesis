.class public final Lcom/squareup/protos/client/Merchant$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Merchant.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/Merchant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/Merchant;",
        "Lcom/squareup/protos/client/Merchant$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public merchant_token:Ljava/lang/String;

.field public read_only_business_name:Ljava/lang/String;

.field public read_only_photo_url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 118
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/Merchant;
    .locals 5

    .line 141
    new-instance v0, Lcom/squareup/protos/client/Merchant;

    iget-object v1, p0, Lcom/squareup/protos/client/Merchant$Builder;->merchant_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/Merchant$Builder;->read_only_photo_url:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/Merchant$Builder;->read_only_business_name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/Merchant;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/client/Merchant$Builder;->build()Lcom/squareup/protos/client/Merchant;

    move-result-object v0

    return-object v0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/Merchant$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/protos/client/Merchant$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public read_only_business_name(Ljava/lang/String;)Lcom/squareup/protos/client/Merchant$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/client/Merchant$Builder;->read_only_business_name:Ljava/lang/String;

    return-object p0
.end method

.method public read_only_photo_url(Ljava/lang/String;)Lcom/squareup/protos/client/Merchant$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/client/Merchant$Builder;->read_only_photo_url:Ljava/lang/String;

    return-object p0
.end method
