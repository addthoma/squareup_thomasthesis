.class final Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$ProtoAdapter_GetBalanceSummaryResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetBalanceSummaryResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetBalanceSummaryResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 668
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 703
    new-instance v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;-><init>()V

    .line 704
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 705
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 718
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 716
    :pswitch_0
    iget-object v3, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->eligibility_details:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/deposits/EligibilityDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 715
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->balance_information(Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;

    goto :goto_0

    .line 714
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->deposit_activity(Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;

    goto :goto_0

    .line 713
    :pswitch_3
    iget-object v3, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->card_activity:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 712
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->allow_partial_deposit(Ljava/lang/Boolean;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;

    goto :goto_0

    .line 711
    :pswitch_5
    iget-object v3, v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->square_card:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 710
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/deposits/CardInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/deposits/CardInfo;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->linked_card(Lcom/squareup/protos/client/deposits/CardInfo;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;

    goto :goto_0

    .line 709
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->linked_bank_account(Lcom/squareup/protos/client/bankaccount/BankAccount;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;

    goto :goto_0

    .line 708
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->instant_deposit_details(Lcom/squareup/protos/client/deposits/InstantDepositDetails;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;

    goto :goto_0

    .line 707
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->balance(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;

    goto/16 :goto_0

    .line 722
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 723
    invoke-virtual {v0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->build()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 666
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$ProtoAdapter_GetBalanceSummaryResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 688
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 689
    sget-object v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 690
    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 691
    sget-object v0, Lcom/squareup/protos/client/deposits/CardInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_card:Lcom/squareup/protos/client/deposits/CardInfo;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 692
    sget-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->square_card:Ljava/util/List;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 693
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->allow_partial_deposit:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 694
    sget-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->card_activity:Ljava/util/List;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 695
    sget-object v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->deposit_activity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 696
    sget-object v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance_information:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 697
    sget-object v0, Lcom/squareup/protos/client/deposits/EligibilityDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->eligibility_details:Ljava/util/List;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 698
    invoke-virtual {p2}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 666
    check-cast p2, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$ProtoAdapter_GetBalanceSummaryResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;)I
    .locals 4

    .line 673
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    const/4 v3, 0x2

    .line 674
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    const/4 v3, 0x3

    .line 675
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/deposits/CardInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->linked_card:Lcom/squareup/protos/client/deposits/CardInfo;

    const/4 v3, 0x4

    .line 676
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 677
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->square_card:Ljava/util/List;

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->allow_partial_deposit:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 678
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 679
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->card_activity:Ljava/util/List;

    const/4 v3, 0x7

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->deposit_activity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    const/16 v3, 0x8

    .line 680
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->balance_information:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    const/16 v3, 0x9

    .line 681
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/deposits/EligibilityDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 682
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->eligibility_details:Ljava/util/List;

    const/16 v3, 0xa

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 683
    invoke-virtual {p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 666
    check-cast p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$ProtoAdapter_GetBalanceSummaryResponse;->encodedSize(Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;
    .locals 2

    .line 728
    invoke-virtual {p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;->newBuilder()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;

    move-result-object p1

    .line 729
    iget-object v0, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->balance:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->balance:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->balance:Lcom/squareup/protos/common/Money;

    .line 730
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    .line 731
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->linked_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->linked_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bankaccount/BankAccount;

    iput-object v0, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->linked_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    .line 732
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->linked_card:Lcom/squareup/protos/client/deposits/CardInfo;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/deposits/CardInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->linked_card:Lcom/squareup/protos/client/deposits/CardInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/deposits/CardInfo;

    iput-object v0, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->linked_card:Lcom/squareup/protos/client/deposits/CardInfo;

    .line 733
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->square_card:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 734
    iget-object v0, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->card_activity:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 735
    iget-object v0, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->deposit_activity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->deposit_activity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    iput-object v0, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->deposit_activity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    .line 736
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->balance_information:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->balance_information:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    iput-object v0, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->balance_information:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    .line 737
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->eligibility_details:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/deposits/EligibilityDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 738
    invoke-virtual {p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 739
    invoke-virtual {p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->build()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 666
    check-cast p1, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$ProtoAdapter_GetBalanceSummaryResponse;->redact(Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    move-result-object p1

    return-object p1
.end method
