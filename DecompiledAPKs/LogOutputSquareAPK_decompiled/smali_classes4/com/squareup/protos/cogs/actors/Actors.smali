.class public final enum Lcom/squareup/protos/cogs/actors/Actors;
.super Ljava/lang/Enum;
.source "Actors.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/cogs/actors/Actors$ProtoAdapter_Actors;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/cogs/actors/Actors;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/cogs/actors/Actors;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/cogs/actors/Actors;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum PROMO:Lcom/squareup/protos/cogs/actors/Actors;

.field public static final enum USER:Lcom/squareup/protos/cogs/actors/Actors;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 11
    new-instance v0, Lcom/squareup/protos/cogs/actors/Actors;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "USER"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/cogs/actors/Actors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/cogs/actors/Actors;->USER:Lcom/squareup/protos/cogs/actors/Actors;

    .line 13
    new-instance v0, Lcom/squareup/protos/cogs/actors/Actors;

    const/4 v3, 0x2

    const-string v4, "PROMO"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/cogs/actors/Actors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/cogs/actors/Actors;->PROMO:Lcom/squareup/protos/cogs/actors/Actors;

    new-array v0, v3, [Lcom/squareup/protos/cogs/actors/Actors;

    .line 10
    sget-object v3, Lcom/squareup/protos/cogs/actors/Actors;->USER:Lcom/squareup/protos/cogs/actors/Actors;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/cogs/actors/Actors;->PROMO:Lcom/squareup/protos/cogs/actors/Actors;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/cogs/actors/Actors;->$VALUES:[Lcom/squareup/protos/cogs/actors/Actors;

    .line 15
    new-instance v0, Lcom/squareup/protos/cogs/actors/Actors$ProtoAdapter_Actors;

    invoke-direct {v0}, Lcom/squareup/protos/cogs/actors/Actors$ProtoAdapter_Actors;-><init>()V

    sput-object v0, Lcom/squareup/protos/cogs/actors/Actors;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput p3, p0, Lcom/squareup/protos/cogs/actors/Actors;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/cogs/actors/Actors;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 29
    :cond_0
    sget-object p0, Lcom/squareup/protos/cogs/actors/Actors;->PROMO:Lcom/squareup/protos/cogs/actors/Actors;

    return-object p0

    .line 28
    :cond_1
    sget-object p0, Lcom/squareup/protos/cogs/actors/Actors;->USER:Lcom/squareup/protos/cogs/actors/Actors;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/cogs/actors/Actors;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/cogs/actors/Actors;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/cogs/actors/Actors;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/cogs/actors/Actors;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/cogs/actors/Actors;->$VALUES:[Lcom/squareup/protos/cogs/actors/Actors;

    invoke-virtual {v0}, [Lcom/squareup/protos/cogs/actors/Actors;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/cogs/actors/Actors;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 36
    iget v0, p0, Lcom/squareup/protos/cogs/actors/Actors;->value:I

    return v0
.end method
