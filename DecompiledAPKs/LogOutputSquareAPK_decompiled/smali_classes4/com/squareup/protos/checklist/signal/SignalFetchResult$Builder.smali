.class public final Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SignalFetchResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/checklist/signal/SignalFetchResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/checklist/signal/SignalFetchResult;",
        "Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public created_at:Lcom/squareup/protos/common/time/DateTime;

.field public identifier:Ljava/lang/String;

.field public signal_name:Lcom/squareup/protos/checklist/signal/SignalName;

.field public signal_value:Lcom/squareup/protos/checklist/signal/SignalValue;

.field public updated_at:Lcom/squareup/protos/common/time/DateTime;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 155
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/checklist/signal/SignalFetchResult;
    .locals 8

    .line 201
    new-instance v7, Lcom/squareup/protos/checklist/signal/SignalFetchResult;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->identifier:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->signal_name:Lcom/squareup/protos/checklist/signal/SignalName;

    iget-object v3, p0, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->signal_value:Lcom/squareup/protos/checklist/signal/SignalValue;

    iget-object v4, p0, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v5, p0, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/checklist/signal/SignalFetchResult;-><init>(Ljava/lang/String;Lcom/squareup/protos/checklist/signal/SignalName;Lcom/squareup/protos/checklist/signal/SignalValue;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 144
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->build()Lcom/squareup/protos/checklist/signal/SignalFetchResult;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public identifier(Ljava/lang/String;)Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->identifier:Ljava/lang/String;

    return-object p0
.end method

.method public signal_name(Lcom/squareup/protos/checklist/signal/SignalName;)Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->signal_name:Lcom/squareup/protos/checklist/signal/SignalName;

    return-object p0
.end method

.method public signal_value(Lcom/squareup/protos/checklist/signal/SignalValue;)Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->signal_value:Lcom/squareup/protos/checklist/signal/SignalValue;

    return-object p0
.end method

.method public updated_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;
    .locals 0

    .line 195
    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method
