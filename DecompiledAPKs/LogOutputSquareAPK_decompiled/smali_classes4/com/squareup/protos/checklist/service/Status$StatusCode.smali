.class public final enum Lcom/squareup/protos/checklist/service/Status$StatusCode;
.super Ljava/lang/Enum;
.source "Status.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/checklist/service/Status;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "StatusCode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/checklist/service/Status$StatusCode$ProtoAdapter_StatusCode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/checklist/service/Status$StatusCode;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/checklist/service/Status$StatusCode;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/checklist/service/Status$StatusCode;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/checklist/service/Status$StatusCode;

.field public static final enum INVALID_ACTION_ITEM:Lcom/squareup/protos/checklist/service/Status$StatusCode;

.field public static final enum UNAUTHORIZED:Lcom/squareup/protos/checklist/service/Status$StatusCode;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 117
    new-instance v0, Lcom/squareup/protos/checklist/service/Status$StatusCode;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/checklist/service/Status$StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/service/Status$StatusCode;->DO_NOT_USE:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    .line 122
    new-instance v0, Lcom/squareup/protos/checklist/service/Status$StatusCode;

    const/4 v2, 0x1

    const-string v3, "UNAUTHORIZED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/checklist/service/Status$StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/service/Status$StatusCode;->UNAUTHORIZED:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    .line 127
    new-instance v0, Lcom/squareup/protos/checklist/service/Status$StatusCode;

    const/4 v3, 0x2

    const-string v4, "INVALID_ACTION_ITEM"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/checklist/service/Status$StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/service/Status$StatusCode;->INVALID_ACTION_ITEM:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/checklist/service/Status$StatusCode;

    .line 116
    sget-object v4, Lcom/squareup/protos/checklist/service/Status$StatusCode;->DO_NOT_USE:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/checklist/service/Status$StatusCode;->UNAUTHORIZED:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/service/Status$StatusCode;->INVALID_ACTION_ITEM:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/checklist/service/Status$StatusCode;->$VALUES:[Lcom/squareup/protos/checklist/service/Status$StatusCode;

    .line 129
    new-instance v0, Lcom/squareup/protos/checklist/service/Status$StatusCode$ProtoAdapter_StatusCode;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/service/Status$StatusCode$ProtoAdapter_StatusCode;-><init>()V

    sput-object v0, Lcom/squareup/protos/checklist/service/Status$StatusCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 133
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 134
    iput p3, p0, Lcom/squareup/protos/checklist/service/Status$StatusCode;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/checklist/service/Status$StatusCode;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 144
    :cond_0
    sget-object p0, Lcom/squareup/protos/checklist/service/Status$StatusCode;->INVALID_ACTION_ITEM:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    return-object p0

    .line 143
    :cond_1
    sget-object p0, Lcom/squareup/protos/checklist/service/Status$StatusCode;->UNAUTHORIZED:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    return-object p0

    .line 142
    :cond_2
    sget-object p0, Lcom/squareup/protos/checklist/service/Status$StatusCode;->DO_NOT_USE:Lcom/squareup/protos/checklist/service/Status$StatusCode;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/checklist/service/Status$StatusCode;
    .locals 1

    .line 116
    const-class v0, Lcom/squareup/protos/checklist/service/Status$StatusCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/checklist/service/Status$StatusCode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/checklist/service/Status$StatusCode;
    .locals 1

    .line 116
    sget-object v0, Lcom/squareup/protos/checklist/service/Status$StatusCode;->$VALUES:[Lcom/squareup/protos/checklist/service/Status$StatusCode;

    invoke-virtual {v0}, [Lcom/squareup/protos/checklist/service/Status$StatusCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/checklist/service/Status$StatusCode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 151
    iget v0, p0, Lcom/squareup/protos/checklist/service/Status$StatusCode;->value:I

    return v0
.end method
