.class public final Lcom/squareup/protos/common/client/Version;
.super Lcom/squareup/wire/Message;
.source "Version.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/client/Version$ProtoAdapter_Version;,
        Lcom/squareup/protos/common/client/Version$PrereleaseType;,
        Lcom/squareup/protos/common/client/Version$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/common/client/Version;",
        "Lcom/squareup/protos/common/client/Version$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/client/Version;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BUILD_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_MAJOR:Ljava/lang/Integer;

.field public static final DEFAULT_MINOR:Ljava/lang/Integer;

.field public static final DEFAULT_PRERELEASE:Ljava/lang/Integer;

.field public static final DEFAULT_PRERELEASE_TYPE:Lcom/squareup/protos/common/client/Version$PrereleaseType;

.field public static final DEFAULT_REVISION:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final build_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final major:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x1
    .end annotation
.end field

.field public final minor:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x2
    .end annotation
.end field

.field public final prerelease:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x5
    .end annotation
.end field

.field public final prerelease_type:Lcom/squareup/protos/common/client/Version$PrereleaseType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.client.Version$PrereleaseType#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final revision:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 39
    new-instance v0, Lcom/squareup/protos/common/client/Version$ProtoAdapter_Version;

    invoke-direct {v0}, Lcom/squareup/protos/common/client/Version$ProtoAdapter_Version;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/client/Version;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 43
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/common/client/Version;->DEFAULT_MAJOR:Ljava/lang/Integer;

    .line 45
    sput-object v0, Lcom/squareup/protos/common/client/Version;->DEFAULT_MINOR:Ljava/lang/Integer;

    .line 47
    sput-object v0, Lcom/squareup/protos/common/client/Version;->DEFAULT_REVISION:Ljava/lang/Integer;

    .line 49
    sget-object v1, Lcom/squareup/protos/common/client/Version$PrereleaseType;->ALPHA:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    sput-object v1, Lcom/squareup/protos/common/client/Version;->DEFAULT_PRERELEASE_TYPE:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    .line 51
    sput-object v0, Lcom/squareup/protos/common/client/Version;->DEFAULT_PRERELEASE:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/protos/common/client/Version$PrereleaseType;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 8

    .line 93
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/common/client/Version;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/protos/common/client/Version$PrereleaseType;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/protos/common/client/Version$PrereleaseType;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 98
    sget-object v0, Lcom/squareup/protos/common/client/Version;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 99
    iput-object p1, p0, Lcom/squareup/protos/common/client/Version;->major:Ljava/lang/Integer;

    .line 100
    iput-object p2, p0, Lcom/squareup/protos/common/client/Version;->minor:Ljava/lang/Integer;

    .line 101
    iput-object p3, p0, Lcom/squareup/protos/common/client/Version;->revision:Ljava/lang/Integer;

    .line 102
    iput-object p4, p0, Lcom/squareup/protos/common/client/Version;->prerelease_type:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    .line 103
    iput-object p5, p0, Lcom/squareup/protos/common/client/Version;->prerelease:Ljava/lang/Integer;

    .line 104
    iput-object p6, p0, Lcom/squareup/protos/common/client/Version;->build_id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 123
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/common/client/Version;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 124
    :cond_1
    check-cast p1, Lcom/squareup/protos/common/client/Version;

    .line 125
    invoke-virtual {p0}, Lcom/squareup/protos/common/client/Version;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/common/client/Version;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->major:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/common/client/Version;->major:Ljava/lang/Integer;

    .line 126
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->minor:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/common/client/Version;->minor:Ljava/lang/Integer;

    .line 127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->revision:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/common/client/Version;->revision:Ljava/lang/Integer;

    .line 128
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->prerelease_type:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    iget-object v3, p1, Lcom/squareup/protos/common/client/Version;->prerelease_type:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    .line 129
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->prerelease:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/common/client/Version;->prerelease:Ljava/lang/Integer;

    .line 130
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->build_id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/common/client/Version;->build_id:Ljava/lang/String;

    .line 131
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 136
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 138
    invoke-virtual {p0}, Lcom/squareup/protos/common/client/Version;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->major:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->minor:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->revision:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->prerelease_type:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/client/Version$PrereleaseType;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->prerelease:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->build_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 145
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/common/client/Version$Builder;
    .locals 2

    .line 109
    new-instance v0, Lcom/squareup/protos/common/client/Version$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/client/Version$Builder;-><init>()V

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->major:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Version$Builder;->major:Ljava/lang/Integer;

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->minor:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Version$Builder;->minor:Ljava/lang/Integer;

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->revision:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Version$Builder;->revision:Ljava/lang/Integer;

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->prerelease_type:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Version$Builder;->prerelease_type:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->prerelease:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Version$Builder;->prerelease:Ljava/lang/Integer;

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->build_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Version$Builder;->build_id:Ljava/lang/String;

    .line 116
    invoke-virtual {p0}, Lcom/squareup/protos/common/client/Version;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/client/Version$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/squareup/protos/common/client/Version;->newBuilder()Lcom/squareup/protos/common/client/Version$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->major:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", major="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->major:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 154
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->minor:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", minor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->minor:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 155
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->revision:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", revision="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->revision:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 156
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->prerelease_type:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    if-eqz v1, :cond_3

    const-string v1, ", prerelease_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->prerelease_type:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 157
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->prerelease:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", prerelease="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->prerelease:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 158
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->build_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", build_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Version;->build_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Version{"

    .line 159
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
