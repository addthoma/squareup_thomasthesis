.class public final Lcom/squareup/protos/common/location/GlobalAddress$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GlobalAddress.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/location/GlobalAddress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/location/GlobalAddress;",
        "Lcom/squareup/protos/common/location/GlobalAddress$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public address_coordinates:Lcom/squareup/protos/common/location/Coordinates;

.field public address_line_1:Ljava/lang/String;

.field public address_line_2:Ljava/lang/String;

.field public address_line_3:Ljava/lang/String;

.field public address_line_4:Ljava/lang/String;

.field public address_line_5:Ljava/lang/String;

.field public administrative_district_level_1:Ljava/lang/String;

.field public administrative_district_level_2:Ljava/lang/String;

.field public administrative_district_level_3:Ljava/lang/String;

.field public country_code:Lcom/squareup/protos/common/countries/Country;

.field public locality:Ljava/lang/String;

.field public postal_code:Ljava/lang/String;

.field public sublocality:Ljava/lang/String;

.field public sublocality_1:Ljava/lang/String;

.field public sublocality_2:Ljava/lang/String;

.field public sublocality_3:Ljava/lang/String;

.field public sublocality_4:Ljava/lang/String;

.field public sublocality_5:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 365
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public address_coordinates(Lcom/squareup/protos/common/location/Coordinates;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 0

    .line 481
    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_coordinates:Lcom/squareup/protos/common/location/Coordinates;

    return-object p0
.end method

.method public address_line_1(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 0

    .line 384
    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_line_1:Ljava/lang/String;

    return-object p0
.end method

.method public address_line_2(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 0

    .line 389
    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_line_2:Ljava/lang/String;

    return-object p0
.end method

.method public address_line_3(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 0

    .line 394
    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_line_3:Ljava/lang/String;

    return-object p0
.end method

.method public address_line_4(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 0

    .line 399
    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_line_4:Ljava/lang/String;

    return-object p0
.end method

.method public address_line_5(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 0

    .line 404
    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->address_line_5:Ljava/lang/String;

    return-object p0
.end method

.method public administrative_district_level_1(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 0

    .line 453
    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->administrative_district_level_1:Ljava/lang/String;

    return-object p0
.end method

.method public administrative_district_level_2(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 0

    .line 461
    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->administrative_district_level_2:Ljava/lang/String;

    return-object p0
.end method

.method public administrative_district_level_3(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 0

    .line 466
    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->administrative_district_level_3:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/common/location/GlobalAddress;
    .locals 2

    .line 487
    new-instance v0, Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/common/location/GlobalAddress;-><init>(Lcom/squareup/protos/common/location/GlobalAddress$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 328
    invoke-virtual {p0}, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->build()Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object v0

    return-object v0
.end method

.method public country_code(Lcom/squareup/protos/common/countries/Country;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 0

    .line 476
    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->country_code:Lcom/squareup/protos/common/countries/Country;

    return-object p0
.end method

.method public locality(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 0

    .line 415
    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->locality:Ljava/lang/String;

    return-object p0
.end method

.method public postal_code(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 0

    .line 471
    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->postal_code:Ljava/lang/String;

    return-object p0
.end method

.method public sublocality(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 0

    .line 420
    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->sublocality:Ljava/lang/String;

    return-object p0
.end method

.method public sublocality_1(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 0

    .line 425
    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->sublocality_1:Ljava/lang/String;

    return-object p0
.end method

.method public sublocality_2(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 0

    .line 430
    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->sublocality_2:Ljava/lang/String;

    return-object p0
.end method

.method public sublocality_3(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 0

    .line 435
    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->sublocality_3:Ljava/lang/String;

    return-object p0
.end method

.method public sublocality_4(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 0

    .line 440
    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->sublocality_4:Ljava/lang/String;

    return-object p0
.end method

.method public sublocality_5(Ljava/lang/String;)Lcom/squareup/protos/common/location/GlobalAddress$Builder;
    .locals 0

    .line 445
    iput-object p1, p0, Lcom/squareup/protos/common/location/GlobalAddress$Builder;->sublocality_5:Ljava/lang/String;

    return-object p0
.end method
