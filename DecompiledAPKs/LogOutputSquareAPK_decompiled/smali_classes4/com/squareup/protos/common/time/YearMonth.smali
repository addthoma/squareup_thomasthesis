.class public final Lcom/squareup/protos/common/time/YearMonth;
.super Lcom/squareup/wire/Message;
.source "YearMonth.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/time/YearMonth$ProtoAdapter_YearMonth;,
        Lcom/squareup/protos/common/time/YearMonth$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/common/time/YearMonth;",
        "Lcom/squareup/protos/common/time/YearMonth$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/time/YearMonth;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MONTH_OF_YEAR:Ljava/lang/Integer;

.field public static final DEFAULT_YEAR:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final month_of_year:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field

.field public final year:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/protos/common/time/YearMonth$ProtoAdapter_YearMonth;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/YearMonth$ProtoAdapter_YearMonth;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/time/YearMonth;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 38
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/common/time/YearMonth;->DEFAULT_YEAR:Ljava/lang/Integer;

    .line 40
    sput-object v0, Lcom/squareup/protos/common/time/YearMonth;->DEFAULT_MONTH_OF_YEAR:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1

    .line 61
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/common/time/YearMonth;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 65
    sget-object v0, Lcom/squareup/protos/common/time/YearMonth;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 66
    iput-object p1, p0, Lcom/squareup/protos/common/time/YearMonth;->year:Ljava/lang/Integer;

    .line 67
    iput-object p2, p0, Lcom/squareup/protos/common/time/YearMonth;->month_of_year:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 82
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/common/time/YearMonth;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 83
    :cond_1
    check-cast p1, Lcom/squareup/protos/common/time/YearMonth;

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/YearMonth;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/common/time/YearMonth;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/time/YearMonth;->year:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/common/time/YearMonth;->year:Ljava/lang/Integer;

    .line 85
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/time/YearMonth;->month_of_year:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/common/time/YearMonth;->month_of_year:Ljava/lang/Integer;

    .line 86
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 91
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/YearMonth;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/common/time/YearMonth;->year:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/common/time/YearMonth;->month_of_year:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 96
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/common/time/YearMonth$Builder;
    .locals 2

    .line 72
    new-instance v0, Lcom/squareup/protos/common/time/YearMonth$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/YearMonth$Builder;-><init>()V

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/common/time/YearMonth;->year:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/common/time/YearMonth$Builder;->year:Ljava/lang/Integer;

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/common/time/YearMonth;->month_of_year:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/common/time/YearMonth$Builder;->month_of_year:Ljava/lang/Integer;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/YearMonth;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/time/YearMonth$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/YearMonth;->newBuilder()Lcom/squareup/protos/common/time/YearMonth$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/common/time/YearMonth;->year:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", year="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/time/YearMonth;->year:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 105
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/common/time/YearMonth;->month_of_year:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", month_of_year="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/time/YearMonth;->month_of_year:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "YearMonth{"

    .line 106
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
