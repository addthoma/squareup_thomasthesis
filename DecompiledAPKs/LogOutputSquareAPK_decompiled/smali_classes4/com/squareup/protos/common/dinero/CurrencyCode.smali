.class public final enum Lcom/squareup/protos/common/dinero/CurrencyCode;
.super Ljava/lang/Enum;
.source "CurrencyCode.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/dinero/CurrencyCode$ProtoAdapter_CurrencyCode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/common/dinero/CurrencyCode;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/dinero/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AED:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum AFN:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum ALL:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum AMD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum ANG:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum AOA:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum ARS:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum AUD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum AWG:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum AZN:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum BAM:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum BBD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum BDT:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum BGN:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum BHD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum BIF:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum BMD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum BND:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum BOB:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum BOV:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum BRL:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum BSD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum BTN:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum BWP:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum BYR:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum BZD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum CAD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum CDF:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum CHE:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum CHF:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum CHW:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum CLF:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum CLP:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum CNY:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum COP:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum COU:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum CRC:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum CUC:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum CUP:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum CVE:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum CZK:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum DJF:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum DKK:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum DOP:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum DZD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum EGP:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum ERN:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum ETB:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum EUR:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum FJD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum FKP:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum GBP:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum GEL:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum GHS:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum GIP:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum GMD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum GNF:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum GTQ:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum GYD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum HKD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum HNL:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum HRK:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum HTG:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum HUF:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum IDR:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum ILS:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum INR:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum IQD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum IRR:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum ISK:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum JMD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum JOD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum JPY:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum KES:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum KGS:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum KHR:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum KMF:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum KPW:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum KRW:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum KWD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum KYD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum KZT:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum LAK:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum LBP:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum LKR:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum LRD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum LSL:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum LTL:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum LVL:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum LYD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum MAD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum MDL:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum MGA:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum MKD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum MMK:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum MNT:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum MOP:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum MRO:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum MUR:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum MVR:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum MWK:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum MXN:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum MXV:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum MYR:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum MZN:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum NAD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum NGN:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum NIO:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum NOK:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum NPR:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum NZD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum OMR:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum PAB:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum PEN:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum PGK:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum PHP:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum PKR:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum PLN:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum PYG:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum QAR:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum RON:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum RSD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum RUB:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum RWF:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum SAR:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum SBD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum SCR:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum SDG:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum SEK:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum SGD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum SHP:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum SLL:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum SOS:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum SRD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum SSP:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum STD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum SVC:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum SYP:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum SZL:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum THB:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum TJS:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum TMT:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum TND:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum TOP:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum TRY:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum TTD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum TWD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum TZS:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum UAH:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum UGX:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum USD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum USN:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum USS:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum UYI:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum UYU:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum UZS:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum VEF:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum VND:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum VUV:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum WST:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum XAF:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum XAG:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum XAU:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum XBA:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum XBB:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum XBC:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum XBD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum XCD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum XDR:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum XOF:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum XPD:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum XPF:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum XPT:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum XTS:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum XXX:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum YER:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum ZAR:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum ZMK:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final enum ZMW:Lcom/squareup/protos/common/dinero/CurrencyCode;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 16
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v1, "AED"

    const/4 v2, 0x0

    const/16 v3, 0x310

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->AED:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 18
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v1, "AFN"

    const/4 v2, 0x1

    const/16 v3, 0x3cb

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->AFN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 20
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v1, 0x8

    const-string v2, "ALL"

    const/4 v3, 0x2

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ALL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 22
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x33

    const-string v3, "AMD"

    const/4 v4, 0x3

    invoke-direct {v0, v3, v4, v2}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->AMD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 24
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v3, "ANG"

    const/4 v4, 0x4

    const/16 v5, 0x214

    invoke-direct {v0, v3, v4, v5}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ANG:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 26
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v3, "AOA"

    const/4 v4, 0x5

    const/16 v5, 0x3cd

    invoke-direct {v0, v3, v4, v5}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->AOA:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 28
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v3, 0x20

    const-string v4, "ARS"

    const/4 v5, 0x6

    invoke-direct {v0, v4, v5, v3}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ARS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 30
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v4, 0x24

    const-string v5, "AUD"

    const/4 v6, 0x7

    invoke-direct {v0, v5, v6, v4}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->AUD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 32
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v5, "AWG"

    const/16 v6, 0x215

    invoke-direct {v0, v5, v1, v6}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->AWG:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 34
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v5, "AZN"

    const/16 v6, 0x9

    const/16 v7, 0x3b0

    invoke-direct {v0, v5, v6, v7}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->AZN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 36
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v5, "BAM"

    const/16 v6, 0xa

    const/16 v7, 0x3d1

    invoke-direct {v0, v5, v6, v7}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BAM:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 38
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v5, 0x34

    const-string v6, "BBD"

    const/16 v7, 0xb

    invoke-direct {v0, v6, v7, v5}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BBD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 40
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v6, 0x32

    const/16 v7, 0xc

    const-string v8, "BDT"

    invoke-direct {v0, v8, v7, v6}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BDT:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 42
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v8, "BGN"

    const/16 v9, 0xd

    const/16 v10, 0x3cf

    invoke-direct {v0, v8, v9, v10}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BGN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 44
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v8, 0x30

    const-string v9, "BHD"

    const/16 v10, 0xe

    invoke-direct {v0, v9, v10, v8}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BHD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 46
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0x6c

    const-string v10, "BIF"

    const/16 v11, 0xf

    invoke-direct {v0, v10, v11, v9}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BIF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 48
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v10, 0x3c

    const-string v11, "BMD"

    const/16 v12, 0x10

    invoke-direct {v0, v11, v12, v10}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BMD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 50
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v11, 0x60

    const-string v12, "BND"

    const/16 v13, 0x11

    invoke-direct {v0, v12, v13, v11}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BND:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 52
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v12, 0x44

    const-string v13, "BOB"

    const/16 v14, 0x12

    invoke-direct {v0, v13, v14, v12}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BOB:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 54
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v13, "BOV"

    const/16 v14, 0x13

    const/16 v15, 0x3d8

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BOV:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 56
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v13, "BRL"

    const/16 v14, 0x14

    const/16 v15, 0x3da

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BRL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 58
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v13, 0x2c

    const-string v14, "BSD"

    const/16 v15, 0x15

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BSD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 60
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v14, 0x40

    const-string v15, "BTN"

    const/16 v1, 0x16

    invoke-direct {v0, v15, v1, v14}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BTN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 62
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v1, 0x48

    const-string v15, "BWP"

    const/16 v9, 0x17

    invoke-direct {v0, v15, v9, v1}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BWP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 64
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "BYR"

    const/16 v15, 0x18

    const/16 v11, 0x3ce

    invoke-direct {v0, v9, v15, v11}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BYR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 66
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "BZD"

    const/16 v11, 0x19

    const/16 v15, 0x54

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BZD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 68
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "CAD"

    const/16 v11, 0x1a

    const/16 v15, 0x7c

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CAD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 70
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "CDF"

    const/16 v11, 0x1b

    const/16 v15, 0x3d0

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CDF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 72
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "CHE"

    const/16 v11, 0x1c

    const/16 v15, 0x3b3

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CHE:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 74
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "CHF"

    const/16 v11, 0x1d

    const/16 v15, 0x2f4

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CHF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 76
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "CHW"

    const/16 v11, 0x1e

    const/16 v15, 0x3b4

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CHW:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 78
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "CLF"

    const/16 v11, 0x1f

    const/16 v15, 0x3de

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CLF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 80
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "CLP"

    const/16 v11, 0x98

    invoke-direct {v0, v9, v3, v11}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CLP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 82
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "CNY"

    const/16 v11, 0x21

    const/16 v15, 0x9c

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CNY:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 84
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "COP"

    const/16 v11, 0x22

    const/16 v15, 0xaa

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->COP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 86
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "COU"

    const/16 v11, 0x23

    const/16 v15, 0x3ca

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->COU:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 88
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "CRC"

    const/16 v11, 0xbc

    invoke-direct {v0, v9, v4, v11}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CRC:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 90
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "CUC"

    const/16 v11, 0x25

    const/16 v15, 0x3a3

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CUC:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 92
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "CUP"

    const/16 v11, 0x26

    const/16 v15, 0xc0

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CUP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 94
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "CVE"

    const/16 v11, 0x27

    const/16 v15, 0x84

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CVE:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 96
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "CZK"

    const/16 v11, 0x28

    const/16 v15, 0xcb

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CZK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 98
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "DJF"

    const/16 v11, 0x29

    const/16 v15, 0x106

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->DJF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 100
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "DKK"

    const/16 v11, 0x2a

    const/16 v15, 0xd0

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->DKK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 102
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "DOP"

    const/16 v11, 0x2b

    const/16 v15, 0xd6

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->DOP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 104
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "DZD"

    invoke-direct {v0, v9, v13, v7}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->DZD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 106
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "EGP"

    const/16 v11, 0x2d

    const/16 v15, 0x332

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->EGP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 108
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "ERN"

    const/16 v11, 0x2e

    const/16 v15, 0xe8

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ERN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 110
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "ETB"

    const/16 v11, 0x2f

    const/16 v15, 0xe6

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ETB:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 112
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "EUR"

    const/16 v11, 0x3d2

    invoke-direct {v0, v9, v8, v11}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->EUR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 114
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "FJD"

    const/16 v11, 0x31

    const/16 v15, 0xf2

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->FJD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 116
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "FKP"

    const/16 v11, 0xee

    invoke-direct {v0, v9, v6, v11}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->FKP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 118
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "GBP"

    const/16 v11, 0x33a

    invoke-direct {v0, v9, v2, v11}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->GBP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 120
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "GEL"

    const/16 v11, 0x3d5

    invoke-direct {v0, v9, v5, v11}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->GEL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 122
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "GHS"

    const/16 v11, 0x35

    const/16 v15, 0x3a8

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->GHS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 124
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "GIP"

    const/16 v11, 0x36

    const/16 v15, 0x124

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->GIP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 126
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "GMD"

    const/16 v11, 0x37

    const/16 v15, 0x10e

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->GMD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 128
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "GNF"

    const/16 v11, 0x38

    const/16 v15, 0x144

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->GNF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 130
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "GTQ"

    const/16 v11, 0x39

    const/16 v15, 0x140

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->GTQ:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 132
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "GYD"

    const/16 v11, 0x3a

    const/16 v15, 0x148

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->GYD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 134
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "HKD"

    const/16 v11, 0x3b

    const/16 v15, 0x158

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->HKD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 136
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "HNL"

    const/16 v11, 0x154

    invoke-direct {v0, v9, v10, v11}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->HNL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 138
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "HRK"

    const/16 v11, 0x3d

    const/16 v15, 0xbf

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->HRK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 140
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "HTG"

    const/16 v11, 0x3e

    const/16 v15, 0x14c

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->HTG:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 142
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "HUF"

    const/16 v11, 0x3f

    const/16 v15, 0x15c

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->HUF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 144
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "IDR"

    const/16 v11, 0x168

    invoke-direct {v0, v9, v14, v11}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->IDR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 146
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "ILS"

    const/16 v11, 0x41

    const/16 v15, 0x178

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ILS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 148
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "INR"

    const/16 v11, 0x42

    const/16 v15, 0x164

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->INR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 150
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "IQD"

    const/16 v11, 0x43

    const/16 v15, 0x170

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->IQD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 152
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "IRR"

    const/16 v11, 0x16c

    invoke-direct {v0, v9, v12, v11}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->IRR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 154
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "ISK"

    const/16 v11, 0x45

    const/16 v15, 0x160

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ISK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 156
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "JMD"

    const/16 v11, 0x46

    const/16 v15, 0x184

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->JMD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 158
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "JOD"

    const/16 v11, 0x47

    const/16 v15, 0x190

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->JOD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 160
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "JPY"

    const/16 v11, 0x188

    invoke-direct {v0, v9, v1, v11}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->JPY:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 162
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "KES"

    const/16 v11, 0x49

    const/16 v15, 0x194

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->KES:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 164
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "KGS"

    const/16 v11, 0x4a

    const/16 v15, 0x1a1

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->KGS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 166
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "KHR"

    const/16 v11, 0x4b

    const/16 v15, 0x74

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->KHR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 168
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "KMF"

    const/16 v11, 0x4c

    const/16 v15, 0xae

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->KMF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 170
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "KPW"

    const/16 v11, 0x4d

    const/16 v15, 0x198

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->KPW:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 172
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "KRW"

    const/16 v11, 0x4e

    const/16 v15, 0x19a

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->KRW:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 174
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "KWD"

    const/16 v11, 0x4f

    const/16 v15, 0x19e

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->KWD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 176
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "KYD"

    const/16 v11, 0x50

    const/16 v15, 0x88

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->KYD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 178
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "KZT"

    const/16 v11, 0x51

    const/16 v15, 0x18e

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->KZT:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 180
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "LAK"

    const/16 v11, 0x52

    const/16 v15, 0x1a2

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->LAK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 182
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "LBP"

    const/16 v11, 0x53

    const/16 v15, 0x1a6

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->LBP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 184
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "LKR"

    const/16 v11, 0x54

    const/16 v15, 0x90

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->LKR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 186
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "LRD"

    const/16 v11, 0x55

    const/16 v15, 0x1ae

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->LRD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 188
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "LSL"

    const/16 v11, 0x56

    const/16 v15, 0x1aa

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->LSL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 190
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "LTL"

    const/16 v11, 0x57

    const/16 v15, 0x1b8

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->LTL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 192
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "LVL"

    const/16 v11, 0x58

    const/16 v15, 0x1ac

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->LVL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 194
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "LYD"

    const/16 v11, 0x59

    const/16 v15, 0x1b2

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->LYD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 196
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "MAD"

    const/16 v11, 0x5a

    const/16 v15, 0x1f8

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MAD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 198
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "MDL"

    const/16 v11, 0x5b

    const/16 v15, 0x1f2

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MDL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 200
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "MGA"

    const/16 v11, 0x5c

    const/16 v15, 0x3c9

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MGA:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 202
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "MKD"

    const/16 v11, 0x5d

    const/16 v15, 0x327

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MKD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 204
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "MMK"

    const/16 v11, 0x5e

    const/16 v15, 0x68

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MMK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 206
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "MNT"

    const/16 v11, 0x5f

    const/16 v15, 0x1f0

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MNT:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 208
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "MOP"

    const/16 v11, 0x1be

    const/16 v15, 0x60

    invoke-direct {v0, v9, v15, v11}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MOP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 210
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "MRO"

    const/16 v11, 0x61

    const/16 v15, 0x1de

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MRO:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 212
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "MUR"

    const/16 v11, 0x62

    const/16 v15, 0x1e0

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MUR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 214
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "MVR"

    const/16 v11, 0x63

    const/16 v15, 0x1ce

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MVR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 216
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "MWK"

    const/16 v11, 0x64

    const/16 v15, 0x1c6

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MWK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 218
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "MXN"

    const/16 v11, 0x65

    const/16 v15, 0x1e4

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MXN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 220
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "MXV"

    const/16 v11, 0x66

    const/16 v15, 0x3d3

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MXV:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 222
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "MYR"

    const/16 v11, 0x67

    const/16 v15, 0x1ca

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MYR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 224
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "MZN"

    const/16 v11, 0x68

    const/16 v15, 0x3af

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MZN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 226
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "NAD"

    const/16 v11, 0x69

    const/16 v15, 0x204

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->NAD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 228
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "NGN"

    const/16 v11, 0x6a

    const/16 v15, 0x236

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->NGN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 230
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "NIO"

    const/16 v11, 0x6b

    const/16 v15, 0x22e

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->NIO:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 232
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "NOK"

    const/16 v11, 0x242

    const/16 v15, 0x6c

    invoke-direct {v0, v9, v15, v11}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->NOK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 234
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "NPR"

    const/16 v11, 0x6d

    const/16 v15, 0x20c

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->NPR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 236
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "NZD"

    const/16 v11, 0x6e

    const/16 v15, 0x22a

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->NZD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 238
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "OMR"

    const/16 v11, 0x6f

    const/16 v15, 0x200

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->OMR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 240
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "PAB"

    const/16 v11, 0x70

    const/16 v15, 0x24e

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->PAB:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 242
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "PEN"

    const/16 v11, 0x71

    const/16 v15, 0x25c

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->PEN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 244
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "PGK"

    const/16 v11, 0x72

    const/16 v15, 0x256

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->PGK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 246
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "PHP"

    const/16 v11, 0x73

    const/16 v15, 0x260

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->PHP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 248
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "PKR"

    const/16 v11, 0x74

    const/16 v15, 0x24a

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->PKR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 250
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "PLN"

    const/16 v11, 0x75

    const/16 v15, 0x3d9

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->PLN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 252
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "PYG"

    const/16 v11, 0x76

    const/16 v15, 0x258

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->PYG:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 254
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "QAR"

    const/16 v11, 0x77

    const/16 v15, 0x27a

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->QAR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 256
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "RON"

    const/16 v11, 0x78

    const/16 v15, 0x3b2

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->RON:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 258
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "RSD"

    const/16 v11, 0x79

    const/16 v15, 0x3ad

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->RSD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 260
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "RUB"

    const/16 v11, 0x7a

    const/16 v15, 0x283

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->RUB:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 262
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "RWF"

    const/16 v11, 0x7b

    const/16 v15, 0x286

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->RWF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 264
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "SAR"

    const/16 v11, 0x7c

    const/16 v15, 0x2aa

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SAR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 266
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "SBD"

    const/16 v11, 0x7d

    const/16 v15, 0x5a

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SBD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 268
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "SCR"

    const/16 v11, 0x7e

    const/16 v15, 0x2b2

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SCR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 270
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "SDG"

    const/16 v11, 0x7f

    const/16 v15, 0x3aa

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SDG:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 272
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "SEK"

    const/16 v11, 0x80

    const/16 v15, 0x2f0

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SEK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 274
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "SGD"

    const/16 v11, 0x81

    const/16 v15, 0x2be

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SGD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 276
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "SHP"

    const/16 v11, 0x82

    const/16 v15, 0x28e

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SHP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 278
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "SLL"

    const/16 v11, 0x83

    const/16 v15, 0x2b6

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SLL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 280
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "SOS"

    const/16 v11, 0x84

    const/16 v15, 0x2c2

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SOS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 282
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "SRD"

    const/16 v11, 0x85

    const/16 v15, 0x3c8

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SRD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 284
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "SSP"

    const/16 v11, 0x86

    const/16 v15, 0x2d8

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SSP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 286
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "STD"

    const/16 v11, 0x87

    const/16 v15, 0x2a6

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->STD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 288
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "SVC"

    const/16 v11, 0x88

    const/16 v15, 0xde

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SVC:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 290
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "SYP"

    const/16 v11, 0x89

    const/16 v15, 0x2f8

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SYP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 292
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "SZL"

    const/16 v11, 0x8a

    const/16 v15, 0x2ec

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SZL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 294
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "THB"

    const/16 v11, 0x8b

    const/16 v15, 0x2fc

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->THB:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 296
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "TJS"

    const/16 v11, 0x8c

    const/16 v15, 0x3cc

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->TJS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 298
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "TMT"

    const/16 v11, 0x8d

    const/16 v15, 0x3a6

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->TMT:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 300
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "TND"

    const/16 v11, 0x8e

    const/16 v15, 0x314

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->TND:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 302
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "TOP"

    const/16 v11, 0x8f

    const/16 v15, 0x308

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->TOP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 304
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "TRY"

    const/16 v11, 0x90

    const/16 v15, 0x3b5

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->TRY:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 306
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "TTD"

    const/16 v11, 0x91

    const/16 v15, 0x30c

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->TTD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 308
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "TWD"

    const/16 v11, 0x92

    const/16 v15, 0x385

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->TWD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 310
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "TZS"

    const/16 v11, 0x93

    const/16 v15, 0x342

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->TZS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 312
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "UAH"

    const/16 v11, 0x94

    const/16 v15, 0x3d4

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->UAH:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 314
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "UGX"

    const/16 v11, 0x95

    const/16 v15, 0x320

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->UGX:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 316
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "USD"

    const/16 v11, 0x96

    const/16 v15, 0x348

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->USD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 318
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "USN"

    const/16 v11, 0x97

    const/16 v15, 0x3e5

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->USN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 320
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "USS"

    const/16 v11, 0x98

    const/16 v15, 0x3e6

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->USS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 322
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "UYI"

    const/16 v11, 0x99

    const/16 v15, 0x3ac

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->UYI:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 324
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "UYU"

    const/16 v11, 0x9a

    const/16 v15, 0x35a

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->UYU:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 326
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "UZS"

    const/16 v11, 0x9b

    const/16 v15, 0x35c

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->UZS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 328
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "VEF"

    const/16 v11, 0x9c

    const/16 v15, 0x3a9

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->VEF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 330
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "VND"

    const/16 v11, 0x9d

    const/16 v15, 0x2c0

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->VND:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 332
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "VUV"

    const/16 v11, 0x9e

    const/16 v15, 0x224

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->VUV:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 334
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "WST"

    const/16 v11, 0x9f

    const/16 v15, 0x372

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->WST:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 336
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "XAF"

    const/16 v11, 0xa0

    const/16 v15, 0x3b6

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XAF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 338
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "XAG"

    const/16 v11, 0xa1

    const/16 v15, 0x3c1

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XAG:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 340
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "XAU"

    const/16 v11, 0xa2

    const/16 v15, 0x3bf

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XAU:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 342
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "XBA"

    const/16 v11, 0xa3

    const/16 v15, 0x3bb

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XBA:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 344
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "XBB"

    const/16 v11, 0xa4

    const/16 v15, 0x3bc

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XBB:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 346
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "XBC"

    const/16 v11, 0xa5

    const/16 v15, 0x3bd

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XBC:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 348
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "XBD"

    const/16 v11, 0xa6

    const/16 v15, 0x3be

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XBD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 350
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "XCD"

    const/16 v11, 0xa7

    const/16 v15, 0x3b7

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XCD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 352
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "XDR"

    const/16 v11, 0xa8

    const/16 v15, 0x3c0

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XDR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 354
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "XOF"

    const/16 v11, 0xa9

    const/16 v15, 0x3b8

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XOF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 356
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "XPD"

    const/16 v11, 0xaa

    const/16 v15, 0x3c4

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XPD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 358
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "XPF"

    const/16 v11, 0xab

    const/16 v15, 0x3b9

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XPF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 360
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "XPT"

    const/16 v11, 0xac

    const/16 v15, 0x3c2

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XPT:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 362
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "XTS"

    const/16 v11, 0xad

    const/16 v15, 0x3c3

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XTS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 364
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "XXX"

    const/16 v11, 0xae

    const/16 v15, 0x3e7

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XXX:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 366
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "YER"

    const/16 v11, 0xaf

    const/16 v15, 0x376

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->YER:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 368
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "ZAR"

    const/16 v11, 0xb0

    const/16 v15, 0x2c6

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ZAR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 370
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "ZMK"

    const/16 v11, 0xb1

    const/16 v15, 0x37e

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ZMK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 372
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    const-string v9, "ZMW"

    const/16 v11, 0xb2

    const/16 v15, 0x3c7

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/common/dinero/CurrencyCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ZMW:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v0, 0xb3

    new-array v0, v0, [Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 15
    sget-object v9, Lcom/squareup/protos/common/dinero/CurrencyCode;->AED:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/4 v11, 0x0

    aput-object v9, v0, v11

    sget-object v9, Lcom/squareup/protos/common/dinero/CurrencyCode;->AFN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/4 v11, 0x1

    aput-object v9, v0, v11

    sget-object v9, Lcom/squareup/protos/common/dinero/CurrencyCode;->ALL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/4 v11, 0x2

    aput-object v9, v0, v11

    sget-object v9, Lcom/squareup/protos/common/dinero/CurrencyCode;->AMD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/4 v11, 0x3

    aput-object v9, v0, v11

    sget-object v9, Lcom/squareup/protos/common/dinero/CurrencyCode;->ANG:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/4 v11, 0x4

    aput-object v9, v0, v11

    sget-object v9, Lcom/squareup/protos/common/dinero/CurrencyCode;->AOA:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/4 v11, 0x5

    aput-object v9, v0, v11

    sget-object v9, Lcom/squareup/protos/common/dinero/CurrencyCode;->ARS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/4 v11, 0x6

    aput-object v9, v0, v11

    sget-object v9, Lcom/squareup/protos/common/dinero/CurrencyCode;->AUD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/4 v11, 0x7

    aput-object v9, v0, v11

    sget-object v9, Lcom/squareup/protos/common/dinero/CurrencyCode;->AWG:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v11, 0x8

    aput-object v9, v0, v11

    sget-object v9, Lcom/squareup/protos/common/dinero/CurrencyCode;->AZN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v11, 0x9

    aput-object v9, v0, v11

    sget-object v9, Lcom/squareup/protos/common/dinero/CurrencyCode;->BAM:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v11, 0xa

    aput-object v9, v0, v11

    sget-object v9, Lcom/squareup/protos/common/dinero/CurrencyCode;->BBD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v11, 0xb

    aput-object v9, v0, v11

    sget-object v9, Lcom/squareup/protos/common/dinero/CurrencyCode;->BDT:Lcom/squareup/protos/common/dinero/CurrencyCode;

    aput-object v9, v0, v7

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->BGN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0xd

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->BHD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0xe

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->BIF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0xf

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->BMD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0x10

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->BND:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0x11

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->BOB:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0x12

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->BOV:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0x13

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->BRL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0x14

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->BSD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0x15

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->BTN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0x16

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->BWP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0x17

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->BYR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0x18

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->BZD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0x19

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->CAD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0x1a

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->CDF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0x1b

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->CHE:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0x1c

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->CHF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0x1d

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->CHW:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0x1e

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->CLF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v9, 0x1f

    aput-object v7, v0, v9

    sget-object v7, Lcom/squareup/protos/common/dinero/CurrencyCode;->CLP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    aput-object v7, v0, v3

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->CNY:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v7, 0x21

    aput-object v3, v0, v7

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->COP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v7, 0x22

    aput-object v3, v0, v7

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->COU:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v7, 0x23

    aput-object v3, v0, v7

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->CRC:Lcom/squareup/protos/common/dinero/CurrencyCode;

    aput-object v3, v0, v4

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->CUC:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v4, 0x25

    aput-object v3, v0, v4

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->CUP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v4, 0x26

    aput-object v3, v0, v4

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->CVE:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v4, 0x27

    aput-object v3, v0, v4

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->CZK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v4, 0x28

    aput-object v3, v0, v4

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->DJF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v4, 0x29

    aput-object v3, v0, v4

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->DKK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v4, 0x2a

    aput-object v3, v0, v4

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->DOP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v4, 0x2b

    aput-object v3, v0, v4

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->DZD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    aput-object v3, v0, v13

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->EGP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v4, 0x2d

    aput-object v3, v0, v4

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->ERN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v4, 0x2e

    aput-object v3, v0, v4

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->ETB:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v4, 0x2f

    aput-object v3, v0, v4

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->EUR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    aput-object v3, v0, v8

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->FJD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v4, 0x31

    aput-object v3, v0, v4

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->FKP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    aput-object v3, v0, v6

    sget-object v3, Lcom/squareup/protos/common/dinero/CurrencyCode;->GBP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    aput-object v3, v0, v2

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->GEL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->GHS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v3, 0x35

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->GIP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v3, 0x36

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->GMD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v3, 0x37

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->GNF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v3, 0x38

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->GTQ:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v3, 0x39

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->GYD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v3, 0x3a

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->HKD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v3, 0x3b

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->HNL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    aput-object v2, v0, v10

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->HRK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v3, 0x3d

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->HTG:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v3, 0x3e

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->HUF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v3, 0x3f

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->IDR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    aput-object v2, v0, v14

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->ILS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v3, 0x41

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->INR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v3, 0x42

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->IQD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v3, 0x43

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->IRR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    aput-object v2, v0, v12

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->ISK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v3, 0x45

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->JMD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v3, 0x46

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->JOD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v3, 0x47

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/protos/common/dinero/CurrencyCode;->JPY:Lcom/squareup/protos/common/dinero/CurrencyCode;

    aput-object v2, v0, v1

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->KES:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->KGS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->KHR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->KMF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->KPW:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->KRW:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->KWD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->KYD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->KZT:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->LAK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->LBP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->LKR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->LRD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->LSL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->LTL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->LVL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->LYD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->MAD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->MDL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->MGA:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->MKD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->MMK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->MNT:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->MOP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->MRO:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->MUR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->MVR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->MWK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->MXN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->MXV:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->MYR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->MZN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->NAD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->NGN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->NIO:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->NOK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->NPR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->NZD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->OMR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->PAB:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->PEN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->PGK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->PHP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->PKR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->PLN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->PYG:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->QAR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->RON:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->RSD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->RUB:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->RWF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->SAR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->SBD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->SCR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->SDG:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->SEK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->SGD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->SHP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->SLL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->SOS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->SRD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->SSP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->STD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->SVC:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->SYP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->SZL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->THB:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->TJS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->TMT:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->TND:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->TOP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->TRY:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->TTD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->TWD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->TZS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->UAH:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->UGX:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->USD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->USN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->USS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->UYI:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->UYU:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->UZS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->VEF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->VND:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->VUV:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->WST:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->XAF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->XAG:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->XAU:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->XBA:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->XBB:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->XBC:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->XBD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->XCD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->XDR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->XOF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->XPD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->XPF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xab

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->XPT:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xac

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->XTS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xad

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->XXX:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xae

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->YER:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->ZAR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->ZMK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xb1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/dinero/CurrencyCode;->ZMW:Lcom/squareup/protos/common/dinero/CurrencyCode;

    const/16 v2, 0xb2

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->$VALUES:[Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 374
    new-instance v0, Lcom/squareup/protos/common/dinero/CurrencyCode$ProtoAdapter_CurrencyCode;

    invoke-direct {v0}, Lcom/squareup/protos/common/dinero/CurrencyCode$ProtoAdapter_CurrencyCode;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 378
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 379
    iput p3, p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/common/dinero/CurrencyCode;
    .locals 1

    const/16 v0, 0xbf

    if-eq p0, v0, :cond_9

    const/16 v0, 0xc0

    if-eq p0, v0, :cond_8

    const/16 v0, 0x1a1

    if-eq p0, v0, :cond_7

    const/16 v0, 0x1a2

    if-eq p0, v0, :cond_6

    const/16 v0, 0x214

    if-eq p0, v0, :cond_5

    const/16 v0, 0x215

    if-eq p0, v0, :cond_4

    const/16 v0, 0x3ac

    if-eq p0, v0, :cond_3

    const/16 v0, 0x3ad

    if-eq p0, v0, :cond_2

    const/16 v0, 0x3af

    if-eq p0, v0, :cond_1

    const/16 v0, 0x3b0

    if-eq p0, v0, :cond_0

    sparse-switch p0, :sswitch_data_0

    packed-switch p0, :pswitch_data_0

    packed-switch p0, :pswitch_data_1

    packed-switch p0, :pswitch_data_2

    packed-switch p0, :pswitch_data_3

    const/4 p0, 0x0

    return-object p0

    .line 557
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XPD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 560
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XTS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 559
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XPT:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 548
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XAG:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 555
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XDR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 549
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XAU:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 553
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XBD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 552
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XBC:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 551
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XBB:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 550
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XBA:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 558
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XPF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 556
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XOF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 554
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XCD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 547
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XAF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 531
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->TRY:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 417
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CHW:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 415
    :pswitch_10
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CHE:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 507
    :pswitch_11
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->RON:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 514
    :pswitch_12
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SDG:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 543
    :pswitch_13
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->VEF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 440
    :pswitch_14
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->GHS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 398
    :pswitch_15
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BBD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 390
    :pswitch_16
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->AMD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 399
    :pswitch_17
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BDT:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 561
    :sswitch_0
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->XXX:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 539
    :sswitch_1
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->USS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 538
    :sswitch_2
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->USN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 418
    :sswitch_3
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CLF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 407
    :sswitch_4
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BRL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 504
    :sswitch_5
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->PLN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 406
    :sswitch_6
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BOV:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 439
    :sswitch_7
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->GEL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 535
    :sswitch_8
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->UAH:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 489
    :sswitch_9
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MXV:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 435
    :sswitch_a
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->EUR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 397
    :sswitch_b
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BAM:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 414
    :sswitch_c
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CDF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 400
    :sswitch_d
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BGN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 411
    :sswitch_e
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BYR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 392
    :sswitch_f
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->AOA:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 527
    :sswitch_10
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->TJS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 388
    :sswitch_11
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->AFN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 422
    :sswitch_12
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->COU:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 479
    :sswitch_13
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MGA:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 520
    :sswitch_14
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SRD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 565
    :sswitch_15
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ZMW:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 528
    :sswitch_16
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->TMT:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 424
    :sswitch_17
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CUC:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 533
    :sswitch_18
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->TWD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 564
    :sswitch_19
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ZMK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 562
    :sswitch_1a
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->YER:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 546
    :sswitch_1b
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->WST:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 542
    :sswitch_1c
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->UZS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 541
    :sswitch_1d
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->UYU:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 537
    :sswitch_1e
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->USD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 534
    :sswitch_1f
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->TZS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 438
    :sswitch_20
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->GBP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 432
    :sswitch_21
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->EGP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 480
    :sswitch_22
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MKD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 536
    :sswitch_23
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->UGX:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 529
    :sswitch_24
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->TND:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 387
    :sswitch_25
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->AED:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 532
    :sswitch_26
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->TTD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 530
    :sswitch_27
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->TOP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 526
    :sswitch_28
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->THB:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 524
    :sswitch_29
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SYP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 416
    :sswitch_2a
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CHF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 515
    :sswitch_2b
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SEK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 525
    :sswitch_2c
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SZL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 521
    :sswitch_2d
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SSP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 563
    :sswitch_2e
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ZAR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 519
    :sswitch_2f
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SOS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 544
    :sswitch_30
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->VND:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 516
    :sswitch_31
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SGD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 518
    :sswitch_32
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SLL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 513
    :sswitch_33
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SCR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 511
    :sswitch_34
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SAR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 522
    :sswitch_35
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->STD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 517
    :sswitch_36
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SHP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 510
    :sswitch_37
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->RWF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 509
    :sswitch_38
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->RUB:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 506
    :sswitch_39
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->QAR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 502
    :sswitch_3a
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->PHP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 500
    :sswitch_3b
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->PEN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 505
    :sswitch_3c
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->PYG:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 501
    :sswitch_3d
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->PGK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 499
    :sswitch_3e
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->PAB:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 503
    :sswitch_3f
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->PKR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 495
    :sswitch_40
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->NOK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 493
    :sswitch_41
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->NGN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 494
    :sswitch_42
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->NIO:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 497
    :sswitch_43
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->NZD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 545
    :sswitch_44
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->VUV:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 496
    :sswitch_45
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->NPR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 492
    :sswitch_46
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->NAD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 498
    :sswitch_47
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->OMR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 477
    :sswitch_48
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MAD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 478
    :sswitch_49
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MDL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 482
    :sswitch_4a
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MNT:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 488
    :sswitch_4b
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MXN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 485
    :sswitch_4c
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MUR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 484
    :sswitch_4d
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MRO:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 486
    :sswitch_4e
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MVR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 490
    :sswitch_4f
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MYR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 487
    :sswitch_50
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MWK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 483
    :sswitch_51
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MOP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 474
    :sswitch_52
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->LTL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 476
    :sswitch_53
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->LYD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 472
    :sswitch_54
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->LRD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 475
    :sswitch_55
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->LVL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 473
    :sswitch_56
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->LSL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 470
    :sswitch_57
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->LBP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 466
    :sswitch_58
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->KWD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 465
    :sswitch_59
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->KRW:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 464
    :sswitch_5a
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->KPW:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 460
    :sswitch_5b
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->KES:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 458
    :sswitch_5c
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->JOD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 468
    :sswitch_5d
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->KZT:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 459
    :sswitch_5e
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->JPY:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 457
    :sswitch_5f
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->JMD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 452
    :sswitch_60
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ILS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 454
    :sswitch_61
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->IQD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 455
    :sswitch_62
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->IRR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 451
    :sswitch_63
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->IDR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 453
    :sswitch_64
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->INR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 456
    :sswitch_65
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ISK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 450
    :sswitch_66
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->HUF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 446
    :sswitch_67
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->HKD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 447
    :sswitch_68
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->HNL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 449
    :sswitch_69
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->HTG:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 445
    :sswitch_6a
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->GYD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 443
    :sswitch_6b
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->GNF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 444
    :sswitch_6c
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->GTQ:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 441
    :sswitch_6d
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->GIP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 442
    :sswitch_6e
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->GMD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 428
    :sswitch_6f
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->DJF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 436
    :sswitch_70
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->FJD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 437
    :sswitch_71
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->FKP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 433
    :sswitch_72
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ERN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 434
    :sswitch_73
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ETB:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 523
    :sswitch_74
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SVC:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 430
    :sswitch_75
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->DOP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 429
    :sswitch_76
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->DKK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 427
    :sswitch_77
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CZK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 423
    :sswitch_78
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CRC:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 463
    :sswitch_79
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->KMF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 421
    :sswitch_7a
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->COP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 420
    :sswitch_7b
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CNY:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 419
    :sswitch_7c
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CLP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 471
    :sswitch_7d
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->LKR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 467
    :sswitch_7e
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->KYD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 426
    :sswitch_7f
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CVE:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 413
    :sswitch_80
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CAD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 462
    :sswitch_81
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->KHR:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 402
    :sswitch_82
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BIF:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 481
    :sswitch_83
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MMK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 404
    :sswitch_84
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BND:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 512
    :sswitch_85
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->SBD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 412
    :sswitch_86
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BZD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 410
    :sswitch_87
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BWP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 405
    :sswitch_88
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BOB:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 409
    :sswitch_89
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BTN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 403
    :sswitch_8a
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BMD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 401
    :sswitch_8b
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BHD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 408
    :sswitch_8c
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->BSD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 394
    :sswitch_8d
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->AUD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 393
    :sswitch_8e
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ARS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 431
    :sswitch_8f
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->DZD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 389
    :sswitch_90
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ALL:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 396
    :cond_0
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->AZN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 491
    :cond_1
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->MZN:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 508
    :cond_2
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->RSD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 540
    :cond_3
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->UYI:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 395
    :cond_4
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->AWG:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 391
    :cond_5
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->ANG:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 469
    :cond_6
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->LAK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 461
    :cond_7
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->KGS:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 425
    :cond_8
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->CUP:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    .line 448
    :cond_9
    sget-object p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->HRK:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_90
        0xc -> :sswitch_8f
        0x20 -> :sswitch_8e
        0x24 -> :sswitch_8d
        0x2c -> :sswitch_8c
        0x30 -> :sswitch_8b
        0x3c -> :sswitch_8a
        0x40 -> :sswitch_89
        0x44 -> :sswitch_88
        0x48 -> :sswitch_87
        0x54 -> :sswitch_86
        0x5a -> :sswitch_85
        0x60 -> :sswitch_84
        0x68 -> :sswitch_83
        0x6c -> :sswitch_82
        0x74 -> :sswitch_81
        0x7c -> :sswitch_80
        0x84 -> :sswitch_7f
        0x88 -> :sswitch_7e
        0x90 -> :sswitch_7d
        0x98 -> :sswitch_7c
        0x9c -> :sswitch_7b
        0xaa -> :sswitch_7a
        0xae -> :sswitch_79
        0xbc -> :sswitch_78
        0xcb -> :sswitch_77
        0xd0 -> :sswitch_76
        0xd6 -> :sswitch_75
        0xde -> :sswitch_74
        0xe6 -> :sswitch_73
        0xe8 -> :sswitch_72
        0xee -> :sswitch_71
        0xf2 -> :sswitch_70
        0x106 -> :sswitch_6f
        0x10e -> :sswitch_6e
        0x124 -> :sswitch_6d
        0x140 -> :sswitch_6c
        0x144 -> :sswitch_6b
        0x148 -> :sswitch_6a
        0x14c -> :sswitch_69
        0x154 -> :sswitch_68
        0x158 -> :sswitch_67
        0x15c -> :sswitch_66
        0x160 -> :sswitch_65
        0x164 -> :sswitch_64
        0x168 -> :sswitch_63
        0x16c -> :sswitch_62
        0x170 -> :sswitch_61
        0x178 -> :sswitch_60
        0x184 -> :sswitch_5f
        0x188 -> :sswitch_5e
        0x18e -> :sswitch_5d
        0x190 -> :sswitch_5c
        0x194 -> :sswitch_5b
        0x198 -> :sswitch_5a
        0x19a -> :sswitch_59
        0x19e -> :sswitch_58
        0x1a6 -> :sswitch_57
        0x1aa -> :sswitch_56
        0x1ac -> :sswitch_55
        0x1ae -> :sswitch_54
        0x1b2 -> :sswitch_53
        0x1b8 -> :sswitch_52
        0x1be -> :sswitch_51
        0x1c6 -> :sswitch_50
        0x1ca -> :sswitch_4f
        0x1ce -> :sswitch_4e
        0x1de -> :sswitch_4d
        0x1e0 -> :sswitch_4c
        0x1e4 -> :sswitch_4b
        0x1f0 -> :sswitch_4a
        0x1f2 -> :sswitch_49
        0x1f8 -> :sswitch_48
        0x200 -> :sswitch_47
        0x204 -> :sswitch_46
        0x20c -> :sswitch_45
        0x224 -> :sswitch_44
        0x22a -> :sswitch_43
        0x22e -> :sswitch_42
        0x236 -> :sswitch_41
        0x242 -> :sswitch_40
        0x24a -> :sswitch_3f
        0x24e -> :sswitch_3e
        0x256 -> :sswitch_3d
        0x258 -> :sswitch_3c
        0x25c -> :sswitch_3b
        0x260 -> :sswitch_3a
        0x27a -> :sswitch_39
        0x283 -> :sswitch_38
        0x286 -> :sswitch_37
        0x28e -> :sswitch_36
        0x2a6 -> :sswitch_35
        0x2aa -> :sswitch_34
        0x2b2 -> :sswitch_33
        0x2b6 -> :sswitch_32
        0x2be -> :sswitch_31
        0x2c0 -> :sswitch_30
        0x2c2 -> :sswitch_2f
        0x2c6 -> :sswitch_2e
        0x2d8 -> :sswitch_2d
        0x2ec -> :sswitch_2c
        0x2f0 -> :sswitch_2b
        0x2f4 -> :sswitch_2a
        0x2f8 -> :sswitch_29
        0x2fc -> :sswitch_28
        0x308 -> :sswitch_27
        0x30c -> :sswitch_26
        0x310 -> :sswitch_25
        0x314 -> :sswitch_24
        0x320 -> :sswitch_23
        0x327 -> :sswitch_22
        0x332 -> :sswitch_21
        0x33a -> :sswitch_20
        0x342 -> :sswitch_1f
        0x348 -> :sswitch_1e
        0x35a -> :sswitch_1d
        0x35c -> :sswitch_1c
        0x372 -> :sswitch_1b
        0x376 -> :sswitch_1a
        0x37e -> :sswitch_19
        0x385 -> :sswitch_18
        0x3a3 -> :sswitch_17
        0x3a6 -> :sswitch_16
        0x3c7 -> :sswitch_15
        0x3c8 -> :sswitch_14
        0x3c9 -> :sswitch_13
        0x3ca -> :sswitch_12
        0x3cb -> :sswitch_11
        0x3cc -> :sswitch_10
        0x3cd -> :sswitch_f
        0x3ce -> :sswitch_e
        0x3cf -> :sswitch_d
        0x3d0 -> :sswitch_c
        0x3d1 -> :sswitch_b
        0x3d2 -> :sswitch_a
        0x3d3 -> :sswitch_9
        0x3d4 -> :sswitch_8
        0x3d5 -> :sswitch_7
        0x3d8 -> :sswitch_6
        0x3d9 -> :sswitch_5
        0x3da -> :sswitch_4
        0x3de -> :sswitch_3
        0x3e5 -> :sswitch_2
        0x3e6 -> :sswitch_1
        0x3e7 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_17
        :pswitch_16
        :pswitch_15
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3a8
        :pswitch_14
        :pswitch_13
        :pswitch_12
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x3b2
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x3bb
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/dinero/CurrencyCode;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/common/dinero/CurrencyCode;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->$VALUES:[Lcom/squareup/protos/common/dinero/CurrencyCode;

    invoke-virtual {v0}, [Lcom/squareup/protos/common/dinero/CurrencyCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 572
    iget v0, p0, Lcom/squareup/protos/common/dinero/CurrencyCode;->value:I

    return v0
.end method
