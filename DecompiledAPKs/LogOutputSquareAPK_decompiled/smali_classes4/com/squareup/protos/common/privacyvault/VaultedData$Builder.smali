.class public final Lcom/squareup/protos/common/privacyvault/VaultedData$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "VaultedData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/privacyvault/VaultedData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/privacyvault/VaultedData;",
        "Lcom/squareup/protos/common/privacyvault/VaultedData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public entries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 82
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 83
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/common/privacyvault/VaultedData$Builder;->entries:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/common/privacyvault/VaultedData;
    .locals 3

    .line 94
    new-instance v0, Lcom/squareup/protos/common/privacyvault/VaultedData;

    iget-object v1, p0, Lcom/squareup/protos/common/privacyvault/VaultedData$Builder;->entries:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/common/privacyvault/VaultedData;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/protos/common/privacyvault/VaultedData$Builder;->build()Lcom/squareup/protos/common/privacyvault/VaultedData;

    move-result-object v0

    return-object v0
.end method

.method public entries(Ljava/util/List;)Lcom/squareup/protos/common/privacyvault/VaultedData$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/privacyvault/VaultedDataEntry;",
            ">;)",
            "Lcom/squareup/protos/common/privacyvault/VaultedData$Builder;"
        }
    .end annotation

    .line 87
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 88
    iput-object p1, p0, Lcom/squareup/protos/common/privacyvault/VaultedData$Builder;->entries:Ljava/util/List;

    return-object p0
.end method
