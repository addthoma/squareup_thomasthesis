.class public final Lcom/squareup/protos/messagehub/service/CustomIssueField$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CustomIssueField.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messagehub/service/CustomIssueField;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/messagehub/service/CustomIssueField;",
        "Lcom/squareup/protos/messagehub/service/CustomIssueField$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public issue_data_type:Ljava/lang/String;

.field public issue_data_value:Ljava/lang/String;

.field public issue_key:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 110
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/messagehub/service/CustomIssueField;
    .locals 5

    .line 130
    new-instance v0, Lcom/squareup/protos/messagehub/service/CustomIssueField;

    iget-object v1, p0, Lcom/squareup/protos/messagehub/service/CustomIssueField$Builder;->issue_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/messagehub/service/CustomIssueField$Builder;->issue_data_type:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/messagehub/service/CustomIssueField$Builder;->issue_data_value:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/messagehub/service/CustomIssueField;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/messagehub/service/CustomIssueField$Builder;->build()Lcom/squareup/protos/messagehub/service/CustomIssueField;

    move-result-object v0

    return-object v0
.end method

.method public issue_data_type(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/CustomIssueField$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/CustomIssueField$Builder;->issue_data_type:Ljava/lang/String;

    return-object p0
.end method

.method public issue_data_value(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/CustomIssueField$Builder;
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/CustomIssueField$Builder;->issue_data_value:Ljava/lang/String;

    return-object p0
.end method

.method public issue_key(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/CustomIssueField$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/CustomIssueField$Builder;->issue_key:Ljava/lang/String;

    return-object p0
.end method
