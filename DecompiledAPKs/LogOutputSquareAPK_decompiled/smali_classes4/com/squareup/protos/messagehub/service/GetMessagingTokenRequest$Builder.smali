.class public final Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetMessagingTokenRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest;",
        "Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public client_slug:Ljava/lang/String;

.field public device_push_token:Ljava/lang/String;

.field public email_for_hmac:Ljava/lang/String;

.field public sdk_type:Lcom/squareup/protos/messagehub/service/SDKType;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 155
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest;
    .locals 8

    .line 193
    new-instance v7, Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest;

    iget-object v1, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;->unit_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;->email_for_hmac:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;->client_slug:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;->sdk_type:Lcom/squareup/protos/messagehub/service/SDKType;

    iget-object v5, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;->device_push_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/messagehub/service/SDKType;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 144
    invoke-virtual {p0}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;->build()Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest;

    move-result-object v0

    return-object v0
.end method

.method public client_slug(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 173
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;->client_slug:Ljava/lang/String;

    return-object p0
.end method

.method public device_push_token(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;->device_push_token:Ljava/lang/String;

    return-object p0
.end method

.method public email_for_hmac(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;->email_for_hmac:Ljava/lang/String;

    return-object p0
.end method

.method public sdk_type(Lcom/squareup/protos/messagehub/service/SDKType;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;->sdk_type:Lcom/squareup/protos/messagehub/service/SDKType;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
