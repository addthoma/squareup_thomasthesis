.class public final Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SetNotificationPreferencesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest;",
        "Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public send_card_declined_notifications:Ljava/lang/Boolean;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest;
    .locals 4

    .line 112
    new-instance v0, Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest$Builder;->unit_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest$Builder;->send_card_declined_notifications:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest$Builder;->build()Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest;

    move-result-object v0

    return-object v0
.end method

.method public send_card_declined_notifications(Ljava/lang/Boolean;)Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest$Builder;->send_card_declined_notifications:Ljava/lang/Boolean;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest$Builder;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
