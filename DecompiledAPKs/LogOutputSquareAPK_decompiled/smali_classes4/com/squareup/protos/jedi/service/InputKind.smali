.class public final enum Lcom/squareup/protos/jedi/service/InputKind;
.super Ljava/lang/Enum;
.source "InputKind.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/jedi/service/InputKind$ProtoAdapter_InputKind;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/jedi/service/InputKind;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/jedi/service/InputKind;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/jedi/service/InputKind;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BOOLEAN:Lcom/squareup/protos/jedi/service/InputKind;

.field public static final enum BUTTON_PRESS:Lcom/squareup/protos/jedi/service/InputKind;

.field public static final enum TEXT:Lcom/squareup/protos/jedi/service/InputKind;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 11
    new-instance v0, Lcom/squareup/protos/jedi/service/InputKind;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "BUTTON_PRESS"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/jedi/service/InputKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/InputKind;->BUTTON_PRESS:Lcom/squareup/protos/jedi/service/InputKind;

    .line 13
    new-instance v0, Lcom/squareup/protos/jedi/service/InputKind;

    const/4 v3, 0x2

    const-string v4, "TEXT"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/jedi/service/InputKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/InputKind;->TEXT:Lcom/squareup/protos/jedi/service/InputKind;

    .line 15
    new-instance v0, Lcom/squareup/protos/jedi/service/InputKind;

    const/4 v4, 0x3

    const-string v5, "BOOLEAN"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/jedi/service/InputKind;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/jedi/service/InputKind;->BOOLEAN:Lcom/squareup/protos/jedi/service/InputKind;

    new-array v0, v4, [Lcom/squareup/protos/jedi/service/InputKind;

    .line 10
    sget-object v4, Lcom/squareup/protos/jedi/service/InputKind;->BUTTON_PRESS:Lcom/squareup/protos/jedi/service/InputKind;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/jedi/service/InputKind;->TEXT:Lcom/squareup/protos/jedi/service/InputKind;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/jedi/service/InputKind;->BOOLEAN:Lcom/squareup/protos/jedi/service/InputKind;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/jedi/service/InputKind;->$VALUES:[Lcom/squareup/protos/jedi/service/InputKind;

    .line 17
    new-instance v0, Lcom/squareup/protos/jedi/service/InputKind$ProtoAdapter_InputKind;

    invoke-direct {v0}, Lcom/squareup/protos/jedi/service/InputKind$ProtoAdapter_InputKind;-><init>()V

    sput-object v0, Lcom/squareup/protos/jedi/service/InputKind;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput p3, p0, Lcom/squareup/protos/jedi/service/InputKind;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/jedi/service/InputKind;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 32
    :cond_0
    sget-object p0, Lcom/squareup/protos/jedi/service/InputKind;->BOOLEAN:Lcom/squareup/protos/jedi/service/InputKind;

    return-object p0

    .line 31
    :cond_1
    sget-object p0, Lcom/squareup/protos/jedi/service/InputKind;->TEXT:Lcom/squareup/protos/jedi/service/InputKind;

    return-object p0

    .line 30
    :cond_2
    sget-object p0, Lcom/squareup/protos/jedi/service/InputKind;->BUTTON_PRESS:Lcom/squareup/protos/jedi/service/InputKind;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/InputKind;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/jedi/service/InputKind;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/jedi/service/InputKind;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/jedi/service/InputKind;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/jedi/service/InputKind;->$VALUES:[Lcom/squareup/protos/jedi/service/InputKind;

    invoke-virtual {v0}, [Lcom/squareup/protos/jedi/service/InputKind;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/jedi/service/InputKind;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/squareup/protos/jedi/service/InputKind;->value:I

    return v0
.end method
