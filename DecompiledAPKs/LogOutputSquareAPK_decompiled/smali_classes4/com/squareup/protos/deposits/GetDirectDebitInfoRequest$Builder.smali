.class public final Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetDirectDebitInfoRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest;",
        "Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public country_specific_detail:Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;

.field public idempotence_key:Ljava/lang/String;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 120
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest;
    .locals 5

    .line 150
    new-instance v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest;

    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;->unit_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;->idempotence_key:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;->country_specific_detail:Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 113
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;->build()Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest;

    move-result-object v0

    return-object v0
.end method

.method public country_specific_detail(Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;)Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;->country_specific_detail:Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;

    return-object p0
.end method

.method public idempotence_key(Ljava/lang/String;)Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;->idempotence_key:Ljava/lang/String;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
