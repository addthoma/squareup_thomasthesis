.class public final Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BalanceActivityFailureMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;",
        "Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public expected_balance:Lcom/squareup/protos/common/Money;

.field public sweep_failure_reason:Lcom/squareup/protos/deposits/SweepFailureReason;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 99
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;
    .locals 4

    .line 120
    new-instance v0, Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata$Builder;->expected_balance:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata$Builder;->sweep_failure_reason:Lcom/squareup/protos/deposits/SweepFailureReason;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/deposits/SweepFailureReason;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata$Builder;->build()Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    move-result-object v0

    return-object v0
.end method

.method public expected_balance(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata$Builder;->expected_balance:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public sweep_failure_reason(Lcom/squareup/protos/deposits/SweepFailureReason;)Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata$Builder;->sweep_failure_reason:Lcom/squareup/protos/deposits/SweepFailureReason;

    return-object p0
.end method
