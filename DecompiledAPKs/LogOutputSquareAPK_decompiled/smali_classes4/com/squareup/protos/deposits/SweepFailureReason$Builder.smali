.class public final Lcom/squareup/protos/deposits/SweepFailureReason$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SweepFailureReason.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/SweepFailureReason;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/deposits/SweepFailureReason;",
        "Lcom/squareup/protos/deposits/SweepFailureReason$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bookkeeper_pull_failure:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

.field public bookkeeper_push_failure:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

.field public esperanto_failure:Lcom/squareup/protos/payments/common/PushMoneyError;

.field public teller_failure:Lcom/squareup/protos/teller/FailureReason;

.field public wallet_pull_failure:Lcom/squareup/protos/connect/v2/resources/Error$Code;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 171
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bookkeeper_pull_failure(Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;)Lcom/squareup/protos/deposits/SweepFailureReason$Builder;
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->bookkeeper_pull_failure:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    const/4 p1, 0x0

    .line 204
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->esperanto_failure:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->teller_failure:Lcom/squareup/protos/teller/FailureReason;

    .line 206
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->bookkeeper_push_failure:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    .line 207
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->wallet_pull_failure:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0
.end method

.method public bookkeeper_push_failure(Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;)Lcom/squareup/protos/deposits/SweepFailureReason$Builder;
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->bookkeeper_push_failure:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    const/4 p1, 0x0

    .line 217
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->esperanto_failure:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 218
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->teller_failure:Lcom/squareup/protos/teller/FailureReason;

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->bookkeeper_pull_failure:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->wallet_pull_failure:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/deposits/SweepFailureReason;
    .locals 8

    .line 238
    new-instance v7, Lcom/squareup/protos/deposits/SweepFailureReason;

    iget-object v1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->esperanto_failure:Lcom/squareup/protos/payments/common/PushMoneyError;

    iget-object v2, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->teller_failure:Lcom/squareup/protos/teller/FailureReason;

    iget-object v3, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->bookkeeper_pull_failure:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    iget-object v4, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->bookkeeper_push_failure:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    iget-object v5, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->wallet_pull_failure:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/deposits/SweepFailureReason;-><init>(Lcom/squareup/protos/payments/common/PushMoneyError;Lcom/squareup/protos/teller/FailureReason;Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;Lcom/squareup/protos/connect/v2/resources/Error$Code;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 160
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->build()Lcom/squareup/protos/deposits/SweepFailureReason;

    move-result-object v0

    return-object v0
.end method

.method public esperanto_failure(Lcom/squareup/protos/payments/common/PushMoneyError;)Lcom/squareup/protos/deposits/SweepFailureReason$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->esperanto_failure:Lcom/squareup/protos/payments/common/PushMoneyError;

    const/4 p1, 0x0

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->teller_failure:Lcom/squareup/protos/teller/FailureReason;

    .line 180
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->bookkeeper_pull_failure:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    .line 181
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->bookkeeper_push_failure:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    .line 182
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->wallet_pull_failure:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0
.end method

.method public teller_failure(Lcom/squareup/protos/teller/FailureReason;)Lcom/squareup/protos/deposits/SweepFailureReason$Builder;
    .locals 0

    .line 190
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->teller_failure:Lcom/squareup/protos/teller/FailureReason;

    const/4 p1, 0x0

    .line 191
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->esperanto_failure:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 192
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->bookkeeper_pull_failure:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    .line 193
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->bookkeeper_push_failure:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    .line 194
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->wallet_pull_failure:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0
.end method

.method public wallet_pull_failure(Lcom/squareup/protos/connect/v2/resources/Error$Code;)Lcom/squareup/protos/deposits/SweepFailureReason$Builder;
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->wallet_pull_failure:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/4 p1, 0x0

    .line 229
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->esperanto_failure:Lcom/squareup/protos/payments/common/PushMoneyError;

    .line 230
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->teller_failure:Lcom/squareup/protos/teller/FailureReason;

    .line 231
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->bookkeeper_pull_failure:Lcom/squareup/protos/bookkeeper/SweepBizbankMoneyResponse$Error;

    .line 232
    iput-object p1, p0, Lcom/squareup/protos/deposits/SweepFailureReason$Builder;->bookkeeper_push_failure:Lcom/squareup/protos/bookkeeper/ReverseSweepBizbankMoneyResponse$Error;

    return-object p0
.end method
