.class public final Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;
.super Lcom/squareup/wire/Message;
.source "ChangeAppointmentStatusRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$ProtoAdapter_ChangeAppointmentStatusRequest;,
        Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;",
        "Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACTION:Lcom/squareup/protos/agenda/ReservationAction;

.field public static final DEFAULT_APPOINTMENT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CHARGE_FEE:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final action:Lcom/squareup/protos/agenda/ReservationAction;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.agenda.ReservationAction#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final appointment_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final charge_fee:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final client_message:Lcom/squareup/protos/agenda/ClientMessage;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.agenda.ClientMessage#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final context:Lcom/squareup/protos/agenda/RequestContext;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.agenda.RequestContext#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$ProtoAdapter_ChangeAppointmentStatusRequest;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$ProtoAdapter_ChangeAppointmentStatusRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 25
    sget-object v0, Lcom/squareup/protos/agenda/ReservationAction;->ACCEPT:Lcom/squareup/protos/agenda/ReservationAction;

    sput-object v0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->DEFAULT_ACTION:Lcom/squareup/protos/agenda/ReservationAction;

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->DEFAULT_CHARGE_FEE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/agenda/RequestContext;Lcom/squareup/protos/agenda/ReservationAction;Ljava/lang/String;Lcom/squareup/protos/agenda/ClientMessage;Ljava/lang/Boolean;)V
    .locals 7

    .line 72
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;-><init>(Lcom/squareup/protos/agenda/RequestContext;Lcom/squareup/protos/agenda/ReservationAction;Ljava/lang/String;Lcom/squareup/protos/agenda/ClientMessage;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/agenda/RequestContext;Lcom/squareup/protos/agenda/ReservationAction;Ljava/lang/String;Lcom/squareup/protos/agenda/ClientMessage;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 78
    sget-object v0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 79
    iput-object p1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->context:Lcom/squareup/protos/agenda/RequestContext;

    .line 80
    iput-object p2, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->action:Lcom/squareup/protos/agenda/ReservationAction;

    .line 81
    iput-object p3, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->appointment_id:Ljava/lang/String;

    .line 82
    iput-object p4, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->client_message:Lcom/squareup/protos/agenda/ClientMessage;

    .line 83
    iput-object p5, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->charge_fee:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 101
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 102
    :cond_1
    check-cast p1, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->context:Lcom/squareup/protos/agenda/RequestContext;

    iget-object v3, p1, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->context:Lcom/squareup/protos/agenda/RequestContext;

    .line 104
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->action:Lcom/squareup/protos/agenda/ReservationAction;

    iget-object v3, p1, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->action:Lcom/squareup/protos/agenda/ReservationAction;

    .line 105
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->appointment_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->appointment_id:Ljava/lang/String;

    .line 106
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->client_message:Lcom/squareup/protos/agenda/ClientMessage;

    iget-object v3, p1, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->client_message:Lcom/squareup/protos/agenda/ClientMessage;

    .line 107
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->charge_fee:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->charge_fee:Ljava/lang/Boolean;

    .line 108
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 113
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 115
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->context:Lcom/squareup/protos/agenda/RequestContext;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/agenda/RequestContext;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->action:Lcom/squareup/protos/agenda/ReservationAction;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/agenda/ReservationAction;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->appointment_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->client_message:Lcom/squareup/protos/agenda/ClientMessage;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/agenda/ClientMessage;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->charge_fee:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 121
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;
    .locals 2

    .line 88
    new-instance v0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;-><init>()V

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->context:Lcom/squareup/protos/agenda/RequestContext;

    iput-object v1, v0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;->context:Lcom/squareup/protos/agenda/RequestContext;

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->action:Lcom/squareup/protos/agenda/ReservationAction;

    iput-object v1, v0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;->action:Lcom/squareup/protos/agenda/ReservationAction;

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->appointment_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;->appointment_id:Ljava/lang/String;

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->client_message:Lcom/squareup/protos/agenda/ClientMessage;

    iput-object v1, v0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;->client_message:Lcom/squareup/protos/agenda/ClientMessage;

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->charge_fee:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;->charge_fee:Ljava/lang/Boolean;

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->newBuilder()Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->context:Lcom/squareup/protos/agenda/RequestContext;

    if-eqz v1, :cond_0

    const-string v1, ", context="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->context:Lcom/squareup/protos/agenda/RequestContext;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 130
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->action:Lcom/squareup/protos/agenda/ReservationAction;

    if-eqz v1, :cond_1

    const-string v1, ", action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->action:Lcom/squareup/protos/agenda/ReservationAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 131
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->appointment_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", appointment_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->appointment_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->client_message:Lcom/squareup/protos/agenda/ClientMessage;

    if-eqz v1, :cond_3

    const-string v1, ", client_message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->client_message:Lcom/squareup/protos/agenda/ClientMessage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 133
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->charge_fee:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", charge_fee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ChangeAppointmentStatusRequest;->charge_fee:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ChangeAppointmentStatusRequest{"

    .line 134
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
