.class public final Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateMerchantFeedbackRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;",
        "Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public current_unit:Lcom/squareup/protos/agenda/UnitIdentifier;

.field public feedback:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public installation_identity:Lcom/squareup/protos/agenda/ClientInstallationIdentity;

.field public rating:Ljava/lang/Integer;

.field public timestamp:Lcom/squareup/protos/common/time/DateTime;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 172
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;
    .locals 9

    .line 226
    new-instance v8, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;

    iget-object v1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->current_unit:Lcom/squareup/protos/agenda/UnitIdentifier;

    iget-object v3, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->feedback:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->rating:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->installation_identity:Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    iget-object v6, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->timestamp:Lcom/squareup/protos/common/time/DateTime;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/agenda/UnitIdentifier;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/protos/agenda/ClientInstallationIdentity;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 159
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->build()Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest;

    move-result-object v0

    return-object v0
.end method

.method public current_unit(Lcom/squareup/protos/agenda/UnitIdentifier;)Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->current_unit:Lcom/squareup/protos/agenda/UnitIdentifier;

    return-object p0
.end method

.method public feedback(Ljava/lang/String;)Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;
    .locals 0

    .line 196
    iput-object p1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->feedback:Ljava/lang/String;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;
    .locals 0

    .line 180
    iput-object p1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public installation_identity(Lcom/squareup/protos/agenda/ClientInstallationIdentity;)Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;
    .locals 0

    .line 212
    iput-object p1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->installation_identity:Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    return-object p0
.end method

.method public rating(Ljava/lang/Integer;)Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;
    .locals 0

    .line 204
    iput-object p1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->rating:Ljava/lang/Integer;

    return-object p0
.end method

.method public timestamp(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/agenda/CreateMerchantFeedbackRequest$Builder;->timestamp:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method
