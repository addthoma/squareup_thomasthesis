.class public final Lcom/squareup/protos/agenda/Unit;
.super Lcom/squareup/wire/Message;
.source "Unit.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/agenda/Unit$ProtoAdapter_Unit;,
        Lcom/squareup/protos/agenda/Unit$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/agenda/Unit;",
        "Lcom/squareup/protos/agenda/Unit$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/agenda/Unit;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DISPLAY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PHONE_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_TIME_ZONE:Ljava/lang/String; = ""

.field public static final DEFAULT_UNIT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final address:Lcom/squareup/protos/common/location/GlobalAddress;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.location.GlobalAddress#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final display_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final phone_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final time_zone:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/agenda/Unit$ProtoAdapter_Unit;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/Unit$ProtoAdapter_Unit;-><init>()V

    sput-object v0, Lcom/squareup/protos/agenda/Unit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;)V
    .locals 7

    .line 72
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/agenda/Unit;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Lokio/ByteString;)V
    .locals 1

    .line 77
    sget-object v0, Lcom/squareup/protos/agenda/Unit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 78
    iput-object p1, p0, Lcom/squareup/protos/agenda/Unit;->unit_token:Ljava/lang/String;

    .line 79
    iput-object p2, p0, Lcom/squareup/protos/agenda/Unit;->display_name:Ljava/lang/String;

    .line 80
    iput-object p3, p0, Lcom/squareup/protos/agenda/Unit;->phone_number:Ljava/lang/String;

    .line 81
    iput-object p4, p0, Lcom/squareup/protos/agenda/Unit;->time_zone:Ljava/lang/String;

    .line 82
    iput-object p5, p0, Lcom/squareup/protos/agenda/Unit;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 100
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/agenda/Unit;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 101
    :cond_1
    check-cast p1, Lcom/squareup/protos/agenda/Unit;

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/Unit;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/agenda/Unit;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Unit;->unit_token:Ljava/lang/String;

    .line 103
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->display_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Unit;->display_name:Ljava/lang/String;

    .line 104
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->phone_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Unit;->phone_number:Ljava/lang/String;

    .line 105
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->time_zone:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/agenda/Unit;->time_zone:Ljava/lang/String;

    .line 106
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object p1, p1, Lcom/squareup/protos/agenda/Unit;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 107
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 112
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/Unit;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->unit_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->phone_number:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->time_zone:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/location/GlobalAddress;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 120
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/agenda/Unit$Builder;
    .locals 2

    .line 87
    new-instance v0, Lcom/squareup/protos/agenda/Unit$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/Unit$Builder;-><init>()V

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Unit$Builder;->unit_token:Ljava/lang/String;

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->display_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Unit$Builder;->display_name:Ljava/lang/String;

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->phone_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Unit$Builder;->phone_number:Ljava/lang/String;

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->time_zone:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Unit$Builder;->time_zone:Ljava/lang/String;

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    iput-object v1, v0, Lcom/squareup/protos/agenda/Unit$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/Unit;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/agenda/Unit$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/Unit;->newBuilder()Lcom/squareup/protos/agenda/Unit$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", display_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->display_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->phone_number:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", phone_number=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->time_zone:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", time_zone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->time_zone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v1, :cond_4

    const-string v1, ", address="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/Unit;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Unit{"

    .line 133
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
