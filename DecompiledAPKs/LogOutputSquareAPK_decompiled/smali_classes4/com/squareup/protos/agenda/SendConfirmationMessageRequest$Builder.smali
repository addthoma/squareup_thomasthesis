.class public final Lcom/squareup/protos/agenda/SendConfirmationMessageRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SendConfirmationMessageRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/SendConfirmationMessageRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/agenda/SendConfirmationMessageRequest;",
        "Lcom/squareup/protos/agenda/SendConfirmationMessageRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public appointment_id:Ljava/lang/String;

.field public appointment_start_date:Lcom/squareup/protos/common/time/DateTime;

.field public context:Lcom/squareup/protos/agenda/RequestContext;

.field public idempotence_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 137
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public appointment_id(Ljava/lang/String;)Lcom/squareup/protos/agenda/SendConfirmationMessageRequest$Builder;
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/protos/agenda/SendConfirmationMessageRequest$Builder;->appointment_id:Ljava/lang/String;

    return-object p0
.end method

.method public appointment_start_date(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/agenda/SendConfirmationMessageRequest$Builder;
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/squareup/protos/agenda/SendConfirmationMessageRequest$Builder;->appointment_start_date:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/agenda/SendConfirmationMessageRequest;
    .locals 7

    .line 172
    new-instance v6, Lcom/squareup/protos/agenda/SendConfirmationMessageRequest;

    iget-object v1, p0, Lcom/squareup/protos/agenda/SendConfirmationMessageRequest$Builder;->context:Lcom/squareup/protos/agenda/RequestContext;

    iget-object v2, p0, Lcom/squareup/protos/agenda/SendConfirmationMessageRequest$Builder;->appointment_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/agenda/SendConfirmationMessageRequest$Builder;->appointment_start_date:Lcom/squareup/protos/common/time/DateTime;

    iget-object v4, p0, Lcom/squareup/protos/agenda/SendConfirmationMessageRequest$Builder;->idempotence_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/agenda/SendConfirmationMessageRequest;-><init>(Lcom/squareup/protos/agenda/RequestContext;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 128
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/SendConfirmationMessageRequest$Builder;->build()Lcom/squareup/protos/agenda/SendConfirmationMessageRequest;

    move-result-object v0

    return-object v0
.end method

.method public context(Lcom/squareup/protos/agenda/RequestContext;)Lcom/squareup/protos/agenda/SendConfirmationMessageRequest$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/agenda/SendConfirmationMessageRequest$Builder;->context:Lcom/squareup/protos/agenda/RequestContext;

    return-object p0
.end method

.method public idempotence_token(Ljava/lang/String;)Lcom/squareup/protos/agenda/SendConfirmationMessageRequest$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/agenda/SendConfirmationMessageRequest$Builder;->idempotence_token:Ljava/lang/String;

    return-object p0
.end method
