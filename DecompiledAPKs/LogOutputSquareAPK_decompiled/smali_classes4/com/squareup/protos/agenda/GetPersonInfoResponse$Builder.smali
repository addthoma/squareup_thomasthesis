.class public final Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetPersonInfoResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/GetPersonInfoResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/agenda/GetPersonInfoResponse;",
        "Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public email:Ljava/lang/String;

.field public employments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/agenda/Employment;",
            ">;"
        }
    .end annotation
.end field

.field public person_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 117
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 118
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;->employments:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/agenda/GetPersonInfoResponse;
    .locals 5

    .line 145
    new-instance v0, Lcom/squareup/protos/agenda/GetPersonInfoResponse;

    iget-object v1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;->email:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;->person_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;->employments:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/agenda/GetPersonInfoResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 110
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;->build()Lcom/squareup/protos/agenda/GetPersonInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public email(Ljava/lang/String;)Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;->email:Ljava/lang/String;

    return-object p0
.end method

.method public employments(Ljava/util/List;)Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/agenda/Employment;",
            ">;)",
            "Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;"
        }
    .end annotation

    .line 138
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;->employments:Ljava/util/List;

    return-object p0
.end method

.method public person_token(Ljava/lang/String;)Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/agenda/GetPersonInfoResponse$Builder;->person_token:Ljava/lang/String;

    return-object p0
.end method
