.class public final Lcom/squareup/protos/queuebert/model/PaymentResult;
.super Lcom/squareup/wire/Message;
.source "PaymentResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/queuebert/model/PaymentResult$ProtoAdapter_PaymentResult;,
        Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/queuebert/model/PaymentResult;",
        "Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/queuebert/model/PaymentResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ERROR_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_ERROR_TITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_PAYMENT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS:Lcom/squareup/protos/queuebert/model/PaymentStatus;

.field public static final DEFAULT_UNIQUE_KEY:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final error_message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final error_title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final payment_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/queuebert/model/PaymentStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.queuebert.model.PaymentStatus#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final unique_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/queuebert/model/PaymentResult$ProtoAdapter_PaymentResult;

    invoke-direct {v0}, Lcom/squareup/protos/queuebert/model/PaymentResult$ProtoAdapter_PaymentResult;-><init>()V

    sput-object v0, Lcom/squareup/protos/queuebert/model/PaymentResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->UNKNOWN_PAYMENT_STATUS:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    sput-object v0, Lcom/squareup/protos/queuebert/model/PaymentResult;->DEFAULT_STATUS:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/queuebert/model/PaymentStatus;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 75
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/queuebert/model/PaymentResult;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/queuebert/model/PaymentStatus;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/queuebert/model/PaymentStatus;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 80
    sget-object v0, Lcom/squareup/protos/queuebert/model/PaymentResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 81
    iput-object p1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->unique_key:Ljava/lang/String;

    .line 82
    iput-object p2, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->payment_id:Ljava/lang/String;

    .line 83
    iput-object p3, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->status:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    .line 84
    iput-object p4, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->error_title:Ljava/lang/String;

    .line 85
    iput-object p5, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->error_message:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 103
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/queuebert/model/PaymentResult;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 104
    :cond_1
    check-cast p1, Lcom/squareup/protos/queuebert/model/PaymentResult;

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/queuebert/model/PaymentResult;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/queuebert/model/PaymentResult;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->unique_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/queuebert/model/PaymentResult;->unique_key:Ljava/lang/String;

    .line 106
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->payment_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/queuebert/model/PaymentResult;->payment_id:Ljava/lang/String;

    .line 107
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->status:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    iget-object v3, p1, Lcom/squareup/protos/queuebert/model/PaymentResult;->status:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    .line 108
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->error_title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/queuebert/model/PaymentResult;->error_title:Ljava/lang/String;

    .line 109
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->error_message:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/queuebert/model/PaymentResult;->error_message:Ljava/lang/String;

    .line 110
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 115
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/queuebert/model/PaymentResult;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->unique_key:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->payment_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->status:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/queuebert/model/PaymentStatus;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->error_title:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 123
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;
    .locals 2

    .line 90
    new-instance v0, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;-><init>()V

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->unique_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->unique_key:Ljava/lang/String;

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->payment_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->payment_id:Ljava/lang/String;

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->status:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    iput-object v1, v0, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->status:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->error_title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->error_title:Ljava/lang/String;

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->error_message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->error_message:Ljava/lang/String;

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/queuebert/model/PaymentResult;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/queuebert/model/PaymentResult;->newBuilder()Lcom/squareup/protos/queuebert/model/PaymentResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->unique_key:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", unique_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->unique_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->payment_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", payment_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->payment_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->status:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    if-eqz v1, :cond_2

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->status:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 134
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->error_title:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", error_title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->error_title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", error_message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/PaymentResult;->error_message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PaymentResult{"

    .line 136
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
