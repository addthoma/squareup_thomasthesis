.class public final enum Lcom/squareup/protos/privacyvault/model/PrivacyScope;
.super Ljava/lang/Enum;
.source "PrivacyScope.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/privacyvault/model/PrivacyScope$ProtoAdapter_PrivacyScope;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/privacyvault/model/PrivacyScope;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/privacyvault/model/PrivacyScope;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/privacyvault/model/PrivacyScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CASH_CUSTOMER:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

.field public static final enum CASH_RECIPIENT:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

.field public static final enum CASH_SENDER:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

.field public static final enum REGISTER_BUYER:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

.field public static final enum REGISTER_MERCHANT:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

.field public static final enum UNKNOWN:Lcom/squareup/protos/privacyvault/model/PrivacyScope;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 18
    new-instance v0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/privacyvault/model/PrivacyScope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->UNKNOWN:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    .line 23
    new-instance v0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    const/4 v2, 0x1

    const-string v3, "REGISTER_MERCHANT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/privacyvault/model/PrivacyScope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->REGISTER_MERCHANT:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    .line 28
    new-instance v0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    const/4 v3, 0x2

    const-string v4, "REGISTER_BUYER"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/privacyvault/model/PrivacyScope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->REGISTER_BUYER:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    .line 33
    new-instance v0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    const/4 v4, 0x3

    const-string v5, "CASH_SENDER"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/privacyvault/model/PrivacyScope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->CASH_SENDER:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    .line 38
    new-instance v0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    const/4 v5, 0x4

    const-string v6, "CASH_RECIPIENT"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/privacyvault/model/PrivacyScope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->CASH_RECIPIENT:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    .line 43
    new-instance v0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    const/4 v6, 0x5

    const-string v7, "CASH_CUSTOMER"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/privacyvault/model/PrivacyScope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->CASH_CUSTOMER:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    .line 14
    sget-object v7, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->UNKNOWN:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->REGISTER_MERCHANT:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->REGISTER_BUYER:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->CASH_SENDER:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->CASH_RECIPIENT:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->CASH_CUSTOMER:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->$VALUES:[Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    .line 45
    new-instance v0, Lcom/squareup/protos/privacyvault/model/PrivacyScope$ProtoAdapter_PrivacyScope;

    invoke-direct {v0}, Lcom/squareup/protos/privacyvault/model/PrivacyScope$ProtoAdapter_PrivacyScope;-><init>()V

    sput-object v0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 50
    iput p3, p0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/privacyvault/model/PrivacyScope;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 63
    :cond_0
    sget-object p0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->CASH_CUSTOMER:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    return-object p0

    .line 62
    :cond_1
    sget-object p0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->CASH_RECIPIENT:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    return-object p0

    .line 61
    :cond_2
    sget-object p0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->CASH_SENDER:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    return-object p0

    .line 60
    :cond_3
    sget-object p0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->REGISTER_BUYER:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    return-object p0

    .line 59
    :cond_4
    sget-object p0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->REGISTER_MERCHANT:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    return-object p0

    .line 58
    :cond_5
    sget-object p0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->UNKNOWN:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/privacyvault/model/PrivacyScope;
    .locals 1

    .line 14
    const-class v0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/privacyvault/model/PrivacyScope;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->$VALUES:[Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    invoke-virtual {v0}, [Lcom/squareup/protos/privacyvault/model/PrivacyScope;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 70
    iget v0, p0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->value:I

    return v0
.end method
