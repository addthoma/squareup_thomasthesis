.class final Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$ProtoAdapter_MessageWithDescriptors;
.super Lcom/squareup/wire/ProtoAdapter;
.source "MessageWithDescriptors.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_MessageWithDescriptors"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 163
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 184
    new-instance v0, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;-><init>()V

    .line 185
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 186
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 199
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 192
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;->scope(Lcom/squareup/protos/privacyvault/model/PrivacyScope;)Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 194
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 189
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;->message_data:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 188
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;->type_name(Ljava/lang/String;)Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;

    goto :goto_0

    .line 203
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 204
    invoke-virtual {v0}, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;->build()Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 161
    invoke-virtual {p0, p1}, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$ProtoAdapter_MessageWithDescriptors;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 176
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;->type_name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 177
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;->message_data:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 178
    sget-object v0, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;->scope:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 179
    invoke-virtual {p2}, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 161
    check-cast p2, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$ProtoAdapter_MessageWithDescriptors;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;)I
    .locals 4

    .line 168
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;->type_name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    .line 169
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;->message_data:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/privacyvault/model/PrivacyScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;->scope:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    const/4 v3, 0x4

    .line 170
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 171
    invoke-virtual {p1}, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 161
    check-cast p1, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$ProtoAdapter_MessageWithDescriptors;->encodedSize(Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;)Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;
    .locals 0

    .line 209
    invoke-virtual {p1}, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;->newBuilder()Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;

    move-result-object p1

    .line 210
    invoke-virtual {p1}, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 211
    invoke-virtual {p1}, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;->build()Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 161
    check-cast p1, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$ProtoAdapter_MessageWithDescriptors;->redact(Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;)Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    move-result-object p1

    return-object p1
.end method
