.class final Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$ProtoAdapter_ProtectedFideliusTokenWithCategory;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Protected.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ProtectedFideliusTokenWithCategory"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 341
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 361
    new-instance v0, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;-><init>()V

    .line 362
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 363
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 368
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 366
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;

    goto :goto_0

    .line 365
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;->category(Ljava/lang/String;)Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;

    goto :goto_0

    .line 372
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 373
    invoke-virtual {v0}, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;->build()Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 339
    invoke-virtual {p0, p1}, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$ProtoAdapter_ProtectedFideliusTokenWithCategory;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 354
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;->category:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 355
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;->token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 356
    invoke-virtual {p2}, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 339
    check-cast p2, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$ProtoAdapter_ProtectedFideliusTokenWithCategory;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;)I
    .locals 4

    .line 346
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;->category:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;->token:Ljava/lang/String;

    const/4 v3, 0x2

    .line 347
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 348
    invoke-virtual {p1}, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 339
    check-cast p1, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$ProtoAdapter_ProtectedFideliusTokenWithCategory;->encodedSize(Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;)Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;
    .locals 0

    .line 378
    invoke-virtual {p1}, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;->newBuilder()Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;

    move-result-object p1

    .line 379
    invoke-virtual {p1}, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 380
    invoke-virtual {p1}, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$Builder;->build()Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 339
    check-cast p1, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory$ProtoAdapter_ProtectedFideliusTokenWithCategory;->redact(Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;)Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    move-result-object p1

    return-object p1
.end method
