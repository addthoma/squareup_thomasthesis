.class public final Lcom/squareup/protos/multipass/common/SmsTwoFactor;
.super Lcom/squareup/wire/Message;
.source "SmsTwoFactor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/multipass/common/SmsTwoFactor$ProtoAdapter_SmsTwoFactor;,
        Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/multipass/common/SmsTwoFactor;",
        "Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/multipass/common/SmsTwoFactor;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PHONE:Ljava/lang/String; = ""

.field public static final DEFAULT_VERIFICATION_CODE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final phone:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final verification_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/multipass/common/SmsTwoFactor$ProtoAdapter_SmsTwoFactor;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/common/SmsTwoFactor$ProtoAdapter_SmsTwoFactor;-><init>()V

    sput-object v0, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 50
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/multipass/common/SmsTwoFactor;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 55
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->phone:Ljava/lang/String;

    .line 56
    iput-object p2, p0, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->verification_code:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 71
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 72
    :cond_1
    check-cast p1, Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    .line 73
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->phone:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->phone:Ljava/lang/String;

    .line 74
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->verification_code:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->verification_code:Ljava/lang/String;

    .line 75
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 80
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->phone:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->verification_code:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 85
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;
    .locals 2

    .line 61
    new-instance v0, Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;-><init>()V

    .line 62
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->phone:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;->phone:Ljava/lang/String;

    .line 63
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->verification_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;->verification_code:Ljava/lang/String;

    .line 64
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->newBuilder()Lcom/squareup/protos/multipass/common/SmsTwoFactor$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->phone:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", phone=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->verification_code:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", verification_code=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SmsTwoFactor{"

    .line 95
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
