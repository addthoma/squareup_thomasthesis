.class public final Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AlertButtonActionDismiss.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;",
        "Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss$Builder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 57
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;
    .locals 2

    .line 62
    new-instance v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;-><init>(Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 56
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss$Builder;->build()Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    move-result-object v0

    return-object v0
.end method
