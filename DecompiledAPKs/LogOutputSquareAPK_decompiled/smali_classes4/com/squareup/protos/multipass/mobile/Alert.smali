.class public final Lcom/squareup/protos/multipass/mobile/Alert;
.super Lcom/squareup/wire/Message;
.source "Alert.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/multipass/mobile/Alert$ProtoAdapter_Alert;,
        Lcom/squareup/protos/multipass/mobile/Alert$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/multipass/mobile/Alert;",
        "Lcom/squareup/protos/multipass/mobile/Alert$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/multipass/mobile/Alert;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_TITLE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final other_buttons:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.mobile.AlertButton#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/mobile/AlertButton;",
            ">;"
        }
    .end annotation
.end field

.field public final primary_button:Lcom/squareup/protos/multipass/mobile/AlertButton;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.mobile.AlertButton#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/multipass/mobile/Alert$ProtoAdapter_Alert;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/mobile/Alert$ProtoAdapter_Alert;-><init>()V

    sput-object v0, Lcom/squareup/protos/multipass/mobile/Alert;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/multipass/mobile/AlertButton;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/multipass/mobile/AlertButton;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/mobile/AlertButton;",
            ">;)V"
        }
    .end annotation

    .line 72
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/multipass/mobile/Alert;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/multipass/mobile/AlertButton;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/multipass/mobile/AlertButton;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/multipass/mobile/AlertButton;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/mobile/AlertButton;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 77
    sget-object v0, Lcom/squareup/protos/multipass/mobile/Alert;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 78
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->title:Ljava/lang/String;

    .line 79
    iput-object p2, p0, Lcom/squareup/protos/multipass/mobile/Alert;->message:Ljava/lang/String;

    .line 80
    iput-object p3, p0, Lcom/squareup/protos/multipass/mobile/Alert;->primary_button:Lcom/squareup/protos/multipass/mobile/AlertButton;

    const-string p1, "other_buttons"

    .line 81
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->other_buttons:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 98
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/multipass/mobile/Alert;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 99
    :cond_1
    check-cast p1, Lcom/squareup/protos/multipass/mobile/Alert;

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/mobile/Alert;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/multipass/mobile/Alert;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/multipass/mobile/Alert;->title:Ljava/lang/String;

    .line 101
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->message:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/multipass/mobile/Alert;->message:Ljava/lang/String;

    .line 102
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->primary_button:Lcom/squareup/protos/multipass/mobile/AlertButton;

    iget-object v3, p1, Lcom/squareup/protos/multipass/mobile/Alert;->primary_button:Lcom/squareup/protos/multipass/mobile/AlertButton;

    .line 103
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->other_buttons:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/multipass/mobile/Alert;->other_buttons:Ljava/util/List;

    .line 104
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 109
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/mobile/Alert;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->title:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->message:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->primary_button:Lcom/squareup/protos/multipass/mobile/AlertButton;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/mobile/AlertButton;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->other_buttons:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 116
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/multipass/mobile/Alert$Builder;
    .locals 2

    .line 86
    new-instance v0, Lcom/squareup/protos/multipass/mobile/Alert$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/mobile/Alert$Builder;-><init>()V

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/multipass/mobile/Alert$Builder;->title:Ljava/lang/String;

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/multipass/mobile/Alert$Builder;->message:Ljava/lang/String;

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->primary_button:Lcom/squareup/protos/multipass/mobile/AlertButton;

    iput-object v1, v0, Lcom/squareup/protos/multipass/mobile/Alert$Builder;->primary_button:Lcom/squareup/protos/multipass/mobile/AlertButton;

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->other_buttons:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/multipass/mobile/Alert$Builder;->other_buttons:Ljava/util/List;

    .line 91
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/mobile/Alert;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/multipass/mobile/Alert$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/mobile/Alert;->newBuilder()Lcom/squareup/protos/multipass/mobile/Alert$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->title:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->message:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->primary_button:Lcom/squareup/protos/multipass/mobile/AlertButton;

    if-eqz v1, :cond_2

    const-string v1, ", primary_button="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->primary_button:Lcom/squareup/protos/multipass/mobile/AlertButton;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 127
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->other_buttons:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", other_buttons="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/Alert;->other_buttons:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Alert{"

    .line 128
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
