.class public final enum Lcom/squareup/protos/connect/v2/resources/Country;
.super Ljava/lang/Enum;
.source "Country.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Country$ProtoAdapter_Country;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/resources/Country;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum AD:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Country;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AE:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum AF:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum AG:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum AI:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum AL:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum AM:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum AO:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum AQ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum AR:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum AS:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum AT:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum AU:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum AW:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum AX:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum AZ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BA:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BB:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BD:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BE:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BF:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BG:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BH:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BI:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BJ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BL:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BM:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BN:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BO:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BQ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BR:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BS:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BT:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BV:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BW:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BY:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum BZ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CA:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CC:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CD:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CF:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CG:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CH:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CI:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CK:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CL:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CM:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CN:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CO:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CR:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CU:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CV:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CW:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CX:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CY:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum CZ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum DE:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum DJ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum DK:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum DM:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum DO:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum DZ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum EC:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum EE:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum EG:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum EH:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum ER:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum ES:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum ET:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum FI:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum FJ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum FK:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum FM:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum FO:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum FR:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GA:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GB:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GD:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GE:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GF:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GG:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GH:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GI:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GL:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GM:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GN:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GP:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GQ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GR:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GS:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GT:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GU:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GW:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum GY:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum HK:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum HM:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum HN:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum HR:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum HT:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum HU:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum ID:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum IE:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum IL:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum IM:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum IN:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum IO:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum IQ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum IR:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum IS:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum IT:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum JE:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum JM:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum JO:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum JP:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum KE:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum KG:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum KH:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum KI:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum KM:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum KN:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum KP:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum KR:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum KW:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum KY:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum KZ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum LA:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum LB:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum LC:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum LI:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum LK:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum LR:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum LS:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum LT:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum LU:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum LV:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum LY:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MA:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MC:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MD:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum ME:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MF:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MG:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MH:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MK:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum ML:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MM:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MN:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MO:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MP:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MQ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MR:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MS:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MT:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MU:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MV:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MW:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MX:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MY:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum MZ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum NA:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum NC:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum NE:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum NF:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum NG:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum NI:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum NL:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum NO:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum NP:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum NR:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum NU:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum NZ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum OM:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum PA:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum PE:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum PF:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum PG:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum PH:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum PK:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum PL:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum PM:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum PN:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum PR:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum PS:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum PT:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum PW:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum PY:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum QA:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum RE:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum RO:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum RS:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum RU:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum RW:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SA:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SB:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SC:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SD:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SE:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SG:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SH:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SI:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SJ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SK:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SL:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SM:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SN:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SO:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SR:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SS:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum ST:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SV:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SX:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SY:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum SZ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum TC:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum TD:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum TF:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum TG:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum TH:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum TJ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum TK:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum TL:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum TM:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum TN:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum TO:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum TR:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum TT:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum TV:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum TW:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum TZ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum UA:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum UG:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum UM:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum US:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum UY:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum UZ:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum VA:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum VC:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum VE:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum VG:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum VI:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum VN:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum VU:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum WF:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum WS:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum YE:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum YT:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum ZA:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum ZM:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum ZW:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final enum ZZ:Lcom/squareup/protos/connect/v2/resources/Country;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 23
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const/4 v1, 0x0

    const-string v2, "ZZ"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->ZZ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 25
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x14

    const-string v3, "AD"

    const/4 v4, 0x1

    invoke-direct {v0, v3, v4, v2}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->AD:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 27
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v3, "AE"

    const/4 v4, 0x2

    const/16 v5, 0x310

    invoke-direct {v0, v3, v4, v5}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->AE:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 29
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const/4 v3, 0x4

    const-string v4, "AF"

    const/4 v5, 0x3

    invoke-direct {v0, v4, v5, v3}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->AF:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 31
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v4, 0x1c

    const-string v5, "AG"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->AG:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 33
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "AI"

    const/4 v6, 0x5

    const/16 v7, 0x294

    invoke-direct {v0, v5, v6, v7}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->AI:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 35
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v5, 0x8

    const-string v6, "AL"

    const/4 v7, 0x6

    invoke-direct {v0, v6, v7, v5}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->AL:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 37
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v6, 0x33

    const-string v7, "AM"

    const/4 v8, 0x7

    invoke-direct {v0, v7, v8, v6}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->AM:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 39
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v7, 0x18

    const-string v8, "AO"

    invoke-direct {v0, v8, v5, v7}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->AO:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 41
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v8, 0xa

    const-string v9, "AQ"

    const/16 v10, 0x9

    invoke-direct {v0, v9, v10, v8}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->AQ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 43
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v9, 0x20

    const-string v10, "AR"

    invoke-direct {v0, v10, v8, v9}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->AR:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 45
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v10, 0x10

    const-string v11, "AS"

    const/16 v12, 0xb

    invoke-direct {v0, v11, v12, v10}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->AS:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 47
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v11, 0x28

    const/16 v12, 0xc

    const-string v13, "AT"

    invoke-direct {v0, v13, v12, v11}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->AT:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 49
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v13, 0x24

    const-string v14, "AU"

    const/16 v15, 0xd

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->AU:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 51
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v14, "AW"

    const/16 v15, 0xe

    const/16 v8, 0x215

    invoke-direct {v0, v14, v15, v8}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->AW:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 53
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v8, 0xf8

    const-string v14, "AX"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v15, v8}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->AX:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 55
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v14, 0x1f

    const-string v15, "AZ"

    invoke-direct {v0, v15, v10, v14}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->AZ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 57
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v15, "BA"

    const/16 v10, 0x11

    const/16 v5, 0x46

    invoke-direct {v0, v15, v10, v5}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BA:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 59
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BB"

    const/16 v10, 0x12

    const/16 v15, 0x34

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BB:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 61
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BD"

    const/16 v10, 0x13

    const/16 v15, 0x32

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BD:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 63
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BE"

    const/16 v10, 0x38

    invoke-direct {v0, v5, v2, v10}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BE:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 65
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BF"

    const/16 v10, 0x15

    const/16 v15, 0x356

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BF:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 67
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BG"

    const/16 v10, 0x16

    const/16 v15, 0x64

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BG:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 69
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BH"

    const/16 v10, 0x17

    const/16 v15, 0x30

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BH:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 71
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BI"

    const/16 v10, 0x6c

    invoke-direct {v0, v5, v7, v10}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BI:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 73
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BJ"

    const/16 v10, 0x19

    const/16 v15, 0xcc

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BJ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 75
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BL"

    const/16 v10, 0x1a

    const/16 v15, 0x28c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BL:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 77
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BM"

    const/16 v10, 0x1b

    const/16 v15, 0x3c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BM:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 79
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BN"

    const/16 v10, 0x60

    invoke-direct {v0, v5, v4, v10}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BN:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 81
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BO"

    const/16 v10, 0x1d

    const/16 v15, 0x44

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BO:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 83
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BQ"

    const/16 v10, 0x1e

    const/16 v15, 0x217

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BQ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 85
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BR"

    const/16 v10, 0x4c

    invoke-direct {v0, v5, v14, v10}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BR:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 87
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BS"

    const/16 v10, 0x2c

    invoke-direct {v0, v5, v9, v10}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BS:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 89
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BT"

    const/16 v10, 0x21

    const/16 v15, 0x40

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BT:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 91
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BV"

    const/16 v10, 0x22

    const/16 v15, 0x4a

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BV:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 93
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BW"

    const/16 v10, 0x23

    const/16 v15, 0x48

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BW:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 95
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BY"

    const/16 v10, 0x70

    invoke-direct {v0, v5, v13, v10}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BY:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 97
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "BZ"

    const/16 v10, 0x25

    const/16 v15, 0x54

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->BZ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 99
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CA"

    const/16 v10, 0x26

    const/16 v15, 0x7c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CA:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 101
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CC"

    const/16 v10, 0x27

    const/16 v15, 0xa6

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CC:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 103
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CD"

    const/16 v10, 0xb4

    invoke-direct {v0, v5, v11, v10}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CD:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 105
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CF"

    const/16 v10, 0x29

    const/16 v15, 0x8c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CF:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 107
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CG"

    const/16 v10, 0x2a

    const/16 v15, 0xb2

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CG:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 109
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CH"

    const/16 v10, 0x2b

    const/16 v15, 0x2f4

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CH:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 111
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CI"

    const/16 v10, 0x2c

    const/16 v15, 0x180

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CI:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 113
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CK"

    const/16 v10, 0x2d

    const/16 v15, 0xb8

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CK:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 115
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CL"

    const/16 v10, 0x2e

    const/16 v15, 0x98

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CL:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 117
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CM"

    const/16 v10, 0x2f

    const/16 v15, 0x78

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CM:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 119
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CN"

    const/16 v10, 0x30

    const/16 v15, 0x9c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CN:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 121
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CO"

    const/16 v10, 0x31

    const/16 v15, 0xaa

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CO:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 123
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CR"

    const/16 v10, 0x32

    const/16 v15, 0xbc

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CR:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 125
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CU"

    const/16 v10, 0xc0

    invoke-direct {v0, v5, v6, v10}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CU:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 127
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CV"

    const/16 v10, 0x34

    const/16 v15, 0x84

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CV:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 129
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CW"

    const/16 v10, 0x35

    const/16 v15, 0x213

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CW:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 131
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CX"

    const/16 v10, 0x36

    const/16 v15, 0xa2

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CX:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 133
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CY"

    const/16 v10, 0x37

    const/16 v15, 0xc4

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CY:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 135
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "CZ"

    const/16 v10, 0x38

    const/16 v15, 0xcb

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->CZ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 137
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "DE"

    const/16 v10, 0x39

    const/16 v15, 0x114

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->DE:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 139
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "DJ"

    const/16 v10, 0x3a

    const/16 v15, 0x106

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->DJ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 141
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "DK"

    const/16 v10, 0x3b

    const/16 v15, 0xd0

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->DK:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 143
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "DM"

    const/16 v10, 0x3c

    const/16 v15, 0xd4

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->DM:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 145
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "DO"

    const/16 v10, 0x3d

    const/16 v15, 0xd6

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->DO:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 147
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "DZ"

    const/16 v10, 0x3e

    invoke-direct {v0, v5, v10, v12}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->DZ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 149
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "EC"

    const/16 v10, 0x3f

    const/16 v15, 0xda

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->EC:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 151
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "EE"

    const/16 v10, 0x40

    const/16 v15, 0xe9

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->EE:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 153
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "EG"

    const/16 v10, 0x41

    const/16 v15, 0x332

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->EG:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 155
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "EH"

    const/16 v10, 0x42

    const/16 v15, 0x2dc

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->EH:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 157
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "ER"

    const/16 v10, 0x43

    const/16 v15, 0xe8

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->ER:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 159
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "ES"

    const/16 v10, 0x44

    const/16 v15, 0x2d4

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->ES:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 161
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "ET"

    const/16 v10, 0x45

    const/16 v15, 0xe7

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->ET:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 163
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "FI"

    const/16 v10, 0x46

    const/16 v15, 0xf6

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->FI:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 165
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "FJ"

    const/16 v10, 0x47

    const/16 v15, 0xf2

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->FJ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 167
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "FK"

    const/16 v10, 0x48

    const/16 v15, 0xee

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->FK:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 169
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "FM"

    const/16 v10, 0x49

    const/16 v15, 0x247

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->FM:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 171
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "FO"

    const/16 v10, 0x4a

    const/16 v15, 0xea

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->FO:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 173
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "FR"

    const/16 v10, 0x4b

    const/16 v15, 0xfa

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->FR:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 175
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GA"

    const/16 v10, 0x4c

    const/16 v15, 0x10a

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GA:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 177
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GB"

    const/16 v10, 0x4d

    const/16 v15, 0x33a

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GB:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 179
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GD"

    const/16 v10, 0x4e

    const/16 v15, 0x134

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GD:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 181
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GE"

    const/16 v10, 0x4f

    const/16 v15, 0x10c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GE:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 183
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GF"

    const/16 v10, 0x50

    const/16 v15, 0xfe

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GF:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 185
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GG"

    const/16 v10, 0x51

    const/16 v15, 0x33f

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GG:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 187
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GH"

    const/16 v10, 0x52

    const/16 v15, 0x120

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GH:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 189
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GI"

    const/16 v10, 0x53

    const/16 v15, 0x124

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GI:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 191
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GL"

    const/16 v10, 0x54

    const/16 v15, 0x130

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GL:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 193
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GM"

    const/16 v10, 0x55

    const/16 v15, 0x10e

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GM:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 195
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GN"

    const/16 v10, 0x56

    const/16 v15, 0x144

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GN:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 197
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GP"

    const/16 v10, 0x57

    const/16 v15, 0x138

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GP:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 199
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GQ"

    const/16 v10, 0x58

    const/16 v15, 0xe2

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GQ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 201
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GR"

    const/16 v10, 0x59

    const/16 v15, 0x12c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GR:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 203
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GS"

    const/16 v10, 0x5a

    const/16 v15, 0xef

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GS:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 205
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GT"

    const/16 v10, 0x5b

    const/16 v15, 0x140

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GT:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 207
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GU"

    const/16 v10, 0x5c

    const/16 v15, 0x13c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GU:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 209
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GW"

    const/16 v10, 0x5d

    const/16 v15, 0x270

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GW:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 211
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "GY"

    const/16 v10, 0x5e

    const/16 v15, 0x148

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->GY:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 213
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "HK"

    const/16 v10, 0x5f

    const/16 v15, 0x158

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->HK:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 215
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "HM"

    const/16 v10, 0x60

    const/16 v15, 0x14e

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->HM:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 217
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "HN"

    const/16 v10, 0x61

    const/16 v15, 0x154

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->HN:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 219
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "HR"

    const/16 v10, 0x62

    const/16 v15, 0xbf

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->HR:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 221
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "HT"

    const/16 v10, 0x63

    const/16 v15, 0x14c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->HT:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 223
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "HU"

    const/16 v10, 0x64

    const/16 v15, 0x15c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->HU:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 225
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "ID"

    const/16 v10, 0x65

    const/16 v15, 0x168

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->ID:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 227
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "IE"

    const/16 v10, 0x66

    const/16 v15, 0x174

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->IE:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 229
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "IL"

    const/16 v10, 0x67

    const/16 v15, 0x178

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->IL:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 231
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "IM"

    const/16 v10, 0x68

    const/16 v15, 0x341

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->IM:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 233
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "IN"

    const/16 v10, 0x69

    const/16 v15, 0x164

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->IN:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 235
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "IO"

    const/16 v10, 0x6a

    const/16 v15, 0x56

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->IO:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 237
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "IQ"

    const/16 v10, 0x6b

    const/16 v15, 0x170

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->IQ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 239
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "IR"

    const/16 v10, 0x6c

    const/16 v15, 0x16c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->IR:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 241
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "IS"

    const/16 v10, 0x6d

    const/16 v15, 0x160

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->IS:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 243
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "IT"

    const/16 v10, 0x6e

    const/16 v15, 0x17c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->IT:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 245
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "JE"

    const/16 v10, 0x6f

    const/16 v15, 0x340

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->JE:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 247
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "JM"

    const/16 v10, 0x70

    const/16 v15, 0x184

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->JM:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 249
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "JO"

    const/16 v10, 0x71

    const/16 v15, 0x190

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->JO:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 251
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "JP"

    const/16 v10, 0x72

    const/16 v15, 0x188

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->JP:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 253
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "KE"

    const/16 v10, 0x73

    const/16 v15, 0x194

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->KE:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 255
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "KG"

    const/16 v10, 0x74

    const/16 v15, 0x1a1

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->KG:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 257
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "KH"

    const/16 v10, 0x75

    const/16 v15, 0x74

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->KH:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 259
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "KI"

    const/16 v10, 0x76

    const/16 v15, 0x128

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->KI:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 261
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "KM"

    const/16 v10, 0x77

    const/16 v15, 0xae

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->KM:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 263
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "KN"

    const/16 v10, 0x78

    const/16 v15, 0x293

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->KN:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 265
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "KP"

    const/16 v10, 0x79

    const/16 v15, 0x198

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->KP:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 267
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "KR"

    const/16 v10, 0x7a

    const/16 v15, 0x19a

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->KR:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 269
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "KW"

    const/16 v10, 0x7b

    const/16 v15, 0x19e

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->KW:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 271
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "KY"

    const/16 v10, 0x7c

    const/16 v15, 0x88

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->KY:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 273
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "KZ"

    const/16 v10, 0x7d

    const/16 v15, 0x18e

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->KZ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 275
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "LA"

    const/16 v10, 0x7e

    const/16 v15, 0x1a2

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->LA:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 277
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "LB"

    const/16 v10, 0x7f

    const/16 v15, 0x1a6

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->LB:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 279
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "LC"

    const/16 v10, 0x80

    const/16 v15, 0x296

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->LC:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 281
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "LI"

    const/16 v10, 0x81

    const/16 v15, 0x1b6

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->LI:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 283
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "LK"

    const/16 v10, 0x82

    const/16 v15, 0x90

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->LK:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 285
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "LR"

    const/16 v10, 0x83

    const/16 v15, 0x1ae

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->LR:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 287
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "LS"

    const/16 v10, 0x84

    const/16 v15, 0x1aa

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->LS:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 289
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "LT"

    const/16 v10, 0x85

    const/16 v15, 0x1b8

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->LT:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 291
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "LU"

    const/16 v10, 0x86

    const/16 v15, 0x1ba

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->LU:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 293
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "LV"

    const/16 v10, 0x87

    const/16 v15, 0x1ac

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->LV:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 295
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "LY"

    const/16 v10, 0x88

    const/16 v15, 0x1b2

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->LY:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 297
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MA"

    const/16 v10, 0x89

    const/16 v15, 0x1f8

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MA:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 299
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MC"

    const/16 v10, 0x8a

    const/16 v15, 0x1ec

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MC:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 301
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MD"

    const/16 v10, 0x8b

    const/16 v15, 0x1f2

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MD:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 303
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "ME"

    const/16 v10, 0x8c

    const/16 v15, 0x1f3

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->ME:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 305
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MF"

    const/16 v10, 0x8d

    const/16 v15, 0x297

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MF:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 307
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MG"

    const/16 v10, 0x8e

    const/16 v15, 0x1c2

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MG:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 309
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MH"

    const/16 v10, 0x8f

    const/16 v15, 0x248

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MH:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 311
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MK"

    const/16 v10, 0x90

    const/16 v15, 0x327

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MK:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 313
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "ML"

    const/16 v10, 0x91

    const/16 v15, 0x1d2

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->ML:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 315
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MM"

    const/16 v10, 0x92

    const/16 v15, 0x68

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MM:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 317
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MN"

    const/16 v10, 0x93

    const/16 v15, 0x1f0

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MN:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 319
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MO"

    const/16 v10, 0x94

    const/16 v15, 0x1be

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MO:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 321
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MP"

    const/16 v10, 0x95

    const/16 v15, 0x244

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MP:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 323
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MQ"

    const/16 v10, 0x96

    const/16 v15, 0x1da

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MQ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 325
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MR"

    const/16 v10, 0x97

    const/16 v15, 0x1de

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MR:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 327
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MS"

    const/16 v10, 0x98

    const/16 v15, 0x1f4

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MS:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 329
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MT"

    const/16 v10, 0x99

    const/16 v15, 0x1d6

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MT:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 331
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MU"

    const/16 v10, 0x9a

    const/16 v15, 0x1e0

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MU:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 333
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MV"

    const/16 v10, 0x9b

    const/16 v15, 0x1ce

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MV:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 335
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MW"

    const/16 v10, 0x9c

    const/16 v15, 0x1c6

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MW:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 337
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MX"

    const/16 v10, 0x9d

    const/16 v15, 0x1e4

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MX:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 339
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MY"

    const/16 v10, 0x9e

    const/16 v15, 0x1ca

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MY:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 341
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "MZ"

    const/16 v10, 0x9f

    const/16 v15, 0x1fc

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->MZ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 343
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "NA"

    const/16 v10, 0xa0

    const/16 v15, 0x204

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->NA:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 345
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "NC"

    const/16 v10, 0xa1

    const/16 v15, 0x21c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->NC:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 347
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "NE"

    const/16 v10, 0xa2

    const/16 v15, 0x232

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->NE:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 349
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "NF"

    const/16 v10, 0xa3

    const/16 v15, 0x23e

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->NF:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 351
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "NG"

    const/16 v10, 0xa4

    const/16 v15, 0x236

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->NG:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 353
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "NI"

    const/16 v10, 0xa5

    const/16 v15, 0x22e

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->NI:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 355
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "NL"

    const/16 v10, 0xa6

    const/16 v15, 0x210

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->NL:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 357
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "NO"

    const/16 v10, 0xa7

    const/16 v15, 0x242

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->NO:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 359
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "NP"

    const/16 v10, 0xa8

    const/16 v15, 0x20c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->NP:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 361
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "NR"

    const/16 v10, 0xa9

    const/16 v15, 0x208

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->NR:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 363
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "NU"

    const/16 v10, 0xaa

    const/16 v15, 0x23a

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->NU:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 365
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "NZ"

    const/16 v10, 0xab

    const/16 v15, 0x22a

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->NZ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 367
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "OM"

    const/16 v10, 0xac

    const/16 v15, 0x200

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->OM:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 369
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "PA"

    const/16 v10, 0xad

    const/16 v15, 0x24f

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->PA:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 371
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "PE"

    const/16 v10, 0xae

    const/16 v15, 0x25c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->PE:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 373
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "PF"

    const/16 v10, 0xaf

    const/16 v15, 0x102

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->PF:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 375
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "PG"

    const/16 v10, 0xb0

    const/16 v15, 0x256

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->PG:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 377
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "PH"

    const/16 v10, 0xb1

    const/16 v15, 0x260

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->PH:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 379
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "PK"

    const/16 v10, 0xb2

    const/16 v15, 0x24a

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->PK:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 381
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "PL"

    const/16 v10, 0xb3

    const/16 v15, 0x268

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->PL:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 383
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "PM"

    const/16 v10, 0xb4

    const/16 v15, 0x29a

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->PM:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 385
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "PN"

    const/16 v10, 0xb5

    const/16 v15, 0x264

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->PN:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 387
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "PR"

    const/16 v10, 0xb6

    const/16 v15, 0x276

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->PR:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 389
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "PS"

    const/16 v10, 0xb7

    const/16 v15, 0x113

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->PS:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 391
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "PT"

    const/16 v10, 0xb8

    const/16 v15, 0x26c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->PT:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 393
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "PW"

    const/16 v10, 0xb9

    const/16 v15, 0x249

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->PW:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 395
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "PY"

    const/16 v10, 0xba

    const/16 v15, 0x258

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->PY:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 397
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "QA"

    const/16 v10, 0xbb

    const/16 v15, 0x27a

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->QA:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 399
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "RE"

    const/16 v10, 0xbc

    const/16 v15, 0x27e

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->RE:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 401
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "RO"

    const/16 v10, 0xbd

    const/16 v15, 0x282

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->RO:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 403
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "RS"

    const/16 v10, 0xbe

    const/16 v15, 0x2b0

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->RS:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 405
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "RU"

    const/16 v10, 0xbf

    const/16 v15, 0x283

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->RU:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 407
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "RW"

    const/16 v10, 0xc0

    const/16 v15, 0x286

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->RW:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 409
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SA"

    const/16 v10, 0xc1

    const/16 v15, 0x2aa

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SA:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 411
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SB"

    const/16 v10, 0xc2

    const/16 v15, 0x5a

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SB:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 413
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SC"

    const/16 v10, 0xc3

    const/16 v15, 0x2b2

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SC:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 415
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SD"

    const/16 v10, 0xc4

    const/16 v15, 0x2d9

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SD:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 417
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SE"

    const/16 v10, 0xc5

    const/16 v15, 0x2f0

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SE:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 419
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SG"

    const/16 v10, 0xc6

    const/16 v15, 0x2be

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SG:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 421
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SH"

    const/16 v10, 0xc7

    const/16 v15, 0x28e

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SH:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 423
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SI"

    const/16 v10, 0xc8

    const/16 v15, 0x2c1

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SI:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 425
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SJ"

    const/16 v10, 0xc9

    const/16 v15, 0x2e8

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SJ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 427
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SK"

    const/16 v10, 0xca

    const/16 v15, 0x2bf

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SK:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 429
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SL"

    const/16 v10, 0xcb

    const/16 v15, 0x2b6

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SL:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 431
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SM"

    const/16 v10, 0xcc

    const/16 v15, 0x2a2

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SM:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 433
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SN"

    const/16 v10, 0xcd

    const/16 v15, 0x2ae

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SN:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 435
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SO"

    const/16 v10, 0xce

    const/16 v15, 0x2c2

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SO:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 437
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SR"

    const/16 v10, 0xcf

    const/16 v15, 0x2e4

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SR:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 439
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SS"

    const/16 v10, 0xd0

    const/16 v15, 0x2d8

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SS:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 441
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "ST"

    const/16 v10, 0xd1

    const/16 v15, 0x2a6

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->ST:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 443
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SV"

    const/16 v10, 0xd2

    const/16 v15, 0xde

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SV:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 445
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SX"

    const/16 v10, 0xd3

    const/16 v15, 0x216

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SX:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 447
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SY"

    const/16 v10, 0xd4

    const/16 v15, 0x2f8

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SY:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 449
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "SZ"

    const/16 v10, 0xd5

    const/16 v15, 0x2ec

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->SZ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 451
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "TC"

    const/16 v10, 0xd6

    const/16 v15, 0x31c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->TC:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 453
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "TD"

    const/16 v10, 0xd7

    const/16 v15, 0x94

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->TD:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 455
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "TF"

    const/16 v10, 0xd8

    const/16 v15, 0x104

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->TF:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 457
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "TG"

    const/16 v10, 0xd9

    const/16 v15, 0x300

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->TG:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 459
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "TH"

    const/16 v10, 0xda

    const/16 v15, 0x2fc

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->TH:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 461
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "TJ"

    const/16 v10, 0xdb

    const/16 v15, 0x2fa

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->TJ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 463
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "TK"

    const/16 v10, 0xdc

    const/16 v15, 0x304

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->TK:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 465
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "TL"

    const/16 v10, 0xdd

    const/16 v15, 0x272

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->TL:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 467
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "TM"

    const/16 v10, 0xde

    const/16 v15, 0x31b

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->TM:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 469
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "TN"

    const/16 v10, 0xdf

    const/16 v15, 0x314

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->TN:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 471
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "TO"

    const/16 v10, 0xe0

    const/16 v15, 0x308

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->TO:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 473
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "TR"

    const/16 v10, 0xe1

    const/16 v15, 0x318

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->TR:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 475
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "TT"

    const/16 v10, 0xe2

    const/16 v15, 0x30c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->TT:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 477
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "TV"

    const/16 v10, 0xe3

    const/16 v15, 0x31e

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->TV:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 479
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "TW"

    const/16 v10, 0xe4

    const/16 v15, 0x9e

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->TW:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 481
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "TZ"

    const/16 v10, 0xe5

    const/16 v15, 0x342

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->TZ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 483
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "UA"

    const/16 v10, 0xe6

    const/16 v15, 0x324

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->UA:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 485
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "UG"

    const/16 v10, 0xe7

    const/16 v15, 0x320

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->UG:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 487
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "UM"

    const/16 v10, 0xe8

    const/16 v15, 0x245

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->UM:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 489
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "US"

    const/16 v10, 0xe9

    const/16 v15, 0x348

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->US:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 491
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "UY"

    const/16 v10, 0xea

    const/16 v15, 0x35a

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->UY:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 493
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "UZ"

    const/16 v10, 0xeb

    const/16 v15, 0x35c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->UZ:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 495
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "VA"

    const/16 v10, 0xec

    const/16 v15, 0x150

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->VA:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 497
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "VC"

    const/16 v10, 0xed

    const/16 v15, 0x29e

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->VC:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 499
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "VE"

    const/16 v10, 0xee

    const/16 v15, 0x35e

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->VE:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 501
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "VG"

    const/16 v10, 0xef

    const/16 v15, 0x5c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->VG:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 503
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "VI"

    const/16 v10, 0xf0

    const/16 v15, 0x352

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->VI:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 505
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "VN"

    const/16 v10, 0xf1

    const/16 v15, 0x2c0

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->VN:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 507
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "VU"

    const/16 v10, 0xf2

    const/16 v15, 0x224

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->VU:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 509
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "WF"

    const/16 v10, 0xf3

    const/16 v15, 0x36c

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->WF:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 511
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "WS"

    const/16 v10, 0xf4

    const/16 v15, 0x372

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->WS:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 513
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "YE"

    const/16 v10, 0xf5

    const/16 v15, 0x377

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->YE:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 515
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "YT"

    const/16 v10, 0xf6

    const/16 v15, 0xaf

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->YT:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 517
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "ZA"

    const/16 v10, 0xf7

    const/16 v15, 0x2c6

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->ZA:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 519
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "ZM"

    const/16 v10, 0x37e

    invoke-direct {v0, v5, v8, v10}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->ZM:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 521
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country;

    const-string v5, "ZW"

    const/16 v10, 0xf9

    const/16 v15, 0x2cc

    invoke-direct {v0, v5, v10, v15}, Lcom/squareup/protos/connect/v2/resources/Country;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->ZW:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v0, 0xfa

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/resources/Country;

    .line 19
    sget-object v5, Lcom/squareup/protos/connect/v2/resources/Country;->ZZ:Lcom/squareup/protos/connect/v2/resources/Country;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->AD:Lcom/squareup/protos/connect/v2/resources/Country;

    const/4 v5, 0x1

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->AE:Lcom/squareup/protos/connect/v2/resources/Country;

    const/4 v5, 0x2

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->AF:Lcom/squareup/protos/connect/v2/resources/Country;

    const/4 v5, 0x3

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->AG:Lcom/squareup/protos/connect/v2/resources/Country;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->AI:Lcom/squareup/protos/connect/v2/resources/Country;

    const/4 v3, 0x5

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->AL:Lcom/squareup/protos/connect/v2/resources/Country;

    const/4 v3, 0x6

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->AM:Lcom/squareup/protos/connect/v2/resources/Country;

    const/4 v3, 0x7

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->AO:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v3, 0x8

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->AQ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v3, 0x9

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->AR:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v3, 0xa

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->AS:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v3, 0xb

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->AT:Lcom/squareup/protos/connect/v2/resources/Country;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->AU:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v3, 0xd

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->AW:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v3, 0xe

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->AX:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v3, 0xf

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->AZ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v3, 0x10

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BA:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v3, 0x11

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BB:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v3, 0x12

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BD:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v3, 0x13

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BE:Lcom/squareup/protos/connect/v2/resources/Country;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BF:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BG:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BH:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BI:Lcom/squareup/protos/connect/v2/resources/Country;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BJ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BL:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BM:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BN:Lcom/squareup/protos/connect/v2/resources/Country;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BO:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BQ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BR:Lcom/squareup/protos/connect/v2/resources/Country;

    aput-object v1, v0, v14

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BS:Lcom/squareup/protos/connect/v2/resources/Country;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BT:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BV:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BW:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BY:Lcom/squareup/protos/connect/v2/resources/Country;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->BZ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CA:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CC:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CD:Lcom/squareup/protos/connect/v2/resources/Country;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CF:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CG:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CH:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CI:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CK:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CL:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CM:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CN:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CO:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CR:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CU:Lcom/squareup/protos/connect/v2/resources/Country;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CV:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CW:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CX:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CY:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->CZ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->DE:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->DJ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->DK:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->DM:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->DO:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->DZ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->EC:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->EE:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->EG:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->EH:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->ER:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->ES:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->ET:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->FI:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->FJ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->FK:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->FM:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->FO:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->FR:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GA:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GB:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GD:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GE:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GF:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GG:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GH:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GI:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GL:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GM:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GN:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GP:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GQ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GR:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GS:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GT:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GU:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GW:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->GY:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->HK:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->HM:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->HN:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->HR:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->HT:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->HU:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->ID:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->IE:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->IL:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->IM:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->IN:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->IO:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->IQ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->IR:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->IS:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->IT:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->JE:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->JM:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->JO:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->JP:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->KE:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->KG:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->KH:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->KI:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->KM:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->KN:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->KP:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->KR:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->KW:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->KY:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->KZ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->LA:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->LB:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->LC:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->LI:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->LK:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->LR:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->LS:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->LT:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->LU:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->LV:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->LY:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MA:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MC:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MD:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->ME:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MF:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MG:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MH:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MK:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->ML:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MM:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MN:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MO:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MP:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MQ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MR:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MS:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MT:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MU:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MV:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MW:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MX:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MY:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->MZ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->NA:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->NC:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->NE:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->NF:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->NG:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->NI:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->NL:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->NO:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->NP:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->NR:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->NU:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->NZ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xab

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->OM:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xac

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->PA:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xad

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->PE:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xae

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->PF:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->PG:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->PH:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xb1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->PK:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xb2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->PL:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xb3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->PM:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xb4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->PN:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xb5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->PR:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xb6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->PS:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xb7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->PT:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xb8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->PW:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xb9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->PY:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xba

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->QA:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xbb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->RE:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xbc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->RO:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xbd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->RS:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xbe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->RU:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xbf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->RW:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xc0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SA:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xc1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SB:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xc2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SC:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xc3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SD:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xc4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SE:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xc5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SG:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xc6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SH:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xc7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SI:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xc8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SJ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xc9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SK:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xca

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SL:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xcb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SM:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xcc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SN:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xcd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SO:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xce

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SR:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xcf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SS:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xd0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->ST:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xd1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SV:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xd2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SX:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xd3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SY:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xd4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->SZ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xd5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->TC:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xd6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->TD:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xd7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->TF:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xd8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->TG:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xd9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->TH:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xda

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->TJ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xdb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->TK:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xdc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->TL:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xdd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->TM:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xde

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->TN:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xdf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->TO:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xe0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->TR:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xe1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->TT:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xe2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->TV:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xe3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->TW:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xe4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->TZ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xe5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->UA:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xe6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->UG:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xe7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->UM:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xe8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->US:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xe9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->UY:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xea

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->UZ:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xeb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->VA:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xec

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->VC:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xed

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->VE:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xee

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->VG:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xef

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->VI:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xf0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->VN:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xf1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->VU:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xf2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->WF:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xf3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->WS:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xf4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->YE:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xf5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->YT:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xf6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->ZA:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xf7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->ZM:Lcom/squareup/protos/connect/v2/resources/Country;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Country;->ZW:Lcom/squareup/protos/connect/v2/resources/Country;

    const/16 v2, 0xf9

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Country;

    .line 523
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Country$ProtoAdapter_Country;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Country$ProtoAdapter_Country;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 527
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 528
    iput p3, p0, Lcom/squareup/protos/connect/v2/resources/Country;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/resources/Country;
    .locals 0

    sparse-switch p0, :sswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 784
    :sswitch_0
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->ZM:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 781
    :sswitch_1
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->YE:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 780
    :sswitch_2
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->WS:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 779
    :sswitch_3
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->WF:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 774
    :sswitch_4
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->VE:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 771
    :sswitch_5
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->UZ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 770
    :sswitch_6
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->UY:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 557
    :sswitch_7
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BF:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 776
    :sswitch_8
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->VI:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 769
    :sswitch_9
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->US:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 765
    :sswitch_a
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->TZ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 640
    :sswitch_b
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->IM:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 647
    :sswitch_c
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->JE:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 617
    :sswitch_d
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GG:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 613
    :sswitch_e
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GB:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 601
    :sswitch_f
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->EG:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 680
    :sswitch_10
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MK:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 766
    :sswitch_11
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->UA:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 767
    :sswitch_12
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->UG:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 763
    :sswitch_13
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->TV:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 750
    :sswitch_14
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->TC:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 758
    :sswitch_15
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->TM:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 761
    :sswitch_16
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->TR:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 759
    :sswitch_17
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->TN:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 538
    :sswitch_18
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->AE:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 762
    :sswitch_19
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->TT:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 760
    :sswitch_1a
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->TO:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 756
    :sswitch_1b
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->TK:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 753
    :sswitch_1c
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->TG:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 754
    :sswitch_1d
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->TH:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 755
    :sswitch_1e
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->TJ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 748
    :sswitch_1f
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SY:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 579
    :sswitch_20
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CH:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 733
    :sswitch_21
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SE:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 749
    :sswitch_22
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SZ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 737
    :sswitch_23
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SJ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 743
    :sswitch_24
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SR:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 602
    :sswitch_25
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->EH:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 732
    :sswitch_26
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SD:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 744
    :sswitch_27
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SS:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 604
    :sswitch_28
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->ES:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 785
    :sswitch_29
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->ZW:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 783
    :sswitch_2a
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->ZA:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 742
    :sswitch_2b
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SO:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 736
    :sswitch_2c
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SI:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 777
    :sswitch_2d
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->VN:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 738
    :sswitch_2e
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SK:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 734
    :sswitch_2f
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SG:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 739
    :sswitch_30
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SL:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 731
    :sswitch_31
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SC:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 726
    :sswitch_32
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->RS:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 741
    :sswitch_33
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SN:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 729
    :sswitch_34
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SA:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 745
    :sswitch_35
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->ST:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 740
    :sswitch_36
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SM:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 773
    :sswitch_37
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->VC:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 716
    :sswitch_38
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->PM:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 677
    :sswitch_39
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MF:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 664
    :sswitch_3a
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->LC:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 541
    :sswitch_3b
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->AI:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 656
    :sswitch_3c
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->KN:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 735
    :sswitch_3d
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SH:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 562
    :sswitch_3e
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BL:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 728
    :sswitch_3f
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->RW:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 727
    :sswitch_40
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->RU:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 725
    :sswitch_41
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->RO:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 724
    :sswitch_42
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->RE:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 723
    :sswitch_43
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->QA:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 718
    :sswitch_44
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->PR:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 757
    :sswitch_45
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->TL:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 629
    :sswitch_46
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GW:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 720
    :sswitch_47
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->PT:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 715
    :sswitch_48
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->PL:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 717
    :sswitch_49
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->PN:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 713
    :sswitch_4a
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->PH:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 710
    :sswitch_4b
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->PE:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 722
    :sswitch_4c
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->PY:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 712
    :sswitch_4d
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->PG:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 709
    :sswitch_4e
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->PA:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 714
    :sswitch_4f
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->PK:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 721
    :sswitch_50
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->PW:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 679
    :sswitch_51
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MH:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 609
    :sswitch_52
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->FM:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 768
    :sswitch_53
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->UM:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 685
    :sswitch_54
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MP:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 703
    :sswitch_55
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->NO:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 699
    :sswitch_56
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->NF:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 706
    :sswitch_57
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->NU:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 700
    :sswitch_58
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->NG:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 698
    :sswitch_59
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->NE:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 701
    :sswitch_5a
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->NI:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 707
    :sswitch_5b
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->NZ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 778
    :sswitch_5c
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->VU:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 697
    :sswitch_5d
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->NC:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 566
    :sswitch_5e
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BQ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 747
    :sswitch_5f
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SX:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 550
    :sswitch_60
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->AW:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 589
    :sswitch_61
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CW:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 702
    :sswitch_62
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->NL:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 704
    :sswitch_63
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->NP:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 705
    :sswitch_64
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->NR:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 696
    :sswitch_65
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->NA:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 708
    :sswitch_66
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->OM:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 695
    :sswitch_67
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MZ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 673
    :sswitch_68
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MA:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 688
    :sswitch_69
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MS:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 676
    :sswitch_6a
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->ME:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 675
    :sswitch_6b
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MD:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 683
    :sswitch_6c
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MN:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 674
    :sswitch_6d
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MC:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 693
    :sswitch_6e
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MX:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 690
    :sswitch_6f
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MU:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 687
    :sswitch_70
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MR:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 686
    :sswitch_71
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MQ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 689
    :sswitch_72
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MT:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 681
    :sswitch_73
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->ML:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 691
    :sswitch_74
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MV:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 694
    :sswitch_75
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MY:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 692
    :sswitch_76
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MW:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 678
    :sswitch_77
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MG:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 684
    :sswitch_78
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MO:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 670
    :sswitch_79
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->LU:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 669
    :sswitch_7a
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->LT:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 665
    :sswitch_7b
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->LI:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 672
    :sswitch_7c
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->LY:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 667
    :sswitch_7d
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->LR:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 671
    :sswitch_7e
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->LV:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 668
    :sswitch_7f
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->LS:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 663
    :sswitch_80
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->LB:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 662
    :sswitch_81
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->LA:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 652
    :sswitch_82
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->KG:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 659
    :sswitch_83
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->KW:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 658
    :sswitch_84
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->KR:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 657
    :sswitch_85
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->KP:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 651
    :sswitch_86
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->KE:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 649
    :sswitch_87
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->JO:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 661
    :sswitch_88
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->KZ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 650
    :sswitch_89
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->JP:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 648
    :sswitch_8a
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->JM:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 580
    :sswitch_8b
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CI:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 646
    :sswitch_8c
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->IT:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 639
    :sswitch_8d
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->IL:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 638
    :sswitch_8e
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->IE:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 643
    :sswitch_8f
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->IQ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 644
    :sswitch_90
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->IR:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 637
    :sswitch_91
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->ID:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 641
    :sswitch_92
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->IN:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 645
    :sswitch_93
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->IS:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 636
    :sswitch_94
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->HU:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 631
    :sswitch_95
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->HK:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 633
    :sswitch_96
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->HN:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 772
    :sswitch_97
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->VA:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 632
    :sswitch_98
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->HM:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 635
    :sswitch_99
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->HT:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 630
    :sswitch_9a
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GY:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 622
    :sswitch_9b
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GN:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 627
    :sswitch_9c
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GT:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 628
    :sswitch_9d
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GU:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 623
    :sswitch_9e
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GP:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 614
    :sswitch_9f
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GD:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 620
    :sswitch_a0
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GL:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 625
    :sswitch_a1
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GR:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 654
    :sswitch_a2
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->KI:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 619
    :sswitch_a3
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GI:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 618
    :sswitch_a4
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GH:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 593
    :sswitch_a5
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->DE:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 719
    :sswitch_a6
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->PS:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 621
    :sswitch_a7
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GM:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 615
    :sswitch_a8
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GE:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 612
    :sswitch_a9
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GA:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 594
    :sswitch_aa
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->DJ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 752
    :sswitch_ab
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->TF:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 711
    :sswitch_ac
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->PF:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 616
    :sswitch_ad
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GF:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 611
    :sswitch_ae
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->FR:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 551
    :sswitch_af
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->AX:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 606
    :sswitch_b0
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->FI:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 607
    :sswitch_b1
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->FJ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 626
    :sswitch_b2
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GS:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 608
    :sswitch_b3
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->FK:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 610
    :sswitch_b4
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->FO:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 600
    :sswitch_b5
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->EE:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 603
    :sswitch_b6
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->ER:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 605
    :sswitch_b7
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->ET:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 624
    :sswitch_b8
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->GQ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 746
    :sswitch_b9
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SV:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 599
    :sswitch_ba
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->EC:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 597
    :sswitch_bb
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->DO:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 596
    :sswitch_bc
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->DM:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 595
    :sswitch_bd
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->DK:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 561
    :sswitch_be
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BJ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 592
    :sswitch_bf
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CZ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 591
    :sswitch_c0
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CY:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 587
    :sswitch_c1
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CU:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 634
    :sswitch_c2
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->HR:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 586
    :sswitch_c3
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CR:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 581
    :sswitch_c4
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CK:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 576
    :sswitch_c5
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CD:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 578
    :sswitch_c6
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CG:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 782
    :sswitch_c7
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->YT:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 655
    :sswitch_c8
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->KM:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 585
    :sswitch_c9
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CO:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 575
    :sswitch_ca
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CC:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 590
    :sswitch_cb
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CX:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 764
    :sswitch_cc
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->TW:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 584
    :sswitch_cd
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CN:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 582
    :sswitch_ce
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CL:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 751
    :sswitch_cf
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->TD:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 666
    :sswitch_d0
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->LK:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 577
    :sswitch_d1
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CF:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 660
    :sswitch_d2
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->KY:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 588
    :sswitch_d3
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CV:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 574
    :sswitch_d4
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CA:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 583
    :sswitch_d5
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->CM:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 653
    :sswitch_d6
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->KH:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 572
    :sswitch_d7
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BY:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 560
    :sswitch_d8
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BI:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 682
    :sswitch_d9
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->MM:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 558
    :sswitch_da
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BG:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 564
    :sswitch_db
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BN:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 775
    :sswitch_dc
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->VG:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 730
    :sswitch_dd
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->SB:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 642
    :sswitch_de
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->IO:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 573
    :sswitch_df
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BZ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 567
    :sswitch_e0
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BR:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 570
    :sswitch_e1
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BV:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 571
    :sswitch_e2
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BW:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 553
    :sswitch_e3
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BA:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 565
    :sswitch_e4
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BO:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 569
    :sswitch_e5
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BT:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 563
    :sswitch_e6
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BM:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 556
    :sswitch_e7
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BE:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 554
    :sswitch_e8
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BB:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 543
    :sswitch_e9
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->AM:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 555
    :sswitch_ea
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BD:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 559
    :sswitch_eb
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BH:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 568
    :sswitch_ec
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->BS:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 548
    :sswitch_ed
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->AT:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 549
    :sswitch_ee
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->AU:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 546
    :sswitch_ef
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->AR:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 552
    :sswitch_f0
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->AZ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 540
    :sswitch_f1
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->AG:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 544
    :sswitch_f2
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->AO:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 537
    :sswitch_f3
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->AD:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 547
    :sswitch_f4
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->AS:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 598
    :sswitch_f5
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->DZ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 545
    :sswitch_f6
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->AQ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 542
    :sswitch_f7
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->AL:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 539
    :sswitch_f8
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->AF:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    .line 536
    :sswitch_f9
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Country;->ZZ:Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_f9
        0x4 -> :sswitch_f8
        0x8 -> :sswitch_f7
        0xa -> :sswitch_f6
        0xc -> :sswitch_f5
        0x10 -> :sswitch_f4
        0x14 -> :sswitch_f3
        0x18 -> :sswitch_f2
        0x1c -> :sswitch_f1
        0x1f -> :sswitch_f0
        0x20 -> :sswitch_ef
        0x24 -> :sswitch_ee
        0x28 -> :sswitch_ed
        0x2c -> :sswitch_ec
        0x30 -> :sswitch_eb
        0x32 -> :sswitch_ea
        0x33 -> :sswitch_e9
        0x34 -> :sswitch_e8
        0x38 -> :sswitch_e7
        0x3c -> :sswitch_e6
        0x40 -> :sswitch_e5
        0x44 -> :sswitch_e4
        0x46 -> :sswitch_e3
        0x48 -> :sswitch_e2
        0x4a -> :sswitch_e1
        0x4c -> :sswitch_e0
        0x54 -> :sswitch_df
        0x56 -> :sswitch_de
        0x5a -> :sswitch_dd
        0x5c -> :sswitch_dc
        0x60 -> :sswitch_db
        0x64 -> :sswitch_da
        0x68 -> :sswitch_d9
        0x6c -> :sswitch_d8
        0x70 -> :sswitch_d7
        0x74 -> :sswitch_d6
        0x78 -> :sswitch_d5
        0x7c -> :sswitch_d4
        0x84 -> :sswitch_d3
        0x88 -> :sswitch_d2
        0x8c -> :sswitch_d1
        0x90 -> :sswitch_d0
        0x94 -> :sswitch_cf
        0x98 -> :sswitch_ce
        0x9c -> :sswitch_cd
        0x9e -> :sswitch_cc
        0xa2 -> :sswitch_cb
        0xa6 -> :sswitch_ca
        0xaa -> :sswitch_c9
        0xae -> :sswitch_c8
        0xaf -> :sswitch_c7
        0xb2 -> :sswitch_c6
        0xb4 -> :sswitch_c5
        0xb8 -> :sswitch_c4
        0xbc -> :sswitch_c3
        0xbf -> :sswitch_c2
        0xc0 -> :sswitch_c1
        0xc4 -> :sswitch_c0
        0xcb -> :sswitch_bf
        0xcc -> :sswitch_be
        0xd0 -> :sswitch_bd
        0xd4 -> :sswitch_bc
        0xd6 -> :sswitch_bb
        0xda -> :sswitch_ba
        0xde -> :sswitch_b9
        0xe2 -> :sswitch_b8
        0xe7 -> :sswitch_b7
        0xe8 -> :sswitch_b6
        0xe9 -> :sswitch_b5
        0xea -> :sswitch_b4
        0xee -> :sswitch_b3
        0xef -> :sswitch_b2
        0xf2 -> :sswitch_b1
        0xf6 -> :sswitch_b0
        0xf8 -> :sswitch_af
        0xfa -> :sswitch_ae
        0xfe -> :sswitch_ad
        0x102 -> :sswitch_ac
        0x104 -> :sswitch_ab
        0x106 -> :sswitch_aa
        0x10a -> :sswitch_a9
        0x10c -> :sswitch_a8
        0x10e -> :sswitch_a7
        0x113 -> :sswitch_a6
        0x114 -> :sswitch_a5
        0x120 -> :sswitch_a4
        0x124 -> :sswitch_a3
        0x128 -> :sswitch_a2
        0x12c -> :sswitch_a1
        0x130 -> :sswitch_a0
        0x134 -> :sswitch_9f
        0x138 -> :sswitch_9e
        0x13c -> :sswitch_9d
        0x140 -> :sswitch_9c
        0x144 -> :sswitch_9b
        0x148 -> :sswitch_9a
        0x14c -> :sswitch_99
        0x14e -> :sswitch_98
        0x150 -> :sswitch_97
        0x154 -> :sswitch_96
        0x158 -> :sswitch_95
        0x15c -> :sswitch_94
        0x160 -> :sswitch_93
        0x164 -> :sswitch_92
        0x168 -> :sswitch_91
        0x16c -> :sswitch_90
        0x170 -> :sswitch_8f
        0x174 -> :sswitch_8e
        0x178 -> :sswitch_8d
        0x17c -> :sswitch_8c
        0x180 -> :sswitch_8b
        0x184 -> :sswitch_8a
        0x188 -> :sswitch_89
        0x18e -> :sswitch_88
        0x190 -> :sswitch_87
        0x194 -> :sswitch_86
        0x198 -> :sswitch_85
        0x19a -> :sswitch_84
        0x19e -> :sswitch_83
        0x1a1 -> :sswitch_82
        0x1a2 -> :sswitch_81
        0x1a6 -> :sswitch_80
        0x1aa -> :sswitch_7f
        0x1ac -> :sswitch_7e
        0x1ae -> :sswitch_7d
        0x1b2 -> :sswitch_7c
        0x1b6 -> :sswitch_7b
        0x1b8 -> :sswitch_7a
        0x1ba -> :sswitch_79
        0x1be -> :sswitch_78
        0x1c2 -> :sswitch_77
        0x1c6 -> :sswitch_76
        0x1ca -> :sswitch_75
        0x1ce -> :sswitch_74
        0x1d2 -> :sswitch_73
        0x1d6 -> :sswitch_72
        0x1da -> :sswitch_71
        0x1de -> :sswitch_70
        0x1e0 -> :sswitch_6f
        0x1e4 -> :sswitch_6e
        0x1ec -> :sswitch_6d
        0x1f0 -> :sswitch_6c
        0x1f2 -> :sswitch_6b
        0x1f3 -> :sswitch_6a
        0x1f4 -> :sswitch_69
        0x1f8 -> :sswitch_68
        0x1fc -> :sswitch_67
        0x200 -> :sswitch_66
        0x204 -> :sswitch_65
        0x208 -> :sswitch_64
        0x20c -> :sswitch_63
        0x210 -> :sswitch_62
        0x213 -> :sswitch_61
        0x215 -> :sswitch_60
        0x216 -> :sswitch_5f
        0x217 -> :sswitch_5e
        0x21c -> :sswitch_5d
        0x224 -> :sswitch_5c
        0x22a -> :sswitch_5b
        0x22e -> :sswitch_5a
        0x232 -> :sswitch_59
        0x236 -> :sswitch_58
        0x23a -> :sswitch_57
        0x23e -> :sswitch_56
        0x242 -> :sswitch_55
        0x244 -> :sswitch_54
        0x245 -> :sswitch_53
        0x247 -> :sswitch_52
        0x248 -> :sswitch_51
        0x249 -> :sswitch_50
        0x24a -> :sswitch_4f
        0x24f -> :sswitch_4e
        0x256 -> :sswitch_4d
        0x258 -> :sswitch_4c
        0x25c -> :sswitch_4b
        0x260 -> :sswitch_4a
        0x264 -> :sswitch_49
        0x268 -> :sswitch_48
        0x26c -> :sswitch_47
        0x270 -> :sswitch_46
        0x272 -> :sswitch_45
        0x276 -> :sswitch_44
        0x27a -> :sswitch_43
        0x27e -> :sswitch_42
        0x282 -> :sswitch_41
        0x283 -> :sswitch_40
        0x286 -> :sswitch_3f
        0x28c -> :sswitch_3e
        0x28e -> :sswitch_3d
        0x293 -> :sswitch_3c
        0x294 -> :sswitch_3b
        0x296 -> :sswitch_3a
        0x297 -> :sswitch_39
        0x29a -> :sswitch_38
        0x29e -> :sswitch_37
        0x2a2 -> :sswitch_36
        0x2a6 -> :sswitch_35
        0x2aa -> :sswitch_34
        0x2ae -> :sswitch_33
        0x2b0 -> :sswitch_32
        0x2b2 -> :sswitch_31
        0x2b6 -> :sswitch_30
        0x2be -> :sswitch_2f
        0x2bf -> :sswitch_2e
        0x2c0 -> :sswitch_2d
        0x2c1 -> :sswitch_2c
        0x2c2 -> :sswitch_2b
        0x2c6 -> :sswitch_2a
        0x2cc -> :sswitch_29
        0x2d4 -> :sswitch_28
        0x2d8 -> :sswitch_27
        0x2d9 -> :sswitch_26
        0x2dc -> :sswitch_25
        0x2e4 -> :sswitch_24
        0x2e8 -> :sswitch_23
        0x2ec -> :sswitch_22
        0x2f0 -> :sswitch_21
        0x2f4 -> :sswitch_20
        0x2f8 -> :sswitch_1f
        0x2fa -> :sswitch_1e
        0x2fc -> :sswitch_1d
        0x300 -> :sswitch_1c
        0x304 -> :sswitch_1b
        0x308 -> :sswitch_1a
        0x30c -> :sswitch_19
        0x310 -> :sswitch_18
        0x314 -> :sswitch_17
        0x318 -> :sswitch_16
        0x31b -> :sswitch_15
        0x31c -> :sswitch_14
        0x31e -> :sswitch_13
        0x320 -> :sswitch_12
        0x324 -> :sswitch_11
        0x327 -> :sswitch_10
        0x332 -> :sswitch_f
        0x33a -> :sswitch_e
        0x33f -> :sswitch_d
        0x340 -> :sswitch_c
        0x341 -> :sswitch_b
        0x342 -> :sswitch_a
        0x348 -> :sswitch_9
        0x352 -> :sswitch_8
        0x356 -> :sswitch_7
        0x35a -> :sswitch_6
        0x35c -> :sswitch_5
        0x35e -> :sswitch_4
        0x36c -> :sswitch_3
        0x372 -> :sswitch_2
        0x377 -> :sswitch_1
        0x37e -> :sswitch_0
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Country;
    .locals 1

    .line 19
    const-class v0, Lcom/squareup/protos/connect/v2/resources/Country;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/resources/Country;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/resources/Country;
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Country;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/resources/Country;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/resources/Country;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 792
    iget v0, p0, Lcom/squareup/protos/connect/v2/resources/Country;->value:I

    return v0
.end method
