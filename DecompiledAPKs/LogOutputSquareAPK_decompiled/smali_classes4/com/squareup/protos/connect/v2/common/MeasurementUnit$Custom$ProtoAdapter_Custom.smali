.class final Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$ProtoAdapter_Custom;
.super Lcom/squareup/wire/ProtoAdapter;
.source "MeasurementUnit.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Custom"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 448
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 467
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;-><init>()V

    .line 468
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 469
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 474
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 472
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;->abbreviation(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;

    goto :goto_0

    .line 471
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;

    goto :goto_0

    .line 478
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 479
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 446
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$ProtoAdapter_Custom;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 460
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;->name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 461
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;->abbreviation:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 462
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 446
    check-cast p2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$ProtoAdapter_Custom;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;)I
    .locals 4

    .line 453
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;->name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;->abbreviation:Ljava/lang/String;

    const/4 v3, 0x2

    .line 454
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 455
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 446
    check-cast p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$ProtoAdapter_Custom;->encodedSize(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;
    .locals 0

    .line 484
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;->newBuilder()Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;

    move-result-object p1

    .line 485
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 486
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 446
    check-cast p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom$ProtoAdapter_Custom;->redact(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    move-result-object p1

    return-object p1
.end method
