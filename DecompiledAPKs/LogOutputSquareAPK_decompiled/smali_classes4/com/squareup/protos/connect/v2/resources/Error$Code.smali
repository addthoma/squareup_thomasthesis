.class public final enum Lcom/squareup/protos/connect/v2/resources/Error$Code;
.super Ljava/lang/Enum;
.source "Error.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Code"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Error$Code$ProtoAdapter_Code;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/resources/Error$Code;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum ACCESS_TOKEN_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum ACCESS_TOKEN_REVOKED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Error$Code;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ADDRESS_VERIFICATION_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum ALLOWABLE_PIN_TRIES_EXCEEDED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum AMOUNT_TOO_HIGH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum API_VERSION_INCOMPATIBLE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum APPLE_PAYMENT_PROCESSING_CERTIFICATE_HASH_NOT_FOUND:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum APPLICATION_DISABLED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum ARRAY_EMPTY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum ARRAY_LENGTH_TOO_LONG:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum ARRAY_LENGTH_TOO_SHORT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum BAD_CERTIFICATE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum BAD_EXPIRATION:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum BAD_GATEWAY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum BAD_REQUEST:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum BLOCKED_BY_BLOCKLIST:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum CARDHOLDER_INSUFFICIENT_PERMISSIONS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum CARD_DECLINED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum CARD_DECLINED_CALL_ISSUER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum CARD_DECLINED_VERIFICATION_REQUIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum CARD_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum CARD_NOT_SUPPORTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum CARD_PROCESSING_NOT_ENABLED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum CARD_TOKEN_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum CARD_TOKEN_USED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum CHECKOUT_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum CHIP_INSERTION_REQUIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum CLIENT_CLOSED_REQUEST:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum CONFLICT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum CONFLICTING_PARAMETERS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum CURRENCY_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum CVV_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum DELAYED_TRANSACTION_CANCELED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum DELAYED_TRANSACTION_CAPTURED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum DELAYED_TRANSACTION_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum DELAYED_TRANSACTION_FAILED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum DEPRECATED_FIELD_SET:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum EXPECTED_ARRAY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum EXPECTED_BASE64_ENCODED_BYTE_ARRAY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum EXPECTED_BOOLEAN:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum EXPECTED_FLOAT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum EXPECTED_INTEGER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum EXPECTED_JSON_BODY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum EXPECTED_MAP:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum EXPECTED_OBJECT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum EXPECTED_STRING:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum EXPIRATION_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum FORBIDDEN:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum GATEWAY_TIMEOUT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum GENERIC_DECLINE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum GIFT_CARD_AVAILABLE_AMOUNT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum GONE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum HTTPS_ONLY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum IDEMPOTENCY_KEY_REUSED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INCORRECT_TYPE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INSUFFICIENT_FUNDS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INSUFFICIENT_INVENTORY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INSUFFICIENT_PERMISSIONS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INSUFFICIENT_SCOPES:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INTERNAL_SERVER_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_ACCOUNT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_ARRAY_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_CARD:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_CARD_DATA:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_CONTENT_TYPE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_CURSOR:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_EMAIL_ADDRESS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_ENCRYPTED_CARD:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_ENUM_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_EXPIRATION:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_EXPIRATION_DATE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_EXPIRATION_YEAR:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_FEES:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_FORM_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_LOCATION:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_PHONE_NUMBER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_PIN:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_POSTAL_CODE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_SORT_ORDER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_SQUARE_VERSION_FORMAT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_TIME:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_TIME_RANGE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_URL:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum INVALID_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum LOCATION_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum MANUALLY_ENTERED_PAYMENT_NOT_SUPPORTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum METHOD_NOT_ALLOWED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum MISSING_REQUIRED_PARAMETER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum NOT_ACCEPTABLE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum NOT_FOUND:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum NOT_IMPLEMENTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum NO_FIELDS_SET:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum ONE_INSTRUMENT_EXPECTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum ORDER_ALREADY_USED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum ORDER_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum ORDER_TOO_MANY_CATALOG_OBJECTS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum PAN_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum PAYMENT_LIMIT_EXCEEDED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum PAYMENT_NOT_REFUNDABLE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum PRICE_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum RATE_LIMITED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum REFUND_ALREADY_PENDING:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum REFUND_AMOUNT_INVALID:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum REQUEST_ENTITY_TOO_LARGE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum REQUEST_TIMEOUT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum RESERVATION_DECLINED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum RETIRED_FIELD_SET:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum SANDBOX_NOT_SUPPORTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum SERVICE_UNAVAILABLE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum TEMPORARY_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum TRANSACTION_LIMIT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum UNAUTHORIZED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum UNEXPECTED_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum UNKNOWN_QUERY_PARAMETER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum UNPROCESSABLE_ENTITY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum UNREACHABLE_URL:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum UNSUPPORTED_CARD_BRAND:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum UNSUPPORTED_ENTRY_METHOD:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum UNSUPPORTED_INSTRUMENT_TYPE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum UNSUPPORTED_MEDIA_TYPE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum V1_ACCESS_TOKEN:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum V1_APPLICATION:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum VALUE_EMPTY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum VALUE_REGEX_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum VALUE_TOO_HIGH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum VALUE_TOO_LONG:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum VALUE_TOO_LOW:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum VALUE_TOO_SHORT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum VERIFY_AVS_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum VERIFY_CVV_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum VERSION_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

.field public static final enum VOICE_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 329
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/4 v1, 0x0

    const-string v2, "INTERNAL_SERVER_ERROR"

    const v3, 0x4c4b40

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INTERNAL_SERVER_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 339
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/4 v2, 0x1

    const-string v3, "UNAUTHORIZED"

    const v4, 0x3d3010

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNAUTHORIZED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 346
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/4 v3, 0x2

    const-string v4, "ACCESS_TOKEN_EXPIRED"

    const v5, 0x3d3011

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ACCESS_TOKEN_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 353
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/4 v4, 0x3

    const-string v5, "ACCESS_TOKEN_REVOKED"

    const v6, 0x3d3012

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ACCESS_TOKEN_REVOKED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 363
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/4 v5, 0x4

    const-string v6, "FORBIDDEN"

    const v7, 0x3d7e30

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->FORBIDDEN:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 371
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/4 v6, 0x5

    const-string v7, "INSUFFICIENT_SCOPES"

    const v8, 0x3d7e31

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INSUFFICIENT_SCOPES:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 378
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/4 v7, 0x6

    const-string v8, "APPLICATION_DISABLED"

    const v9, 0x3d7e32

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->APPLICATION_DISABLED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 386
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/4 v8, 0x7

    const-string v9, "V1_APPLICATION"

    const v10, 0x3d7e33

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->V1_APPLICATION:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 395
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v9, 0x8

    const-string v10, "V1_ACCESS_TOKEN"

    const v11, 0x3d7e34

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->V1_ACCESS_TOKEN:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 403
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v10, 0x9

    const-string v11, "CARD_PROCESSING_NOT_ENABLED"

    const v12, 0x3d7e35

    invoke-direct {v0, v11, v10, v12}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_PROCESSING_NOT_ENABLED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 414
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v11, 0xa

    const-string v12, "BAD_REQUEST"

    const v13, 0x3d0900

    invoke-direct {v0, v12, v11, v13}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->BAD_REQUEST:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 422
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v12, 0xb

    const-string v13, "MISSING_REQUIRED_PARAMETER"

    const v14, 0x3d0901

    invoke-direct {v0, v13, v12, v14}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->MISSING_REQUIRED_PARAMETER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 431
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v13, 0xc

    const-string v14, "INCORRECT_TYPE"

    const v15, 0x3d0902

    invoke-direct {v0, v14, v13, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INCORRECT_TYPE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 439
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v14, 0xd

    const-string v15, "INVALID_TIME"

    const v13, 0x3d0903

    invoke-direct {v0, v15, v14, v13}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_TIME:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 447
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v13, 0xe

    const-string v15, "INVALID_TIME_RANGE"

    const v14, 0x3d0904

    invoke-direct {v0, v15, v13, v14}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_TIME_RANGE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 456
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v14, "INVALID_VALUE"

    const/16 v15, 0xf

    const v13, 0x3d0905

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 464
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_CURSOR"

    const/16 v14, 0x10

    const v15, 0x3d0906

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_CURSOR:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 472
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "UNKNOWN_QUERY_PARAMETER"

    const/16 v14, 0x11

    const v15, 0x3d0907

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNKNOWN_QUERY_PARAMETER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 480
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "CONFLICTING_PARAMETERS"

    const/16 v14, 0x12

    const v15, 0x3d0908

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CONFLICTING_PARAMETERS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 487
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "EXPECTED_JSON_BODY"

    const/16 v14, 0x13

    const v15, 0x3d0909

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_JSON_BODY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 495
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_SORT_ORDER"

    const/16 v14, 0x14

    const v15, 0x3d090a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_SORT_ORDER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 503
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "VALUE_REGEX_MISMATCH"

    const/16 v14, 0x15

    const v15, 0x3d093d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VALUE_REGEX_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 511
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "VALUE_TOO_SHORT"

    const/16 v14, 0x16

    const v15, 0x3d0939

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VALUE_TOO_SHORT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 519
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "VALUE_TOO_LONG"

    const/16 v14, 0x17

    const v15, 0x3d090b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VALUE_TOO_LONG:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 527
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "VALUE_TOO_LOW"

    const/16 v14, 0x18

    const v15, 0x3d090c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VALUE_TOO_LOW:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 535
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "VALUE_TOO_HIGH"

    const/16 v14, 0x19

    const v15, 0x3d093a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VALUE_TOO_HIGH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 543
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "VALUE_EMPTY"

    const/16 v14, 0x1a

    const v15, 0x3d093b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VALUE_EMPTY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 550
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "ARRAY_LENGTH_TOO_LONG"

    const/16 v14, 0x1b

    const v15, 0x3d0949

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ARRAY_LENGTH_TOO_LONG:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 557
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "ARRAY_LENGTH_TOO_SHORT"

    const/16 v14, 0x1c

    const v15, 0x3d094a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ARRAY_LENGTH_TOO_SHORT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 564
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "ARRAY_EMPTY"

    const/16 v14, 0x1d

    const v15, 0x3d093c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ARRAY_EMPTY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 572
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "EXPECTED_BOOLEAN"

    const/16 v14, 0x1e

    const v15, 0x3d090d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_BOOLEAN:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 580
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "EXPECTED_INTEGER"

    const/16 v14, 0x1f

    const v15, 0x3d090e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_INTEGER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 588
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "EXPECTED_FLOAT"

    const/16 v14, 0x20

    const v15, 0x3d090f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_FLOAT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 596
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "EXPECTED_STRING"

    const/16 v14, 0x21

    const v15, 0x3d0910

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_STRING:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 604
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "EXPECTED_OBJECT"

    const/16 v14, 0x22

    const v15, 0x3d0911

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_OBJECT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 612
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "EXPECTED_ARRAY"

    const/16 v14, 0x23

    const v15, 0x3d0912

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_ARRAY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 620
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "EXPECTED_MAP"

    const/16 v14, 0x24

    const v15, 0x3d094f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_MAP:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 628
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "EXPECTED_BASE64_ENCODED_BYTE_ARRAY"

    const/16 v14, 0x25

    const v15, 0x3d0938

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_BASE64_ENCODED_BYTE_ARRAY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 636
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_ARRAY_VALUE"

    const/16 v14, 0x26

    const v15, 0x3d0913

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_ARRAY_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 644
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_ENUM_VALUE"

    const/16 v14, 0x27

    const v15, 0x3d0914

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_ENUM_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 651
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_CONTENT_TYPE"

    const/16 v14, 0x28

    const v15, 0x3d0915

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_CONTENT_TYPE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 660
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_FORM_VALUE"

    const/16 v14, 0x29

    const v15, 0x3d0916

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_FORM_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 668
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "ONE_INSTRUMENT_EXPECTED"

    const/16 v14, 0x2a

    const v15, 0x3d0934

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ONE_INSTRUMENT_EXPECTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 676
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "NO_FIELDS_SET"

    const/16 v14, 0x2b

    const v15, 0x3d0935

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->NO_FIELDS_SET:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 682
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "DEPRECATED_FIELD_SET"

    const/16 v14, 0x2c

    const v15, 0x3d094e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->DEPRECATED_FIELD_SET:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 687
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "RETIRED_FIELD_SET"

    const/16 v14, 0x2d

    const v15, 0x3d0967

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->RETIRED_FIELD_SET:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 697
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "CARD_EXPIRED"

    const/16 v14, 0x2e

    const v15, 0x3d0918

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 705
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_EXPIRATION"

    const/16 v14, 0x2f

    const v15, 0x3d091b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_EXPIRATION:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 713
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_EXPIRATION_YEAR"

    const/16 v14, 0x30

    const v15, 0x3d091c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_EXPIRATION_YEAR:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 721
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_EXPIRATION_DATE"

    const/16 v14, 0x31

    const v15, 0x3d0927

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_EXPIRATION_DATE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 728
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "UNSUPPORTED_CARD_BRAND"

    const/16 v14, 0x32

    const v15, 0x3d0937

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNSUPPORTED_CARD_BRAND:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 735
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "UNSUPPORTED_ENTRY_METHOD"

    const/16 v14, 0x33

    const v15, 0x3d094c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNSUPPORTED_ENTRY_METHOD:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 742
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_ENCRYPTED_CARD"

    const/16 v14, 0x34

    const v15, 0x3d0951

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_ENCRYPTED_CARD:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 749
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_CARD"

    const/16 v14, 0x35

    const v15, 0x3d0928

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_CARD:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 757
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "GENERIC_DECLINE"

    const/16 v14, 0x36

    const v15, 0x3d0952

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->GENERIC_DECLINE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 764
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "CVV_FAILURE"

    const/16 v14, 0x37

    const v15, 0x3d0953

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CVV_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 772
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "ADDRESS_VERIFICATION_FAILURE"

    const/16 v14, 0x38

    const v15, 0x3d0954

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ADDRESS_VERIFICATION_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 779
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_ACCOUNT"

    const/16 v14, 0x39

    const v15, 0x3d0955

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_ACCOUNT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 790
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "CURRENCY_MISMATCH"

    const/16 v14, 0x3a

    const v15, 0x3d0956

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CURRENCY_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 797
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INSUFFICIENT_FUNDS"

    const/16 v14, 0x3b

    const v15, 0x3d0957

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INSUFFICIENT_FUNDS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 806
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INSUFFICIENT_PERMISSIONS"

    const/16 v14, 0x3c

    const v15, 0x3d0958

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INSUFFICIENT_PERMISSIONS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 817
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "CARDHOLDER_INSUFFICIENT_PERMISSIONS"

    const/16 v14, 0x3d

    const v15, 0x3d0959

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARDHOLDER_INSUFFICIENT_PERMISSIONS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 825
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_LOCATION"

    const/16 v14, 0x3e

    const v15, 0x3d095a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_LOCATION:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 835
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "TRANSACTION_LIMIT"

    const/16 v14, 0x3f

    const v15, 0x3d095b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->TRANSACTION_LIMIT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 842
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "VOICE_FAILURE"

    const/16 v14, 0x40

    const v15, 0x3d095c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VOICE_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 854
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "PAN_FAILURE"

    const/16 v14, 0x41

    const v15, 0x3d095d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->PAN_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 865
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "EXPIRATION_FAILURE"

    const/16 v14, 0x42

    const v15, 0x3d095e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPIRATION_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 873
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "CARD_NOT_SUPPORTED"

    const/16 v14, 0x43

    const v15, 0x3d095f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_NOT_SUPPORTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 880
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_PIN"

    const/16 v14, 0x44

    const v15, 0x3d0960

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_PIN:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 887
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_POSTAL_CODE"

    const/16 v14, 0x45

    const v15, 0x3d0961

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_POSTAL_CODE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 894
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_FEES"

    const/16 v14, 0x46

    const v15, 0x3d0962

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_FEES:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 901
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "MANUALLY_ENTERED_PAYMENT_NOT_SUPPORTED"

    const/16 v14, 0x47

    const v15, 0x3d0963

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->MANUALLY_ENTERED_PAYMENT_NOT_SUPPORTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 909
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "PAYMENT_LIMIT_EXCEEDED"

    const/16 v14, 0x48

    const v15, 0x3d0964

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->PAYMENT_LIMIT_EXCEEDED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 932
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "GIFT_CARD_AVAILABLE_AMOUNT"

    const/16 v14, 0x49

    const v15, 0x3d096a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->GIFT_CARD_AVAILABLE_AMOUNT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 940
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "DELAYED_TRANSACTION_EXPIRED"

    const/16 v14, 0x4a

    const v15, 0x3d0929

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->DELAYED_TRANSACTION_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 948
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "DELAYED_TRANSACTION_CANCELED"

    const/16 v14, 0x4b

    const v15, 0x3d092a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->DELAYED_TRANSACTION_CANCELED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 956
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "DELAYED_TRANSACTION_CAPTURED"

    const/16 v14, 0x4c

    const v15, 0x3d092b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->DELAYED_TRANSACTION_CAPTURED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 964
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "DELAYED_TRANSACTION_FAILED"

    const/16 v14, 0x4d

    const v15, 0x3d092c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->DELAYED_TRANSACTION_FAILED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 971
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "CARD_TOKEN_EXPIRED"

    const/16 v14, 0x4e

    const v15, 0x3d092d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_TOKEN_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 978
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "CARD_TOKEN_USED"

    const/16 v14, 0x4f

    const v15, 0x3d092e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_TOKEN_USED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 985
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "AMOUNT_TOO_HIGH"

    const/16 v14, 0x50

    const v15, 0x3d092f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->AMOUNT_TOO_HIGH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 992
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "UNSUPPORTED_INSTRUMENT_TYPE"

    const/16 v14, 0x51

    const v15, 0x3d0936

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNSUPPORTED_INSTRUMENT_TYPE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 999
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "REFUND_AMOUNT_INVALID"

    const/16 v14, 0x52

    const v15, 0x3d0930

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->REFUND_AMOUNT_INVALID:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1006
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "REFUND_ALREADY_PENDING"

    const/16 v14, 0x53

    const v15, 0x3d0931

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->REFUND_ALREADY_PENDING:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1014
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "PAYMENT_NOT_REFUNDABLE"

    const/16 v14, 0x54

    const v15, 0x3d0932

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->PAYMENT_NOT_REFUNDABLE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1021
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_CARD_DATA"

    const/16 v14, 0x55

    const v15, 0x3d0933

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_CARD_DATA:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1028
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "LOCATION_MISMATCH"

    const/16 v14, 0x56

    const v15, 0x3d094d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->LOCATION_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1035
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "ORDER_EXPIRED"

    const/16 v14, 0x57

    const v15, 0x3d093e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ORDER_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1042
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "ORDER_ALREADY_USED"

    const/16 v14, 0x58

    const v15, 0x3d0943

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ORDER_ALREADY_USED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1049
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "ORDER_TOO_MANY_CATALOG_OBJECTS"

    const/16 v14, 0x59

    const v15, 0x3d0948

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ORDER_TOO_MANY_CATALOG_OBJECTS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1056
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INSUFFICIENT_INVENTORY"

    const/16 v14, 0x5a

    const v15, 0x3d093f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INSUFFICIENT_INVENTORY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1063
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "PRICE_MISMATCH"

    const/16 v14, 0x5b

    const v15, 0x3d0940

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->PRICE_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1070
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "VERSION_MISMATCH"

    const/16 v14, 0x5c

    const v15, 0x3d0941

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VERSION_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1077
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "IDEMPOTENCY_KEY_REUSED"

    const/16 v14, 0x5d

    const v15, 0x3d0942

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->IDEMPOTENCY_KEY_REUSED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1084
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "UNEXPECTED_VALUE"

    const/16 v14, 0x5e

    const v15, 0x3d0944

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNEXPECTED_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1091
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "SANDBOX_NOT_SUPPORTED"

    const/16 v14, 0x5f

    const v15, 0x3d0945

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->SANDBOX_NOT_SUPPORTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1098
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_EMAIL_ADDRESS"

    const/16 v14, 0x60

    const v15, 0x3d0946

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_EMAIL_ADDRESS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1105
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_PHONE_NUMBER"

    const/16 v14, 0x61

    const v15, 0x3d094b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_PHONE_NUMBER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1112
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "CHECKOUT_EXPIRED"

    const/16 v14, 0x62

    const v15, 0x3d0947

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CHECKOUT_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1119
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "BAD_CERTIFICATE"

    const/16 v14, 0x63

    const v15, 0x3d0950

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->BAD_CERTIFICATE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1127
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_SQUARE_VERSION_FORMAT"

    const/16 v14, 0x64

    const v15, 0x3d0965

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_SQUARE_VERSION_FORMAT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1135
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "API_VERSION_INCOMPATIBLE"

    const/16 v14, 0x65

    const v15, 0x3d0966

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->API_VERSION_INCOMPATIBLE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1142
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "INVALID_URL"

    const/16 v14, 0x66

    const v15, 0x3d0968

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_URL:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1149
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "HTTPS_ONLY"

    const/16 v14, 0x67

    const v15, 0x3d0969

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->HTTPS_ONLY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1156
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "UNREACHABLE_URL"

    const/16 v14, 0x68

    const v15, 0x3d096b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNREACHABLE_URL:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1171
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "CARD_DECLINED"

    const/16 v14, 0x69

    const v15, 0x3d5721

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_DECLINED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1178
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "VERIFY_CVV_FAILURE"

    const/16 v14, 0x6a

    const v15, 0x3d5722

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VERIFY_CVV_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1185
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "VERIFY_AVS_FAILURE"

    const/16 v14, 0x6b

    const v15, 0x3d5723

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VERIFY_AVS_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1193
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "CARD_DECLINED_CALL_ISSUER"

    const/16 v14, 0x6c

    const v15, 0x3d5724

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_DECLINED_CALL_ISSUER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1201
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "CARD_DECLINED_VERIFICATION_REQUIRED"

    const/16 v14, 0x6d

    const v15, 0x3d5725

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_DECLINED_VERIFICATION_REQUIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1209
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "BAD_EXPIRATION"

    const/16 v14, 0x6e

    const v15, 0x3d5726

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->BAD_EXPIRATION:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1217
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "CHIP_INSERTION_REQUIRED"

    const/16 v14, 0x6f

    const v15, 0x3d5727

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CHIP_INSERTION_REQUIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1226
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "ALLOWABLE_PIN_TRIES_EXCEEDED"

    const/16 v14, 0x70

    const v15, 0x3d5728

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ALLOWABLE_PIN_TRIES_EXCEEDED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1233
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "RESERVATION_DECLINED"

    const/16 v14, 0x71

    const v15, 0x3d5729

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->RESERVATION_DECLINED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1240
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "BLOCKED_BY_BLOCKLIST"

    const/16 v14, 0x72

    const v15, 0x3d572a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->BLOCKED_BY_BLOCKLIST:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1250
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "NOT_FOUND"

    const/16 v14, 0x73

    const v15, 0x3da540

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->NOT_FOUND:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1260
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "APPLE_PAYMENT_PROCESSING_CERTIFICATE_HASH_NOT_FOUND"

    const/16 v14, 0x74

    const v15, 0x3da541

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->APPLE_PAYMENT_PROCESSING_CERTIFICATE_HASH_NOT_FOUND:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1270
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "METHOD_NOT_ALLOWED"

    const/16 v14, 0x75

    const v15, 0x3dcc50

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->METHOD_NOT_ALLOWED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1280
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "NOT_ACCEPTABLE"

    const/16 v14, 0x76

    const v15, 0x3df360

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->NOT_ACCEPTABLE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1290
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "REQUEST_TIMEOUT"

    const/16 v14, 0x77

    const v15, 0x3e4180

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->REQUEST_TIMEOUT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1300
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "CONFLICT"

    const/16 v14, 0x78

    const v15, 0x3e6890

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CONFLICT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1311
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "GONE"

    const/16 v14, 0x79

    const v15, 0x3e8fa0

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->GONE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1321
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "REQUEST_ENTITY_TOO_LARGE"

    const/16 v14, 0x7a

    const v15, 0x3f04d0

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->REQUEST_ENTITY_TOO_LARGE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1331
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "UNSUPPORTED_MEDIA_TYPE"

    const/16 v14, 0x7b

    const v15, 0x3f52f0

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNSUPPORTED_MEDIA_TYPE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1341
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "UNPROCESSABLE_ENTITY"

    const/16 v14, 0x7c

    const v15, 0x406460

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNPROCESSABLE_ENTITY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1351
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "RATE_LIMITED"

    const/16 v14, 0x7d

    const v15, 0x4175d0

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->RATE_LIMITED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1361
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "CLIENT_CLOSED_REQUEST"

    const/16 v14, 0x7e

    const v15, 0x4c2430

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CLIENT_CLOSED_REQUEST:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1372
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "NOT_IMPLEMENTED"

    const/16 v14, 0x7f

    const v15, 0x4c7250

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->NOT_IMPLEMENTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1382
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "BAD_GATEWAY"

    const/16 v14, 0x80

    const v15, 0x4c9960

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->BAD_GATEWAY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1392
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "SERVICE_UNAVAILABLE"

    const/16 v14, 0x81

    const v15, 0x4cc070

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->SERVICE_UNAVAILABLE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1402
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "TEMPORARY_ERROR"

    const/16 v14, 0x82

    const v15, 0x4cc071

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->TEMPORARY_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1412
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const-string v13, "GATEWAY_TIMEOUT"

    const/16 v14, 0x83

    const v15, 0x4ce780

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/connect/v2/resources/Error$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->GATEWAY_TIMEOUT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v0, 0x84

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 320
    sget-object v13, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INTERNAL_SERVER_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNAUTHORIZED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ACCESS_TOKEN_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ACCESS_TOKEN_REVOKED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->FORBIDDEN:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INSUFFICIENT_SCOPES:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->APPLICATION_DISABLED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->V1_APPLICATION:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->V1_ACCESS_TOKEN:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_PROCESSING_NOT_ENABLED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->BAD_REQUEST:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->MISSING_REQUIRED_PARAMETER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INCORRECT_TYPE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_TIME:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_TIME_RANGE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_CURSOR:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNKNOWN_QUERY_PARAMETER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CONFLICTING_PARAMETERS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_JSON_BODY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_SORT_ORDER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VALUE_REGEX_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VALUE_TOO_SHORT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VALUE_TOO_LONG:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VALUE_TOO_LOW:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VALUE_TOO_HIGH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VALUE_EMPTY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ARRAY_LENGTH_TOO_LONG:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ARRAY_LENGTH_TOO_SHORT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ARRAY_EMPTY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_BOOLEAN:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_INTEGER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_FLOAT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_STRING:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_OBJECT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_ARRAY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_MAP:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_BASE64_ENCODED_BYTE_ARRAY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_ARRAY_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_ENUM_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_CONTENT_TYPE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_FORM_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ONE_INSTRUMENT_EXPECTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->NO_FIELDS_SET:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->DEPRECATED_FIELD_SET:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->RETIRED_FIELD_SET:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_EXPIRATION:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_EXPIRATION_YEAR:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_EXPIRATION_DATE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNSUPPORTED_CARD_BRAND:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNSUPPORTED_ENTRY_METHOD:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_ENCRYPTED_CARD:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_CARD:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->GENERIC_DECLINE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CVV_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ADDRESS_VERIFICATION_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_ACCOUNT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CURRENCY_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INSUFFICIENT_FUNDS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INSUFFICIENT_PERMISSIONS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARDHOLDER_INSUFFICIENT_PERMISSIONS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_LOCATION:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->TRANSACTION_LIMIT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VOICE_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->PAN_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPIRATION_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_NOT_SUPPORTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_PIN:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_POSTAL_CODE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_FEES:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->MANUALLY_ENTERED_PAYMENT_NOT_SUPPORTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->PAYMENT_LIMIT_EXCEEDED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->GIFT_CARD_AVAILABLE_AMOUNT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->DELAYED_TRANSACTION_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->DELAYED_TRANSACTION_CANCELED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->DELAYED_TRANSACTION_CAPTURED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->DELAYED_TRANSACTION_FAILED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_TOKEN_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_TOKEN_USED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->AMOUNT_TOO_HIGH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNSUPPORTED_INSTRUMENT_TYPE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->REFUND_AMOUNT_INVALID:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->REFUND_ALREADY_PENDING:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->PAYMENT_NOT_REFUNDABLE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_CARD_DATA:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->LOCATION_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ORDER_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ORDER_ALREADY_USED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ORDER_TOO_MANY_CATALOG_OBJECTS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INSUFFICIENT_INVENTORY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->PRICE_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VERSION_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->IDEMPOTENCY_KEY_REUSED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNEXPECTED_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->SANDBOX_NOT_SUPPORTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_EMAIL_ADDRESS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_PHONE_NUMBER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CHECKOUT_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->BAD_CERTIFICATE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_SQUARE_VERSION_FORMAT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->API_VERSION_INCOMPATIBLE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_URL:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->HTTPS_ONLY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNREACHABLE_URL:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_DECLINED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VERIFY_CVV_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VERIFY_AVS_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_DECLINED_CALL_ISSUER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_DECLINED_VERIFICATION_REQUIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->BAD_EXPIRATION:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CHIP_INSERTION_REQUIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ALLOWABLE_PIN_TRIES_EXCEEDED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->RESERVATION_DECLINED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->BLOCKED_BY_BLOCKLIST:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->NOT_FOUND:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->APPLE_PAYMENT_PROCESSING_CERTIFICATE_HASH_NOT_FOUND:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->METHOD_NOT_ALLOWED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->NOT_ACCEPTABLE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->REQUEST_TIMEOUT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CONFLICT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->GONE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->REQUEST_ENTITY_TOO_LARGE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNSUPPORTED_MEDIA_TYPE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNPROCESSABLE_ENTITY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->RATE_LIMITED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CLIENT_CLOSED_REQUEST:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->NOT_IMPLEMENTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->BAD_GATEWAY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->SERVICE_UNAVAILABLE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->TEMPORARY_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Code;->GATEWAY_TIMEOUT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Error$Code;

    .line 1414
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Code$ProtoAdapter_Code;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Error$Code$ProtoAdapter_Code;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1418
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1419
    iput p3, p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/resources/Error$Code;
    .locals 0

    packed-switch p0, :pswitch_data_0

    sparse-switch p0, :sswitch_data_0

    packed-switch p0, :pswitch_data_1

    packed-switch p0, :pswitch_data_2

    packed-switch p0, :pswitch_data_3

    packed-switch p0, :pswitch_data_4

    packed-switch p0, :pswitch_data_5

    packed-switch p0, :pswitch_data_6

    packed-switch p0, :pswitch_data_7

    const/4 p0, 0x0

    return-object p0

    .line 1557
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->TEMPORARY_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1556
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->SERVICE_UNAVAILABLE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1543
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->APPLE_PAYMENT_PROCESSING_CERTIFICATE_HASH_NOT_FOUND:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1542
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->NOT_FOUND:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1436
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_PROCESSING_NOT_ENABLED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1435
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->V1_ACCESS_TOKEN:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1434
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->V1_APPLICATION:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1433
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->APPLICATION_DISABLED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1432
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INSUFFICIENT_SCOPES:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1431
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->FORBIDDEN:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1541
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->BLOCKED_BY_BLOCKLIST:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1540
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->RESERVATION_DECLINED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1539
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ALLOWABLE_PIN_TRIES_EXCEEDED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1538
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CHIP_INSERTION_REQUIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1537
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->BAD_EXPIRATION:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1536
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_DECLINED_VERIFICATION_REQUIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1535
    :pswitch_10
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_DECLINED_CALL_ISSUER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1534
    :pswitch_11
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VERIFY_AVS_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1533
    :pswitch_12
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VERIFY_CVV_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1532
    :pswitch_13
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_DECLINED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1430
    :pswitch_14
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ACCESS_TOKEN_REVOKED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1429
    :pswitch_15
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ACCESS_TOKEN_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1428
    :pswitch_16
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNAUTHORIZED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1531
    :pswitch_17
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNREACHABLE_URL:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1500
    :pswitch_18
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->GIFT_CARD_AVAILABLE_AMOUNT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1530
    :pswitch_19
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->HTTPS_ONLY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1529
    :pswitch_1a
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_URL:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1472
    :pswitch_1b
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->RETIRED_FIELD_SET:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1528
    :pswitch_1c
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->API_VERSION_INCOMPATIBLE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1527
    :pswitch_1d
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_SQUARE_VERSION_FORMAT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1499
    :pswitch_1e
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->PAYMENT_LIMIT_EXCEEDED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1498
    :pswitch_1f
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->MANUALLY_ENTERED_PAYMENT_NOT_SUPPORTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1497
    :pswitch_20
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_FEES:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1496
    :pswitch_21
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_POSTAL_CODE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1495
    :pswitch_22
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_PIN:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1494
    :pswitch_23
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_NOT_SUPPORTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1493
    :pswitch_24
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPIRATION_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1492
    :pswitch_25
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->PAN_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1491
    :pswitch_26
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VOICE_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1490
    :pswitch_27
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->TRANSACTION_LIMIT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1489
    :pswitch_28
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_LOCATION:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1488
    :pswitch_29
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARDHOLDER_INSUFFICIENT_PERMISSIONS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1487
    :pswitch_2a
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INSUFFICIENT_PERMISSIONS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1486
    :pswitch_2b
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INSUFFICIENT_FUNDS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1485
    :pswitch_2c
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CURRENCY_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1484
    :pswitch_2d
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_ACCOUNT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1483
    :pswitch_2e
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ADDRESS_VERIFICATION_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1482
    :pswitch_2f
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CVV_FAILURE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1481
    :pswitch_30
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->GENERIC_DECLINE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1479
    :pswitch_31
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_ENCRYPTED_CARD:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1526
    :pswitch_32
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->BAD_CERTIFICATE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1463
    :pswitch_33
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_MAP:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1471
    :pswitch_34
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->DEPRECATED_FIELD_SET:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1513
    :pswitch_35
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->LOCATION_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1478
    :pswitch_36
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNSUPPORTED_ENTRY_METHOD:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1524
    :pswitch_37
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_PHONE_NUMBER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1455
    :pswitch_38
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ARRAY_LENGTH_TOO_SHORT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1454
    :pswitch_39
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ARRAY_LENGTH_TOO_LONG:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1516
    :pswitch_3a
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ORDER_TOO_MANY_CATALOG_OBJECTS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1525
    :pswitch_3b
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CHECKOUT_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1523
    :pswitch_3c
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_EMAIL_ADDRESS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1522
    :pswitch_3d
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->SANDBOX_NOT_SUPPORTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1521
    :pswitch_3e
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNEXPECTED_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1515
    :pswitch_3f
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ORDER_ALREADY_USED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1520
    :pswitch_40
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->IDEMPOTENCY_KEY_REUSED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1519
    :pswitch_41
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VERSION_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1518
    :pswitch_42
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->PRICE_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1517
    :pswitch_43
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INSUFFICIENT_INVENTORY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1514
    :pswitch_44
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ORDER_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1448
    :pswitch_45
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VALUE_REGEX_MISMATCH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1456
    :pswitch_46
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ARRAY_EMPTY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1453
    :pswitch_47
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VALUE_EMPTY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1452
    :pswitch_48
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VALUE_TOO_HIGH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1449
    :pswitch_49
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VALUE_TOO_SHORT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1464
    :pswitch_4a
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_BASE64_ENCODED_BYTE_ARRAY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1477
    :pswitch_4b
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNSUPPORTED_CARD_BRAND:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1508
    :pswitch_4c
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNSUPPORTED_INSTRUMENT_TYPE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1470
    :pswitch_4d
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->NO_FIELDS_SET:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1469
    :pswitch_4e
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->ONE_INSTRUMENT_EXPECTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1512
    :pswitch_4f
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_CARD_DATA:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1511
    :pswitch_50
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->PAYMENT_NOT_REFUNDABLE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1510
    :pswitch_51
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->REFUND_ALREADY_PENDING:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1509
    :pswitch_52
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->REFUND_AMOUNT_INVALID:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1507
    :pswitch_53
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->AMOUNT_TOO_HIGH:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1506
    :pswitch_54
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_TOKEN_USED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1505
    :pswitch_55
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_TOKEN_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1504
    :pswitch_56
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->DELAYED_TRANSACTION_FAILED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1503
    :pswitch_57
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->DELAYED_TRANSACTION_CAPTURED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1502
    :pswitch_58
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->DELAYED_TRANSACTION_CANCELED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1501
    :pswitch_59
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->DELAYED_TRANSACTION_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1480
    :pswitch_5a
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_CARD:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1476
    :pswitch_5b
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_EXPIRATION_DATE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1475
    :pswitch_5c
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_EXPIRATION_YEAR:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1474
    :pswitch_5d
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_EXPIRATION:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1558
    :sswitch_0
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->GATEWAY_TIMEOUT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1555
    :sswitch_1
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->BAD_GATEWAY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1554
    :sswitch_2
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->NOT_IMPLEMENTED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1427
    :sswitch_3
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INTERNAL_SERVER_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1553
    :sswitch_4
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CLIENT_CLOSED_REQUEST:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1552
    :sswitch_5
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->RATE_LIMITED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1551
    :sswitch_6
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNPROCESSABLE_ENTITY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1550
    :sswitch_7
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNSUPPORTED_MEDIA_TYPE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1549
    :sswitch_8
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->REQUEST_ENTITY_TOO_LARGE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1548
    :sswitch_9
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->GONE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1547
    :sswitch_a
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CONFLICT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1546
    :sswitch_b
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->REQUEST_TIMEOUT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1545
    :sswitch_c
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->NOT_ACCEPTABLE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1544
    :sswitch_d
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->METHOD_NOT_ALLOWED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1473
    :sswitch_e
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CARD_EXPIRED:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1468
    :pswitch_5e
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_FORM_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1467
    :pswitch_5f
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_CONTENT_TYPE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1466
    :pswitch_60
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_ENUM_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1465
    :pswitch_61
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_ARRAY_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1462
    :pswitch_62
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_ARRAY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1461
    :pswitch_63
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_OBJECT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1460
    :pswitch_64
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_STRING:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1459
    :pswitch_65
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_FLOAT:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1458
    :pswitch_66
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_INTEGER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1457
    :pswitch_67
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_BOOLEAN:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1451
    :pswitch_68
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VALUE_TOO_LOW:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1450
    :pswitch_69
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->VALUE_TOO_LONG:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1447
    :pswitch_6a
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_SORT_ORDER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1446
    :pswitch_6b
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->EXPECTED_JSON_BODY:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1445
    :pswitch_6c
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->CONFLICTING_PARAMETERS:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1444
    :pswitch_6d
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->UNKNOWN_QUERY_PARAMETER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1443
    :pswitch_6e
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_CURSOR:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1442
    :pswitch_6f
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_VALUE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1441
    :pswitch_70
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_TIME_RANGE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1440
    :pswitch_71
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INVALID_TIME:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1439
    :pswitch_72
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->INCORRECT_TYPE:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1438
    :pswitch_73
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->MISSING_REQUIRED_PARAMETER:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    .line 1437
    :pswitch_74
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->BAD_REQUEST:Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x3d0900
        :pswitch_74
        :pswitch_73
        :pswitch_72
        :pswitch_71
        :pswitch_70
        :pswitch_6f
        :pswitch_6e
        :pswitch_6d
        :pswitch_6c
        :pswitch_6b
        :pswitch_6a
        :pswitch_69
        :pswitch_68
        :pswitch_67
        :pswitch_66
        :pswitch_65
        :pswitch_64
        :pswitch_63
        :pswitch_62
        :pswitch_61
        :pswitch_60
        :pswitch_5f
        :pswitch_5e
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x3d0918 -> :sswitch_e
        0x3dcc50 -> :sswitch_d
        0x3df360 -> :sswitch_c
        0x3e4180 -> :sswitch_b
        0x3e6890 -> :sswitch_a
        0x3e8fa0 -> :sswitch_9
        0x3f04d0 -> :sswitch_8
        0x3f52f0 -> :sswitch_7
        0x406460 -> :sswitch_6
        0x4175d0 -> :sswitch_5
        0x4c2430 -> :sswitch_4
        0x4c4b40 -> :sswitch_3
        0x4c7250 -> :sswitch_2
        0x4c9960 -> :sswitch_1
        0x4ce780 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x3d091b
        :pswitch_5d
        :pswitch_5c
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x3d0927
        :pswitch_5b
        :pswitch_5a
        :pswitch_59
        :pswitch_58
        :pswitch_57
        :pswitch_56
        :pswitch_55
        :pswitch_54
        :pswitch_53
        :pswitch_52
        :pswitch_51
        :pswitch_50
        :pswitch_4f
        :pswitch_4e
        :pswitch_4d
        :pswitch_4c
        :pswitch_4b
        :pswitch_4a
        :pswitch_49
        :pswitch_48
        :pswitch_47
        :pswitch_46
        :pswitch_45
        :pswitch_44
        :pswitch_43
        :pswitch_42
        :pswitch_41
        :pswitch_40
        :pswitch_3f
        :pswitch_3e
        :pswitch_3d
        :pswitch_3c
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x3d3010
        :pswitch_16
        :pswitch_15
        :pswitch_14
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x3d5721
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x3d7e30
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x3da540
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x4cc070
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Error$Code;
    .locals 1

    .line 320
    const-class v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/resources/Error$Code;
    .locals 1

    .line 320
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Error$Code;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/resources/Error$Code;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/resources/Error$Code;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 1565
    iget v0, p0, Lcom/squareup/protos/connect/v2/resources/Error$Code;->value:I

    return v0
.end method
