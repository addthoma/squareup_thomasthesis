.class public final Lcom/squareup/protos/connect/v2/PaymentFeesSpec$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PaymentFeesSpec.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/PaymentFeesSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/PaymentFeesSpec;",
        "Lcom/squareup/protos/connect/v2/PaymentFeesSpec$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public price_allocation_spec:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;",
            ">;"
        }
    .end annotation
.end field

.field public square_fee_payer:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 108
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 109
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/PaymentFeesSpec$Builder;->price_allocation_spec:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/PaymentFeesSpec;
    .locals 4

    .line 131
    new-instance v0, Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/PaymentFeesSpec$Builder;->price_allocation_spec:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/PaymentFeesSpec$Builder;->square_fee_payer:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/PaymentFeesSpec;-><init>(Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/PaymentFeesSpec$Builder;->build()Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    move-result-object v0

    return-object v0
.end method

.method public price_allocation_spec(Ljava/util/List;)Lcom/squareup/protos/connect/v2/PaymentFeesSpec$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/PaymentFeesSpec$Builder;"
        }
    .end annotation

    .line 113
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/PaymentFeesSpec$Builder;->price_allocation_spec:Ljava/util/List;

    return-object p0
.end method

.method public square_fee_payer(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/PaymentFeesSpec$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/PaymentFeesSpec$Builder;->square_fee_payer:Ljava/lang/String;

    return-object p0
.end method
