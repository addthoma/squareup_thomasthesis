.class public final Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BusinessHoursPeriod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod;",
        "Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public day_of_week:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

.field public end_local_time:Ljava/lang/String;

.field public start_local_time:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 133
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod;
    .locals 5

    .line 168
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod$Builder;->day_of_week:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod$Builder;->start_local_time:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod$Builder;->end_local_time:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod;-><init>(Lcom/squareup/protos/connect/v2/common/DayOfWeek;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 126
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod$Builder;->build()Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod;

    move-result-object v0

    return-object v0
.end method

.method public day_of_week(Lcom/squareup/protos/connect/v2/common/DayOfWeek;)Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod$Builder;->day_of_week:Lcom/squareup/protos/connect/v2/common/DayOfWeek;

    return-object p0
.end method

.method public end_local_time(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod$Builder;->end_local_time:Ljava/lang/String;

    return-object p0
.end method

.method public start_local_time(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod$Builder;->start_local_time:Ljava/lang/String;

    return-object p0
.end method
