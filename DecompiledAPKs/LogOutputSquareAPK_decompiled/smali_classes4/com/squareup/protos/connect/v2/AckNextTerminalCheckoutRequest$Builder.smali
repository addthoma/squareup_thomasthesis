.class public final Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AckNextTerminalCheckoutRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;",
        "Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

.field public device_id:Ljava/lang/String;

.field public idempotency_key:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 129
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public ack_reason(Lcom/squareup/protos/connect/v2/AckReasons$AckReason;)Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;
    .locals 5

    .line 167
    new-instance v0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->idempotency_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->device_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/AckReasons$AckReason;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 122
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->build()Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;

    move-result-object v0

    return-object v0
.end method

.method public device_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->device_id:Ljava/lang/String;

    return-object p0
.end method

.method public idempotency_key(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->idempotency_key:Ljava/lang/String;

    return-object p0
.end method
