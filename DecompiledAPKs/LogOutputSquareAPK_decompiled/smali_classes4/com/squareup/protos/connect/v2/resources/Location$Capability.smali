.class public final enum Lcom/squareup/protos/connect/v2/resources/Location$Capability;
.super Ljava/lang/Enum;
.source "Location.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Location;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Capability"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Location$Capability$ProtoAdapter_Capability;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/resources/Location$Capability;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/resources/Location$Capability;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Location$Capability;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CREDIT_CARD_PROCESSING:Lcom/squareup/protos/connect/v2/resources/Location$Capability;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 874
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Location$Capability;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "CREDIT_CARD_PROCESSING"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/protos/connect/v2/resources/Location$Capability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Location$Capability;->CREDIT_CARD_PROCESSING:Lcom/squareup/protos/connect/v2/resources/Location$Capability;

    new-array v0, v1, [Lcom/squareup/protos/connect/v2/resources/Location$Capability;

    .line 865
    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Location$Capability;->CREDIT_CARD_PROCESSING:Lcom/squareup/protos/connect/v2/resources/Location$Capability;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Location$Capability;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Location$Capability;

    .line 876
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Location$Capability$ProtoAdapter_Capability;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Location$Capability$ProtoAdapter_Capability;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Location$Capability;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 880
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 881
    iput p3, p0, Lcom/squareup/protos/connect/v2/resources/Location$Capability;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/resources/Location$Capability;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 889
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Location$Capability;->CREDIT_CARD_PROCESSING:Lcom/squareup/protos/connect/v2/resources/Location$Capability;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Capability;
    .locals 1

    .line 865
    const-class v0, Lcom/squareup/protos/connect/v2/resources/Location$Capability;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/resources/Location$Capability;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/resources/Location$Capability;
    .locals 1

    .line 865
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Location$Capability;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Location$Capability;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/resources/Location$Capability;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/resources/Location$Capability;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 896
    iget v0, p0, Lcom/squareup/protos/connect/v2/resources/Location$Capability;->value:I

    return v0
.end method
