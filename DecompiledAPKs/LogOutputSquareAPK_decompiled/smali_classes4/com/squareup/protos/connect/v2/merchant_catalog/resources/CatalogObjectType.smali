.class public final enum Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;
.super Ljava/lang/Enum;
.source "CatalogObjectType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType$ProtoAdapter_CatalogObjectType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CATEGORY:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum CUSTOM_ATTRIBUTE_DEFINITION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum DINING_OPTION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum DISCOUNT:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum IMAGE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum ITEM:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum ITEM_OPTION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum ITEM_OPTION_VAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum ITEM_VARIATION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum MEASUREMENT_UNIT:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum MODIFIER:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum MODIFIER_LIST:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum PRICING_RULE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum PRODUCT_SET:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum QUICK_AMOUNTS_SETTINGS:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum RESOURCE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum SUBSCRIPTION_PLAN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum TAX:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum TAX_RULE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public static final enum TIME_PERIOD:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 23
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "ITEM"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 31
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/4 v3, 0x2

    const-string v4, "IMAGE"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->IMAGE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 39
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/4 v4, 0x4

    const-string v5, "CATEGORY"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->CATEGORY:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 47
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/4 v5, 0x3

    const/4 v6, 0x5

    const-string v7, "ITEM_VARIATION"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM_VARIATION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 55
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/4 v7, 0x6

    const-string v8, "TAX"

    invoke-direct {v0, v8, v4, v7}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->TAX:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 63
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/16 v8, 0x8

    const-string v9, "DISCOUNT"

    invoke-direct {v0, v9, v6, v8}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->DISCOUNT:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 71
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/16 v9, 0xa

    const-string v10, "MODIFIER_LIST"

    invoke-direct {v0, v10, v7, v9}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->MODIFIER_LIST:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 79
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/4 v10, 0x7

    const/16 v11, 0xc

    const-string v12, "MODIFIER"

    invoke-direct {v0, v12, v10, v11}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->MODIFIER:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 87
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/16 v12, 0x12

    const-string v13, "DINING_OPTION"

    invoke-direct {v0, v13, v8, v12}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->DINING_OPTION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 95
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/16 v13, 0x9

    const/16 v14, 0x13

    const-string v15, "TAX_RULE"

    invoke-direct {v0, v15, v13, v14}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->TAX_RULE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 103
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-string v15, "PRICING_RULE"

    const/16 v13, 0x21

    invoke-direct {v0, v15, v9, v13}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->PRICING_RULE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 111
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/16 v13, 0xb

    const-string v15, "PRODUCT_SET"

    const/16 v9, 0x22

    invoke-direct {v0, v15, v13, v9}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->PRODUCT_SET:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 119
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-string v9, "TIME_PERIOD"

    const/16 v15, 0x23

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->TIME_PERIOD:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 127
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-string v9, "MEASUREMENT_UNIT"

    const/16 v15, 0xd

    const/16 v11, 0x25

    invoke-direct {v0, v9, v15, v11}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->MEASUREMENT_UNIT:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 135
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-string v9, "SUBSCRIPTION_PLAN"

    const/16 v11, 0xe

    const/16 v15, 0x26

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->SUBSCRIPTION_PLAN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 143
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-string v9, "ITEM_OPTION"

    const/16 v11, 0xf

    const/16 v15, 0x27

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM_OPTION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 153
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-string v9, "ITEM_OPTION_VAL"

    const/16 v11, 0x10

    const/16 v15, 0x28

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM_OPTION_VAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 160
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-string v9, "CUSTOM_ATTRIBUTE_DEFINITION"

    const/16 v11, 0x11

    const/16 v15, 0x29

    invoke-direct {v0, v9, v11, v15}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->CUSTOM_ATTRIBUTE_DEFINITION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 168
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-string v9, "QUICK_AMOUNTS_SETTINGS"

    const/16 v11, 0x2d

    invoke-direct {v0, v9, v12, v11}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->QUICK_AMOUNTS_SETTINGS:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 176
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const-string v9, "RESOURCE"

    const/16 v11, 0x31

    invoke-direct {v0, v9, v14, v11}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->RESOURCE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/16 v0, 0x14

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 16
    sget-object v9, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->IMAGE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->CATEGORY:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM_VARIATION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->TAX:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->DISCOUNT:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->MODIFIER_LIST:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->MODIFIER:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->DINING_OPTION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->TAX_RULE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->PRICING_RULE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->PRODUCT_SET:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->TIME_PERIOD:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->MEASUREMENT_UNIT:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->SUBSCRIPTION_PLAN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM_OPTION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM_OPTION_VAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->CUSTOM_ATTRIBUTE_DEFINITION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->QUICK_AMOUNTS_SETTINGS:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->RESOURCE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    aput-object v1, v0, v14

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    .line 178
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType$ProtoAdapter_CatalogObjectType;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType$ProtoAdapter_CatalogObjectType;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 182
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 183
    iput p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_b

    const/4 v0, 0x2

    if-eq p0, v0, :cond_a

    const/4 v0, 0x4

    if-eq p0, v0, :cond_9

    const/4 v0, 0x5

    if-eq p0, v0, :cond_8

    const/4 v0, 0x6

    if-eq p0, v0, :cond_7

    const/16 v0, 0x8

    if-eq p0, v0, :cond_6

    const/16 v0, 0xa

    if-eq p0, v0, :cond_5

    const/16 v0, 0xc

    if-eq p0, v0, :cond_4

    const/16 v0, 0x2d

    if-eq p0, v0, :cond_3

    const/16 v0, 0x31

    if-eq p0, v0, :cond_2

    const/16 v0, 0x12

    if-eq p0, v0, :cond_1

    const/16 v0, 0x13

    if-eq p0, v0, :cond_0

    packed-switch p0, :pswitch_data_0

    packed-switch p0, :pswitch_data_1

    const/4 p0, 0x0

    return-object p0

    .line 208
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->CUSTOM_ATTRIBUTE_DEFINITION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 207
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM_OPTION_VAL:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 206
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM_OPTION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 205
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->SUBSCRIPTION_PLAN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 204
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->MEASUREMENT_UNIT:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 203
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->TIME_PERIOD:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 202
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->PRODUCT_SET:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 201
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->PRICING_RULE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 200
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->TAX_RULE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 199
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->DINING_OPTION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 210
    :cond_2
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->RESOURCE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 209
    :cond_3
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->QUICK_AMOUNTS_SETTINGS:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 198
    :cond_4
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->MODIFIER:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 197
    :cond_5
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->MODIFIER_LIST:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 196
    :cond_6
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->DISCOUNT:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 195
    :cond_7
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->TAX:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 194
    :cond_8
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM_VARIATION:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 193
    :cond_9
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->CATEGORY:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 192
    :cond_a
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->IMAGE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    .line 191
    :cond_b
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->ITEM:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x21
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x25
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;
    .locals 1

    .line 16
    const-class v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 217
    iget v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->value:I

    return v0
.end method
