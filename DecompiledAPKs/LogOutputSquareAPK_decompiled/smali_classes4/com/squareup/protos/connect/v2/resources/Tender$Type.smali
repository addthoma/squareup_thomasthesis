.class public final enum Lcom/squareup/protos/connect/v2/resources/Tender$Type;
.super Ljava/lang/Enum;
.source "Tender.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Tender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Tender$Type$ProtoAdapter_Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/resources/Tender$Type;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/resources/Tender$Type;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Tender$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

.field public static final enum CASH:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

.field public static final enum CHECK:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

.field public static final enum MERCHANT_GIFT_CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

.field public static final enum NO_SALE:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

.field public static final enum OTHER:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

.field public static final enum SQUARE_GIFT_CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

.field public static final enum THIRD_PARTY_CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

.field public static final enum THIRD_PARTY_E_MONEY:Lcom/squareup/protos/connect/v2/resources/Tender$Type;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 582
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "CARD"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    .line 587
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    const/4 v3, 0x2

    const-string v4, "CASH"

    invoke-direct {v0, v4, v1, v3}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->CASH:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    .line 597
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    const/4 v4, 0x3

    const-string v5, "THIRD_PARTY_CARD"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->THIRD_PARTY_CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    .line 602
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    const/4 v5, 0x4

    const-string v6, "SQUARE_GIFT_CARD"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->SQUARE_GIFT_CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    .line 607
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    const/4 v6, 0x5

    const-string v7, "NO_SALE"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->NO_SALE:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    .line 612
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    const/4 v7, 0x6

    const-string v8, "CHECK"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->CHECK:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    .line 617
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    const/4 v8, 0x7

    const-string v9, "MERCHANT_GIFT_CARD"

    invoke-direct {v0, v9, v7, v8}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->MERCHANT_GIFT_CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    .line 622
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    const/16 v9, 0x8

    const-string v10, "THIRD_PARTY_E_MONEY"

    invoke-direct {v0, v10, v8, v9}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->THIRD_PARTY_E_MONEY:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    .line 627
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    const-string v10, "OTHER"

    invoke-direct {v0, v10, v9, v2}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->OTHER:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    .line 578
    sget-object v10, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    aput-object v10, v0, v2

    sget-object v2, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->CASH:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    aput-object v2, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->THIRD_PARTY_CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->SQUARE_GIFT_CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->NO_SALE:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->CHECK:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->MERCHANT_GIFT_CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->THIRD_PARTY_E_MONEY:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->OTHER:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    .line 629
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type$ProtoAdapter_Type;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Tender$Type$ProtoAdapter_Type;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 633
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 634
    iput p3, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/resources/Tender$Type;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 649
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->THIRD_PARTY_E_MONEY:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    return-object p0

    .line 648
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->MERCHANT_GIFT_CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    return-object p0

    .line 647
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->CHECK:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    return-object p0

    .line 646
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->NO_SALE:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    return-object p0

    .line 645
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->SQUARE_GIFT_CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    return-object p0

    .line 644
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->THIRD_PARTY_CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    return-object p0

    .line 643
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->CASH:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    return-object p0

    .line 642
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    return-object p0

    .line 650
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->OTHER:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Tender$Type;
    .locals 1

    .line 578
    const-class v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/resources/Tender$Type;
    .locals 1

    .line 578
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/resources/Tender$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 657
    iget v0, p0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->value:I

    return v0
.end method
