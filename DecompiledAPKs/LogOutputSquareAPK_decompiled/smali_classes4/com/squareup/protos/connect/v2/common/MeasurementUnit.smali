.class public final Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
.super Lcom/squareup/wire/Message;
.source "MeasurementUnit.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/common/MeasurementUnit$ProtoAdapter_MeasurementUnit;,
        Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;,
        Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;,
        Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;,
        Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;,
        Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;,
        Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;,
        Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;,
        Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;,
        Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AREA_UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

.field public static final DEFAULT_GENERIC_UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

.field public static final DEFAULT_LENGTH_UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

.field public static final DEFAULT_TIME_UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

.field public static final DEFAULT_VOLUME_UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

.field public static final DEFAULT_WEIGHT_UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

.field private static final serialVersionUID:J


# instance fields
.field public final area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.MeasurementUnit$Area#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.MeasurementUnit$Custom#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final generic_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.MeasurementUnit$Generic#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.MeasurementUnit$Length#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.MeasurementUnit$Time#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.MeasurementUnit$UnitType#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.MeasurementUnit$Volume#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.MeasurementUnit$Weight#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$ProtoAdapter_MeasurementUnit;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$ProtoAdapter_MeasurementUnit;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 33
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->INVALID_AREA:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->DEFAULT_AREA_UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    .line 35
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->INVALID_LENGTH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->DEFAULT_LENGTH_UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    .line 37
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->INVALID_VOLUME:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->DEFAULT_VOLUME_UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    .line 39
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->INVALID_WEIGHT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->DEFAULT_WEIGHT_UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    .line 41
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->INVALID_GENERIC_UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->DEFAULT_GENERIC_UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    .line 43
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->INVALID_TIME:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->DEFAULT_TIME_UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    .line 45
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->INVALID_TYPE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->DEFAULT_TYPE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;)V
    .locals 10

    .line 138
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;-><init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;Lokio/ByteString;)V
    .locals 1

    .line 144
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    .line 146
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    .line 147
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    .line 148
    iput-object p4, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    .line 149
    iput-object p5, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    .line 150
    iput-object p6, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->generic_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    .line 151
    iput-object p7, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    .line 152
    iput-object p8, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->type:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 173
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 174
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 175
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    .line 176
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    .line 177
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    .line 178
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    .line 179
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    .line 180
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->generic_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->generic_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    .line 181
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    .line 182
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->type:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->type:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    .line 183
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 188
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 190
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->generic_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->type:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 199
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;
    .locals 2

    .line 157
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    .line 160
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->generic_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->generic_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    .line 164
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    .line 165
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->type:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->type:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    .line 166
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->newBuilder()Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    if-eqz v1, :cond_0

    const-string v1, ", custom_unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 208
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    if-eqz v1, :cond_1

    const-string v1, ", area_unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 209
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    if-eqz v1, :cond_2

    const-string v1, ", length_unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 210
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    if-eqz v1, :cond_3

    const-string v1, ", volume_unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 211
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    if-eqz v1, :cond_4

    const-string v1, ", weight_unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 212
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->generic_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    if-eqz v1, :cond_5

    const-string v1, ", generic_unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->generic_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 213
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    if-eqz v1, :cond_6

    const-string v1, ", time_unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 214
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->type:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    if-eqz v1, :cond_7

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->type:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "MeasurementUnit{"

    .line 215
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
