.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogTaxRule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public dining_option_id:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public exempt_product_set_id:Ljava/lang/String;

.field public max_item_price:Lcom/squareup/protos/connect/v2/common/Money;

.field public max_total_amount:Lcom/squareup/protos/connect/v2/common/Money;

.field public min_item_price:Lcom/squareup/protos/connect/v2/common/Money;

.field public name:Ljava/lang/String;

.field public tax_id:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 214
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 215
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;->dining_option_id:Ljava/util/List;

    .line 216
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;->tax_id:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;
    .locals 10

    .line 301
    new-instance v9, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;->dining_option_id:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;->max_total_amount:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;->min_item_price:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;->max_item_price:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;->tax_id:Ljava/util/List;

    iget-object v7, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;->exempt_product_set_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 199
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    move-result-object v0

    return-object v0
.end method

.method public dining_option_id(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;"
        }
    .end annotation

    .line 235
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 236
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;->dining_option_id:Ljava/util/List;

    return-object p0
.end method

.method public exempt_product_set_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;
    .locals 0

    .line 295
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;->exempt_product_set_id:Ljava/lang/String;

    return-object p0
.end method

.method public max_item_price(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;
    .locals 0

    .line 268
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;->max_item_price:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public max_total_amount(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;
    .locals 0

    .line 246
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;->max_total_amount:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public min_item_price(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;
    .locals 0

    .line 257
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;->min_item_price:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;
    .locals 0

    .line 225
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public tax_id(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;"
        }
    .end annotation

    .line 278
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 279
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule$Builder;->tax_id:Ljava/util/List;

    return-object p0
.end method
