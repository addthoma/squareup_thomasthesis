.class public final Lcom/squareup/protos/connect/v2/PaymentOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PaymentOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/PaymentOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/PaymentOptions;",
        "Lcom/squareup/protos/connect/v2/PaymentOptions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public autocomplete:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 86
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public autocomplete(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/PaymentOptions$Builder;
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/PaymentOptions$Builder;->autocomplete:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/PaymentOptions;
    .locals 3

    .line 102
    new-instance v0, Lcom/squareup/protos/connect/v2/PaymentOptions;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/PaymentOptions$Builder;->autocomplete:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/connect/v2/PaymentOptions;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 83
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/PaymentOptions$Builder;->build()Lcom/squareup/protos/connect/v2/PaymentOptions;

    move-result-object v0

    return-object v0
.end method
