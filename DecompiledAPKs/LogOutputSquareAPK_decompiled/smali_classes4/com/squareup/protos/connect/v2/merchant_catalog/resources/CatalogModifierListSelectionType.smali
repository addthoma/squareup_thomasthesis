.class public final enum Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;
.super Ljava/lang/Enum;
.source "CatalogModifierListSelectionType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType$ProtoAdapter_CatalogModifierListSelectionType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum MULTIPLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;

.field public static final enum SINGLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 20
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;

    const/4 v1, 0x0

    const-string v2, "SINGLE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;->SINGLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;

    .line 28
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;

    const/4 v2, 0x1

    const-string v3, "MULTIPLE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;->MULTIPLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;

    .line 13
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;->SINGLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;->MULTIPLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;

    .line 30
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType$ProtoAdapter_CatalogModifierListSelectionType;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType$ProtoAdapter_CatalogModifierListSelectionType;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 35
    iput p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 44
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;->MULTIPLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;

    return-object p0

    .line 43
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;->SINGLE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 51
    iget v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierListSelectionType;->value:I

    return v0
.end method
