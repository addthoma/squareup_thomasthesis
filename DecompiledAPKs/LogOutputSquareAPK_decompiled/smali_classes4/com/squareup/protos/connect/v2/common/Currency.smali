.class public final enum Lcom/squareup/protos/connect/v2/common/Currency;
.super Ljava/lang/Enum;
.source "Currency.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/common/Currency$ProtoAdapter_Currency;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/common/Currency;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/common/Currency;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AED:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum AFN:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum ALL:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum AMD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum ANG:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum AOA:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum ARS:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum AUD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum AWG:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum AZN:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum BAM:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum BBD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum BDT:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum BGN:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum BHD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum BIF:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum BMD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum BND:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum BOB:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum BOV:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum BRL:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum BSD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum BTC:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum BTN:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum BWP:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum BYR:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum BZD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum CAD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum CDF:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum CHE:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum CHF:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum CHW:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum CLF:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum CLP:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum CNY:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum COP:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum COU:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum CRC:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum CUC:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum CUP:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum CVE:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum CZK:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum DJF:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum DKK:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum DOP:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum DZD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum EGP:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum ERN:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum ETB:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum EUR:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum FJD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum FKP:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum GBP:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum GEL:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum GHS:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum GIP:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum GMD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum GNF:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum GTQ:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum GYD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum HKD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum HNL:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum HRK:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum HTG:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum HUF:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum IDR:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum ILS:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum INR:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum IQD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum IRR:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum ISK:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum JMD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum JOD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum JPY:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum KES:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum KGS:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum KHR:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum KMF:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum KPW:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum KRW:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum KWD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum KYD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum KZT:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum LAK:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum LBP:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum LKR:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum LRD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum LSL:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum LTL:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum LVL:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum LYD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum MAD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum MDL:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum MGA:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum MKD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum MMK:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum MNT:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum MOP:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum MRO:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum MUR:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum MVR:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum MWK:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum MXN:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum MXV:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum MYR:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum MZN:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum NAD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum NGN:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum NIO:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum NOK:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum NPR:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum NZD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum OMR:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum PAB:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum PEN:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum PGK:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum PHP:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum PKR:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum PLN:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum PYG:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum QAR:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum RON:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum RSD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum RUB:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum RWF:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum SAR:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum SBD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum SCR:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum SDG:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum SEK:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum SGD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum SHP:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum SLL:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum SOS:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum SRD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum SSP:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum STD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum SVC:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum SYP:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum SZL:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum THB:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum TJS:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum TMT:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum TND:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum TOP:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum TRY:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum TTD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum TWD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum TZS:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum UAH:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum UGX:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum UNKNOWN_CURRENCY:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum USD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum USN:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum USS:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum UYI:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum UYU:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum UZS:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum VEF:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum VND:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum VUV:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum WST:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum XAF:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum XAG:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum XAU:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum XBA:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum XBB:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum XBC:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum XBD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum XCD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum XDR:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum XOF:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum XPD:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum XPF:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum XPT:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum XTS:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum XXX:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum YER:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum ZAR:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum ZMK:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final enum ZMW:Lcom/squareup/protos/connect/v2/common/Currency;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 17
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_CURRENCY"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->UNKNOWN_CURRENCY:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 19
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v2, "AED"

    const/4 v3, 0x1

    const/16 v4, 0x310

    invoke-direct {v0, v2, v3, v4}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->AED:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 21
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v2, "AFN"

    const/4 v3, 0x2

    const/16 v4, 0x3cb

    invoke-direct {v0, v2, v3, v4}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->AFN:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 23
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x8

    const-string v3, "ALL"

    const/4 v4, 0x3

    invoke-direct {v0, v3, v4, v2}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->ALL:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 25
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v3, 0x33

    const-string v4, "AMD"

    const/4 v5, 0x4

    invoke-direct {v0, v4, v5, v3}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->AMD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 27
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v4, "ANG"

    const/4 v5, 0x5

    const/16 v6, 0x214

    invoke-direct {v0, v4, v5, v6}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->ANG:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 29
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v4, "AOA"

    const/4 v5, 0x6

    const/16 v6, 0x3cd

    invoke-direct {v0, v4, v5, v6}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->AOA:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 31
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v4, 0x20

    const-string v5, "ARS"

    const/4 v6, 0x7

    invoke-direct {v0, v5, v6, v4}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->ARS:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 33
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v5, 0x24

    const-string v6, "AUD"

    invoke-direct {v0, v6, v2, v5}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->AUD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 35
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v6, "AWG"

    const/16 v7, 0x9

    const/16 v8, 0x215

    invoke-direct {v0, v6, v7, v8}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->AWG:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 37
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v6, "AZN"

    const/16 v7, 0xa

    const/16 v8, 0x3b0

    invoke-direct {v0, v6, v7, v8}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->AZN:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 39
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v6, "BAM"

    const/16 v7, 0xb

    const/16 v8, 0x3d1

    invoke-direct {v0, v6, v7, v8}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->BAM:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 41
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v6, 0x34

    const/16 v7, 0xc

    const-string v8, "BBD"

    invoke-direct {v0, v8, v7, v6}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->BBD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 43
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v8, 0x32

    const-string v9, "BDT"

    const/16 v10, 0xd

    invoke-direct {v0, v9, v10, v8}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->BDT:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 45
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v9, "BGN"

    const/16 v10, 0xe

    const/16 v11, 0x3cf

    invoke-direct {v0, v9, v10, v11}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->BGN:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 47
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v9, 0x30

    const-string v10, "BHD"

    const/16 v11, 0xf

    invoke-direct {v0, v10, v11, v9}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->BHD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 49
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v10, 0x6c

    const-string v11, "BIF"

    const/16 v12, 0x10

    invoke-direct {v0, v11, v12, v10}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->BIF:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 51
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v11, 0x3c

    const-string v12, "BMD"

    const/16 v13, 0x11

    invoke-direct {v0, v12, v13, v11}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->BMD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 53
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v12, 0x60

    const-string v13, "BND"

    const/16 v14, 0x12

    invoke-direct {v0, v13, v14, v12}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->BND:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 55
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v13, 0x44

    const-string v14, "BOB"

    const/16 v15, 0x13

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->BOB:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 57
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v14, "BOV"

    const/16 v15, 0x14

    const/16 v2, 0x3d8

    invoke-direct {v0, v14, v15, v2}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->BOV:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 59
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v2, "BRL"

    const/16 v14, 0x15

    const/16 v15, 0x3da

    invoke-direct {v0, v2, v14, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->BRL:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 61
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x2c

    const-string v14, "BSD"

    const/16 v15, 0x16

    invoke-direct {v0, v14, v15, v2}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->BSD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 63
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v14, 0x40

    const-string v15, "BTN"

    const/16 v1, 0x17

    invoke-direct {v0, v15, v1, v14}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->BTN:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 65
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "BWP"

    const/16 v15, 0x18

    const/16 v10, 0x48

    invoke-direct {v0, v1, v15, v10}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->BWP:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 67
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "BYR"

    const/16 v10, 0x19

    const/16 v15, 0x3ce

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->BYR:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 69
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "BZD"

    const/16 v10, 0x1a

    const/16 v15, 0x54

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->BZD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 71
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "CAD"

    const/16 v10, 0x1b

    const/16 v15, 0x7c

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->CAD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 73
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "CDF"

    const/16 v10, 0x1c

    const/16 v15, 0x3d0

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->CDF:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 75
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "CHE"

    const/16 v10, 0x1d

    const/16 v15, 0x3b3

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->CHE:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 77
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "CHF"

    const/16 v10, 0x1e

    const/16 v15, 0x2f4

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->CHF:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 79
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "CHW"

    const/16 v10, 0x1f

    const/16 v15, 0x3b4

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->CHW:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 81
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "CLF"

    const/16 v10, 0x3de

    invoke-direct {v0, v1, v4, v10}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->CLF:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 83
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "CLP"

    const/16 v10, 0x21

    const/16 v15, 0x98

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->CLP:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 85
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "CNY"

    const/16 v10, 0x22

    const/16 v15, 0x9c

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->CNY:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 87
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "COP"

    const/16 v10, 0x23

    const/16 v15, 0xaa

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->COP:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 89
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "COU"

    const/16 v10, 0x3ca

    invoke-direct {v0, v1, v5, v10}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->COU:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 91
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "CRC"

    const/16 v10, 0x25

    const/16 v15, 0xbc

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->CRC:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 93
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "CUC"

    const/16 v10, 0x26

    const/16 v15, 0x3a3

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->CUC:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 95
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "CUP"

    const/16 v10, 0x27

    const/16 v15, 0xc0

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->CUP:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 97
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "CVE"

    const/16 v10, 0x28

    const/16 v15, 0x84

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->CVE:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 99
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "CZK"

    const/16 v10, 0x29

    const/16 v15, 0xcb

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->CZK:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 101
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "DJF"

    const/16 v10, 0x2a

    const/16 v15, 0x106

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->DJF:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 103
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "DKK"

    const/16 v10, 0x2b

    const/16 v15, 0xd0

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->DKK:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 105
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "DOP"

    const/16 v10, 0xd6

    invoke-direct {v0, v1, v2, v10}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->DOP:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 107
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "DZD"

    const/16 v10, 0x2d

    invoke-direct {v0, v1, v10, v7}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->DZD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 109
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "EGP"

    const/16 v10, 0x2e

    const/16 v15, 0x332

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->EGP:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 111
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "ERN"

    const/16 v10, 0x2f

    const/16 v15, 0xe8

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->ERN:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 113
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "ETB"

    const/16 v10, 0xe6

    invoke-direct {v0, v1, v9, v10}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->ETB:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 115
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "EUR"

    const/16 v10, 0x31

    const/16 v15, 0x3d2

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->EUR:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 117
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "FJD"

    const/16 v10, 0xf2

    invoke-direct {v0, v1, v8, v10}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->FJD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 119
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "FKP"

    const/16 v10, 0xee

    invoke-direct {v0, v1, v3, v10}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->FKP:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 121
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "GBP"

    const/16 v10, 0x33a

    invoke-direct {v0, v1, v6, v10}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->GBP:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 123
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "GEL"

    const/16 v10, 0x35

    const/16 v15, 0x3d5

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->GEL:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 125
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "GHS"

    const/16 v10, 0x36

    const/16 v15, 0x3a8

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->GHS:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 127
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "GIP"

    const/16 v10, 0x37

    const/16 v15, 0x124

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->GIP:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 129
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "GMD"

    const/16 v10, 0x38

    const/16 v15, 0x10e

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->GMD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 131
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "GNF"

    const/16 v10, 0x39

    const/16 v15, 0x144

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->GNF:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 133
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "GTQ"

    const/16 v10, 0x3a

    const/16 v15, 0x140

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->GTQ:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 135
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "GYD"

    const/16 v10, 0x3b

    const/16 v15, 0x148

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->GYD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 137
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "HKD"

    const/16 v10, 0x158

    invoke-direct {v0, v1, v11, v10}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->HKD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 139
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "HNL"

    const/16 v10, 0x3d

    const/16 v15, 0x154

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->HNL:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 141
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "HRK"

    const/16 v10, 0x3e

    const/16 v15, 0xbf

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->HRK:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 143
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "HTG"

    const/16 v10, 0x3f

    const/16 v15, 0x14c

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->HTG:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 145
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "HUF"

    const/16 v10, 0x15c

    invoke-direct {v0, v1, v14, v10}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->HUF:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 147
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "IDR"

    const/16 v10, 0x41

    const/16 v15, 0x168

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->IDR:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 149
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "ILS"

    const/16 v10, 0x42

    const/16 v15, 0x178

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->ILS:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 151
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "INR"

    const/16 v10, 0x43

    const/16 v15, 0x164

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->INR:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 153
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "IQD"

    const/16 v10, 0x170

    invoke-direct {v0, v1, v13, v10}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->IQD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 155
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "IRR"

    const/16 v10, 0x45

    const/16 v15, 0x16c

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->IRR:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 157
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "ISK"

    const/16 v10, 0x46

    const/16 v15, 0x160

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->ISK:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 159
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "JMD"

    const/16 v10, 0x47

    const/16 v15, 0x184

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->JMD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 161
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "JOD"

    const/16 v10, 0x48

    const/16 v15, 0x190

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->JOD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 163
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "JPY"

    const/16 v10, 0x49

    const/16 v15, 0x188

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->JPY:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 165
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "KES"

    const/16 v10, 0x4a

    const/16 v15, 0x194

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->KES:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 167
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "KGS"

    const/16 v10, 0x4b

    const/16 v15, 0x1a1

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->KGS:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 169
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "KHR"

    const/16 v10, 0x4c

    const/16 v15, 0x74

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->KHR:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 171
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "KMF"

    const/16 v10, 0x4d

    const/16 v15, 0xae

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->KMF:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 173
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "KPW"

    const/16 v10, 0x4e

    const/16 v15, 0x198

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->KPW:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 175
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "KRW"

    const/16 v10, 0x4f

    const/16 v15, 0x19a

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->KRW:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 177
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "KWD"

    const/16 v10, 0x50

    const/16 v15, 0x19e

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->KWD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 179
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "KYD"

    const/16 v10, 0x51

    const/16 v15, 0x88

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->KYD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 181
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "KZT"

    const/16 v10, 0x52

    const/16 v15, 0x18e

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->KZT:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 183
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "LAK"

    const/16 v10, 0x53

    const/16 v15, 0x1a2

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->LAK:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 185
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "LBP"

    const/16 v10, 0x54

    const/16 v15, 0x1a6

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->LBP:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 187
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "LKR"

    const/16 v10, 0x55

    const/16 v15, 0x90

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->LKR:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 189
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "LRD"

    const/16 v10, 0x56

    const/16 v15, 0x1ae

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->LRD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 191
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "LSL"

    const/16 v10, 0x57

    const/16 v15, 0x1aa

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->LSL:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 193
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "LTL"

    const/16 v10, 0x58

    const/16 v15, 0x1b8

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->LTL:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 195
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "LVL"

    const/16 v10, 0x59

    const/16 v15, 0x1ac

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->LVL:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 197
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "LYD"

    const/16 v10, 0x5a

    const/16 v15, 0x1b2

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->LYD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 199
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "MAD"

    const/16 v10, 0x5b

    const/16 v15, 0x1f8

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->MAD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 201
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "MDL"

    const/16 v10, 0x5c

    const/16 v15, 0x1f2

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->MDL:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 203
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "MGA"

    const/16 v10, 0x5d

    const/16 v15, 0x3c9

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->MGA:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 205
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "MKD"

    const/16 v10, 0x5e

    const/16 v15, 0x327

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->MKD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 207
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "MMK"

    const/16 v10, 0x5f

    const/16 v15, 0x68

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->MMK:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 209
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "MNT"

    const/16 v10, 0x1f0

    invoke-direct {v0, v1, v12, v10}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->MNT:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 211
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "MOP"

    const/16 v10, 0x61

    const/16 v15, 0x1be

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->MOP:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 213
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "MRO"

    const/16 v10, 0x62

    const/16 v15, 0x1de

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->MRO:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 215
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "MUR"

    const/16 v10, 0x63

    const/16 v15, 0x1e0

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->MUR:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 217
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "MVR"

    const/16 v10, 0x64

    const/16 v15, 0x1ce

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->MVR:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 219
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "MWK"

    const/16 v10, 0x65

    const/16 v15, 0x1c6

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->MWK:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 221
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "MXN"

    const/16 v10, 0x66

    const/16 v15, 0x1e4

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->MXN:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 223
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "MXV"

    const/16 v10, 0x67

    const/16 v15, 0x3d3

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->MXV:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 225
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "MYR"

    const/16 v10, 0x68

    const/16 v15, 0x1ca

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->MYR:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 227
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "MZN"

    const/16 v10, 0x69

    const/16 v15, 0x3af

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->MZN:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 229
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "NAD"

    const/16 v10, 0x6a

    const/16 v15, 0x204

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->NAD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 231
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "NGN"

    const/16 v10, 0x6b

    const/16 v15, 0x236

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->NGN:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 233
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "NIO"

    const/16 v10, 0x22e

    const/16 v15, 0x6c

    invoke-direct {v0, v1, v15, v10}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->NIO:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 235
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "NOK"

    const/16 v10, 0x6d

    const/16 v15, 0x242

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->NOK:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 237
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "NPR"

    const/16 v10, 0x6e

    const/16 v15, 0x20c

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->NPR:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 239
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "NZD"

    const/16 v10, 0x6f

    const/16 v15, 0x22a

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->NZD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 241
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "OMR"

    const/16 v10, 0x70

    const/16 v15, 0x200

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->OMR:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 243
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "PAB"

    const/16 v10, 0x71

    const/16 v15, 0x24e

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->PAB:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 245
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "PEN"

    const/16 v10, 0x72

    const/16 v15, 0x25c

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->PEN:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 247
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "PGK"

    const/16 v10, 0x73

    const/16 v15, 0x256

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->PGK:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 249
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "PHP"

    const/16 v10, 0x74

    const/16 v15, 0x260

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->PHP:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 251
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "PKR"

    const/16 v10, 0x75

    const/16 v15, 0x24a

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->PKR:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 253
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "PLN"

    const/16 v10, 0x76

    const/16 v15, 0x3d9

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->PLN:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 255
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "PYG"

    const/16 v10, 0x77

    const/16 v15, 0x258

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->PYG:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 257
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "QAR"

    const/16 v10, 0x78

    const/16 v15, 0x27a

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->QAR:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 259
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "RON"

    const/16 v10, 0x79

    const/16 v15, 0x3b2

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->RON:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 261
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "RSD"

    const/16 v10, 0x7a

    const/16 v15, 0x3ad

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->RSD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 263
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "RUB"

    const/16 v10, 0x7b

    const/16 v15, 0x283

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->RUB:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 265
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "RWF"

    const/16 v10, 0x7c

    const/16 v15, 0x286

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->RWF:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 267
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "SAR"

    const/16 v10, 0x7d

    const/16 v15, 0x2aa

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->SAR:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 269
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "SBD"

    const/16 v10, 0x7e

    const/16 v15, 0x5a

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->SBD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 271
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "SCR"

    const/16 v10, 0x7f

    const/16 v15, 0x2b2

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->SCR:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 273
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "SDG"

    const/16 v10, 0x80

    const/16 v15, 0x3aa

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->SDG:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 275
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "SEK"

    const/16 v10, 0x81

    const/16 v15, 0x2f0

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->SEK:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 277
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "SGD"

    const/16 v10, 0x82

    const/16 v15, 0x2be

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->SGD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 279
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "SHP"

    const/16 v10, 0x83

    const/16 v15, 0x28e

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->SHP:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 281
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "SLL"

    const/16 v10, 0x84

    const/16 v15, 0x2b6

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->SLL:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 283
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "SOS"

    const/16 v10, 0x85

    const/16 v15, 0x2c2

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->SOS:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 285
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "SRD"

    const/16 v10, 0x86

    const/16 v15, 0x3c8

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->SRD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 287
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "SSP"

    const/16 v10, 0x87

    const/16 v15, 0x2d8

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->SSP:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 289
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "STD"

    const/16 v10, 0x88

    const/16 v15, 0x2a6

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->STD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 291
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "SVC"

    const/16 v10, 0x89

    const/16 v15, 0xde

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->SVC:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 293
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "SYP"

    const/16 v10, 0x8a

    const/16 v15, 0x2f8

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->SYP:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 295
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "SZL"

    const/16 v10, 0x8b

    const/16 v15, 0x2ec

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->SZL:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 297
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "THB"

    const/16 v10, 0x8c

    const/16 v15, 0x2fc

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->THB:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 299
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "TJS"

    const/16 v10, 0x8d

    const/16 v15, 0x3cc

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->TJS:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 301
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "TMT"

    const/16 v10, 0x8e

    const/16 v15, 0x3a6

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->TMT:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 303
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "TND"

    const/16 v10, 0x8f

    const/16 v15, 0x314

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->TND:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 305
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "TOP"

    const/16 v10, 0x90

    const/16 v15, 0x308

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->TOP:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 307
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "TRY"

    const/16 v10, 0x91

    const/16 v15, 0x3b5

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->TRY:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 309
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "TTD"

    const/16 v10, 0x92

    const/16 v15, 0x30c

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->TTD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 311
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "TWD"

    const/16 v10, 0x93

    const/16 v15, 0x385

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->TWD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 313
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "TZS"

    const/16 v10, 0x94

    const/16 v15, 0x342

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->TZS:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 315
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "UAH"

    const/16 v10, 0x95

    const/16 v15, 0x3d4

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->UAH:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 317
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "UGX"

    const/16 v10, 0x96

    const/16 v15, 0x320

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->UGX:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 319
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "USD"

    const/16 v10, 0x97

    const/16 v15, 0x348

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->USD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 321
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "USN"

    const/16 v10, 0x98

    const/16 v15, 0x3e5

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->USN:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 323
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "USS"

    const/16 v10, 0x99

    const/16 v15, 0x3e6

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->USS:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 325
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "UYI"

    const/16 v10, 0x9a

    const/16 v15, 0x3ac

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->UYI:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 327
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "UYU"

    const/16 v10, 0x9b

    const/16 v15, 0x35a

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->UYU:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 329
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "UZS"

    const/16 v10, 0x9c

    const/16 v15, 0x35c

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->UZS:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 331
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "VEF"

    const/16 v10, 0x9d

    const/16 v15, 0x3a9

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->VEF:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 333
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "VND"

    const/16 v10, 0x9e

    const/16 v15, 0x2c0

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->VND:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 335
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "VUV"

    const/16 v10, 0x9f

    const/16 v15, 0x224

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->VUV:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 337
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "WST"

    const/16 v10, 0xa0

    const/16 v15, 0x372

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->WST:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 339
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "XAF"

    const/16 v10, 0xa1

    const/16 v15, 0x3b6

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->XAF:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 341
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "XAG"

    const/16 v10, 0xa2

    const/16 v15, 0x3c1

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->XAG:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 343
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "XAU"

    const/16 v10, 0xa3

    const/16 v15, 0x3bf

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->XAU:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 345
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "XBA"

    const/16 v10, 0xa4

    const/16 v15, 0x3bb

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->XBA:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 347
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "XBB"

    const/16 v10, 0xa5

    const/16 v15, 0x3bc

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->XBB:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 349
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "XBC"

    const/16 v10, 0xa6

    const/16 v15, 0x3bd

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->XBC:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 351
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "XBD"

    const/16 v10, 0xa7

    const/16 v15, 0x3be

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->XBD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 353
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "XCD"

    const/16 v10, 0xa8

    const/16 v15, 0x3b7

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->XCD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 355
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "XDR"

    const/16 v10, 0xa9

    const/16 v15, 0x3c0

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->XDR:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 357
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "XOF"

    const/16 v10, 0xaa

    const/16 v15, 0x3b8

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->XOF:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 359
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "XPD"

    const/16 v10, 0xab

    const/16 v15, 0x3c4

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->XPD:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 361
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "XPF"

    const/16 v10, 0xac

    const/16 v15, 0x3b9

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->XPF:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 363
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "XPT"

    const/16 v10, 0xad

    const/16 v15, 0x3c2

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->XPT:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 365
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "XTS"

    const/16 v10, 0xae

    const/16 v15, 0x3c3

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->XTS:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 367
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "XXX"

    const/16 v10, 0xaf

    const/16 v15, 0x3e7

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->XXX:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 369
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "YER"

    const/16 v10, 0xb0

    const/16 v15, 0x376

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->YER:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 371
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "ZAR"

    const/16 v10, 0xb1

    const/16 v15, 0x2c6

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->ZAR:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 373
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "ZMK"

    const/16 v10, 0xb2

    const/16 v15, 0x37e

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->ZMK:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 375
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "ZMW"

    const/16 v10, 0xb3

    const/16 v15, 0x3c7

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->ZMW:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 380
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "BTC"

    const/16 v10, 0xb4

    const/16 v15, 0x3e9

    invoke-direct {v0, v1, v10, v15}, Lcom/squareup/protos/connect/v2/common/Currency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->BTC:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v0, 0xb5

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/common/Currency;

    .line 16
    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->UNKNOWN_CURRENCY:Lcom/squareup/protos/connect/v2/common/Currency;

    const/4 v10, 0x0

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->AED:Lcom/squareup/protos/connect/v2/common/Currency;

    const/4 v10, 0x1

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->AFN:Lcom/squareup/protos/connect/v2/common/Currency;

    const/4 v10, 0x2

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->ALL:Lcom/squareup/protos/connect/v2/common/Currency;

    const/4 v10, 0x3

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->AMD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/4 v10, 0x4

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->ANG:Lcom/squareup/protos/connect/v2/common/Currency;

    const/4 v10, 0x5

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->AOA:Lcom/squareup/protos/connect/v2/common/Currency;

    const/4 v10, 0x6

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->ARS:Lcom/squareup/protos/connect/v2/common/Currency;

    const/4 v10, 0x7

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->AUD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v10, 0x8

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->AWG:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v10, 0x9

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->AZN:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v10, 0xa

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->BAM:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v10, 0xb

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->BBD:Lcom/squareup/protos/connect/v2/common/Currency;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->BDT:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0xd

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->BGN:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0xe

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->BHD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0xf

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->BIF:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0x10

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->BMD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0x11

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->BND:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0x12

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->BOB:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0x13

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->BOV:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0x14

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->BRL:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0x15

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->BSD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0x16

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->BTN:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0x17

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->BWP:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0x18

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->BYR:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0x19

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->BZD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0x1a

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->CAD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0x1b

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->CDF:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0x1c

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->CHE:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0x1d

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->CHF:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0x1e

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->CHW:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v7, 0x1f

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->CLF:Lcom/squareup/protos/connect/v2/common/Currency;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->CLP:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v4, 0x21

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->CNY:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v4, 0x22

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->COP:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v4, 0x23

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->COU:Lcom/squareup/protos/connect/v2/common/Currency;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->CRC:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v4, 0x25

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->CUC:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v4, 0x26

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->CUP:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v4, 0x27

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->CVE:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v4, 0x28

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->CZK:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v4, 0x29

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->DJF:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v4, 0x2a

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->DKK:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v4, 0x2b

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->DOP:Lcom/squareup/protos/connect/v2/common/Currency;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->DZD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->EGP:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->ERN:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->ETB:Lcom/squareup/protos/connect/v2/common/Currency;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->EUR:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->FJD:Lcom/squareup/protos/connect/v2/common/Currency;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->FKP:Lcom/squareup/protos/connect/v2/common/Currency;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->GBP:Lcom/squareup/protos/connect/v2/common/Currency;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->GEL:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->GHS:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->GIP:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->GMD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->GNF:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->GTQ:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->GYD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->HKD:Lcom/squareup/protos/connect/v2/common/Currency;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->HNL:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->HRK:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->HTG:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->HUF:Lcom/squareup/protos/connect/v2/common/Currency;

    aput-object v1, v0, v14

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->IDR:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->ILS:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->INR:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->IQD:Lcom/squareup/protos/connect/v2/common/Currency;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->IRR:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->ISK:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->JMD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->JOD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->JPY:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->KES:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->KGS:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->KHR:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->KMF:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->KPW:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->KRW:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->KWD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->KYD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->KZT:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->LAK:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->LBP:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->LKR:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->LRD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->LSL:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->LTL:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->LVL:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->LYD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->MAD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->MDL:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->MGA:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->MKD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->MMK:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->MNT:Lcom/squareup/protos/connect/v2/common/Currency;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->MOP:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->MRO:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->MUR:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->MVR:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->MWK:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->MXN:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->MXV:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->MYR:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->MZN:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->NAD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->NGN:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->NIO:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->NOK:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->NPR:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->NZD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->OMR:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->PAB:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->PEN:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->PGK:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->PHP:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->PKR:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->PLN:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->PYG:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->QAR:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->RON:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->RSD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->RUB:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->RWF:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->SAR:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->SBD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->SCR:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->SDG:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->SEK:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->SGD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->SHP:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->SLL:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->SOS:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->SRD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->SSP:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->STD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->SVC:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->SYP:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->SZL:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->THB:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->TJS:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->TMT:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->TND:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->TOP:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->TRY:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->TTD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->TWD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->TZS:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->UAH:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->UGX:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->USD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->USN:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->USS:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->UYI:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->UYU:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->UZS:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->VEF:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->VND:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->VUV:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->WST:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->XAF:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->XAG:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->XAU:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->XBA:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->XBB:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->XBC:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->XBD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->XCD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->XDR:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->XOF:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->XPD:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xab

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->XPF:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xac

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->XPT:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xad

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->XTS:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xae

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->XXX:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->YER:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->ZAR:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xb1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->ZMK:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xb2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->ZMW:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xb3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Currency;->BTC:Lcom/squareup/protos/connect/v2/common/Currency;

    const/16 v2, 0xb4

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->$VALUES:[Lcom/squareup/protos/connect/v2/common/Currency;

    .line 382
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Currency$ProtoAdapter_Currency;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/Currency$ProtoAdapter_Currency;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 386
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 387
    iput p3, p0, Lcom/squareup/protos/connect/v2/common/Currency;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/common/Currency;
    .locals 1

    if-eqz p0, :cond_a

    const/16 v0, 0xbf

    if-eq p0, v0, :cond_9

    const/16 v0, 0xc0

    if-eq p0, v0, :cond_8

    const/16 v0, 0x1a1

    if-eq p0, v0, :cond_7

    const/16 v0, 0x1a2

    if-eq p0, v0, :cond_6

    const/16 v0, 0x214

    if-eq p0, v0, :cond_5

    const/16 v0, 0x215

    if-eq p0, v0, :cond_4

    const/16 v0, 0x3ac

    if-eq p0, v0, :cond_3

    const/16 v0, 0x3ad

    if-eq p0, v0, :cond_2

    const/16 v0, 0x3af

    if-eq p0, v0, :cond_1

    const/16 v0, 0x3b0

    if-eq p0, v0, :cond_0

    sparse-switch p0, :sswitch_data_0

    packed-switch p0, :pswitch_data_0

    packed-switch p0, :pswitch_data_1

    packed-switch p0, :pswitch_data_2

    packed-switch p0, :pswitch_data_3

    const/4 p0, 0x0

    return-object p0

    .line 566
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->XPD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 569
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->XTS:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 568
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->XPT:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 557
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->XAG:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 564
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->XDR:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 558
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->XAU:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 562
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->XBD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 561
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->XBC:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 560
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->XBB:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 559
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->XBA:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 567
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->XPF:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 565
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->XOF:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 563
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->XCD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 556
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->XAF:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 540
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->TRY:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 426
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->CHW:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 424
    :pswitch_10
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->CHE:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 516
    :pswitch_11
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->RON:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 523
    :pswitch_12
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->SDG:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 552
    :pswitch_13
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->VEF:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 449
    :pswitch_14
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->GHS:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 407
    :pswitch_15
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->BBD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 399
    :pswitch_16
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->AMD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 408
    :pswitch_17
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->BDT:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 575
    :sswitch_0
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->BTC:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 570
    :sswitch_1
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->XXX:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 548
    :sswitch_2
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->USS:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 547
    :sswitch_3
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->USN:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 427
    :sswitch_4
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->CLF:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 416
    :sswitch_5
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->BRL:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 513
    :sswitch_6
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->PLN:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 415
    :sswitch_7
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->BOV:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 448
    :sswitch_8
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->GEL:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 544
    :sswitch_9
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->UAH:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 498
    :sswitch_a
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->MXV:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 444
    :sswitch_b
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->EUR:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 406
    :sswitch_c
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->BAM:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 423
    :sswitch_d
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->CDF:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 409
    :sswitch_e
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->BGN:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 420
    :sswitch_f
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->BYR:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 401
    :sswitch_10
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->AOA:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 536
    :sswitch_11
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->TJS:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 397
    :sswitch_12
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->AFN:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 431
    :sswitch_13
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->COU:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 488
    :sswitch_14
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->MGA:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 529
    :sswitch_15
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->SRD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 574
    :sswitch_16
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->ZMW:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 537
    :sswitch_17
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->TMT:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 433
    :sswitch_18
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->CUC:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 542
    :sswitch_19
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->TWD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 573
    :sswitch_1a
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->ZMK:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 571
    :sswitch_1b
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->YER:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 555
    :sswitch_1c
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->WST:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 551
    :sswitch_1d
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->UZS:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 550
    :sswitch_1e
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->UYU:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 546
    :sswitch_1f
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->USD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 543
    :sswitch_20
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->TZS:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 447
    :sswitch_21
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->GBP:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 441
    :sswitch_22
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->EGP:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 489
    :sswitch_23
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->MKD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 545
    :sswitch_24
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->UGX:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 538
    :sswitch_25
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->TND:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 396
    :sswitch_26
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->AED:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 541
    :sswitch_27
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->TTD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 539
    :sswitch_28
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->TOP:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 535
    :sswitch_29
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->THB:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 533
    :sswitch_2a
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->SYP:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 425
    :sswitch_2b
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->CHF:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 524
    :sswitch_2c
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->SEK:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 534
    :sswitch_2d
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->SZL:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 530
    :sswitch_2e
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->SSP:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 572
    :sswitch_2f
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->ZAR:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 528
    :sswitch_30
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->SOS:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 553
    :sswitch_31
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->VND:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 525
    :sswitch_32
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->SGD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 527
    :sswitch_33
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->SLL:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 522
    :sswitch_34
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->SCR:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 520
    :sswitch_35
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->SAR:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 531
    :sswitch_36
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->STD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 526
    :sswitch_37
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->SHP:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 519
    :sswitch_38
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->RWF:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 518
    :sswitch_39
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->RUB:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 515
    :sswitch_3a
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->QAR:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 511
    :sswitch_3b
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->PHP:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 509
    :sswitch_3c
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->PEN:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 514
    :sswitch_3d
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->PYG:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 510
    :sswitch_3e
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->PGK:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 508
    :sswitch_3f
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->PAB:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 512
    :sswitch_40
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->PKR:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 504
    :sswitch_41
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->NOK:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 502
    :sswitch_42
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->NGN:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 503
    :sswitch_43
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->NIO:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 506
    :sswitch_44
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->NZD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 554
    :sswitch_45
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->VUV:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 505
    :sswitch_46
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->NPR:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 501
    :sswitch_47
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->NAD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 507
    :sswitch_48
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->OMR:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 486
    :sswitch_49
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->MAD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 487
    :sswitch_4a
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->MDL:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 491
    :sswitch_4b
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->MNT:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 497
    :sswitch_4c
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->MXN:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 494
    :sswitch_4d
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->MUR:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 493
    :sswitch_4e
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->MRO:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 495
    :sswitch_4f
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->MVR:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 499
    :sswitch_50
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->MYR:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 496
    :sswitch_51
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->MWK:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 492
    :sswitch_52
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->MOP:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 483
    :sswitch_53
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->LTL:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 485
    :sswitch_54
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->LYD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 481
    :sswitch_55
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->LRD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 484
    :sswitch_56
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->LVL:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 482
    :sswitch_57
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->LSL:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 479
    :sswitch_58
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->LBP:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 475
    :sswitch_59
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->KWD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 474
    :sswitch_5a
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->KRW:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 473
    :sswitch_5b
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->KPW:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 469
    :sswitch_5c
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->KES:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 467
    :sswitch_5d
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->JOD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 477
    :sswitch_5e
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->KZT:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 468
    :sswitch_5f
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->JPY:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 466
    :sswitch_60
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->JMD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 461
    :sswitch_61
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->ILS:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 463
    :sswitch_62
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->IQD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 464
    :sswitch_63
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->IRR:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 460
    :sswitch_64
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->IDR:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 462
    :sswitch_65
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->INR:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 465
    :sswitch_66
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->ISK:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 459
    :sswitch_67
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->HUF:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 455
    :sswitch_68
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->HKD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 456
    :sswitch_69
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->HNL:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 458
    :sswitch_6a
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->HTG:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 454
    :sswitch_6b
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->GYD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 452
    :sswitch_6c
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->GNF:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 453
    :sswitch_6d
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->GTQ:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 450
    :sswitch_6e
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->GIP:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 451
    :sswitch_6f
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->GMD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 437
    :sswitch_70
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->DJF:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 445
    :sswitch_71
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->FJD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 446
    :sswitch_72
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->FKP:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 442
    :sswitch_73
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->ERN:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 443
    :sswitch_74
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->ETB:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 532
    :sswitch_75
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->SVC:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 439
    :sswitch_76
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->DOP:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 438
    :sswitch_77
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->DKK:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 436
    :sswitch_78
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->CZK:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 432
    :sswitch_79
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->CRC:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 472
    :sswitch_7a
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->KMF:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 430
    :sswitch_7b
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->COP:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 429
    :sswitch_7c
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->CNY:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 428
    :sswitch_7d
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->CLP:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 480
    :sswitch_7e
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->LKR:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 476
    :sswitch_7f
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->KYD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 435
    :sswitch_80
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->CVE:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 422
    :sswitch_81
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->CAD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 471
    :sswitch_82
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->KHR:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 411
    :sswitch_83
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->BIF:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 490
    :sswitch_84
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->MMK:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 413
    :sswitch_85
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->BND:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 521
    :sswitch_86
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->SBD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 421
    :sswitch_87
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->BZD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 419
    :sswitch_88
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->BWP:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 414
    :sswitch_89
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->BOB:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 418
    :sswitch_8a
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->BTN:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 412
    :sswitch_8b
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->BMD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 410
    :sswitch_8c
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->BHD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 417
    :sswitch_8d
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->BSD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 403
    :sswitch_8e
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->AUD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 402
    :sswitch_8f
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->ARS:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 440
    :sswitch_90
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->DZD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 398
    :sswitch_91
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->ALL:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 405
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->AZN:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 500
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->MZN:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 517
    :cond_2
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->RSD:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 549
    :cond_3
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->UYI:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 404
    :cond_4
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->AWG:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 400
    :cond_5
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->ANG:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 478
    :cond_6
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->LAK:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 470
    :cond_7
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->KGS:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 434
    :cond_8
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->CUP:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 457
    :cond_9
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->HRK:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    .line 395
    :cond_a
    :sswitch_92
    sget-object p0, Lcom/squareup/protos/connect/v2/common/Currency;->UNKNOWN_CURRENCY:Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_92
        0x8 -> :sswitch_91
        0xc -> :sswitch_90
        0x20 -> :sswitch_8f
        0x24 -> :sswitch_8e
        0x2c -> :sswitch_8d
        0x30 -> :sswitch_8c
        0x3c -> :sswitch_8b
        0x40 -> :sswitch_8a
        0x44 -> :sswitch_89
        0x48 -> :sswitch_88
        0x54 -> :sswitch_87
        0x5a -> :sswitch_86
        0x60 -> :sswitch_85
        0x68 -> :sswitch_84
        0x6c -> :sswitch_83
        0x74 -> :sswitch_82
        0x7c -> :sswitch_81
        0x84 -> :sswitch_80
        0x88 -> :sswitch_7f
        0x90 -> :sswitch_7e
        0x98 -> :sswitch_7d
        0x9c -> :sswitch_7c
        0xaa -> :sswitch_7b
        0xae -> :sswitch_7a
        0xbc -> :sswitch_79
        0xcb -> :sswitch_78
        0xd0 -> :sswitch_77
        0xd6 -> :sswitch_76
        0xde -> :sswitch_75
        0xe6 -> :sswitch_74
        0xe8 -> :sswitch_73
        0xee -> :sswitch_72
        0xf2 -> :sswitch_71
        0x106 -> :sswitch_70
        0x10e -> :sswitch_6f
        0x124 -> :sswitch_6e
        0x140 -> :sswitch_6d
        0x144 -> :sswitch_6c
        0x148 -> :sswitch_6b
        0x14c -> :sswitch_6a
        0x154 -> :sswitch_69
        0x158 -> :sswitch_68
        0x15c -> :sswitch_67
        0x160 -> :sswitch_66
        0x164 -> :sswitch_65
        0x168 -> :sswitch_64
        0x16c -> :sswitch_63
        0x170 -> :sswitch_62
        0x178 -> :sswitch_61
        0x184 -> :sswitch_60
        0x188 -> :sswitch_5f
        0x18e -> :sswitch_5e
        0x190 -> :sswitch_5d
        0x194 -> :sswitch_5c
        0x198 -> :sswitch_5b
        0x19a -> :sswitch_5a
        0x19e -> :sswitch_59
        0x1a6 -> :sswitch_58
        0x1aa -> :sswitch_57
        0x1ac -> :sswitch_56
        0x1ae -> :sswitch_55
        0x1b2 -> :sswitch_54
        0x1b8 -> :sswitch_53
        0x1be -> :sswitch_52
        0x1c6 -> :sswitch_51
        0x1ca -> :sswitch_50
        0x1ce -> :sswitch_4f
        0x1de -> :sswitch_4e
        0x1e0 -> :sswitch_4d
        0x1e4 -> :sswitch_4c
        0x1f0 -> :sswitch_4b
        0x1f2 -> :sswitch_4a
        0x1f8 -> :sswitch_49
        0x200 -> :sswitch_48
        0x204 -> :sswitch_47
        0x20c -> :sswitch_46
        0x224 -> :sswitch_45
        0x22a -> :sswitch_44
        0x22e -> :sswitch_43
        0x236 -> :sswitch_42
        0x242 -> :sswitch_41
        0x24a -> :sswitch_40
        0x24e -> :sswitch_3f
        0x256 -> :sswitch_3e
        0x258 -> :sswitch_3d
        0x25c -> :sswitch_3c
        0x260 -> :sswitch_3b
        0x27a -> :sswitch_3a
        0x283 -> :sswitch_39
        0x286 -> :sswitch_38
        0x28e -> :sswitch_37
        0x2a6 -> :sswitch_36
        0x2aa -> :sswitch_35
        0x2b2 -> :sswitch_34
        0x2b6 -> :sswitch_33
        0x2be -> :sswitch_32
        0x2c0 -> :sswitch_31
        0x2c2 -> :sswitch_30
        0x2c6 -> :sswitch_2f
        0x2d8 -> :sswitch_2e
        0x2ec -> :sswitch_2d
        0x2f0 -> :sswitch_2c
        0x2f4 -> :sswitch_2b
        0x2f8 -> :sswitch_2a
        0x2fc -> :sswitch_29
        0x308 -> :sswitch_28
        0x30c -> :sswitch_27
        0x310 -> :sswitch_26
        0x314 -> :sswitch_25
        0x320 -> :sswitch_24
        0x327 -> :sswitch_23
        0x332 -> :sswitch_22
        0x33a -> :sswitch_21
        0x342 -> :sswitch_20
        0x348 -> :sswitch_1f
        0x35a -> :sswitch_1e
        0x35c -> :sswitch_1d
        0x372 -> :sswitch_1c
        0x376 -> :sswitch_1b
        0x37e -> :sswitch_1a
        0x385 -> :sswitch_19
        0x3a3 -> :sswitch_18
        0x3a6 -> :sswitch_17
        0x3c7 -> :sswitch_16
        0x3c8 -> :sswitch_15
        0x3c9 -> :sswitch_14
        0x3ca -> :sswitch_13
        0x3cb -> :sswitch_12
        0x3cc -> :sswitch_11
        0x3cd -> :sswitch_10
        0x3ce -> :sswitch_f
        0x3cf -> :sswitch_e
        0x3d0 -> :sswitch_d
        0x3d1 -> :sswitch_c
        0x3d2 -> :sswitch_b
        0x3d3 -> :sswitch_a
        0x3d4 -> :sswitch_9
        0x3d5 -> :sswitch_8
        0x3d8 -> :sswitch_7
        0x3d9 -> :sswitch_6
        0x3da -> :sswitch_5
        0x3de -> :sswitch_4
        0x3e5 -> :sswitch_3
        0x3e6 -> :sswitch_2
        0x3e7 -> :sswitch_1
        0x3e9 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_17
        :pswitch_16
        :pswitch_15
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3a8
        :pswitch_14
        :pswitch_13
        :pswitch_12
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x3b2
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x3bb
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/Currency;
    .locals 1

    .line 16
    const-class v0, Lcom/squareup/protos/connect/v2/common/Currency;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/common/Currency;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/common/Currency;
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->$VALUES:[Lcom/squareup/protos/connect/v2/common/Currency;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/common/Currency;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/common/Currency;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 582
    iget v0, p0, Lcom/squareup/protos/connect/v2/common/Currency;->value:I

    return v0
.end method
