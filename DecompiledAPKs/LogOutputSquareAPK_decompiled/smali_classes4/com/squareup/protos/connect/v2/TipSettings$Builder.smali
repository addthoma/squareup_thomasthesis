.class public final Lcom/squareup/protos/connect/v2/TipSettings$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TipSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/TipSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/TipSettings;",
        "Lcom/squareup/protos/connect/v2/TipSettings$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allow_tipping:Ljava/lang/Boolean;

.field public custom_tip_field:Ljava/lang/Boolean;

.field public separate_tip_screen:Ljava/lang/Boolean;

.field public tip_percentages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 150
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 151
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/TipSettings$Builder;->tip_percentages:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public allow_tipping(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/TipSettings$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TipSettings$Builder;->allow_tipping:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/TipSettings;
    .locals 7

    .line 199
    new-instance v6, Lcom/squareup/protos/connect/v2/TipSettings;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings$Builder;->allow_tipping:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/TipSettings$Builder;->separate_tip_screen:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/TipSettings$Builder;->custom_tip_field:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/TipSettings$Builder;->tip_percentages:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/connect/v2/TipSettings;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 141
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/TipSettings$Builder;->build()Lcom/squareup/protos/connect/v2/TipSettings;

    move-result-object v0

    return-object v0
.end method

.method public custom_tip_field(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/TipSettings$Builder;
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TipSettings$Builder;->custom_tip_field:Ljava/lang/Boolean;

    return-object p0
.end method

.method public separate_tip_screen(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/TipSettings$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TipSettings$Builder;->separate_tip_screen:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tip_percentages(Ljava/util/List;)Lcom/squareup/protos/connect/v2/TipSettings$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/TipSettings$Builder;"
        }
    .end annotation

    .line 192
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 193
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TipSettings$Builder;->tip_percentages:Ljava/util/List;

    return-object p0
.end method
