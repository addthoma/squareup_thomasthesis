.class final Lcom/squareup/protos/connect/v2/CreatePaymentRequest$ProtoAdapter_CreatePaymentRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CreatePaymentRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/CreatePaymentRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CreatePaymentRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/CreatePaymentRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1287
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1366
    new-instance v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;-><init>()V

    .line 1367
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1368
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 1403
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1401
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->delay_action(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto :goto_0

    .line 1400
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->external_details(Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto :goto_0

    .line 1399
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/connect/v2/CashPaymentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->cash_details(Lcom/squareup/protos/connect/v2/CashPaymentDetails;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto :goto_0

    .line 1398
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/connect/v2/CardPresentOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/CardPresentOptions;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->card_present_options(Lcom/squareup/protos/connect/v2/CardPresentOptions;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto :goto_0

    .line 1397
    :pswitch_5
    iget-object v3, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->auxiliary_info:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/connect/v2/AuxiliaryInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1396
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/connect/v2/GeoLocation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/GeoLocation;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->geo_location(Lcom/squareup/protos/connect/v2/GeoLocation;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto :goto_0

    .line 1395
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->employee_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto :goto_0

    .line 1394
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->requested_statement_description(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto :goto_0

    .line 1393
    :pswitch_9
    iget-object v3, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->revenue_association_tags:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1392
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->statement_description_identifier(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1391
    :pswitch_b
    sget-object v3, Lcom/squareup/protos/connect/v2/PaymentFeesSpec;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->payment_fees_spec(Lcom/squareup/protos/connect/v2/PaymentFeesSpec;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1390
    :pswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->verification_token(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1389
    :pswitch_d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->note(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1388
    :pswitch_e
    sget-object v3, Lcom/squareup/protos/connect/v2/resources/Address;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/resources/Address;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->shipping_address(Lcom/squareup/protos/connect/v2/resources/Address;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1387
    :pswitch_f
    sget-object v3, Lcom/squareup/protos/connect/v2/resources/Address;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/resources/Address;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->billing_address(Lcom/squareup/protos/connect/v2/resources/Address;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1386
    :pswitch_10
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->buyer_email_address(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1385
    :pswitch_11
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->tax_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1384
    :pswitch_12
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->accept_partial_authorization(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1383
    :pswitch_13
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->terminal_checkout_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1382
    :pswitch_14
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->reference_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1381
    :pswitch_15
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->create_order(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1380
    :pswitch_16
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->order_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1379
    :pswitch_17
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->location_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1378
    :pswitch_18
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->payment_config_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1377
    :pswitch_19
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->customer_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1376
    :pswitch_1a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->autocomplete(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1375
    :pswitch_1b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->delay_duration(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1374
    :pswitch_1c
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->app_fee_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1373
    :pswitch_1d
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->tip_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1372
    :pswitch_1e
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1371
    :pswitch_1f
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->idempotency_key(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1370
    :pswitch_20
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->source_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    goto/16 :goto_0

    .line 1407
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1408
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->build()Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1285
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$ProtoAdapter_CreatePaymentRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/CreatePaymentRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1329
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->source_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1330
    sget-object v0, Lcom/squareup/protos/connect/v2/CardPresentOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->card_present_options:Lcom/squareup/protos/connect/v2/CardPresentOptions;

    const/16 v2, 0x21

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1331
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->idempotency_key:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1332
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1333
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1334
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v2, 0x13

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1335
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1336
    sget-object v0, Lcom/squareup/protos/connect/v2/PaymentFeesSpec;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    const/16 v2, 0x19

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1337
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->delay_duration:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1338
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->delay_action:Ljava/lang/String;

    const/16 v2, 0x24

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1339
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->autocomplete:Ljava/lang/Boolean;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1340
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->create_order:Ljava/lang/Boolean;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1341
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->order_id:Ljava/lang/String;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1342
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->customer_id:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1343
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->payment_config_id:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1344
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->location_id:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1345
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->employee_id:Ljava/lang/String;

    const/16 v2, 0x1d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1346
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->reference_id:Ljava/lang/String;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1347
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->verification_token:Ljava/lang/String;

    const/16 v2, 0x18

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1348
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->terminal_checkout_id:Ljava/lang/String;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1349
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->accept_partial_authorization:Ljava/lang/Boolean;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1350
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->buyer_email_address:Ljava/lang/String;

    const/16 v2, 0x14

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1351
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Address;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    const/16 v2, 0x15

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1352
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Address;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    const/16 v2, 0x16

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1353
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->note:Ljava/lang/String;

    const/16 v2, 0x17

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1354
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->statement_description_identifier:Ljava/lang/String;

    const/16 v2, 0x1a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1355
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->requested_statement_description:Ljava/lang/String;

    const/16 v2, 0x1c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1356
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->revenue_association_tags:Ljava/util/List;

    const/16 v2, 0x1b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1357
    sget-object v0, Lcom/squareup/protos/connect/v2/GeoLocation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->geo_location:Lcom/squareup/protos/connect/v2/GeoLocation;

    const/16 v2, 0x1f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1358
    sget-object v0, Lcom/squareup/protos/connect/v2/AuxiliaryInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->auxiliary_info:Ljava/util/List;

    const/16 v2, 0x20

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1359
    sget-object v0, Lcom/squareup/protos/connect/v2/CashPaymentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    const/16 v2, 0x22

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1360
    sget-object v0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    const/16 v2, 0x23

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1361
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1285
    check-cast p2, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$ProtoAdapter_CreatePaymentRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/CreatePaymentRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/CreatePaymentRequest;)I
    .locals 4

    .line 1292
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->source_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/connect/v2/CardPresentOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->card_present_options:Lcom/squareup/protos/connect/v2/CardPresentOptions;

    const/16 v3, 0x21

    .line 1293
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->idempotency_key:Ljava/lang/String;

    const/4 v3, 0x2

    .line 1294
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v3, 0x3

    .line 1295
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v3, 0x4

    .line 1296
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v3, 0x13

    .line 1297
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v3, 0x5

    .line 1298
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/PaymentFeesSpec;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    const/16 v3, 0x19

    .line 1299
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->delay_duration:Ljava/lang/String;

    const/4 v3, 0x6

    .line 1300
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->delay_action:Ljava/lang/String;

    const/16 v3, 0x24

    .line 1301
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->autocomplete:Ljava/lang/Boolean;

    const/4 v3, 0x7

    .line 1302
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->create_order:Ljava/lang/Boolean;

    const/16 v3, 0xc

    .line 1303
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->order_id:Ljava/lang/String;

    const/16 v3, 0xb

    .line 1304
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->customer_id:Ljava/lang/String;

    const/16 v3, 0x8

    .line 1305
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->payment_config_id:Ljava/lang/String;

    const/16 v3, 0x9

    .line 1306
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->location_id:Ljava/lang/String;

    const/16 v3, 0xa

    .line 1307
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->employee_id:Ljava/lang/String;

    const/16 v3, 0x1d

    .line 1308
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->reference_id:Ljava/lang/String;

    const/16 v3, 0xd

    .line 1309
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->verification_token:Ljava/lang/String;

    const/16 v3, 0x18

    .line 1310
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->terminal_checkout_id:Ljava/lang/String;

    const/16 v3, 0x11

    .line 1311
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->accept_partial_authorization:Ljava/lang/Boolean;

    const/16 v3, 0x12

    .line 1312
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->buyer_email_address:Ljava/lang/String;

    const/16 v3, 0x14

    .line 1313
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Address;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    const/16 v3, 0x15

    .line 1314
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Address;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    const/16 v3, 0x16

    .line 1315
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->note:Ljava/lang/String;

    const/16 v3, 0x17

    .line 1316
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->statement_description_identifier:Ljava/lang/String;

    const/16 v3, 0x1a

    .line 1317
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->requested_statement_description:Ljava/lang/String;

    const/16 v3, 0x1c

    .line 1318
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 1319
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->revenue_association_tags:Ljava/util/List;

    const/16 v3, 0x1b

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/GeoLocation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->geo_location:Lcom/squareup/protos/connect/v2/GeoLocation;

    const/16 v3, 0x1f

    .line 1320
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/AuxiliaryInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1321
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->auxiliary_info:Ljava/util/List;

    const/16 v3, 0x20

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/CashPaymentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    const/16 v3, 0x22

    .line 1322
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    const/16 v3, 0x23

    .line 1323
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1324
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1285
    check-cast p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$ProtoAdapter_CreatePaymentRequest;->encodedSize(Lcom/squareup/protos/connect/v2/CreatePaymentRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/CreatePaymentRequest;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest;
    .locals 3

    .line 1413
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->newBuilder()Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 1414
    iput-object v0, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->source_id:Ljava/lang/String;

    .line 1415
    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->card_present_options:Lcom/squareup/protos/connect/v2/CardPresentOptions;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/protos/connect/v2/CardPresentOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->card_present_options:Lcom/squareup/protos/connect/v2/CardPresentOptions;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/CardPresentOptions;

    iput-object v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->card_present_options:Lcom/squareup/protos/connect/v2/CardPresentOptions;

    .line 1416
    :cond_0
    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 1417
    :cond_1
    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 1418
    :cond_2
    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_3

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 1419
    :cond_3
    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_4

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 1420
    :cond_4
    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    if-eqz v1, :cond_5

    sget-object v1, Lcom/squareup/protos/connect/v2/PaymentFeesSpec;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    iput-object v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    .line 1421
    :cond_5
    iput-object v0, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->buyer_email_address:Ljava/lang/String;

    .line 1422
    iput-object v0, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 1423
    iput-object v0, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 1424
    iput-object v0, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->note:Ljava/lang/String;

    .line 1425
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->geo_location:Lcom/squareup/protos/connect/v2/GeoLocation;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/connect/v2/GeoLocation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->geo_location:Lcom/squareup/protos/connect/v2/GeoLocation;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/GeoLocation;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->geo_location:Lcom/squareup/protos/connect/v2/GeoLocation;

    .line 1426
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->auxiliary_info:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/connect/v2/AuxiliaryInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1427
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/connect/v2/CashPaymentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    .line 1428
    :cond_7
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    .line 1429
    :cond_8
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1430
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->build()Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1285
    check-cast p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$ProtoAdapter_CreatePaymentRequest;->redact(Lcom/squareup/protos/connect/v2/CreatePaymentRequest;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    move-result-object p1

    return-object p1
.end method
