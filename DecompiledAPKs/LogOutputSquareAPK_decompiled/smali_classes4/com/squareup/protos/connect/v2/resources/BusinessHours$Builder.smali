.class public final Lcom/squareup/protos/connect/v2/resources/BusinessHours$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BusinessHours.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/BusinessHours;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/resources/BusinessHours;",
        "Lcom/squareup/protos/connect/v2/resources/BusinessHours$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public periods:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 91
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 92
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/resources/BusinessHours$Builder;->periods:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/resources/BusinessHours;
    .locals 3

    .line 109
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/BusinessHours;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/BusinessHours$Builder;->periods:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/connect/v2/resources/BusinessHours;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/BusinessHours$Builder;->build()Lcom/squareup/protos/connect/v2/resources/BusinessHours;

    move-result-object v0

    return-object v0
.end method

.method public periods(Ljava/util/List;)Lcom/squareup/protos/connect/v2/resources/BusinessHours$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/BusinessHoursPeriod;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/resources/BusinessHours$Builder;"
        }
    .end annotation

    .line 102
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/BusinessHours$Builder;->periods:Ljava/util/List;

    return-object p0
.end method
