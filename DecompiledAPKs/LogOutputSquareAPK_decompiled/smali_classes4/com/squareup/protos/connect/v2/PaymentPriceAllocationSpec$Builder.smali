.class public final Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PaymentPriceAllocationSpec.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;",
        "Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public fee_calculation_amount:Lcom/squareup/protos/connect/v2/common/Money;

.field public party:Ljava/lang/String;

.field public price_selectors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 133
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 134
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;->price_selectors:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;
    .locals 5

    .line 171
    new-instance v0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;->party:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;->price_selectors:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;->fee_calculation_amount:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 126
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;->build()Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec;

    move-result-object v0

    return-object v0
.end method

.method public fee_calculation_amount(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;->fee_calculation_amount:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public party(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;->party:Ljava/lang/String;

    return-object p0
.end method

.method public price_selectors(Ljava/util/List;)Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;"
        }
    .end annotation

    .line 154
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 155
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/PaymentPriceAllocationSpec$Builder;->price_selectors:Ljava/util/List;

    return-object p0
.end method
