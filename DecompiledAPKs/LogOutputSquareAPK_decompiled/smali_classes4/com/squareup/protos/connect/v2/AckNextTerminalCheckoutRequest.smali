.class public final Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;
.super Lcom/squareup/wire/Message;
.source "AckNextTerminalCheckoutRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$ProtoAdapter_AckNextTerminalCheckoutRequest;,
        Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;",
        "Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACK_REASON:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

.field public static final DEFAULT_DEVICE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_IDEMPOTENCY_KEY:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.AckReasons$AckReason#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final device_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final idempotency_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$ProtoAdapter_AckNextTerminalCheckoutRequest;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$ProtoAdapter_AckNextTerminalCheckoutRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->DO_NOT_USE:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    sput-object v0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->DEFAULT_ACK_REASON:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/AckReasons$AckReason;)V
    .locals 1

    .line 68
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/AckReasons$AckReason;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/AckReasons$AckReason;Lokio/ByteString;)V
    .locals 1

    .line 73
    sget-object v0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 74
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->idempotency_key:Ljava/lang/String;

    .line 75
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->device_id:Ljava/lang/String;

    .line 76
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 92
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 93
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->idempotency_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->idempotency_key:Ljava/lang/String;

    .line 95
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->device_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->device_id:Ljava/lang/String;

    .line 96
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    .line 97
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 102
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->idempotency_key:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->device_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 108
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;
    .locals 2

    .line 81
    new-instance v0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;-><init>()V

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->idempotency_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->idempotency_key:Ljava/lang/String;

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->device_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->device_id:Ljava/lang/String;

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->newBuilder()Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->idempotency_key:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", idempotency_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->idempotency_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->device_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", device_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->device_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    if-eqz v1, :cond_2

    const-string v1, ", ack_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutRequest;->ack_reason:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AckNextTerminalCheckoutRequest{"

    .line 119
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
