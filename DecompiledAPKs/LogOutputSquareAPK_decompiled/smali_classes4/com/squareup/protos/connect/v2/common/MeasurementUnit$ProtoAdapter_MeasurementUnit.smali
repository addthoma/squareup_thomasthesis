.class final Lcom/squareup/protos/connect/v2/common/MeasurementUnit$ProtoAdapter_MeasurementUnit;
.super Lcom/squareup/wire/ProtoAdapter;
.source "MeasurementUnit.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_MeasurementUnit"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1184
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1215
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    .line 1216
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1217
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 1277
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1270
    :pswitch_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->type(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 1272
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 1262
    :pswitch_1
    :try_start_1
    sget-object v4, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->time_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 1264
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 1254
    :pswitch_2
    :try_start_2
    sget-object v4, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->generic_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;
    :try_end_2
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v4

    .line 1256
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 1246
    :pswitch_3
    :try_start_3
    sget-object v4, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->weight_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;
    :try_end_3
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    :catch_3
    move-exception v4

    .line 1248
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 1238
    :pswitch_4
    :try_start_4
    sget-object v4, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->volume_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;
    :try_end_4
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v4

    .line 1240
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 1230
    :pswitch_5
    :try_start_5
    sget-object v4, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->length_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;
    :try_end_5
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_0

    :catch_5
    move-exception v4

    .line 1232
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 1222
    :pswitch_6
    :try_start_6
    sget-object v4, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->area_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;
    :try_end_6
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_0

    :catch_6
    move-exception v4

    .line 1224
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 1219
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->custom_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    goto/16 :goto_0

    .line 1281
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1282
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1182
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$ProtoAdapter_MeasurementUnit;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1202
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1203
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1204
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1205
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1206
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1207
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->generic_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1208
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1209
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->type:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1210
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1182
    check-cast p2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$ProtoAdapter_MeasurementUnit;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)I
    .locals 4

    .line 1189
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    const/4 v3, 0x2

    .line 1190
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    const/4 v3, 0x3

    .line 1191
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    const/4 v3, 0x4

    .line 1192
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    const/4 v3, 0x5

    .line 1193
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->generic_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    const/4 v3, 0x6

    .line 1194
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    const/4 v3, 0x7

    .line 1195
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->type:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    const/16 v3, 0x8

    .line 1196
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1197
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1182
    check-cast p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$ProtoAdapter_MeasurementUnit;->encodedSize(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
    .locals 2

    .line 1287
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->newBuilder()Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object p1

    .line 1288
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->custom_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Custom;

    .line 1289
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1290
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1182
    check-cast p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$ProtoAdapter_MeasurementUnit;->redact(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    return-object p1
.end method
