.class public final Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;
.super Lcom/squareup/wire/Message;
.source "CreateMobileAuthorizationCodeResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$ProtoAdapter_CreateMobileAuthorizationCodeResponse;,
        Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;",
        "Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AUTHORIZATION_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_EXPIRES_AT:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final authorization_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final error:Lcom/squareup/protos/xp/v1/Error;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.xp.v1.Error#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final expires_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$ProtoAdapter_CreateMobileAuthorizationCodeResponse;

    invoke-direct {v0}, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$ProtoAdapter_CreateMobileAuthorizationCodeResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/xp/v1/Error;)V
    .locals 1

    .line 73
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/xp/v1/Error;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/xp/v1/Error;Lokio/ByteString;)V
    .locals 1

    .line 78
    sget-object v0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 79
    iput-object p1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->authorization_code:Ljava/lang/String;

    .line 80
    iput-object p2, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->expires_at:Ljava/lang/String;

    .line 81
    iput-object p3, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->error:Lcom/squareup/protos/xp/v1/Error;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 97
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 98
    :cond_1
    check-cast p1, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->authorization_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->authorization_code:Ljava/lang/String;

    .line 100
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->expires_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->expires_at:Ljava/lang/String;

    .line 101
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->error:Lcom/squareup/protos/xp/v1/Error;

    iget-object p1, p1, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->error:Lcom/squareup/protos/xp/v1/Error;

    .line 102
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 107
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->authorization_code:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->expires_at:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->error:Lcom/squareup/protos/xp/v1/Error;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/xp/v1/Error;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 113
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;
    .locals 2

    .line 86
    new-instance v0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;-><init>()V

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->authorization_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;->authorization_code:Ljava/lang/String;

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->expires_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;->expires_at:Ljava/lang/String;

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->error:Lcom/squareup/protos/xp/v1/Error;

    iput-object v1, v0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;->error:Lcom/squareup/protos/xp/v1/Error;

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->newBuilder()Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->authorization_code:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", authorization_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->authorization_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->expires_at:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", expires_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->expires_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->error:Lcom/squareup/protos/xp/v1/Error;

    if-eqz v1, :cond_2

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/CreateMobileAuthorizationCodeResponse;->error:Lcom/squareup/protos/xp/v1/Error;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CreateMobileAuthorizationCodeResponse{"

    .line 124
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
