.class public final Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RedeemMobileAuthorizationCodeResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;",
        "Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error:Lcom/squareup/protos/xp/v1/Error;

.field public session_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 110
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;
    .locals 4

    .line 135
    new-instance v0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;

    iget-object v1, p0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$Builder;->session_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$Builder;->error:Lcom/squareup/protos/xp/v1/Error;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;-><init>(Ljava/lang/String;Lcom/squareup/protos/xp/v1/Error;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$Builder;->build()Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse;

    move-result-object v0

    return-object v0
.end method

.method public error(Lcom/squareup/protos/xp/v1/Error;)Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$Builder;->error:Lcom/squareup/protos/xp/v1/Error;

    return-object p0
.end method

.method public session_token(Ljava/lang/String;)Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/oauth/v1/RedeemMobileAuthorizationCodeResponse$Builder;->session_token:Ljava/lang/String;

    return-object p0
.end method
