.class public final Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InventoryAdjustment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/inventory/InventoryAdjustment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/inventory/InventoryAdjustment;",
        "Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public adjustment_id:Ljava/lang/Integer;

.field public adjustment_type:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

.field public inventory_adjustment_line:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/inventory/InventoryAdjustmentLine;",
            ">;"
        }
    .end annotation
.end field

.field public memo:Ljava/lang/String;

.field public payment_create_unique_key:Ljava/lang/String;

.field public transaction_timestamp:Lcom/squareup/protos/common/time/DateTime;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 189
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 190
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->inventory_adjustment_line:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public adjustment_id(Ljava/lang/Integer;)Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->adjustment_id:Ljava/lang/Integer;

    return-object p0
.end method

.method public adjustment_type(Lcom/squareup/protos/inventory/InventoryAdjustmentType;)Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->adjustment_type:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/inventory/InventoryAdjustment;
    .locals 9

    .line 251
    new-instance v8, Lcom/squareup/protos/inventory/InventoryAdjustment;

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->adjustment_id:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->inventory_adjustment_line:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->adjustment_type:Lcom/squareup/protos/inventory/InventoryAdjustmentType;

    iget-object v4, p0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->payment_create_unique_key:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->memo:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->transaction_timestamp:Lcom/squareup/protos/common/time/DateTime;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/inventory/InventoryAdjustment;-><init>(Ljava/lang/Integer;Ljava/util/List;Lcom/squareup/protos/inventory/InventoryAdjustmentType;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 176
    invoke-virtual {p0}, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->build()Lcom/squareup/protos/inventory/InventoryAdjustment;

    move-result-object v0

    return-object v0
.end method

.method public inventory_adjustment_line(Ljava/util/List;)Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/inventory/InventoryAdjustmentLine;",
            ">;)",
            "Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;"
        }
    .end annotation

    .line 206
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 207
    iput-object p1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->inventory_adjustment_line:Ljava/util/List;

    return-object p0
.end method

.method public memo(Ljava/lang/String;)Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;
    .locals 0

    .line 233
    iput-object p1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->memo:Ljava/lang/String;

    return-object p0
.end method

.method public payment_create_unique_key(Ljava/lang/String;)Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;
    .locals 0

    .line 225
    iput-object p1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->payment_create_unique_key:Ljava/lang/String;

    return-object p0
.end method

.method public transaction_timestamp(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/squareup/protos/inventory/InventoryAdjustment$Builder;->transaction_timestamp:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method
