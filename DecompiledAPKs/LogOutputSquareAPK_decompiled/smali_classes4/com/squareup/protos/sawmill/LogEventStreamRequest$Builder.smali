.class public final Lcom/squareup/protos/sawmill/LogEventStreamRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LogEventStreamRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/sawmill/LogEventStreamRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/sawmill/LogEventStreamRequest;",
        "Lcom/squareup/protos/sawmill/LogEventStreamRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/eventstream/v1/Event;",
            ">;"
        }
    .end annotation
.end field

.field public v2_events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 108
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 109
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/sawmill/LogEventStreamRequest$Builder;->events:Ljava/util/List;

    .line 110
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/sawmill/LogEventStreamRequest$Builder;->v2_events:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/sawmill/LogEventStreamRequest;
    .locals 4

    .line 133
    new-instance v0, Lcom/squareup/protos/sawmill/LogEventStreamRequest;

    iget-object v1, p0, Lcom/squareup/protos/sawmill/LogEventStreamRequest$Builder;->events:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/sawmill/LogEventStreamRequest$Builder;->v2_events:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/sawmill/LogEventStreamRequest;-><init>(Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/sawmill/LogEventStreamRequest$Builder;->build()Lcom/squareup/protos/sawmill/LogEventStreamRequest;

    move-result-object v0

    return-object v0
.end method

.method public events(Ljava/util/List;)Lcom/squareup/protos/sawmill/LogEventStreamRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/eventstream/v1/Event;",
            ">;)",
            "Lcom/squareup/protos/sawmill/LogEventStreamRequest$Builder;"
        }
    .end annotation

    .line 117
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/sawmill/LogEventStreamRequest$Builder;->events:Ljava/util/List;

    return-object p0
.end method

.method public v2_events(Ljava/util/List;)Lcom/squareup/protos/sawmill/LogEventStreamRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/sawmill/EventstreamV2Event;",
            ">;)",
            "Lcom/squareup/protos/sawmill/LogEventStreamRequest$Builder;"
        }
    .end annotation

    .line 126
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/sawmill/LogEventStreamRequest$Builder;->v2_events:Ljava/util/List;

    return-object p0
.end method
