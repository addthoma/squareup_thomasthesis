.class public final Lcom/squareup/protos/invoice/v2/common/Status;
.super Lcom/squareup/wire/Message;
.source "Status.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/invoice/v2/common/Status$ProtoAdapter_Status;,
        Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;,
        Lcom/squareup/protos/invoice/v2/common/Status$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/invoice/v2/common/Status;",
        "Lcom/squareup/protos/invoice/v2/common/Status$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/invoice/v2/common/Status;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_LOCALIZED_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_LOCALIZED_TITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS_CODE:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

.field public static final DEFAULT_SUCCESS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final localized_description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final localized_title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final status_code:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.invoice.v2.common.Status$StatusCode#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final success:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/invoice/v2/common/Status$ProtoAdapter_Status;

    invoke-direct {v0}, Lcom/squareup/protos/invoice/v2/common/Status$ProtoAdapter_Status;-><init>()V

    sput-object v0, Lcom/squareup/protos/invoice/v2/common/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/invoice/v2/common/Status;->DEFAULT_SUCCESS:Ljava/lang/Boolean;

    .line 33
    sget-object v0, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->DO_NOT_USE:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    sput-object v0, Lcom/squareup/protos/invoice/v2/common/Status;->DEFAULT_STATUS_CODE:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;)V
    .locals 6

    .line 61
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/invoice/v2/common/Status;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;Lokio/ByteString;)V
    .locals 1

    .line 66
    sget-object v0, Lcom/squareup/protos/invoice/v2/common/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 67
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->success:Ljava/lang/Boolean;

    .line 68
    iput-object p2, p0, Lcom/squareup/protos/invoice/v2/common/Status;->localized_title:Ljava/lang/String;

    .line 69
    iput-object p3, p0, Lcom/squareup/protos/invoice/v2/common/Status;->localized_description:Ljava/lang/String;

    .line 70
    iput-object p4, p0, Lcom/squareup/protos/invoice/v2/common/Status;->status_code:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 87
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/invoice/v2/common/Status;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 88
    :cond_1
    check-cast p1, Lcom/squareup/protos/invoice/v2/common/Status;

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/invoice/v2/common/Status;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/invoice/v2/common/Status;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->success:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/invoice/v2/common/Status;->success:Ljava/lang/Boolean;

    .line 90
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->localized_title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/invoice/v2/common/Status;->localized_title:Ljava/lang/String;

    .line 91
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->localized_description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/invoice/v2/common/Status;->localized_description:Ljava/lang/String;

    .line 92
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->status_code:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    iget-object p1, p1, Lcom/squareup/protos/invoice/v2/common/Status;->status_code:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    .line 93
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 98
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/invoice/v2/common/Status;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->success:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->localized_title:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->localized_description:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->status_code:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 105
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/invoice/v2/common/Status$Builder;
    .locals 2

    .line 75
    new-instance v0, Lcom/squareup/protos/invoice/v2/common/Status$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/invoice/v2/common/Status$Builder;-><init>()V

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->success:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/invoice/v2/common/Status$Builder;->success:Ljava/lang/Boolean;

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->localized_title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/invoice/v2/common/Status$Builder;->localized_title:Ljava/lang/String;

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->localized_description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/invoice/v2/common/Status$Builder;->localized_description:Ljava/lang/String;

    .line 79
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->status_code:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    iput-object v1, v0, Lcom/squareup/protos/invoice/v2/common/Status$Builder;->status_code:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    .line 80
    invoke-virtual {p0}, Lcom/squareup/protos/invoice/v2/common/Status;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/invoice/v2/common/Status$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/invoice/v2/common/Status;->newBuilder()Lcom/squareup/protos/invoice/v2/common/Status$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->success:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", success="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 114
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->localized_title:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", localized_title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->localized_title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->localized_description:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", localized_description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->localized_description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->status_code:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    if-eqz v1, :cond_3

    const-string v1, ", status_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/common/Status;->status_code:Lcom/squareup/protos/invoice/v2/common/Status$StatusCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Status{"

    .line 117
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
