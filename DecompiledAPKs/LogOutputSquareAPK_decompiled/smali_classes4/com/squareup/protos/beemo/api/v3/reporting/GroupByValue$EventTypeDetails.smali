.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;
.super Lcom/squareup/wire/Message;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EventTypeDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails$ProtoAdapter_EventTypeDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EVENT_TYPE:Lcom/squareup/protos/beemo/v3/EventType;

.field private static final serialVersionUID:J


# instance fields
.field public final event_type:Lcom/squareup/protos/beemo/v3/EventType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.v3.EventType#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 4735
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails$ProtoAdapter_EventTypeDetails;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails$ProtoAdapter_EventTypeDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 4739
    sget-object v0, Lcom/squareup/protos/beemo/v3/EventType;->PAYMENT:Lcom/squareup/protos/beemo/v3/EventType;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->DEFAULT_EVENT_TYPE:Lcom/squareup/protos/beemo/v3/EventType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/v3/EventType;)V
    .locals 1

    .line 4748
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;-><init>(Lcom/squareup/protos/beemo/v3/EventType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/v3/EventType;Lokio/ByteString;)V
    .locals 1

    .line 4752
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 4753
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->event_type:Lcom/squareup/protos/beemo/v3/EventType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 4767
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 4768
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    .line 4769
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->event_type:Lcom/squareup/protos/beemo/v3/EventType;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->event_type:Lcom/squareup/protos/beemo/v3/EventType;

    .line 4770
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 4775
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 4777
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 4778
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->event_type:Lcom/squareup/protos/beemo/v3/EventType;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/v3/EventType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 4779
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails$Builder;
    .locals 2

    .line 4758
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails$Builder;-><init>()V

    .line 4759
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->event_type:Lcom/squareup/protos/beemo/v3/EventType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails$Builder;->event_type:Lcom/squareup/protos/beemo/v3/EventType;

    .line 4760
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 4734
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 4786
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4787
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->event_type:Lcom/squareup/protos/beemo/v3/EventType;

    if-eqz v1, :cond_0

    const-string v1, ", event_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->event_type:Lcom/squareup/protos/beemo/v3/EventType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "EventTypeDetails{"

    .line 4788
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
