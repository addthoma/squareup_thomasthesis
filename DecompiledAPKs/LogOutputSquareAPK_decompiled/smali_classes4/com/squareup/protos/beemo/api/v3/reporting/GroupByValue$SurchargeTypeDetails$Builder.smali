.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public surcharge_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8137
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;
    .locals 3

    .line 8147
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$Builder;->surcharge_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 8134
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    move-result-object v0

    return-object v0
.end method

.method public surcharge_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$Builder;
    .locals 0

    .line 8141
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$Builder;->surcharge_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails$SurchargeType;

    return-object p0
.end method
