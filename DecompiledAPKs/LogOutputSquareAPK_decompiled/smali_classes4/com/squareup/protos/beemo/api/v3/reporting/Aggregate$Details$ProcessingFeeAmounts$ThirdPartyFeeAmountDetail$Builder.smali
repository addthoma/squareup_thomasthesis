.class public final Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Aggregate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount:Lcom/squareup/protos/common/Money;

.field public third_party_fee_entity:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1809
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;
    .locals 0

    .line 1816
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;->amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;
    .locals 4

    .line 1830
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;->amount:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;->third_party_fee_entity:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1804
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail;

    move-result-object v0

    return-object v0
.end method

.method public third_party_fee_entity(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;
    .locals 0

    .line 1824
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeAmountDetail$Builder;->third_party_fee_entity:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts$ThirdPartyFeeEntity;

    return-object p0
.end method
