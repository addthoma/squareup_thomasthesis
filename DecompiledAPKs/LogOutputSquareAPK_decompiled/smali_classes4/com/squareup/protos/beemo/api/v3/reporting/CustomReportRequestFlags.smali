.class public final Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;
.super Lcom/squareup/wire/Message;
.source "CustomReportRequestFlags.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$ProtoAdapter_CustomReportRequestFlags;,
        Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CACHE_CUSTOM_REPORT:Ljava/lang/Boolean;

.field public static final DEFAULT_LOCAL_AGGREGATION_REPORT_SIZE_LIMIT_OVERRIDE:Ljava/lang/Integer;

.field public static final DEFAULT_PROTO_AGGREGATOR_BILL_EVENT_COUNT_LIMIT_OVERRIDE:Ljava/lang/Integer;

.field public static final DEFAULT_PROTO_AGGREGATOR_REPORT_SIZE_LIMIT_OVERRIDE:Ljava/lang/Integer;

.field public static final DEFAULT_REQUEST_TIMEOUT_OVERRIDE_SECONDS:Ljava/lang/Integer;

.field public static final DEFAULT_SPANNER_MAX_FANOUTS_PER_REQUEST:Ljava/lang/Integer;

.field public static final DEFAULT_SPANNER_MAX_THREADS_PER_REQUEST:Ljava/lang/Integer;

.field public static final DEFAULT_SPANNER_SCAN_INFOS_PER_THREAD:Ljava/lang/Integer;

.field public static final DEFAULT_SPANNER_SCAN_INFO_THRESHOLD_SIZE:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final cache_custom_report:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x10
    .end annotation
.end field

.field public final local_aggregation_report_size_limit_override:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field

.field public final proto_aggregator_bill_event_count_limit_override:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field

.field public final proto_aggregator_report_size_limit_override:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field

.field public final request_timeout_override_seconds:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field

.field public final spanner_max_fanouts_per_request:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xb
    .end annotation
.end field

.field public final spanner_max_threads_per_request:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xc
    .end annotation
.end field

.field public final spanner_scan_info_threshold_size:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xa
    .end annotation
.end field

.field public final spanner_scan_infos_per_thread:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x8
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 22
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$ProtoAdapter_CustomReportRequestFlags;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$ProtoAdapter_CustomReportRequestFlags;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 26
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->DEFAULT_REQUEST_TIMEOUT_OVERRIDE_SECONDS:Ljava/lang/Integer;

    .line 28
    sput-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->DEFAULT_LOCAL_AGGREGATION_REPORT_SIZE_LIMIT_OVERRIDE:Ljava/lang/Integer;

    .line 30
    sput-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->DEFAULT_PROTO_AGGREGATOR_REPORT_SIZE_LIMIT_OVERRIDE:Ljava/lang/Integer;

    .line 32
    sput-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->DEFAULT_PROTO_AGGREGATOR_BILL_EVENT_COUNT_LIMIT_OVERRIDE:Ljava/lang/Integer;

    const/4 v1, 0x5

    .line 34
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->DEFAULT_SPANNER_SCAN_INFOS_PER_THREAD:Ljava/lang/Integer;

    const/16 v1, 0xa

    .line 36
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->DEFAULT_SPANNER_SCAN_INFO_THRESHOLD_SIZE:Ljava/lang/Integer;

    const/16 v1, 0x14

    .line 38
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->DEFAULT_SPANNER_MAX_FANOUTS_PER_REQUEST:Ljava/lang/Integer;

    const/16 v1, 0x10

    .line 40
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->DEFAULT_SPANNER_MAX_THREADS_PER_REQUEST:Ljava/lang/Integer;

    .line 42
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->DEFAULT_CACHE_CUSTOM_REPORT:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;)V
    .locals 11

    .line 115
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 125
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->request_timeout_override_seconds:Ljava/lang/Integer;

    .line 127
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->local_aggregation_report_size_limit_override:Ljava/lang/Integer;

    .line 128
    iput-object p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->proto_aggregator_report_size_limit_override:Ljava/lang/Integer;

    .line 129
    iput-object p4, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->proto_aggregator_bill_event_count_limit_override:Ljava/lang/Integer;

    .line 130
    iput-object p5, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_scan_infos_per_thread:Ljava/lang/Integer;

    .line 131
    iput-object p6, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_scan_info_threshold_size:Ljava/lang/Integer;

    .line 132
    iput-object p7, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_max_fanouts_per_request:Ljava/lang/Integer;

    .line 133
    iput-object p8, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_max_threads_per_request:Ljava/lang/Integer;

    .line 134
    iput-object p9, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->cache_custom_report:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 156
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 157
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    .line 158
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->request_timeout_override_seconds:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->request_timeout_override_seconds:Ljava/lang/Integer;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->local_aggregation_report_size_limit_override:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->local_aggregation_report_size_limit_override:Ljava/lang/Integer;

    .line 160
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->proto_aggregator_report_size_limit_override:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->proto_aggregator_report_size_limit_override:Ljava/lang/Integer;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->proto_aggregator_bill_event_count_limit_override:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->proto_aggregator_bill_event_count_limit_override:Ljava/lang/Integer;

    .line 162
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_scan_infos_per_thread:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_scan_infos_per_thread:Ljava/lang/Integer;

    .line 163
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_scan_info_threshold_size:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_scan_info_threshold_size:Ljava/lang/Integer;

    .line 164
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_max_fanouts_per_request:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_max_fanouts_per_request:Ljava/lang/Integer;

    .line 165
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_max_threads_per_request:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_max_threads_per_request:Ljava/lang/Integer;

    .line 166
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->cache_custom_report:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->cache_custom_report:Ljava/lang/Boolean;

    .line 167
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 172
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 174
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->request_timeout_override_seconds:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->local_aggregation_report_size_limit_override:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->proto_aggregator_report_size_limit_override:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->proto_aggregator_bill_event_count_limit_override:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_scan_infos_per_thread:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 180
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_scan_info_threshold_size:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 181
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_max_fanouts_per_request:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 182
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_max_threads_per_request:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 183
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->cache_custom_report:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 184
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;
    .locals 2

    .line 139
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;-><init>()V

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->request_timeout_override_seconds:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->request_timeout_override_seconds:Ljava/lang/Integer;

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->local_aggregation_report_size_limit_override:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->local_aggregation_report_size_limit_override:Ljava/lang/Integer;

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->proto_aggregator_report_size_limit_override:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->proto_aggregator_report_size_limit_override:Ljava/lang/Integer;

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->proto_aggregator_bill_event_count_limit_override:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->proto_aggregator_bill_event_count_limit_override:Ljava/lang/Integer;

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_scan_infos_per_thread:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->spanner_scan_infos_per_thread:Ljava/lang/Integer;

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_scan_info_threshold_size:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->spanner_scan_info_threshold_size:Ljava/lang/Integer;

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_max_fanouts_per_request:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->spanner_max_fanouts_per_request:Ljava/lang/Integer;

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_max_threads_per_request:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->spanner_max_threads_per_request:Ljava/lang/Integer;

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->cache_custom_report:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->cache_custom_report:Ljava/lang/Boolean;

    .line 149
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->request_timeout_override_seconds:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", request_timeout_override_seconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->request_timeout_override_seconds:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 193
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->local_aggregation_report_size_limit_override:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", local_aggregation_report_size_limit_override="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->local_aggregation_report_size_limit_override:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 194
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->proto_aggregator_report_size_limit_override:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", proto_aggregator_report_size_limit_override="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->proto_aggregator_report_size_limit_override:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 195
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->proto_aggregator_bill_event_count_limit_override:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", proto_aggregator_bill_event_count_limit_override="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->proto_aggregator_bill_event_count_limit_override:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 196
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_scan_infos_per_thread:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", spanner_scan_infos_per_thread="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_scan_infos_per_thread:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 197
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_scan_info_threshold_size:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    const-string v1, ", spanner_scan_info_threshold_size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_scan_info_threshold_size:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 198
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_max_fanouts_per_request:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    const-string v1, ", spanner_max_fanouts_per_request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_max_fanouts_per_request:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 199
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_max_threads_per_request:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    const-string v1, ", spanner_max_threads_per_request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->spanner_max_threads_per_request:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 200
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->cache_custom_report:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", cache_custom_report="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->cache_custom_report:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CustomReportRequestFlags{"

    .line 201
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
