.class public final Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;
.super Lcom/squareup/wire/Message;
.source "Aggregate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Details"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProtoAdapter_Details;,
        Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;,
        Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;,
        Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COVER_COUNT:Ljava/lang/Long;

.field public static final DEFAULT_DISCOUNT_COUNT:Ljava/lang/Long;

.field public static final DEFAULT_GIFT_CARDS_DISCOUNTED_COUNT:Ljava/lang/Long;

.field public static final DEFAULT_GIFT_CARD_LOAD_COUNT:Ljava/lang/Long;

.field public static final DEFAULT_ITEMS_DISCOUNTED_COUNT:Ljava/lang/String; = "0"

.field public static final DEFAULT_ITEMS_MODIFIED_COUNT:Ljava/lang/String; = "0"

.field public static final DEFAULT_ITEMS_TAXED_COUNT:Ljava/lang/String; = "0"

.field public static final DEFAULT_ITEM_COUNT:Ljava/lang/String; = "0"

.field public static final DEFAULT_ITEM_QUANTITY:Ljava/lang/String; = "0"

.field public static final DEFAULT_MODIFIER_COUNT:Ljava/lang/Long;

.field public static final DEFAULT_MODIFIER_QUANTITY:Ljava/lang/String; = "0"

.field public static final DEFAULT_REFUND_COUNT:Ljava/lang/Long;

.field public static final DEFAULT_SURCHARGE_COUNT:Ljava/lang/Long;

.field public static final DEFAULT_TENDER_COUNT:Ljava/lang/Long;

.field public static final DEFAULT_TRANSACTION_COUNT:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final australian_gross_sales_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x15
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final auto_gratuity_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x24
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final average_australian_gross_sales_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x16
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final average_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.Aggregate$Details$CostOfGoodsAmounts#ADAPTER"
        tag = 0x1e
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final average_gross_sales_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x12
    .end annotation
.end field

.field public final cover_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x25
    .end annotation
.end field

.field public final credit_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final discount_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x3
    .end annotation
.end field

.field public final discount_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final fifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.Aggregate$Details$CostOfGoodsAmounts#ADAPTER"
        tag = 0x1c
    .end annotation
.end field

.field public final gift_card_load_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x13
    .end annotation
.end field

.field public final gift_card_load_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x11
    .end annotation
.end field

.field public final gift_card_net_sales_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final gift_cards_discounted_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x22
    .end annotation
.end field

.field public final gift_cards_discounted_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x21
    .end annotation
.end field

.field public final gross_sales_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final item_count:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2c
    .end annotation
.end field

.field public final item_gross_sales_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x26
    .end annotation
.end field

.field public final item_net_sales_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x27
    .end annotation
.end field

.field public final item_quantity:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final items_discounted_count:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x18
    .end annotation
.end field

.field public final items_modified_count:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x19
    .end annotation
.end field

.field public final items_taxed_count:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x17
    .end annotation
.end field

.field public final lifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.Aggregate$Details$CostOfGoodsAmounts#ADAPTER"
        tag = 0x1d
    .end annotation
.end field

.field public final measured_quantity:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.MeasuredQuantity#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2f
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;",
            ">;"
        }
    .end annotation
.end field

.field public final modifier_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x5
    .end annotation
.end field

.field public final modifier_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final modifier_quantity:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1b
    .end annotation
.end field

.field public final net_sales_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final net_total_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final processing_fee_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.Aggregate$Details$ProcessingFeeAmounts#ADAPTER"
        tag = 0x1a
    .end annotation
.end field

.field public final processing_fee_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final product_sales_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x1f
    .end annotation
.end field

.field public final refund_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2a
    .end annotation
.end field

.field public final surcharge_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x28
    .end annotation
.end field

.field public final surcharge_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x23
    .end annotation
.end field

.field public final swedish_rounding_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final tax_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final taxable_item_net_sales_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2e
    .end annotation
.end field

.field public final taxable_net_sales_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2d
    .end annotation
.end field

.field public final tender_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x29
    .end annotation
.end field

.field public final tender_tax_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x20
    .end annotation
.end field

.field public final tip_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final total_collected_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final total_sales_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2b
    .end annotation
.end field

.field public final transaction_count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 150
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProtoAdapter_Details;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProtoAdapter_Details;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 154
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->DEFAULT_TRANSACTION_COUNT:Ljava/lang/Long;

    .line 156
    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->DEFAULT_DISCOUNT_COUNT:Ljava/lang/Long;

    .line 158
    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->DEFAULT_MODIFIER_COUNT:Ljava/lang/Long;

    .line 166
    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->DEFAULT_GIFT_CARD_LOAD_COUNT:Ljava/lang/Long;

    .line 168
    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->DEFAULT_GIFT_CARDS_DISCOUNTED_COUNT:Ljava/lang/Long;

    .line 176
    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->DEFAULT_COVER_COUNT:Ljava/lang/Long;

    .line 178
    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->DEFAULT_SURCHARGE_COUNT:Ljava/lang/Long;

    .line 180
    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->DEFAULT_TENDER_COUNT:Ljava/lang/Long;

    .line 182
    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->DEFAULT_REFUND_COUNT:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;Lokio/ByteString;)V
    .locals 1

    .line 618
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 619
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->transaction_count:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->transaction_count:Ljava/lang/Long;

    .line 620
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->discount_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_money:Lcom/squareup/protos/common/Money;

    .line 621
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->discount_count:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_count:Ljava/lang/Long;

    .line 622
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->modifier_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_money:Lcom/squareup/protos/common/Money;

    .line 623
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->modifier_count:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_count:Ljava/lang/Long;

    .line 624
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->modifier_quantity:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_quantity:Ljava/lang/String;

    .line 625
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gross_sales_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 626
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_gross_sales_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 627
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->product_sales_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->product_sales_money:Lcom/squareup/protos/common/Money;

    .line 628
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->processing_fee_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_money:Lcom/squareup/protos/common/Money;

    .line 629
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tip_money:Lcom/squareup/protos/common/Money;

    .line 630
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tax_money:Lcom/squareup/protos/common/Money;

    .line 631
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tender_tax_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tender_tax_money:Lcom/squareup/protos/common/Money;

    .line 632
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_quantity:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_quantity:Ljava/lang/String;

    .line 633
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_count:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_count:Ljava/lang/String;

    .line 634
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->net_sales_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_sales_money:Lcom/squareup/protos/common/Money;

    .line 635
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_net_sales_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_net_sales_money:Lcom/squareup/protos/common/Money;

    .line 636
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->taxable_net_sales_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->taxable_net_sales_money:Lcom/squareup/protos/common/Money;

    .line 637
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->taxable_item_net_sales_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->taxable_item_net_sales_money:Lcom/squareup/protos/common/Money;

    .line 638
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->total_collected_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_collected_money:Lcom/squareup/protos/common/Money;

    .line 639
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->net_total_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_total_money:Lcom/squareup/protos/common/Money;

    .line 640
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->credit_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->credit_money:Lcom/squareup/protos/common/Money;

    .line 641
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->swedish_rounding_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->swedish_rounding_money:Lcom/squareup/protos/common/Money;

    .line 642
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_card_net_sales_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_net_sales_money:Lcom/squareup/protos/common/Money;

    .line 643
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_card_load_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_load_money:Lcom/squareup/protos/common/Money;

    .line 644
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_gross_sales_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 645
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_card_load_count:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_load_count:Ljava/lang/Long;

    .line 646
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_cards_discounted_count:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_cards_discounted_count:Ljava/lang/Long;

    .line 647
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_cards_discounted_amount:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_cards_discounted_amount:Lcom/squareup/protos/common/Money;

    .line 648
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 649
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 650
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->items_taxed_count:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_taxed_count:Ljava/lang/String;

    .line 651
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->items_discounted_count:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_discounted_count:Ljava/lang/String;

    .line 652
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->items_modified_count:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_modified_count:Ljava/lang/String;

    .line 653
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->processing_fee_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;

    .line 654
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->fifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->fifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    .line 655
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->lifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->lifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    .line 656
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    .line 657
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->surcharge_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_money:Lcom/squareup/protos/common/Money;

    .line 658
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    .line 659
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->cover_count:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->cover_count:Ljava/lang/Long;

    .line 660
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->surcharge_count:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_count:Ljava/lang/Long;

    .line 661
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tender_count:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tender_count:Ljava/lang/Long;

    .line 662
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->refund_count:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->refund_count:Ljava/lang/Long;

    .line 663
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->total_sales_money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_sales_money:Lcom/squareup/protos/common/Money;

    .line 664
    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->measured_quantity:Ljava/util/List;

    const-string p2, "measured_quantity"

    invoke-static {p2, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->measured_quantity:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 723
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 724
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    .line 725
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->transaction_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->transaction_count:Ljava/lang/Long;

    .line 726
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_money:Lcom/squareup/protos/common/Money;

    .line 727
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_count:Ljava/lang/Long;

    .line 728
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_money:Lcom/squareup/protos/common/Money;

    .line 729
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_count:Ljava/lang/Long;

    .line 730
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_quantity:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_quantity:Ljava/lang/String;

    .line 731
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 732
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_gross_sales_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 733
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->product_sales_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->product_sales_money:Lcom/squareup/protos/common/Money;

    .line 734
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_money:Lcom/squareup/protos/common/Money;

    .line 735
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tip_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tip_money:Lcom/squareup/protos/common/Money;

    .line 736
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tax_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tax_money:Lcom/squareup/protos/common/Money;

    .line 737
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tender_tax_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tender_tax_money:Lcom/squareup/protos/common/Money;

    .line 738
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_quantity:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_quantity:Ljava/lang/String;

    .line 739
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_count:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_count:Ljava/lang/String;

    .line 740
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_sales_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_sales_money:Lcom/squareup/protos/common/Money;

    .line 741
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_net_sales_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_net_sales_money:Lcom/squareup/protos/common/Money;

    .line 742
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->taxable_net_sales_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->taxable_net_sales_money:Lcom/squareup/protos/common/Money;

    .line 743
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->taxable_item_net_sales_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->taxable_item_net_sales_money:Lcom/squareup/protos/common/Money;

    .line 744
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_collected_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_collected_money:Lcom/squareup/protos/common/Money;

    .line 745
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_total_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_total_money:Lcom/squareup/protos/common/Money;

    .line 746
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->credit_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->credit_money:Lcom/squareup/protos/common/Money;

    .line 747
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->swedish_rounding_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->swedish_rounding_money:Lcom/squareup/protos/common/Money;

    .line 748
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_net_sales_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_net_sales_money:Lcom/squareup/protos/common/Money;

    .line 749
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_load_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_load_money:Lcom/squareup/protos/common/Money;

    .line 750
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_gross_sales_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 751
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_load_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_load_count:Ljava/lang/Long;

    .line 752
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_cards_discounted_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_cards_discounted_count:Ljava/lang/Long;

    .line 753
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_cards_discounted_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_cards_discounted_amount:Lcom/squareup/protos/common/Money;

    .line 754
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 755
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 756
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_taxed_count:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_taxed_count:Ljava/lang/String;

    .line 757
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_discounted_count:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_discounted_count:Ljava/lang/String;

    .line 758
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_modified_count:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_modified_count:Ljava/lang/String;

    .line 759
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;

    .line 760
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->fifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->fifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    .line 761
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->lifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->lifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    .line 762
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    .line 763
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_money:Lcom/squareup/protos/common/Money;

    .line 764
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    .line 765
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->cover_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->cover_count:Ljava/lang/Long;

    .line 766
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_count:Ljava/lang/Long;

    .line 767
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tender_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tender_count:Ljava/lang/Long;

    .line 768
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->refund_count:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->refund_count:Ljava/lang/Long;

    .line 769
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_sales_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_sales_money:Lcom/squareup/protos/common/Money;

    .line 770
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->measured_quantity:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->measured_quantity:Ljava/util/List;

    .line 771
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 776
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2d

    .line 778
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 779
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->transaction_count:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 780
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 781
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_count:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 782
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 783
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_count:Ljava/lang/Long;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 784
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_quantity:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 785
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 786
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_gross_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 787
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->product_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 788
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 789
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tip_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 790
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tax_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 791
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tender_tax_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 792
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_quantity:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 793
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_count:Ljava/lang/String;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 794
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 795
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_net_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 796
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->taxable_net_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 797
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->taxable_item_net_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 798
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_collected_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 799
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_total_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 800
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->credit_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 801
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->swedish_rounding_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 802
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_net_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 803
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_load_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 804
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_gross_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 805
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_load_count:Ljava/lang/Long;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1a

    :cond_1a
    const/4 v1, 0x0

    :goto_1a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 806
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_cards_discounted_count:Ljava/lang/Long;

    if-eqz v1, :cond_1b

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1b

    :cond_1b
    const/4 v1, 0x0

    :goto_1b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 807
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_cards_discounted_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1c

    :cond_1c
    const/4 v1, 0x0

    :goto_1c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 808
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1d

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1d

    :cond_1d
    const/4 v1, 0x0

    :goto_1d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 809
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1e

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1e

    :cond_1e
    const/4 v1, 0x0

    :goto_1e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 810
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_taxed_count:Ljava/lang/String;

    if-eqz v1, :cond_1f

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1f

    :cond_1f
    const/4 v1, 0x0

    :goto_1f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 811
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_discounted_count:Ljava/lang/String;

    if-eqz v1, :cond_20

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_20

    :cond_20
    const/4 v1, 0x0

    :goto_20
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 812
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_modified_count:Ljava/lang/String;

    if-eqz v1, :cond_21

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_21

    :cond_21
    const/4 v1, 0x0

    :goto_21
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 813
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;

    if-eqz v1, :cond_22

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;->hashCode()I

    move-result v1

    goto :goto_22

    :cond_22
    const/4 v1, 0x0

    :goto_22
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 814
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->fifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    if-eqz v1, :cond_23

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->hashCode()I

    move-result v1

    goto :goto_23

    :cond_23
    const/4 v1, 0x0

    :goto_23
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 815
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->lifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    if-eqz v1, :cond_24

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->hashCode()I

    move-result v1

    goto :goto_24

    :cond_24
    const/4 v1, 0x0

    :goto_24
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 816
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    if-eqz v1, :cond_25

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->hashCode()I

    move-result v1

    goto :goto_25

    :cond_25
    const/4 v1, 0x0

    :goto_25
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 817
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_26

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_26

    :cond_26
    const/4 v1, 0x0

    :goto_26
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 818
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_27

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_27

    :cond_27
    const/4 v1, 0x0

    :goto_27
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 819
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->cover_count:Ljava/lang/Long;

    if-eqz v1, :cond_28

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_28

    :cond_28
    const/4 v1, 0x0

    :goto_28
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 820
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_count:Ljava/lang/Long;

    if-eqz v1, :cond_29

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_29

    :cond_29
    const/4 v1, 0x0

    :goto_29
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 821
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tender_count:Ljava/lang/Long;

    if-eqz v1, :cond_2a

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2a

    :cond_2a
    const/4 v1, 0x0

    :goto_2a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 822
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->refund_count:Ljava/lang/Long;

    if-eqz v1, :cond_2b

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2b

    :cond_2b
    const/4 v1, 0x0

    :goto_2b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 823
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2c

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_2c
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 824
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->measured_quantity:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 825
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2d
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;
    .locals 2

    .line 669
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;-><init>()V

    .line 670
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->transaction_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->transaction_count:Ljava/lang/Long;

    .line 671
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->discount_money:Lcom/squareup/protos/common/Money;

    .line 672
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->discount_count:Ljava/lang/Long;

    .line 673
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->modifier_money:Lcom/squareup/protos/common/Money;

    .line 674
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->modifier_count:Ljava/lang/Long;

    .line 675
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_quantity:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->modifier_quantity:Ljava/lang/String;

    .line 676
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 677
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_gross_sales_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 678
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->product_sales_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->product_sales_money:Lcom/squareup/protos/common/Money;

    .line 679
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->processing_fee_money:Lcom/squareup/protos/common/Money;

    .line 680
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tip_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    .line 681
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tax_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tax_money:Lcom/squareup/protos/common/Money;

    .line 682
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tender_tax_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tender_tax_money:Lcom/squareup/protos/common/Money;

    .line 683
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_quantity:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_quantity:Ljava/lang/String;

    .line 684
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_count:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_count:Ljava/lang/String;

    .line 685
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_sales_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->net_sales_money:Lcom/squareup/protos/common/Money;

    .line 686
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_net_sales_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->item_net_sales_money:Lcom/squareup/protos/common/Money;

    .line 687
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->taxable_net_sales_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->taxable_net_sales_money:Lcom/squareup/protos/common/Money;

    .line 688
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->taxable_item_net_sales_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->taxable_item_net_sales_money:Lcom/squareup/protos/common/Money;

    .line 689
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_collected_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->total_collected_money:Lcom/squareup/protos/common/Money;

    .line 690
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_total_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->net_total_money:Lcom/squareup/protos/common/Money;

    .line 691
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->credit_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->credit_money:Lcom/squareup/protos/common/Money;

    .line 692
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->swedish_rounding_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->swedish_rounding_money:Lcom/squareup/protos/common/Money;

    .line 693
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_net_sales_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_card_net_sales_money:Lcom/squareup/protos/common/Money;

    .line 694
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_load_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_card_load_money:Lcom/squareup/protos/common/Money;

    .line 695
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_gross_sales_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 696
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_load_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_card_load_count:Ljava/lang/Long;

    .line 697
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_cards_discounted_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_cards_discounted_count:Ljava/lang/Long;

    .line 698
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_cards_discounted_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->gift_cards_discounted_amount:Lcom/squareup/protos/common/Money;

    .line 699
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 700
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    .line 701
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_taxed_count:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->items_taxed_count:Ljava/lang/String;

    .line 702
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_discounted_count:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->items_discounted_count:Ljava/lang/String;

    .line 703
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_modified_count:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->items_modified_count:Ljava/lang/String;

    .line 704
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->processing_fee_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;

    .line 705
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->fifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->fifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    .line 706
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->lifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->lifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    .line 707
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->average_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    .line 708
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->surcharge_money:Lcom/squareup/protos/common/Money;

    .line 709
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    .line 710
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->cover_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->cover_count:Ljava/lang/Long;

    .line 711
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->surcharge_count:Ljava/lang/Long;

    .line 712
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tender_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->tender_count:Ljava/lang/Long;

    .line 713
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->refund_count:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->refund_count:Ljava/lang/Long;

    .line 714
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_sales_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->total_sales_money:Lcom/squareup/protos/common/Money;

    .line 715
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->measured_quantity:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->measured_quantity:Ljava/util/List;

    .line 716
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 149
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 832
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 833
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->transaction_count:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", transaction_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->transaction_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 834
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", discount_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 835
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_count:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", discount_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 836
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", modifier_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 837
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_count:Ljava/lang/Long;

    if-eqz v1, :cond_4

    const-string v1, ", modifier_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 838
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_quantity:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", modifier_quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->modifier_quantity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 839
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", gross_sales_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 840
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_gross_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_7

    const-string v1, ", item_gross_sales_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 841
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->product_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_8

    const-string v1, ", product_sales_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->product_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 842
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_9

    const-string v1, ", processing_fee_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 843
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tip_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_a

    const-string v1, ", tip_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 844
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tax_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_b

    const-string v1, ", tax_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tax_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 845
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tender_tax_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_c

    const-string v1, ", tender_tax_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tender_tax_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 846
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_quantity:Ljava/lang/String;

    if-eqz v1, :cond_d

    const-string v1, ", item_quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_quantity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 847
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_count:Ljava/lang/String;

    if-eqz v1, :cond_e

    const-string v1, ", item_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_count:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 848
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_f

    const-string v1, ", net_sales_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 849
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_net_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_10

    const-string v1, ", item_net_sales_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->item_net_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 850
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->taxable_net_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_11

    const-string v1, ", taxable_net_sales_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->taxable_net_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 851
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->taxable_item_net_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_12

    const-string v1, ", taxable_item_net_sales_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->taxable_item_net_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 852
    :cond_12
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_collected_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_13

    const-string v1, ", total_collected_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_collected_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 853
    :cond_13
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_total_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_14

    const-string v1, ", net_total_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_total_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 854
    :cond_14
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->credit_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_15

    const-string v1, ", credit_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->credit_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 855
    :cond_15
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->swedish_rounding_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_16

    const-string v1, ", swedish_rounding_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->swedish_rounding_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 856
    :cond_16
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_net_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_17

    const-string v1, ", gift_card_net_sales_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_net_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 857
    :cond_17
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_load_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_18

    const-string v1, ", gift_card_load_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_load_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 858
    :cond_18
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_gross_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_19

    const-string v1, ", average_gross_sales_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 859
    :cond_19
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_load_count:Ljava/lang/Long;

    if-eqz v1, :cond_1a

    const-string v1, ", gift_card_load_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_load_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 860
    :cond_1a
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_cards_discounted_count:Ljava/lang/Long;

    if-eqz v1, :cond_1b

    const-string v1, ", gift_cards_discounted_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_cards_discounted_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 861
    :cond_1b
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_cards_discounted_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1c

    const-string v1, ", gift_cards_discounted_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_cards_discounted_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 862
    :cond_1c
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1d

    const-string v1, ", australian_gross_sales_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 863
    :cond_1d
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1e

    const-string v1, ", average_australian_gross_sales_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_australian_gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 864
    :cond_1e
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_taxed_count:Ljava/lang/String;

    if-eqz v1, :cond_1f

    const-string v1, ", items_taxed_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_taxed_count:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 865
    :cond_1f
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_discounted_count:Ljava/lang/String;

    if-eqz v1, :cond_20

    const-string v1, ", items_discounted_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_discounted_count:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 866
    :cond_20
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_modified_count:Ljava/lang/String;

    if-eqz v1, :cond_21

    const-string v1, ", items_modified_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->items_modified_count:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 867
    :cond_21
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;

    if-eqz v1, :cond_22

    const-string v1, ", processing_fee_amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$ProcessingFeeAmounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 868
    :cond_22
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->fifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    if-eqz v1, :cond_23

    const-string v1, ", fifo_cost_of_goods_amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->fifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 869
    :cond_23
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->lifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    if-eqz v1, :cond_24

    const-string v1, ", lifo_cost_of_goods_amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->lifo_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 870
    :cond_24
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    if-eqz v1, :cond_25

    const-string v1, ", average_cost_of_goods_amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->average_cost_of_goods_amounts:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 871
    :cond_25
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_26

    const-string v1, ", surcharge_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 872
    :cond_26
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_27

    const-string v1, ", auto_gratuity_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 873
    :cond_27
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->cover_count:Ljava/lang/Long;

    if-eqz v1, :cond_28

    const-string v1, ", cover_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->cover_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 874
    :cond_28
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_count:Ljava/lang/Long;

    if-eqz v1, :cond_29

    const-string v1, ", surcharge_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 875
    :cond_29
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tender_count:Ljava/lang/Long;

    if-eqz v1, :cond_2a

    const-string v1, ", tender_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tender_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 876
    :cond_2a
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->refund_count:Ljava/lang/Long;

    if-eqz v1, :cond_2b

    const-string v1, ", refund_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->refund_count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 877
    :cond_2b
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_sales_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2c

    const-string v1, ", total_sales_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_sales_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 878
    :cond_2c
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->measured_quantity:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2d

    const-string v1, ", measured_quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->measured_quantity:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2d
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Details{"

    .line 879
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
