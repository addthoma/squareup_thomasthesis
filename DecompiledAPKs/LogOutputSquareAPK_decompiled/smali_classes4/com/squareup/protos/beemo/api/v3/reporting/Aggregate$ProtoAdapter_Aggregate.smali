.class final Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$ProtoAdapter_Aggregate;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Aggregate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Aggregate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2654
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2675
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;-><init>()V

    .line 2676
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2677
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 2683
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2681
    :cond_0
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->net(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;

    goto :goto_0

    .line 2680
    :cond_1
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->refunds(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;

    goto :goto_0

    .line 2679
    :cond_2
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->sales(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;

    goto :goto_0

    .line 2687
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2688
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2652
    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$ProtoAdapter_Aggregate;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2667
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2668
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2669
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2670
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2652
    check-cast p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$ProtoAdapter_Aggregate;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)I
    .locals 4

    .line 2659
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    const/4 v3, 0x2

    .line 2660
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    const/4 v3, 0x3

    .line 2661
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2662
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2652
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$ProtoAdapter_Aggregate;->encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;
    .locals 2

    .line 2693
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;

    move-result-object p1

    .line 2694
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    .line 2695
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    .line 2696
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    .line 2697
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2698
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2652
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$ProtoAdapter_Aggregate;->redact(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    move-result-object p1

    return-object p1
.end method
