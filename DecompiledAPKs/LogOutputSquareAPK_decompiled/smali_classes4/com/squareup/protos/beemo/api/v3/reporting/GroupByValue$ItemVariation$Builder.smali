.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

.field public name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

.field public sku:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2385
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;
    .locals 7

    .line 2418
    new-instance v6, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->sku:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iget-object v4, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;-><init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/protos/beemo/translation_types/TranslationType;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2376
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    move-result-object v0

    return-object v0
.end method

.method public default_measurement_unit(Lcom/squareup/protos/beemo/translation_types/TranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;
    .locals 0

    .line 2411
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/4 p1, 0x0

    .line 2412
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    return-object p0
.end method

.method public defined_measurement_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;
    .locals 0

    .line 2402
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    const/4 p1, 0x0

    .line 2403
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0
.end method

.method public name_or_translation_type(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;
    .locals 0

    .line 2392
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    return-object p0
.end method

.method public sku(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;
    .locals 0

    .line 2397
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation$Builder;->sku:Ljava/lang/String;

    return-object p0
.end method
