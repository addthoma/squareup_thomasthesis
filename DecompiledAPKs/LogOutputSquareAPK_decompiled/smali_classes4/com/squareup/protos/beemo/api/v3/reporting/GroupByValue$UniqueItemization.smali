.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;
.super Lcom/squareup/wire/Message;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UniqueItemization"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$ProtoAdapter_UniqueItemization;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final discount_details:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$DiscountDetails#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;",
            ">;"
        }
    .end annotation
.end field

.field public final item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$Item#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$ItemCategory#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$ItemVariation#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final modifier_details:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$ModifierDetails#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;",
            ">;"
        }
    .end annotation
.end field

.field public final tax_details:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$TaxDetails#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;",
            ">;"
        }
    .end annotation
.end field

.field public final unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$UnitPrice#ADAPTER"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5759
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$ProtoAdapter_UniqueItemization;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$ProtoAdapter_UniqueItemization;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;",
            ">;)V"
        }
    .end annotation

    .line 5811
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 5817
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 5818
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    .line 5819
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    .line 5820
    iput-object p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    .line 5821
    iput-object p4, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    const-string p1, "modifier_details"

    .line 5822
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->modifier_details:Ljava/util/List;

    const-string p1, "tax_details"

    .line 5823
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->tax_details:Ljava/util/List;

    const-string p1, "discount_details"

    .line 5824
    invoke-static {p1, p7}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->discount_details:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 5844
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 5845
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    .line 5846
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    .line 5847
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    .line 5848
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    .line 5849
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    .line 5850
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->modifier_details:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->modifier_details:Ljava/util/List;

    .line 5851
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->tax_details:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->tax_details:Ljava/util/List;

    .line 5852
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->discount_details:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->discount_details:Ljava/util/List;

    .line 5853
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 5858
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 5860
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 5861
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5862
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5863
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5864
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 5865
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->modifier_details:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5866
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->tax_details:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5867
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->discount_details:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 5868
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;
    .locals 2

    .line 5829
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;-><init>()V

    .line 5830
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    .line 5831
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    .line 5832
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    .line 5833
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    .line 5834
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->modifier_details:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->modifier_details:Ljava/util/List;

    .line 5835
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->tax_details:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->tax_details:Ljava/util/List;

    .line 5836
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->discount_details:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->discount_details:Ljava/util/List;

    .line 5837
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 5758
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 5875
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 5876
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    if-eqz v1, :cond_0

    const-string v1, ", item_category="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5877
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    if-eqz v1, :cond_1

    const-string v1, ", item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5878
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    if-eqz v1, :cond_2

    const-string v1, ", item_variation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5879
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    if-eqz v1, :cond_3

    const-string v1, ", unit_price="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5880
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->modifier_details:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", modifier_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->modifier_details:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5881
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->tax_details:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", tax_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->tax_details:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5882
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->discount_details:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ", discount_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->discount_details:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "UniqueItemization{"

    .line 5883
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
