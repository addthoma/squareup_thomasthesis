.class public final Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;
.super Lcom/squareup/wire/Message;
.source "CustomReportRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$ProtoAdapter_CustomReportRequest;,
        Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final aggregate_options:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.CustomReportAggregateOptions#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final filter:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.Filter#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Filter;",
            ">;"
        }
    .end annotation
.end field

.field public final group_by_type:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByType#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final grouping_type:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupingType#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xa
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;",
            ">;"
        }
    .end annotation
.end field

.field public final request_flags:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.CustomReportRequestFlags#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final request_params:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v2.reporting.RequestParams#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$ProtoAdapter_CustomReportRequest;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$ProtoAdapter_CustomReportRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Filter;",
            ">;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;",
            ">;)V"
        }
    .end annotation

    .line 84
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;-><init>(Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Filter;",
            ">;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 91
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 92
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->request_params:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    const-string p1, "group_by_type"

    .line 93
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->group_by_type:Ljava/util/List;

    const-string p1, "filter"

    .line 94
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->filter:Ljava/util/List;

    .line 95
    iput-object p4, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->request_flags:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    .line 96
    iput-object p5, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->aggregate_options:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    const-string p1, "grouping_type"

    .line 97
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->grouping_type:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 116
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 117
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->request_params:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->request_params:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    .line 119
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->group_by_type:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->group_by_type:Ljava/util/List;

    .line 120
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->filter:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->filter:Ljava/util/List;

    .line 121
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->request_flags:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->request_flags:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    .line 122
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->aggregate_options:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->aggregate_options:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    .line 123
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->grouping_type:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->grouping_type:Ljava/util/List;

    .line 124
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 129
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 131
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->request_params:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->group_by_type:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->filter:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->request_flags:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->aggregate_options:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->grouping_type:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 138
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;
    .locals 2

    .line 102
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;-><init>()V

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->request_params:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->request_params:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->group_by_type:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->group_by_type:Ljava/util/List;

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->filter:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->filter:Ljava/util/List;

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->request_flags:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->request_flags:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->aggregate_options:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->aggregate_options:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->grouping_type:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->grouping_type:Ljava/util/List;

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->request_params:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    if-eqz v1, :cond_0

    const-string v1, ", request_params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->request_params:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 147
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->group_by_type:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", group_by_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->group_by_type:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 148
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->filter:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", filter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->filter:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 149
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->request_flags:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    if-eqz v1, :cond_3

    const-string v1, ", request_flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->request_flags:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 150
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->aggregate_options:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    if-eqz v1, :cond_4

    const-string v1, ", aggregate_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->aggregate_options:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 151
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->grouping_type:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", grouping_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->grouping_type:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CustomReportRequest{"

    .line 152
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
