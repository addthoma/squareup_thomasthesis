.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public fee_plan_type:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7442
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;
    .locals 3

    .line 7452
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails$Builder;->fee_plan_type:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;-><init>(Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 7439
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    move-result-object v0

    return-object v0
.end method

.method public fee_plan_type(Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails$Builder;
    .locals 0

    .line 7446
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails$Builder;->fee_plan_type:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    return-object p0
.end method
