.class public final enum Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;
.super Ljava/lang/Enum;
.source "GroupByValue.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InclusionType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType$ProtoAdapter_InclusionType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ADDITIVE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

.field public static final enum INCLUSIVE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 5417
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    const/4 v1, 0x0

    const-string v2, "ADDITIVE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;->ADDITIVE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    .line 5419
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    const/4 v2, 0x1

    const-string v3, "INCLUSIVE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;->INCLUSIVE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    .line 5416
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;->ADDITIVE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;->INCLUSIVE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    .line 5421
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType$ProtoAdapter_InclusionType;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType$ProtoAdapter_InclusionType;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 5425
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 5426
    iput p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 5435
    :cond_0
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;->INCLUSIVE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    return-object p0

    .line 5434
    :cond_1
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;->ADDITIVE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;
    .locals 1

    .line 5416
    const-class v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;
    .locals 1

    .line 5416
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    invoke-virtual {v0}, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 5442
    iget v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails$InclusionType;->value:I

    return v0
.end method
