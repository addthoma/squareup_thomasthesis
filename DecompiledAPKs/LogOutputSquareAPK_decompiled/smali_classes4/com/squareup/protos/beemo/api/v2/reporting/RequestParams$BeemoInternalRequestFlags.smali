.class public final Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;
.super Lcom/squareup/wire/Message;
.source "RequestParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BeemoInternalRequestFlags"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$ProtoAdapter_BeemoInternalRequestFlags;,
        Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;",
        "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TRANSACTIONS_TIME_RANGE_FROM_BEWFO_START_TIME_MS:Ljava/lang/Long;

.field public static final DEFAULT_USE_LEDGER_SUMMARY_RESULTS_IN_SETTLEMENT_REPORT_RESPONSE:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_NEW_LEDGER_SUMMARY_ENDPOINT:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final transactions_time_range_from_bewfo_start_time_ms:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x1
    .end annotation
.end field

.field public final use_ledger_summary_results_in_settlement_report_response:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final use_new_ledger_summary_endpoint:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 496
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$ProtoAdapter_BeemoInternalRequestFlags;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$ProtoAdapter_BeemoInternalRequestFlags;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 500
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->DEFAULT_TRANSACTIONS_TIME_RANGE_FROM_BEWFO_START_TIME_MS:Ljava/lang/Long;

    const/4 v0, 0x0

    .line 502
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->DEFAULT_USE_NEW_LEDGER_SUMMARY_ENDPOINT:Ljava/lang/Boolean;

    .line 504
    sput-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->DEFAULT_USE_LEDGER_SUMMARY_RESULTS_IN_SETTLEMENT_REPORT_RESPONSE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 1

    .line 527
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;-><init>(Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 534
    sget-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 535
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->transactions_time_range_from_bewfo_start_time_ms:Ljava/lang/Long;

    .line 536
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->use_new_ledger_summary_endpoint:Ljava/lang/Boolean;

    .line 537
    iput-object p3, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->use_ledger_summary_results_in_settlement_report_response:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 553
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 554
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    .line 555
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->transactions_time_range_from_bewfo_start_time_ms:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->transactions_time_range_from_bewfo_start_time_ms:Ljava/lang/Long;

    .line 556
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->use_new_ledger_summary_endpoint:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->use_new_ledger_summary_endpoint:Ljava/lang/Boolean;

    .line 557
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->use_ledger_summary_results_in_settlement_report_response:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->use_ledger_summary_results_in_settlement_report_response:Ljava/lang/Boolean;

    .line 558
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 563
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 565
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 566
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->transactions_time_range_from_bewfo_start_time_ms:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 567
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->use_new_ledger_summary_endpoint:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 568
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->use_ledger_summary_results_in_settlement_report_response:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 569
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;
    .locals 2

    .line 542
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;-><init>()V

    .line 543
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->transactions_time_range_from_bewfo_start_time_ms:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;->transactions_time_range_from_bewfo_start_time_ms:Ljava/lang/Long;

    .line 544
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->use_new_ledger_summary_endpoint:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;->use_new_ledger_summary_endpoint:Ljava/lang/Boolean;

    .line 545
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->use_ledger_summary_results_in_settlement_report_response:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;->use_ledger_summary_results_in_settlement_report_response:Ljava/lang/Boolean;

    .line 546
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 495
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->newBuilder()Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 576
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 577
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->transactions_time_range_from_bewfo_start_time_ms:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", transactions_time_range_from_bewfo_start_time_ms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->transactions_time_range_from_bewfo_start_time_ms:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 578
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->use_new_ledger_summary_endpoint:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", use_new_ledger_summary_endpoint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->use_new_ledger_summary_endpoint:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 579
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->use_ledger_summary_results_in_settlement_report_response:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", use_ledger_summary_results_in_settlement_report_response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->use_ledger_summary_results_in_settlement_report_response:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BeemoInternalRequestFlags{"

    .line 580
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
