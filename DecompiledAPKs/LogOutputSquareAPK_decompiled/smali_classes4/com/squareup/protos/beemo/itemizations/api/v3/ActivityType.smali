.class public final enum Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;
.super Ljava/lang/Enum;
.source "ActivityType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType$ProtoAdapter_ActivityType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ATO_FRAUD:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

.field public static final enum BOOKED:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

.field public static final enum CANCELED:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

.field public static final enum FAILED:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

.field public static final enum OPEN:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

.field public static final enum PENDING:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

.field public static final enum VOIDED:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 18
    new-instance v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "BOOKED"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->BOOKED:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    .line 24
    new-instance v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    const/4 v3, 0x2

    const-string v4, "PENDING"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->PENDING:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    .line 31
    new-instance v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    const/4 v4, 0x3

    const-string v5, "FAILED"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->FAILED:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    .line 37
    new-instance v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    const/4 v5, 0x4

    const-string v6, "CANCELED"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->CANCELED:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    .line 42
    new-instance v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    const/4 v6, 0x5

    const-string v7, "VOIDED"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->VOIDED:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    .line 48
    new-instance v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    const/4 v7, 0x6

    const-string v8, "OPEN"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->OPEN:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    .line 54
    new-instance v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    const/4 v8, 0x7

    const-string v9, "ATO_FRAUD"

    invoke-direct {v0, v9, v7, v8}, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->ATO_FRAUD:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    new-array v0, v8, [Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    .line 13
    sget-object v8, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->BOOKED:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->PENDING:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->FAILED:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->CANCELED:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->VOIDED:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->OPEN:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->ATO_FRAUD:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->$VALUES:[Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    .line 56
    new-instance v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType$ProtoAdapter_ActivityType;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType$ProtoAdapter_ActivityType;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 61
    iput p3, p0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 75
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->ATO_FRAUD:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    return-object p0

    .line 74
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->OPEN:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    return-object p0

    .line 73
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->VOIDED:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    return-object p0

    .line 72
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->CANCELED:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    return-object p0

    .line 71
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->FAILED:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    return-object p0

    .line 70
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->PENDING:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    return-object p0

    .line 69
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->BOOKED:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->$VALUES:[Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    invoke-virtual {v0}, [Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 82
    iget v0, p0, Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;->value:I

    return v0
.end method
