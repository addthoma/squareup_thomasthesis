.class public final Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;
.super Lcom/squareup/wire/Message;
.source "NameOrTranslationType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$ProtoAdapter_NameOrTranslationType;,
        Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;",
        "Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_TRANSLATION_TYPE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field private static final serialVersionUID:J


# instance fields
.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final translation_type:Lcom/squareup/protos/beemo/translation_types/TranslationType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.translation_types.TranslationType#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$ProtoAdapter_NameOrTranslationType;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$ProtoAdapter_NameOrTranslationType;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 29
    sget-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CUSTOM_AMOUNT_ITEM:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    sput-object v0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->DEFAULT_TRANSLATION_TYPE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/beemo/translation_types/TranslationType;)V
    .locals 1

    .line 47
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;-><init>(Ljava/lang/String;Lcom/squareup/protos/beemo/translation_types/TranslationType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/beemo/translation_types/TranslationType;Lokio/ByteString;)V
    .locals 1

    .line 52
    sget-object v0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 53
    iput-object p1, p0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->name:Ljava/lang/String;

    .line 54
    iput-object p2, p0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->translation_type:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 69
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 70
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    .line 71
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->name:Ljava/lang/String;

    .line 72
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->translation_type:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    iget-object p1, p1, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->translation_type:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 73
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 78
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 80
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->translation_type:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 83
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;
    .locals 2

    .line 59
    new-instance v0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;-><init>()V

    .line 60
    iget-object v1, p0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;->name:Ljava/lang/String;

    .line 61
    iget-object v1, p0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->translation_type:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;->translation_type:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 62
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->newBuilder()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->translation_type:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    if-eqz v1, :cond_1

    const-string v1, ", translation_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->translation_type:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "NameOrTranslationType{"

    .line 93
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
