.class Lcom/epson/eposdevice/Device$CreateDeviceCallbackAdapter;
.super Ljava/lang/Object;
.source "Device.java"

# interfaces
.implements Lcom/epson/eposdevice/NativeDevice$NativeCreateDeviceCallbackAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/eposdevice/Device;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CreateDeviceCallbackAdapter"
.end annotation


# instance fields
.field private mListener:Lcom/epson/eposdevice/CreateDeviceListener;

.field final synthetic this$0:Lcom/epson/eposdevice/Device;


# direct methods
.method public constructor <init>(Lcom/epson/eposdevice/Device;Lcom/epson/eposdevice/CreateDeviceListener;)V
    .locals 0

    .line 400
    iput-object p1, p0, Lcom/epson/eposdevice/Device$CreateDeviceCallbackAdapter;->this$0:Lcom/epson/eposdevice/Device;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 398
    iput-object p1, p0, Lcom/epson/eposdevice/Device$CreateDeviceCallbackAdapter;->mListener:Lcom/epson/eposdevice/CreateDeviceListener;

    .line 401
    iput-object p2, p0, Lcom/epson/eposdevice/Device$CreateDeviceCallbackAdapter;->mListener:Lcom/epson/eposdevice/CreateDeviceListener;

    return-void
.end method


# virtual methods
.method public nativeOnCreateDevice(Ljava/lang/String;Ljava/lang/String;IJI)V
    .locals 10

    move-object v0, p0

    move v4, p3

    move-wide v1, p4

    const/4 v3, 0x4

    const/4 v5, 0x3

    const/4 v6, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-nez p6, :cond_5

    if-eqz v4, :cond_4

    if-eq v4, v7, :cond_3

    if-eq v4, v6, :cond_2

    if-eq v4, v5, :cond_1

    if-eq v4, v3, :cond_0

    goto :goto_0

    .line 422
    :cond_0
    new-instance v8, Lcom/epson/eposdevice/DeviceInnerImplement$SimpleSerialInner;

    iget-object v9, v0, Lcom/epson/eposdevice/Device$CreateDeviceCallbackAdapter;->this$0:Lcom/epson/eposdevice/Device;

    invoke-direct {v8, v9, p4, p5}, Lcom/epson/eposdevice/DeviceInnerImplement$SimpleSerialInner;-><init>(Lcom/epson/eposdevice/DeviceInnerImplement;J)V

    goto :goto_0

    .line 419
    :cond_1
    new-instance v8, Lcom/epson/eposdevice/DeviceInnerImplement$ScannerInner;

    iget-object v9, v0, Lcom/epson/eposdevice/Device$CreateDeviceCallbackAdapter;->this$0:Lcom/epson/eposdevice/Device;

    invoke-direct {v8, v9, p4, p5}, Lcom/epson/eposdevice/DeviceInnerImplement$ScannerInner;-><init>(Lcom/epson/eposdevice/DeviceInnerImplement;J)V

    goto :goto_0

    .line 416
    :cond_2
    new-instance v8, Lcom/epson/eposdevice/DeviceInnerImplement$KeyboardInner;

    iget-object v9, v0, Lcom/epson/eposdevice/Device$CreateDeviceCallbackAdapter;->this$0:Lcom/epson/eposdevice/Device;

    invoke-direct {v8, v9, p4, p5}, Lcom/epson/eposdevice/DeviceInnerImplement$KeyboardInner;-><init>(Lcom/epson/eposdevice/DeviceInnerImplement;J)V

    goto :goto_0

    .line 413
    :cond_3
    new-instance v8, Lcom/epson/eposdevice/DeviceInnerImplement$DisplayInner;

    iget-object v9, v0, Lcom/epson/eposdevice/Device$CreateDeviceCallbackAdapter;->this$0:Lcom/epson/eposdevice/Device;

    invoke-direct {v8, v9, p4, p5}, Lcom/epson/eposdevice/DeviceInnerImplement$DisplayInner;-><init>(Lcom/epson/eposdevice/DeviceInnerImplement;J)V

    goto :goto_0

    .line 410
    :cond_4
    new-instance v8, Lcom/epson/eposdevice/DeviceInnerImplement$PrinterInner;

    iget-object v9, v0, Lcom/epson/eposdevice/Device$CreateDeviceCallbackAdapter;->this$0:Lcom/epson/eposdevice/Device;

    invoke-direct {v8, v9, p4, p5}, Lcom/epson/eposdevice/DeviceInnerImplement$PrinterInner;-><init>(Lcom/epson/eposdevice/DeviceInnerImplement;J)V

    .line 425
    :goto_0
    iget-object v1, v0, Lcom/epson/eposdevice/Device$CreateDeviceCallbackAdapter;->this$0:Lcom/epson/eposdevice/Device;

    invoke-static {v1}, Lcom/epson/eposdevice/Device;->access$000(Lcom/epson/eposdevice/Device;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 428
    :cond_5
    iget-object v1, v0, Lcom/epson/eposdevice/Device$CreateDeviceCallbackAdapter;->mListener:Lcom/epson/eposdevice/CreateDeviceListener;

    if-eqz v1, :cond_6

    .line 429
    iget-object v1, v0, Lcom/epson/eposdevice/Device$CreateDeviceCallbackAdapter;->this$0:Lcom/epson/eposdevice/Device;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v2, v9

    aput-object p2, v2, v7

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v2, v6

    aput-object v8, v2, v5

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    const-string v3, "OnCreateDevice"

    invoke-virtual {v1, v3, v2}, Lcom/epson/eposdevice/Device;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 430
    iget-object v1, v0, Lcom/epson/eposdevice/Device$CreateDeviceCallbackAdapter;->mListener:Lcom/epson/eposdevice/CreateDeviceListener;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, v8

    move/from16 v6, p6

    invoke-interface/range {v1 .. v6}, Lcom/epson/eposdevice/CreateDeviceListener;->onCreateDevice(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;I)V

    :cond_6
    return-void
.end method
