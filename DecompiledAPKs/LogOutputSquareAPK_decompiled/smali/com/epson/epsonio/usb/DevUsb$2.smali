.class final Lcom/epson/epsonio/usb/DevUsb$2;
.super Ljava/lang/Object;
.source "DevUsb.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/epson/epsonio/usb/DevUsb;->CreateDiscoverThread()Ljava/lang/Thread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .line 247
    :goto_0
    invoke-static {}, Lcom/epson/epsonio/usb/DevUsb;->access$200()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 256
    :try_start_0
    invoke-static {}, Lcom/epson/epsonio/usb/DevUsb;->access$400()Landroid/hardware/usb/UsbManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/epsonio/usb/DevUsb;->access$302(Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 257
    invoke-static {}, Lcom/epson/epsonio/usb/DevUsb;->access$300()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 263
    :cond_0
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 264
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/usb/UsbDevice;

    const/16 v2, 0x4b8

    .line 266
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v3

    if-ne v2, v3, :cond_0

    const/16 v2, 0x202

    .line 267
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v3

    if-eq v2, v3, :cond_1

    const/16 v2, 0xe00

    .line 268
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v3

    if-gt v2, v3, :cond_0

    const/16 v2, 0xeff

    .line 269
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 282
    :cond_1
    invoke-static {}, Lcom/epson/epsonio/usb/DevUsb;->access$100()Ljava/util/ArrayList;

    move-result-object v2

    monitor-enter v2

    const/4 v3, 0x0

    .line 285
    :try_start_1
    invoke-static {}, Lcom/epson/epsonio/usb/DevUsb;->access$100()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/epson/epsonio/DeviceInfo;

    .line 286
    invoke-virtual {v5}, Lcom/epson/epsonio/DeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    .line 287
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v3, 0x1

    :cond_3
    if-nez v3, :cond_4

    .line 294
    new-instance v3, Lcom/epson/epsonio/DeviceInfo;

    const/16 v5, 0x103

    const-string v6, "TM Printer"

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    const-string v9, ""

    move-object v4, v3

    invoke-direct/range {v4 .. v9}, Lcom/epson/epsonio/DeviceInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    invoke-static {}, Lcom/epson/epsonio/usb/DevUsb;->access$100()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 298
    :cond_4
    monitor-exit v2

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_5
    const-wide/16 v0, 0x12c

    .line 305
    :try_start_2
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    :catch_0
    nop

    goto/16 :goto_0

    :catch_1
    :cond_6
    return-void
.end method
