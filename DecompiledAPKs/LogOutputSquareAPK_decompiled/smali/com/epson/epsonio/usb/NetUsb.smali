.class public Lcom/epson/epsonio/usb/NetUsb;
.super Ljava/lang/Object;
.source "NetUsb.java"


# static fields
.field private static final ACTION_USB_PERMISSION:Ljava/lang/String; = "com.android.eposprint.USB_PERMISSION"

.field private static final M_INVALID_HANDLE_INDEX:I = -0x1

.field private static final M_MAX_HANDLE_COUNT:I = 0x10

.field private static final M_MAX_RECEIVE_BUFFER_SIZE:I = 0x400

.field private static final M_MAX_SENT_SIZE_AT_ONCE:I = 0x1000

.field private static mCurrentHandle:I

.field private static m_ConnectertList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/epson/epsonio/usb/UsbConnecter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lcom/epson/epsonio/usb/NetUsb;->m_ConnectertList:Ljava/util/Vector;

    const/4 v0, 0x0

    .line 26
    sput v0, Lcom/epson/epsonio/usb/NetUsb;->mCurrentHandle:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static checkDeviceName(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 3

    const/4 v0, 0x0

    .line 428
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 431
    :goto_0
    :try_start_0
    sget-object v2, Lcom/epson/epsonio/usb/NetUsb;->m_ConnectertList:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 432
    sget-object v2, Lcom/epson/epsonio/usb/NetUsb;->m_ConnectertList:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/epson/epsonio/usb/UsbConnecter;

    .line 434
    invoke-virtual {v2, p0}, Lcom/epson/epsonio/usb/UsbConnecter;->checkDevice(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 p0, 0x1

    .line 435
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, p0

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    :cond_1
    :goto_1
    return-object v1
.end method

.method public static close(I)I
    .locals 1

    .line 121
    :try_start_0
    invoke-static {p0}, Lcom/epson/epsonio/usb/NetUsb;->getConnecter(I)Lcom/epson/epsonio/usb/UsbConnecter;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p0, 0x6

    return p0

    .line 126
    :cond_0
    invoke-static {p0}, Lcom/epson/epsonio/usb/NetUsb;->removeConnecter(I)Ljava/lang/Boolean;

    .line 128
    invoke-virtual {v0}, Lcom/epson/epsonio/usb/UsbConnecter;->close()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p0, 0x0

    return p0

    :catch_0
    const/16 p0, 0xff

    return p0
.end method

.method public static enableFlowControl(II)I
    .locals 0

    .line 343
    invoke-static {p0}, Lcom/epson/epsonio/usb/NetUsb;->getConnecter(I)Lcom/epson/epsonio/usb/UsbConnecter;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x6

    return p0

    .line 348
    :cond_0
    invoke-virtual {p0, p1}, Lcom/epson/epsonio/usb/UsbConnecter;->enableFlowControl(I)I

    move-result p0

    return p0
.end method

.method protected static getConnecter(I)Lcom/epson/epsonio/usb/UsbConnecter;
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 383
    :goto_0
    :try_start_0
    sget-object v2, Lcom/epson/epsonio/usb/NetUsb;->m_ConnectertList:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 384
    sget-object v2, Lcom/epson/epsonio/usb/NetUsb;->m_ConnectertList:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/epson/epsonio/usb/UsbConnecter;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 386
    :try_start_1
    invoke-virtual {v2, p0}, Lcom/epson/epsonio/usb/UsbConnecter;->checkHandle(I)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v1, :cond_0

    :catch_0
    move-object v1, v2

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    move-object v1, v2

    goto :goto_0

    :catch_1
    :cond_1
    :goto_1
    return-object v1
.end method

.method public static getOnlineDMStatus(I[I)I
    .locals 0

    .line 367
    invoke-static {p0}, Lcom/epson/epsonio/usb/NetUsb;->getConnecter(I)Lcom/epson/epsonio/usb/UsbConnecter;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x6

    return p0

    .line 372
    :cond_0
    invoke-virtual {p0, p1}, Lcom/epson/epsonio/usb/UsbConnecter;->getOnlineDMStatus([I)I

    move-result p0

    return p0
.end method

.method public static getOnlineTMStatus(I[I)I
    .locals 0

    .line 355
    invoke-static {p0}, Lcom/epson/epsonio/usb/NetUsb;->getConnecter(I)Lcom/epson/epsonio/usb/UsbConnecter;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x6

    return p0

    .line 360
    :cond_0
    invoke-virtual {p0, p1}, Lcom/epson/epsonio/usb/UsbConnecter;->getOnlineTMStatus([I)I

    move-result p0

    return p0
.end method

.method public static kill(I)I
    .locals 1

    .line 144
    :try_start_0
    invoke-static {p0}, Lcom/epson/epsonio/usb/NetUsb;->getConnecter(I)Lcom/epson/epsonio/usb/UsbConnecter;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p0, 0x6

    return p0

    .line 149
    :cond_0
    invoke-static {p0}, Lcom/epson/epsonio/usb/NetUsb;->removeConnecter(I)Ljava/lang/Boolean;

    .line 151
    invoke-virtual {v0}, Lcom/epson/epsonio/usb/UsbConnecter;->close()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p0, 0x0

    return p0

    :catch_0
    const/16 p0, 0xff

    return p0
.end method

.method public static open(Ljava/lang/String;Ljava/lang/String;[ILjava/lang/Object;)I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 p1, 0x1

    if-eqz p0, :cond_a

    if-nez p3, :cond_0

    goto/16 :goto_1

    :cond_0
    if-eqz p2, :cond_a

    .line 43
    array-length v0, p2

    if-nez v0, :cond_1

    goto/16 :goto_1

    :cond_1
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 47
    aput v0, p2, v1

    .line 50
    invoke-static {p0}, Lcom/epson/epsonio/usb/NetUsb;->checkDeviceName(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v2, 0x6

    if-eqz v0, :cond_2

    return v2

    :cond_2
    const/16 v0, 0x10

    .line 54
    sget-object v3, Lcom/epson/epsonio/usb/NetUsb;->m_ConnectertList:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-gt v0, v3, :cond_3

    return v2

    .line 58
    :cond_3
    new-instance v0, Lcom/epson/epsonio/usb/UsbConnecter;

    invoke-direct {v0}, Lcom/epson/epsonio/usb/UsbConnecter;-><init>()V

    .line 61
    check-cast p3, Landroid/content/Context;

    const-string v2, "usb"

    invoke-virtual {p3, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/usb/UsbManager;

    if-nez v2, :cond_4

    const/16 p0, 0xff

    return p0

    .line 67
    :cond_4
    invoke-virtual {v2}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v3

    .line 69
    invoke-virtual {v3, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/hardware/usb/UsbDevice;

    if-nez p0, :cond_5

    return p1

    .line 74
    :cond_5
    invoke-virtual {v2, p0}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 76
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.android.eposprint.USB_PERMISSION"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p3, v1, v3, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p3

    if-nez p3, :cond_6

    return p1

    .line 83
    :cond_6
    invoke-virtual {v2, p0, p3}, Landroid/hardware/usb/UsbManager;->requestPermission(Landroid/hardware/usb/UsbDevice;Landroid/app/PendingIntent;)V

    .line 85
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 87
    :goto_0
    invoke-virtual {v2, p0}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    move-result p3

    if-nez p3, :cond_8

    .line 88
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v3

    const-wide/16 v7, 0x7530

    cmp-long p3, v5, v7

    if-lez p3, :cond_7

    const/4 p0, 0x2

    return p0

    :cond_7
    const-wide/16 v5, 0x64

    .line 94
    :try_start_0
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    .line 103
    :cond_8
    sget p3, Lcom/epson/epsonio/usb/NetUsb;->mCurrentHandle:I

    add-int/2addr p3, p1

    .line 104
    invoke-virtual {v0, p3, v2, p0}, Lcom/epson/epsonio/usb/UsbConnecter;->connect(ILandroid/hardware/usb/UsbManager;Landroid/hardware/usb/UsbDevice;)I

    move-result p0

    if-nez p0, :cond_9

    .line 107
    sput p3, Lcom/epson/epsonio/usb/NetUsb;->mCurrentHandle:I

    .line 108
    sget p1, Lcom/epson/epsonio/usb/NetUsb;->mCurrentHandle:I

    aput p1, p2, v1

    .line 110
    sget-object p1, Lcom/epson/epsonio/usb/NetUsb;->m_ConnectertList:Ljava/util/Vector;

    invoke-virtual {p1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_9
    return p0

    :cond_a
    :goto_1
    return p1
.end method

.method public static read(I[BIII[I)I
    .locals 16

    move-object/from16 v0, p1

    move/from16 v1, p3

    move/from16 v2, p4

    move-object/from16 v3, p5

    const/4 v4, 0x1

    if-eqz v3, :cond_d

    .line 253
    array-length v5, v3

    if-le v4, v5, :cond_0

    goto/16 :goto_5

    :cond_0
    const/4 v5, 0x0

    .line 257
    aput v5, v3, v5

    if-ltz p2, :cond_c

    if-gez v1, :cond_1

    goto :goto_4

    :cond_1
    if-gez v2, :cond_2

    return v4

    .line 267
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/epson/epsonio/usb/NetUsb;->getConnecter(I)Lcom/epson/epsonio/usb/UsbConnecter;

    move-result-object v6

    if-nez v6, :cond_3

    const/4 v0, 0x6

    return v0

    :cond_3
    if-nez v1, :cond_4

    return v5

    :cond_4
    add-int v7, p2, v1

    if-gez v7, :cond_5

    const/16 v0, 0xff

    return v0

    :cond_5
    if-eqz v0, :cond_b

    .line 280
    array-length v8, v0

    if-le v7, v8, :cond_6

    goto :goto_3

    .line 285
    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    move/from16 v9, p2

    const/4 v10, 0x0

    const/4 v11, 0x0

    :goto_0
    const/16 v12, 0x400

    new-array v12, v12, [B

    .line 293
    array-length v13, v12

    invoke-static {v13, v1}, Ljava/lang/Math;->min(II)I

    move-result v13

    new-array v14, v4, [I

    .line 298
    invoke-virtual {v6, v12, v13, v14, v2}, Lcom/epson/epsonio/usb/UsbConnecter;->balkRead([BI[II)I

    .line 300
    aget v13, v14, v5

    if-lez v13, :cond_8

    .line 305
    :try_start_0
    aget v11, v14, v5

    invoke-static {v12, v5, v0, v9, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 312
    aget v11, v14, v5

    add-int/2addr v10, v11

    .line 313
    aput v10, v3, v5

    .line 314
    aget v11, v14, v5

    add-int/2addr v9, v11

    if-ne v10, v1, :cond_7

    goto :goto_2

    :cond_7
    const/4 v11, 0x1

    goto :goto_1

    :catch_0
    const/4 v5, 0x5

    goto :goto_2

    :cond_8
    :goto_1
    if-nez v11, :cond_9

    .line 324
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long/2addr v12, v7

    int-to-long v4, v2

    cmp-long v15, v12, v4

    if-lez v15, :cond_9

    const/4 v5, 0x4

    goto :goto_2

    :cond_9
    const/4 v5, 0x0

    if-eqz v11, :cond_a

    .line 330
    aget v4, v14, v5

    if-gtz v4, :cond_a

    :goto_2
    return v5

    :cond_a
    const/4 v4, 0x1

    goto :goto_0

    :cond_b
    :goto_3
    const/4 v0, 0x1

    return v0

    :cond_c
    :goto_4
    const/4 v0, 0x1

    return v0

    :cond_d
    :goto_5
    const/4 v0, 0x1

    return v0
.end method

.method protected static removeConnecter(I)Ljava/lang/Boolean;
    .locals 3

    const/4 v0, 0x0

    .line 403
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 406
    :goto_0
    :try_start_0
    sget-object v2, Lcom/epson/epsonio/usb/NetUsb;->m_ConnectertList:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 407
    sget-object v2, Lcom/epson/epsonio/usb/NetUsb;->m_ConnectertList:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/epson/epsonio/usb/UsbConnecter;

    .line 409
    invoke-virtual {v2, p0}, Lcom/epson/epsonio/usb/UsbConnecter;->checkHandle(I)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 410
    sget-object p0, Lcom/epson/epsonio/usb/NetUsb;->m_ConnectertList:Ljava/util/Vector;

    invoke-virtual {p0, v0}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    const/4 p0, 0x1

    .line 411
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, p0

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    :cond_1
    :goto_1
    return-object v1
.end method

.method public static write(I[BIII[I)I
    .locals 10

    const/4 v0, 0x1

    if-eqz p5, :cond_a

    .line 165
    array-length v1, p5

    if-le v0, v1, :cond_0

    goto :goto_2

    :cond_0
    const/4 v1, 0x0

    .line 169
    aput v1, p5, v1

    if-ltz p2, :cond_a

    if-gez p3, :cond_1

    goto :goto_2

    :cond_1
    if-gez p4, :cond_2

    return v0

    .line 179
    :cond_2
    invoke-static {p0}, Lcom/epson/epsonio/usb/NetUsb;->getConnecter(I)Lcom/epson/epsonio/usb/UsbConnecter;

    move-result-object p0

    if-nez p0, :cond_3

    const/4 p0, 0x6

    return p0

    :cond_3
    if-nez p3, :cond_4

    return v1

    :cond_4
    add-int v2, p2, p3

    const/16 v3, 0xff

    if-gez v2, :cond_5

    return v3

    :cond_5
    if-eqz p1, :cond_a

    .line 192
    array-length v4, p1

    if-le v2, v4, :cond_6

    goto :goto_2

    .line 199
    :cond_6
    :try_start_0
    new-array v2, p3, [B

    .line 200
    invoke-static {p1, p2, v2, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 210
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    .line 211
    array-length p3, v2

    const/4 v4, 0x0

    :cond_7
    :goto_0
    if-lez p3, :cond_9

    const/16 v5, 0x1000

    new-array v6, v5, [B

    .line 216
    invoke-static {p3, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    new-array v7, v0, [I

    .line 220
    invoke-static {v2, v4, v6, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 223
    invoke-virtual {p0, v6, v5, v7, p4}, Lcom/epson/epsonio/usb/UsbConnecter;->balkWrite([BI[II)I

    .line 225
    aget v5, v7, v1

    sub-int/2addr p3, v5

    .line 226
    aget v5, v7, v1

    add-int/2addr v4, v5

    .line 227
    aget v5, p5, v1

    aget v6, v7, v1

    add-int/2addr v5, v6

    aput v5, p5, v1

    .line 229
    aget v5, v7, v1

    if-nez v5, :cond_7

    .line 230
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, p1

    int-to-long v7, p4

    cmp-long v9, v5, v7

    if-lez v9, :cond_8

    const/4 v1, 0x4

    const/4 v3, 0x4

    goto :goto_1

    :cond_8
    const-wide/16 v5, 0x14

    .line 235
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :cond_9
    const/4 v3, 0x0

    :catch_0
    :goto_1
    return v3

    :catch_1
    const/4 p0, 0x5

    return p0

    :cond_a
    :goto_2
    return v0
.end method
