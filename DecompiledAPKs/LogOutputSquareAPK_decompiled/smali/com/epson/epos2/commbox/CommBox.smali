.class public Lcom/epson/epos2/commbox/CommBox;
.super Ljava/lang/Object;
.source "CommBox.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;,
        Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;
    }
.end annotation


# static fields
.field public static final EVENT_DISCONNECT:I = 0x2

.field public static final EVENT_RECONNECT:I = 0x1

.field public static final EVENT_RECONNECTING:I = 0x0

.field public static final FALSE:I = 0x0

.field private static final NO_EXCEPTION:I = 0x0

.field public static final PARAM_DEFAULT:I = -0x2

.field private static final RETURN_NULL:I = 0x101

.field private static final RETURN_NULL_CHARACTER:I = 0x100

.field public static final TRUE:I = 0x1

.field private static connection:I


# instance fields
.field private mClassOutputLog:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private mCommBoxHandle:J

.field private mConnectionListener:Lcom/epson/epos2/ConnectionListener;

.field private mContext:Landroid/content/Context;

.field private mOutputExceptionMethod:Ljava/lang/reflect/Method;

.field private mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

.field private mOutputLogEventMethod:Ljava/lang/reflect/Method;

.field private mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

.field private mReadLogSettingsMethod:Ljava/lang/reflect/Method;

.field private mReceiveListener:Lcom/epson/epos2/commbox/ReceiveListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "epos2"

    .line 16
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v0, 0x0

    .line 49
    sput v0, Lcom/epson/epos2/commbox/CommBox;->connection:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 24
    iput-wide v0, p0, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    const/4 v0, 0x0

    .line 25
    iput-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mContext:Landroid/content/Context;

    .line 26
    iput-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mReceiveListener:Lcom/epson/epos2/commbox/ReceiveListener;

    .line 27
    iput-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    .line 29
    iput-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mClassOutputLog:Ljava/lang/Class;

    .line 30
    iput-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 31
    iput-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 32
    iput-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 33
    iput-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    .line 34
    iput-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    .line 73
    invoke-direct {p0, p1}, Lcom/epson/epos2/commbox/CommBox;->initializeOuputLogFunctions(Landroid/content/Context;)V

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "CommBox"

    .line 74
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/commbox/CommBox;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    const-string v1, "com.epson.epos2.NativeInitializer"

    .line 77
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v4, "initializeNativeEnv"

    new-array v5, v0, [Ljava/lang/Class;

    .line 78
    const-class v6, Landroid/content/Context;

    aput-object v6, v5, v2

    invoke-virtual {v1, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 79
    invoke-virtual {v4, v0}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    new-array v5, v0, [Ljava/lang/Object;

    aput-object p1, v5, v2

    .line 80
    invoke-virtual {v4, v1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 83
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/commbox/CommBox;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 84
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 86
    :goto_0
    iput-object p1, p0, Lcom/epson/epos2/commbox/CommBox;->mContext:Landroid/content/Context;

    .line 88
    invoke-virtual {p0}, Lcom/epson/epos2/commbox/CommBox;->initializeCommBoxInstance()V

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 89
    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/commbox/CommBox;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/epos2/commbox/CommBox;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1, p2}, Lcom/epson/epos2/commbox/CommBox;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$100(Lcom/epson/epos2/commbox/CommBox;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1, p2}, Lcom/epson/epos2/commbox/CommBox;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$200(Lcom/epson/epos2/commbox/CommBox;Ljava/lang/String;I[Ljava/lang/Object;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1, p2, p3}, Lcom/epson/epos2/commbox/CommBox;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private initializeOuputLogFunctions(Landroid/content/Context;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    :try_start_0
    const-string v0, "com.epson.epos2.OutputLog"

    .line 659
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mClassOutputLog:Ljava/lang/Class;

    .line 661
    iget-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogCallFunction"

    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    const-class v4, [Ljava/lang/Object;

    const/4 v7, 0x2

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 662
    iget-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 663
    iget-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogReturnFunction"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v7

    const-class v4, [Ljava/lang/Object;

    aput-object v4, v3, v2

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 664
    iget-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 665
    iget-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputException"

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    const-class v4, Ljava/lang/Exception;

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 666
    iget-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 667
    iget-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogEvent"

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    aput-object v3, v2, v5

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v6

    const-class v3, [Ljava/lang/Object;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    .line 668
    iget-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 669
    iget-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "readLogSettings"

    new-array v2, v6, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    .line 670
    iget-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 672
    iget-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/commbox/CommBox;->mClassOutputLog:Ljava/lang/Class;

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 675
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private native nativeEpos2Connect(JLjava/lang/String;JLjava/lang/String;Ljava/lang/Object;)I
.end method

.method private native nativeEpos2CreateHandle([J)I
.end method

.method private native nativeEpos2DestroyHandle(J)I
.end method

.method private native nativeEpos2Disconnect(J)I
.end method

.method private onConnection(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 588
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onConnection"

    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/commbox/CommBox;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 590
    iget-object v1, p0, Lcom/epson/epos2/commbox/CommBox;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    if-eqz v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v5, "eventType->"

    aput-object v5, v1, v3

    .line 594
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    .line 593
    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/commbox/CommBox;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 596
    iget-object v1, p0, Lcom/epson/epos2/commbox/CommBox;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/ConnectionListener;->onConnection(Ljava/lang/Object;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 600
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-direct {p0, v4, v3, v0}, Lcom/epson/epos2/commbox/CommBox;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private outputException(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 5

    .line 699
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/commbox/CommBox;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 681
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/commbox/CommBox;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 708
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/commbox/CommBox;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    .locals 5

    .line 690
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/commbox/CommBox;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/commbox/CommBox;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, p1

    const/4 p1, 0x3

    aput-object p3, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method


# virtual methods
.method protected checkHandle()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 250
    iget-wide v0, p0, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    return-void

    .line 251
    :cond_0
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
.end method

.method public connect(Ljava/lang/String;ILjava/lang/String;)V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v9, p0

    const/4 v10, 0x3

    new-array v0, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p1, v0, v11

    .line 179
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v12, 0x1

    aput-object v1, v0, v12

    const/4 v13, 0x2

    aput-object p3, v0, v13

    const-string v14, "connect"

    invoke-direct {v9, v14, v0}, Lcom/epson/epos2/commbox/CommBox;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_2

    .line 186
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/commbox/CommBox;->checkHandle()V

    if-eqz p3, :cond_0

    move-object/from16 v7, p3

    goto :goto_0

    :cond_0
    const-string v0, ""

    move-object v7, v0

    .line 195
    :goto_0
    iget-wide v2, v9, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    move/from16 v15, p2

    int-to-long v5, v15

    :try_start_1
    iget-object v8, v9, Lcom/epson/epos2/commbox/CommBox;->mContext:Landroid/content/Context;

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    invoke-direct/range {v1 .. v8}, Lcom/epson/epos2/commbox/CommBox;->nativeEpos2Connect(JLjava/lang/String;JLjava/lang/String;Ljava/lang/Object;)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_1

    new-array v0, v10, [Ljava/lang/Object;

    aput-object p1, v0, v11

    .line 206
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    aput-object p3, v0, v13

    invoke-direct {v9, v14, v11, v0}, Lcom/epson/epos2/commbox/CommBox;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 197
    :cond_1
    :try_start_2
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1

    :catch_0
    move-exception v0

    move/from16 v15, p2

    goto :goto_1

    :cond_2
    move/from16 v15, p2

    .line 183
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v12}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    .line 201
    :goto_1
    invoke-direct {v9, v14, v0}, Lcom/epson/epos2/commbox/CommBox;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 202
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v2, v10, [Ljava/lang/Object;

    aput-object p1, v2, v11

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v12

    aput-object p3, v2, v13

    invoke-direct {v9, v14, v1, v2}, Lcom/epson/epos2/commbox/CommBox;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 203
    throw v0
.end method

.method public disconnect()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "disconnect"

    .line 221
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/commbox/CommBox;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 224
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/commbox/CommBox;->checkHandle()V

    .line 226
    iget-wide v3, p0, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/commbox/CommBox;->nativeEpos2Disconnect(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 237
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/commbox/CommBox;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 228
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 232
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/commbox/CommBox;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 233
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/commbox/CommBox;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 234
    throw v1
.end method

.method protected finalize()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "finalize"

    .line 120
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/commbox/CommBox;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    .line 122
    iput-object v1, p0, Lcom/epson/epos2/commbox/CommBox;->mReceiveListener:Lcom/epson/epos2/commbox/ReceiveListener;

    .line 123
    iput-object v1, p0, Lcom/epson/epos2/commbox/CommBox;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    .line 126
    :try_start_0
    iget-wide v3, p0, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_0

    .line 128
    iget-wide v3, p0, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/commbox/CommBox;->nativeEpos2Disconnect(J)I

    .line 129
    iget-wide v3, p0, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/commbox/CommBox;->nativeEpos2DestroyHandle(J)I

    .line 130
    iput-wide v5, p0, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    new-array v1, v0, [Ljava/lang/Object;

    .line 137
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/commbox/CommBox;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    .line 134
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 135
    throw v0
.end method

.method public getAdmin()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getAdmin"

    .line 536
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/commbox/CommBox;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    new-array v1, v0, [Ljava/lang/Object;

    .line 540
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/commbox/CommBox;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0
.end method

.method public getCommHistory(Lcom/epson/epos2/commbox/GetCommHistoryCallback;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "getCommHistory"

    .line 297
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/commbox/CommBox;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 300
    :try_start_0
    iget-wide v4, p0, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_2

    if-eqz p1, :cond_1

    .line 308
    iget-wide v4, p0, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    new-instance v1, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;

    iget-wide v8, p0, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    move-object v6, v1

    move-object v7, p0

    move-object v10, p1

    move-object v11, p0

    invoke-direct/range {v6 .. v11}, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;-><init>(Lcom/epson/epos2/commbox/CommBox;JLcom/epson/epos2/commbox/GetCommHistoryCallback;Lcom/epson/epos2/commbox/CommBox;)V

    invoke-virtual {p0, v4, v5, v1}, Lcom/epson/epos2/commbox/CommBox;->nativeEpos2GetCommHistory(JLcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 319
    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/commbox/CommBox;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 310
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4

    .line 305
    :cond_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1

    .line 301
    :cond_2
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    const/4 v4, 0x5

    invoke-direct {v1, v4}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 314
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/commbox/CommBox;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 315
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-direct {p0, v3, v4, v0}, Lcom/epson/epos2/commbox/CommBox;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 316
    throw v1
.end method

.method public getLocation()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getLocation"

    .line 556
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/commbox/CommBox;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    new-array v1, v0, [Ljava/lang/Object;

    .line 560
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/commbox/CommBox;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0
.end method

.method public getStatus()Lcom/epson/epos2/commbox/CommBoxStatusInfo;
    .locals 6

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getStatus"

    .line 510
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/commbox/CommBox;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 512
    iget-wide v3, p0, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/commbox/CommBox;->nativeEpos2GetStatus(J)Lcom/epson/epos2/commbox/CommBoxStatusInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 514
    invoke-virtual {v1}, Lcom/epson/epos2/commbox/CommBoxStatusInfo;->getConnection()I

    move-result v3

    sput v3, Lcom/epson/epos2/commbox/CommBox;->connection:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 516
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "connection->"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v5, Lcom/epson/epos2/commbox/CommBox;->connection:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-direct {p0, v2, v0, v3}, Lcom/epson/epos2/commbox/CommBox;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/16 v3, 0x101

    new-array v0, v0, [Ljava/lang/Object;

    .line 519
    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/commbox/CommBox;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v1
.end method

.method protected initializeCommBoxInstance()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [J

    .line 103
    invoke-direct {p0, v0}, Lcom/epson/epos2/commbox/CommBox;->nativeEpos2CreateHandle([J)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 109
    aget-wide v2, v0, v1

    iput-wide v2, p0, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    const-wide/16 v2, 0x0

    aput-wide v2, v0, v1

    return-void

    .line 106
    :cond_0
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
.end method

.method protected native nativeEpos2GetCommHistory(JLcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;)I
.end method

.method protected native nativeEpos2GetStatus(J)Lcom/epson/epos2/commbox/CommBoxStatusInfo;
.end method

.method protected native nativeEpos2SendMessage(JLjava/lang/String;Ljava/lang/String;Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;)I
.end method

.method protected onCommBoxReceive(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 p1, 0x4

    new-array p2, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p3, p2, v0

    const/4 v1, 0x1

    aput-object p4, p2, v1

    const/4 v2, 0x2

    aput-object p5, p2, v2

    const/4 v3, 0x3

    aput-object p0, p2, v3

    const-string v4, "onCommBoxReceive"

    .line 443
    invoke-direct {p0, v4, p2}, Lcom/epson/epos2/commbox/CommBox;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 444
    iget-object p2, p0, Lcom/epson/epos2/commbox/CommBox;->mReceiveListener:Lcom/epson/epos2/commbox/ReceiveListener;

    if-eqz p2, :cond_0

    const/4 p2, 0x6

    new-array p2, p2, [Ljava/lang/Object;

    const-string v5, "senderId->"

    aput-object v5, p2, v0

    aput-object p3, p2, v1

    const-string v5, "receiverId->"

    aput-object v5, p2, v2

    aput-object p4, p2, v3

    const-string v5, "message->"

    aput-object v5, p2, p1

    const/4 v5, 0x5

    aput-object p5, p2, v5

    .line 446
    invoke-direct {p0, v4, p2}, Lcom/epson/epos2/commbox/CommBox;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 451
    iget-object p2, p0, Lcom/epson/epos2/commbox/CommBox;->mReceiveListener:Lcom/epson/epos2/commbox/ReceiveListener;

    invoke-interface {p2, p0, p3, p4, p5}, Lcom/epson/epos2/commbox/ReceiveListener;->onCommBoxReceive(Lcom/epson/epos2/commbox/CommBox;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-array p1, p1, [Ljava/lang/Object;

    aput-object p3, p1, v0

    aput-object p4, p1, v1

    aput-object p5, p1, v2

    aput-object p0, p1, v3

    .line 453
    invoke-direct {p0, v4, v0, p1}, Lcom/epson/epos2/commbox/CommBox;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method public sendMessage(Ljava/lang/String;Ljava/lang/String;Lcom/epson/epos2/commbox/SendMessageCallback;)V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v7, p0

    const/4 v8, 0x3

    new-array v0, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v0, v9

    const/4 v10, 0x1

    aput-object p2, v0, v10

    const/4 v11, 0x2

    aput-object p3, v0, v11

    const-string v12, "sendMessage"

    .line 362
    invoke-direct {v7, v12, v0}, Lcom/epson/epos2/commbox/CommBox;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 365
    :try_start_0
    iget-wide v0, v7, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_3

    if-eqz p1, :cond_2

    if-eqz p3, :cond_2

    if-eqz p2, :cond_0

    move-object/from16 v0, p2

    goto :goto_0

    :cond_0
    const-string v0, ""

    .line 381
    :goto_0
    iget-wide v13, v7, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    new-instance v15, Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;

    iget-wide v3, v7, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    move-object v1, v15

    move-object/from16 v2, p0

    move-object/from16 v5, p3

    move-object/from16 v6, p0

    invoke-direct/range {v1 .. v6}, Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;-><init>(Lcom/epson/epos2/commbox/CommBox;JLcom/epson/epos2/commbox/SendMessageCallback;Lcom/epson/epos2/commbox/CommBox;)V

    move-object/from16 v1, p0

    move-wide v2, v13

    move-object/from16 v4, p1

    move-object v5, v0

    move-object v6, v15

    invoke-virtual/range {v1 .. v6}, Lcom/epson/epos2/commbox/CommBox;->nativeEpos2SendMessage(JLjava/lang/String;Ljava/lang/String;Lcom/epson/epos2/commbox/CommBox$CommBoxSendMessageCallbackAdapter;)I

    move-result v0
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    new-array v0, v8, [Ljava/lang/Object;

    aput-object p1, v0, v9

    aput-object p2, v0, v10

    aput-object p3, v0, v11

    .line 392
    invoke-direct {v7, v12, v9, v0}, Lcom/epson/epos2/commbox/CommBox;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 383
    :cond_1
    :try_start_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1

    .line 370
    :cond_2
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v10}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0

    .line 366
    :cond_3
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 387
    invoke-direct {v7, v12, v0}, Lcom/epson/epos2/commbox/CommBox;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 388
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v2, v8, [Ljava/lang/Object;

    aput-object p1, v2, v9

    aput-object p2, v2, v10

    aput-object p3, v2, v11

    invoke-direct {v7, v12, v1, v2}, Lcom/epson/epos2/commbox/CommBox;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 389
    throw v0
.end method

.method public setConnectionEventListener(Lcom/epson/epos2/ConnectionListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setConnectionEventListener"

    .line 572
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/commbox/CommBox;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 574
    iget-wide v0, p0, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 578
    iput-object p1, p0, Lcom/epson/epos2/commbox/CommBox;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 581
    iput-object p1, p0, Lcom/epson/epos2/commbox/CommBox;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    :goto_0
    return-void
.end method

.method public setReceiveEventListener(Lcom/epson/epos2/commbox/ReceiveListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setReceiveEventListener"

    .line 429
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/commbox/CommBox;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 431
    iget-wide v0, p0, Lcom/epson/epos2/commbox/CommBox;->mCommBoxHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 436
    iput-object p1, p0, Lcom/epson/epos2/commbox/CommBox;->mReceiveListener:Lcom/epson/epos2/commbox/ReceiveListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 439
    iput-object p1, p0, Lcom/epson/epos2/commbox/CommBox;->mReceiveListener:Lcom/epson/epos2/commbox/ReceiveListener;

    :goto_0
    return-void
.end method
