.class public Lcom/epson/epos2/cashchanger/Epos2CallbackCode;
.super Ljava/lang/Object;
.source "Epos2CallbackCode.java"


# static fields
.field public static final CODE_BUSY:I = 0x1

.field public static final CODE_DISCREPANCY:I = 0x2

.field public static final CODE_ERR_CASH_IN_TRAY:I = 0x3

.field public static final CODE_ERR_COMMAND:I = 0x9

.field public static final CODE_ERR_DEVICE:I = 0xa

.field public static final CODE_ERR_FAILURE:I = 0xc

.field public static final CODE_ERR_OPOSCODE:I = 0x6

.field public static final CODE_ERR_PARAM:I = 0x8

.field public static final CODE_ERR_REJECT_UNIT:I = 0x5

.field public static final CODE_ERR_SHORTAGE:I = 0x4

.field public static final CODE_ERR_SYSTEM:I = 0xb

.field public static final CODE_ERR_UNSUPPORTED:I = 0x7

.field public static final CODE_SUCCESS:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
