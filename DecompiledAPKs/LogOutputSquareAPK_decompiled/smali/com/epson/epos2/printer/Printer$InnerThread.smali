.class Lcom/epson/epos2/printer/Printer$InnerThread;
.super Ljava/lang/Thread;
.source "Printer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/epos2/printer/Printer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InnerThread"
.end annotation


# instance fields
.field mHandle:J

.field mListener:Ljava/lang/Object;

.field mPrn:Lcom/epson/epos2/printer/Printer;

.field mTimeout:I

.field mType:I

.field mTypes:[I

.field mValues:[I

.field final synthetic this$0:Lcom/epson/epos2/printer/Printer;


# direct methods
.method public constructor <init>(Lcom/epson/epos2/printer/Printer;Lcom/epson/epos2/printer/Printer;Ljava/lang/Object;JII[I[I)V
    .locals 2

    .line 1608
    iput-object p1, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->this$0:Lcom/epson/epos2/printer/Printer;

    .line 1609
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 p1, 0x0

    .line 1601
    iput-object p1, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->mPrn:Lcom/epson/epos2/printer/Printer;

    .line 1602
    iput-object p1, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->mListener:Ljava/lang/Object;

    const-wide/16 v0, 0x0

    .line 1603
    iput-wide v0, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->mHandle:J

    const/4 v0, 0x0

    .line 1604
    iput v0, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->mTimeout:I

    .line 1605
    iput v0, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->mType:I

    .line 1606
    iput-object p1, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->mTypes:[I

    .line 1607
    iput-object p1, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->mValues:[I

    .line 1610
    iput-object p2, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->mPrn:Lcom/epson/epos2/printer/Printer;

    .line 1611
    iput-object p3, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->mListener:Ljava/lang/Object;

    .line 1612
    iput-wide p4, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->mHandle:J

    .line 1613
    iput p6, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->mTimeout:I

    .line 1614
    iput p7, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->mType:I

    if-eqz p8, :cond_0

    .line 1616
    array-length p2, p8

    new-array p2, p2, [I

    iput-object p2, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->mTypes:[I

    .line 1618
    :try_start_0
    iget-object p2, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->mTypes:[I

    array-length p3, p8

    invoke-static {p8, v0, p2, v0, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1621
    :catch_0
    iput-object p1, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->mTypes:[I

    :cond_0
    :goto_0
    if-eqz p9, :cond_1

    .line 1625
    array-length p2, p9

    new-array p2, p2, [I

    iput-object p2, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->mValues:[I

    .line 1627
    :try_start_1
    iget-object p2, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->mValues:[I

    array-length p3, p9

    invoke-static {p9, v0, p2, v0, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 1630
    :catch_1
    iput-object p1, p0, Lcom/epson/epos2/printer/Printer$InnerThread;->mValues:[I

    :cond_1
    :goto_1
    return-void
.end method
