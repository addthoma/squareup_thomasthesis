.class public Lcom/epson/epos2/Epos2Exception;
.super Ljava/lang/Exception;
.source "Epos2Exception.java"


# static fields
.field public static final ERR_ALREADY_OPENED:I = 0xb

.field public static final ERR_ALREADY_USED:I = 0xc

.field public static final ERR_BOX_CLIENT_OVER:I = 0xe

.field public static final ERR_BOX_COUNT_OVER:I = 0xd

.field public static final ERR_CONNECT:I = 0x2

.field public static final ERR_DISCONNECT:I = 0xa

.field public static final ERR_FAILURE:I = 0xff

.field public static final ERR_ILLEGAL:I = 0x5

.field public static final ERR_IN_USE:I = 0x8

.field public static final ERR_MEMORY:I = 0x4

.field public static final ERR_NOT_FOUND:I = 0x7

.field public static final ERR_PARAM:I = 0x1

.field public static final ERR_PROCESSING:I = 0x6

.field public static final ERR_TIMEOUT:I = 0x3

.field public static final ERR_TYPE_INVALID:I = 0x9

.field public static final ERR_UNSUPPORTED:I = 0xf

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mErrorStatus:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 30
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    const/16 v0, 0xff

    .line 24
    iput v0, p0, Lcom/epson/epos2/Epos2Exception;->mErrorStatus:I

    .line 31
    iput p1, p0, Lcom/epson/epos2/Epos2Exception;->mErrorStatus:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 p1, 0xff

    .line 24
    iput p1, p0, Lcom/epson/epos2/Epos2Exception;->mErrorStatus:I

    return-void
.end method


# virtual methods
.method public getErrorStatus()I
    .locals 1

    .line 41
    iget v0, p0, Lcom/epson/epos2/Epos2Exception;->mErrorStatus:I

    return v0
.end method
