.class public final Lcom/google/android/material/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/material/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f0d0000

.field public static final abc_action_bar_up_container:I = 0x7f0d0001

.field public static final abc_action_menu_item_layout:I = 0x7f0d0002

.field public static final abc_action_menu_layout:I = 0x7f0d0003

.field public static final abc_action_mode_bar:I = 0x7f0d0004

.field public static final abc_action_mode_close_item_material:I = 0x7f0d0005

.field public static final abc_activity_chooser_view:I = 0x7f0d0006

.field public static final abc_activity_chooser_view_list_item:I = 0x7f0d0007

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f0d0008

.field public static final abc_alert_dialog_material:I = 0x7f0d0009

.field public static final abc_alert_dialog_title_material:I = 0x7f0d000a

.field public static final abc_cascading_menu_item_layout:I = 0x7f0d000b

.field public static final abc_dialog_title_material:I = 0x7f0d000c

.field public static final abc_expanded_menu_layout:I = 0x7f0d000d

.field public static final abc_list_menu_item_checkbox:I = 0x7f0d000e

.field public static final abc_list_menu_item_icon:I = 0x7f0d000f

.field public static final abc_list_menu_item_layout:I = 0x7f0d0010

.field public static final abc_list_menu_item_radio:I = 0x7f0d0011

.field public static final abc_popup_menu_header_item_layout:I = 0x7f0d0012

.field public static final abc_popup_menu_item_layout:I = 0x7f0d0013

.field public static final abc_screen_content_include:I = 0x7f0d0014

.field public static final abc_screen_simple:I = 0x7f0d0015

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f0d0016

.field public static final abc_screen_toolbar:I = 0x7f0d0017

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f0d0018

.field public static final abc_search_view:I = 0x7f0d0019

.field public static final abc_select_dialog_material:I = 0x7f0d001a

.field public static final abc_tooltip:I = 0x7f0d001b

.field public static final custom_dialog:I = 0x7f0d01a9

.field public static final design_bottom_navigation_item:I = 0x7f0d01c1

.field public static final design_bottom_sheet_dialog:I = 0x7f0d01c2

.field public static final design_layout_snackbar:I = 0x7f0d01c3

.field public static final design_layout_snackbar_include:I = 0x7f0d01c4

.field public static final design_layout_tab_icon:I = 0x7f0d01c5

.field public static final design_layout_tab_text:I = 0x7f0d01c6

.field public static final design_menu_item_action_area:I = 0x7f0d01c7

.field public static final design_navigation_item:I = 0x7f0d01c8

.field public static final design_navigation_item_header:I = 0x7f0d01c9

.field public static final design_navigation_item_separator:I = 0x7f0d01ca

.field public static final design_navigation_item_subheader:I = 0x7f0d01cb

.field public static final design_navigation_menu:I = 0x7f0d01cc

.field public static final design_navigation_menu_item:I = 0x7f0d01cd

.field public static final design_text_input_end_icon:I = 0x7f0d01ce

.field public static final design_text_input_start_icon:I = 0x7f0d01cf

.field public static final mtrl_alert_dialog:I = 0x7f0d0361

.field public static final mtrl_alert_dialog_actions:I = 0x7f0d0362

.field public static final mtrl_alert_dialog_title:I = 0x7f0d0363

.field public static final mtrl_alert_select_dialog_item:I = 0x7f0d0364

.field public static final mtrl_alert_select_dialog_multichoice:I = 0x7f0d0365

.field public static final mtrl_alert_select_dialog_singlechoice:I = 0x7f0d0366

.field public static final mtrl_calendar_day:I = 0x7f0d0367

.field public static final mtrl_calendar_day_of_week:I = 0x7f0d0368

.field public static final mtrl_calendar_days_of_week:I = 0x7f0d0369

.field public static final mtrl_calendar_horizontal:I = 0x7f0d036a

.field public static final mtrl_calendar_month:I = 0x7f0d036b

.field public static final mtrl_calendar_month_labeled:I = 0x7f0d036c

.field public static final mtrl_calendar_month_navigation:I = 0x7f0d036d

.field public static final mtrl_calendar_months:I = 0x7f0d036e

.field public static final mtrl_calendar_vertical:I = 0x7f0d036f

.field public static final mtrl_calendar_year:I = 0x7f0d0370

.field public static final mtrl_layout_snackbar:I = 0x7f0d0371

.field public static final mtrl_layout_snackbar_include:I = 0x7f0d0372

.field public static final mtrl_picker_actions:I = 0x7f0d0373

.field public static final mtrl_picker_dialog:I = 0x7f0d0374

.field public static final mtrl_picker_fullscreen:I = 0x7f0d0375

.field public static final mtrl_picker_header_dialog:I = 0x7f0d0376

.field public static final mtrl_picker_header_fullscreen:I = 0x7f0d0377

.field public static final mtrl_picker_header_selection_text:I = 0x7f0d0378

.field public static final mtrl_picker_header_title_text:I = 0x7f0d0379

.field public static final mtrl_picker_header_toggle:I = 0x7f0d037a

.field public static final mtrl_picker_text_input_date:I = 0x7f0d037b

.field public static final mtrl_picker_text_input_date_range:I = 0x7f0d037c

.field public static final notification_action:I = 0x7f0d039b

.field public static final notification_action_tombstone:I = 0x7f0d039c

.field public static final notification_template_custom_big:I = 0x7f0d03aa

.field public static final notification_template_icon_group:I = 0x7f0d03ab

.field public static final notification_template_part_chronometer:I = 0x7f0d03af

.field public static final notification_template_part_time:I = 0x7f0d03b0

.field public static final select_dialog_item_material:I = 0x7f0d04b9

.field public static final select_dialog_multichoice_material:I = 0x7f0d04ba

.field public static final select_dialog_singlechoice_material:I = 0x7f0d04bb

.field public static final support_simple_spinner_dropdown_item:I = 0x7f0d050b

.field public static final test_action_chip:I = 0x7f0d0519

.field public static final test_design_checkbox:I = 0x7f0d051a

.field public static final test_reflow_chipgroup:I = 0x7f0d051b

.field public static final test_toolbar:I = 0x7f0d051c

.field public static final test_toolbar_custom_background:I = 0x7f0d051d

.field public static final test_toolbar_elevation:I = 0x7f0d051e

.field public static final test_toolbar_surface:I = 0x7f0d051f

.field public static final text_view_with_line_height_from_appearance:I = 0x7f0d0523

.field public static final text_view_with_line_height_from_layout:I = 0x7f0d0524

.field public static final text_view_with_line_height_from_style:I = 0x7f0d0525

.field public static final text_view_with_theme_line_height:I = 0x7f0d0526

.field public static final text_view_without_line_height:I = 0x7f0d0527


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
