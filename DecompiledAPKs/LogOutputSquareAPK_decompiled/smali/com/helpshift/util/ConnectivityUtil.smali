.class public Lcom/helpshift/util/ConnectivityUtil;
.super Ljava/lang/Object;
.source "ConnectivityUtil.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private final defaultBatchSize:I

.field private final maximumBatchSize:I


# direct methods
.method public constructor <init>(IILandroid/content/Context;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput p1, p0, Lcom/helpshift/util/ConnectivityUtil;->defaultBatchSize:I

    .line 16
    iput p2, p0, Lcom/helpshift/util/ConnectivityUtil;->maximumBatchSize:I

    .line 17
    iput-object p3, p0, Lcom/helpshift/util/ConnectivityUtil;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public getBatchSize()I
    .locals 4

    .line 21
    iget v0, p0, Lcom/helpshift/util/ConnectivityUtil;->defaultBatchSize:I

    .line 23
    iget-object v1, p0, Lcom/helpshift/util/ConnectivityUtil;->context:Landroid/content/Context;

    const-string v2, "connectivity"

    .line 24
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 25
    iget-object v2, p0, Lcom/helpshift/util/ConnectivityUtil;->context:Landroid/content/Context;

    const-string v3, "phone"

    .line 26
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 27
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 30
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    if-eq v1, v3, :cond_0

    goto :goto_0

    .line 32
    :cond_0
    iget v0, p0, Lcom/helpshift/util/ConnectivityUtil;->maximumBatchSize:I

    goto :goto_0

    .line 35
    :cond_1
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v3, :cond_3

    if-eq v1, v2, :cond_3

    const/16 v2, 0xd

    if-eq v1, v2, :cond_2

    const/16 v2, 0xf

    if-eq v1, v2, :cond_2

    goto :goto_0

    .line 38
    :cond_2
    iget v0, p0, Lcom/helpshift/util/ConnectivityUtil;->defaultBatchSize:I

    mul-int/lit8 v0, v0, 0x4

    goto :goto_0

    .line 42
    :cond_3
    iget v0, p0, Lcom/helpshift/util/ConnectivityUtil;->defaultBatchSize:I

    div-int/2addr v0, v2

    :cond_4
    :goto_0
    return v0
.end method
