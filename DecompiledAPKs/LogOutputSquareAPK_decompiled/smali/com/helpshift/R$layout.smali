.class public final Lcom/helpshift/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/helpshift/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f0d0000

.field public static final abc_action_bar_up_container:I = 0x7f0d0001

.field public static final abc_action_menu_item_layout:I = 0x7f0d0002

.field public static final abc_action_menu_layout:I = 0x7f0d0003

.field public static final abc_action_mode_bar:I = 0x7f0d0004

.field public static final abc_action_mode_close_item_material:I = 0x7f0d0005

.field public static final abc_activity_chooser_view:I = 0x7f0d0006

.field public static final abc_activity_chooser_view_list_item:I = 0x7f0d0007

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f0d0008

.field public static final abc_alert_dialog_material:I = 0x7f0d0009

.field public static final abc_alert_dialog_title_material:I = 0x7f0d000a

.field public static final abc_cascading_menu_item_layout:I = 0x7f0d000b

.field public static final abc_dialog_title_material:I = 0x7f0d000c

.field public static final abc_expanded_menu_layout:I = 0x7f0d000d

.field public static final abc_list_menu_item_checkbox:I = 0x7f0d000e

.field public static final abc_list_menu_item_icon:I = 0x7f0d000f

.field public static final abc_list_menu_item_layout:I = 0x7f0d0010

.field public static final abc_list_menu_item_radio:I = 0x7f0d0011

.field public static final abc_popup_menu_header_item_layout:I = 0x7f0d0012

.field public static final abc_popup_menu_item_layout:I = 0x7f0d0013

.field public static final abc_screen_content_include:I = 0x7f0d0014

.field public static final abc_screen_simple:I = 0x7f0d0015

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f0d0016

.field public static final abc_screen_toolbar:I = 0x7f0d0017

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f0d0018

.field public static final abc_search_view:I = 0x7f0d0019

.field public static final abc_select_dialog_material:I = 0x7f0d001a

.field public static final abc_tooltip:I = 0x7f0d001b

.field public static final design_bottom_navigation_item:I = 0x7f0d01c1

.field public static final design_bottom_sheet_dialog:I = 0x7f0d01c2

.field public static final design_layout_snackbar:I = 0x7f0d01c3

.field public static final design_layout_snackbar_include:I = 0x7f0d01c4

.field public static final design_layout_tab_icon:I = 0x7f0d01c5

.field public static final design_layout_tab_text:I = 0x7f0d01c6

.field public static final design_menu_item_action_area:I = 0x7f0d01c7

.field public static final design_navigation_item:I = 0x7f0d01c8

.field public static final design_navigation_item_header:I = 0x7f0d01c9

.field public static final design_navigation_item_separator:I = 0x7f0d01ca

.field public static final design_navigation_item_subheader:I = 0x7f0d01cb

.field public static final design_navigation_menu:I = 0x7f0d01cc

.field public static final design_navigation_menu_item:I = 0x7f0d01cd

.field public static final hs__action_download_layout:I = 0x7f0d028f

.field public static final hs__action_view_search:I = 0x7f0d0290

.field public static final hs__authentication_failure_fragment:I = 0x7f0d0291

.field public static final hs__badge_layout:I = 0x7f0d0292

.field public static final hs__bottomsheet_wrapper:I = 0x7f0d0293

.field public static final hs__collapsed_picker_header:I = 0x7f0d0294

.field public static final hs__conversation_fragment:I = 0x7f0d0295

.field public static final hs__conversation_layout:I = 0x7f0d0296

.field public static final hs__conversation_replyboxview:I = 0x7f0d0297

.field public static final hs__conversational_labelledreplyboxview:I = 0x7f0d0298

.field public static final hs__csat_dialog:I = 0x7f0d0299

.field public static final hs__csat_holder:I = 0x7f0d029a

.field public static final hs__csat_view:I = 0x7f0d029b

.field public static final hs__dynamic_form_fragment:I = 0x7f0d029c

.field public static final hs__expanded_picker_header:I = 0x7f0d029d

.field public static final hs__faq_flow_fragment:I = 0x7f0d029e

.field public static final hs__faq_fragment:I = 0x7f0d029f

.field public static final hs__history_loading_view_layout:I = 0x7f0d02a0

.field public static final hs__issue_archival_message_view_holder:I = 0x7f0d02a1

.field public static final hs__messages_list_footer:I = 0x7f0d02a2

.field public static final hs__msg_admin_suggesstion_item:I = 0x7f0d02a3

.field public static final hs__msg_admin_suggesstions_container:I = 0x7f0d02a4

.field public static final hs__msg_agent_typing:I = 0x7f0d02a5

.field public static final hs__msg_attachment_generic:I = 0x7f0d02a6

.field public static final hs__msg_attachment_image:I = 0x7f0d02a7

.field public static final hs__msg_publish_id_layout:I = 0x7f0d02a8

.field public static final hs__msg_request_screenshot:I = 0x7f0d02a9

.field public static final hs__msg_review_request:I = 0x7f0d02aa

.field public static final hs__msg_screenshot_status:I = 0x7f0d02ab

.field public static final hs__msg_system_conversation_redacted_layout:I = 0x7f0d02ac

.field public static final hs__msg_system_divider_layout:I = 0x7f0d02ad

.field public static final hs__msg_system_layout:I = 0x7f0d02ae

.field public static final hs__msg_txt_admin:I = 0x7f0d02af

.field public static final hs__msg_txt_user:I = 0x7f0d02b0

.field public static final hs__msg_user_selectable_option:I = 0x7f0d02b1

.field public static final hs__msg_user_selectable_options_container:I = 0x7f0d02b2

.field public static final hs__new_conversation_fragment:I = 0x7f0d02b3

.field public static final hs__new_conversation_layout:I = 0x7f0d02b4

.field public static final hs__parent_activity:I = 0x7f0d02b5

.field public static final hs__picker_layout:I = 0x7f0d02b6

.field public static final hs__picker_option:I = 0x7f0d02b7

.field public static final hs__question_list_fragment:I = 0x7f0d02b8

.field public static final hs__screenshot_preview_fragment:I = 0x7f0d02b9

.field public static final hs__screenshot_preview_layout:I = 0x7f0d02ba

.field public static final hs__search_fragment:I = 0x7f0d02bb

.field public static final hs__search_list_footer:I = 0x7f0d02bc

.field public static final hs__search_result_footer:I = 0x7f0d02bd

.field public static final hs__search_result_fragment:I = 0x7f0d02be

.field public static final hs__search_result_header:I = 0x7f0d02bf

.field public static final hs__search_result_layout:I = 0x7f0d02c0

.field public static final hs__section_divider:I = 0x7f0d02c1

.field public static final hs__section_list_fragment:I = 0x7f0d02c2

.field public static final hs__section_pager_fragment:I = 0x7f0d02c3

.field public static final hs__simple_list_item_1:I = 0x7f0d02c4

.field public static final hs__single_question_fragment:I = 0x7f0d02c5

.field public static final hs__single_question_layout_with_cardview:I = 0x7f0d02c6

.field public static final hs__support_fragment:I = 0x7f0d02c7

.field public static final hs__user_setup_fragment:I = 0x7f0d02c8

.field public static final hs_simple_recycler_view_item:I = 0x7f0d02c9

.field public static final mtrl_layout_snackbar:I = 0x7f0d0371

.field public static final mtrl_layout_snackbar_include:I = 0x7f0d0372

.field public static final notification_action:I = 0x7f0d039b

.field public static final notification_action_tombstone:I = 0x7f0d039c

.field public static final notification_template_custom_big:I = 0x7f0d03aa

.field public static final notification_template_icon_group:I = 0x7f0d03ab

.field public static final notification_template_part_chronometer:I = 0x7f0d03af

.field public static final notification_template_part_time:I = 0x7f0d03b0

.field public static final select_dialog_item_material:I = 0x7f0d04b9

.field public static final select_dialog_multichoice_material:I = 0x7f0d04ba

.field public static final select_dialog_singlechoice_material:I = 0x7f0d04bb

.field public static final support_simple_spinner_dropdown_item:I = 0x7f0d050b


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
