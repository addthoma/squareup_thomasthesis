.class public interface abstract Lcom/helpshift/cif/dao/CustomIssueFieldDAO;
.super Ljava/lang/Object;
.source "CustomIssueFieldDAO.java"


# virtual methods
.method public abstract getCustomIssueFields()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/helpshift/cif/dto/CustomIssueFieldDTO;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setCustomIssueFields(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/helpshift/cif/dto/CustomIssueFieldDTO;",
            ">;)V"
        }
    .end annotation
.end method
