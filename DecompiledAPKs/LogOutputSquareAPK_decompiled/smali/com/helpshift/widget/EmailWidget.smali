.class public Lcom/helpshift/widget/EmailWidget;
.super Lcom/helpshift/widget/TextWidget;
.source "EmailWidget.java"


# instance fields
.field private isRequired:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/helpshift/widget/TextWidget;-><init>()V

    return-void
.end method


# virtual methods
.method public isRequired()Z
    .locals 1

    .line 14
    iget-boolean v0, p0, Lcom/helpshift/widget/EmailWidget;->isRequired:Z

    return v0
.end method

.method public setRequired(Z)V
    .locals 0

    .line 18
    iput-boolean p1, p0, Lcom/helpshift/widget/EmailWidget;->isRequired:Z

    return-void
.end method

.method public validateText()V
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/helpshift/widget/EmailWidget;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 25
    invoke-virtual {p0}, Lcom/helpshift/widget/EmailWidget;->isRequired()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    sget-object v0, Lcom/helpshift/widget/TextWidget$TextWidgetError;->EMPTY:Lcom/helpshift/widget/TextWidget$TextWidgetError;

    invoke-virtual {p0, v0}, Lcom/helpshift/widget/EmailWidget;->setError(Lcom/helpshift/widget/TextWidget$TextWidgetError;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 29
    invoke-virtual {p0, v0}, Lcom/helpshift/widget/EmailWidget;->setError(Lcom/helpshift/widget/TextWidget$TextWidgetError;)V

    goto :goto_0

    .line 32
    :cond_1
    invoke-virtual {p0}, Lcom/helpshift/widget/EmailWidget;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/helpshift/util/HSPattern;->isValidEmail(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 33
    sget-object v0, Lcom/helpshift/widget/TextWidget$TextWidgetError;->INVALID_EMAIL:Lcom/helpshift/widget/TextWidget$TextWidgetError;

    invoke-virtual {p0, v0}, Lcom/helpshift/widget/EmailWidget;->setError(Lcom/helpshift/widget/TextWidget$TextWidgetError;)V

    :cond_2
    :goto_0
    return-void
.end method
