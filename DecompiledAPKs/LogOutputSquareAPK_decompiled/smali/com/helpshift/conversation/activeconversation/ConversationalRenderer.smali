.class public interface abstract Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;
.super Ljava/lang/Object;
.source "ConversationalRenderer.java"

# interfaces
.implements Lcom/helpshift/conversation/activeconversation/ConversationRenderer;


# virtual methods
.method public abstract hideListPicker(Z)V
.end method

.method public abstract hideNetworkErrorFooter()V
.end method

.method public abstract hidePickerClearButton()V
.end method

.method public abstract hideReplyValidationFailedError()V
.end method

.method public abstract hideSkipButton()V
.end method

.method public abstract onBackPressed()Z
.end method

.method public abstract onFocusChanged(Z)V
.end method

.method public abstract showEmptyListPickerView()V
.end method

.method public abstract showInput(Lcom/helpshift/conversation/activeconversation/message/input/Input;)V
.end method

.method public abstract showListPicker(Ljava/util/List;Ljava/lang/String;ZLjava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/viewmodel/OptionUIModel;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract showNetworkErrorFooter(I)V
.end method

.method public abstract showPickerClearButton()V
.end method

.method public abstract showReplyValidationFailedError(I)V
.end method

.method public abstract showSkipButton()V
.end method

.method public abstract updateListPickerOptions(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/viewmodel/OptionUIModel;",
            ">;)V"
        }
    .end annotation
.end method
