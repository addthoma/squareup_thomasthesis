.class public Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;
.super Lcom/helpshift/conversation/activeconversation/message/MessageDM;
.source "RequestAppReviewMessageDM.java"


# instance fields
.field public isAnswered:Z

.field public isReviewButtonClickable:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V
    .locals 8

    .line 23
    sget-object v7, Lcom/helpshift/conversation/activeconversation/message/MessageType;->REQUESTED_APP_REVIEW:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-wide v3, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v7}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZLcom/helpshift/conversation/activeconversation/message/MessageType;)V

    .line 24
    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->serverId:Ljava/lang/String;

    .line 25
    iput-boolean p7, p0, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->isAnswered:Z

    const/4 p1, 0x1

    .line 26
    iput-boolean p1, p0, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->isReviewButtonClickable:Z

    return-void
.end method


# virtual methods
.method public handleAcceptedReviewSuccess(Lcom/helpshift/common/platform/Platform;)V
    .locals 1

    const/4 v0, 0x0

    .line 65
    iput-boolean v0, p0, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->isReviewButtonClickable:Z

    const/4 v0, 0x1

    .line 68
    iput-boolean v0, p0, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->isAnswered:Z

    .line 69
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->notifyUpdated()V

    .line 71
    invoke-interface {p1}, Lcom/helpshift/common/platform/Platform;->getConversationDAO()Lcom/helpshift/conversation/dao/ConversationDAO;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/helpshift/conversation/dao/ConversationDAO;->insertOrUpdateMessage(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    return-void
.end method

.method public handleRequestReviewClick(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;
    .locals 9

    .line 36
    iget-boolean v0, p0, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->isAnswered:Z

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const/4 v0, 0x0

    .line 40
    invoke-virtual {p0, v0}, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->setIsReviewButtonClickable(Z)V

    .line 42
    invoke-static {p2}, Lcom/helpshift/common/util/HSDateFormatSpec;->getCurrentAdjustedTimeForStorage(Lcom/helpshift/common/platform/Platform;)Ljava/lang/String;

    move-result-object v3

    .line 43
    invoke-static {v3}, Lcom/helpshift/common/util/HSDateFormatSpec;->convertToEpochTime(Ljava/lang/String;)J

    move-result-wide v4

    .line 44
    new-instance v0, Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;

    iget-object v7, p0, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->serverId:Ljava/lang/String;

    const/4 v8, 0x1

    const-string v2, "Accepted review request"

    const-string v6, "mobile"

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;I)V

    .line 47
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->conversationLocalId:Ljava/lang/Long;

    iput-object v1, v0, Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;->conversationLocalId:Ljava/lang/Long;

    .line 48
    invoke-virtual {v0, p1, p2}, Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 50
    invoke-interface {p2}, Lcom/helpshift/common/platform/Platform;->getConversationDAO()Lcom/helpshift/conversation/dao/ConversationDAO;

    move-result-object p2

    invoke-interface {p2, v0}, Lcom/helpshift/conversation/dao/ConversationDAO;->insertOrUpdateMessage(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 53
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    const-string v1, "type"

    const-string v2, "conversation"

    .line 54
    invoke-interface {p2, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getAnalyticsEventDM()Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    move-result-object v1

    sget-object v2, Lcom/helpshift/analytics/AnalyticsEventType;->REVIEWED_APP:Lcom/helpshift/analytics/AnalyticsEventType;

    invoke-virtual {v1, v2, p2}, Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;->pushEvent(Lcom/helpshift/analytics/AnalyticsEventType;Ljava/util/Map;)V

    .line 58
    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getDelegate()Lcom/helpshift/delegate/UIThreadDelegateDecorator;

    move-result-object p1

    const-string p2, "User reviewed the app"

    invoke-virtual {p1, p2}, Lcom/helpshift/delegate/UIThreadDelegateDecorator;->userRepliedToConversation(Ljava/lang/String;)V

    return-object v0
.end method

.method public isUISupportedMessage()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public merge(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 1

    .line 76
    invoke-super {p0, p1}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->merge(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 77
    instance-of v0, p1, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;

    if-eqz v0, :cond_0

    .line 78
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;

    .line 79
    iget-boolean p1, p1, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->isAnswered:Z

    iput-boolean p1, p0, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->isAnswered:Z

    :cond_0
    return-void
.end method

.method public setIsReviewButtonClickable(Z)V
    .locals 0

    .line 30
    iput-boolean p1, p0, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->isReviewButtonClickable:Z

    .line 31
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->notifyUpdated()V

    return-void
.end method
