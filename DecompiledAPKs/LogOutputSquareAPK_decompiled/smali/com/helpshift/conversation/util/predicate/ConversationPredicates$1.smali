.class final Lcom/helpshift/conversation/util/predicate/ConversationPredicates$1;
.super Ljava/lang/Object;
.source "ConversationPredicates.java"

# interfaces
.implements Lcom/helpshift/util/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/conversation/util/predicate/ConversationPredicates;->newSyncedConversationPredicate()Lcom/helpshift/util/Predicate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/helpshift/util/Predicate<",
        "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public matches(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Z
    .locals 0

    .line 15
    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isSynced()Z

    move-result p1

    return p1
.end method

.method public bridge synthetic matches(Ljava/lang/Object;)Z
    .locals 0

    .line 12
    check-cast p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/util/predicate/ConversationPredicates$1;->matches(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Z

    move-result p1

    return p1
.end method
