.class public Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;
.super Ljava/lang/Object;
.source "ConversationInboxDM.java"

# interfaces
.implements Lcom/helpshift/common/AutoRetriableDM;
.implements Lcom/helpshift/account/domainmodel/IUserSyncExecutor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$CreateConversationStateHolder;,
        Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;
    }
.end annotation


# static fields
.field private static final ACTIVE_ISSUE_NOTIFICATION_COUNT_TIMEOUT:J = 0xea60L

.field private static final CREATE_ISSUE_ROUTE:Ljava/lang/String; = "/issues/"

.field private static final CREATE_ISSUE_UNIQUE_MAPPING_KEY:Ljava/lang/String; = "issue_default_unique_key"

.field private static final CREATE_PRE_ISSUE_ROUTE:Ljava/lang/String; = "/preissues/"

.field private static final CREATE_PRE_ISSUE_UNIQUE_MAPPING_KEY:Ljava/lang/String; = "preissue_default_unique_key"

.field private static final INACTIVE_ISSUES_NOTIFICATION_COUNT_TIMEOUT:J = 0x493e0L

.field public static final MESSAGES_PAGE_SIZE:J = 0x64L

.field public static final PRE_ISSUE_RESET_TIMEOUT:J = 0x5265c00L

.field private static final TAG:Ljava/lang/String; = "Helpshift_ConvInboxDM"

.field static final fetchConversationUpdatesLock:Ljava/lang/Object;


# instance fields
.field private aliveConversationDMs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/helpshift/conversation/activeconversation/ViewableConversation;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

.field private final conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

.field private final conversationInboxPoller:Lcom/helpshift/conversation/ConversationInboxPoller;

.field private conversationViewState:I

.field final domain:Lcom/helpshift/common/domain/Domain;

.field private final faqSearchDM:Lcom/helpshift/faq/domainmodel/FAQSearchDM;

.field public fetchConversationUpdatesListenerReference:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lcom/helpshift/common/FetchDataFromThread<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private inAppNotificationMessageCountMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field inProgressPreIssueCreators:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Lcom/helpshift/common/domain/One;",
            ">;"
        }
    .end annotation
.end field

.field private isCreateConversationInProgress:Z

.field private lastAliveConversationLocalId:J

.field private lastNotifCountFetchTime:J

.field private final liveUpdateDM:Lcom/helpshift/conversation/activeconversation/LiveUpdateDM;

.field final platform:Lcom/helpshift/common/platform/Platform;

.field private remoteConversationLoader:Lcom/helpshift/conversation/loaders/RemoteConversationLoader;

.field private final sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

.field private shouldDropCustomMetadata:Z

.field private startNewConversationListenerRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;",
            ">;"
        }
    .end annotation
.end field

.field private userCanReadMessages:Z

.field final userDM:Lcom/helpshift/account/domainmodel/UserDM;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 119
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->fetchConversationUpdatesLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V
    .locals 4

    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 116
    iput-wide v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->lastNotifCountFetchTime:J

    const/4 v2, 0x0

    .line 134
    iput-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->fetchConversationUpdatesListenerReference:Ljava/util/concurrent/atomic/AtomicReference;

    .line 138
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->inProgressPreIssueCreators:Ljava/util/HashMap;

    const/4 v2, -0x1

    .line 143
    iput v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationViewState:I

    .line 144
    new-instance v2, Ljava/util/WeakHashMap;

    invoke-direct {v2}, Ljava/util/WeakHashMap;-><init>()V

    .line 145
    invoke-static {v2}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    iput-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->aliveConversationDMs:Ljava/util/Map;

    .line 146
    iput-wide v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->lastAliveConversationLocalId:J

    .line 149
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->inAppNotificationMessageCountMap:Ljava/util/Map;

    .line 152
    iput-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    .line 153
    iput-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    .line 154
    iput-object p3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    .line 155
    invoke-interface {p1}, Lcom/helpshift/common/platform/Platform;->getConversationInboxDAO()Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    .line 156
    invoke-interface {p1}, Lcom/helpshift/common/platform/Platform;->getConversationDAO()Lcom/helpshift/conversation/dao/ConversationDAO;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    .line 157
    invoke-interface {p1}, Lcom/helpshift/common/platform/Platform;->getFAQSearchDM()Lcom/helpshift/faq/domainmodel/FAQSearchDM;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->faqSearchDM:Lcom/helpshift/faq/domainmodel/FAQSearchDM;

    .line 158
    invoke-virtual {p2}, Lcom/helpshift/common/domain/Domain;->getSDKConfigurationDM()Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    .line 159
    new-instance v0, Lcom/helpshift/conversation/ConversationInboxPoller;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getPoller()Lcom/helpshift/common/domain/Poller;

    move-result-object v2

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-direct {v0, p3, v1, v2, v3}, Lcom/helpshift/conversation/ConversationInboxPoller;-><init>(Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;Lcom/helpshift/common/domain/Poller;Lcom/helpshift/conversation/dao/ConversationDAO;)V

    iput-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxPoller:Lcom/helpshift/conversation/ConversationInboxPoller;

    .line 160
    new-instance v0, Lcom/helpshift/conversation/activeconversation/LiveUpdateDM;

    invoke-direct {v0, p2, p1}, Lcom/helpshift/conversation/activeconversation/LiveUpdateDM;-><init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    iput-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->liveUpdateDM:Lcom/helpshift/conversation/activeconversation/LiveUpdateDM;

    .line 161
    new-instance v0, Lcom/helpshift/conversation/loaders/RemoteConversationLoader;

    invoke-direct {v0, p1, p2, p3}, Lcom/helpshift/conversation/loaders/RemoteConversationLoader;-><init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    iput-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->remoteConversationLoader:Lcom/helpshift/conversation/loaders/RemoteConversationLoader;

    return-void
.end method

.method static synthetic access$000(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Ljava/lang/Long;)Lcom/helpshift/conversation/activeconversation/ViewableConversation;
    .locals 0

    .line 95
    invoke-direct {p0, p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getInMemoryConversationDM(Ljava/lang/Long;)Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    move-result-object p0

    return-object p0
.end method

.method private buildForwardPollerNetwork()Lcom/helpshift/common/domain/network/Network;
    .locals 4

    .line 689
    new-instance v0, Lcom/helpshift/common/domain/network/POSTNetwork;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    const-string v3, "/conversations/updates/"

    invoke-direct {v0, v3, v1, v2}, Lcom/helpshift/common/domain/network/POSTNetwork;-><init>(Ljava/lang/String;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 690
    new-instance v1, Lcom/helpshift/common/domain/network/AuthenticationFailureNetwork;

    invoke-direct {v1, v0}, Lcom/helpshift/common/domain/network/AuthenticationFailureNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;)V

    .line 691
    new-instance v0, Lcom/helpshift/common/domain/network/FailedAPICallNetworkDecorator;

    invoke-direct {v0, v1}, Lcom/helpshift/common/domain/network/FailedAPICallNetworkDecorator;-><init>(Lcom/helpshift/common/domain/network/Network;)V

    .line 692
    new-instance v1, Lcom/helpshift/common/domain/network/TSCorrectedNetwork;

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v1, v0, v2}, Lcom/helpshift/common/domain/network/TSCorrectedNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;Lcom/helpshift/common/platform/Platform;)V

    .line 693
    new-instance v0, Lcom/helpshift/common/domain/network/GuardOKNetwork;

    invoke-direct {v0, v1}, Lcom/helpshift/common/domain/network/GuardOKNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;)V

    return-object v0
.end method

.method private buildForwardPollerRequestData(Ljava/lang/String;)Lcom/helpshift/common/platform/network/RequestData;
    .locals 2

    .line 698
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-static {v0}, Lcom/helpshift/common/domain/network/NetworkDataRequestUtil;->getUserRequestData(Lcom/helpshift/account/domainmodel/UserDM;)Ljava/util/HashMap;

    move-result-object v0

    .line 700
    invoke-static {p1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "cursor"

    .line 701
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 704
    :cond_0
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getLastViewableSyncedConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 707
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-static {v1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 708
    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    const-string v1, "issue_id"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 710
    :cond_1
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-static {v1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 711
    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    const-string v1, "preissue_id"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 714
    :cond_2
    :goto_0
    iget-boolean p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userCanReadMessages:Z

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p1

    const-string v1, "ucrm"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 715
    new-instance p1, Lcom/helpshift/common/platform/network/RequestData;

    invoke-direct {p1, v0}, Lcom/helpshift/common/platform/network/RequestData;-><init>(Ljava/util/Map;)V

    return-object p1
.end method

.method private canShowNotificationForConversation(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Z
    .locals 6

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    .line 938
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    .line 939
    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-wide v3, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->userLocalId:J

    cmp-long v5, v1, v3

    if-nez v5, :cond_3

    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localUUID:Ljava/lang/String;

    .line 940
    invoke-static {v1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 944
    :cond_0
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getAliveConversationDM()Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 945
    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->isVisibleOnUI()Z

    move-result v2

    if-eqz v2, :cond_1

    return v0

    :cond_1
    const/4 v0, 0x1

    if-nez v1, :cond_2

    .line 955
    invoke-virtual {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getActiveConversationFromStorage()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v1

    goto :goto_0

    .line 958
    :cond_2
    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_3

    .line 964
    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localUUID:Ljava/lang/String;

    iget-object v0, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localUUID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_3
    :goto_1
    return v0
.end method

.method private checkAndDropCustomMeta(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V
    .locals 1

    .line 390
    iget-boolean v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->shouldDropCustomMetadata:Z

    if-eqz v0, :cond_0

    .line 391
    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->dropCustomMetaData()V

    :cond_0
    return-void
.end method

.method private checkAndGenerateNotification(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;)V"
        }
    .end annotation

    .line 865
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 866
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->shouldShowInAppNotification(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 867
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getMessageCountForShowingInAppNotification(Lcom/helpshift/conversation/activeconversation/ConversationDM;)I

    move-result v1

    .line 868
    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v0, v2, v3, v4}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    .line 869
    invoke-direct {p0, v0, v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->showInAppNotification(Lcom/helpshift/conversation/activeconversation/ConversationDM;I)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private checkAndTryToUploadImage(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/dto/ImagePickerFile;)V
    .locals 1

    if-eqz p2, :cond_0

    .line 396
    iget-object v0, p2, Lcom/helpshift/conversation/dto/ImagePickerFile;->filePath:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 398
    :try_start_0
    invoke-virtual {p1, p2, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendScreenshot(Lcom/helpshift/conversation/dto/ImagePickerFile;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 405
    :catch_0
    invoke-virtual {p0, v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->saveImageAttachmentDraft(Lcom/helpshift/conversation/dto/ImagePickerFile;)V

    :cond_0
    return-void
.end method

.method private checkForReOpen(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;)V"
        }
    .end annotation

    .line 987
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getActiveConversationFromUIOrStorage()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 991
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v3

    if-nez v3, :cond_0

    .line 992
    iget-object v1, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    .line 999
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getAliveConversationDM()Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    move-result-object v0

    .line 1000
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 1001
    iget-object v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v5, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v6, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v3, v4, v5, v6}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    if-eqz v0, :cond_3

    .line 1004
    invoke-virtual {v0, v3}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->isActiveConversationEqual(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1005
    iget v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationViewState:I

    invoke-virtual {v0, v4, v1, v2}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->checkForReopen(ILjava/lang/String;Z)Z

    move-result v4

    goto :goto_2

    .line 1010
    :cond_3
    iget v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationViewState:I

    .line 1011
    invoke-virtual {v3, v4, v1, v2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->checkForReOpen(ILjava/lang/String;Z)Z

    move-result v4

    :goto_2
    if-eqz v4, :cond_2

    .line 1017
    invoke-direct {p0, v3}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->shouldShowInAppNotification(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1018
    invoke-direct {p0, v3}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getMessageCountForShowingInAppNotification(Lcom/helpshift/conversation/activeconversation/ConversationDM;)I

    move-result v4

    .line 1019
    invoke-direct {p0, v3, v4}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->showInAppNotification(Lcom/helpshift/conversation/activeconversation/ConversationDM;I)V

    goto :goto_1

    :cond_4
    return-void
.end method

.method private clearInAppNotificationCountCache()V
    .locals 1

    .line 901
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->inAppNotificationMessageCountMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method private clearRequestIdForPendingCreateConversationCall(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;)V"
        }
    .end annotation

    .line 833
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v0}, Lcom/helpshift/common/platform/Platform;->getNetworkRequestDAO()Lcom/helpshift/common/platform/network/NetworkRequestDAO;

    move-result-object v0

    const-string v1, "issue_default_unique_key"

    const-string v2, "/issues/"

    .line 834
    invoke-interface {v0, v2, v1}, Lcom/helpshift/common/platform/network/NetworkRequestDAO;->getPendingRequestId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 836
    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v3}, Lcom/helpshift/common/platform/Platform;->getNetworkRequestDAO()Lcom/helpshift/common/platform/network/NetworkRequestDAO;

    move-result-object v3

    const-string v4, "preissue_default_unique_key"

    const-string v5, "/preissues/"

    .line 837
    invoke-interface {v3, v5, v4}, Lcom/helpshift/common/platform/network/NetworkRequestDAO;->getPendingRequestId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v0, :cond_0

    if-eqz v3, :cond_3

    .line 840
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 841
    iget-object v7, v6, Lcom/helpshift/conversation/activeconversation/ConversationDM;->createdRequestId:Ljava/lang/String;

    if-eqz v7, :cond_1

    .line 842
    iget-object v7, v6, Lcom/helpshift/conversation/activeconversation/ConversationDM;->createdRequestId:Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 843
    iget-object v6, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v6}, Lcom/helpshift/common/platform/Platform;->getNetworkRequestDAO()Lcom/helpshift/common/platform/network/NetworkRequestDAO;

    move-result-object v6

    invoke-interface {v6, v2, v1}, Lcom/helpshift/common/platform/network/NetworkRequestDAO;->deletePendingRequestId(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 846
    :cond_2
    iget-object v6, v6, Lcom/helpshift/conversation/activeconversation/ConversationDM;->createdRequestId:Ljava/lang/String;

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 847
    iget-object v6, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v6}, Lcom/helpshift/common/platform/Platform;->getNetworkRequestDAO()Lcom/helpshift/common/platform/network/NetworkRequestDAO;

    move-result-object v6

    invoke-interface {v6, v5, v4}, Lcom/helpshift/common/platform/network/NetworkRequestDAO;->deletePendingRequestId(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method private deleteConversationsAndMessages()V
    .locals 7

    .line 185
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 187
    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {v2, v0, v1}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationsWithoutMessages(J)Ljava/util/List;

    move-result-object v2

    .line 188
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 189
    iget-object v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v5, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v6, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v3, v4, v5, v6}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    .line 190
    invoke-virtual {v3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->deleteCachedScreenshotFiles()V

    goto :goto_0

    .line 193
    :cond_0
    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {v2, v0, v1}, Lcom/helpshift/conversation/dao/ConversationDAO;->deleteConversations(J)V

    return-void
.end method

.method private fetchConversationHistory()V
    .locals 2

    .line 1872
    sget-object v0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->fetchConversationUpdatesLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1873
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->remoteConversationLoader:Lcom/helpshift/conversation/loaders/RemoteConversationLoader;

    invoke-virtual {v1}, Lcom/helpshift/conversation/loaders/RemoteConversationLoader;->loadMoreMessages()Z

    .line 1874
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private fetchConversationUpdatesInternal(Ljava/lang/String;)Lcom/helpshift/conversation/dto/ConversationInbox;
    .locals 4

    .line 720
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->buildForwardPollerNetwork()Lcom/helpshift/common/domain/network/Network;

    move-result-object v0

    .line 721
    invoke-direct {p0, p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->buildForwardPollerRequestData(Ljava/lang/String;)Lcom/helpshift/common/platform/network/RequestData;

    move-result-object p1

    .line 723
    :try_start_0
    invoke-interface {v0, p1}, Lcom/helpshift/common/domain/network/Network;->makeRequest(Lcom/helpshift/common/platform/network/RequestData;)Lcom/helpshift/common/platform/network/Response;

    move-result-object v0

    .line 724
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v1}, Lcom/helpshift/common/platform/Platform;->getResponseParser()Lcom/helpshift/common/platform/network/ResponseParser;

    move-result-object v1

    .line 725
    iget-object v0, v0, Lcom/helpshift/common/platform/network/Response;->responseString:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/helpshift/common/platform/network/ResponseParser;->parseConversationInbox(Ljava/lang/String;)Lcom/helpshift/conversation/dto/ConversationInbox;

    move-result-object v0
    :try_end_0
    .catch Lcom/helpshift/common/exception/RootAPIException; {:try_start_0 .. :try_end_0} :catch_0

    .line 741
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v1}, Lcom/helpshift/common/domain/Domain;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object v1

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    iget-boolean v3, v0, Lcom/helpshift/conversation/dto/ConversationInbox;->issueExists:Z

    invoke-virtual {v1, v2, v3}, Lcom/helpshift/account/domainmodel/UserManagerDM;->updateIssueExists(Lcom/helpshift/account/domainmodel/UserDM;Z)V

    .line 744
    iget-object p1, p1, Lcom/helpshift/common/platform/network/RequestData;->body:Ljava/util/Map;

    const-string v1, "cursor"

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, v0, Lcom/helpshift/conversation/dto/ConversationInbox;->hasOlderMessages:Ljava/lang/Boolean;

    if-eqz p1, :cond_0

    .line 746
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v3, v0, Lcom/helpshift/conversation/dto/ConversationInbox;->hasOlderMessages:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-interface {p1, v1, v2, v3}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->saveHasOlderMessages(JZ)V

    .line 750
    :cond_0
    iget-object p1, v0, Lcom/helpshift/conversation/dto/ConversationInbox;->conversations:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->merge(Ljava/util/List;)V

    .line 755
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v3, v0, Lcom/helpshift/conversation/dto/ConversationInbox;->cursor:Ljava/lang/String;

    invoke-interface {p1, v1, v2, v3}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->saveConversationInboxTimestamp(JLjava/lang/String;)V

    return-object v0

    :catch_0
    move-exception p1

    .line 728
    iget-object v0, p1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object v1, Lcom/helpshift/common/exception/NetworkException;->INVALID_AUTH_TOKEN:Lcom/helpshift/common/exception/NetworkException;

    if-eq v0, v1, :cond_1

    iget-object v0, p1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object v1, Lcom/helpshift/common/exception/NetworkException;->AUTH_TOKEN_NOT_PROVIDED:Lcom/helpshift/common/exception/NetworkException;

    if-eq v0, v1, :cond_1

    .line 732
    iget-object v0, p1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    instance-of v0, v0, Lcom/helpshift/common/exception/NetworkException;

    if-eqz v0, :cond_2

    .line 733
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getAliveConversationDM()Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 734
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->isVisibleOnUI()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 735
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getConversationVMCallback()Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;->onConversationInboxPollFailure()V

    goto :goto_0

    .line 730
    :cond_1
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getAuthenticationFailureDM()Lcom/helpshift/account/AuthenticationFailureDM;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    iget-object v2, p1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/account/AuthenticationFailureDM;->notifyAuthenticationFailure(Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/common/exception/ExceptionType;)V

    .line 738
    :cond_2
    :goto_0
    throw p1
.end method

.method private generateMessageCursors()Ljava/lang/String;
    .locals 6

    .line 1215
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1217
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1218
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1220
    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v4}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationsWithoutMessages(J)Ljava/util/List;

    move-result-object v3

    .line 1223
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 1224
    invoke-virtual {v4}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-static {v5}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1225
    iget-object v5, v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    iget-object v4, v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageCursor:Ljava/lang/String;

    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1228
    :cond_0
    iget-object v5, v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    iget-object v4, v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageCursor:Ljava/lang/String;

    invoke-interface {v1, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1232
    :cond_1
    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v3}, Lcom/helpshift/common/platform/Platform;->getJsonifier()Lcom/helpshift/common/platform/Jsonifier;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/helpshift/common/platform/Jsonifier;->jsonifyToObject(Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v1

    const-string v3, "issues"

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1233
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v1}, Lcom/helpshift/common/platform/Platform;->getJsonifier()Lcom/helpshift/common/platform/Jsonifier;

    move-result-object v1

    invoke-interface {v1, v2}, Lcom/helpshift/common/platform/Jsonifier;->jsonifyToObject(Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "preissues"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1235
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v1}, Lcom/helpshift/common/platform/Platform;->getJsonifier()Lcom/helpshift/common/platform/Jsonifier;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/helpshift/common/platform/Jsonifier;->jsonify(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getActiveConversationFromUIOrStorage()Lcom/helpshift/conversation/activeconversation/ConversationDM;
    .locals 4

    .line 1612
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getAliveConversationDM()Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1614
    invoke-virtual {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getActiveConversationFromStorage()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 1618
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v0, v1, v2, v3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    .line 1622
    :cond_1
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    return-object v0
.end method

.method private getAliveConversationDM()Lcom/helpshift/conversation/activeconversation/ViewableConversation;
    .locals 2

    .line 861
    iget-wide v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->lastAliveConversationLocalId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getInMemoryConversationDM(Ljava/lang/Long;)Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    move-result-object v0

    return-object v0
.end method

.method private getCampaignDID()Ljava/lang/String;
    .locals 1

    .line 663
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v0}, Lcom/helpshift/common/platform/Platform;->getCampaignModuleAPIs()Lcom/helpshift/providers/ICampaignsModuleAPIs;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 668
    :cond_0
    invoke-interface {v0}, Lcom/helpshift/providers/ICampaignsModuleAPIs;->getDeviceIdentifier()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getCampaignUID()Ljava/lang/String;
    .locals 1

    .line 654
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v0}, Lcom/helpshift/common/platform/Platform;->getCampaignModuleAPIs()Lcom/helpshift/providers/ICampaignsModuleAPIs;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 659
    :cond_0
    invoke-interface {v0}, Lcom/helpshift/providers/ICampaignsModuleAPIs;->getUserIdentifier()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getInAppNotificationCountCache(Ljava/lang/String;)I
    .locals 1

    .line 889
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->inAppNotificationMessageCountMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-nez p1, :cond_0

    const/4 p1, -0x1

    return p1

    .line 893
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1
.end method

.method private getInMemoryConversationDM(Ljava/lang/Long;)Lcom/helpshift/conversation/activeconversation/ViewableConversation;
    .locals 4

    .line 1292
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->aliveConversationDMs:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1293
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2, p1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1294
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    return-object p1

    .line 1296
    :cond_1
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    sget-object v3, Lcom/helpshift/conversation/activeconversation/ConversationHistoryDM;->CONVERSATION_HISTORY_DM_IDENTIFIER:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1297
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    .line 1298
    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v2

    iget-object v2, v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {p1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method private getLastViewableSyncedConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;
    .locals 2

    .line 1634
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getAliveConversationDM()Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1641
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 1642
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isSynced()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 1647
    :cond_0
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getLastViewableSyncedConversationFromStorage()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    goto :goto_0

    .line 1651
    :cond_1
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getLastViewableSyncedConversationFromStorage()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private getLastViewableSyncedConversationFromStorage()Lcom/helpshift/conversation/activeconversation/ConversationDM;
    .locals 4

    .line 1669
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationsWithoutMessages(J)Ljava/util/List;

    move-result-object v0

    .line 1670
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    return-object v2

    .line 1675
    :cond_0
    invoke-static {}, Lcom/helpshift/conversation/util/predicate/ConversationPredicates;->newSyncedConversationPredicate()Lcom/helpshift/util/Predicate;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/util/Filters;->filter(Ljava/util/List;Lcom/helpshift/util/Predicate;)Ljava/util/List;

    move-result-object v0

    .line 1676
    invoke-static {}, Lcom/helpshift/conversation/util/predicate/ConversationPredicates;->newInProgressConversationPredicate()Lcom/helpshift/util/Predicate;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/util/Filters;->filter(Ljava/util/List;Lcom/helpshift/util/Predicate;)Ljava/util/List;

    move-result-object v1

    .line 1678
    invoke-static {v0}, Lcom/helpshift/common/ListUtils;->isEmpty(Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_1

    return-object v2

    .line 1681
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1682
    invoke-static {v0}, Lcom/helpshift/conversation/ConversationUtil;->getLastConversationDMBasedOnCreatedAt(Ljava/util/Collection;)Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    goto :goto_0

    .line 1685
    :cond_2
    invoke-static {v1}, Lcom/helpshift/conversation/ConversationUtil;->getLastConversationDMBasedOnCreatedAt(Ljava/util/Collection;)Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private getMessageCountForShowingInAppNotification(Lcom/helpshift/conversation/activeconversation/ConversationDM;)I
    .locals 2

    .line 931
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localUUID:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getInAppNotificationCountCache(Ljava/lang/String;)I

    move-result v0

    .line 932
    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getUnSeenMessageCount()I

    move-result p1

    const/4 v1, 0x0

    if-lez p1, :cond_0

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method private getPoller()Lcom/helpshift/common/domain/Poller;
    .locals 3

    .line 197
    new-instance v0, Lcom/helpshift/common/domain/Poller;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v2, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$1;

    invoke-direct {v2, p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$1;-><init>(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;)V

    invoke-direct {v0, v1, v2}, Lcom/helpshift/common/domain/Poller;-><init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/domain/F;)V

    return-object v0
.end method

.method private isAtLeastOneConversationNonActionable(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;)Z"
        }
    .end annotation

    .line 1915
    invoke-static {p1}, Lcom/helpshift/common/ListUtils;->isEmpty(Ljava/util/List;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 1919
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 1920
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isIssueInProgress()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_2
    return v1
.end method

.method private markStatusAsResolutionAcceptedIfResolved(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;)V"
        }
    .end annotation

    .line 822
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 823
    iget-object v1, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    .line 824
    sget-object v2, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REQUESTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v1, v2, :cond_0

    .line 825
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    .line 826
    invoke-virtual {v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->shouldShowConversationResolutionQuestion()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    .line 827
    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->markConversationResolutionStatus(Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private merge(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;)V"
        }
    .end annotation

    .line 762
    invoke-static {p1}, Lcom/helpshift/common/ListUtils;->isEmpty(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 766
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationsWithoutMessages(J)Ljava/util/List;

    move-result-object v4

    .line 768
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 769
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 770
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 772
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v5, 0x1

    if-le v3, v5, :cond_1

    .line 779
    invoke-static {p1}, Lcom/helpshift/conversation/ConversationUtil;->sortConversationsBasedOnCreatedAt(Ljava/util/List;)V

    :cond_1
    move-object v3, p0

    move-object v5, p1

    move-object v6, v0

    move-object v7, v2

    move-object v8, v1

    .line 782
    invoke-direct/range {v3 .. v8}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->merge(Ljava/util/List;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;)V

    .line 784
    invoke-direct {p0, p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->clearRequestIdForPendingCreateConversationCall(Ljava/util/List;)V

    .line 786
    invoke-virtual {p0, v0, v2, v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->putConversations(Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;)V

    .line 787
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 788
    iget-object v4, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/helpshift/conversation/activeconversation/ConversationUpdate;

    invoke-virtual {v3, v4}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->clearMessageUpdates(Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)V

    goto :goto_0

    .line 791
    :cond_2
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 792
    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 793
    invoke-interface {p1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 795
    invoke-direct {p0, p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->checkForReOpen(Ljava/util/List;)V

    .line 796
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->markStatusAsResolutionAcceptedIfResolved(Ljava/util/Collection;)V

    .line 798
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserDM;->isPushTokenSynced()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v1, "enableInAppNotification"

    .line 799
    invoke-virtual {v0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 800
    invoke-direct {p0, p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->checkAndGenerateNotification(Ljava/util/List;)V

    .line 803
    :cond_3
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->sendUnreadCountUpdate()V

    return-void
.end method

.method private merge(Ljava/util/List;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/helpshift/conversation/activeconversation/ConversationUpdate;",
            ">;)V"
        }
    .end annotation

    .line 1053
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1054
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1055
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1056
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1058
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 1061
    iget-object v5, v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-static {v5}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1062
    iget-object v5, v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-interface {v1, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1064
    :cond_1
    iget-object v5, v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-static {v5}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1065
    iget-object v5, v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1067
    :cond_2
    invoke-virtual {v4}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1072
    iget-object v5, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v5}, Lcom/helpshift/common/platform/Platform;->getNetworkRequestDAO()Lcom/helpshift/common/platform/network/NetworkRequestDAO;

    move-result-object v5

    const-string v6, "/preissues/"

    const-string v7, "preissue_default_unique_key"

    .line 1073
    invoke-interface {v5, v6, v7}, Lcom/helpshift/common/platform/network/NetworkRequestDAO;->getPendingRequestId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1076
    invoke-interface {v3, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1081
    :cond_3
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    const/4 v4, 0x1

    if-eqz p2, :cond_e

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 1082
    iget-object v5, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    .line 1083
    iget-object v6, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    .line 1084
    iget-object v7, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->createdRequestId:Ljava/lang/String;

    const/4 v8, 0x0

    .line 1089
    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1090
    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v8, v5

    check-cast v8, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    goto :goto_2

    .line 1092
    :cond_4
    invoke-interface {v2, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1093
    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v8, v5

    check-cast v8, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    goto :goto_2

    .line 1095
    :cond_5
    invoke-static {v7}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1096
    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1097
    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v8, v5

    check-cast v8, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    :cond_6
    :goto_2
    if-eqz v8, :cond_9

    .line 1103
    iget-object v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v5, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v6, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v8, v4, v5, v6}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    .line 1108
    iget-object v4, v8, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-interface {p5, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1109
    iget-object v4, v8, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-interface {p5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/helpshift/conversation/activeconversation/ConversationUpdate;

    goto :goto_3

    .line 1112
    :cond_7
    new-instance v4, Lcom/helpshift/conversation/activeconversation/ConversationUpdate;

    invoke-direct {v4}, Lcom/helpshift/conversation/activeconversation/ConversationUpdate;-><init>()V

    .line 1115
    :goto_3
    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1116
    invoke-direct {p0, v8, p2, p3, v4}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->mergePreIssue(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/activeconversation/ConversationDM;Ljava/util/Set;Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)V

    goto :goto_4

    .line 1119
    :cond_8
    invoke-direct {p0, v8, p2, p3, v4}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->mergeIssue(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/activeconversation/ConversationDM;Ljava/util/Set;Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)V

    .line 1121
    :goto_4
    iget-object p2, v8, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-interface {p5, p2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1125
    :cond_9
    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 1129
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iput-wide v5, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->lastUserActivityTime:J

    .line 1133
    iget-object v5, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v6, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REQUESTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v5, v6, :cond_a

    .line 1134
    sget-object v5, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_ACCEPTED:Lcom/helpshift/conversation/dto/IssueState;

    iput-object v5, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    .line 1139
    :cond_a
    iget-object v5, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    if-eqz v5, :cond_c

    .line 1141
    sget-object v6, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_ACCEPTED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq v5, v6, :cond_b

    sget-object v6, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq v5, v6, :cond_b

    sget-object v6, Lcom/helpshift/conversation/dto/IssueState;->REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq v5, v6, :cond_b

    sget-object v6, Lcom/helpshift/conversation/dto/IssueState;->ARCHIVED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v5, v6, :cond_c

    .line 1145
    :cond_b
    iput-boolean v4, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isStartNewConversationClicked:Z

    :cond_c
    if-eqz v5, :cond_d

    .line 1150
    iget-boolean v5, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isRedacted:Z

    if-eqz v5, :cond_d

    iget-object v5, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v6, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REQUESTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v5, v6, :cond_d

    .line 1152
    iput-boolean v4, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isStartNewConversationClicked:Z

    .line 1153
    sget-object v4, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_ACCEPTED:Lcom/helpshift/conversation/dto/IssueState;

    iput-object v4, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    .line 1157
    :cond_d
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1162
    :cond_e
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    if-gt p1, v4, :cond_f

    .line 1163
    invoke-interface {p4, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void

    .line 1171
    :cond_f
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1174
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    sub-int/2addr p2, v4

    :goto_5
    if-ltz p2, :cond_12

    .line 1175
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 1179
    invoke-virtual {p3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result p5

    if-nez p5, :cond_11

    add-int/lit8 p5, p2, -0x1

    :goto_6
    if-ltz p5, :cond_11

    .line 1181
    invoke-interface {p1, p5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 1182
    iget-object v2, p3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-static {v2}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_10

    iget-object v2, p3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    iget-object v3, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    .line 1183
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    iget-object v2, p3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    iget-object v3, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    .line 1184
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1185
    iget-object p3, p3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    iget-object v1, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {p3, v1}, Lcom/helpshift/common/util/HSObservableList;->addAll(Ljava/util/Collection;)Z

    .line 1187
    invoke-interface {v0, p5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_7

    :cond_10
    add-int/lit8 p5, p5, -0x1

    goto :goto_6

    :cond_11
    :goto_7
    add-int/lit8 p2, p2, -0x1

    goto :goto_5

    .line 1195
    :cond_12
    invoke-interface {p4, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method private mergeIssue(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/activeconversation/ConversationDM;Ljava/util/Set;Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            "Ljava/util/Set<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;",
            "Lcom/helpshift/conversation/activeconversation/ConversationUpdate;",
            ")V"
        }
    .end annotation

    .line 1825
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-direct {p0, v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getInMemoryConversationDM(Ljava/lang/Long;)Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 1827
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v2

    .line 1828
    iget-object v3, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    iget-object v4, v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    move-object v2, p1

    .line 1832
    :goto_0
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->isVisibleOnUI()Z

    move-result v4

    goto :goto_1

    :cond_1
    move-object v2, p1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1835
    :goto_1
    iget-object v5, v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    if-eqz v3, :cond_2

    .line 1837
    invoke-virtual {v0, p2, p4}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->mergeIssue(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)V

    goto :goto_2

    .line 1840
    :cond_2
    invoke-virtual {p1, p2, v1, p4}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->mergeIssue(Lcom/helpshift/conversation/activeconversation/ConversationDM;ZLcom/helpshift/conversation/activeconversation/ConversationUpdate;)V

    :goto_2
    if-eqz v0, :cond_3

    .line 1845
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->isVisibleOnUI()Z

    move-result p1

    if-nez p1, :cond_4

    :cond_3
    iget-object p1, v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object p2, Lcom/helpshift/conversation/dto/IssueState;->REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne p1, p2, :cond_4

    .line 1848
    invoke-virtual {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getActiveConversationFromStorage()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 1852
    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iget-object p2, v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    .line 1853
    invoke-virtual {p1, p2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 1854
    invoke-virtual {v2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->handleConversationEnded()V

    :cond_4
    if-nez v4, :cond_5

    .line 1863
    invoke-virtual {v2, v5}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->checkAndIncrementMessageCount(Lcom/helpshift/conversation/dto/IssueState;)V

    .line 1865
    :cond_5
    invoke-interface {p3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private mergePreIssue(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/activeconversation/ConversationDM;Ljava/util/Set;Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            "Ljava/util/Set<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;",
            "Lcom/helpshift/conversation/activeconversation/ConversationUpdate;",
            ")V"
        }
    .end annotation

    .line 1775
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-direct {p0, v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getInMemoryConversationDM(Ljava/lang/Long;)Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 1778
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v2

    .line 1779
    iget-object v3, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    iget-object v4, v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    move-object v2, p1

    .line 1783
    :goto_0
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->isVisibleOnUI()Z

    move-result v4

    goto :goto_1

    :cond_1
    move-object v2, p1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1789
    :goto_1
    invoke-virtual {v2, p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendConversationPostedEvent(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    .line 1790
    iget-object v5, v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-static {v5}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1791
    invoke-virtual {v2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    .line 1792
    invoke-static {v5}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    if-eqz v3, :cond_2

    .line 1794
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->handlePreIssueCreationSuccess()V

    goto :goto_2

    .line 1797
    :cond_2
    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->handlePreIssueCreationSuccess()V

    .line 1801
    :cond_3
    :goto_2
    iget-object v5, v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    if-eqz v3, :cond_4

    .line 1803
    invoke-virtual {v0, p2, p4}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->mergePreIssue(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)V

    goto :goto_3

    .line 1806
    :cond_4
    invoke-virtual {p1, p2, v1, p4}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->mergePreIssue(Lcom/helpshift/conversation/activeconversation/ConversationDM;ZLcom/helpshift/conversation/activeconversation/ConversationUpdate;)V

    :goto_3
    if-nez v4, :cond_5

    .line 1813
    invoke-virtual {v2, v5}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->checkAndIncrementMessageCount(Lcom/helpshift/conversation/dto/IssueState;)V

    .line 1815
    :cond_5
    invoke-virtual {v2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendConversationEndedDelegateForPreIssue()V

    .line 1816
    invoke-interface {p3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private retryCallsForConversation(Lcom/helpshift/conversation/activeconversation/ConversationDM;Z)V
    .locals 3

    .line 224
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {p1, v0, v1, v2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    .line 225
    invoke-virtual {p1, p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->retryMessages(Z)V

    .line 226
    iget-object p2, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatState:Lcom/helpshift/conversation/states/ConversationCSATState;

    sget-object v0, Lcom/helpshift/conversation/states/ConversationCSATState;->SUBMITTED_NOT_SYNCED:Lcom/helpshift/conversation/states/ConversationCSATState;

    if-ne p2, v0, :cond_1

    .line 228
    :try_start_0
    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendCSATSurveyInternal()V
    :try_end_0
    .catch Lcom/helpshift/common/exception/RootAPIException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 232
    iget-object p2, p1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object v0, Lcom/helpshift/common/exception/NetworkException;->NON_RETRIABLE:Lcom/helpshift/common/exception/NetworkException;

    if-ne p2, v0, :cond_0

    goto :goto_0

    .line 233
    :cond_0
    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method private sendConversationPostedEvent(Ljava/lang/String;)V
    .locals 2

    .line 410
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getAnalyticsEventDM()Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    move-result-object v0

    sget-object v1, Lcom/helpshift/analytics/AnalyticsEventType;->CONVERSATION_POSTED:Lcom/helpshift/analytics/AnalyticsEventType;

    invoke-virtual {v0, v1, p1}, Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;->pushEvent(Lcom/helpshift/analytics/AnalyticsEventType;Ljava/lang/String;)V

    return-void
.end method

.method private sendUnreadCountUpdate()V
    .locals 3

    .line 807
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->fetchConversationUpdatesListenerReference:Ljava/util/concurrent/atomic/AtomicReference;

    if-eqz v0, :cond_0

    .line 809
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/common/FetchDataFromThread;

    if-eqz v0, :cond_0

    .line 811
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v2, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$3;

    invoke-direct {v2, p0, v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$3;-><init>(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Lcom/helpshift/common/FetchDataFromThread;)V

    invoke-virtual {v1, v2}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    :cond_0
    return-void
.end method

.method private declared-synchronized setAliveConversation(Lcom/helpshift/conversation/activeconversation/ViewableConversation;)V
    .locals 3

    monitor-enter p0

    .line 361
    :try_start_0
    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getIdentifier()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->lastAliveConversationLocalId:J

    .line 362
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->aliveConversationDMs:Ljava/util/Map;

    iget-wide v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->lastAliveConversationLocalId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 363
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private shouldShowInAppNotification(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Z
    .locals 2

    .line 915
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v1, "enableInAppNotification"

    invoke-virtual {v0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 917
    invoke-direct {p0, p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->canShowNotificationForConversation(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private showInAppNotification(Lcom/helpshift/conversation/activeconversation/ConversationDM;I)V
    .locals 3

    if-lez p2, :cond_0

    .line 876
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localUUID:Ljava/lang/String;

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    .line 877
    invoke-interface {v2}, Lcom/helpshift/common/platform/Platform;->getDevice()Lcom/helpshift/common/platform/Device;

    move-result-object v2

    invoke-interface {v2}, Lcom/helpshift/common/platform/Device;->getAppName()Ljava/lang/String;

    move-result-object v2

    .line 876
    invoke-direct {p0, v0, v1, p2, v2}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->showNotificationOnUI(Ljava/lang/Long;Ljava/lang/String;ILjava/lang/String;)V

    .line 878
    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localUUID:Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->updateInAppNotificationCountCache(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method private showNotificationOnUI(Ljava/lang/Long;Ljava/lang/String;ILjava/lang/String;)V
    .locals 8

    if-lez p3, :cond_0

    .line 974
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v7, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$4;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$4;-><init>(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Ljava/lang/Long;Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v0, v7}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    :cond_0
    return-void
.end method

.method private tryToStartNewConversation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/helpshift/conversation/activeconversation/ConversationDM;
    .locals 4

    const/4 v0, 0x0

    .line 368
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->createConversation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v1

    const-string v2, ""

    .line 369
    invoke-virtual {p0, v2, v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->saveDescriptionDetail(Ljava/lang/String;I)V

    .line 370
    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-virtual {v2}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->shouldCreateConversationAnonymously()Z

    move-result v2

    if-nez v2, :cond_0

    .line 371
    invoke-virtual {p0, p2}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->saveName(Ljava/lang/String;)V

    .line 372
    invoke-virtual {p0, p3}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->saveEmail(Ljava/lang/String;)V

    .line 374
    :cond_0
    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object p3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {p3}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 p3, 0x0

    invoke-interface {p2, v2, v3, p3}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->saveConversationArchivalPrefillText(JLjava/lang/String;)V

    .line 375
    invoke-direct {p0, v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->checkAndDropCustomMeta(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    .line 376
    iget-object p2, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->sendConversationPostedEvent(Ljava/lang/String;)V

    .line 377
    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p2}, Lcom/helpshift/common/domain/Domain;->getDelegate()Lcom/helpshift/delegate/UIThreadDelegateDecorator;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/helpshift/delegate/UIThreadDelegateDecorator;->newConversationStarted(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception p1

    .line 381
    iput-boolean v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->isCreateConversationInProgress:Z

    .line 382
    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->startNewConversationListenerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 383
    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->startNewConversationListenerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;

    invoke-interface {p2, p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;->onCreateConversationFailure(Ljava/lang/Exception;)V

    .line 385
    :cond_1
    throw p1
.end method

.method private updateInAppNotificationCountCache(Ljava/lang/String;I)V
    .locals 1

    .line 897
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->inAppNotificationMessageCountMap:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public clearNotification(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V
    .locals 2

    .line 1542
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$5;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$5;-><init>(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    .line 1548
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->clearInAppNotificationCountCache()V

    return-void
.end method

.method public clearPushNotifications()V
    .locals 3

    .line 1518
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    .line 1519
    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationsWithoutMessages(J)Ljava/util/List;

    move-result-object v0

    .line 1520
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 1521
    invoke-virtual {p0, v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->clearNotification(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public createConversation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/helpshift/conversation/activeconversation/ConversationDM;
    .locals 7

    .line 415
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v0, v1}, Lcom/helpshift/account/domainmodel/UserManagerDM;->registerUserWithServer(Lcom/helpshift/account/domainmodel/UserDM;)V

    .line 417
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-static {v0}, Lcom/helpshift/common/domain/network/NetworkDataRequestUtil;->getUserRequestData(Lcom/helpshift/account/domainmodel/UserDM;)Ljava/util/HashMap;

    move-result-object v0

    .line 418
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v1}, Lcom/helpshift/common/platform/Platform;->getJsonifier()Lcom/helpshift/common/platform/Jsonifier;

    move-result-object v1

    invoke-static {p3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p3

    invoke-interface {v1, p3}, Lcom/helpshift/common/platform/Jsonifier;->jsonify(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object p3

    .line 419
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    const-string v1, "user_provided_emails"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p3, "user_provided_name"

    .line 420
    invoke-interface {v0, p3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "body"

    .line 421
    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 422
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getCampaignUID()Ljava/lang/String;

    move-result-object p1

    const-string p2, "cuid"

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 423
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getCampaignDID()Ljava/lang/String;

    move-result-object p1

    const-string p2, "cdid"

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getLocaleProviderDM()Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;

    move-result-object p1

    invoke-virtual {p1}, Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;->getDefaultLanguage()Ljava/lang/String;

    move-result-object p1

    const-string p2, "device_language"

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 425
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getLocaleProviderDM()Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;

    move-result-object p1

    invoke-virtual {p1}, Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;->getSDKLanguage()Ljava/lang/String;

    move-result-object p1

    .line 426
    invoke-static {p1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_0

    const-string p2, "developer_set_language"

    .line 427
    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 429
    :cond_0
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getMetaDataDM()Lcom/helpshift/meta/MetaDataDM;

    move-result-object p1

    invoke-virtual {p1}, Lcom/helpshift/meta/MetaDataDM;->getMetaInfo()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "meta"

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string p2, "fullPrivacy"

    invoke-virtual {p1, p2}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    .line 433
    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p2}, Lcom/helpshift/common/domain/Domain;->getCustomIssueFieldDM()Lcom/helpshift/cif/CustomIssueFieldDM;

    move-result-object p2

    invoke-virtual {p2}, Lcom/helpshift/cif/CustomIssueFieldDM;->getCustomIssueFieldData()Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 435
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "custom_fields"

    invoke-interface {v0, p3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 439
    :cond_1
    new-instance v2, Lcom/helpshift/common/domain/network/POSTNetwork;

    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object p3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    const-string v1, "/issues/"

    invoke-direct {v2, v1, p2, p3}, Lcom/helpshift/common/domain/network/POSTNetwork;-><init>(Ljava/lang/String;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 440
    new-instance v4, Lcom/helpshift/common/domain/idempotent/AllStatusCodeIdempotentPolicy;

    invoke-direct {v4}, Lcom/helpshift/common/domain/idempotent/AllStatusCodeIdempotentPolicy;-><init>()V

    .line 441
    new-instance p2, Lcom/helpshift/common/domain/network/IdempotentNetwork;

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    const-string v5, "/issues/"

    const-string v6, "issue_default_unique_key"

    move-object v1, p2

    invoke-direct/range {v1 .. v6}, Lcom/helpshift/common/domain/network/IdempotentNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/idempotent/IdempotentPolicy;Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    new-instance p3, Lcom/helpshift/common/domain/network/AuthenticationFailureNetwork;

    invoke-direct {p3, p2}, Lcom/helpshift/common/domain/network/AuthenticationFailureNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;)V

    .line 446
    new-instance p2, Lcom/helpshift/common/domain/network/TSCorrectedNetwork;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {p2, p3, v1}, Lcom/helpshift/common/domain/network/TSCorrectedNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;Lcom/helpshift/common/platform/Platform;)V

    .line 447
    new-instance p3, Lcom/helpshift/common/domain/network/MetaCorrectedNetwork;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {p3, p2, v1}, Lcom/helpshift/common/domain/network/MetaCorrectedNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;Lcom/helpshift/common/platform/Platform;)V

    .line 448
    new-instance p2, Lcom/helpshift/common/domain/network/GuardOKNetwork;

    invoke-direct {p2, p3}, Lcom/helpshift/common/domain/network/GuardOKNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;)V

    .line 452
    :try_start_0
    new-instance p3, Lcom/helpshift/common/platform/network/RequestData;

    invoke-direct {p3, v0}, Lcom/helpshift/common/platform/network/RequestData;-><init>(Ljava/util/Map;)V

    .line 453
    invoke-interface {p2, p3}, Lcom/helpshift/common/domain/network/Network;->makeRequest(Lcom/helpshift/common/platform/network/RequestData;)Lcom/helpshift/common/platform/network/Response;

    move-result-object p2

    .line 454
    iget-object p3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    .line 455
    invoke-interface {p3}, Lcom/helpshift/common/platform/Platform;->getResponseParser()Lcom/helpshift/common/platform/network/ResponseParser;

    move-result-object p3

    iget-object p2, p2, Lcom/helpshift/common/platform/network/Response;->responseString:Ljava/lang/String;

    invoke-interface {p3, p2}, Lcom/helpshift/common/platform/network/ResponseParser;->parseReadableConversation(Ljava/lang/String;)Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p2

    .line 456
    iput-boolean p1, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->wasFullPrivacyEnabledAtCreation:Z

    .line 457
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object p3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {p2, p1, p3, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    .line 459
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object p3, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-interface {p1, p3}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationWithoutMessages(Ljava/lang/String;)Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p1

    if-nez p1, :cond_2

    .line 462
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {p1, p2}, Lcom/helpshift/conversation/dao/ConversationDAO;->insertConversation(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    .line 465
    :cond_2
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object p1

    iget-object p3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    const/4 v0, 0x1

    invoke-virtual {p1, p3, v0}, Lcom/helpshift/account/domainmodel/UserManagerDM;->updateIssueExists(Lcom/helpshift/account/domainmodel/UserDM;Z)V

    .line 467
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object p1

    invoke-virtual {p1}, Lcom/helpshift/account/domainmodel/UserManagerDM;->sendPushToken()V

    .line 468
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxPoller:Lcom/helpshift/conversation/ConversationInboxPoller;

    invoke-virtual {p1, v0}, Lcom/helpshift/conversation/ConversationInboxPoller;->startAppPoller(Z)V
    :try_end_0
    .catch Lcom/helpshift/common/exception/RootAPIException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p2

    :catch_0
    move-exception p1

    .line 472
    iget-object p2, p1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object p3, Lcom/helpshift/common/exception/NetworkException;->INVALID_AUTH_TOKEN:Lcom/helpshift/common/exception/NetworkException;

    if-eq p2, p3, :cond_3

    iget-object p2, p1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object p3, Lcom/helpshift/common/exception/NetworkException;->AUTH_TOKEN_NOT_PROVIDED:Lcom/helpshift/common/exception/NetworkException;

    if-ne p2, p3, :cond_4

    .line 474
    :cond_3
    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p2}, Lcom/helpshift/common/domain/Domain;->getAuthenticationFailureDM()Lcom/helpshift/account/AuthenticationFailureDM;

    move-result-object p2

    iget-object p3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    iget-object v0, p1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    invoke-virtual {p2, p3, v0}, Lcom/helpshift/account/AuthenticationFailureDM;->notifyAuthenticationFailure(Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/common/exception/ExceptionType;)V

    .line 476
    :cond_4
    throw p1
.end method

.method public createLocalPreIssueConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;
    .locals 6

    .line 1384
    new-instance v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-direct {v0, v1, v2, v3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;-><init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    .line 1386
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-static {v1}, Lcom/helpshift/common/util/HSDateFormatSpec;->getCurrentAdjustedTimeForStorage(Lcom/helpshift/common/platform/Platform;)Ljava/lang/String;

    move-result-object v1

    .line 1387
    invoke-static {v1}, Lcom/helpshift/common/util/HSDateFormatSpec;->convertToEpochTime(Ljava/lang/String;)J

    move-result-wide v2

    .line 1388
    iget-object v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v4}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->userLocalId:J

    .line 1389
    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setCreatedAt(Ljava/lang/String;)V

    .line 1390
    invoke-virtual {v0, v2, v3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setEpochCreatedAtTime(J)V

    .line 1391
    iput-object v1, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updatedAt:Ljava/lang/String;

    .line 1392
    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->NEW:Lcom/helpshift/conversation/dto/IssueState;

    iput-object v1, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    const-string v1, "preissue"

    .line 1393
    iput-object v1, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->issueType:Ljava/lang/String;

    const-string v1, "Pre Issue Conversation"

    .line 1394
    iput-object v1, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->title:Ljava/lang/String;

    .line 1397
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {v1, v0}, Lcom/helpshift/conversation/dao/ConversationDAO;->insertPreIssueConversation(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    return-object v0
.end method

.method public createPreIssue(Lcom/helpshift/conversation/activeconversation/ViewableConversation;Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;)V
    .locals 3

    .line 509
    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 510
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->inProgressPreIssueCreators:Ljava/util/HashMap;

    iget-object v2, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/common/domain/One;

    if-eqz v1, :cond_0

    .line 514
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Pre issue creation already in progress: "

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "Helpshift_ConvInboxDM"

    invoke-static {v0, p1}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    invoke-virtual {v1}, Lcom/helpshift/common/domain/One;->getF()Lcom/helpshift/common/domain/F;

    move-result-object p1

    check-cast p1, Lcom/helpshift/conversation/CreatePreIssueDM;

    .line 516
    invoke-virtual {p1, p2}, Lcom/helpshift/conversation/CreatePreIssueDM;->setListener(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;)V

    return-void

    .line 521
    :cond_0
    new-instance v1, Lcom/helpshift/conversation/CreatePreIssueDM;

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-direct {v1, p0, v0, v2, p2}, Lcom/helpshift/conversation/CreatePreIssueDM;-><init>(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;)V

    .line 522
    new-instance p2, Lcom/helpshift/common/domain/One;

    invoke-direct {p2, v1}, Lcom/helpshift/common/domain/One;-><init>(Lcom/helpshift/common/domain/F;)V

    .line 523
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->inProgressPreIssueCreators:Ljava/util/HashMap;

    iget-object v2, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {v1, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 525
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v2, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$2;

    invoke-direct {v2, p0, p2, v0, p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$2;-><init>(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Lcom/helpshift/common/domain/One;Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/activeconversation/ViewableConversation;)V

    invoke-virtual {v1, v2}, Lcom/helpshift/common/domain/Domain;->runParallel(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public createPreIssueNetwork(Lcom/helpshift/conversation/activeconversation/ConversationDM;Ljava/lang/String;)V
    .locals 9

    .line 551
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-static {v0}, Lcom/helpshift/common/domain/network/NetworkDataRequestUtil;->getUserRequestData(Lcom/helpshift/account/domainmodel/UserDM;)Ljava/util/HashMap;

    move-result-object v0

    .line 553
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getName()Ljava/lang/String;

    move-result-object v1

    .line 554
    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v2}, Lcom/helpshift/account/domainmodel/UserDM;->getEmail()Ljava/lang/String;

    move-result-object v2

    .line 555
    invoke-static {v1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "name"

    .line 556
    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 559
    :cond_0
    invoke-static {v2}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "email"

    .line 560
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    :cond_1
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getCampaignUID()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cuid"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getCampaignDID()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cdid"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v1}, Lcom/helpshift/common/domain/Domain;->getLocaleProviderDM()Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;

    move-result-object v1

    invoke-virtual {v1}, Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;->getDefaultLanguage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "device_language"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 566
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v1}, Lcom/helpshift/common/domain/Domain;->getLocaleProviderDM()Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;

    move-result-object v1

    invoke-virtual {v1}, Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;->getSDKLanguage()Ljava/lang/String;

    move-result-object v1

    .line 567
    invoke-static {v1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "developer_set_language"

    .line 568
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 571
    :cond_2
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v1}, Lcom/helpshift/common/domain/Domain;->getMetaDataDM()Lcom/helpshift/meta/MetaDataDM;

    move-result-object v1

    invoke-virtual {v1}, Lcom/helpshift/meta/MetaDataDM;->getMetaInfo()Ljava/lang/Object;

    move-result-object v1

    .line 572
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "meta"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 573
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v2, "fullPrivacy"

    invoke-virtual {v1, v2}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 576
    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v2}, Lcom/helpshift/common/domain/Domain;->getCustomIssueFieldDM()Lcom/helpshift/cif/CustomIssueFieldDM;

    move-result-object v2

    invoke-virtual {v2}, Lcom/helpshift/cif/CustomIssueFieldDM;->getCustomIssueFieldData()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 578
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "custom_fields"

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 581
    :cond_3
    invoke-static {p2}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "greeting"

    .line 582
    invoke-interface {v0, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 586
    :cond_4
    new-instance v4, Lcom/helpshift/common/domain/network/POSTNetwork;

    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    const-string v3, "/preissues/"

    invoke-direct {v4, v3, p2, v2}, Lcom/helpshift/common/domain/network/POSTNetwork;-><init>(Ljava/lang/String;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 587
    new-instance v6, Lcom/helpshift/common/domain/idempotent/SuccessOrNonRetriableStatusCodeIdempotentPolicy;

    invoke-direct {v6}, Lcom/helpshift/common/domain/idempotent/SuccessOrNonRetriableStatusCodeIdempotentPolicy;-><init>()V

    .line 588
    new-instance p2, Lcom/helpshift/common/domain/network/IdempotentNetwork;

    iget-object v5, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    const-string v7, "/preissues/"

    const-string v8, "preissue_default_unique_key"

    move-object v3, p2

    invoke-direct/range {v3 .. v8}, Lcom/helpshift/common/domain/network/IdempotentNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/idempotent/IdempotentPolicy;Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    new-instance v2, Lcom/helpshift/common/domain/network/AuthenticationFailureNetwork;

    invoke-direct {v2, p2}, Lcom/helpshift/common/domain/network/AuthenticationFailureNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;)V

    .line 593
    new-instance p2, Lcom/helpshift/common/domain/network/TSCorrectedNetwork;

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {p2, v2, v3}, Lcom/helpshift/common/domain/network/TSCorrectedNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;Lcom/helpshift/common/platform/Platform;)V

    .line 594
    new-instance v2, Lcom/helpshift/common/domain/network/MetaCorrectedNetwork;

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v2, p2, v3}, Lcom/helpshift/common/domain/network/MetaCorrectedNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;Lcom/helpshift/common/platform/Platform;)V

    .line 595
    new-instance p2, Lcom/helpshift/common/domain/network/GuardOKNetwork;

    invoke-direct {p2, v2}, Lcom/helpshift/common/domain/network/GuardOKNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;)V

    .line 598
    new-instance v2, Lcom/helpshift/common/platform/network/RequestData;

    invoke-direct {v2, v0}, Lcom/helpshift/common/platform/network/RequestData;-><init>(Ljava/util/Map;)V

    .line 601
    :try_start_0
    invoke-interface {p2, v2}, Lcom/helpshift/common/domain/network/Network;->makeRequest(Lcom/helpshift/common/platform/network/RequestData;)Lcom/helpshift/common/platform/network/Response;

    move-result-object p2

    .line 602
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v0}, Lcom/helpshift/common/platform/Platform;->getResponseParser()Lcom/helpshift/common/platform/network/ResponseParser;

    move-result-object v0

    iget-object p2, p2, Lcom/helpshift/common/platform/network/Response;->responseString:Ljava/lang/String;

    invoke-interface {v0, p2}, Lcom/helpshift/common/platform/network/ResponseParser;->parseReadableConversation(Ljava/lang/String;)Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p2

    .line 605
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 606
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    iput-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    .line 608
    :cond_5
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->title:Ljava/lang/String;

    iput-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->title:Ljava/lang/String;

    .line 609
    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getCreatedAt()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setCreatedAt(Ljava/lang/String;)V

    .line 610
    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getEpochCreatedAtTime()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setEpochCreatedAtTime(J)V

    .line 611
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updatedAt:Ljava/lang/String;

    iput-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updatedAt:Ljava/lang/String;

    .line 612
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->publishId:Ljava/lang/String;

    iput-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->publishId:Ljava/lang/String;

    .line 613
    iget-boolean v0, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->showAgentName:Z

    iput-boolean v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->showAgentName:Z

    .line 614
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    iput-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    .line 615
    iput-boolean v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->wasFullPrivacyEnabledAtCreation:Z

    .line 617
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {p1, v0, v1, v2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    .line 619
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_6

    .line 622
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    iput-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    .line 627
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v0}, Lcom/helpshift/common/util/HSObservableList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 628
    iget-object v3, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iput-object v3, v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->conversationLocalId:Ljava/lang/Long;

    .line 629
    iput v1, v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->deliveryState:I

    goto :goto_0

    .line 632
    :cond_6
    iget-object p2, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    iput-object p2, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    .line 635
    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p2}, Lcom/helpshift/common/domain/Domain;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object p2

    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {p2, v0, v1}, Lcom/helpshift/account/domainmodel/UserManagerDM;->updateIssueExists(Lcom/helpshift/account/domainmodel/UserDM;Z)V

    .line 637
    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p2}, Lcom/helpshift/common/domain/Domain;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object p2

    invoke-virtual {p2}, Lcom/helpshift/account/domainmodel/UserManagerDM;->sendPushToken()V

    .line 639
    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {p2, p1}, Lcom/helpshift/conversation/dao/ConversationDAO;->updateConversation(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    .line 642
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getDelegate()Lcom/helpshift/delegate/UIThreadDelegateDecorator;

    move-result-object p1

    const-string p2, ""

    invoke-virtual {p1, p2}, Lcom/helpshift/delegate/UIThreadDelegateDecorator;->newConversationStarted(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/helpshift/common/exception/RootAPIException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 645
    iget-object p2, p1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object v0, Lcom/helpshift/common/exception/NetworkException;->INVALID_AUTH_TOKEN:Lcom/helpshift/common/exception/NetworkException;

    if-eq p2, v0, :cond_7

    iget-object p2, p1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object v0, Lcom/helpshift/common/exception/NetworkException;->AUTH_TOKEN_NOT_PROVIDED:Lcom/helpshift/common/exception/NetworkException;

    if-ne p2, v0, :cond_8

    .line 647
    :cond_7
    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p2}, Lcom/helpshift/common/domain/Domain;->getAuthenticationFailureDM()Lcom/helpshift/account/AuthenticationFailureDM;

    move-result-object p2

    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    iget-object v1, p1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    invoke-virtual {p2, v0, v1}, Lcom/helpshift/account/AuthenticationFailureDM;->notifyAuthenticationFailure(Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/common/exception/ExceptionType;)V

    .line 649
    :cond_8
    throw p1
.end method

.method deleteAllConversationsData()V
    .locals 3

    .line 178
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->deleteConversationsAndMessages()V

    .line 179
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 181
    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    invoke-interface {v2, v0, v1}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->deleteUserData(J)V

    return-void
.end method

.method public deleteCachedFilesForResolvedConversations()V
    .locals 2

    .line 1692
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$6;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$6;-><init>(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runParallel(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public deletePreIssueIfNotCreated(Lcom/helpshift/conversation/activeconversation/ViewableConversation;)V
    .locals 2

    .line 490
    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 491
    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->isConversationVMAttached()Z

    move-result p1

    if-nez p1, :cond_0

    .line 492
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    .line 493
    invoke-static {p1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->inProgressPreIssueCreators:Ljava/util/HashMap;

    iget-object v1, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    .line 494
    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    .line 495
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, Lcom/helpshift/conversation/dao/ConversationDAO;->deleteConversation(J)V

    :cond_0
    return-void
.end method

.method public executeUserSync()V
    .locals 5

    .line 1885
    invoke-virtual {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->fetchInitialConversationUpdates()Lcom/helpshift/conversation/dto/ConversationInbox;

    .line 1888
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationsWithoutMessages(J)Ljava/util/List;

    move-result-object v0

    .line 1890
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->isAtLeastOneConversationNonActionable(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x3

    const/4 v2, 0x0

    .line 1900
    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->remoteConversationLoader:Lcom/helpshift/conversation/loaders/RemoteConversationLoader;

    invoke-virtual {v3}, Lcom/helpshift/conversation/loaders/RemoteConversationLoader;->hasMoreMessage()Z

    move-result v3

    .line 1901
    :goto_0
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->isAtLeastOneConversationNonActionable(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz v3, :cond_1

    if-ge v2, v1, :cond_1

    .line 1905
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->fetchConversationHistory()V

    .line 1907
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v3}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-interface {v0, v3, v4}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationsWithoutMessages(J)Ljava/util/List;

    move-result-object v0

    .line 1908
    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->remoteConversationLoader:Lcom/helpshift/conversation/loaders/RemoteConversationLoader;

    invoke-virtual {v3}, Lcom/helpshift/conversation/loaders/RemoteConversationLoader;->hasMoreMessage()Z

    move-result v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public fetchConversationUpdates()Lcom/helpshift/conversation/dto/ConversationInbox;
    .locals 4

    .line 676
    sget-object v0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->fetchConversationUpdatesLock:Ljava/lang/Object;

    monitor-enter v0

    .line 677
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v2}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->getConversationInboxTimestamp(J)Ljava/lang/String;

    move-result-object v1

    .line 678
    invoke-direct {p0, v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->fetchConversationUpdatesInternal(Ljava/lang/String;)Lcom/helpshift/conversation/dto/ConversationInbox;

    move-result-object v1

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 679
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public fetchConversationsAndGetNotificationCount()Lcom/helpshift/util/ValuePair;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/helpshift/util/ValuePair<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 1576
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserDM;->issueExists()Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_2

    .line 1580
    :cond_0
    iget-boolean v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userCanReadMessages:Z

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 1581
    new-instance v0, Lcom/helpshift/util/ValuePair;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/helpshift/util/ValuePair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    .line 1584
    :cond_1
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v3}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-interface {v0, v3, v4}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationsWithoutMessages(J)Ljava/util/List;

    move-result-object v0

    .line 1586
    invoke-static {v0}, Lcom/helpshift/common/ListUtils;->isEmpty(Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1587
    new-instance v0, Lcom/helpshift/util/ValuePair;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/helpshift/util/ValuePair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    .line 1591
    :cond_2
    invoke-static {v0}, Lcom/helpshift/conversation/ConversationUtil;->shouldPollActivelyForConversations(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-wide/32 v3, 0xea60

    goto :goto_0

    :cond_3
    const-wide/32 v3, 0x493e0

    .line 1594
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->lastNotifCountFetchTime:J

    sub-long/2addr v5, v7

    cmp-long v0, v5, v3

    if-gez v0, :cond_4

    .line 1596
    invoke-virtual {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getNotificationCountSync()I

    move-result v0

    .line 1597
    new-instance v2, Lcom/helpshift/util/ValuePair;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Lcom/helpshift/util/ValuePair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    .line 1601
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->lastNotifCountFetchTime:J

    .line 1602
    invoke-virtual {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->fetchConversationUpdates()Lcom/helpshift/conversation/dto/ConversationInbox;

    .line 1604
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getActiveConversationFromUIOrStorage()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1606
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getUnSeenMessageCount()I

    move-result v0

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    .line 1608
    :goto_1
    new-instance v1, Lcom/helpshift/util/ValuePair;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/helpshift/util/ValuePair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1

    .line 1577
    :cond_6
    :goto_2
    new-instance v0, Lcom/helpshift/util/ValuePair;

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/helpshift/util/ValuePair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public fetchInitialConversationUpdates()Lcom/helpshift/conversation/dto/ConversationInbox;
    .locals 2

    .line 683
    sget-object v0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->fetchConversationUpdatesLock:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x0

    .line 684
    :try_start_0
    invoke-direct {p0, v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->fetchConversationUpdatesInternal(Ljava/lang/String;)Lcom/helpshift/conversation/dto/ConversationInbox;

    move-result-object v1

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 685
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getActiveConversationFromStorage()Lcom/helpshift/conversation/activeconversation/ConversationDM;
    .locals 6

    .line 1336
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v1, "disableInAppConversation"

    invoke-virtual {v0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1337
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationsWithoutMessages(J)Ljava/util/List;

    move-result-object v0

    .line 1338
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1339
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 1340
    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v5, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v2, v3, v4, v5}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    .line 1341
    invoke-virtual {v2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldOpen()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1342
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1345
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 1346
    invoke-static {v1}, Lcom/helpshift/conversation/ConversationUtil;->getLastConversationDMBasedOnCreatedAt(Ljava/util/Collection;)Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    return-object v0
.end method

.method public getActiveConversationOrPreIssue()Lcom/helpshift/conversation/activeconversation/ConversationDM;
    .locals 3

    .line 1327
    invoke-virtual {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getActiveConversationFromStorage()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1328
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v2, "conversationalIssueFiling"

    invoke-virtual {v1, v2}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1329
    invoke-virtual {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->createLocalPreIssueConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getConversationArchivalPrefillText()Ljava/lang/String;
    .locals 3

    .line 248
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->getConversationArchivalPrefillText(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getConversationDM(ZLjava/lang/Long;)Lcom/helpshift/conversation/activeconversation/ViewableConversation;
    .locals 7

    if-eqz p1, :cond_0

    .line 1250
    sget-object p1, Lcom/helpshift/conversation/activeconversation/ConversationHistoryDM;->CONVERSATION_HISTORY_DM_IDENTIFIER:Ljava/lang/Long;

    invoke-direct {p0, p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getInMemoryConversationDM(Ljava/lang/Long;)Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    move-result-object p1

    if-nez p1, :cond_2

    .line 1252
    new-instance p1, Lcom/helpshift/conversation/loaders/ConversationHistoryLoader;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->remoteConversationLoader:Lcom/helpshift/conversation/loaders/RemoteConversationLoader;

    const-wide/16 v4, 0x64

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/helpshift/conversation/loaders/ConversationHistoryLoader;-><init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/conversation/loaders/RemoteConversationLoader;J)V

    .line 1254
    new-instance p2, Lcom/helpshift/conversation/activeconversation/ConversationHistoryDM;

    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-direct {p2, v0, v1, v2, p1}, Lcom/helpshift/conversation/activeconversation/ConversationHistoryDM;-><init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/conversation/loaders/ConversationHistoryLoader;)V

    .line 1255
    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->init()V

    .line 1256
    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getAllConversations()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/helpshift/common/ListUtils;->isEmpty(Ljava/util/List;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1258
    invoke-virtual {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->createLocalPreIssueConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p1

    .line 1259
    invoke-virtual {p2, p1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->onNewConversationStarted(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    goto :goto_0

    .line 1264
    :cond_0
    invoke-direct {p0, p2}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getInMemoryConversationDM(Ljava/lang/Long;)Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    move-result-object p1

    if-nez p1, :cond_2

    .line 1266
    new-instance p1, Lcom/helpshift/conversation/loaders/SingleConversationLoader;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    iget-object v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->remoteConversationLoader:Lcom/helpshift/conversation/loaders/RemoteConversationLoader;

    const-wide/16 v5, 0x64

    move-object v0, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/helpshift/conversation/loaders/SingleConversationLoader;-><init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/account/domainmodel/UserDM;Ljava/lang/Long;Lcom/helpshift/conversation/loaders/RemoteConversationLoader;J)V

    .line 1269
    new-instance p2, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;

    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-direct {p2, v0, v1, v2, p1}, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;-><init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/conversation/loaders/SingleConversationLoader;)V

    .line 1270
    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->init()V

    :cond_1
    :goto_0
    move-object p1, p2

    .line 1273
    :cond_2
    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->liveUpdateDM:Lcom/helpshift/conversation/activeconversation/LiveUpdateDM;

    invoke-virtual {p1, p2}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->setLiveUpdateDM(Lcom/helpshift/conversation/activeconversation/LiveUpdateDM;)V

    .line 1274
    invoke-direct {p0, p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->setAliveConversation(Lcom/helpshift/conversation/activeconversation/ViewableConversation;)V

    return-object p1
.end method

.method public getConversationDetail()Lcom/helpshift/conversation/dto/ConversationDetailDTO;
    .locals 3

    .line 244
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->getDescriptionDetail(J)Lcom/helpshift/conversation/dto/ConversationDetailDTO;

    move-result-object v0

    return-object v0
.end method

.method public getConversationInboxPoller()Lcom/helpshift/conversation/ConversationInboxPoller;
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxPoller:Lcom/helpshift/conversation/ConversationInboxPoller;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 3

    .line 275
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->getEmail(J)Ljava/lang/String;

    move-result-object v0

    .line 276
    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 277
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserDM;->getEmail()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getFAQSearchResults(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1

    .line 307
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->faqSearchDM:Lcom/helpshift/faq/domainmodel/FAQSearchDM;

    invoke-interface {v0, p1}, Lcom/helpshift/faq/domainmodel/FAQSearchDM;->getSearchResults(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method public getImageAttachmentDraft()Lcom/helpshift/conversation/dto/ImagePickerFile;
    .locals 3

    .line 295
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->getImageAttachment(J)Lcom/helpshift/conversation/dto/ImagePickerFile;

    move-result-object v0

    return-object v0
.end method

.method public getLastConversationsRedactionTime()Ljava/lang/Long;
    .locals 3

    .line 291
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->getLastConversationsRedactionTime(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 3

    .line 267
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->getName(J)Ljava/lang/String;

    move-result-object v0

    .line 268
    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 269
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserDM;->getName()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getNotificationCountSync()I
    .locals 4

    .line 1553
    iget-boolean v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userCanReadMessages:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 1557
    :cond_0
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getActiveConversationFromUIOrStorage()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    if-nez v0, :cond_1

    return v1

    .line 1563
    :cond_1
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getUnSeenMessageCount()I

    move-result v2

    .line 1566
    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localUUID:Ljava/lang/String;

    invoke-interface {v3, v0}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->getPushNotificationData(Ljava/lang/String;)Lcom/helpshift/conversation/dao/PushNotificationData;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1568
    iget v1, v0, Lcom/helpshift/conversation/dao/PushNotificationData;->count:I

    .line 1571
    :cond_2
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public getOldestConversationCreatedAtTime()Ljava/lang/Long;
    .locals 3

    .line 1935
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationDAO;->getOldestConversationCreatedAtTime(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getOpenConversationWithMessages()Lcom/helpshift/conversation/activeconversation/ConversationDM;
    .locals 7

    .line 1358
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationsWithoutMessages(J)Ljava/util/List;

    move-result-object v0

    .line 1359
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1360
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    return-object v3

    .line 1363
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 1364
    iget-object v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v5, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v6, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v2, v4, v5, v6}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    .line 1365
    invoke-virtual {v2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isIssueInProgress()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1366
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1369
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    return-object v3

    .line 1373
    :cond_3
    invoke-static {v1}, Lcom/helpshift/conversation/ConversationUtil;->getLastConversationDMBasedOnCreatedAt(Ljava/util/Collection;)Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 1374
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v2, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/helpshift/conversation/dao/ConversationDAO;->readMessages(J)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setMessageDMs(Ljava/util/List;)V

    return-object v0
.end method

.method public getUserReplyText()Ljava/lang/String;
    .locals 3

    .line 303
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->getUserReplyDraft(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handlePushNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const-string v0, "issue"

    .line 1462
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1463
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {p1, p2}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationWithoutMessages(Ljava/lang/String;)Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string v0, "preissue"

    .line 1465
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1466
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {p1, p2}, Lcom/helpshift/conversation/dao/ConversationDAO;->readPreConversationWithoutMessages(Ljava/lang/String;)Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p1

    :goto_0
    if-nez p1, :cond_1

    return-void

    .line 1480
    :cond_1
    invoke-static {p3}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 1481
    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {p2}, Lcom/helpshift/common/platform/Platform;->getDevice()Lcom/helpshift/common/platform/Device;

    move-result-object p2

    invoke-interface {p2}, Lcom/helpshift/common/platform/Device;->getAppName()Ljava/lang/String;

    move-result-object p3

    .line 1484
    :cond_2
    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localUUID:Ljava/lang/String;

    .line 1485
    invoke-interface {p2, v0}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->getPushNotificationData(Ljava/lang/String;)Lcom/helpshift/conversation/dao/PushNotificationData;

    move-result-object p2

    const/4 v0, 0x1

    if-nez p2, :cond_3

    move-object p2, p3

    goto :goto_1

    .line 1493
    :cond_3
    iget v1, p2, Lcom/helpshift/conversation/dao/PushNotificationData;->count:I

    add-int/2addr v0, v1

    .line 1494
    iget-object p2, p2, Lcom/helpshift/conversation/dao/PushNotificationData;->title:Ljava/lang/String;

    .line 1498
    :goto_1
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v2, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localUUID:Ljava/lang/String;

    new-instance v3, Lcom/helpshift/conversation/dao/PushNotificationData;

    invoke-direct {v3, v0, p2}, Lcom/helpshift/conversation/dao/PushNotificationData;-><init>(ILjava/lang/String;)V

    invoke-interface {v1, v2, v3}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->setPushNotificationData(Ljava/lang/String;Lcom/helpshift/conversation/dao/PushNotificationData;)V

    if-lez v0, :cond_4

    .line 1501
    invoke-direct {p0, p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->canShowNotificationForConversation(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Z

    move-result p2

    if-eqz p2, :cond_4

    .line 1502
    iget-object p2, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localUUID:Ljava/lang/String;

    invoke-direct {p0, p2, p1, v0, p3}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->showNotificationOnUI(Ljava/lang/Long;Ljava/lang/String;ILjava/lang/String;)V

    .line 1508
    :cond_4
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->sendUnreadCountUpdate()V

    return-void

    .line 1469
    :cond_5
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Cannot handle push for unknown issue type. "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "Helpshift_ConvInboxDM"

    invoke-static {p2, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public initialize()V
    .locals 2

    .line 165
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getAutoRetryFailedEventDM()Lcom/helpshift/common/AutoRetryFailedEventDM;

    move-result-object v0

    sget-object v1, Lcom/helpshift/common/AutoRetryFailedEventDM$EventType;->CONVERSATION:Lcom/helpshift/common/AutoRetryFailedEventDM$EventType;

    invoke-virtual {v0, v1, p0}, Lcom/helpshift/common/AutoRetryFailedEventDM;->register(Lcom/helpshift/common/AutoRetryFailedEventDM$EventType;Lcom/helpshift/common/AutoRetriableDM;)V

    .line 167
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserDM;->getSyncState()Lcom/helpshift/account/domainmodel/UserSyncStatus;

    move-result-object v0

    sget-object v1, Lcom/helpshift/account/domainmodel/UserSyncStatus;->COMPLETED:Lcom/helpshift/account/domainmodel/UserSyncStatus;

    if-ne v0, v1, :cond_0

    .line 168
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getConversationInboxPoller()Lcom/helpshift/conversation/ConversationInboxPoller;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/account/domainmodel/UserDM;->addObserver(Ljava/util/Observer;)V

    :cond_0
    return-void
.end method

.method public isActiveConversationActionable()Z
    .locals 6

    .line 1403
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getAliveConversationDM()Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1406
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_1

    .line 1410
    invoke-virtual {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getActiveConversationFromStorage()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v1

    :cond_1
    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_7

    .line 1417
    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    .line 1418
    invoke-static {v4}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1419
    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isIssueInProgress()Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_2

    .line 1423
    :cond_2
    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isIssueInProgress()Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v5, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REQUESTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v4, v5, :cond_3

    goto :goto_2

    .line 1426
    :cond_3
    iget-object v1, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v4, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v1, v4, :cond_7

    if-eqz v0, :cond_4

    .line 1429
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getConversationVMCallback()Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1432
    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVMCallback;->isMessageBoxVisible()Z

    move-result v0

    move v1, v0

    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_1
    if-nez v0, :cond_5

    .line 1436
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v0, v4, v5}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->getPersistMessageBox(J)Z

    move-result v0

    .line 1437
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v4}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v1, v4, v5}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->getUserReplyDraft(J)Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_6

    .line 1438
    invoke-static {v1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    goto :goto_2

    :cond_5
    move v2, v1

    goto :goto_3

    :cond_6
    :goto_2
    const/4 v2, 0x1

    :cond_7
    :goto_3
    return v2
.end method

.method public isCreateConversationInProgress()Z
    .locals 1

    .line 672
    iget-boolean v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->isCreateConversationInProgress:Z

    return v0
.end method

.method putConversations(Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/helpshift/conversation/activeconversation/ConversationUpdate;",
            ">;)V"
        }
    .end annotation

    .line 1029
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 1031
    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1, v2, v3, v4}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    goto :goto_0

    .line 1034
    :cond_0
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 1036
    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1, v2, v3, v4}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    goto :goto_1

    .line 1039
    :cond_1
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, v1, p3}, Lcom/helpshift/conversation/dao/ConversationDAO;->updateConversations(Ljava/util/List;Ljava/util/Map;)V

    .line 1041
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {p1, p3}, Lcom/helpshift/conversation/dao/ConversationDAO;->insertConversations(Ljava/util/List;)V

    return-void
.end method

.method public redactConversations()V
    .locals 4

    .line 1943
    sget-object v0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->fetchConversationUpdatesLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1945
    :try_start_0
    invoke-direct {p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->deleteConversationsAndMessages()V

    .line 1947
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->aliveConversationDMs:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 1949
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v2}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->resetDataAfterConversationsDeletion(J)V

    .line 1950
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public registerStartNewConversationListener(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;)V
    .locals 1

    .line 319
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->startNewConversationListenerRef:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method resetPreIssueConversationsForUser(Lcom/helpshift/account/domainmodel/UserDM;)V
    .locals 7

    .line 1708
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-virtual {p1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationsWithoutMessages(J)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1710
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 1714
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 1715
    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v1, v2, v3, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    .line 1716
    iget-object v2, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-static {v2}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    .line 1721
    :cond_2
    iget-wide v2, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->lastUserActivityTime:J

    .line 1722
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v2

    const-wide/32 v2, 0x5265c00

    cmp-long v6, v4, v2

    if-ltz v6, :cond_1

    .line 1724
    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isIssueInProgress()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v3, Lcom/helpshift/conversation/dto/IssueState;->UNKNOWN:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v2, v3, :cond_1

    .line 1726
    :cond_3
    invoke-virtual {p0, v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->clearNotification(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    .line 1727
    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v3, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$7;

    invoke-direct {v3, p0, p1, v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$7;-><init>(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    invoke-virtual {v2, v3}, Lcom/helpshift/common/domain/Domain;->runParallel(Lcom/helpshift/common/domain/F;)V

    goto :goto_0

    :cond_4
    :goto_1
    return-void
.end method

.method public resetPushNotificationCount(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V
    .locals 2

    .line 1513
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localUUID:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->setPushNotificationData(Ljava/lang/String;Lcom/helpshift/conversation/dao/PushNotificationData;)V

    .line 1514
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getDelegate()Lcom/helpshift/delegate/UIThreadDelegateDecorator;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/helpshift/delegate/UIThreadDelegateDecorator;->didReceiveNotification(I)V

    return-void
.end method

.method public saveDescriptionDetail(Ljava/lang/String;I)V
    .locals 6

    .line 252
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    new-instance v3, Lcom/helpshift/conversation/dto/ConversationDetailDTO;

    .line 254
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    invoke-direct {v3, p1, v4, v5, p2}, Lcom/helpshift/conversation/dto/ConversationDetailDTO;-><init>(Ljava/lang/String;JI)V

    .line 252
    invoke-interface {v0, v1, v2, v3}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->saveDescriptionDetail(JLcom/helpshift/conversation/dto/ConversationDetailDTO;)V

    return-void
.end method

.method public saveEmail(Ljava/lang/String;)V
    .locals 3

    .line 263
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2, p1}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->saveEmail(JLjava/lang/String;)V

    return-void
.end method

.method public saveImageAttachmentDraft(Lcom/helpshift/conversation/dto/ImagePickerFile;)V
    .locals 3

    .line 283
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2, p1}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->saveImageAttachment(JLcom/helpshift/conversation/dto/ImagePickerFile;)V

    return-void
.end method

.method public saveLastConversationsRedactionTime(J)V
    .locals 3

    .line 287
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2, p1, p2}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->saveLastConversationsRedactionTime(JJ)V

    return-void
.end method

.method public saveName(Ljava/lang/String;)V
    .locals 3

    .line 259
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2, p1}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->saveName(JLjava/lang/String;)V

    return-void
.end method

.method public saveUserReplyText(Ljava/lang/String;)V
    .locals 3

    .line 299
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2, p1}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->saveUserReplyDraft(JLjava/lang/String;)V

    return-void
.end method

.method public sendFailedApiCalls(Lcom/helpshift/common/AutoRetryFailedEventDM$EventType;)V
    .locals 2

    .line 207
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    .line 208
    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationsWithoutMessages(J)Ljava/util/List;

    move-result-object p1

    .line 209
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 211
    iget-object v1, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-direct {p0, v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getInMemoryConversationDM(Ljava/lang/Long;)Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 214
    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->retryCallsForConversation(Lcom/helpshift/conversation/activeconversation/ConversationDM;Z)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 218
    invoke-direct {p0, v0, v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->retryCallsForConversation(Lcom/helpshift/conversation/activeconversation/ConversationDM;Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setConversationViewState(I)V
    .locals 0

    .line 1455
    iput p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationViewState:I

    return-void
.end method

.method public setPersistMessageBox(Z)V
    .locals 3

    .line 1451
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2, p1}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->savePersistMessageBox(JZ)V

    return-void
.end method

.method public setShouldDropCustomMetadata(Z)V
    .locals 0

    .line 315
    iput-boolean p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->shouldDropCustomMetadata:Z

    return-void
.end method

.method public setUserCanReadMessages(Z)V
    .locals 0

    .line 240
    iput-boolean p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userCanReadMessages:Z

    return-void
.end method

.method public shouldOpenConversationFromNotification(J)Z
    .locals 2

    .line 1307
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getInMemoryConversationDM(Ljava/lang/Long;)Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1308
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1309
    :cond_0
    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationWithoutMessages(Ljava/lang/Long;)Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 1312
    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {p1, p2, v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    .line 1313
    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldOpen()Z

    move-result p1

    return p1

    :cond_1
    if-eqz v0, :cond_2

    .line 1317
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->shouldOpen()Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public shouldPersistMessageBox()Z
    .locals 3

    .line 1447
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->getPersistMessageBox(J)Z

    move-result v0

    return v0
.end method

.method public showPushNotifications()V
    .locals 5

    .line 1526
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    .line 1527
    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationsWithoutMessages(J)Ljava/util/List;

    move-result-object v0

    .line 1528
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 1529
    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationInboxDAO:Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    iget-object v3, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localUUID:Ljava/lang/String;

    .line 1530
    invoke-interface {v2, v3}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->getPushNotificationData(Ljava/lang/String;)Lcom/helpshift/conversation/dao/PushNotificationData;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1531
    iget v3, v2, Lcom/helpshift/conversation/dao/PushNotificationData;->count:I

    if-lez v3, :cond_0

    .line 1532
    iget-object v3, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    .line 1533
    iget-object v1, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localUUID:Ljava/lang/String;

    iget v4, v2, Lcom/helpshift/conversation/dao/PushNotificationData;->count:I

    iget-object v2, v2, Lcom/helpshift/conversation/dao/PushNotificationData;->title:Ljava/lang/String;

    invoke-direct {p0, v3, v1, v4, v2}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->showNotificationOnUI(Ljava/lang/Long;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public startNewConversation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/helpshift/conversation/dto/ImagePickerFile;)V
    .locals 7

    .line 331
    new-instance v6, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$CreateConversationStateHolder;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$CreateConversationStateHolder;-><init>(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/helpshift/conversation/dto/ImagePickerFile;)V

    .line 335
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v6}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$CreateConversationStateHolder;->getStartNewConversationInternalF()Lcom/helpshift/common/domain/F;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/helpshift/common/domain/Domain;->runParallel(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method startNewConversationInternal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/helpshift/conversation/dto/ImagePickerFile;)V
    .locals 7

    const/4 v0, 0x1

    .line 340
    iput-boolean v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->isCreateConversationInProgress:Z

    .line 341
    invoke-direct {p0, p1, p2, p3}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->tryToStartNewConversation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p1

    .line 342
    new-instance p2, Lcom/helpshift/conversation/loaders/SingleConversationLoader;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    iget-object v3, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iget-object v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->remoteConversationLoader:Lcom/helpshift/conversation/loaders/RemoteConversationLoader;

    const-wide/16 v5, 0x64

    move-object v0, p2

    invoke-direct/range {v0 .. v6}, Lcom/helpshift/conversation/loaders/SingleConversationLoader;-><init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/account/domainmodel/UserDM;Ljava/lang/Long;Lcom/helpshift/conversation/loaders/RemoteConversationLoader;J)V

    .line 347
    new-instance p3, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;

    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-direct {p3, v0, v1, v2, p2}, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;-><init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/conversation/loaders/SingleConversationLoader;)V

    .line 349
    invoke-virtual {p3}, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->init()V

    .line 350
    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->liveUpdateDM:Lcom/helpshift/conversation/activeconversation/LiveUpdateDM;

    invoke-virtual {p3, p2}, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->setLiveUpdateDM(Lcom/helpshift/conversation/activeconversation/LiveUpdateDM;)V

    .line 351
    invoke-direct {p0, p3}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->setAliveConversation(Lcom/helpshift/conversation/activeconversation/ViewableConversation;)V

    .line 353
    invoke-direct {p0, p1, p4}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->checkAndTryToUploadImage(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/dto/ImagePickerFile;)V

    const/4 p2, 0x0

    .line 354
    iput-boolean p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->isCreateConversationInProgress:Z

    .line 355
    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->startNewConversationListenerRef:Ljava/lang/ref/WeakReference;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 356
    iget-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->startNewConversationListenerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;

    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p3

    invoke-interface {p2, p3, p4}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;->onCreateConversationSuccess(J)V

    :cond_0
    return-void
.end method

.method public triggerFAQSearchIndexing()V
    .locals 1

    .line 311
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->faqSearchDM:Lcom/helpshift/faq/domainmodel/FAQSearchDM;

    invoke-interface {v0}, Lcom/helpshift/faq/domainmodel/FAQSearchDM;->startFAQSearchIndexing()V

    return-void
.end method

.method public unregisterStartNewConversationListener(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;)V
    .locals 1

    .line 323
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->startNewConversationListenerRef:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 324
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 325
    new-instance p1, Ljava/lang/ref/WeakReference;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->startNewConversationListenerRef:Ljava/lang/ref/WeakReference;

    :cond_0
    return-void
.end method
