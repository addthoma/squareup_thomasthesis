.class Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$6;
.super Lcom/helpshift/common/domain/F;
.source "ConversationInboxDM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->deleteCachedFilesForResolvedConversations()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;)V
    .locals 0

    .line 1692
    iput-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$6;->this$0:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 5

    .line 1695
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$6;->this$0:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    iget-object v0, v0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$6;->this$0:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    iget-object v1, v1, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    .line 1696
    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationsWithoutMessages(J)Ljava/util/List;

    move-result-object v0

    .line 1697
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 1698
    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$6;->this$0:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    iget-object v2, v2, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$6;->this$0:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    iget-object v3, v3, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$6;->this$0:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    iget-object v4, v4, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1, v2, v3, v4}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    .line 1699
    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldOpen()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1700
    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->deleteCachedScreenshotFiles()V

    goto :goto_0

    :cond_1
    return-void
.end method
