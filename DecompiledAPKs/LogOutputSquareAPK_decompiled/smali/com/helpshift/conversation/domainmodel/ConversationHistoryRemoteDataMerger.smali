.class public Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;
.super Ljava/lang/Object;
.source "ConversationHistoryRemoteDataMerger.java"


# instance fields
.field private domain:Lcom/helpshift/common/domain/Domain;

.field private platform:Lcom/helpshift/common/platform/Platform;

.field private userDM:Lcom/helpshift/account/domainmodel/UserDM;


# direct methods
.method public constructor <init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;->platform:Lcom/helpshift/common/platform/Platform;

    .line 31
    iput-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;->domain:Lcom/helpshift/common/domain/Domain;

    .line 32
    iput-object p3, p0, Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    return-void
.end method


# virtual methods
.method public merge(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-static {p1}, Lcom/helpshift/common/ListUtils;->isEmpty(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 41
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 42
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 43
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 45
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_1

    .line 46
    invoke-static {p1}, Lcom/helpshift/conversation/ConversationUtil;->sortConversationsBasedOnCreatedAt(Ljava/util/List;)V

    .line 49
    :cond_1
    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v3}, Lcom/helpshift/common/platform/Platform;->getConversationDAO()Lcom/helpshift/conversation/dao/ConversationDAO;

    move-result-object v3

    iget-object v5, p0, Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    .line 50
    invoke-virtual {v5}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-interface {v3, v5, v6}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationsWithoutMessages(J)Ljava/util/List;

    move-result-object v3

    .line 52
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 54
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 55
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 57
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 59
    iget-object v9, v8, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-static {v9}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 60
    iget-object v9, v8, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-interface {v6, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 62
    :cond_3
    iget-object v9, v8, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-static {v9}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 63
    iget-object v9, v8, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-interface {v7, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 67
    :cond_4
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 68
    iget-object v8, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    .line 69
    iget-object v9, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    const/4 v10, 0x0

    .line 73
    invoke-interface {v6, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 74
    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v10, v8

    check-cast v10, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    goto :goto_2

    .line 76
    :cond_5
    invoke-interface {v7, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 77
    invoke-interface {v7, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v10, v8

    check-cast v10, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    :cond_6
    :goto_2
    if-eqz v10, :cond_a

    .line 82
    iget-object v8, p0, Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v9, p0, Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v11, p0, Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v10, v8, v9, v11}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    .line 86
    iget-object v8, v10, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-interface {v1, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 87
    iget-object v8, v10, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-interface {v1, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/helpshift/conversation/activeconversation/ConversationUpdate;

    goto :goto_3

    .line 90
    :cond_7
    new-instance v8, Lcom/helpshift/conversation/activeconversation/ConversationUpdate;

    invoke-direct {v8}, Lcom/helpshift/conversation/activeconversation/ConversationUpdate;-><init>()V

    .line 93
    :goto_3
    invoke-virtual {v3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v9

    const/4 v11, 0x0

    if-eqz v9, :cond_9

    .line 98
    invoke-virtual {v10}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v9

    if-eqz v9, :cond_8

    .line 99
    invoke-virtual {v10, v3, v11, v8}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->mergePreIssue(Lcom/helpshift/conversation/activeconversation/ConversationDM;ZLcom/helpshift/conversation/activeconversation/ConversationUpdate;)V

    goto :goto_4

    .line 102
    :cond_8
    iget-object v3, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v10, v3, v8}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->mergeMessageFromConversationHistory(Ljava/util/List;Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)V

    goto :goto_4

    .line 106
    :cond_9
    invoke-virtual {v10, v3, v11, v8}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->mergeIssue(Lcom/helpshift/conversation/activeconversation/ConversationDM;ZLcom/helpshift/conversation/activeconversation/ConversationUpdate;)V

    .line 108
    :goto_4
    invoke-interface {v0, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 109
    iget-object v3, v10, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-interface {v1, v3, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 112
    :cond_a
    invoke-virtual {v3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v8

    if-eqz v8, :cond_b

    .line 115
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iput-wide v8, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->lastUserActivityTime:J

    .line 119
    iget-object v8, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v9, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REQUESTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v8, v9, :cond_b

    .line 120
    sget-object v8, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_ACCEPTED:Lcom/helpshift/conversation/dto/IssueState;

    iput-object v8, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    .line 125
    :cond_b
    iget-object v8, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    if-eqz v8, :cond_d

    .line 127
    sget-object v9, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_ACCEPTED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq v8, v9, :cond_c

    sget-object v9, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq v8, v9, :cond_c

    sget-object v9, Lcom/helpshift/conversation/dto/IssueState;->REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq v8, v9, :cond_c

    sget-object v9, Lcom/helpshift/conversation/dto/IssueState;->ARCHIVED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v8, v9, :cond_d

    .line 131
    :cond_c
    iput-boolean v4, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isStartNewConversationClicked:Z

    :cond_d
    if-eqz v8, :cond_e

    .line 136
    iget-boolean v8, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isRedacted:Z

    if-eqz v8, :cond_e

    iget-object v8, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v9, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REQUESTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v8, v9, :cond_e

    .line 138
    iput-boolean v4, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isStartNewConversationClicked:Z

    .line 139
    sget-object v8, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_ACCEPTED:Lcom/helpshift/conversation/dto/IssueState;

    iput-object v8, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    .line 143
    :cond_e
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 148
    :cond_f
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result p1

    if-gt p1, v4, :cond_10

    .line 149
    invoke-interface {v2, v5}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 150
    invoke-virtual {p0, v0, v2, v1}, Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;->putConversations(Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;)V

    return-void

    .line 158
    :cond_10
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 161
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    sub-int/2addr v3, v4

    :goto_5
    if-ltz v3, :cond_13

    .line 162
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 166
    invoke-virtual {v4}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v6

    if-nez v6, :cond_12

    add-int/lit8 v6, v3, -0x1

    :goto_6
    if-ltz v6, :cond_12

    .line 168
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 169
    iget-object v8, v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-static {v8}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_11

    iget-object v8, v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    iget-object v9, v7, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    .line 170
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_11

    iget-object v8, v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    iget-object v9, v7, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    .line 171
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_11

    .line 172
    iget-object v4, v4, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    iget-object v7, v7, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v4, v7}, Lcom/helpshift/common/util/HSObservableList;->addAll(Ljava/util/Collection;)Z

    .line 174
    invoke-interface {v5, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_7

    :cond_11
    add-int/lit8 v6, v6, -0x1

    goto :goto_6

    :cond_12
    :goto_7
    add-int/lit8 v3, v3, -0x1

    goto :goto_5

    .line 182
    :cond_13
    invoke-interface {v2, v5}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 184
    invoke-virtual {p0, v0, v2, v1}, Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;->putConversations(Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;)V

    return-void
.end method

.method putConversations(Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/helpshift/conversation/activeconversation/ConversationUpdate;",
            ">;)V"
        }
    .end annotation

    .line 192
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 194
    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1, v2, v3, v4}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    goto :goto_0

    .line 197
    :cond_0
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 199
    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1, v2, v3, v4}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    goto :goto_1

    .line 202
    :cond_1
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v0}, Lcom/helpshift/common/platform/Platform;->getConversationDAO()Lcom/helpshift/conversation/dao/ConversationDAO;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, v1, p3}, Lcom/helpshift/conversation/dao/ConversationDAO;->updateConversations(Ljava/util/List;Ljava/util/Map;)V

    .line 204
    iget-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationHistoryRemoteDataMerger;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {p1}, Lcom/helpshift/common/platform/Platform;->getConversationDAO()Lcom/helpshift/conversation/dao/ConversationDAO;

    move-result-object p1

    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {p1, p3}, Lcom/helpshift/conversation/dao/ConversationDAO;->insertConversations(Ljava/util/List;)V

    return-void
.end method
