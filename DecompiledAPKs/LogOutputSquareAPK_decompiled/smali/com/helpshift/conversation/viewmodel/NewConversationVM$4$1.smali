.class Lcom/helpshift/conversation/viewmodel/NewConversationVM$4$1;
.super Lcom/helpshift/common/domain/F;
.source "NewConversationVM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/conversation/viewmodel/NewConversationVM$4;->f()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/helpshift/conversation/viewmodel/NewConversationVM$4;

.field final synthetic val$activeConversation:Lcom/helpshift/conversation/activeconversation/ConversationDM;


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/viewmodel/NewConversationVM$4;Lcom/helpshift/conversation/activeconversation/ConversationDM;)V
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM$4$1;->this$1:Lcom/helpshift/conversation/viewmodel/NewConversationVM$4;

    iput-object p2, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM$4$1;->val$activeConversation:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 3

    .line 154
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM$4$1;->this$1:Lcom/helpshift/conversation/viewmodel/NewConversationVM$4;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/NewConversationVM$4;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationVM;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ProgressBarWidget;->setVisible(Z)V

    .line 155
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM$4$1;->this$1:Lcom/helpshift/conversation/viewmodel/NewConversationVM$4;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/NewConversationVM$4;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationVM;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->rendererWeakRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM$4$1;->this$1:Lcom/helpshift/conversation/viewmodel/NewConversationVM$4;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/NewConversationVM$4;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationVM;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->rendererWeakRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM$4$1;->val$activeConversation:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iget-object v1, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->gotoConversation(J)V

    :cond_0
    return-void
.end method
