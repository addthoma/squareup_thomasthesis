.class Lcom/helpshift/conversation/viewmodel/NewConversationVM$10;
.super Lcom/helpshift/common/domain/F;
.source "NewConversationVM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/conversation/viewmodel/NewConversationVM;->setImageAttachment(Lcom/helpshift/conversation/dto/ImagePickerFile;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/conversation/viewmodel/NewConversationVM;

.field final synthetic val$imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/viewmodel/NewConversationVM;Lcom/helpshift/conversation/dto/ImagePickerFile;)V
    .locals 0

    .line 282
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM$10;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationVM;

    iput-object p2, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM$10;->val$imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 2

    .line 285
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM$10;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationVM;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->imageAttachmentWidget:Lcom/helpshift/widget/ImageAttachmentWidget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM$10;->val$imagePickerFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ImageAttachmentWidget;->setImagePickerFile(Lcom/helpshift/conversation/dto/ImagePickerFile;)V

    .line 286
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM$10;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationVM;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationVM$10;->this$0:Lcom/helpshift/conversation/viewmodel/NewConversationVM;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/NewConversationVM;->imageAttachmentWidget:Lcom/helpshift/widget/ImageAttachmentWidget;

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/WidgetGateway;->save(Lcom/helpshift/widget/ImageAttachmentWidget;)V

    return-void
.end method
