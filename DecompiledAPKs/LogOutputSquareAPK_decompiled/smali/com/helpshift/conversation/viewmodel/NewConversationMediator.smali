.class Lcom/helpshift/conversation/viewmodel/NewConversationMediator;
.super Ljava/lang/Object;
.source "NewConversationMediator.java"

# interfaces
.implements Lcom/helpshift/widget/WidgetMediator;


# instance fields
.field attachImageButton:Lcom/helpshift/widget/ButtonWidget;

.field descriptionWidget:Lcom/helpshift/widget/TextWidget;

.field private final domain:Lcom/helpshift/common/domain/Domain;

.field emailWidget:Lcom/helpshift/widget/EmailWidget;

.field imageAttachmentWidget:Lcom/helpshift/widget/ImageAttachmentWidget;

.field nameWidget:Lcom/helpshift/widget/TextWidget;

.field private profileFormWidget:Lcom/helpshift/widget/ProfileFormWidget;

.field progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

.field private renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

.field startConversationButton:Lcom/helpshift/widget/ButtonWidget;


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;Lcom/helpshift/common/domain/Domain;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    .line 42
    iput-object p2, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->domain:Lcom/helpshift/common/domain/Domain;

    return-void
.end method

.method private renderProfileFormWidget()V
    .locals 1

    .line 225
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->profileFormWidget:Lcom/helpshift/widget/ProfileFormWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ProfileFormWidget;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->showProfileForm()V

    goto :goto_0

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->hideProfileForm()V

    :goto_0
    return-void
.end method


# virtual methods
.method public onChanged(Lcom/helpshift/widget/Widget;)V
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator$1;-><init>(Lcom/helpshift/conversation/viewmodel/NewConversationMediator;Lcom/helpshift/widget/Widget;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method renderAll()V
    .locals 0

    .line 124
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderDescription()V

    .line 125
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderName()V

    .line 126
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderEmail()V

    .line 127
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderImageAttachment()V

    .line 128
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderAttachImageButton()V

    .line 129
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderProfileFormWidget()V

    .line 130
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderProgressBarWidget()V

    .line 131
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderStartNewConversationButton()V

    return-void
.end method

.method renderAttachImageButton()V
    .locals 1

    .line 216
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->attachImageButton:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ButtonWidget;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->showImageAttachmentButton()V

    goto :goto_0

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->hideImageAttachmentButton()V

    :goto_0
    return-void
.end method

.method renderDescription()V
    .locals 2

    .line 169
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->descriptionWidget:Lcom/helpshift/widget/TextWidget;

    invoke-virtual {v1}, Lcom/helpshift/widget/TextWidget;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->setDescription(Ljava/lang/String;)V

    .line 170
    sget-object v0, Lcom/helpshift/widget/TextWidget$TextWidgetError;->EMPTY:Lcom/helpshift/widget/TextWidget$TextWidgetError;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->descriptionWidget:Lcom/helpshift/widget/TextWidget;

    invoke-virtual {v1}, Lcom/helpshift/widget/TextWidget;->getError()Lcom/helpshift/widget/TextWidget$TextWidgetError;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/TextWidget$TextWidgetError;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->showDescriptionEmptyError()V

    goto :goto_0

    .line 173
    :cond_0
    sget-object v0, Lcom/helpshift/widget/TextWidget$TextWidgetError;->ONLY_SPECIAL_CHARACTERS:Lcom/helpshift/widget/TextWidget$TextWidgetError;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->descriptionWidget:Lcom/helpshift/widget/TextWidget;

    invoke-virtual {v1}, Lcom/helpshift/widget/TextWidget;->getError()Lcom/helpshift/widget/TextWidget$TextWidgetError;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/TextWidget$TextWidgetError;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->showDescriptionOnlySpecialCharactersError()V

    goto :goto_0

    .line 176
    :cond_1
    sget-object v0, Lcom/helpshift/widget/TextWidget$TextWidgetError;->LESS_THAN_MINIMUM_LENGTH:Lcom/helpshift/widget/TextWidget$TextWidgetError;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->descriptionWidget:Lcom/helpshift/widget/TextWidget;

    invoke-virtual {v1}, Lcom/helpshift/widget/TextWidget;->getError()Lcom/helpshift/widget/TextWidget$TextWidgetError;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/TextWidget$TextWidgetError;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 177
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->showDescriptionLessThanMinimumError()V

    goto :goto_0

    .line 180
    :cond_2
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->clearDescriptionError()V

    :goto_0
    return-void
.end method

.method renderEmail()V
    .locals 2

    .line 153
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->emailWidget:Lcom/helpshift/widget/EmailWidget;

    invoke-virtual {v1}, Lcom/helpshift/widget/EmailWidget;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->setEmail(Ljava/lang/String;)V

    .line 154
    sget-object v0, Lcom/helpshift/widget/TextWidget$TextWidgetError;->INVALID_EMAIL:Lcom/helpshift/widget/TextWidget$TextWidgetError;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->emailWidget:Lcom/helpshift/widget/EmailWidget;

    invoke-virtual {v1}, Lcom/helpshift/widget/EmailWidget;->getError()Lcom/helpshift/widget/TextWidget$TextWidgetError;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/TextWidget$TextWidgetError;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->showEmailInvalidError()V

    goto :goto_0

    .line 157
    :cond_0
    sget-object v0, Lcom/helpshift/widget/TextWidget$TextWidgetError;->EMPTY:Lcom/helpshift/widget/TextWidget$TextWidgetError;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->emailWidget:Lcom/helpshift/widget/EmailWidget;

    invoke-virtual {v1}, Lcom/helpshift/widget/EmailWidget;->getError()Lcom/helpshift/widget/TextWidget$TextWidgetError;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/TextWidget$TextWidgetError;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->showEmailEmptyError()V

    goto :goto_0

    .line 161
    :cond_1
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->clearEmailError()V

    .line 163
    :goto_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->emailWidget:Lcom/helpshift/widget/EmailWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/EmailWidget;->isRequired()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 164
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->setEmailRequired()V

    :cond_2
    return-void
.end method

.method renderImageAttachment()V
    .locals 4

    .line 198
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->imageAttachmentWidget:Lcom/helpshift/widget/ImageAttachmentWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ImageAttachmentWidget;->getImagePickerFile()Lcom/helpshift/conversation/dto/ImagePickerFile;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 199
    iget-object v1, v0, Lcom/helpshift/conversation/dto/ImagePickerFile;->filePath:Ljava/lang/String;

    invoke-static {v1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 203
    :cond_0
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    iget-object v2, v0, Lcom/helpshift/conversation/dto/ImagePickerFile;->filePath:Ljava/lang/String;

    iget-object v3, v0, Lcom/helpshift/conversation/dto/ImagePickerFile;->originalFileName:Ljava/lang/String;

    iget-object v0, v0, Lcom/helpshift/conversation/dto/ImagePickerFile;->originalFileSize:Ljava/lang/Long;

    invoke-interface {v1, v2, v3, v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->showImageAttachmentContainer(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    .line 200
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->hideImageAttachmentContainer()V

    .line 207
    :goto_1
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->imageAttachmentWidget:Lcom/helpshift/widget/ImageAttachmentWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ImageAttachmentWidget;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 208
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->enableImageAttachmentClickable()V

    goto :goto_2

    .line 211
    :cond_2
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->disableImageAttachmentClickable()V

    :goto_2
    return-void
.end method

.method renderName()V
    .locals 2

    .line 185
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->nameWidget:Lcom/helpshift/widget/TextWidget;

    invoke-virtual {v1}, Lcom/helpshift/widget/TextWidget;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->setName(Ljava/lang/String;)V

    .line 186
    sget-object v0, Lcom/helpshift/widget/TextWidget$TextWidgetError;->EMPTY:Lcom/helpshift/widget/TextWidget$TextWidgetError;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->nameWidget:Lcom/helpshift/widget/TextWidget;

    invoke-virtual {v1}, Lcom/helpshift/widget/TextWidget;->getError()Lcom/helpshift/widget/TextWidget$TextWidgetError;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/TextWidget$TextWidgetError;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->showNameEmptyError()V

    goto :goto_0

    .line 189
    :cond_0
    sget-object v0, Lcom/helpshift/widget/TextWidget$TextWidgetError;->ONLY_SPECIAL_CHARACTERS:Lcom/helpshift/widget/TextWidget$TextWidgetError;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->nameWidget:Lcom/helpshift/widget/TextWidget;

    invoke-virtual {v1}, Lcom/helpshift/widget/TextWidget;->getError()Lcom/helpshift/widget/TextWidget$TextWidgetError;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/TextWidget$TextWidgetError;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 190
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->showNameOnlySpecialCharactersError()V

    goto :goto_0

    .line 193
    :cond_1
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->clearNameError()V

    :goto_0
    return-void
.end method

.method renderProgressBarWidget()V
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ProgressBarWidget;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->showProgressBar()V

    goto :goto_0

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->hideProgressBar()V

    :goto_0
    return-void
.end method

.method renderStartNewConversationButton()V
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->startConversationButton:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ButtonWidget;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->showStartConversationButton()V

    goto :goto_0

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;->hideStartConversationButton()V

    :goto_0
    return-void
.end method

.method setAttachImageButton(Lcom/helpshift/widget/ButtonWidget;)V
    .locals 0

    .line 75
    invoke-virtual {p1, p0}, Lcom/helpshift/widget/ButtonWidget;->setMediator(Lcom/helpshift/widget/WidgetMediator;)V

    .line 76
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->attachImageButton:Lcom/helpshift/widget/ButtonWidget;

    return-void
.end method

.method setDescriptionWidget(Lcom/helpshift/widget/TextWidget;)V
    .locals 0

    .line 50
    invoke-virtual {p1, p0}, Lcom/helpshift/widget/TextWidget;->setMediator(Lcom/helpshift/widget/WidgetMediator;)V

    .line 51
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->descriptionWidget:Lcom/helpshift/widget/TextWidget;

    return-void
.end method

.method setEmailWidget(Lcom/helpshift/widget/EmailWidget;)V
    .locals 0

    .line 60
    invoke-virtual {p1, p0}, Lcom/helpshift/widget/EmailWidget;->setMediator(Lcom/helpshift/widget/WidgetMediator;)V

    .line 61
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->emailWidget:Lcom/helpshift/widget/EmailWidget;

    return-void
.end method

.method setImageAttachmentWidget(Lcom/helpshift/widget/ImageAttachmentWidget;)V
    .locals 0

    .line 70
    invoke-virtual {p1, p0}, Lcom/helpshift/widget/ImageAttachmentWidget;->setMediator(Lcom/helpshift/widget/WidgetMediator;)V

    .line 71
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->imageAttachmentWidget:Lcom/helpshift/widget/ImageAttachmentWidget;

    return-void
.end method

.method setNameWidget(Lcom/helpshift/widget/TextWidget;)V
    .locals 0

    .line 55
    invoke-virtual {p1, p0}, Lcom/helpshift/widget/TextWidget;->setMediator(Lcom/helpshift/widget/WidgetMediator;)V

    .line 56
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->nameWidget:Lcom/helpshift/widget/TextWidget;

    return-void
.end method

.method setProfileFormWidget(Lcom/helpshift/widget/ProfileFormWidget;)V
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->profileFormWidget:Lcom/helpshift/widget/ProfileFormWidget;

    return-void
.end method

.method setProgressBarWidget(Lcom/helpshift/widget/ProgressBarWidget;)V
    .locals 0

    .line 80
    invoke-virtual {p1, p0}, Lcom/helpshift/widget/ProgressBarWidget;->setMediator(Lcom/helpshift/widget/WidgetMediator;)V

    .line 81
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    return-void
.end method

.method setRenderer(Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->renderer:Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;

    return-void
.end method

.method setStartConversationButton(Lcom/helpshift/widget/ButtonWidget;)V
    .locals 0

    .line 65
    invoke-virtual {p1, p0}, Lcom/helpshift/widget/ButtonWidget;->setMediator(Lcom/helpshift/widget/WidgetMediator;)V

    .line 66
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->startConversationButton:Lcom/helpshift/widget/ButtonWidget;

    return-void
.end method

.method updateAttachImageButtonWidget()V
    .locals 2

    .line 234
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ProgressBarWidget;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->attachImageButton:Lcom/helpshift/widget/ButtonWidget;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    goto :goto_0

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->attachImageButton:Lcom/helpshift/widget/ButtonWidget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->imageAttachmentWidget:Lcom/helpshift/widget/ImageAttachmentWidget;

    invoke-virtual {v1}, Lcom/helpshift/widget/ImageAttachmentWidget;->getImagePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    :goto_0
    return-void
.end method

.method updateImageAttachmentWidget()V
    .locals 2

    .line 252
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->imageAttachmentWidget:Lcom/helpshift/widget/ImageAttachmentWidget;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    invoke-virtual {v1}, Lcom/helpshift/widget/ProgressBarWidget;->isVisible()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ImageAttachmentWidget;->setClickable(Z)V

    return-void
.end method

.method updateStartNewConversationButtonWidget()V
    .locals 2

    .line 243
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->progressBarWidget:Lcom/helpshift/widget/ProgressBarWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ProgressBarWidget;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->startConversationButton:Lcom/helpshift/widget/ButtonWidget;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    goto :goto_0

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/NewConversationMediator;->startConversationButton:Lcom/helpshift/widget/ButtonWidget;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    :goto_0
    return-void
.end method
