.class public interface abstract Lcom/helpshift/migration/LegacyAnalyticsEventIDDAO;
.super Ljava/lang/Object;
.source "LegacyAnalyticsEventIDDAO.java"


# virtual methods
.method public abstract getLegacyAnalyticsEventId(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract storeLegacyAnalyticsEventIDs(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/common/platform/network/KeyValuePair;",
            ">;)V"
        }
    .end annotation
.end method
