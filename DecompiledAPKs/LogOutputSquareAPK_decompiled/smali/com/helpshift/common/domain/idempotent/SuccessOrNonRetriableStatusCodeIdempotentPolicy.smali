.class public Lcom/helpshift/common/domain/idempotent/SuccessOrNonRetriableStatusCodeIdempotentPolicy;
.super Lcom/helpshift/common/domain/idempotent/BaseIdempotentPolicy;
.source "SuccessOrNonRetriableStatusCodeIdempotentPolicy.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/helpshift/common/domain/idempotent/BaseIdempotentPolicy;-><init>()V

    return-void
.end method


# virtual methods
.method shouldMarkRequestCompleted(I)Z
    .locals 2

    const/4 v0, 0x1

    const/16 v1, 0xc8

    if-lt p1, v1, :cond_0

    const/16 v1, 0x12c

    if-ge p1, v1, :cond_0

    return v0

    .line 18
    :cond_0
    sget-object v1, Lcom/helpshift/common/domain/network/NetworkErrorCodes;->NOT_RETRIABLE_STATUS_CODES:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    return v0

    :cond_1
    const/4 p1, 0x0

    return p1
.end method
