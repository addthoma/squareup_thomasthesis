.class public Lcom/helpshift/support/conversations/ConversationFragment;
.super Lcom/helpshift/support/conversations/BaseConversationFragment;
.source "ConversationFragment.java"

# interfaces
.implements Lcom/helpshift/support/conversations/messages/MessagesAdapterClickListener;
.implements Lcom/helpshift/support/conversations/ConversationFragmentRouter;
.implements Lcom/helpshift/support/fragments/IMenuItemEventListener;
.implements Lcom/helpshift/support/conversations/HSRecyclerViewScrollListener$RecyclerViewScrollCallback;


# static fields
.field public static final BUNDLE_ARG_CONVERSATION_LOCAL_ID:Ljava/lang/String; = "issueId"

.field public static final BUNDLE_ARG_SHOW_CONVERSATION_HISTORY:Ljava/lang/String; = "show_conv_history"

.field public static final FRAGMENT_TAG:Ljava/lang/String; = "HSConversationFragment"

.field private static final TAG:Ljava/lang/String; = "Helpshift_ConvFragment"


# instance fields
.field private final SHOULD_SHOW_UNREAD_MESSAGE_INDICATOR:Ljava/lang/String;

.field protected conversationId:Ljava/lang/Long;

.field conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

.field private hsRecyclerViewScrollListener:Lcom/helpshift/support/conversations/HSRecyclerViewScrollListener;

.field private imageRefersId:Ljava/lang/String;

.field private isConversationVMInitialized:Z

.field private lastSoftInputMode:I

.field private lastWindowFlags:I

.field private messagesRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private readableAttachmentMessage:Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;

.field protected renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

.field protected retainMessageBoxOnUI:Z

.field private selectedImageFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

.field private selectedImageRefersId:Ljava/lang/String;

.field private shouldUpdateAttachment:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 60
    invoke-direct {p0}, Lcom/helpshift/support/conversations/BaseConversationFragment;-><init>()V

    const-string v0, "should_show_unread_message_indicator"

    .line 68
    iput-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->SHOULD_SHOW_UNREAD_MESSAGE_INDICATOR:Ljava/lang/String;

    const/4 v0, 0x0

    .line 80
    iput-boolean v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->isConversationVMInitialized:Z

    return-void
.end method

.method static synthetic access$000(Lcom/helpshift/support/conversations/ConversationFragment;Ljava/lang/String;)V
    .locals 0

    .line 60
    invoke-virtual {p0, p1}, Lcom/helpshift/support/conversations/ConversationFragment;->copyToClipboard(Ljava/lang/String;)V

    return-void
.end method

.method private checkWriteStoragePermissionAndDelegateToVM(ZLcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;)V
    .locals 2

    const/4 v0, 0x0

    .line 376
    iput-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->readableAttachmentMessage:Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;

    if-eqz p1, :cond_3

    .line 378
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getPlatform()Lcom/helpshift/common/platform/Platform;

    move-result-object p1

    invoke-interface {p1}, Lcom/helpshift/common/platform/Platform;->getDevice()Lcom/helpshift/common/platform/Device;

    move-result-object p1

    .line 379
    sget-object v0, Lcom/helpshift/support/conversations/ConversationFragment$5;->$SwitchMap$com$helpshift$common$platform$Device$PermissionState:[I

    sget-object v1, Lcom/helpshift/common/platform/Device$PermissionType;->WRITE_STORAGE:Lcom/helpshift/common/platform/Device$PermissionType;

    .line 380
    invoke-interface {p1, v1}, Lcom/helpshift/common/platform/Device;->checkPermission(Lcom/helpshift/common/platform/Device$PermissionType;)Lcom/helpshift/common/platform/Device$PermissionState;

    move-result-object p1

    .line 379
    invoke-virtual {p1}, Lcom/helpshift/common/platform/Device$PermissionState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    goto :goto_0

    .line 388
    :cond_0
    iput-object p2, p0, Lcom/helpshift/support/conversations/ConversationFragment;->readableAttachmentMessage:Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;

    .line 389
    invoke-virtual {p0, v0}, Lcom/helpshift/support/conversations/ConversationFragment;->requestWriteExternalStoragePermission(Z)V

    goto :goto_0

    .line 385
    :cond_1
    iget-object p1, p2, Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;->attachmentUrl:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/helpshift/support/conversations/ConversationFragment;->startDownloadWithSystemService(Ljava/lang/String;)V

    goto :goto_0

    .line 382
    :cond_2
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {p1, p2}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->handleAdminAttachmentMessageClick(Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;)V

    goto :goto_0

    .line 394
    :cond_3
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {p1, p2}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->handleAdminAttachmentMessageClick(Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;)V

    :goto_0
    return-void
.end method

.method public static newInstance(Landroid/os/Bundle;)Lcom/helpshift/support/conversations/ConversationFragment;
    .locals 1

    .line 88
    new-instance v0, Lcom/helpshift/support/conversations/ConversationFragment;

    invoke-direct {v0}, Lcom/helpshift/support/conversations/ConversationFragment;-><init>()V

    .line 89
    invoke-virtual {v0, p0}, Lcom/helpshift/support/conversations/ConversationFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private startDownloadWithSystemService(Ljava/lang/String;)V
    .locals 2

    .line 418
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "download"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    if-nez v0, :cond_0

    return-void

    .line 424
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 425
    new-instance v1, Landroid/app/DownloadManager$Request;

    invoke-direct {v1, p1}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    const/4 p1, 0x1

    .line 426
    invoke-virtual {v1, p1}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    .line 427
    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    .line 428
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->isDetached()Z

    move-result p1

    if-nez p1, :cond_1

    .line 429
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getView()Landroid/view/View;

    move-result-object p1

    sget v0, Lcom/helpshift/R$string;->hs__starting_download:I

    const/4 v1, -0x1

    invoke-static {p1, v0, v1}, Lcom/helpshift/support/util/SnackbarUtil;->showSnackbar(Landroid/view/View;II)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected getScreenshotMode()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method protected getToolbarTitle()Ljava/lang/String;
    .locals 1

    .line 289
    sget v0, Lcom/helpshift/R$string;->hs__conversation_header:I

    invoke-virtual {p0, v0}, Lcom/helpshift/support/conversations/ConversationFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getViewName()Lcom/helpshift/support/util/AppSessionConstants$Screen;
    .locals 1

    .line 249
    sget-object v0, Lcom/helpshift/support/util/AppSessionConstants$Screen;->CONVERSATION:Lcom/helpshift/support/util/AppSessionConstants$Screen;

    return-object v0
.end method

.method public handleAdminImageAttachmentMessageClick(Lcom/helpshift/conversation/activeconversation/message/AdminImageAttachmentMessageDM;)V
    .locals 1

    const/4 v0, 0x1

    .line 344
    invoke-direct {p0, v0, p1}, Lcom/helpshift/support/conversations/ConversationFragment;->checkWriteStoragePermissionAndDelegateToVM(ZLcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;)V

    return-void
.end method

.method public handleGenericAttachmentMessageClick(Lcom/helpshift/conversation/activeconversation/message/AdminAttachmentMessageDM;)V
    .locals 1

    .line 338
    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/message/AdminAttachmentMessageDM;->isWriteStoragePermissionRequired()Z

    move-result v0

    invoke-direct {p0, v0, p1}, Lcom/helpshift/support/conversations/ConversationFragment;->checkWriteStoragePermissionAndDelegateToVM(ZLcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;)V

    return-void
.end method

.method public handleOptionSelected(Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Option;Z)V
    .locals 0

    return-void
.end method

.method public handleReplyReviewButtonClick(Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;)V
    .locals 1

    .line 328
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->handleAppReviewRequestClick(Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;)V

    return-void
.end method

.method public handleScreenshotAction(Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$ScreenshotAction;Lcom/helpshift/conversation/dto/ImagePickerFile;Ljava/lang/String;)Z
    .locals 1

    .line 270
    sget-object v0, Lcom/helpshift/support/conversations/ConversationFragment$5;->$SwitchMap$com$helpshift$support$fragments$ScreenshotPreviewFragment$ScreenshotAction:[I

    invoke-virtual {p1}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$ScreenshotAction;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 273
    :cond_0
    iget-boolean p1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->isConversationVMInitialized:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    if-eqz p1, :cond_1

    .line 274
    invoke-virtual {p1, p2, p3}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->sendScreenShot(Lcom/helpshift/conversation/dto/ImagePickerFile;Ljava/lang/String;)V

    goto :goto_0

    .line 278
    :cond_1
    iput-object p2, p0, Lcom/helpshift/support/conversations/ConversationFragment;->selectedImageFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

    .line 279
    iput-object p3, p0, Lcom/helpshift/support/conversations/ConversationFragment;->selectedImageRefersId:Ljava/lang/String;

    .line 280
    iput-boolean v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->shouldUpdateAttachment:Z

    :goto_0
    return v0
.end method

.method protected inflateReplyBoxView(Landroid/view/View;)V
    .locals 1

    .line 127
    sget v0, Lcom/helpshift/R$id;->replyBoxViewStub:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewStub;

    .line 128
    sget v0, Lcom/helpshift/R$layout;->hs__conversation_replyboxview:I

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 129
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    return-void
.end method

.method protected initConversationVM()V
    .locals 4

    .line 133
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationId:Ljava/lang/Long;

    iget-object v2, p0, Lcom/helpshift/support/conversations/ConversationFragment;->renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    iget-boolean v3, p0, Lcom/helpshift/support/conversations/ConversationFragment;->retainMessageBoxOnUI:Z

    invoke-interface {v0, v1, v2, v3}, Lcom/helpshift/CoreApi;->getConversationViewModel(Ljava/lang/Long;Lcom/helpshift/conversation/activeconversation/ConversationRenderer;Z)Lcom/helpshift/conversation/viewmodel/ConversationVM;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    return-void
.end method

.method protected initRenderer(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 10

    .line 139
    new-instance v9, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 141
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getView()Landroid/view/View;

    move-result-object v3

    .line 144
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getSupportFragment()Lcom/helpshift/support/fragments/SupportFragment;

    move-result-object v8

    move-object v0, v9

    move-object v2, p1

    move-object v4, p2

    move-object v5, p0

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v8}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;-><init>(Landroid/content/Context;Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;Landroid/view/View;Lcom/helpshift/support/conversations/ConversationFragmentRouter;Landroid/view/View;Landroid/view/View;Lcom/helpshift/support/fragments/IToolbarMenuItemRenderer;)V

    iput-object v9, p0, Lcom/helpshift/support/conversations/ConversationFragment;->renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    return-void
.end method

.method protected initialize(Landroid/view/View;)V
    .locals 7

    .line 149
    sget v0, Lcom/helpshift/R$id;->hs__messagesList:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->messagesRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 150
    sget v0, Lcom/helpshift/R$id;->hs__confirmation:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 151
    sget v1, Lcom/helpshift/R$id;->scroll_indicator:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 152
    sget v2, Lcom/helpshift/R$id;->unread_indicator_red_dot:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 153
    sget v3, Lcom/helpshift/R$id;->unread_indicator_red_dot_image_view:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 158
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-ge v4, v5, :cond_0

    .line 159
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/helpshift/R$drawable;->hs__ring:I

    invoke-static {v4, v5}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 160
    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 161
    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 164
    :cond_0
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/helpshift/R$drawable;->hs__circle:I

    sget v6, Lcom/helpshift/R$attr;->colorAccent:I

    invoke-static {v4, v3, v5, v6}, Lcom/helpshift/util/Styles;->setDrawable(Landroid/content/Context;Landroid/view/View;II)V

    .line 167
    iget-object v3, p0, Lcom/helpshift/support/conversations/ConversationFragment;->messagesRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p0, v3, v0, v1, v2}, Lcom/helpshift/support/conversations/ConversationFragment;->initRenderer(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    .line 168
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->initConversationVM()V

    .line 169
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    invoke-virtual {v0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->setReplyboxListeners()V

    const/4 v0, 0x0

    .line 175
    iput-boolean v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->retainMessageBoxOnUI:Z

    .line 177
    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->startLiveUpdates()V

    const/4 v1, 0x1

    .line 180
    iput-boolean v1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->isConversationVMInitialized:Z

    .line 183
    iget-boolean v1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->shouldUpdateAttachment:Z

    if-eqz v1, :cond_1

    .line 184
    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iget-object v2, p0, Lcom/helpshift/support/conversations/ConversationFragment;->selectedImageFile:Lcom/helpshift/conversation/dto/ImagePickerFile;

    iget-object v3, p0, Lcom/helpshift/support/conversations/ConversationFragment;->selectedImageRefersId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->sendScreenShot(Lcom/helpshift/conversation/dto/ImagePickerFile;Ljava/lang/String;)V

    .line 185
    iput-boolean v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->shouldUpdateAttachment:Z

    .line 188
    :cond_1
    sget v0, Lcom/helpshift/R$id;->resolution_accepted_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/helpshift/support/conversations/ConversationFragment$1;

    invoke-direct {v1, p0}, Lcom/helpshift/support/conversations/ConversationFragment$1;-><init>(Lcom/helpshift/support/conversations/ConversationFragment;)V

    .line 189
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    sget v0, Lcom/helpshift/R$id;->resolution_rejected_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/helpshift/support/conversations/ConversationFragment$2;

    invoke-direct {v1, p0}, Lcom/helpshift/support/conversations/ConversationFragment$2;-><init>(Lcom/helpshift/support/conversations/ConversationFragment;)V

    .line 197
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    sget v0, Lcom/helpshift/R$id;->scroll_jump_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageButton;

    .line 207
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/helpshift/R$drawable;->hs__circle_shape_scroll_jump:I

    sget v2, Lcom/helpshift/R$attr;->hs__composeBackgroundColor:I

    invoke-static {v0, p1, v1, v2}, Lcom/helpshift/util/Styles;->setDrawable(Landroid/content/Context;Landroid/view/View;II)V

    .line 209
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget v2, Lcom/helpshift/R$attr;->hs__selectableOptionColor:I

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/Styles;->setColorFilter(Landroid/content/Context;Landroid/graphics/drawable/Drawable;I)V

    .line 211
    new-instance v0, Lcom/helpshift/support/conversations/ConversationFragment$3;

    invoke-direct {v0, p0}, Lcom/helpshift/support/conversations/ConversationFragment$3;-><init>(Lcom/helpshift/support/conversations/ConversationFragment;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 218
    new-instance p1, Lcom/helpshift/support/conversations/HSRecyclerViewScrollListener;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p1, v0, p0}, Lcom/helpshift/support/conversations/HSRecyclerViewScrollListener;-><init>(Landroid/os/Handler;Lcom/helpshift/support/conversations/HSRecyclerViewScrollListener$RecyclerViewScrollCallback;)V

    iput-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->hsRecyclerViewScrollListener:Lcom/helpshift/support/conversations/HSRecyclerViewScrollListener;

    .line 219
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->messagesRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->hsRecyclerViewScrollListener:Lcom/helpshift/support/conversations/HSRecyclerViewScrollListener;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    return-void
.end method

.method public launchImagePicker(Lcom/helpshift/conversation/activeconversation/message/RequestScreenshotMessageDM;)V
    .locals 2

    .line 318
    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/message/RequestScreenshotMessageDM;->serverId:Ljava/lang/String;

    iput-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->imageRefersId:Ljava/lang/String;

    .line 319
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->onImageAttachmentButtonClick()V

    .line 320
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 321
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getScreenshotMode()I

    move-result v0

    const-string v1, "key_screenshot_mode"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 322
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->imageRefersId:Ljava/lang/String;

    const-string v1, "key_refers_id"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getSupportFragment()Lcom/helpshift/support/fragments/SupportFragment;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/helpshift/support/fragments/SupportFragment;->launchImagePicker(ZLandroid/os/Bundle;)V

    return-void
.end method

.method public onAdminMessageLinkClicked(Ljava/lang/String;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 1

    .line 294
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0, p1, p2}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->onAdminMessageLinkClicked(Ljava/lang/String;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    return-void
.end method

.method public onAdminSuggestedQuestionSelected(Lcom/helpshift/conversation/activeconversation/message/MessageDM;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 366
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getSupportController()Lcom/helpshift/support/controllers/SupportController;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, p2, p3, v0}, Lcom/helpshift/support/controllers/SupportController;->onAdminSuggestedQuestionSelected(Ljava/lang/String;Ljava/lang/String;Lcom/helpshift/support/fragments/SingleQuestionFragment$QuestionReadListener;)V

    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 0

    .line 95
    invoke-super {p0, p1}, Lcom/helpshift/support/conversations/BaseConversationFragment;->onAttach(Landroid/content/Context;)V

    .line 96
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->isChangingConfigurations()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    if-eqz p1, :cond_0

    .line 97
    invoke-virtual {p1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->isReplyBoxVisible()Z

    move-result p1

    iput-boolean p1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->retainMessageBoxOnUI:Z

    :cond_0
    return-void
.end method

.method public onAuthenticationFailure()V
    .locals 1

    .line 528
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getSupportController()Lcom/helpshift/support/controllers/SupportController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/support/controllers/SupportController;->onAuthenticationFailure()V

    return-void
.end method

.method public onCSATSurveySubmitted(ILjava/lang/String;)V
    .locals 1

    .line 356
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0, p1, p2}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->onCSATSurveySubmitted(ILjava/lang/String;)V

    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Ljava/lang/String;)V
    .locals 2

    .line 299
    invoke-static {p2}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 300
    sget v0, Lcom/helpshift/R$string;->hs__copy:I

    const/4 v1, 0x0

    invoke-interface {p1, v1, v1, v1, v0}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object p1

    .line 301
    new-instance v0, Lcom/helpshift/support/conversations/ConversationFragment$4;

    invoke-direct {v0, p0, p2}, Lcom/helpshift/support/conversations/ConversationFragment$4;-><init>(Lcom/helpshift/support/conversations/ConversationFragment;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_0
    return-void
.end method

.method public onCreateOptionMenuCalled()V
    .locals 1

    .line 484
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->renderMenuItems()V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .line 104
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p3

    invoke-virtual {p3}, Landroidx/fragment/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object p3

    invoke-virtual {p3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object p3

    iget p3, p3, Landroid/view/WindowManager$LayoutParams;->flags:I

    iput p3, p0, Lcom/helpshift/support/conversations/ConversationFragment;->lastWindowFlags:I

    .line 105
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p3

    invoke-virtual {p3}, Landroidx/fragment/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object p3

    const/16 v0, 0x800

    invoke-virtual {p3, v0}, Landroid/view/Window;->addFlags(I)V

    .line 106
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p3

    invoke-virtual {p3}, Landroidx/fragment/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object p3

    const/16 v0, 0x400

    invoke-virtual {p3, v0}, Landroid/view/Window;->clearFlags(I)V

    .line 107
    sget p3, Lcom/helpshift/R$layout;->hs__conversation_fragment:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onDestroyView()V
    .locals 2

    .line 448
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 449
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x800

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 450
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget v1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->lastWindowFlags:I

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    :cond_0
    const/4 v0, 0x0

    .line 452
    iput-boolean v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->isConversationVMInitialized:Z

    .line 453
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->setConversationViewState(I)V

    .line 454
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    invoke-virtual {v0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->unregisterFragmentRenderer()V

    .line 455
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->unregisterRenderer()V

    .line 456
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    invoke-virtual {v0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->destroy()V

    .line 459
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->messagesRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->hsRecyclerViewScrollListener:Lcom/helpshift/support/conversations/HSRecyclerViewScrollListener;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->removeOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    const/4 v0, 0x0

    .line 460
    iput-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->messagesRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 461
    invoke-super {p0}, Lcom/helpshift/support/conversations/BaseConversationFragment;->onDestroyView()V

    return-void
.end method

.method public onDetach()V
    .locals 2

    .line 440
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 441
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getConversationInboxPoller()Lcom/helpshift/conversation/ConversationInboxPoller;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/ConversationInboxPoller;->startAppPoller(Z)V

    .line 443
    :cond_0
    invoke-super {p0}, Lcom/helpshift/support/conversations/BaseConversationFragment;->onDetach()V

    return-void
.end method

.method public onHistoryLoadingRetryClicked()V
    .locals 1

    .line 371
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->retryHistoryLoadingMessages()V

    return-void
.end method

.method public onMenuItemClicked(Lcom/helpshift/support/fragments/HSMenuItemType;)V
    .locals 4

    .line 489
    sget-object v0, Lcom/helpshift/support/conversations/ConversationFragment$5;->$SwitchMap$com$helpshift$support$fragments$HSMenuItemType:[I

    invoke-virtual {p1}, Lcom/helpshift/support/fragments/HSMenuItemType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 491
    iput-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->imageRefersId:Ljava/lang/String;

    .line 492
    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->onImageAttachmentButtonClick()V

    .line 493
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 494
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getScreenshotMode()I

    move-result v2

    const-string v3, "key_screenshot_mode"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "key_refers_id"

    .line 495
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getSupportFragment()Lcom/helpshift/support/fragments/SupportFragment;

    move-result-object p1

    invoke-virtual {p1, v0, v1}, Lcom/helpshift/support/fragments/SupportFragment;->launchImagePicker(ZLandroid/os/Bundle;)V

    :goto_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .line 241
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget v1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->lastSoftInputMode:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 242
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    invoke-virtual {v0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->hideKeyboard()V

    .line 243
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->onPause()V

    .line 244
    invoke-super {p0}, Lcom/helpshift/support/conversations/BaseConversationFragment;->onPause()V

    return-void
.end method

.method protected onPermissionGranted(I)V
    .locals 2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 408
    :cond_0
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->readableAttachmentMessage:Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;

    if-eqz p1, :cond_2

    .line 409
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->handleAdminAttachmentMessageClick(Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;)V

    const/4 p1, 0x0

    .line 410
    iput-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->readableAttachmentMessage:Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;

    goto :goto_0

    .line 402
    :cond_1
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 403
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getScreenshotMode()I

    move-result v0

    const-string v1, "key_screenshot_mode"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 404
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->imageRefersId:Ljava/lang/String;

    const-string v1, "key_refers_id"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getSupportFragment()Lcom/helpshift/support/fragments/SupportFragment;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/helpshift/support/fragments/SupportFragment;->launchImagePicker(ZLandroid/os/Bundle;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .line 224
    invoke-super {p0}, Lcom/helpshift/support/conversations/BaseConversationFragment;->onResume()V

    .line 225
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->onResume()V

    .line 226
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    iput v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->lastSoftInputMode:I

    .line 227
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 228
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 230
    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    .line 231
    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 232
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "id"

    .line 233
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    sget-object v2, Lcom/helpshift/analytics/AnalyticsEventType;->OPEN_ISSUE:Lcom/helpshift/analytics/AnalyticsEventType;

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->pushAnalyticsEvent(Lcom/helpshift/analytics/AnalyticsEventType;Ljava/util/Map;)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 466
    invoke-super {p0, p1}, Lcom/helpshift/support/conversations/BaseConversationFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 467
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->shouldShowUnreadMessagesIndicator()Z

    move-result v0

    const-string v1, "should_show_unread_message_indicator"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public onScreenshotMessageClicked(Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;)V
    .locals 1

    .line 333
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->handleScreenshotMessageClick(Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;)V

    return-void
.end method

.method public onScrolledToBottom()V
    .locals 1

    .line 538
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->onScrolledToBottom()V

    return-void
.end method

.method public onScrolledToTop()V
    .locals 1

    .line 533
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->onScrolledToTop()V

    return-void
.end method

.method public onScrolling()V
    .locals 1

    .line 543
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->onScrolling()V

    return-void
.end method

.method public onSendButtonClick()V
    .locals 1

    .line 513
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->sendTextMessage()V

    return-void
.end method

.method public onSkipClick()V
    .locals 1

    .line 518
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->onSkipClick()V

    return-void
.end method

.method public onStartNewConversationButtonClick()V
    .locals 1

    .line 350
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->onNewConversationButtonClicked()V

    .line 351
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    invoke-virtual {v0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->openFreshConversationScreen()V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    if-eqz p1, :cond_0

    .line 503
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 504
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    invoke-virtual {p1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->enableSendReplyButton()V

    goto :goto_0

    .line 507
    :cond_0
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragment;->renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    invoke-virtual {p1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->disableSendReplyButton()V

    :goto_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .line 113
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "issueId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationId:Ljava/lang/Long;

    .line 114
    invoke-virtual {p0, p1}, Lcom/helpshift/support/conversations/ConversationFragment;->inflateReplyBoxView(Landroid/view/View;)V

    .line 115
    invoke-virtual {p0, p1}, Lcom/helpshift/support/conversations/ConversationFragment;->initialize(Landroid/view/View;)V

    .line 116
    invoke-super {p0, p1, p2}, Lcom/helpshift/support/conversations/BaseConversationFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    if-eqz p2, :cond_0

    const-string p1, "should_show_unread_message_indicator"

    .line 119
    invoke-virtual {p2, p1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    .line 121
    iget-object p2, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {p2, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->updateUnreadMessageCountIndicator(Z)V

    :cond_0
    const-string p1, "Helpshift_ConvFragment"

    const-string p2, "Now showing conversation screen"

    .line 123
    invoke-static {p1, p2}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public openFreshConversationScreen()V
    .locals 1

    .line 435
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->getSupportFragment()Lcom/helpshift/support/fragments/SupportFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/support/fragments/SupportFragment;->getSupportController()Lcom/helpshift/support/controllers/SupportController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/support/controllers/SupportController;->startConversationFlow()V

    return-void
.end method

.method public retryMessage(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->retryMessage(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    return-void
.end method

.method public retryPreIssueCreation()V
    .locals 0

    return-void
.end method

.method public startLiveUpdates()V
    .locals 1

    .line 471
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    if-eqz v0, :cond_0

    .line 472
    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->startLiveUpdates()V

    :cond_0
    return-void
.end method

.method public stopLiveUpdates()V
    .locals 1

    .line 477
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    if-eqz v0, :cond_0

    .line 478
    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->stopLiveUpdates()V

    :cond_0
    return-void
.end method
