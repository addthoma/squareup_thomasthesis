.class public Lcom/helpshift/support/fragments/SupportFragment;
.super Lcom/helpshift/support/fragments/MainFragment;
.source "SupportFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/helpshift/support/contracts/SupportScreenView;
.implements Lcom/helpshift/common/FetchDataFromThread;
.implements Lcom/helpshift/support/widget/ImagePicker$ImagePickerListener;
.implements Landroid/view/MenuItem$OnMenuItemClickListener;
.implements Lcom/helpshift/support/fragments/IToolbarMenuItemRenderer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/helpshift/support/fragments/SupportFragment$SupportModes;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/helpshift/support/fragments/MainFragment;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/helpshift/support/contracts/SupportScreenView;",
        "Lcom/helpshift/common/FetchDataFromThread<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;",
        "Lcom/helpshift/support/widget/ImagePicker$ImagePickerListener;",
        "Landroid/view/MenuItem$OnMenuItemClickListener;",
        "Lcom/helpshift/support/fragments/IToolbarMenuItemRenderer;"
    }
.end annotation


# static fields
.field public static final SUPPORT_MODE:Ljava/lang/String; = "support_mode"

.field private static final TAG:Ljava/lang/String; = "Helpshift_SupportFrag"

.field private static isForeground:Z


# instance fields
.field private attachImageMenuItem:Landroid/view/MenuItem;

.field contactUsMenuItem:Landroid/view/MenuItem;

.field private doneMenuItem:Landroid/view/MenuItem;

.field private faqLoaded:Z

.field private fragmentMenuItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private handleNewIntent:Z

.field private imagePicker:Lcom/helpshift/support/widget/ImagePicker;

.field private menuItemEventListener:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/helpshift/support/fragments/IMenuItemEventListener;",
            ">;"
        }
    .end annotation
.end field

.field private menuItemsPrepared:Z

.field private newIntentData:Landroid/os/Bundle;

.field private newMessageCount:I

.field private searchMenuItem:Landroid/view/MenuItem;

.field private searchView:Landroidx/appcompat/widget/SearchView;

.field private startNewConversationMenuItem:Landroid/view/MenuItem;

.field private supportController:Lcom/helpshift/support/controllers/SupportController;

.field private toolbar:Landroidx/appcompat/widget/Toolbar;

.field private toolbarId:I

.field private toolbarImportanceForAccessibility:I

.field private viewFaqsLoadError:Landroid/view/View;

.field private viewFaqsLoading:Landroid/view/View;

.field private viewNoFaqs:Landroid/view/View;

.field private final visibleFragments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 85
    invoke-direct {p0}, Lcom/helpshift/support/fragments/MainFragment;-><init>()V

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->visibleFragments:Ljava/util/List;

    const/4 v0, 0x0

    .line 107
    iput v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->newMessageCount:I

    return-void
.end method

.method private attachMenuListeners(Landroid/view/Menu;)V
    .locals 2

    .line 212
    sget v0, Lcom/helpshift/R$id;->hs__search:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->searchMenuItem:Landroid/view/MenuItem;

    .line 213
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->searchMenuItem:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/helpshift/views/HSMenuItemCompat;->getActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/SearchView;

    iput-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->searchView:Landroidx/appcompat/widget/SearchView;

    .line 215
    sget v0, Lcom/helpshift/R$id;->hs__contact_us:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->contactUsMenuItem:Landroid/view/MenuItem;

    .line 216
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->contactUsMenuItem:Landroid/view/MenuItem;

    sget v1, Lcom/helpshift/R$string;->hs__contact_us_btn:I

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 217
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->contactUsMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 222
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->contactUsMenuItem:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/helpshift/views/HSMenuItemCompat;->getActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/helpshift/support/fragments/SupportFragment$1;

    invoke-direct {v1, p0}, Lcom/helpshift/support/fragments/SupportFragment$1;-><init>(Lcom/helpshift/support/fragments/SupportFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    sget v0, Lcom/helpshift/R$id;->hs__action_done:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->doneMenuItem:Landroid/view/MenuItem;

    .line 230
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->doneMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 232
    sget v0, Lcom/helpshift/R$id;->hs__start_new_conversation:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->startNewConversationMenuItem:Landroid/view/MenuItem;

    .line 233
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->startNewConversationMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 235
    sget v0, Lcom/helpshift/R$id;->hs__attach_screenshot:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->attachImageMenuItem:Landroid/view/MenuItem;

    .line 236
    iget-object p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->attachImageMenuItem:Landroid/view/MenuItem;

    invoke-interface {p1, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const/4 p1, 0x1

    .line 238
    iput-boolean p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->menuItemsPrepared:Z

    const/4 p1, 0x0

    .line 239
    invoke-virtual {p0, p1}, Lcom/helpshift/support/fragments/SupportFragment;->setSearchListeners(Lcom/helpshift/support/controllers/FaqFlowController;)V

    .line 240
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->refreshMenu()V

    return-void
.end method

.method private declared-synchronized getImagePicker()Lcom/helpshift/support/widget/ImagePicker;
    .locals 1

    monitor-enter p0

    .line 740
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->imagePicker:Lcom/helpshift/support/widget/ImagePicker;

    if-nez v0, :cond_0

    .line 741
    new-instance v0, Lcom/helpshift/support/widget/ImagePicker;

    invoke-direct {v0, p0}, Lcom/helpshift/support/widget/ImagePicker;-><init>(Landroidx/fragment/app/Fragment;)V

    iput-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->imagePicker:Lcom/helpshift/support/widget/ImagePicker;

    .line 743
    :cond_0
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->imagePicker:Lcom/helpshift/support/widget/ImagePicker;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private getMenuResourceId()I
    .locals 1

    .line 208
    sget v0, Lcom/helpshift/R$menu;->hs__support_fragment:I

    return v0
.end method

.method private hideAllMenuItems()V
    .locals 2

    .line 309
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->searchMenuItem:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 310
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->contactUsMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 311
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->doneMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 312
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->startNewConversationMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 313
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->attachImageMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return-void
.end method

.method public static isForeground()Z
    .locals 1

    .line 124
    sget-boolean v0, Lcom/helpshift/support/fragments/SupportFragment;->isForeground:Z

    return v0
.end method

.method public static newInstance(Landroid/os/Bundle;)Lcom/helpshift/support/fragments/SupportFragment;
    .locals 1

    .line 118
    new-instance v0, Lcom/helpshift/support/fragments/SupportFragment;

    invoke-direct {v0}, Lcom/helpshift/support/fragments/SupportFragment;-><init>()V

    .line 119
    invoke-virtual {v0, p0}, Lcom/helpshift/support/fragments/SupportFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private quitSupportFragment()V
    .locals 2

    .line 533
    invoke-virtual {p0, p0}, Lcom/helpshift/support/fragments/SupportFragment;->getActivity(Landroidx/fragment/app/Fragment;)Landroid/app/Activity;

    move-result-object v0

    .line 534
    instance-of v1, v0, Lcom/helpshift/support/activities/ParentActivity;

    if-eqz v1, :cond_0

    .line 535
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 538
    :cond_0
    check-cast v0, Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    .line 539
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroidx/fragment/app/FragmentTransaction;->remove(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    :goto_0
    return-void
.end method

.method private restoreConversationFragmentMenu()V
    .locals 3

    const/4 v0, 0x1

    .line 383
    invoke-direct {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setRetainSearchFragmentState(Z)V

    const/4 v0, 0x0

    .line 384
    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setSearchMenuVisible(Z)V

    .line 385
    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setContactUsMenuVisible(Z)V

    .line 388
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getRetainedChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v2, "HSNewConversationFragment"

    .line 389
    invoke-virtual {v1, v2}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/helpshift/support/conversations/BaseConversationFragment;

    if-nez v1, :cond_0

    .line 392
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getRetainedChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    const-string v2, "HSConversationFragment"

    .line 393
    invoke-virtual {v1, v2}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/helpshift/support/conversations/BaseConversationFragment;

    :cond_0
    if-eqz v1, :cond_1

    .line 396
    iget-object v1, p0, Lcom/helpshift/support/fragments/SupportFragment;->doneMenuItem:Landroid/view/MenuItem;

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_1
    return-void
.end method

.method private restoreSearchMenuItem()V
    .locals 1

    .line 427
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getRetainedChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/helpshift/support/util/FragmentUtil;->getFaqFlowFragment(Landroidx/fragment/app/FragmentManager;)Lcom/helpshift/support/fragments/FaqFlowFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 429
    invoke-virtual {v0}, Lcom/helpshift/support/fragments/FaqFlowFragment;->getRetainedChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/helpshift/support/util/FragmentUtil;->getSearchFragment(Landroidx/fragment/app/FragmentManager;)Lcom/helpshift/support/fragments/SearchFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 431
    invoke-virtual {v0}, Lcom/helpshift/support/fragments/SearchFragment;->getCurrentQuery()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setSearchMenuQuery(Ljava/lang/String;)V

    .line 434
    :cond_0
    sget-object v0, Lcom/helpshift/support/ContactUsFilter$LOCATION;->ACTION_BAR:Lcom/helpshift/support/ContactUsFilter$LOCATION;

    invoke-static {v0}, Lcom/helpshift/support/ContactUsFilter;->showContactUs(Lcom/helpshift/support/ContactUsFilter$LOCATION;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setContactUsMenuVisible(Z)V

    const/4 v0, 0x0

    .line 435
    invoke-direct {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setRetainSearchFragmentState(Z)V

    return-void
.end method

.method private restoreSingleQuestionDoneModeFragmentMenu()V
    .locals 2

    .line 379
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->doneMenuItem:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return-void
.end method

.method private sendMenuEventClickEvent(Lcom/helpshift/support/fragments/HSMenuItemType;)V
    .locals 1

    .line 267
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->menuItemEventListener:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->menuItemEventListener:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/support/fragments/IMenuItemEventListener;

    invoke-interface {v0, p1}, Lcom/helpshift/support/fragments/IMenuItemEventListener;->onMenuItemClicked(Lcom/helpshift/support/fragments/HSMenuItemType;)V

    :cond_0
    return-void
.end method

.method private setMenuItemColors()V
    .locals 3

    .line 296
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 297
    iget-object v1, p0, Lcom/helpshift/support/fragments/SupportFragment;->searchMenuItem:Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/util/Styles;->setActionButtonIconColor(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 298
    iget-object v1, p0, Lcom/helpshift/support/fragments/SupportFragment;->contactUsMenuItem:Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/util/Styles;->setActionButtonIconColor(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 299
    iget-object v1, p0, Lcom/helpshift/support/fragments/SupportFragment;->contactUsMenuItem:Landroid/view/MenuItem;

    invoke-static {v1}, Lcom/helpshift/views/HSMenuItemCompat;->getActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v1

    .line 300
    sget v2, Lcom/helpshift/R$id;->hs__notification_badge:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 301
    invoke-virtual {v1}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/util/Styles;->setActionButtonIconColor(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 302
    iget-object v1, p0, Lcom/helpshift/support/fragments/SupportFragment;->doneMenuItem:Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/util/Styles;->setActionButtonIconColor(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 304
    iget-object v1, p0, Lcom/helpshift/support/fragments/SupportFragment;->startNewConversationMenuItem:Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/util/Styles;->setActionButtonIconColor(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 305
    iget-object v1, p0, Lcom/helpshift/support/fragments/SupportFragment;->attachImageMenuItem:Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/util/Styles;->setActionButtonIconColor(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private setRetainSearchFragmentState(Z)V
    .locals 2

    .line 419
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getRetainedChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "Helpshift_FaqFlowFrag"

    .line 420
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/helpshift/support/fragments/FaqFlowFragment;

    if-eqz v0, :cond_0

    .line 421
    invoke-virtual {v0}, Lcom/helpshift/support/fragments/FaqFlowFragment;->getFaqFlowController()Lcom/helpshift/support/controllers/FaqFlowController;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 422
    invoke-virtual {v0}, Lcom/helpshift/support/fragments/FaqFlowFragment;->getFaqFlowController()Lcom/helpshift/support/controllers/FaqFlowController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/helpshift/support/controllers/FaqFlowController;->setRetainSearchFragmentState(Z)V

    :cond_0
    return-void
.end method

.method private showDynamicFormFragmentMenu()V
    .locals 1

    const/4 v0, 0x1

    .line 444
    invoke-direct {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setRetainSearchFragmentState(Z)V

    const/4 v0, 0x0

    .line 445
    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setContactUsMenuVisible(Z)V

    .line 446
    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setSearchMenuVisible(Z)V

    return-void
.end method

.method private showFaqFragmentMenu()V
    .locals 1

    .line 439
    iget-boolean v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->faqLoaded:Z

    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setSearchMenuVisible(Z)V

    .line 440
    sget-object v0, Lcom/helpshift/support/ContactUsFilter$LOCATION;->ACTION_BAR:Lcom/helpshift/support/ContactUsFilter$LOCATION;

    invoke-static {v0}, Lcom/helpshift/support/ContactUsFilter;->showContactUs(Lcom/helpshift/support/ContactUsFilter$LOCATION;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setContactUsMenuVisible(Z)V

    return-void
.end method

.method private showQuestionListFragmentMenu()V
    .locals 1

    .line 401
    iget-boolean v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->faqLoaded:Z

    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setSearchMenuVisible(Z)V

    .line 402
    sget-object v0, Lcom/helpshift/support/ContactUsFilter$LOCATION;->ACTION_BAR:Lcom/helpshift/support/ContactUsFilter$LOCATION;

    invoke-static {v0}, Lcom/helpshift/support/ContactUsFilter;->showContactUs(Lcom/helpshift/support/ContactUsFilter$LOCATION;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setContactUsMenuVisible(Z)V

    return-void
.end method

.method private showSectionPagerFragmentMenu()V
    .locals 1

    const/4 v0, 0x1

    .line 406
    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setSearchMenuVisible(Z)V

    .line 407
    sget-object v0, Lcom/helpshift/support/ContactUsFilter$LOCATION;->ACTION_BAR:Lcom/helpshift/support/ContactUsFilter$LOCATION;

    invoke-static {v0}, Lcom/helpshift/support/ContactUsFilter;->showContactUs(Lcom/helpshift/support/ContactUsFilter$LOCATION;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setContactUsMenuVisible(Z)V

    return-void
.end method

.method private showSingleQuestionFragmentMenu()V
    .locals 1

    .line 411
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->isScreenLarge()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 412
    invoke-direct {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setRetainSearchFragmentState(Z)V

    const/4 v0, 0x0

    .line 413
    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setSearchMenuVisible(Z)V

    .line 415
    :cond_0
    sget-object v0, Lcom/helpshift/support/ContactUsFilter$LOCATION;->QUESTION_ACTION_BAR:Lcom/helpshift/support/ContactUsFilter$LOCATION;

    invoke-static {v0}, Lcom/helpshift/support/ContactUsFilter;->showContactUs(Lcom/helpshift/support/ContactUsFilter$LOCATION;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setContactUsMenuVisible(Z)V

    return-void
.end method

.method private showToolbarElevationLollipop(Z)V
    .locals 3

    .line 631
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->toolbar:Landroidx/appcompat/widget/Toolbar;

    const/high16 v1, 0x40800000    # 4.0f

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 633
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v1}, Lcom/helpshift/util/Styles;->dpToPx(Landroid/content/Context;F)F

    move-result p1

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/Toolbar;->setElevation(F)V

    goto :goto_0

    .line 636
    :cond_0
    invoke-virtual {v0, v2}, Landroidx/appcompat/widget/Toolbar;->setElevation(F)V

    goto :goto_0

    .line 640
    :cond_1
    invoke-virtual {p0, p0}, Lcom/helpshift/support/fragments/SupportFragment;->getActivity(Landroidx/fragment/app/Fragment;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatActivity;->getSupportActionBar()Landroidx/appcompat/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_3

    if-eqz p1, :cond_2

    .line 643
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v1}, Lcom/helpshift/util/Styles;->dpToPx(Landroid/content/Context;F)F

    move-result p1

    invoke-virtual {v0, p1}, Landroidx/appcompat/app/ActionBar;->setElevation(F)V

    goto :goto_0

    .line 646
    :cond_2
    invoke-virtual {v0, v2}, Landroidx/appcompat/app/ActionBar;->setElevation(F)V

    :cond_3
    :goto_0
    return-void
.end method

.method private showToolbarElevationPreLollipop(Z)V
    .locals 2

    .line 653
    invoke-virtual {p0, p0}, Lcom/helpshift/support/fragments/SupportFragment;->getActivity(Landroidx/fragment/app/Fragment;)Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/helpshift/R$id;->flow_fragment_container:I

    .line 654
    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    .line 658
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/helpshift/R$drawable;->hs__actionbar_compat_shadow:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 659
    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 662
    :cond_0
    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {p1, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private startLiveUpdates()V
    .locals 2

    .line 545
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getRetainedChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "HSConversationFragment"

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/helpshift/support/conversations/ConversationFragment;

    if-eqz v0, :cond_0

    .line 547
    invoke-virtual {v0}, Lcom/helpshift/support/conversations/ConversationFragment;->startLiveUpdates()V

    :cond_0
    return-void
.end method

.method private stopLiveUpdates()V
    .locals 2

    .line 553
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getRetainedChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "HSConversationFragment"

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/helpshift/support/conversations/ConversationFragment;

    if-eqz v0, :cond_0

    .line 555
    invoke-virtual {v0}, Lcom/helpshift/support/conversations/ConversationFragment;->stopLiveUpdates()V

    :cond_0
    return-void
.end method

.method private updateBadgeIcon()V
    .locals 5

    .line 478
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->contactUsMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 479
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->contactUsMenuItem:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/helpshift/views/HSMenuItemCompat;->getActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 481
    sget v1, Lcom/helpshift/R$id;->hs__notification_badge:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 482
    sget v2, Lcom/helpshift/R$id;->hs__notification_badge_padding:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 483
    iget v2, p0, Lcom/helpshift/support/fragments/SupportFragment;->newMessageCount:I

    const/4 v3, 0x0

    const/16 v4, 0x8

    if-eqz v2, :cond_0

    .line 484
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 485
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 486
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 489
    :cond_0
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 490
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method private updateMessageBatchCount(Ljava/lang/Integer;)V
    .locals 0

    .line 704
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iput p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->newMessageCount:I

    .line 705
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->updateBadgeIcon()V

    return-void
.end method


# virtual methods
.method public addVisibleFragment(Ljava/lang/String;)V
    .locals 1

    .line 317
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->visibleFragments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 318
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->refreshMenu()V

    return-void
.end method

.method public askForReadStoragePermission()V
    .locals 3

    .line 923
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getRetainedChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "HSConversationFragment"

    .line 924
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/helpshift/support/conversations/BaseConversationFragment;

    if-nez v0, :cond_0

    .line 927
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getRetainedChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-string v1, "HSNewConversationFragment"

    .line 928
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/helpshift/support/conversations/BaseConversationFragment;

    :cond_0
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 931
    invoke-virtual {v0, v1, v2}, Lcom/helpshift/support/conversations/BaseConversationFragment;->requestPermission(ZI)V

    :cond_1
    return-void
.end method

.method public exitSdkSession()V
    .locals 1

    .line 669
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/helpshift/support/activities/ParentActivity;

    if-eqz v0, :cond_0

    .line 670
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 673
    :cond_0
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/helpshift/support/util/FragmentUtil;->removeFragment(Landroidx/fragment/app/FragmentManager;Landroidx/fragment/app/Fragment;)V

    :goto_0
    return-void
.end method

.method public getSupportController()Lcom/helpshift/support/controllers/SupportController;
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->supportController:Lcom/helpshift/support/controllers/SupportController;

    return-object v0
.end method

.method public launchImagePicker(ZLandroid/os/Bundle;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 681
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getImagePicker()Lcom/helpshift/support/widget/ImagePicker;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/helpshift/support/widget/ImagePicker;->checkPermissionAndLaunchImagePicker(Landroid/os/Bundle;)V

    goto :goto_0

    .line 684
    :cond_0
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getImagePicker()Lcom/helpshift/support/widget/ImagePicker;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/helpshift/support/widget/ImagePicker;->launchImagePicker(Landroid/os/Bundle;)V

    :goto_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 763
    invoke-super {p0, p1, p2, p3}, Lcom/helpshift/support/fragments/MainFragment;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    :cond_0
    if-eqz p3, :cond_1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 766
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getImagePicker()Lcom/helpshift/support/widget/ImagePicker;

    move-result-object p2

    invoke-virtual {p2, p1, p3}, Lcom/helpshift/support/widget/ImagePicker;->onImagePickRequestResult(ILandroid/content/Intent;)V

    :cond_1
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 4

    .line 133
    invoke-super {p0, p1}, Lcom/helpshift/support/fragments/MainFragment;->onAttach(Landroid/content/Context;)V

    .line 134
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getPlatform()Lcom/helpshift/common/platform/Platform;

    move-result-object p1

    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/helpshift/common/platform/Platform;->setUIContext(Ljava/lang/Object;)V

    const/4 p1, 0x1

    .line 135
    invoke-virtual {p0, p1}, Lcom/helpshift/support/fragments/SupportFragment;->setRetainInstance(Z)V

    .line 136
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->supportController:Lcom/helpshift/support/controllers/SupportController;

    if-nez v0, :cond_0

    .line 137
    new-instance v0, Lcom/helpshift/support/controllers/SupportController;

    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getRetainedChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    .line 138
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/helpshift/support/controllers/SupportController;-><init>(Landroid/content/Context;Lcom/helpshift/support/contracts/SupportScreenView;Landroidx/fragment/app/FragmentManager;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->supportController:Lcom/helpshift/support/controllers/SupportController;

    goto :goto_0

    .line 141
    :cond_0
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getRetainedChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/support/controllers/SupportController;->onFragmentManagerUpdate(Landroidx/fragment/app/FragmentManager;)V

    .line 143
    :goto_0
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_1

    .line 144
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getConversationInboxPoller()Lcom/helpshift/conversation/ConversationInboxPoller;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/ConversationInboxPoller;->startAppPoller(Z)V

    :cond_1
    return-void
.end method

.method public onBackPressed()Z
    .locals 6

    .line 578
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getRetainedChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->getFragments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    .line 580
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroidx/fragment/app/Fragment;

    if-eqz v2, :cond_0

    .line 581
    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 582
    instance-of v3, v2, Lcom/helpshift/support/fragments/FaqFlowFragment;

    if-nez v3, :cond_2

    instance-of v3, v2, Lcom/helpshift/support/conversations/BaseConversationFragment;

    if-eqz v3, :cond_1

    goto :goto_0

    .line 598
    :cond_1
    instance-of v3, v2, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;

    if-eqz v3, :cond_0

    .line 599
    check-cast v2, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;

    invoke-virtual {v2}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->deleteAttachmentLocalCopy()V

    return v1

    .line 583
    :cond_2
    :goto_0
    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v3

    .line 584
    invoke-virtual {v3}, Landroidx/fragment/app/FragmentManager;->getBackStackEntryCount()I

    move-result v4

    const/4 v5, 0x1

    if-lez v4, :cond_3

    .line 585
    invoke-virtual {v3}, Landroidx/fragment/app/FragmentManager;->popBackStack()V

    return v5

    .line 589
    :cond_3
    instance-of v3, v2, Lcom/helpshift/support/conversations/ConversationalFragment;

    if-eqz v3, :cond_4

    move-object v3, v2

    check-cast v3, Lcom/helpshift/support/conversations/ConversationalFragment;

    invoke-virtual {v3}, Lcom/helpshift/support/conversations/ConversationalFragment;->onBackPressed()Z

    move-result v3

    if-eqz v3, :cond_4

    return v5

    .line 592
    :cond_4
    instance-of v3, v2, Lcom/helpshift/support/conversations/ConversationFragment;

    if-eqz v3, :cond_0

    .line 593
    check-cast v2, Lcom/helpshift/support/conversations/ConversationFragment;

    invoke-virtual {v2}, Lcom/helpshift/support/conversations/ConversationFragment;->stopLiveUpdates()V

    :cond_5
    return v1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 731
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    sget v0, Lcom/helpshift/R$id;->button_retry:I

    if-ne p1, v0, :cond_0

    .line 732
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getRetainedChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-static {p1}, Lcom/helpshift/support/util/FragmentUtil;->getFaqFlowFragment(Landroidx/fragment/app/FragmentManager;)Lcom/helpshift/support/fragments/FaqFlowFragment;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 734
    invoke-virtual {p1}, Lcom/helpshift/support/fragments/FaqFlowFragment;->retryGetSections()V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 788
    invoke-super {p0, p1}, Lcom/helpshift/support/fragments/MainFragment;->onCreate(Landroid/os/Bundle;)V

    .line 791
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "toolbarId"

    .line 793
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->toolbarId:I

    .line 797
    :cond_0
    iget p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->toolbarId:I

    if-nez p1, :cond_1

    const/4 p1, 0x1

    .line 798
    invoke-virtual {p0, p1}, Lcom/helpshift/support/fragments/SupportFragment;->setHasOptionsMenu(Z)V

    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .line 912
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getMenuResourceId()I

    move-result v0

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 913
    invoke-direct {p0, p1}, Lcom/helpshift/support/fragments/SupportFragment;->attachMenuListeners(Landroid/view/Menu;)V

    .line 914
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->menuItemEventListener:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 915
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->menuItemEventListener:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/support/fragments/IMenuItemEventListener;

    invoke-interface {v0}, Lcom/helpshift/support/fragments/IMenuItemEventListener;->onCreateOptionMenuCalled()V

    .line 917
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/helpshift/support/fragments/MainFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .line 805
    sget p3, Lcom/helpshift/R$layout;->hs__support_fragment:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onDataFetched(Ljava/lang/Integer;)V
    .locals 0

    .line 710
    invoke-direct {p0, p1}, Lcom/helpshift/support/fragments/SupportFragment;->updateMessageBatchCount(Ljava/lang/Integer;)V

    return-void
.end method

.method public bridge synthetic onDataFetched(Ljava/lang/Object;)V
    .locals 0

    .line 85
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/helpshift/support/fragments/SupportFragment;->onDataFetched(Ljava/lang/Integer;)V

    return-void
.end method

.method public onDestroyView()V
    .locals 3

    .line 883
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/helpshift/support/util/SnackbarUtil;->hideSnackbar(Landroid/view/View;)V

    .line 885
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->toolbar:Landroidx/appcompat/widget/Toolbar;

    if-eqz v0, :cond_0

    .line 886
    invoke-virtual {v0}, Landroidx/appcompat/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v0

    .line 887
    iget-object v1, p0, Lcom/helpshift/support/fragments/SupportFragment;->fragmentMenuItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 888
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v2}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 893
    iput-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->viewFaqsLoadError:Landroid/view/View;

    .line 894
    iput-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->viewFaqsLoading:Landroid/view/View;

    .line 895
    iput-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->viewNoFaqs:Landroid/view/View;

    .line 897
    invoke-super {p0}, Lcom/helpshift/support/fragments/MainFragment;->onDestroyView()V

    return-void
.end method

.method public onDetach()V
    .locals 2

    .line 902
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getPlatform()Lcom/helpshift/common/platform/Platform;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/helpshift/common/platform/Platform;->setUIContext(Ljava/lang/Object;)V

    .line 903
    invoke-static {}, Lcom/helpshift/util/ApplicationUtil;->restoreApplicationLocale()V

    .line 904
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 905
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getConversationInboxPoller()Lcom/helpshift/conversation/ConversationInboxPoller;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/ConversationInboxPoller;->startAppPoller(Z)V

    .line 907
    :cond_0
    invoke-super {p0}, Lcom/helpshift/support/fragments/MainFragment;->onDetach()V

    return-void
.end method

.method public onFailure(Ljava/lang/Integer;)V
    .locals 0

    return-void
.end method

.method public bridge synthetic onFailure(Ljava/lang/Object;)V
    .locals 0

    .line 85
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/helpshift/support/fragments/SupportFragment;->onFailure(Ljava/lang/Integer;)V

    return-void
.end method

.method public onFaqsLoaded()V
    .locals 3

    const/4 v0, 0x1

    .line 326
    iput-boolean v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->faqLoaded:Z

    .line 327
    iget-boolean v1, p0, Lcom/helpshift/support/fragments/SupportFragment;->menuItemsPrepared:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/helpshift/support/fragments/SupportFragment;->visibleFragments:Ljava/util/List;

    const-class v2, Lcom/helpshift/support/compositions/FaqFragment;

    .line 328
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/helpshift/support/fragments/SupportFragment;->visibleFragments:Ljava/util/List;

    const-class v2, Lcom/helpshift/support/fragments/QuestionListFragment;

    .line 329
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 330
    :cond_0
    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setSearchMenuVisible(Z)V

    :cond_1
    return-void
.end method

.method public onFocusChanged(Z)V
    .locals 3

    .line 567
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getRetainedChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->getFragments()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 569
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/fragment/app/Fragment;

    .line 570
    instance-of v2, v1, Lcom/helpshift/support/conversations/ConversationalFragment;

    if-eqz v2, :cond_0

    .line 571
    check-cast v1, Lcom/helpshift/support/conversations/ConversationalFragment;

    invoke-virtual {v1, p1}, Lcom/helpshift/support/conversations/ConversationalFragment;->onFocusChanged(Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onImagePickerResultFailure(ILjava/lang/Long;)V
    .locals 5

    const/4 v0, -0x4

    const/4 v1, -0x1

    if-eq p1, v0, :cond_3

    const/4 v0, -0x3

    if-eq p1, v0, :cond_2

    const/4 p2, -0x2

    if-eq p1, p2, :cond_1

    if-eq p1, v1, :cond_0

    goto :goto_0

    .line 953
    :cond_0
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getView()Landroid/view/View;

    move-result-object p1

    sget p2, Lcom/helpshift/R$string;->hs__screenshot_cloud_attach_error:I

    invoke-static {p1, p2, v1}, Lcom/helpshift/support/util/SnackbarUtil;->showSnackbar(Landroid/view/View;II)V

    goto :goto_0

    .line 948
    :cond_1
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getView()Landroid/view/View;

    move-result-object p1

    sget p2, Lcom/helpshift/R$string;->hs__screenshot_upload_error_msg:I

    invoke-static {p1, p2, v1}, Lcom/helpshift/support/util/SnackbarUtil;->showSnackbar(Landroid/view/View;II)V

    goto :goto_0

    .line 958
    :cond_2
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/helpshift/R$string;->hs__screenshot_limit_error:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 959
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    long-to-float p2, v3

    const/high16 v3, 0x49800000    # 1048576.0f

    div-float/2addr p2, v3

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    aput-object p2, v0, v2

    .line 958
    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 960
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getView()Landroid/view/View;

    move-result-object p2

    invoke-static {p2, p1, v1}, Lcom/helpshift/support/util/SnackbarUtil;->showSnackbar(Landroid/view/View;Ljava/lang/String;I)V

    goto :goto_0

    .line 945
    :cond_3
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getView()Landroid/view/View;

    move-result-object p1

    sget p2, Lcom/helpshift/R$string;->hs__network_error_msg:I

    invoke-static {p1, p2, v1}, Lcom/helpshift/support/util/SnackbarUtil;->showSnackbar(Landroid/view/View;II)V

    :goto_0
    return-void
.end method

.method public onImagePickerResultSuccess(Lcom/helpshift/conversation/dto/ImagePickerFile;Landroid/os/Bundle;)V
    .locals 2

    .line 938
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getSupportController()Lcom/helpshift/support/controllers/SupportController;

    move-result-object v0

    sget-object v1, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$LaunchSource;->GALLERY_APP:Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$LaunchSource;

    invoke-virtual {v0, p1, p2, v1}, Lcom/helpshift/support/controllers/SupportController;->startScreenshotPreviewFragment(Lcom/helpshift/conversation/dto/ImagePickerFile;Landroid/os/Bundle;Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$LaunchSource;)V

    return-void
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 2

    .line 245
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    .line 247
    sget v0, Lcom/helpshift/R$id;->hs__contact_us:I

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    .line 248
    iget-object p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->supportController:Lcom/helpshift/support/controllers/SupportController;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/helpshift/support/controllers/SupportController;->onContactUsClicked(Ljava/lang/String;)V

    goto :goto_0

    .line 251
    :cond_0
    sget v0, Lcom/helpshift/R$id;->hs__action_done:I

    if-ne p1, v0, :cond_1

    .line 252
    iget-object p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->supportController:Lcom/helpshift/support/controllers/SupportController;

    invoke-virtual {p1}, Lcom/helpshift/support/controllers/SupportController;->actionDone()V

    goto :goto_0

    .line 255
    :cond_1
    sget v0, Lcom/helpshift/R$id;->hs__start_new_conversation:I

    if-ne p1, v0, :cond_2

    .line 256
    sget-object p1, Lcom/helpshift/support/fragments/HSMenuItemType;->START_NEW_CONVERSATION:Lcom/helpshift/support/fragments/HSMenuItemType;

    invoke-direct {p0, p1}, Lcom/helpshift/support/fragments/SupportFragment;->sendMenuEventClickEvent(Lcom/helpshift/support/fragments/HSMenuItemType;)V

    goto :goto_0

    .line 259
    :cond_2
    sget v0, Lcom/helpshift/R$id;->hs__attach_screenshot:I

    if-ne p1, v0, :cond_3

    .line 260
    sget-object p1, Lcom/helpshift/support/fragments/HSMenuItemType;->SCREENSHOT_ATTACHMENT:Lcom/helpshift/support/fragments/HSMenuItemType;

    invoke-direct {p0, p1}, Lcom/helpshift/support/fragments/SupportFragment;->sendMenuEventClickEvent(Lcom/helpshift/support/fragments/HSMenuItemType;)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public onNewIntent(Landroid/os/Bundle;)V
    .locals 1

    .line 694
    sget-boolean v0, Lcom/helpshift/support/fragments/SupportFragment;->isForeground:Z

    if-eqz v0, :cond_0

    .line 695
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->supportController:Lcom/helpshift/support/controllers/SupportController;

    invoke-virtual {v0, p1}, Lcom/helpshift/support/controllers/SupportController;->onNewIntent(Landroid/os/Bundle;)V

    goto :goto_0

    .line 698
    :cond_0
    iput-object p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->newIntentData:Landroid/os/Bundle;

    .line 700
    :goto_0
    sget-boolean p1, Lcom/helpshift/support/fragments/SupportFragment;->isForeground:Z

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->handleNewIntent:Z

    return-void
.end method

.method public onPause()V
    .locals 1

    .line 181
    invoke-virtual {p0, p0}, Lcom/helpshift/support/fragments/SupportFragment;->getActivity(Landroidx/fragment/app/Fragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 182
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->stopLiveUpdates()V

    .line 184
    :cond_0
    invoke-super {p0}, Lcom/helpshift/support/fragments/MainFragment;->onPause()V

    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 3

    .line 772
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getRetainedChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->getFragments()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 774
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/fragment/app/Fragment;

    if-eqz v1, :cond_0

    .line 776
    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    instance-of v2, v1, Lcom/helpshift/support/conversations/BaseConversationFragment;

    if-eqz v2, :cond_0

    .line 778
    invoke-virtual {v1, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    return-void

    .line 783
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/helpshift/support/fragments/MainFragment;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    return-void
.end method

.method public onResume()V
    .locals 2

    .line 861
    invoke-super {p0}, Lcom/helpshift/support/fragments/MainFragment;->onResume()V

    .line 862
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->supportController:Lcom/helpshift/support/controllers/SupportController;

    invoke-virtual {v0}, Lcom/helpshift/support/controllers/SupportController;->start()V

    .line 863
    sget v0, Lcom/helpshift/R$string;->hs__help_header:I

    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->setToolbarTitle(Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 864
    invoke-virtual {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->showToolbarElevation(Z)V

    .line 865
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getConversationInboxDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    move-result-object v0

    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1, p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->fetchConversationUpdatesListenerReference:Ljava/util/concurrent/atomic/AtomicReference;

    .line 867
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->startLiveUpdates()V

    .line 869
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getNotificationCountSync()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->updateMessageBatchCount(Ljava/lang/Integer;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .line 874
    invoke-super {p0, p1}, Lcom/helpshift/support/fragments/MainFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 875
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->supportController:Lcom/helpshift/support/controllers/SupportController;

    if-eqz v0, :cond_0

    .line 876
    invoke-virtual {v0, p1}, Lcom/helpshift/support/controllers/SupportController;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 878
    :cond_0
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getImagePicker()Lcom/helpshift/support/widget/ImagePicker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/helpshift/support/widget/ImagePicker;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onStart()V
    .locals 3

    .line 150
    invoke-super {p0}, Lcom/helpshift/support/fragments/MainFragment;->onStart()V

    .line 152
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    .line 153
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->quitSupportFragment()V

    return-void

    .line 157
    :cond_0
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "Helpshift_SupportFrag"

    const-string v1, "Helpshift session began."

    .line 158
    invoke-static {v0, v1}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    invoke-static {}, Lcom/helpshift/support/HSSearch;->init()V

    .line 160
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "support_mode"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_1

    .line 163
    sget-object v0, Lcom/helpshift/analytics/AnalyticsEventType;->LIBRARY_OPENED:Lcom/helpshift/analytics/AnalyticsEventType;

    goto :goto_0

    .line 166
    :cond_1
    sget-object v0, Lcom/helpshift/analytics/AnalyticsEventType;->LIBRARY_OPENED_DECOMP:Lcom/helpshift/analytics/AnalyticsEventType;

    .line 168
    :goto_0
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v2

    invoke-interface {v2}, Lcom/helpshift/CoreApi;->getAnalyticsEventDM()Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;->pushEvent(Lcom/helpshift/analytics/AnalyticsEventType;)V

    .line 170
    iget-boolean v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->handleNewIntent:Z

    if-eqz v0, :cond_2

    .line 171
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->supportController:Lcom/helpshift/support/controllers/SupportController;

    iget-object v2, p0, Lcom/helpshift/support/fragments/SupportFragment;->newIntentData:Landroid/os/Bundle;

    invoke-virtual {v0, v2}, Lcom/helpshift/support/controllers/SupportController;->onNewIntent(Landroid/os/Bundle;)V

    .line 172
    iput-boolean v1, p0, Lcom/helpshift/support/fragments/SupportFragment;->handleNewIntent:Z

    .line 174
    :cond_2
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->onSDKSessionStarted()V

    :cond_3
    const/4 v0, 0x1

    .line 176
    sput-boolean v0, Lcom/helpshift/support/fragments/SupportFragment;->isForeground:Z

    return-void
.end method

.method public onStop()V
    .locals 3

    .line 189
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Helpshift_SupportFrag"

    const-string v1, "Helpshift session ended."

    .line 190
    invoke-static {v0, v1}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    .line 192
    invoke-static {}, Lcom/helpshift/support/HSSearch;->deinit()V

    .line 193
    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getAnalyticsEventDM()Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    move-result-object v1

    sget-object v2, Lcom/helpshift/analytics/AnalyticsEventType;->LIBRARY_QUIT:Lcom/helpshift/analytics/AnalyticsEventType;

    invoke-virtual {v1, v2}, Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;->pushEvent(Lcom/helpshift/analytics/AnalyticsEventType;)V

    const/4 v1, 0x0

    .line 194
    sput-boolean v1, Lcom/helpshift/support/fragments/SupportFragment;->isForeground:Z

    .line 195
    invoke-interface {v0}, Lcom/helpshift/CoreApi;->sendAnalyticsEvent()V

    .line 196
    invoke-interface {v0}, Lcom/helpshift/CoreApi;->onSDKSessionEnded()V

    .line 198
    :cond_0
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getConversationInboxDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    move-result-object v0

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->fetchConversationUpdatesListenerReference:Ljava/util/concurrent/atomic/AtomicReference;

    .line 199
    invoke-super {p0}, Lcom/helpshift/support/fragments/MainFragment;->onStop()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .line 810
    invoke-super {p0, p1, p2}, Lcom/helpshift/support/fragments/MainFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 811
    sget p2, Lcom/helpshift/R$id;->view_no_faqs:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/helpshift/support/fragments/SupportFragment;->viewNoFaqs:Landroid/view/View;

    .line 812
    sget p2, Lcom/helpshift/R$id;->view_faqs_loading:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/helpshift/support/fragments/SupportFragment;->viewFaqsLoading:Landroid/view/View;

    .line 813
    sget p2, Lcom/helpshift/R$id;->view_faqs_load_error:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/helpshift/support/fragments/SupportFragment;->viewFaqsLoadError:Landroid/view/View;

    .line 814
    sget p2, Lcom/helpshift/R$id;->button_retry:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    .line 815
    invoke-virtual {p2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 817
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object p2

    invoke-interface {p2}, Lcom/helpshift/CoreApi;->getSDKConfigurationDM()Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    move-result-object p2

    invoke-virtual {p2}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->isHelpshiftBrandingDisabled()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 818
    sget p2, Lcom/helpshift/R$id;->hs_logo:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    const/16 p2, 0x8

    .line 819
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 823
    :cond_0
    iget p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->toolbarId:I

    if-eqz p1, :cond_3

    .line 824
    invoke-virtual {p0, p0}, Lcom/helpshift/support/fragments/SupportFragment;->getActivity(Landroidx/fragment/app/Fragment;)Landroid/app/Activity;

    move-result-object p1

    iget p2, p0, Lcom/helpshift/support/fragments/SupportFragment;->toolbarId:I

    invoke-virtual {p1, p2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    iput-object p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->toolbar:Landroidx/appcompat/widget/Toolbar;

    .line 827
    iget-object p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->toolbar:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p1}, Landroidx/appcompat/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object p1

    .line 828
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 829
    :goto_0
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 830
    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 833
    :cond_1
    iget-object p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->toolbar:Landroidx/appcompat/widget/Toolbar;

    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getMenuResourceId()I

    move-result v1

    invoke-virtual {p1, v1}, Landroidx/appcompat/widget/Toolbar;->inflateMenu(I)V

    .line 834
    iget-object p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->toolbar:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p1}, Landroidx/appcompat/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/helpshift/support/fragments/SupportFragment;->attachMenuListeners(Landroid/view/Menu;)V

    .line 837
    iget-object p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->toolbar:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {p1}, Landroidx/appcompat/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object p1

    .line 838
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/helpshift/support/fragments/SupportFragment;->fragmentMenuItems:Ljava/util/List;

    .line 839
    :goto_1
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 840
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 841
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 842
    iget-object v2, p0, Lcom/helpshift/support/fragments/SupportFragment;->fragmentMenuItems:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    return-void
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 1

    .line 850
    invoke-super {p0, p1}, Lcom/helpshift/support/fragments/MainFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    .line 852
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->supportController:Lcom/helpshift/support/controllers/SupportController;

    if-eqz v0, :cond_0

    .line 853
    invoke-virtual {v0, p1}, Lcom/helpshift/support/controllers/SupportController;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 855
    :cond_0
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getImagePicker()Lcom/helpshift/support/widget/ImagePicker;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/helpshift/support/widget/ImagePicker;->onViewStateRestored(Landroid/os/Bundle;)V

    :cond_1
    return-void
.end method

.method public refreshMenu()V
    .locals 6

    .line 335
    iget-boolean v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->menuItemsPrepared:Z

    if-eqz v0, :cond_c

    .line 336
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->hideAllMenuItems()V

    .line 337
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->setMenuItemColors()V

    .line 338
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->visibleFragments:Ljava/util/List;

    monitor-enter v0

    .line 339
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/support/fragments/SupportFragment;->visibleFragments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 340
    const-class v3, Lcom/helpshift/support/compositions/FaqFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 341
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->showFaqFragmentMenu()V

    goto :goto_0

    .line 343
    :cond_1
    const-class v3, Lcom/helpshift/support/fragments/SearchFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 344
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->restoreSearchMenuItem()V

    goto :goto_0

    .line 346
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, Lcom/helpshift/support/fragments/SingleQuestionFragment;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 347
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->showSingleQuestionFragmentMenu()V

    goto :goto_0

    .line 349
    :cond_3
    const-class v3, Lcom/helpshift/support/compositions/SectionPagerFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 350
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->showSectionPagerFragmentMenu()V

    goto :goto_0

    .line 352
    :cond_4
    const-class v3, Lcom/helpshift/support/fragments/QuestionListFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 353
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->showQuestionListFragmentMenu()V

    goto :goto_0

    .line 355
    :cond_5
    const-class v3, Lcom/helpshift/support/conversations/NewConversationFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    const-class v3, Lcom/helpshift/support/conversations/ConversationFragment;

    .line 356
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    const-class v3, Lcom/helpshift/support/conversations/ConversationalFragment;

    .line 357
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    goto :goto_1

    .line 360
    :cond_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-class v5, Lcom/helpshift/support/fragments/SingleQuestionFragment;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v5, 0x2

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 361
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->restoreSingleQuestionDoneModeFragmentMenu()V

    goto/16 :goto_0

    .line 363
    :cond_7
    const-class v3, Lcom/helpshift/support/fragments/DynamicFormFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 364
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->showDynamicFormFragmentMenu()V

    goto/16 :goto_0

    .line 366
    :cond_8
    const-class v3, Lcom/helpshift/support/conversations/usersetup/UserSetupFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-class v3, Lcom/helpshift/support/conversations/AuthenticationFailureFragment;

    .line 367
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 369
    :cond_9
    invoke-direct {p0, v4}, Lcom/helpshift/support/fragments/SupportFragment;->setRetainSearchFragmentState(Z)V

    const/4 v2, 0x0

    .line 370
    invoke-virtual {p0, v2}, Lcom/helpshift/support/fragments/SupportFragment;->setSearchMenuVisible(Z)V

    .line 371
    invoke-virtual {p0, v2}, Lcom/helpshift/support/fragments/SupportFragment;->setContactUsMenuVisible(Z)V

    goto/16 :goto_0

    .line 358
    :cond_a
    :goto_1
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->restoreConversationFragmentMenu()V

    goto/16 :goto_0

    .line 374
    :cond_b
    monitor-exit v0

    goto :goto_2

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_c
    :goto_2
    return-void
.end method

.method public registerToolbarMenuEventsListener(Lcom/helpshift/support/fragments/IMenuItemEventListener;)V
    .locals 1

    .line 719
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->menuItemEventListener:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public removeVisibleFragment(Ljava/lang/String;)V
    .locals 1

    .line 322
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->visibleFragments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public resetNewMessageCount()V
    .locals 1

    const/4 v0, 0x0

    .line 497
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/helpshift/support/fragments/SupportFragment;->updateMessageBatchCount(Ljava/lang/Integer;)V

    return-void
.end method

.method public resetToolbarImportanceForAccessibility()V
    .locals 2

    .line 991
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    return-void

    .line 995
    :cond_0
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->toolbar:Landroidx/appcompat/widget/Toolbar;

    if-eqz v0, :cond_1

    .line 996
    iget v1, p0, Lcom/helpshift/support/fragments/SupportFragment;->toolbarImportanceForAccessibility:I

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setImportantForAccessibility(I)V

    goto :goto_0

    .line 999
    :cond_1
    invoke-virtual {p0, p0}, Lcom/helpshift/support/fragments/SupportFragment;->getActivity(Landroidx/fragment/app/Fragment;)Landroid/app/Activity;

    move-result-object v0

    .line 1000
    instance-of v1, v0, Lcom/helpshift/support/activities/ParentActivity;

    if-eqz v1, :cond_2

    .line 1001
    check-cast v0, Lcom/helpshift/support/activities/ParentActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/support/activities/ParentActivity;->setToolbarImportanceForAccessibility(I)V

    :cond_2
    :goto_0
    return-void
.end method

.method public setContactUsMenuVisible(Z)V
    .locals 1

    .line 450
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->searchMenuItem:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/helpshift/views/HSMenuItemCompat;->isActionViewExpanded(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 451
    iget-object p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->contactUsMenuItem:Landroid/view/MenuItem;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 454
    :cond_0
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->contactUsMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 456
    :goto_0
    invoke-direct {p0}, Lcom/helpshift/support/fragments/SupportFragment;->updateBadgeIcon()V

    return-void
.end method

.method public setSearchListeners(Lcom/helpshift/support/controllers/FaqFlowController;)V
    .locals 1

    .line 280
    iget-boolean v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->menuItemsPrepared:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    .line 282
    invoke-virtual {p0}, Lcom/helpshift/support/fragments/SupportFragment;->getRetainedChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/helpshift/support/util/FragmentUtil;->getFaqFlowFragment(Landroidx/fragment/app/FragmentManager;)Lcom/helpshift/support/fragments/FaqFlowFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 284
    invoke-virtual {v0}, Lcom/helpshift/support/fragments/FaqFlowFragment;->getFaqFlowController()Lcom/helpshift/support/controllers/FaqFlowController;

    move-result-object p1

    :cond_0
    if-eqz p1, :cond_1

    .line 289
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->searchMenuItem:Landroid/view/MenuItem;

    invoke-static {v0, p1}, Lcom/helpshift/views/HSMenuItemCompat;->setOnActionExpandListener(Landroid/view/MenuItem;Landroid/view/MenuItem$OnActionExpandListener;)V

    .line 290
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->searchView:Landroidx/appcompat/widget/SearchView;

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/SearchView;->setOnQueryTextListener(Landroidx/appcompat/widget/SearchView$OnQueryTextListener;)V

    :cond_1
    return-void
.end method

.method public setSearchMenuQuery(Ljava/lang/String;)V
    .locals 2

    .line 468
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->searchMenuItem:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/helpshift/views/HSMenuItemCompat;->isActionViewExpanded(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->searchMenuItem:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/helpshift/views/HSMenuItemCompat;->expandActionView(Landroid/view/MenuItem;)V

    .line 472
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 473
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->searchView:Landroidx/appcompat/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroidx/appcompat/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    :cond_1
    return-void
.end method

.method public setSearchMenuVisible(Z)V
    .locals 2

    .line 460
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->searchMenuItem:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/helpshift/views/HSMenuItemCompat;->isActionViewExpanded(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->visibleFragments:Ljava/util/List;

    const-class v1, Lcom/helpshift/support/fragments/SearchFragment;

    .line 461
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 462
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->searchMenuItem:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/helpshift/views/HSMenuItemCompat;->collapseActionView(Landroid/view/MenuItem;)V

    .line 464
    :cond_0
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->searchMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .line 609
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->toolbar:Landroidx/appcompat/widget/Toolbar;

    if-eqz v0, :cond_0

    .line 610
    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 613
    :cond_0
    invoke-virtual {p0, p0}, Lcom/helpshift/support/fragments/SupportFragment;->getActivity(Landroidx/fragment/app/Fragment;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatActivity;->getSupportActionBar()Landroidx/appcompat/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 615
    invoke-virtual {v0, p1}, Landroidx/appcompat/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public setToolbarImportanceForAccessibility(I)V
    .locals 2

    .line 972
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    return-void

    .line 976
    :cond_0
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->toolbar:Landroidx/appcompat/widget/Toolbar;

    if-eqz v0, :cond_1

    .line 978
    invoke-virtual {v0}, Landroidx/appcompat/widget/Toolbar;->getImportantForAccessibility()I

    move-result v0

    iput v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->toolbarImportanceForAccessibility:I

    .line 979
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->toolbar:Landroidx/appcompat/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/Toolbar;->setImportantForAccessibility(I)V

    goto :goto_0

    .line 982
    :cond_1
    invoke-virtual {p0, p0}, Lcom/helpshift/support/fragments/SupportFragment;->getActivity(Landroidx/fragment/app/Fragment;)Landroid/app/Activity;

    move-result-object v0

    .line 983
    instance-of v1, v0, Lcom/helpshift/support/activities/ParentActivity;

    if-eqz v1, :cond_2

    .line 984
    check-cast v0, Lcom/helpshift/support/activities/ParentActivity;

    invoke-virtual {v0, p1}, Lcom/helpshift/support/activities/ParentActivity;->setToolbarImportanceForAccessibility(I)V

    :cond_2
    :goto_0
    return-void
.end method

.method public shouldRefreshMenu()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public showToolbarElevation(Z)V
    .locals 2

    .line 621
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 622
    invoke-direct {p0, p1}, Lcom/helpshift/support/fragments/SupportFragment;->showToolbarElevationLollipop(Z)V

    goto :goto_0

    .line 625
    :cond_0
    invoke-direct {p0, p1}, Lcom/helpshift/support/fragments/SupportFragment;->showToolbarElevationPreLollipop(Z)V

    :goto_0
    return-void
.end method

.method public unRegisterSearchListener()V
    .locals 2

    .line 273
    iget-boolean v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->menuItemsPrepared:Z

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->searchMenuItem:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/helpshift/views/HSMenuItemCompat;->setOnActionExpandListener(Landroid/view/MenuItem;Landroid/view/MenuItem$OnActionExpandListener;)V

    .line 275
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->searchView:Landroidx/appcompat/widget/SearchView;

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/SearchView;->setOnQueryTextListener(Landroidx/appcompat/widget/SearchView$OnQueryTextListener;)V

    :cond_0
    return-void
.end method

.method public unRegisterToolbarMenuEventsListener(Lcom/helpshift/support/fragments/IMenuItemEventListener;)V
    .locals 1

    .line 723
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->menuItemEventListener:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 724
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 p1, 0x0

    .line 725
    iput-object p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->menuItemEventListener:Ljava/lang/ref/WeakReference;

    :cond_0
    return-void
.end method

.method public updateFaqLoadingUI(I)V
    .locals 2

    .line 501
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->viewNoFaqs:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 502
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->viewFaqsLoading:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 503
    iget-object v0, p0, Lcom/helpshift/support/fragments/SupportFragment;->viewFaqsLoadError:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    const/4 v1, 0x1

    if-eq p1, v1, :cond_3

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    goto :goto_0

    .line 515
    :cond_0
    iget-object p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->viewFaqsLoadError:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 512
    :cond_1
    iget-object p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->viewNoFaqs:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 507
    :cond_2
    iget-object p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->viewFaqsLoading:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    :goto_0
    return-void
.end method

.method public updateMenuItemVisibility(Lcom/helpshift/support/fragments/HSMenuItemType;Z)V
    .locals 1

    .line 748
    sget-object v0, Lcom/helpshift/support/fragments/SupportFragment$2;->$SwitchMap$com$helpshift$support$fragments$HSMenuItemType:[I

    invoke-virtual {p1}, Lcom/helpshift/support/fragments/HSMenuItemType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 755
    :cond_0
    iget-object p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->attachImageMenuItem:Landroid/view/MenuItem;

    if-eqz p1, :cond_2

    .line 756
    invoke-interface {p1, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 750
    :cond_1
    iget-object p1, p0, Lcom/helpshift/support/fragments/SupportFragment;->startNewConversationMenuItem:Landroid/view/MenuItem;

    if-eqz p1, :cond_2

    .line 751
    invoke-interface {p1, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_2
    :goto_0
    return-void
.end method
