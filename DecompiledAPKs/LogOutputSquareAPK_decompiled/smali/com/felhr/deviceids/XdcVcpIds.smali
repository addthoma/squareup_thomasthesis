.class public Lcom/felhr/deviceids/XdcVcpIds;
.super Ljava/lang/Object;
.source "XdcVcpIds.java"


# static fields
.field private static final xdcvcpDevices:[J


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x3

    new-array v0, v0, [J

    const/16 v1, 0x264d

    const/16 v2, 0x232

    .line 15
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/4 v4, 0x0

    aput-wide v2, v0, v4

    const/16 v2, 0x120

    .line 16
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/4 v3, 0x1

    aput-wide v1, v0, v3

    const/16 v1, 0x483

    const/16 v2, 0x5740

    .line 17
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/4 v3, 0x2

    aput-wide v1, v0, v3

    .line 14
    invoke-static {v0}, Lcom/felhr/deviceids/Helpers;->createTable([J)[J

    move-result-object v0

    sput-object v0, Lcom/felhr/deviceids/XdcVcpIds;->xdcvcpDevices:[J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isDeviceSupported(II)Z
    .locals 1

    .line 22
    sget-object v0, Lcom/felhr/deviceids/XdcVcpIds;->xdcvcpDevices:[J

    invoke-static {v0, p0, p1}, Lcom/felhr/deviceids/Helpers;->exists([JII)Z

    move-result p0

    return p0
.end method
