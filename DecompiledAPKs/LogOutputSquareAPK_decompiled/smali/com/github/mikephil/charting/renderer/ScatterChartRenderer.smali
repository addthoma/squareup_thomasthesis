.class public Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;
.super Lcom/github/mikephil/charting/renderer/DataRenderer;
.source "ScatterChartRenderer.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$github$mikephil$charting$charts$ScatterChart$ScatterShape:[I


# instance fields
.field protected mChart:Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;

.field protected mScatterBuffers:[Lcom/github/mikephil/charting/buffer/ScatterBuffer;


# direct methods
.method static synthetic $SWITCH_TABLE$com$github$mikephil$charting$charts$ScatterChart$ScatterShape()[I
    .locals 3

    .line 22
    sget-object v0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->$SWITCH_TABLE$com$github$mikephil$charting$charts$ScatterChart$ScatterShape:[I

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    invoke-static {}, Lcom/github/mikephil/charting/charts/ScatterChart$ScatterShape;->values()[Lcom/github/mikephil/charting/charts/ScatterChart$ScatterShape;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/github/mikephil/charting/charts/ScatterChart$ScatterShape;->CIRCLE:Lcom/github/mikephil/charting/charts/ScatterChart$ScatterShape;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/charts/ScatterChart$ScatterShape;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v1, Lcom/github/mikephil/charting/charts/ScatterChart$ScatterShape;->CROSS:Lcom/github/mikephil/charting/charts/ScatterChart$ScatterShape;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/charts/ScatterChart$ScatterShape;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v1, Lcom/github/mikephil/charting/charts/ScatterChart$ScatterShape;->SQUARE:Lcom/github/mikephil/charting/charts/ScatterChart$ScatterShape;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/charts/ScatterChart$ScatterShape;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v1, Lcom/github/mikephil/charting/charts/ScatterChart$ScatterShape;->TRIANGLE:Lcom/github/mikephil/charting/charts/ScatterChart$ScatterShape;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/charts/ScatterChart$ScatterShape;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    sput-object v0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->$SWITCH_TABLE$com$github$mikephil$charting$charts$ScatterChart$ScatterShape:[I

    return-object v0
.end method

.method public constructor <init>(Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;Lcom/github/mikephil/charting/animation/ChartAnimator;Lcom/github/mikephil/charting/utils/ViewPortHandler;)V
    .locals 0

    .line 30
    invoke-direct {p0, p2, p3}, Lcom/github/mikephil/charting/renderer/DataRenderer;-><init>(Lcom/github/mikephil/charting/animation/ChartAnimator;Lcom/github/mikephil/charting/utils/ViewPortHandler;)V

    .line 31
    iput-object p1, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;

    .line 33
    iget-object p1, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-static {p2}, Lcom/github/mikephil/charting/utils/Utils;->convertDpToPixel(F)F

    move-result p2

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void
.end method


# virtual methods
.method public drawData(Landroid/graphics/Canvas;)V
    .locals 3

    .line 52
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;

    invoke-interface {v0}, Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;->getScatterData()Lcom/github/mikephil/charting/data/ScatterData;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/ScatterData;->getDataSets()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/github/mikephil/charting/data/ScatterDataSet;

    .line 56
    invoke-virtual {v1}, Lcom/github/mikephil/charting/data/ScatterDataSet;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 57
    invoke-virtual {p0, p1, v1}, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->drawDataSet(Landroid/graphics/Canvas;Lcom/github/mikephil/charting/data/ScatterDataSet;)V

    goto :goto_0
.end method

.method protected drawDataSet(Landroid/graphics/Canvas;Lcom/github/mikephil/charting/data/ScatterDataSet;)V
    .locals 13

    .line 63
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;

    invoke-virtual {p2}, Lcom/github/mikephil/charting/data/ScatterDataSet;->getAxisDependency()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;->getTransformer(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Lcom/github/mikephil/charting/utils/Transformer;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    invoke-virtual {v1}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseX()F

    move-result v1

    .line 66
    iget-object v2, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseY()F

    move-result v2

    .line 68
    invoke-virtual {p2}, Lcom/github/mikephil/charting/data/ScatterDataSet;->getYVals()Ljava/util/List;

    move-result-object v3

    .line 70
    invoke-virtual {p2}, Lcom/github/mikephil/charting/data/ScatterDataSet;->getScatterShapeSize()F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    .line 72
    invoke-virtual {p2}, Lcom/github/mikephil/charting/data/ScatterDataSet;->getScatterShape()Lcom/github/mikephil/charting/charts/ScatterChart$ScatterShape;

    move-result-object v5

    .line 74
    iget-object v6, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mScatterBuffers:[Lcom/github/mikephil/charting/buffer/ScatterBuffer;

    iget-object v7, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;

    invoke-interface {v7}, Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;->getScatterData()Lcom/github/mikephil/charting/data/ScatterData;

    move-result-object v7

    invoke-virtual {v7, p2}, Lcom/github/mikephil/charting/data/ScatterData;->getIndexOfDataSet(Lcom/github/mikephil/charting/data/DataSet;)I

    move-result v7

    aget-object v6, v6, v7

    .line 76
    invoke-virtual {v6, v1, v2}, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->setPhases(FF)V

    .line 77
    invoke-virtual {v6, v3}, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->feed(Ljava/util/List;)V

    .line 79
    iget-object v1, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/utils/Transformer;->pointValuesToPixel([F)V

    .line 81
    invoke-static {}, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->$SWITCH_TABLE$com$github$mikephil$charting$charts$ScatterChart$ScatterShape()[I

    move-result-object v0

    invoke-virtual {v5}, Lcom/github/mikephil/charting/charts/ScatterChart$ScatterShape;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_f

    const/4 v2, 0x2

    if-eq v0, v2, :cond_a

    const/4 v2, 0x3

    if-eq v0, v2, :cond_5

    const/4 v2, 0x4

    if-eq v0, v2, :cond_0

    goto/16 :goto_7

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 86
    :goto_0
    invoke-virtual {v6}, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    goto/16 :goto_7

    .line 88
    :cond_1
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    iget-object v2, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v2, v2, v1

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsRight(F)Z

    move-result v0

    if-nez v0, :cond_2

    goto/16 :goto_7

    .line 91
    :cond_2
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    iget-object v2, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v2, v2, v1

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsLeft(F)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 92
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    iget-object v2, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    add-int/lit8 v3, v1, 0x1

    aget v2, v2, v3

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsY(F)Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_1

    .line 95
    :cond_3
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    div-int/lit8 v2, v1, 0x2

    invoke-virtual {p2, v2}, Lcom/github/mikephil/charting/data/ScatterDataSet;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 96
    iget-object v0, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v0, v0, v1

    sub-float v8, v0, v4

    .line 97
    iget-object v0, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v0, v0, v3

    sub-float v9, v0, v4

    iget-object v0, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v0, v0, v1

    add-float v10, v0, v4

    .line 98
    iget-object v0, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v0, v0, v3

    add-float v11, v0, v4

    .line 99
    iget-object v12, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    move-object v7, p1

    .line 96
    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_4
    :goto_1
    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    .line 104
    :cond_5
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 106
    :goto_2
    invoke-virtual {v6}, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->size()I

    move-result v0

    if-lt v1, v0, :cond_6

    goto/16 :goto_7

    .line 108
    :cond_6
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    iget-object v2, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v2, v2, v1

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsRight(F)Z

    move-result v0

    if-nez v0, :cond_7

    goto/16 :goto_7

    .line 111
    :cond_7
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    iget-object v2, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v2, v2, v1

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsLeft(F)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 112
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    iget-object v2, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    add-int/lit8 v3, v1, 0x1

    aget v2, v2, v3

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsY(F)Z

    move-result v0

    if-nez v0, :cond_8

    goto :goto_3

    .line 115
    :cond_8
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    div-int/lit8 v2, v1, 0x2

    invoke-virtual {p2, v2}, Lcom/github/mikephil/charting/data/ScatterDataSet;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 116
    iget-object v0, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v0, v0, v1

    iget-object v2, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v2, v2, v3

    .line 117
    iget-object v3, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    .line 116
    invoke-virtual {p1, v0, v2, v4, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :cond_9
    :goto_3
    add-int/lit8 v1, v1, 0x2

    goto :goto_2

    .line 122
    :cond_a
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 125
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 127
    :goto_4
    invoke-virtual {v6}, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->size()I

    move-result v2

    if-lt v1, v2, :cond_b

    goto/16 :goto_7

    .line 129
    :cond_b
    iget-object v2, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    iget-object v3, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v3, v3, v1

    invoke-virtual {v2, v3}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsRight(F)Z

    move-result v2

    if-nez v2, :cond_c

    goto/16 :goto_7

    .line 132
    :cond_c
    iget-object v2, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    iget-object v3, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v3, v3, v1

    invoke-virtual {v2, v3}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsLeft(F)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 133
    iget-object v2, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    iget-object v3, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    add-int/lit8 v5, v1, 0x1

    aget v3, v3, v5

    invoke-virtual {v2, v3}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsY(F)Z

    move-result v2

    if-nez v2, :cond_d

    goto :goto_5

    .line 136
    :cond_d
    iget-object v2, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    div-int/lit8 v3, v1, 0x2

    invoke-virtual {p2, v3}, Lcom/github/mikephil/charting/data/ScatterDataSet;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 137
    iget-object v2, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v2, v2, v1

    iget-object v3, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v3, v3, v5

    sub-float/2addr v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 138
    iget-object v2, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v2, v2, v1

    add-float/2addr v2, v4

    iget-object v3, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v3, v3, v5

    add-float/2addr v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 139
    iget-object v2, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v2, v2, v1

    sub-float/2addr v2, v4

    iget-object v3, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v3, v3, v5

    add-float/2addr v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 140
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 142
    iget-object v2, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 143
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    :cond_e
    :goto_5
    add-int/lit8 v1, v1, 0x2

    goto :goto_4

    .line 148
    :cond_f
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 150
    :goto_6
    invoke-virtual {v6}, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->size()I

    move-result v0

    if-lt v1, v0, :cond_10

    goto :goto_7

    .line 152
    :cond_10
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    iget-object v2, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v2, v2, v1

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsRight(F)Z

    move-result v0

    if-nez v0, :cond_11

    :goto_7
    return-void

    .line 155
    :cond_11
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    iget-object v2, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v2, v2, v1

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsLeft(F)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 156
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    iget-object v2, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    add-int/lit8 v3, v1, 0x1

    aget v2, v2, v3

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsY(F)Z

    move-result v0

    if-nez v0, :cond_12

    goto :goto_8

    .line 159
    :cond_12
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    div-int/lit8 v2, v1, 0x2

    invoke-virtual {p2, v2}, Lcom/github/mikephil/charting/data/ScatterDataSet;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 161
    iget-object v0, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v0, v0, v1

    sub-float v8, v0, v4

    iget-object v0, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v9, v0, v3

    .line 162
    iget-object v0, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v0, v0, v1

    add-float v10, v0, v4

    .line 163
    iget-object v0, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v11, v0, v3

    iget-object v12, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    move-object v7, p1

    .line 161
    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 164
    iget-object v0, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v8, v0, v1

    iget-object v0, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v0, v0, v3

    sub-float v9, v0, v4

    .line 165
    iget-object v0, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v10, v0, v1

    iget-object v0, v6, Lcom/github/mikephil/charting/buffer/ScatterBuffer;->buffer:[F

    aget v0, v0, v3

    add-float v11, v0, v4

    .line 166
    iget-object v12, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    .line 164
    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_13
    :goto_8
    add-int/lit8 v1, v1, 0x2

    goto :goto_6
.end method

.method public drawExtras(Landroid/graphics/Canvas;)V
    .locals 0

    return-void
.end method

.method public drawHighlighted(Landroid/graphics/Canvas;[Lcom/github/mikephil/charting/utils/Highlight;)V
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 253
    :goto_0
    array-length v2, p2

    if-lt v1, v2, :cond_0

    return-void

    .line 255
    :cond_0
    iget-object v2, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;

    invoke-interface {v2}, Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;->getScatterData()Lcom/github/mikephil/charting/data/ScatterData;

    move-result-object v2

    aget-object v3, p2, v1

    .line 256
    invoke-virtual {v3}, Lcom/github/mikephil/charting/utils/Highlight;->getDataSetIndex()I

    move-result v3

    .line 255
    invoke-virtual {v2, v3}, Lcom/github/mikephil/charting/data/ScatterData;->getDataSetByIndex(I)Lcom/github/mikephil/charting/data/DataSet;

    move-result-object v2

    check-cast v2, Lcom/github/mikephil/charting/data/ScatterDataSet;

    if-nez v2, :cond_1

    goto :goto_1

    .line 261
    :cond_1
    iget-object v3, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mHighlightPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/data/ScatterDataSet;->getHighLightColor()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 263
    aget-object v3, p2, v1

    invoke-virtual {v3}, Lcom/github/mikephil/charting/utils/Highlight;->getXIndex()I

    move-result v3

    int-to-float v4, v3

    .line 266
    iget-object v5, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;

    invoke-interface {v5}, Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;->getXChartMax()F

    move-result v5

    iget-object v6, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    invoke-virtual {v6}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseX()F

    move-result v6

    mul-float v5, v5, v6

    cmpl-float v5, v4, v5

    if-lez v5, :cond_2

    goto :goto_1

    .line 269
    :cond_2
    invoke-virtual {v2, v3}, Lcom/github/mikephil/charting/data/ScatterDataSet;->getYValForXIndex(I)F

    move-result v3

    iget-object v5, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    invoke-virtual {v5}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseY()F

    move-result v5

    mul-float v3, v3, v5

    const/16 v5, 0x8

    new-array v5, v5, [F

    aput v4, v5, v0

    .line 274
    iget-object v6, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;

    invoke-interface {v6}, Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;->getYChartMax()F

    move-result v6

    const/4 v7, 0x1

    aput v6, v5, v7

    const/4 v6, 0x2

    aput v4, v5, v6

    const/4 v4, 0x3

    iget-object v6, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;

    invoke-interface {v6}, Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;->getYChartMin()F

    move-result v6

    aput v6, v5, v4

    const/4 v4, 0x4

    const/4 v6, 0x0

    aput v6, v5, v4

    const/4 v4, 0x5

    aput v3, v5, v4

    const/4 v4, 0x6

    .line 275
    iget-object v6, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;

    invoke-interface {v6}, Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;->getXChartMax()F

    move-result v6

    aput v6, v5, v4

    const/4 v4, 0x7

    aput v3, v5, v4

    .line 278
    iget-object v3, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/data/ScatterDataSet;->getAxisDependency()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v2

    invoke-interface {v3, v2}, Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;->getTransformer(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Lcom/github/mikephil/charting/utils/Transformer;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/github/mikephil/charting/utils/Transformer;->pointValuesToPixel([F)V

    .line 280
    iget-object v2, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mHighlightPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v2}, Landroid/graphics/Canvas;->drawLines([FLandroid/graphics/Paint;)V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0
.end method

.method public drawValues(Landroid/graphics/Canvas;)V
    .locals 12

    .line 203
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;

    invoke-interface {v0}, Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;->getScatterData()Lcom/github/mikephil/charting/data/ScatterData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/ScatterData;->getYValCount()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;

    invoke-interface {v1}, Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;->getMaxVisibleCount()I

    move-result v1

    int-to-float v1, v1

    .line 204
    iget-object v2, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->getScaleX()F

    move-result v2

    mul-float v1, v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_6

    .line 206
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;

    invoke-interface {v0}, Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;->getScatterData()Lcom/github/mikephil/charting/data/ScatterData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/ScatterData;->getDataSets()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 208
    :goto_0
    iget-object v3, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;

    invoke-interface {v3}, Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;->getScatterData()Lcom/github/mikephil/charting/data/ScatterData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/github/mikephil/charting/data/ScatterData;->getDataSetCount()I

    move-result v3

    if-lt v2, v3, :cond_0

    goto/16 :goto_4

    .line 210
    :cond_0
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/github/mikephil/charting/data/ScatterDataSet;

    .line 212
    invoke-virtual {v3}, Lcom/github/mikephil/charting/data/ScatterDataSet;->isDrawValuesEnabled()Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_2

    .line 216
    :cond_1
    invoke-virtual {p0, v3}, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->applyValueTextStyle(Lcom/github/mikephil/charting/data/DataSet;)V

    .line 218
    invoke-virtual {v3}, Lcom/github/mikephil/charting/data/ScatterDataSet;->getYVals()Ljava/util/List;

    move-result-object v4

    .line 220
    iget-object v5, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;

    invoke-virtual {v3}, Lcom/github/mikephil/charting/data/ScatterDataSet;->getAxisDependency()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;->getTransformer(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Lcom/github/mikephil/charting/utils/Transformer;

    move-result-object v5

    .line 222
    iget-object v6, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    invoke-virtual {v6}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseY()F

    move-result v6

    .line 221
    invoke-virtual {v5, v4, v6}, Lcom/github/mikephil/charting/utils/Transformer;->generateTransformedValuesScatter(Ljava/util/List;F)[F

    move-result-object v5

    .line 224
    invoke-virtual {v3}, Lcom/github/mikephil/charting/data/ScatterDataSet;->getScatterShapeSize()F

    move-result v6

    const/4 v7, 0x0

    :goto_1
    int-to-float v8, v7

    .line 226
    array-length v9, v5

    int-to-float v9, v9

    iget-object v10, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mAnimator:Lcom/github/mikephil/charting/animation/ChartAnimator;

    invoke-virtual {v10}, Lcom/github/mikephil/charting/animation/ChartAnimator;->getPhaseX()F

    move-result v10

    mul-float v9, v9, v10

    cmpg-float v8, v8, v9

    if-ltz v8, :cond_2

    goto :goto_2

    .line 228
    :cond_2
    iget-object v8, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    aget v9, v5, v7

    invoke-virtual {v8, v9}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsRight(F)Z

    move-result v8

    if-nez v8, :cond_3

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 232
    :cond_3
    iget-object v8, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    aget v9, v5, v7

    invoke-virtual {v8, v9}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsLeft(F)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 233
    iget-object v8, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mViewPortHandler:Lcom/github/mikephil/charting/utils/ViewPortHandler;

    add-int/lit8 v9, v7, 0x1

    aget v10, v5, v9

    invoke-virtual {v8, v10}, Lcom/github/mikephil/charting/utils/ViewPortHandler;->isInBoundsY(F)Z

    move-result v8

    if-nez v8, :cond_4

    goto :goto_3

    .line 236
    :cond_4
    div-int/lit8 v8, v7, 0x2

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/github/mikephil/charting/data/Entry;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/data/Entry;->getVal()F

    move-result v8

    .line 238
    invoke-virtual {v3}, Lcom/github/mikephil/charting/data/ScatterDataSet;->getValueFormatter()Lcom/github/mikephil/charting/utils/ValueFormatter;

    move-result-object v10

    invoke-interface {v10, v8}, Lcom/github/mikephil/charting/utils/ValueFormatter;->getFormattedValue(F)Ljava/lang/String;

    move-result-object v8

    aget v10, v5, v7

    .line 239
    aget v9, v5, v9

    sub-float/2addr v9, v6

    .line 240
    iget-object v11, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    .line 238
    invoke-virtual {p1, v8, v10, v9, v11}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_5
    :goto_3
    add-int/lit8 v7, v7, 0x2

    goto :goto_1

    :cond_6
    :goto_4
    return-void
.end method

.method public initBuffers()V
    .locals 5

    .line 39
    iget-object v0, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mChart:Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;

    invoke-interface {v0}, Lcom/github/mikephil/charting/interfaces/ScatterDataProvider;->getScatterData()Lcom/github/mikephil/charting/data/ScatterData;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Lcom/github/mikephil/charting/data/ScatterData;->getDataSetCount()I

    move-result v1

    new-array v1, v1, [Lcom/github/mikephil/charting/buffer/ScatterBuffer;

    iput-object v1, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mScatterBuffers:[Lcom/github/mikephil/charting/buffer/ScatterBuffer;

    const/4 v1, 0x0

    .line 43
    :goto_0
    iget-object v2, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mScatterBuffers:[Lcom/github/mikephil/charting/buffer/ScatterBuffer;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    return-void

    .line 44
    :cond_0
    invoke-virtual {v0, v1}, Lcom/github/mikephil/charting/data/ScatterData;->getDataSetByIndex(I)Lcom/github/mikephil/charting/data/DataSet;

    move-result-object v2

    check-cast v2, Lcom/github/mikephil/charting/data/ScatterDataSet;

    .line 45
    iget-object v3, p0, Lcom/github/mikephil/charting/renderer/ScatterChartRenderer;->mScatterBuffers:[Lcom/github/mikephil/charting/buffer/ScatterBuffer;

    new-instance v4, Lcom/github/mikephil/charting/buffer/ScatterBuffer;

    invoke-virtual {v2}, Lcom/github/mikephil/charting/data/ScatterDataSet;->getEntryCount()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    invoke-direct {v4, v2}, Lcom/github/mikephil/charting/buffer/ScatterBuffer;-><init>(I)V

    aput-object v4, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
