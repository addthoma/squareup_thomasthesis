.class Lcom/evernote/android/job/JobStorage;
.super Ljava/lang/Object;
.source "JobStorage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/evernote/android/job/JobStorage$JobOpenHelper;,
        Lcom/evernote/android/job/JobStorage$JobCacheId;
    }
.end annotation


# static fields
.field private static final CACHE_SIZE:I = 0x1e

.field private static final CAT:Lcom/evernote/android/job/util/JobCat;

.field public static final COLUMN_BACKOFF_MS:Ljava/lang/String; = "backoffMs"

.field public static final COLUMN_BACKOFF_POLICY:Ljava/lang/String; = "backoffPolicy"

.field public static final COLUMN_END_MS:Ljava/lang/String; = "endMs"

.field public static final COLUMN_EXACT:Ljava/lang/String; = "exact"

.field public static final COLUMN_EXTRAS:Ljava/lang/String; = "extras"

.field public static final COLUMN_FLEX_MS:Ljava/lang/String; = "flexMs"

.field public static final COLUMN_FLEX_SUPPORT:Ljava/lang/String; = "flexSupport"

.field public static final COLUMN_ID:Ljava/lang/String; = "_id"

.field public static final COLUMN_INTERVAL_MS:Ljava/lang/String; = "intervalMs"

.field public static final COLUMN_LAST_RUN:Ljava/lang/String; = "lastRun"

.field public static final COLUMN_NETWORK_TYPE:Ljava/lang/String; = "networkType"

.field public static final COLUMN_NUM_FAILURES:Ljava/lang/String; = "numFailures"

.field private static final COLUMN_PERSISTED:Ljava/lang/String; = "persisted"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final COLUMN_REQUIREMENTS_ENFORCED:Ljava/lang/String; = "requirementsEnforced"

.field public static final COLUMN_REQUIRES_BATTERY_NOT_LOW:Ljava/lang/String; = "requiresBatteryNotLow"

.field public static final COLUMN_REQUIRES_CHARGING:Ljava/lang/String; = "requiresCharging"

.field public static final COLUMN_REQUIRES_DEVICE_IDLE:Ljava/lang/String; = "requiresDeviceIdle"

.field public static final COLUMN_REQUIRES_STORAGE_NOT_LOW:Ljava/lang/String; = "requiresStorageNotLow"

.field public static final COLUMN_SCHEDULED_AT:Ljava/lang/String; = "scheduledAt"

.field public static final COLUMN_STARTED:Ljava/lang/String; = "started"

.field public static final COLUMN_START_MS:Ljava/lang/String; = "startMs"

.field public static final COLUMN_TAG:Ljava/lang/String; = "tag"

.field public static final COLUMN_TRANSIENT:Ljava/lang/String; = "transient"

.field private static final COLUMN_TRANSIENT_OLD:Ljava/lang/String; = "isTransient"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DATABASE_NAME:Ljava/lang/String; = "evernote_jobs.db"

.field public static final DATABASE_VERSION:I = 0x6

.field private static final FAILED_DELETE_IDS:Ljava/lang/String; = "FAILED_DELETE_IDS"

.field public static final JOB_ID_COUNTER:Ljava/lang/String; = "JOB_ID_COUNTER_v2"

.field public static final JOB_TABLE_NAME:Ljava/lang/String; = "jobs"

.field public static final PREF_FILE_NAME:Ljava/lang/String; = "evernote_jobs"

.field private static final WHERE_NOT_STARTED:Ljava/lang/String; = "ifnull(started, 0)<=0"


# instance fields
.field private final mCacheId:Lcom/evernote/android/job/JobStorage$JobCacheId;

.field private final mDbHelper:Lcom/evernote/android/job/JobStorage$JobOpenHelper;

.field private final mFailedDeleteIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mInjectedDatabase:Landroid/database/sqlite/SQLiteDatabase;

.field private mJobCounter:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mLock:Ljava/util/concurrent/locks/ReadWriteLock;

.field private final mPreferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 51
    new-instance v0, Lcom/evernote/android/job/util/JobCat;

    const-string v1, "JobStorage"

    invoke-direct {v0, v1}, Lcom/evernote/android/job/util/JobCat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/android/job/JobStorage;->CAT:Lcom/evernote/android/job/util/JobCat;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "evernote_jobs.db"

    .line 111
    invoke-direct {p0, p1, v0}, Lcom/evernote/android/job/JobStorage;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "evernote_jobs"

    const/4 v1, 0x0

    .line 115
    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/android/job/JobStorage;->mPreferences:Landroid/content/SharedPreferences;

    .line 116
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, Lcom/evernote/android/job/JobStorage;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 118
    new-instance v0, Lcom/evernote/android/job/JobStorage$JobCacheId;

    invoke-direct {v0, p0}, Lcom/evernote/android/job/JobStorage$JobCacheId;-><init>(Lcom/evernote/android/job/JobStorage;)V

    iput-object v0, p0, Lcom/evernote/android/job/JobStorage;->mCacheId:Lcom/evernote/android/job/JobStorage$JobCacheId;

    .line 120
    new-instance v0, Lcom/evernote/android/job/JobStorage$JobOpenHelper;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/evernote/android/job/JobStorage$JobOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/evernote/android/job/JobStorage$1;)V

    iput-object v0, p0, Lcom/evernote/android/job/JobStorage;->mDbHelper:Lcom/evernote/android/job/JobStorage$JobOpenHelper;

    .line 122
    iget-object p1, p0, Lcom/evernote/android/job/JobStorage;->mPreferences:Landroid/content/SharedPreferences;

    new-instance p2, Ljava/util/HashSet;

    invoke-direct {p2}, Ljava/util/HashSet;-><init>()V

    const-string v0, "FAILED_DELETE_IDS"

    invoke-interface {p1, v0, p2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/evernote/android/job/JobStorage;->mFailedDeleteIds:Ljava/util/Set;

    .line 123
    iget-object p1, p0, Lcom/evernote/android/job/JobStorage;->mFailedDeleteIds:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    .line 124
    invoke-direct {p0}, Lcom/evernote/android/job/JobStorage;->tryToCleanupFinishedJobs()V

    :cond_0
    return-void
.end method

.method static synthetic access$100(Lcom/evernote/android/job/JobStorage;)Ljava/util/Set;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/evernote/android/job/JobStorage;->mFailedDeleteIds:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$200(Lcom/evernote/android/job/JobStorage;Lcom/evernote/android/job/JobRequest;I)Z
    .locals 0

    .line 49
    invoke-direct {p0, p1, p2}, Lcom/evernote/android/job/JobStorage;->remove(Lcom/evernote/android/job/JobRequest;I)Z

    move-result p0

    return p0
.end method

.method static synthetic access$300()Lcom/evernote/android/job/util/JobCat;
    .locals 1

    .line 49
    sget-object v0, Lcom/evernote/android/job/JobStorage;->CAT:Lcom/evernote/android/job/util/JobCat;

    return-object v0
.end method

.method static synthetic access$400(Lcom/evernote/android/job/JobStorage;IZ)Lcom/evernote/android/job/JobRequest;
    .locals 0

    .line 49
    invoke-direct {p0, p1, p2}, Lcom/evernote/android/job/JobStorage;->load(IZ)Lcom/evernote/android/job/JobRequest;

    move-result-object p0

    return-object p0
.end method

.method private addFailedDeleteId(I)V
    .locals 3

    .line 370
    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mFailedDeleteIds:Ljava/util/Set;

    monitor-enter v0

    .line 371
    :try_start_0
    iget-object v1, p0, Lcom/evernote/android/job/JobStorage;->mFailedDeleteIds:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 372
    iget-object p1, p0, Lcom/evernote/android/job/JobStorage;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    const-string v1, "FAILED_DELETE_IDS"

    iget-object v2, p0, Lcom/evernote/android/job/JobStorage;->mFailedDeleteIds:Ljava/util/Set;

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 373
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private static closeCursor(Landroid/database/Cursor;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 600
    :try_start_0
    invoke-interface {p0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private static closeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    if-eqz p0, :cond_0

    .line 608
    invoke-static {}, Lcom/evernote/android/job/JobConfig;->isCloseDatabase()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610
    :try_start_0
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private didFailToDelete(I)Z
    .locals 2

    .line 377
    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mFailedDeleteIds:Ljava/util/Set;

    monitor-enter v0

    .line 378
    :try_start_0
    iget-object v1, p0, Lcom/evernote/android/job/JobStorage;->mFailedDeleteIds:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/evernote/android/job/JobStorage;->mFailedDeleteIds:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    .line 379
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private load(IZ)Lcom/evernote/android/job/JobRequest;
    .locals 12

    .line 287
    invoke-direct {p0, p1}, Lcom/evernote/android/job/JobStorage;->didFailToDelete(I)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    const/4 v0, 0x0

    const/4 v2, 0x1

    :try_start_0
    const-string v3, "_id=?"

    if-nez p2, :cond_1

    .line 296
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " AND started<=0"

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_1
    move-object v6, v3

    .line 299
    invoke-virtual {p0}, Lcom/evernote/android/job/JobStorage;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    const-string v4, "jobs"

    const/4 v5, 0x0

    new-array v7, v2, [Ljava/lang/String;

    .line 300
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v7, v0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v3, p2

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v3, :cond_2

    .line 301
    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 302
    invoke-static {v3}, Lcom/evernote/android/job/JobRequest;->fromCursor(Landroid/database/Cursor;)Lcom/evernote/android/job/JobRequest;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 309
    invoke-static {v3}, Lcom/evernote/android/job/JobStorage;->closeCursor(Landroid/database/Cursor;)V

    .line 310
    invoke-static {p2}, Lcom/evernote/android/job/JobStorage;->closeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    return-object p1

    :catchall_0
    move-exception p1

    move-object v1, v3

    goto :goto_2

    :catch_0
    move-exception v4

    move-object v11, v4

    move-object v4, p2

    move-object p2, v11

    goto :goto_0

    .line 309
    :cond_2
    invoke-static {v3}, Lcom/evernote/android/job/JobStorage;->closeCursor(Landroid/database/Cursor;)V

    .line 310
    invoke-static {p2}, Lcom/evernote/android/job/JobStorage;->closeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_1

    :catchall_1
    move-exception p1

    goto :goto_2

    :catch_1
    move-exception v3

    move-object v4, p2

    move-object p2, v3

    move-object v3, v1

    goto :goto_0

    :catchall_2
    move-exception p1

    move-object p2, v1

    goto :goto_2

    :catch_2
    move-exception p2

    move-object v3, v1

    move-object v4, v3

    .line 306
    :goto_0
    :try_start_3
    sget-object v5, Lcom/evernote/android/job/JobStorage;->CAT:Lcom/evernote/android/job/util/JobCat;

    const-string v6, "could not load id %d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, v0

    invoke-virtual {v5, p2, v6, v2}, Lcom/evernote/android/job/util/JobCat;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 309
    invoke-static {v3}, Lcom/evernote/android/job/JobStorage;->closeCursor(Landroid/database/Cursor;)V

    .line 310
    invoke-static {v4}, Lcom/evernote/android/job/JobStorage;->closeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    :goto_1
    return-object v1

    :catchall_3
    move-exception p1

    move-object v1, v3

    move-object p2, v4

    .line 309
    :goto_2
    invoke-static {v1}, Lcom/evernote/android/job/JobStorage;->closeCursor(Landroid/database/Cursor;)V

    .line 310
    invoke-static {p2}, Lcom/evernote/android/job/JobStorage;->closeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 311
    throw p1
.end method

.method private remove(Lcom/evernote/android/job/JobRequest;I)Z
    .locals 8

    .line 227
    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 230
    :try_start_0
    iget-object v3, p0, Lcom/evernote/android/job/JobStorage;->mCacheId:Lcom/evernote/android/job/JobStorage$JobCacheId;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/evernote/android/job/JobStorage$JobCacheId;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    invoke-virtual {p0}, Lcom/evernote/android/job/JobStorage;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "jobs"

    const-string v4, "_id=?"

    new-array v5, v0, [Ljava/lang/String;

    .line 233
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    invoke-static {v2}, Lcom/evernote/android/job/JobStorage;->closeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 241
    iget-object p1, p0, Lcom/evernote/android/job/JobStorage;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception v3

    .line 236
    :try_start_1
    sget-object v4, Lcom/evernote/android/job/JobStorage;->CAT:Lcom/evernote/android/job/util/JobCat;

    const-string v5, "could not delete %d %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    aput-object p1, v6, v0

    invoke-virtual {v4, v3, v5, v6}, Lcom/evernote/android/job/util/JobCat;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 237
    invoke-direct {p0, p2}, Lcom/evernote/android/job/JobStorage;->addFailedDeleteId(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 240
    invoke-static {v2}, Lcom/evernote/android/job/JobStorage;->closeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 241
    iget-object p1, p0, Lcom/evernote/android/job/JobStorage;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v1

    .line 240
    :goto_0
    invoke-static {v2}, Lcom/evernote/android/job/JobStorage;->closeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 241
    iget-object p2, p0, Lcom/evernote/android/job/JobStorage;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {p2}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 242
    throw p1
.end method

.method private store(Lcom/evernote/android/job/JobRequest;)V
    .locals 6

    .line 268
    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->toContentValues()Landroid/content/ContentValues;

    move-result-object p1

    const/4 v0, 0x0

    .line 271
    :try_start_0
    invoke-virtual {p0}, Lcom/evernote/android/job/JobStorage;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    const-string v2, "jobs"

    const/4 v3, 0x5

    .line 277
    invoke-virtual {v1, v2, v0, p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-wide/16 v4, 0x0

    cmp-long p1, v2, v4

    if-ltz p1, :cond_0

    .line 281
    invoke-static {v1}, Lcom/evernote/android/job/JobStorage;->closeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void

    .line 278
    :cond_0
    :try_start_2
    new-instance p1, Landroid/database/SQLException;

    const-string v0, "Couldn\'t insert job request into database"

    invoke-direct {p1, v0}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_0

    :catchall_1
    move-exception p1

    move-object v1, v0

    .line 281
    :goto_0
    invoke-static {v1}, Lcom/evernote/android/job/JobStorage;->closeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 282
    throw p1
.end method

.method private tryToCleanupFinishedJobs()V
    .locals 2

    .line 383
    new-instance v0, Lcom/evernote/android/job/JobStorage$1;

    const-string v1, "CleanupFinishedJobsThread"

    invoke-direct {v0, p0, v1}, Lcom/evernote/android/job/JobStorage$1;-><init>(Lcom/evernote/android/job/JobStorage;Ljava/lang/String;)V

    .line 425
    invoke-virtual {v0}, Lcom/evernote/android/job/JobStorage$1;->start()V

    return-void
.end method

.method private updateRequestInCache(Lcom/evernote/android/job/JobRequest;)V
    .locals 2

    .line 159
    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mCacheId:Lcom/evernote/android/job/JobStorage$JobCacheId;

    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getJobId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/evernote/android/job/JobStorage$JobCacheId;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public get(I)Lcom/evernote/android/job/JobRequest;
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 166
    :try_start_0
    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mCacheId:Lcom/evernote/android/job/JobStorage$JobCacheId;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/evernote/android/job/JobStorage$JobCacheId;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/evernote/android/job/JobRequest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object p1

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 169
    throw p1
.end method

.method public getAllJobRequests(Ljava/lang/String;Z)Ljava/util/Set;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/Set<",
            "Lcom/evernote/android/job/JobRequest;",
            ">;"
        }
    .end annotation

    .line 173
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 178
    iget-object v1, p0, Lcom/evernote/android/job/JobStorage;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 183
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz p2, :cond_0

    move-object p1, v2

    goto :goto_0

    :cond_0
    const-string p1, "ifnull(started, 0)<=0"

    :goto_0
    move-object v7, p1

    move-object v8, v2

    goto :goto_2

    :cond_1
    if-eqz p2, :cond_2

    const-string p2, ""

    goto :goto_1

    :cond_2
    const-string p2, "ifnull(started, 0)<=0 AND "

    .line 188
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "tag=?"

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    aput-object p1, v3, v1

    move-object v7, p2

    move-object v8, v3

    .line 192
    :goto_2
    invoke-virtual {p0}, Lcom/evernote/android/job/JobStorage;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v5, "jobs"

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v4, p1

    .line 193
    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 196
    new-instance p2, Ljava/util/HashMap;

    iget-object v3, p0, Lcom/evernote/android/job/JobStorage;->mCacheId:Lcom/evernote/android/job/JobStorage$JobCacheId;

    invoke-virtual {v3}, Lcom/evernote/android/job/JobStorage$JobCacheId;->snapshot()Ljava/util/Map;

    move-result-object v3

    invoke-direct {p2, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    :cond_3
    :goto_3
    if-eqz v2, :cond_5

    .line 198
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "_id"

    .line 200
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 201
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/evernote/android/job/JobStorage;->didFailToDelete(I)Z

    move-result v4

    if-nez v4, :cond_3

    .line 202
    invoke-virtual {p2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 203
    invoke-virtual {p2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 205
    :cond_4
    invoke-static {v2}, Lcom/evernote/android/job/JobRequest;->fromCursor(Landroid/database/Cursor;)Lcom/evernote/android/job/JobRequest;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_3

    :catch_0
    move-exception p2

    goto :goto_4

    :catchall_0
    move-exception p1

    move-object p2, p1

    move-object p1, v2

    goto :goto_5

    :catch_1
    move-exception p1

    move-object p2, p1

    move-object p1, v2

    .line 210
    :goto_4
    :try_start_2
    sget-object v3, Lcom/evernote/android/job/JobStorage;->CAT:Lcom/evernote/android/job/util/JobCat;

    const-string v4, "could not load all jobs"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v3, p2, v4, v1}, Lcom/evernote/android/job/util/JobCat;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 213
    :cond_5
    invoke-static {v2}, Lcom/evernote/android/job/JobStorage;->closeCursor(Landroid/database/Cursor;)V

    .line 214
    invoke-static {p1}, Lcom/evernote/android/job/JobStorage;->closeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 215
    iget-object p1, p0, Lcom/evernote/android/job/JobStorage;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v0

    :catchall_1
    move-exception p2

    .line 213
    :goto_5
    invoke-static {v2}, Lcom/evernote/android/job/JobStorage;->closeCursor(Landroid/database/Cursor;)V

    .line 214
    invoke-static {p1}, Lcom/evernote/android/job/JobStorage;->closeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 215
    iget-object p1, p0, Lcom/evernote/android/job/JobStorage;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 216
    throw p2
.end method

.method getDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .line 319
    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mInjectedDatabase:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    return-object v0

    .line 323
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mDbHelper:Lcom/evernote/android/job/JobStorage$JobOpenHelper;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobStorage$JobOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteCantOpenDatabaseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 326
    sget-object v1, Lcom/evernote/android/job/JobStorage;->CAT:Lcom/evernote/android/job/util/JobCat;

    invoke-virtual {v1, v0}, Lcom/evernote/android/job/util/JobCat;->e(Ljava/lang/Throwable;)V

    .line 329
    new-instance v0, Lcom/evernote/android/job/JobStorageDatabaseErrorHandler;

    invoke-direct {v0}, Lcom/evernote/android/job/JobStorageDatabaseErrorHandler;-><init>()V

    const-string v1, "evernote_jobs.db"

    invoke-virtual {v0, v1}, Lcom/evernote/android/job/JobStorageDatabaseErrorHandler;->deleteDatabaseFile(Ljava/lang/String;)V

    .line 330
    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mDbHelper:Lcom/evernote/android/job/JobStorage$JobOpenHelper;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobStorage$JobOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method getFailedDeleteIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 342
    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mFailedDeleteIds:Ljava/util/Set;

    return-object v0
.end method

.method getMaxJobId()I
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 353
    :try_start_0
    invoke-virtual {p0}, Lcom/evernote/android/job/JobStorage;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    const-string v3, "SELECT MAX(_id) FROM jobs"

    .line 354
    invoke-virtual {v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 355
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 356
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 362
    :goto_0
    invoke-static {v1}, Lcom/evernote/android/job/JobStorage;->closeCursor(Landroid/database/Cursor;)V

    .line 363
    invoke-static {v2}, Lcom/evernote/android/job/JobStorage;->closeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    goto :goto_3

    :catch_0
    move-exception v3

    goto :goto_1

    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_3

    :catch_1
    move-exception v3

    move-object v2, v1

    .line 359
    :goto_1
    :try_start_2
    sget-object v4, Lcom/evernote/android/job/JobStorage;->CAT:Lcom/evernote/android/job/util/JobCat;

    invoke-virtual {v4, v3}, Lcom/evernote/android/job/util/JobCat;->e(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 362
    invoke-static {v1}, Lcom/evernote/android/job/JobStorage;->closeCursor(Landroid/database/Cursor;)V

    .line 363
    invoke-static {v2}, Lcom/evernote/android/job/JobStorage;->closeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    const/4 v3, 0x0

    .line 366
    :goto_2
    invoke-static {}, Lcom/evernote/android/job/JobConfig;->getJobIdOffset()I

    move-result v1

    iget-object v2, p0, Lcom/evernote/android/job/JobStorage;->mPreferences:Landroid/content/SharedPreferences;

    const-string v4, "JOB_ID_COUNTER_v2"

    invoke-interface {v2, v4, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0

    .line 362
    :goto_3
    invoke-static {v1}, Lcom/evernote/android/job/JobStorage;->closeCursor(Landroid/database/Cursor;)V

    .line 363
    invoke-static {v2}, Lcom/evernote/android/job/JobStorage;->closeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 364
    throw v0
.end method

.method injectDatabase(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .line 337
    iput-object p1, p0, Lcom/evernote/android/job/JobStorage;->mInjectedDatabase:Landroid/database/sqlite/SQLiteDatabase;

    return-void
.end method

.method public declared-synchronized nextJobId()I
    .locals 3

    monitor-enter p0

    .line 246
    :try_start_0
    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mJobCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    if-nez v0, :cond_0

    .line 247
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p0}, Lcom/evernote/android/job/JobStorage;->getMaxJobId()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/evernote/android/job/JobStorage;->mJobCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mJobCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 252
    invoke-static {}, Lcom/evernote/android/job/JobConfig;->getJobIdOffset()I

    move-result v1

    if-lt v0, v1, :cond_1

    const v2, 0x7ffff1c0

    if-lt v0, v2, :cond_2

    .line 258
    :cond_1
    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mJobCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 259
    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mJobCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 262
    :cond_2
    iget-object v1, p0, Lcom/evernote/android/job/JobStorage;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "JOB_ID_COUNTER_v2"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 264
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public put(Lcom/evernote/android/job/JobRequest;)V
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 132
    :try_start_0
    invoke-direct {p0, p1}, Lcom/evernote/android/job/JobStorage;->store(Lcom/evernote/android/job/JobRequest;)V

    .line 135
    invoke-direct {p0, p1}, Lcom/evernote/android/job/JobStorage;->updateRequestInCache(Lcom/evernote/android/job/JobRequest;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    iget-object p1, p0, Lcom/evernote/android/job/JobStorage;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 138
    throw p1
.end method

.method public remove(Lcom/evernote/android/job/JobRequest;)V
    .locals 1

    .line 222
    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getJobId()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/evernote/android/job/JobStorage;->remove(Lcom/evernote/android/job/JobRequest;I)Z

    return-void
.end method

.method public update(Lcom/evernote/android/job/JobRequest;Landroid/content/ContentValues;)V
    .locals 7

    .line 143
    iget-object v0, p0, Lcom/evernote/android/job/JobStorage;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 146
    :try_start_0
    invoke-direct {p0, p1}, Lcom/evernote/android/job/JobStorage;->updateRequestInCache(Lcom/evernote/android/job/JobRequest;)V

    .line 147
    invoke-virtual {p0}, Lcom/evernote/android/job/JobStorage;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "jobs"

    const-string v4, "_id=?"

    new-array v5, v1, [Ljava/lang/String;

    .line 148
    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getJobId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-virtual {v2, v3, p2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p2

    .line 151
    :try_start_1
    sget-object v3, Lcom/evernote/android/job/JobStorage;->CAT:Lcom/evernote/android/job/util/JobCat;

    const-string v4, "could not update %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v0

    invoke-virtual {v3, p2, v4, v1}, Lcom/evernote/android/job/util/JobCat;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 153
    :goto_0
    invoke-static {v2}, Lcom/evernote/android/job/JobStorage;->closeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 154
    iget-object p1, p0, Lcom/evernote/android/job/JobStorage;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    .line 153
    :goto_1
    invoke-static {v2}, Lcom/evernote/android/job/JobStorage;->closeDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 154
    iget-object p2, p0, Lcom/evernote/android/job/JobStorage;->mLock:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {p2}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 155
    throw p1
.end method
