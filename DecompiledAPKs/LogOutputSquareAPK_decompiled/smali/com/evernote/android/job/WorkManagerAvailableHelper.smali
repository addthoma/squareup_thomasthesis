.class final Lcom/evernote/android/job/WorkManagerAvailableHelper;
.super Ljava/lang/Object;
.source "WorkManagerAvailableHelper.java"


# static fields
.field private static final MANAGER_IN_CLASSPATH:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "androidx.work.WorkManager"

    .line 16
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    const/4 v0, 0x0

    .line 21
    :goto_0
    sput-boolean v0, Lcom/evernote/android/job/WorkManagerAvailableHelper;->MANAGER_IN_CLASSPATH:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isWorkManagerApiSupported()Z
    .locals 1

    .line 25
    sget-boolean v0, Lcom/evernote/android/job/WorkManagerAvailableHelper;->MANAGER_IN_CLASSPATH:Z

    return v0
.end method
