.class public final Lcom/squareup/orders/CreateOrderRequest$Fulfillment$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateOrderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/CreateOrderRequest$Fulfillment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/CreateOrderRequest$Fulfillment;",
        "Lcom/squareup/orders/CreateOrderRequest$Fulfillment$Builder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1595
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/orders/CreateOrderRequest$Fulfillment;
    .locals 2

    .line 1600
    new-instance v0, Lcom/squareup/orders/CreateOrderRequest$Fulfillment;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/orders/CreateOrderRequest$Fulfillment;-><init>(Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1594
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest$Fulfillment$Builder;->build()Lcom/squareup/orders/CreateOrderRequest$Fulfillment;

    move-result-object v0

    return-object v0
.end method
