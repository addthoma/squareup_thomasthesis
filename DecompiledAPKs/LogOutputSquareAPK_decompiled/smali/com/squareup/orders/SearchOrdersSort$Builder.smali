.class public final Lcom/squareup/orders/SearchOrdersSort$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SearchOrdersSort.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/SearchOrdersSort;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/SearchOrdersSort;",
        "Lcom/squareup/orders/SearchOrdersSort$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public sort_field:Lcom/squareup/orders/SearchOrdersSort$Field;

.field public sort_order:Lcom/squareup/protos/connect/v2/common/SortOrder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 122
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/orders/SearchOrdersSort;
    .locals 4

    .line 156
    new-instance v0, Lcom/squareup/orders/SearchOrdersSort;

    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersSort$Builder;->sort_field:Lcom/squareup/orders/SearchOrdersSort$Field;

    iget-object v2, p0, Lcom/squareup/orders/SearchOrdersSort$Builder;->sort_order:Lcom/squareup/protos/connect/v2/common/SortOrder;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/orders/SearchOrdersSort;-><init>(Lcom/squareup/orders/SearchOrdersSort$Field;Lcom/squareup/protos/connect/v2/common/SortOrder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 117
    invoke-virtual {p0}, Lcom/squareup/orders/SearchOrdersSort$Builder;->build()Lcom/squareup/orders/SearchOrdersSort;

    move-result-object v0

    return-object v0
.end method

.method public sort_field(Lcom/squareup/orders/SearchOrdersSort$Field;)Lcom/squareup/orders/SearchOrdersSort$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/orders/SearchOrdersSort$Builder;->sort_field:Lcom/squareup/orders/SearchOrdersSort$Field;

    return-object p0
.end method

.method public sort_order(Lcom/squareup/protos/connect/v2/common/SortOrder;)Lcom/squareup/orders/SearchOrdersSort$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/orders/SearchOrdersSort$Builder;->sort_order:Lcom/squareup/protos/connect/v2/common/SortOrder;

    return-object p0
.end method
