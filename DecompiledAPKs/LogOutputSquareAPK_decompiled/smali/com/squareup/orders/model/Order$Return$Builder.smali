.class public final Lcom/squareup/orders/model/Order$Return$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$Return;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$Return;",
        "Lcom/squareup/orders/model/Order$Return$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

.field public return_discounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnDiscount;",
            ">;"
        }
    .end annotation
.end field

.field public return_line_items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public return_service_charges:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnServiceCharge;",
            ">;"
        }
    .end annotation
.end field

.field public return_taxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnTax;",
            ">;"
        }
    .end annotation
.end field

.field public return_tips:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnTip;",
            ">;"
        }
    .end annotation
.end field

.field public rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

.field public source_order_id:Ljava/lang/String;

.field public uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 11311
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 11312
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Return$Builder;->return_line_items:Ljava/util/List;

    .line 11313
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Return$Builder;->return_service_charges:Ljava/util/List;

    .line 11314
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Return$Builder;->return_taxes:Ljava/util/List;

    .line 11315
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Return$Builder;->return_discounts:Ljava/util/List;

    .line 11316
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Return$Builder;->return_tips:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/orders/model/Order$Return;
    .locals 12

    .line 11423
    new-instance v11, Lcom/squareup/orders/model/Order$Return;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Return$Builder;->uid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$Return$Builder;->source_order_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$Return$Builder;->return_line_items:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/orders/model/Order$Return$Builder;->return_service_charges:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/orders/model/Order$Return$Builder;->return_taxes:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/orders/model/Order$Return$Builder;->return_discounts:Ljava/util/List;

    iget-object v7, p0, Lcom/squareup/orders/model/Order$Return$Builder;->return_tips:Ljava/util/List;

    iget-object v8, p0, Lcom/squareup/orders/model/Order$Return$Builder;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    iget-object v9, p0, Lcom/squareup/orders/model/Order$Return$Builder;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/orders/model/Order$Return;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/orders/model/Order$RoundingAdjustment;Lcom/squareup/orders/model/Order$MoneyAmounts;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 11292
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Return$Builder;->build()Lcom/squareup/orders/model/Order$Return;

    move-result-object v0

    return-object v0
.end method

.method public return_amounts(Lcom/squareup/orders/model/Order$MoneyAmounts;)Lcom/squareup/orders/model/Order$Return$Builder;
    .locals 0

    .line 11417
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Return$Builder;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    return-object p0
.end method

.method public return_discounts(Ljava/util/List;)Lcom/squareup/orders/model/Order$Return$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnDiscount;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Return$Builder;"
        }
    .end annotation

    .line 11384
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 11385
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Return$Builder;->return_discounts:Ljava/util/List;

    return-object p0
.end method

.method public return_line_items(Ljava/util/List;)Lcom/squareup/orders/model/Order$Return$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnLineItem;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Return$Builder;"
        }
    .end annotation

    .line 11346
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 11347
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Return$Builder;->return_line_items:Ljava/util/List;

    return-object p0
.end method

.method public return_service_charges(Ljava/util/List;)Lcom/squareup/orders/model/Order$Return$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnServiceCharge;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Return$Builder;"
        }
    .end annotation

    .line 11358
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 11359
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Return$Builder;->return_service_charges:Ljava/util/List;

    return-object p0
.end method

.method public return_taxes(Ljava/util/List;)Lcom/squareup/orders/model/Order$Return$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnTax;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Return$Builder;"
        }
    .end annotation

    .line 11371
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 11372
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Return$Builder;->return_taxes:Ljava/util/List;

    return-object p0
.end method

.method public return_tips(Ljava/util/List;)Lcom/squareup/orders/model/Order$Return$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnTip;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Return$Builder;"
        }
    .end annotation

    .line 11394
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 11395
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Return$Builder;->return_tips:Ljava/util/List;

    return-object p0
.end method

.method public rounding_adjustment(Lcom/squareup/orders/model/Order$RoundingAdjustment;)Lcom/squareup/orders/model/Order$Return$Builder;
    .locals 0

    .line 11407
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Return$Builder;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    return-object p0
.end method

.method public source_order_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Return$Builder;
    .locals 0

    .line 11336
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Return$Builder;->source_order_id:Ljava/lang/String;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Return$Builder;
    .locals 0

    .line 11325
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Return$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method
