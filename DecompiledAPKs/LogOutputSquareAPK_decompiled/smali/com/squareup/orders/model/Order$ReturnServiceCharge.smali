.class public final Lcom/squareup/orders/model/Order$ReturnServiceCharge;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReturnServiceCharge"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$ReturnServiceCharge$ProtoAdapter_ReturnServiceCharge;,
        Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$ReturnServiceCharge;",
        "Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$ReturnServiceCharge;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CALCULATION_PHASE:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

.field public static final DEFAULT_CATALOG_OBJECT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_SOURCE_SERVICE_CHARGE_UID:Ljava/lang/String; = ""

.field public static final DEFAULT_TAXABLE:Ljava/lang/Boolean;

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final amount_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final applied_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final applied_taxes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$AppliedTax#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xf
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedTax;",
            ">;"
        }
    .end annotation
.end field

.field public final calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$ServiceCharge$CalculationPhase#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final catalog_object_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final catalog_object_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x6
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final percentage:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final return_taxes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$ReturnTax#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xe
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnTax;",
            ">;"
        }
    .end annotation
.end field

.field public final source_service_charge_uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final taxable:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xd
    .end annotation
.end field

.field public final total_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 13388
    new-instance v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$ProtoAdapter_ReturnServiceCharge;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReturnServiceCharge$ProtoAdapter_ReturnServiceCharge;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 13400
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

    .line 13404
    sget-object v0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->SERVICE_CHARGE_CALCULATION_PHASE_DO_NOT_USE:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->DEFAULT_CALCULATION_PHASE:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    const/4 v0, 0x0

    .line 13406
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->DEFAULT_TAXABLE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnTax;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedTax;",
            ">;)V"
        }
    .end annotation

    .line 13603
    sget-object v15, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/orders/model/Order$ReturnServiceCharge;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnTax;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedTax;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    .line 13612
    sget-object v1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p15

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 13613
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->uid:Ljava/lang/String;

    move-object v1, p2

    .line 13614
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->source_service_charge_uid:Ljava/lang/String;

    move-object v1, p3

    .line 13615
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->name:Ljava/lang/String;

    move-object v1, p4

    .line 13616
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->catalog_object_id:Ljava/lang/String;

    move-object v1, p5

    .line 13617
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->catalog_object_version:Ljava/lang/Long;

    move-object v1, p6

    .line 13618
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->percentage:Ljava/lang/String;

    move-object v1, p7

    .line 13619
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    move-object v1, p8

    .line 13620
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    move-object v1, p9

    .line 13621
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    move-object v1, p10

    .line 13622
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    move-object v1, p11

    .line 13623
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    move-object v1, p12

    .line 13624
    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->taxable:Ljava/lang/Boolean;

    const-string v1, "return_taxes"

    move-object/from16 v2, p13

    .line 13625
    invoke-static {v1, v2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->return_taxes:Ljava/util/List;

    const-string v1, "applied_taxes"

    move-object/from16 v2, p14

    .line 13626
    invoke-static {v1, v2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->applied_taxes:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 13653
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 13654
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;

    .line 13655
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->uid:Ljava/lang/String;

    .line 13656
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->source_service_charge_uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->source_service_charge_uid:Ljava/lang/String;

    .line 13657
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->name:Ljava/lang/String;

    .line 13658
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->catalog_object_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->catalog_object_id:Ljava/lang/String;

    .line 13659
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->catalog_object_version:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->catalog_object_version:Ljava/lang/Long;

    .line 13660
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->percentage:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->percentage:Ljava/lang/String;

    .line 13661
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13662
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13663
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13664
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13665
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    .line 13666
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->taxable:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->taxable:Ljava/lang/Boolean;

    .line 13667
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->return_taxes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->return_taxes:Ljava/util/List;

    .line 13668
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->applied_taxes:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->applied_taxes:Ljava/util/List;

    .line 13669
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 13674
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_c

    .line 13676
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 13677
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->uid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13678
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->source_service_charge_uid:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13679
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13680
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13681
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13682
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13683
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13684
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13685
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13686
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13687
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13688
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->taxable:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_b
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 13689
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->return_taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13690
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->applied_taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 13691
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_c
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;
    .locals 2

    .line 13631
    new-instance v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;-><init>()V

    .line 13632
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->uid:Ljava/lang/String;

    .line 13633
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->source_service_charge_uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->source_service_charge_uid:Ljava/lang/String;

    .line 13634
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->name:Ljava/lang/String;

    .line 13635
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->catalog_object_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->catalog_object_id:Ljava/lang/String;

    .line 13636
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->catalog_object_version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->catalog_object_version:Ljava/lang/Long;

    .line 13637
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->percentage:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->percentage:Ljava/lang/String;

    .line 13638
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13639
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13640
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13641
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13642
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    .line 13643
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->taxable:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->taxable:Ljava/lang/Boolean;

    .line 13644
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->return_taxes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->return_taxes:Ljava/util/List;

    .line 13645
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->applied_taxes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->applied_taxes:Ljava/util/List;

    .line 13646
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 13387
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->newBuilder()Lcom/squareup/orders/model/Order$ReturnServiceCharge$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 13698
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 13699
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13700
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->source_service_charge_uid:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", source_service_charge_uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->source_service_charge_uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13701
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13702
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", catalog_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->catalog_object_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13703
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_4

    const-string v1, ", catalog_object_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->catalog_object_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13704
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->percentage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13705
    :cond_5
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", amount_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13706
    :cond_6
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_7

    const-string v1, ", applied_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13707
    :cond_7
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_8

    const-string v1, ", total_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13708
    :cond_8
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_9

    const-string v1, ", total_tax_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13709
    :cond_9
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    if-eqz v1, :cond_a

    const-string v1, ", calculation_phase="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13710
    :cond_a
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->taxable:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", taxable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->taxable:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13711
    :cond_b
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->return_taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, ", return_taxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->return_taxes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13712
    :cond_c
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->applied_taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, ", applied_taxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnServiceCharge;->applied_taxes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_d
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReturnServiceCharge{"

    .line 13713
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
