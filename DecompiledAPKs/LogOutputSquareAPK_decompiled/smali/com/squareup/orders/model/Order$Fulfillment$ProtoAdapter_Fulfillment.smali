.class final Lcom/squareup/orders/model/Order$Fulfillment$ProtoAdapter_Fulfillment;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$Fulfillment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Fulfillment"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/model/Order$Fulfillment;",
        ">;"
    }
.end annotation


# instance fields
.field private final metadata:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 7538
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/model/Order$Fulfillment;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    .line 7535
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->newMapAdapter(Lcom/squareup/wire/ProtoAdapter;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment$ProtoAdapter_Fulfillment;->metadata:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$Fulfillment;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7579
    new-instance v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;-><init>()V

    .line 7580
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 7581
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/16 v4, 0xc

    if-eq v3, v4, :cond_3

    const/16 v4, 0xd

    if-eq v3, v4, :cond_2

    const/16 v4, 0x2328

    if-eq v3, v4, :cond_1

    const/16 v4, 0x2710

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 7625
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 7612
    :pswitch_0
    sget-object v3, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->shipment_details(Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    goto :goto_0

    .line 7606
    :pswitch_1
    :try_start_0
    sget-object v4, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    invoke-virtual {v0, v4}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->line_item_application(Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 7608
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 7603
    :pswitch_2
    sget-object v3, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->managed_delivery_details(Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    goto :goto_0

    .line 7602
    :pswitch_3
    sget-object v3, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->pickup_details(Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    goto :goto_0

    .line 7601
    :pswitch_4
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->metadata:Ljava/util/Map;

    iget-object v4, p0, Lcom/squareup/orders/model/Order$Fulfillment$ProtoAdapter_Fulfillment;->metadata:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    invoke-interface {v3, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_0

    .line 7600
    :pswitch_5
    iget-object v3, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->entries:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 7594
    :pswitch_6
    :try_start_1
    sget-object v4, Lcom/squareup/orders/model/Order$Fulfillment$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-virtual {v0, v4}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->state(Lcom/squareup/orders/model/Order$Fulfillment$State;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v4

    .line 7596
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 7586
    :pswitch_7
    :try_start_2
    sget-object v4, Lcom/squareup/orders/model/Order$FulfillmentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v0, v4}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->type(Lcom/squareup/orders/model/Order$FulfillmentType;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    :try_end_2
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v4

    .line 7588
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 7583
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    goto/16 :goto_0

    .line 7623
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->ext_fulfillment_client_details(Lcom/squareup/protos/client/orders/FulfillmentClientDetails;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    goto/16 :goto_0

    .line 7617
    :cond_1
    :try_start_3
    sget-object v4, Lcom/squareup/orders/model/Order$Fulfillment$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-virtual {v0, v4}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->was_status(Lcom/squareup/orders/model/Order$Fulfillment$State;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    :try_end_3
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v4

    .line 7619
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 7614
    :cond_2
    sget-object v3, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->delivery_details(Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    goto/16 :goto_0

    .line 7613
    :cond_3
    sget-object v3, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->digital_details(Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    goto/16 :goto_0

    .line 7629
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 7630
    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->build()Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7534
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$Fulfillment$ProtoAdapter_Fulfillment;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$Fulfillment;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7561
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Fulfillment;->uid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7562
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7563
    sget-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Fulfillment;->state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7564
    sget-object v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Fulfillment;->line_item_application:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7565
    sget-object v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Fulfillment;->entries:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7566
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment$ProtoAdapter_Fulfillment;->metadata:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Fulfillment;->metadata:Ljava/util/Map;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7567
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7568
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Fulfillment;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7569
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7570
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Fulfillment;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7571
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7572
    sget-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Fulfillment;->was_status:Lcom/squareup/orders/model/Order$Fulfillment$State;

    const/16 v2, 0x2328

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7573
    sget-object v0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Fulfillment;->ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    const/16 v2, 0x2710

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7574
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$Fulfillment;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7534
    check-cast p2, Lcom/squareup/orders/model/Order$Fulfillment;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/model/Order$Fulfillment$ProtoAdapter_Fulfillment;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$Fulfillment;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/model/Order$Fulfillment;)I
    .locals 4

    .line 7543
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Fulfillment;->uid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v3, 0x2

    .line 7544
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Fulfillment;->state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    const/4 v3, 0x3

    .line 7545
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Fulfillment;->line_item_application:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    const/16 v3, 0x8

    .line 7546
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 7547
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Fulfillment;->entries:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$ProtoAdapter_Fulfillment;->metadata:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Fulfillment;->metadata:Ljava/util/Map;

    const/4 v3, 0x5

    .line 7548
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    const/4 v3, 0x6

    .line 7549
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Fulfillment;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    const/4 v3, 0x7

    .line 7550
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    const/16 v3, 0x9

    .line 7551
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Fulfillment;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    const/16 v3, 0xc

    .line 7552
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    const/16 v3, 0xd

    .line 7553
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Fulfillment;->was_status:Lcom/squareup/orders/model/Order$Fulfillment$State;

    const/16 v3, 0x2328

    .line 7554
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Fulfillment;->ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    const/16 v3, 0x2710

    .line 7555
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7556
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Fulfillment;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 7534
    check-cast p1, Lcom/squareup/orders/model/Order$Fulfillment;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$Fulfillment$ProtoAdapter_Fulfillment;->encodedSize(Lcom/squareup/orders/model/Order$Fulfillment;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/model/Order$Fulfillment;)Lcom/squareup/orders/model/Order$Fulfillment;
    .locals 2

    .line 7635
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Fulfillment;->newBuilder()Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    move-result-object p1

    .line 7636
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->entries:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 7637
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->metadata:Ljava/util/Map;

    .line 7638
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    .line 7639
    :cond_0
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    .line 7640
    :cond_1
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    .line 7641
    :cond_2
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    .line 7642
    :cond_3
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    .line 7643
    :cond_4
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    .line 7644
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 7645
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->build()Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 7534
    check-cast p1, Lcom/squareup/orders/model/Order$Fulfillment;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$Fulfillment$ProtoAdapter_Fulfillment;->redact(Lcom/squareup/orders/model/Order$Fulfillment;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p1

    return-object p1
.end method
