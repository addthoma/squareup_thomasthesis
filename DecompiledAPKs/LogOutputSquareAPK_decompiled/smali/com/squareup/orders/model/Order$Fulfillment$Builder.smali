.class public final Lcom/squareup/orders/model/Order$Fulfillment$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$Fulfillment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$Fulfillment;",
        "Lcom/squareup/orders/model/Order$Fulfillment$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

.field public digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

.field public entries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;",
            ">;"
        }
    .end annotation
.end field

.field public ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

.field public line_item_application:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

.field public managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

.field public metadata:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

.field public shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

.field public state:Lcom/squareup/orders/model/Order$Fulfillment$State;

.field public type:Lcom/squareup/orders/model/Order$FulfillmentType;

.field public uid:Ljava/lang/String;

.field public was_status:Lcom/squareup/orders/model/Order$Fulfillment$State;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 6977
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 6978
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->entries:Ljava/util/List;

    .line 6979
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->metadata:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/orders/model/Order$Fulfillment;
    .locals 17

    move-object/from16 v0, p0

    .line 7124
    new-instance v16, Lcom/squareup/orders/model/Order$Fulfillment;

    iget-object v2, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->uid:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    iget-object v4, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    iget-object v5, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->line_item_application:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    iget-object v6, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->entries:Ljava/util/List;

    iget-object v7, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->metadata:Ljava/util/Map;

    iget-object v8, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    iget-object v9, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    iget-object v10, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    iget-object v11, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    iget-object v12, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    iget-object v13, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->was_status:Lcom/squareup/orders/model/Order$Fulfillment$State;

    iget-object v14, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v15

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/squareup/orders/model/Order$Fulfillment;-><init>(Ljava/lang/String;Lcom/squareup/orders/model/Order$FulfillmentType;Lcom/squareup/orders/model/Order$Fulfillment$State;Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;Ljava/util/List;Ljava/util/Map;Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;Lcom/squareup/orders/model/Order$Fulfillment$State;Lcom/squareup/protos/client/orders/FulfillmentClientDetails;Lokio/ByteString;)V

    return-object v16
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 6950
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->build()Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v0

    return-object v0
.end method

.method public delivery_details(Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    .locals 0

    .line 7100
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    return-object p0
.end method

.method public digital_details(Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    .locals 0

    .line 7095
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    return-object p0
.end method

.method public entries(Ljava/util/List;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Fulfillment$Builder;"
        }
    .end annotation

    .line 7024
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 7025
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->entries:Ljava/util/List;

    return-object p0
.end method

.method public ext_fulfillment_client_details(Lcom/squareup/protos/client/orders/FulfillmentClientDetails;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    .locals 0

    .line 7118
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    return-object p0
.end method

.method public line_item_application(Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    .locals 0

    .line 7019
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->line_item_application:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    return-object p0
.end method

.method public managed_delivery_details(Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    .locals 0

    .line 7071
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    return-object p0
.end method

.method public metadata(Ljava/util/Map;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/orders/model/Order$Fulfillment$Builder;"
        }
    .end annotation

    .line 7052
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/Map;)V

    .line 7053
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->metadata:Ljava/util/Map;

    return-object p0
.end method

.method public pickup_details(Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    .locals 0

    .line 7065
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    return-object p0
.end method

.method public shipment_details(Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    .locals 0

    .line 7090
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    return-object p0
.end method

.method public state(Lcom/squareup/orders/model/Order$Fulfillment$State;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    .locals 0

    .line 7008
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    return-object p0
.end method

.method public type(Lcom/squareup/orders/model/Order$FulfillmentType;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    .locals 0

    .line 6998
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    .locals 0

    .line 6988
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method

.method public was_status(Lcom/squareup/orders/model/Order$Fulfillment$State;)Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 7112
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->was_status:Lcom/squareup/orders/model/Order$Fulfillment$State;

    return-object p0
.end method
