.class public final Lcom/squareup/orders/model/Order$PaymentGroup$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$PaymentGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$PaymentGroup;",
        "Lcom/squareup/orders/model/Order$PaymentGroup$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public payment_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public state:Lcom/squareup/orders/model/Order$PaymentGroup$State;

.field public uid:Ljava/lang/String;

.field public will_complete_at:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 11620
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 11621
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$PaymentGroup$Builder;->payment_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/orders/model/Order$PaymentGroup;
    .locals 7

    .line 11661
    new-instance v6, Lcom/squareup/orders/model/Order$PaymentGroup;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup$Builder;->uid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$PaymentGroup$Builder;->state:Lcom/squareup/orders/model/Order$PaymentGroup$State;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$PaymentGroup$Builder;->payment_ids:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/orders/model/Order$PaymentGroup$Builder;->will_complete_at:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/orders/model/Order$PaymentGroup;-><init>(Ljava/lang/String;Lcom/squareup/orders/model/Order$PaymentGroup$State;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 11611
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$PaymentGroup$Builder;->build()Lcom/squareup/orders/model/Order$PaymentGroup;

    move-result-object v0

    return-object v0
.end method

.method public payment_ids(Ljava/util/List;)Lcom/squareup/orders/model/Order$PaymentGroup$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/orders/model/Order$PaymentGroup$Builder;"
        }
    .end annotation

    .line 11645
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 11646
    iput-object p1, p0, Lcom/squareup/orders/model/Order$PaymentGroup$Builder;->payment_ids:Ljava/util/List;

    return-object p0
.end method

.method public state(Lcom/squareup/orders/model/Order$PaymentGroup$State;)Lcom/squareup/orders/model/Order$PaymentGroup$Builder;
    .locals 0

    .line 11636
    iput-object p1, p0, Lcom/squareup/orders/model/Order$PaymentGroup$Builder;->state:Lcom/squareup/orders/model/Order$PaymentGroup$State;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$PaymentGroup$Builder;
    .locals 0

    .line 11628
    iput-object p1, p0, Lcom/squareup/orders/model/Order$PaymentGroup$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method

.method public will_complete_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$PaymentGroup$Builder;
    .locals 0

    .line 11655
    iput-object p1, p0, Lcom/squareup/orders/model/Order$PaymentGroup$Builder;->will_complete_at:Ljava/lang/String;

    return-object p0
.end method
