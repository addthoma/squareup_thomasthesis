.class final Lcom/squareup/orders/model/Order$ReservedDiscountCode$ProtoAdapter_ReservedDiscountCode;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$ReservedDiscountCode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ReservedDiscountCode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/model/Order$ReservedDiscountCode;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 15397
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/model/Order$ReservedDiscountCode;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$ReservedDiscountCode;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15418
    new-instance v0, Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;-><init>()V

    .line 15419
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 15420
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 15426
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 15424
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;->quantity(Ljava/lang/Integer;)Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;

    goto :goto_0

    .line 15423
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;->pricing_rule_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;

    goto :goto_0

    .line 15422
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;->id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;

    goto :goto_0

    .line 15430
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 15431
    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;->build()Lcom/squareup/orders/model/Order$ReservedDiscountCode;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15395
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$ReservedDiscountCode$ProtoAdapter_ReservedDiscountCode;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$ReservedDiscountCode;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$ReservedDiscountCode;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15410
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15411
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->pricing_rule_id:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15412
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->quantity:Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15413
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15395
    check-cast p2, Lcom/squareup/orders/model/Order$ReservedDiscountCode;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/model/Order$ReservedDiscountCode$ProtoAdapter_ReservedDiscountCode;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$ReservedDiscountCode;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/model/Order$ReservedDiscountCode;)I
    .locals 4

    .line 15402
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->pricing_rule_id:Ljava/lang/String;

    const/4 v3, 0x2

    .line 15403
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->quantity:Ljava/lang/Integer;

    const/4 v3, 0x3

    .line 15404
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15405
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 15395
    check-cast p1, Lcom/squareup/orders/model/Order$ReservedDiscountCode;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$ReservedDiscountCode$ProtoAdapter_ReservedDiscountCode;->encodedSize(Lcom/squareup/orders/model/Order$ReservedDiscountCode;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/model/Order$ReservedDiscountCode;)Lcom/squareup/orders/model/Order$ReservedDiscountCode;
    .locals 0

    .line 15436
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->newBuilder()Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;

    move-result-object p1

    .line 15437
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 15438
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;->build()Lcom/squareup/orders/model/Order$ReservedDiscountCode;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15395
    check-cast p1, Lcom/squareup/orders/model/Order$ReservedDiscountCode;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$ReservedDiscountCode$ProtoAdapter_ReservedDiscountCode;->redact(Lcom/squareup/orders/model/Order$ReservedDiscountCode;)Lcom/squareup/orders/model/Order$ReservedDiscountCode;

    move-result-object p1

    return-object p1
.end method
