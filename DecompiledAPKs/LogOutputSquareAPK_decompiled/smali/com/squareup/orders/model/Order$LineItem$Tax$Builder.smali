.class public final Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$LineItem$Tax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$LineItem$Tax;",
        "Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public applied_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

.field public catalog_object_id:Ljava/lang/String;

.field public catalog_object_version:Ljava/lang/Long;

.field public metadata:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public name:Ljava/lang/String;

.field public percentage:Ljava/lang/String;

.field public scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

.field public type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

.field public uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 4648
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 4649
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->metadata:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public applied_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;
    .locals 0

    .line 4751
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$LineItem$Tax;
    .locals 13

    .line 4773
    new-instance v12, Lcom/squareup/orders/model/Order$LineItem$Tax;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->uid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->catalog_object_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->catalog_object_version:Ljava/lang/Long;

    iget-object v4, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    iget-object v6, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->percentage:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    iget-object v8, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->metadata:Ljava/util/Map;

    iget-object v9, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v10, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/orders/model/Order$LineItem$Tax;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$Type;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;Ljava/util/Map;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4627
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->build()Lcom/squareup/orders/model/Order$LineItem$Tax;

    move-result-object v0

    return-object v0
.end method

.method public calculation_phase(Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;)Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;
    .locals 0

    .line 4712
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    return-object p0
.end method

.method public catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;
    .locals 0

    .line 4669
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->catalog_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public catalog_object_version(Ljava/lang/Long;)Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;
    .locals 0

    .line 4674
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->catalog_object_version:Ljava/lang/Long;

    return-object p0
.end method

.method public metadata(Ljava/util/Map;)Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;"
        }
    .end annotation

    .line 4739
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/Map;)V

    .line 4740
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->metadata:Ljava/util/Map;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;
    .locals 0

    .line 4684
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public percentage(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;
    .locals 0

    .line 4706
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->percentage:Ljava/lang/String;

    return-object p0
.end method

.method public scope(Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;)Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;
    .locals 0

    .line 4767
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    return-object p0
.end method

.method public type(Lcom/squareup/orders/model/Order$LineItem$Tax$Type;)Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;
    .locals 0

    .line 4694
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;
    .locals 0

    .line 4658
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Tax$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method
