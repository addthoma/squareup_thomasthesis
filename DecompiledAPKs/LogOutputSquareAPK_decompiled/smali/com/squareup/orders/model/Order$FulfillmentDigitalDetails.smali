.class public final Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FulfillmentDigitalDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$ProtoAdapter_FulfillmentDigitalDetails;,
        Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;",
        "Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CANCELED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_CANCEL_REASON:Ljava/lang/String; = ""

.field public static final DEFAULT_COMPLETED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_PLACED_AT:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final cancel_reason:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final canceled_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final completed_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final placed_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$FulfillmentRecipient#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9783
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$ProtoAdapter_FulfillmentDigitalDetails;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$ProtoAdapter_FulfillmentDigitalDetails;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/orders/model/Order$FulfillmentRecipient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 9842
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;-><init>(Lcom/squareup/orders/model/Order$FulfillmentRecipient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/orders/model/Order$FulfillmentRecipient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 9847
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 9848
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    .line 9849
    iput-object p2, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->placed_at:Ljava/lang/String;

    .line 9850
    iput-object p3, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->completed_at:Ljava/lang/String;

    .line 9851
    iput-object p4, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->canceled_at:Ljava/lang/String;

    .line 9852
    iput-object p5, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->cancel_reason:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 9870
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 9871
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    .line 9872
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    .line 9873
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->placed_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->placed_at:Ljava/lang/String;

    .line 9874
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->completed_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->completed_at:Ljava/lang/String;

    .line 9875
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->canceled_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->canceled_at:Ljava/lang/String;

    .line 9876
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->cancel_reason:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->cancel_reason:Ljava/lang/String;

    .line 9877
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 9882
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 9884
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 9885
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9886
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->placed_at:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9887
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->completed_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9888
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->canceled_at:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9889
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->cancel_reason:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 9890
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;
    .locals 2

    .line 9857
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;-><init>()V

    .line 9858
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    .line 9859
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->placed_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->placed_at:Ljava/lang/String;

    .line 9860
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->completed_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->completed_at:Ljava/lang/String;

    .line 9861
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->canceled_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->canceled_at:Ljava/lang/String;

    .line 9862
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->cancel_reason:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->cancel_reason:Ljava/lang/String;

    .line 9863
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 9782
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->newBuilder()Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 9897
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 9898
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    if-eqz v1, :cond_0

    const-string v1, ", recipient="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9899
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->placed_at:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", placed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->placed_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9900
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->completed_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", completed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->completed_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9901
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->canceled_at:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", canceled_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->canceled_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9902
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->cancel_reason:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", cancel_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->cancel_reason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FulfillmentDigitalDetails{"

    .line 9903
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
