.class final Lcom/squareup/orders/model/Order$Source$ProtoAdapter_Source;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$Source;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Source"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/model/Order$Source;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1790
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/model/Order$Source;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$Source;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1811
    new-instance v0, Lcom/squareup/orders/model/Order$Source$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$Source$Builder;-><init>()V

    .line 1812
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1813
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 1819
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1817
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Source$Builder;->application_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Source$Builder;

    goto :goto_0

    .line 1816
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Source$Builder;->application_name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Source$Builder;

    goto :goto_0

    .line 1815
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Source$Builder;->name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Source$Builder;

    goto :goto_0

    .line 1823
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/model/Order$Source$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1824
    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$Source$Builder;->build()Lcom/squareup/orders/model/Order$Source;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1788
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$Source$ProtoAdapter_Source;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$Source;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$Source;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1803
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Source;->name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1804
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Source;->application_name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1805
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Source;->application_id:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1806
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$Source;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1788
    check-cast p2, Lcom/squareup/orders/model/Order$Source;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/model/Order$Source$ProtoAdapter_Source;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$Source;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/model/Order$Source;)I
    .locals 4

    .line 1795
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Source;->name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Source;->application_name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 1796
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Source;->application_id:Ljava/lang/String;

    const/4 v3, 0x3

    .line 1797
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1798
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Source;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1788
    check-cast p1, Lcom/squareup/orders/model/Order$Source;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$Source$ProtoAdapter_Source;->encodedSize(Lcom/squareup/orders/model/Order$Source;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/model/Order$Source;)Lcom/squareup/orders/model/Order$Source;
    .locals 0

    .line 1829
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Source;->newBuilder()Lcom/squareup/orders/model/Order$Source$Builder;

    move-result-object p1

    .line 1830
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Source$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1831
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Source$Builder;->build()Lcom/squareup/orders/model/Order$Source;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1788
    check-cast p1, Lcom/squareup/orders/model/Order$Source;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$Source$ProtoAdapter_Source;->redact(Lcom/squareup/orders/model/Order$Source;)Lcom/squareup/orders/model/Order$Source;

    move-result-object p1

    return-object p1
.end method
