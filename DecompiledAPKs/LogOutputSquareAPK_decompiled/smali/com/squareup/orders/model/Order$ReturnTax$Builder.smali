.class public final Lcom/squareup/orders/model/Order$ReturnTax$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$ReturnTax;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$ReturnTax;",
        "Lcom/squareup/orders/model/Order$ReturnTax$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public applied_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

.field public catalog_object_id:Ljava/lang/String;

.field public catalog_object_version:Ljava/lang/Long;

.field public name:Ljava/lang/String;

.field public percentage:Ljava/lang/String;

.field public scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

.field public source_tax_uid:Ljava/lang/String;

.field public type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

.field public uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14278
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public applied_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnTax$Builder;
    .locals 0

    .line 14359
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$ReturnTax;
    .locals 13

    .line 14378
    new-instance v12, Lcom/squareup/orders/model/Order$ReturnTax;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->uid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->source_tax_uid:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->catalog_object_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->catalog_object_version:Ljava/lang/Long;

    iget-object v5, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->name:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    iget-object v7, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->percentage:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    iget-object v9, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v10, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/orders/model/Order$ReturnTax;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$Type;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 14257
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->build()Lcom/squareup/orders/model/Order$ReturnTax;

    move-result-object v0

    return-object v0
.end method

.method public calculation_phase(Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;)Lcom/squareup/orders/model/Order$ReturnTax$Builder;
    .locals 0

    .line 14349
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->calculation_phase:Lcom/squareup/orders/model/Order$LineItem$Tax$TaxCalculationPhaseScope$TaxCalculationPhase;

    return-object p0
.end method

.method public catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnTax$Builder;
    .locals 0

    .line 14307
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->catalog_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public catalog_object_version(Ljava/lang/Long;)Lcom/squareup/orders/model/Order$ReturnTax$Builder;
    .locals 0

    .line 14312
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->catalog_object_version:Ljava/lang/Long;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnTax$Builder;
    .locals 0

    .line 14322
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public percentage(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnTax$Builder;
    .locals 0

    .line 14343
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->percentage:Ljava/lang/String;

    return-object p0
.end method

.method public scope(Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;)Lcom/squareup/orders/model/Order$ReturnTax$Builder;
    .locals 0

    .line 14372
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->scope:Lcom/squareup/orders/model/Order$LineItem$Tax$Scope;

    return-object p0
.end method

.method public source_tax_uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnTax$Builder;
    .locals 0

    .line 14297
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->source_tax_uid:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/orders/model/Order$LineItem$Tax$Type;)Lcom/squareup/orders/model/Order$ReturnTax$Builder;
    .locals 0

    .line 14332
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnTax$Builder;
    .locals 0

    .line 14287
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnTax$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method
