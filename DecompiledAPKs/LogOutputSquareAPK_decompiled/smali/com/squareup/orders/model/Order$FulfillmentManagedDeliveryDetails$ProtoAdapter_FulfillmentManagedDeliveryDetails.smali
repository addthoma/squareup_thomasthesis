.class final Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$ProtoAdapter_FulfillmentManagedDeliveryDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FulfillmentManagedDeliveryDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 9030
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9058
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;-><init>()V

    .line 9059
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 9060
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 9069
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 9067
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->picked_up_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;

    goto :goto_0

    .line 9066
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->ready_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;

    goto :goto_0

    .line 9065
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->accepted_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;

    goto :goto_0

    .line 9064
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->pickup_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;

    goto :goto_0

    .line 9063
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->placed_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;

    goto :goto_0

    .line 9062
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->note(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;

    goto :goto_0

    .line 9073
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 9074
    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9028
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$ProtoAdapter_FulfillmentManagedDeliveryDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9047
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->note:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9048
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->placed_at:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9049
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->pickup_at:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9050
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->accepted_at:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9051
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->ready_at:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9052
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->picked_up_at:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9053
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9028
    check-cast p2, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$ProtoAdapter_FulfillmentManagedDeliveryDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;)I
    .locals 4

    .line 9035
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->note:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->placed_at:Ljava/lang/String;

    const/4 v3, 0x2

    .line 9036
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->pickup_at:Ljava/lang/String;

    const/4 v3, 0x3

    .line 9037
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->accepted_at:Ljava/lang/String;

    const/4 v3, 0x4

    .line 9038
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->ready_at:Ljava/lang/String;

    const/4 v3, 0x5

    .line 9039
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->picked_up_at:Ljava/lang/String;

    const/4 v3, 0x6

    .line 9040
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9041
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 9028
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$ProtoAdapter_FulfillmentManagedDeliveryDetails;->encodedSize(Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;)Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;
    .locals 0

    .line 9079
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->newBuilder()Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;

    move-result-object p1

    .line 9080
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 9081
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 9028
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails$ProtoAdapter_FulfillmentManagedDeliveryDetails;->redact(Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;)Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    move-result-object p1

    return-object p1
.end method
