.class public final enum Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;
.super Ljava/lang/Enum;
.source "Order.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$Fulfillment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FulfillmentLineItemApplication"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication$ProtoAdapter_FulfillmentLineItemApplication;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ALL:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

.field public static final enum ENTRY_LIST:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

.field public static final enum UNKNOWN_APPLICATION:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 7129
    new-instance v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_APPLICATION"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->UNKNOWN_APPLICATION:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    .line 7134
    new-instance v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    const/4 v2, 0x1

    const-string v3, "ALL"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ALL:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    .line 7139
    new-instance v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    const/4 v3, 0x2

    const-string v4, "ENTRY_LIST"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ENTRY_LIST:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    .line 7128
    sget-object v4, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->UNKNOWN_APPLICATION:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ALL:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ENTRY_LIST:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->$VALUES:[Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    .line 7141
    new-instance v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication$ProtoAdapter_FulfillmentLineItemApplication;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication$ProtoAdapter_FulfillmentLineItemApplication;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 7145
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 7146
    iput p3, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 7156
    :cond_0
    sget-object p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ENTRY_LIST:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    return-object p0

    .line 7155
    :cond_1
    sget-object p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ALL:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    return-object p0

    .line 7154
    :cond_2
    sget-object p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->UNKNOWN_APPLICATION:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;
    .locals 1

    .line 7128
    const-class v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    return-object p0
.end method

.method public static values()[Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;
    .locals 1

    .line 7128
    sget-object v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->$VALUES:[Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    invoke-virtual {v0}, [Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 7163
    iget v0, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->value:I

    return v0
.end method
