.class public final enum Lcom/squareup/orders/model/Order$Fulfillment$State;
.super Ljava/lang/Enum;
.source "Order.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$Fulfillment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$Fulfillment$State$ProtoAdapter_State;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/orders/model/Order$Fulfillment$State;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/orders/model/Order$Fulfillment$State;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$Fulfillment$State;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CANCELED:Lcom/squareup/orders/model/Order$Fulfillment$State;

.field public static final enum COMPLETED:Lcom/squareup/orders/model/Order$Fulfillment$State;

.field public static final enum FAILED:Lcom/squareup/orders/model/Order$Fulfillment$State;

.field public static final enum FULFILLMENT_STATE_DO_NOT_USE:Lcom/squareup/orders/model/Order$Fulfillment$State;

.field public static final enum PREPARED:Lcom/squareup/orders/model/Order$Fulfillment$State;

.field public static final enum PROPOSED:Lcom/squareup/orders/model/Order$Fulfillment$State;

.field public static final enum RESERVED:Lcom/squareup/orders/model/Order$Fulfillment$State;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 7448
    new-instance v0, Lcom/squareup/orders/model/Order$Fulfillment$State;

    const/4 v1, 0x0

    const-string v2, "FULFILLMENT_STATE_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/orders/model/Order$Fulfillment$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->FULFILLMENT_STATE_DO_NOT_USE:Lcom/squareup/orders/model/Order$Fulfillment$State;

    .line 7455
    new-instance v0, Lcom/squareup/orders/model/Order$Fulfillment$State;

    const/4 v2, 0x1

    const-string v3, "PROPOSED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/orders/model/Order$Fulfillment$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->PROPOSED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    .line 7462
    new-instance v0, Lcom/squareup/orders/model/Order$Fulfillment$State;

    const/4 v3, 0x2

    const-string v4, "RESERVED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/orders/model/Order$Fulfillment$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->RESERVED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    .line 7469
    new-instance v0, Lcom/squareup/orders/model/Order$Fulfillment$State;

    const/4 v4, 0x3

    const-string v5, "PREPARED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/orders/model/Order$Fulfillment$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->PREPARED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    .line 7476
    new-instance v0, Lcom/squareup/orders/model/Order$Fulfillment$State;

    const/4 v5, 0x4

    const-string v6, "COMPLETED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/orders/model/Order$Fulfillment$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->COMPLETED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    .line 7483
    new-instance v0, Lcom/squareup/orders/model/Order$Fulfillment$State;

    const/4 v6, 0x5

    const-string v7, "CANCELED"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/orders/model/Order$Fulfillment$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->CANCELED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    .line 7491
    new-instance v0, Lcom/squareup/orders/model/Order$Fulfillment$State;

    const/4 v7, 0x6

    const-string v8, "FAILED"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/orders/model/Order$Fulfillment$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->FAILED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/orders/model/Order$Fulfillment$State;

    .line 7447
    sget-object v8, Lcom/squareup/orders/model/Order$Fulfillment$State;->FULFILLMENT_STATE_DO_NOT_USE:Lcom/squareup/orders/model/Order$Fulfillment$State;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->PROPOSED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->RESERVED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->PREPARED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->COMPLETED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->CANCELED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->FAILED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->$VALUES:[Lcom/squareup/orders/model/Order$Fulfillment$State;

    .line 7493
    new-instance v0, Lcom/squareup/orders/model/Order$Fulfillment$State$ProtoAdapter_State;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$Fulfillment$State$ProtoAdapter_State;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 7497
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 7498
    iput p3, p0, Lcom/squareup/orders/model/Order$Fulfillment$State;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/orders/model/Order$Fulfillment$State;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 7512
    :pswitch_0
    sget-object p0, Lcom/squareup/orders/model/Order$Fulfillment$State;->FAILED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    return-object p0

    .line 7511
    :pswitch_1
    sget-object p0, Lcom/squareup/orders/model/Order$Fulfillment$State;->CANCELED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    return-object p0

    .line 7510
    :pswitch_2
    sget-object p0, Lcom/squareup/orders/model/Order$Fulfillment$State;->COMPLETED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    return-object p0

    .line 7509
    :pswitch_3
    sget-object p0, Lcom/squareup/orders/model/Order$Fulfillment$State;->PREPARED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    return-object p0

    .line 7508
    :pswitch_4
    sget-object p0, Lcom/squareup/orders/model/Order$Fulfillment$State;->RESERVED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    return-object p0

    .line 7507
    :pswitch_5
    sget-object p0, Lcom/squareup/orders/model/Order$Fulfillment$State;->PROPOSED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    return-object p0

    .line 7506
    :pswitch_6
    sget-object p0, Lcom/squareup/orders/model/Order$Fulfillment$State;->FULFILLMENT_STATE_DO_NOT_USE:Lcom/squareup/orders/model/Order$Fulfillment$State;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Fulfillment$State;
    .locals 1

    .line 7447
    const-class v0, Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/orders/model/Order$Fulfillment$State;

    return-object p0
.end method

.method public static values()[Lcom/squareup/orders/model/Order$Fulfillment$State;
    .locals 1

    .line 7447
    sget-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->$VALUES:[Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-virtual {v0}, [Lcom/squareup/orders/model/Order$Fulfillment$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/orders/model/Order$Fulfillment$State;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 7519
    iget v0, p0, Lcom/squareup/orders/model/Order$Fulfillment$State;->value:I

    return v0
.end method
