.class public final Lcom/squareup/orders/SearchOrdersFulfillmentFilter;
.super Lcom/squareup/wire/Message;
.source "SearchOrdersFulfillmentFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/SearchOrdersFulfillmentFilter$ProtoAdapter_SearchOrdersFulfillmentFilter;,
        Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/SearchOrdersFulfillmentFilter;",
        "Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/SearchOrdersFulfillmentFilter;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final fulfillment_states:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$Fulfillment$State#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Fulfillment$State;",
            ">;"
        }
    .end annotation
.end field

.field public final fulfillment_types:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$FulfillmentType#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$FulfillmentType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$ProtoAdapter_SearchOrdersFulfillmentFilter;

    invoke-direct {v0}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$ProtoAdapter_SearchOrdersFulfillmentFilter;-><init>()V

    sput-object v0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$FulfillmentType;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Fulfillment$State;",
            ">;)V"
        }
    .end annotation

    .line 61
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;-><init>(Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$FulfillmentType;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Fulfillment$State;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 66
    sget-object v0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p3, "fulfillment_types"

    .line 67
    invoke-static {p3, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->fulfillment_types:Ljava/util/List;

    const-string p1, "fulfillment_states"

    .line 68
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->fulfillment_states:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 83
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 84
    :cond_1
    check-cast p1, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->fulfillment_types:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->fulfillment_types:Ljava/util/List;

    .line 86
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->fulfillment_states:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->fulfillment_states:Ljava/util/List;

    .line 87
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 92
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 95
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->fulfillment_types:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 96
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->fulfillment_states:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_0
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;
    .locals 2

    .line 73
    new-instance v0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;-><init>()V

    .line 74
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->fulfillment_types:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->fulfillment_types:Ljava/util/List;

    .line 75
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->fulfillment_states:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->fulfillment_states:Ljava/util/List;

    .line 76
    invoke-virtual {p0}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->newBuilder()Lcom/squareup/orders/SearchOrdersFulfillmentFilter$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 105
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->fulfillment_types:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", fulfillment_types="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->fulfillment_types:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 106
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->fulfillment_states:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", fulfillment_states="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersFulfillmentFilter;->fulfillment_states:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SearchOrdersFulfillmentFilter{"

    .line 107
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
