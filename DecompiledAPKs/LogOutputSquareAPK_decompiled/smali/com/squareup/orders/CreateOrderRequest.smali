.class public final Lcom/squareup/orders/CreateOrderRequest;
.super Lcom/squareup/wire/Message;
.source "CreateOrderRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/CreateOrderRequest$ProtoAdapter_CreateOrderRequest;,
        Lcom/squareup/orders/CreateOrderRequest$Fulfillment;,
        Lcom/squareup/orders/CreateOrderRequest$Modifier;,
        Lcom/squareup/orders/CreateOrderRequest$Discount;,
        Lcom/squareup/orders/CreateOrderRequest$Tax;,
        Lcom/squareup/orders/CreateOrderRequest$LineItem;,
        Lcom/squareup/orders/CreateOrderRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/CreateOrderRequest;",
        "Lcom/squareup/orders/CreateOrderRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/CreateOrderRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_IDEMPOTENCY_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_LOCATION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_REFERENCE_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final discounts:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.CreateOrderRequest$Discount#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Discount;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final fulfillments:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.CreateOrderRequest$Fulfillment#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Fulfillment;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final idempotency_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final line_items:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.CreateOrderRequest$LineItem#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$LineItem;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final location_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final order:Lcom/squareup/orders/model/Order;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final reference_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final taxes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.CreateOrderRequest$Tax#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Tax;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 36
    new-instance v0, Lcom/squareup/orders/CreateOrderRequest$ProtoAdapter_CreateOrderRequest;

    invoke-direct {v0}, Lcom/squareup/orders/CreateOrderRequest$ProtoAdapter_CreateOrderRequest;-><init>()V

    sput-object v0, Lcom/squareup/orders/CreateOrderRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/orders/model/Order;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$LineItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Tax;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Discount;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Fulfillment;",
            ">;)V"
        }
    .end annotation

    .line 176
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/orders/CreateOrderRequest;-><init>(Lcom/squareup/orders/model/Order;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/orders/model/Order;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orders/model/Order;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$LineItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Tax;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Discount;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/CreateOrderRequest$Fulfillment;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 182
    sget-object v0, Lcom/squareup/orders/CreateOrderRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 183
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest;->order:Lcom/squareup/orders/model/Order;

    .line 184
    iput-object p2, p0, Lcom/squareup/orders/CreateOrderRequest;->location_id:Ljava/lang/String;

    .line 185
    iput-object p3, p0, Lcom/squareup/orders/CreateOrderRequest;->idempotency_key:Ljava/lang/String;

    .line 186
    iput-object p4, p0, Lcom/squareup/orders/CreateOrderRequest;->reference_id:Ljava/lang/String;

    const-string p1, "line_items"

    .line 187
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest;->line_items:Ljava/util/List;

    const-string p1, "taxes"

    .line 188
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest;->taxes:Ljava/util/List;

    const-string p1, "discounts"

    .line 189
    invoke-static {p1, p7}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest;->discounts:Ljava/util/List;

    const-string p1, "fulfillments"

    .line 190
    invoke-static {p1, p8}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest;->fulfillments:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 211
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/CreateOrderRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 212
    :cond_1
    check-cast p1, Lcom/squareup/orders/CreateOrderRequest;

    .line 213
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->order:Lcom/squareup/orders/model/Order;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest;->order:Lcom/squareup/orders/model/Order;

    .line 214
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->location_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest;->location_id:Ljava/lang/String;

    .line 215
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->idempotency_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest;->idempotency_key:Ljava/lang/String;

    .line 216
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->reference_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest;->reference_id:Ljava/lang/String;

    .line 217
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->line_items:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest;->line_items:Ljava/util/List;

    .line 218
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->taxes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest;->taxes:Ljava/util/List;

    .line 219
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->discounts:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest;->discounts:Ljava/util/List;

    .line 220
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->fulfillments:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/orders/CreateOrderRequest;->fulfillments:Ljava/util/List;

    .line 221
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 226
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 228
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 229
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->order:Lcom/squareup/orders/model/Order;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 230
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 231
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->idempotency_key:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 232
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->reference_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 233
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->line_items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 234
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 235
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 236
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->fulfillments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 237
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/CreateOrderRequest$Builder;
    .locals 2

    .line 195
    new-instance v0, Lcom/squareup/orders/CreateOrderRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/CreateOrderRequest$Builder;-><init>()V

    .line 196
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->order:Lcom/squareup/orders/model/Order;

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$Builder;->order:Lcom/squareup/orders/model/Order;

    .line 197
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->location_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$Builder;->location_id:Ljava/lang/String;

    .line 198
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->idempotency_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$Builder;->idempotency_key:Ljava/lang/String;

    .line 199
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->reference_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$Builder;->reference_id:Ljava/lang/String;

    .line 200
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->line_items:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$Builder;->line_items:Ljava/util/List;

    .line 201
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->taxes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$Builder;->taxes:Ljava/util/List;

    .line 202
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->discounts:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$Builder;->discounts:Ljava/util/List;

    .line 203
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->fulfillments:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$Builder;->fulfillments:Ljava/util/List;

    .line 204
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/CreateOrderRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest;->newBuilder()Lcom/squareup/orders/CreateOrderRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 244
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 245
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->order:Lcom/squareup/orders/model/Order;

    if-eqz v1, :cond_0

    const-string v1, ", order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->order:Lcom/squareup/orders/model/Order;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 246
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", location_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->location_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->idempotency_key:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", idempotency_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->idempotency_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->reference_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", reference_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->reference_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->line_items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", line_items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->line_items:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 250
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", taxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->taxes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 251
    :cond_5
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ", discounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->discounts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 252
    :cond_6
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->fulfillments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, ", fulfillments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest;->fulfillments:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CreateOrderRequest{"

    .line 253
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
