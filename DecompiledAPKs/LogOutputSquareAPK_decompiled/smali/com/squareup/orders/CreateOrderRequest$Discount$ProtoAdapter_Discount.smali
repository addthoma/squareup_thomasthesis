.class final Lcom/squareup/orders/CreateOrderRequest$Discount$ProtoAdapter_Discount;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CreateOrderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/CreateOrderRequest$Discount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Discount"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/CreateOrderRequest$Discount;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1290
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/CreateOrderRequest$Discount;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/CreateOrderRequest$Discount;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1313
    new-instance v0, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;-><init>()V

    .line 1314
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1315
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 1322
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1320
    :cond_0
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;

    goto :goto_0

    .line 1319
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->percentage(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;

    goto :goto_0

    .line 1318
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->name(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;

    goto :goto_0

    .line 1317
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;

    goto :goto_0

    .line 1326
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1327
    invoke-virtual {v0}, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->build()Lcom/squareup/orders/CreateOrderRequest$Discount;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1288
    invoke-virtual {p0, p1}, Lcom/squareup/orders/CreateOrderRequest$Discount$ProtoAdapter_Discount;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/CreateOrderRequest$Discount;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/CreateOrderRequest$Discount;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1304
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$Discount;->catalog_object_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1305
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$Discount;->name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1306
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$Discount;->percentage:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1307
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$Discount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1308
    invoke-virtual {p2}, Lcom/squareup/orders/CreateOrderRequest$Discount;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1288
    check-cast p2, Lcom/squareup/orders/CreateOrderRequest$Discount;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/CreateOrderRequest$Discount$ProtoAdapter_Discount;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/CreateOrderRequest$Discount;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/CreateOrderRequest$Discount;)I
    .locals 4

    .line 1295
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/CreateOrderRequest$Discount;->catalog_object_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/CreateOrderRequest$Discount;->name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 1296
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/CreateOrderRequest$Discount;->percentage:Ljava/lang/String;

    const/4 v3, 0x3

    .line 1297
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/CreateOrderRequest$Discount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v3, 0x4

    .line 1298
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1299
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$Discount;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1288
    check-cast p1, Lcom/squareup/orders/CreateOrderRequest$Discount;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/CreateOrderRequest$Discount$ProtoAdapter_Discount;->encodedSize(Lcom/squareup/orders/CreateOrderRequest$Discount;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/CreateOrderRequest$Discount;)Lcom/squareup/orders/CreateOrderRequest$Discount;
    .locals 2

    .line 1332
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$Discount;->newBuilder()Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;

    move-result-object p1

    .line 1333
    iget-object v0, p1, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 1334
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1335
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->build()Lcom/squareup/orders/CreateOrderRequest$Discount;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1288
    check-cast p1, Lcom/squareup/orders/CreateOrderRequest$Discount;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/CreateOrderRequest$Discount$ProtoAdapter_Discount;->redact(Lcom/squareup/orders/CreateOrderRequest$Discount;)Lcom/squareup/orders/CreateOrderRequest$Discount;

    move-result-object p1

    return-object p1
.end method
