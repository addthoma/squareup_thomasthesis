.class Lcom/squareup/comms/common/IoThread$2;
.super Ljava/lang/Object;
.source "IoThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/comms/common/IoThread;->postAndWait(Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/comms/common/IoThread;

.field final synthetic val$done:Ljava/util/concurrent/CountDownLatch;

.field final synthetic val$runnable:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/squareup/comms/common/IoThread;Ljava/lang/Runnable;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/comms/common/IoThread$2;->this$0:Lcom/squareup/comms/common/IoThread;

    iput-object p2, p0, Lcom/squareup/comms/common/IoThread$2;->val$runnable:Ljava/lang/Runnable;

    iput-object p3, p0, Lcom/squareup/comms/common/IoThread$2;->val$done:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/squareup/comms/common/IoThread$2;->val$runnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 157
    iget-object v0, p0, Lcom/squareup/comms/common/IoThread$2;->val$done:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method
