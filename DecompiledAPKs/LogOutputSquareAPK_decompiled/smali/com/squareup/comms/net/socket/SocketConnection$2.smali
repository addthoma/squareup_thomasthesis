.class Lcom/squareup/comms/net/socket/SocketConnection$2;
.super Ljava/lang/Object;
.source "SocketConnection.java"

# interfaces
.implements Lcom/squareup/comms/common/IoCompletion;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/comms/net/socket/SocketConnection;->postReadAndWrite()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/comms/net/socket/SocketConnection;


# direct methods
.method constructor <init>(Lcom/squareup/comms/net/socket/SocketConnection;)V
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/comms/net/socket/SocketConnection$2;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/io/IOException;)V
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/squareup/comms/net/socket/SocketConnection$2;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-static {v0}, Lcom/squareup/comms/net/socket/SocketConnection;->access$200(Lcom/squareup/comms/net/socket/SocketConnection;)Lcom/squareup/comms/net/ConnectionCallback;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/comms/net/ConnectionCallback;->onError(Ljava/lang/Exception;)V

    return-void
.end method

.method public onReady(Ljava/nio/channels/SelectionKey;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 108
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->isReadable()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->isWritable()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    new-array v1, v1, [Ljava/lang/Object;

    .line 109
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->interestOps()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v3, "Unexpected interest ops: %d"

    .line 108
    invoke-static {v0, v3, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 110
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->isValid()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->isWritable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 112
    :goto_2
    iget-object v0, p0, Lcom/squareup/comms/net/socket/SocketConnection$2;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-static {v0}, Lcom/squareup/comms/net/socket/SocketConnection;->access$000(Lcom/squareup/comms/net/socket/SocketConnection;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 113
    iget-object v0, p0, Lcom/squareup/comms/net/socket/SocketConnection$2;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-static {v0}, Lcom/squareup/comms/net/socket/SocketConnection;->access$000(Lcom/squareup/comms/net/socket/SocketConnection;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 114
    iget-object v1, p0, Lcom/squareup/comms/net/socket/SocketConnection$2;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-static {v1}, Lcom/squareup/comms/net/socket/SocketConnection;->access$100(Lcom/squareup/comms/net/socket/SocketConnection;)Ljava/nio/channels/SocketChannel;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/nio/channels/SocketChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 116
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-lez v0, :cond_2

    new-array v0, v2, [Ljava/lang/Object;

    const-string v1, "Buffer is full, stopping processing and re-registering."

    .line 117
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 122
    :cond_2
    iget-object v0, p0, Lcom/squareup/comms/net/socket/SocketConnection$2;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-static {v0}, Lcom/squareup/comms/net/socket/SocketConnection;->access$000(Lcom/squareup/comms/net/socket/SocketConnection;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    goto :goto_2

    .line 126
    :cond_3
    :goto_3
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->isValid()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->isReadable()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 127
    iget-object p1, p0, Lcom/squareup/comms/net/socket/SocketConnection$2;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-static {p1}, Lcom/squareup/comms/net/socket/SocketConnection;->access$100(Lcom/squareup/comms/net/socket/SocketConnection;)Ljava/nio/channels/SocketChannel;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/comms/net/socket/SocketConnection$2;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-static {v0}, Lcom/squareup/comms/net/socket/SocketConnection;->access$400(Lcom/squareup/comms/net/socket/SocketConnection;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/nio/channels/SocketChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result p1

    if-gez p1, :cond_4

    new-array p1, v2, [Ljava/lang/Object;

    const-string v0, "Read buffer is empty, disconnecting."

    .line 128
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    iget-object p1, p0, Lcom/squareup/comms/net/socket/SocketConnection$2;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-static {p1}, Lcom/squareup/comms/net/socket/SocketConnection;->access$200(Lcom/squareup/comms/net/socket/SocketConnection;)Lcom/squareup/comms/net/ConnectionCallback;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/comms/net/ConnectionCallback;->onDisconnect()V

    return-void

    .line 133
    :cond_4
    iget-object p1, p0, Lcom/squareup/comms/net/socket/SocketConnection$2;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-static {p1}, Lcom/squareup/comms/net/socket/SocketConnection;->access$200(Lcom/squareup/comms/net/socket/SocketConnection;)Lcom/squareup/comms/net/ConnectionCallback;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/comms/net/socket/SocketConnection$2;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-static {v0}, Lcom/squareup/comms/net/socket/SocketConnection;->access$400(Lcom/squareup/comms/net/socket/SocketConnection;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/net/socket/SocketConnection$2;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-static {v1}, Lcom/squareup/comms/net/socket/SocketConnection;->access$400(Lcom/squareup/comms/net/socket/SocketConnection;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    invoke-interface {p1, v0, v2, v1}, Lcom/squareup/comms/net/ConnectionCallback;->onReceive([BII)V

    .line 134
    iget-object p1, p0, Lcom/squareup/comms/net/socket/SocketConnection$2;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-static {p1}, Lcom/squareup/comms/net/socket/SocketConnection;->access$400(Lcom/squareup/comms/net/socket/SocketConnection;)Ljava/nio/ByteBuffer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 137
    :cond_5
    iget-object p1, p0, Lcom/squareup/comms/net/socket/SocketConnection$2;->this$0:Lcom/squareup/comms/net/socket/SocketConnection;

    invoke-static {p1}, Lcom/squareup/comms/net/socket/SocketConnection;->access$300(Lcom/squareup/comms/net/socket/SocketConnection;)V

    return-void
.end method
