.class Lcom/squareup/comms/net/Channel$1;
.super Ljava/lang/Object;
.source "Channel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/comms/net/Channel;->start(Lcom/squareup/comms/net/ChannelCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/comms/net/Channel;

.field final synthetic val$callback:Lcom/squareup/comms/net/ChannelCallback;


# direct methods
.method constructor <init>(Lcom/squareup/comms/net/Channel;Lcom/squareup/comms/net/ChannelCallback;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/comms/net/Channel$1;->this$0:Lcom/squareup/comms/net/Channel;

    iput-object p2, p0, Lcom/squareup/comms/net/Channel$1;->val$callback:Lcom/squareup/comms/net/ChannelCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 46
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$1;->this$0:Lcom/squareup/comms/net/Channel;

    iget-object v1, p0, Lcom/squareup/comms/net/Channel$1;->val$callback:Lcom/squareup/comms/net/ChannelCallback;

    invoke-static {v0, v1}, Lcom/squareup/comms/net/Channel;->access$002(Lcom/squareup/comms/net/Channel;Lcom/squareup/comms/net/ChannelCallback;)Lcom/squareup/comms/net/ChannelCallback;

    .line 47
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$1;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$300(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/ConnectionFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/net/Channel$1;->this$0:Lcom/squareup/comms/net/Channel;

    .line 48
    invoke-static {v1}, Lcom/squareup/comms/net/Channel;->access$100(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/common/IoThread;

    move-result-object v1

    new-instance v2, Lcom/squareup/comms/net/Channel$ConnectionFactoryListener;

    iget-object v3, p0, Lcom/squareup/comms/net/Channel$1;->this$0:Lcom/squareup/comms/net/Channel;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/squareup/comms/net/Channel$ConnectionFactoryListener;-><init>(Lcom/squareup/comms/net/Channel;Lcom/squareup/comms/net/Channel$1;)V

    .line 47
    invoke-static {v1, v2}, Lcom/squareup/comms/net/Callbacks;->serializedFactoryCallback(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/net/ConnectionFactory$Callback;)Lcom/squareup/comms/net/ConnectionFactory$Callback;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/comms/net/ConnectionFactory;->setCallback(Lcom/squareup/comms/net/ConnectionFactory$Callback;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$1;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$300(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/ConnectionFactory;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/comms/net/ConnectionFactory;->requestConnection()V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Channel started"

    .line 51
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
