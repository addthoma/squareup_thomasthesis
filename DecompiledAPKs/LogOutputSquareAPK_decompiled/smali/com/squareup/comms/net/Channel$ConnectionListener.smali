.class Lcom/squareup/comms/net/Channel$ConnectionListener;
.super Ljava/lang/Object;
.source "Channel.java"

# interfaces
.implements Lcom/squareup/comms/net/ConnectionCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/net/Channel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectionListener"
.end annotation


# instance fields
.field private targetConnection:Lcom/squareup/comms/net/Connection;

.field final synthetic this$0:Lcom/squareup/comms/net/Channel;


# direct methods
.method constructor <init>(Lcom/squareup/comms/net/Channel;Lcom/squareup/comms/net/Connection;)V
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/squareup/comms/net/Channel$ConnectionListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    iput-object p2, p0, Lcom/squareup/comms/net/Channel$ConnectionListener;->targetConnection:Lcom/squareup/comms/net/Connection;

    return-void
.end method

.method private closeAndReconnect()V
    .locals 2

    .line 167
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$ConnectionListener;->targetConnection:Lcom/squareup/comms/net/Connection;

    invoke-interface {v0}, Lcom/squareup/comms/net/Connection;->close()V

    .line 174
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$ConnectionListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$400(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/Connection;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/net/Channel$ConnectionListener;->targetConnection:Lcom/squareup/comms/net/Connection;

    if-ne v0, v1, :cond_0

    .line 175
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$ConnectionListener;->this$0:Lcom/squareup/comms/net/Channel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/comms/net/Channel;->access$402(Lcom/squareup/comms/net/Channel;Lcom/squareup/comms/net/Connection;)Lcom/squareup/comms/net/Connection;

    .line 176
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$ConnectionListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$000(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/ChannelCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/comms/net/ChannelCallback;->onDisconnected()V

    .line 178
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$ConnectionListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$500(Lcom/squareup/comms/net/Channel;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$ConnectionListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$300(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/ConnectionFactory;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/comms/net/ConnectionFactory;->requestConnection()V

    :cond_0
    return-void
.end method


# virtual methods
.method public onDisconnect()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 162
    iget-object v1, p0, Lcom/squareup/comms/net/Channel$ConnectionListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v1}, Lcom/squareup/comms/net/Channel;->access$400(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/Connection;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Remote endpoint disconnected, closing connection: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 163
    invoke-direct {p0}, Lcom/squareup/comms/net/Channel$ConnectionListener;->closeAndReconnect()V

    return-void
.end method

.method public onError(Ljava/lang/Exception;)V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 157
    iget-object v1, p0, Lcom/squareup/comms/net/Channel$ConnectionListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v1}, Lcom/squareup/comms/net/Channel;->access$400(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/Connection;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const-string p1, "Error, closing connection: %s. Message: %s"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    invoke-direct {p0}, Lcom/squareup/comms/net/Channel$ConnectionListener;->closeAndReconnect()V

    return-void
.end method

.method public onReceive([BII)V
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/squareup/comms/net/Channel$ConnectionListener;->this$0:Lcom/squareup/comms/net/Channel;

    invoke-static {v0}, Lcom/squareup/comms/net/Channel;->access$000(Lcom/squareup/comms/net/Channel;)Lcom/squareup/comms/net/ChannelCallback;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/comms/net/ChannelCallback;->onReceive([BII)V

    return-void
.end method
