.class Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback$1;
.super Ljava/lang/Object;
.source "Callbacks.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback;->onNewConnection(Lcom/squareup/comms/net/Connection;Lcom/squareup/comms/net/Device;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback;

.field final synthetic val$connection:Lcom/squareup/comms/net/Connection;

.field final synthetic val$device:Lcom/squareup/comms/net/Device;


# direct methods
.method constructor <init>(Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback;Lcom/squareup/comms/net/Connection;Lcom/squareup/comms/net/Device;)V
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback$1;->this$0:Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback;

    iput-object p2, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback$1;->val$connection:Lcom/squareup/comms/net/Connection;

    iput-object p3, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback$1;->val$device:Lcom/squareup/comms/net/Device;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 85
    iget-object v0, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback$1;->this$0:Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback;

    invoke-static {v0}, Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback;->access$100(Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback;)Lcom/squareup/comms/net/ConnectionFactory$Callback;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback$1;->val$connection:Lcom/squareup/comms/net/Connection;

    iget-object v2, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback$1;->val$device:Lcom/squareup/comms/net/Device;

    invoke-interface {v0, v1, v2}, Lcom/squareup/comms/net/ConnectionFactory$Callback;->onNewConnection(Lcom/squareup/comms/net/Connection;Lcom/squareup/comms/net/Device;)V

    return-void
.end method
