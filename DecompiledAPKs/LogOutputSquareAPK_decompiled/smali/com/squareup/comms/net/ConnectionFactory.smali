.class public interface abstract Lcom/squareup/comms/net/ConnectionFactory;
.super Ljava/lang/Object;
.source "ConnectionFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/net/ConnectionFactory$Callback;
    }
.end annotation


# virtual methods
.method public abstract close()V
.end method

.method public abstract requestConnection()V
.end method

.method public abstract setCallback(Lcom/squareup/comms/net/ConnectionFactory$Callback;)V
.end method
