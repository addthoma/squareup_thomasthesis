.class Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;
.super Ljava/lang/Object;
.source "FilteringRemoteBusConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/FilteringRemoteBusConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PendingAck"
.end annotation


# instance fields
.field private final blacklist:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/wire/Message;",
            ">;>;"
        }
    .end annotation
.end field

.field private final messageString:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/wire/Message;",
            ">;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "messageString"

    .line 34
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;->messageString:Ljava/lang/String;

    const-string p2, "blacklist"

    .line 35
    invoke-static {p1, p2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;->blacklist:Ljava/util/List;

    return-void
.end method

.method static allowAll(Ljava/lang/String;)Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;
    .locals 2

    .line 43
    new-instance v0, Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;-><init>(Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method

.method static blacklist(Ljava/util/List;Ljava/lang/String;)Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/wire/Message;",
            ">;>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;

    invoke-direct {v0, p0, p1}, Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;-><init>(Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getMessageString()Ljava/lang/String;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;->messageString:Ljava/lang/String;

    return-object v0
.end method

.method ignore(Lcom/squareup/wire/Message;)Z
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;->blacklist:Ljava/util/List;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PendingAck{blacklist="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;->blacklist:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", messageString=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;->messageString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
