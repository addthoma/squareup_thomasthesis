.class public abstract Lcom/squareup/comms/LoggingConnectionListener;
.super Ljava/lang/Object;
.source "LoggingConnectionListener.java"

# interfaces
.implements Lcom/squareup/comms/ConnectionListener;


# instance fields
.field private final delegate:Lcom/squareup/comms/ConnectionListener;


# direct methods
.method public constructor <init>(Lcom/squareup/comms/ConnectionListener;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/comms/LoggingConnectionListener;->delegate:Lcom/squareup/comms/ConnectionListener;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/comms/LoggingConnectionListener;Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;
    .locals 0

    .line 10
    invoke-direct {p0, p1}, Lcom/squareup/comms/LoggingConnectionListener;->unpack(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;

    move-result-object p0

    return-object p0
.end method

.method private unpack(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;
    .locals 1

    .line 39
    instance-of v0, p1, Lcom/squareup/comms/protos/seller/SendMessageToReader;

    if-eqz v0, :cond_0

    .line 40
    check-cast p1, Lcom/squareup/comms/protos/seller/SendMessageToReader;

    invoke-static {p1}, Lcom/squareup/comms/ProtoConverter;->toReaderProto(Lcom/squareup/comms/protos/seller/SendMessageToReader;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;

    move-result-object p1

    .line 42
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/buyer/ProcessMessageFromReader;

    if-eqz v0, :cond_1

    .line 43
    check-cast p1, Lcom/squareup/comms/protos/buyer/ProcessMessageFromReader;

    invoke-static {p1}, Lcom/squareup/comms/ProtoConverter;->toReaderProto(Lcom/squareup/comms/protos/buyer/ProcessMessageFromReader;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;

    move-result-object p1

    :cond_1
    return-object p1
.end method


# virtual methods
.method public synthetic lambda$onConnected$0$LoggingConnectionListener(Lcom/squareup/wire/Message;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/comms/LoggingConnectionListener;->unpack(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/comms/LoggingConnectionListener;->logRpcMessageReceived(Lcom/squareup/wire/Message;)V

    return-void
.end method

.method protected abstract logRpcMessageReceived(Lcom/squareup/wire/Message;)V
.end method

.method protected abstract logRpcMessageSent(Lcom/squareup/wire/Message;)V
.end method

.method public final onConnected(Lcom/squareup/comms/RemoteBusConnection;Lcom/squareup/comms/net/Device;)V
    .locals 2

    .line 20
    invoke-interface {p1}, Lcom/squareup/comms/RemoteBusConnection;->observable()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/comms/-$$Lambda$LoggingConnectionListener$TS0mPX70JEh6hzdik7h4wI0YUa8;

    invoke-direct {v1, p0}, Lcom/squareup/comms/-$$Lambda$LoggingConnectionListener$TS0mPX70JEh6hzdik7h4wI0YUa8;-><init>(Lcom/squareup/comms/LoggingConnectionListener;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 22
    iget-object v0, p0, Lcom/squareup/comms/LoggingConnectionListener;->delegate:Lcom/squareup/comms/ConnectionListener;

    new-instance v1, Lcom/squareup/comms/LoggingConnectionListener$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/comms/LoggingConnectionListener$1;-><init>(Lcom/squareup/comms/LoggingConnectionListener;Lcom/squareup/comms/RemoteBusConnection;)V

    invoke-interface {v0, v1, p2}, Lcom/squareup/comms/ConnectionListener;->onConnected(Lcom/squareup/comms/RemoteBusConnection;Lcom/squareup/comms/net/Device;)V

    return-void
.end method
