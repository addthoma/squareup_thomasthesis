.class public final Lcom/squareup/comms/protos/common/TenderType$Companion;
.super Ljava/lang/Object;
.source "TenderType.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/common/TenderType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0008H\u0007R\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/comms/protos/common/TenderType$Companion;",
        "",
        "()V",
        "ADAPTER",
        "Lcom/squareup/wire/ProtoAdapter;",
        "Lcom/squareup/comms/protos/common/TenderType;",
        "fromValue",
        "value",
        "",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/comms/protos/common/TenderType$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromValue(I)Lcom/squareup/comms/protos/common/TenderType;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    packed-switch p1, :pswitch_data_0

    .line 46
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 45
    :pswitch_0
    sget-object p1, Lcom/squareup/comms/protos/common/TenderType;->CASH:Lcom/squareup/comms/protos/common/TenderType;

    goto :goto_0

    .line 44
    :pswitch_1
    sget-object p1, Lcom/squareup/comms/protos/common/TenderType;->GIFT_CARD:Lcom/squareup/comms/protos/common/TenderType;

    goto :goto_0

    .line 43
    :pswitch_2
    sget-object p1, Lcom/squareup/comms/protos/common/TenderType;->CARD_NOT_PRESENT:Lcom/squareup/comms/protos/common/TenderType;

    goto :goto_0

    .line 42
    :pswitch_3
    sget-object p1, Lcom/squareup/comms/protos/common/TenderType;->CARD_ON_FILE:Lcom/squareup/comms/protos/common/TenderType;

    goto :goto_0

    .line 41
    :pswitch_4
    sget-object p1, Lcom/squareup/comms/protos/common/TenderType;->INVOICE:Lcom/squareup/comms/protos/common/TenderType;

    goto :goto_0

    .line 40
    :pswitch_5
    sget-object p1, Lcom/squareup/comms/protos/common/TenderType;->OTHER:Lcom/squareup/comms/protos/common/TenderType;

    goto :goto_0

    .line 39
    :pswitch_6
    sget-object p1, Lcom/squareup/comms/protos/common/TenderType;->SPLIT:Lcom/squareup/comms/protos/common/TenderType;

    :goto_0
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
