.class public final enum Lcom/squareup/comms/protos/common/Card$Brand;
.super Ljava/lang/Enum;
.source "Card.kt"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/common/Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Brand"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/common/Card$Brand$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/comms/protos/common/Card$Brand;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0014\u0008\u0086\u0001\u0018\u0000 \u00172\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002:\u0001\u0017B\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/comms/protos/common/Card$Brand;",
        "",
        "Lcom/squareup/wire/WireEnum;",
        "value",
        "",
        "(Ljava/lang/String;II)V",
        "getValue",
        "()I",
        "UNKNOWN",
        "VISA",
        "MASTER_CARD",
        "AMERICAN_EXPRESS",
        "DISCOVER",
        "DISCOVER_DINERS",
        "JCB",
        "UNION_PAY",
        "SQUARE_GIFT_CARD_V2",
        "INTERAC",
        "SQUARE_CAPITAL_CARD",
        "EFTPOS",
        "FELICA",
        "ALIPAY",
        "CASH_APP",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/comms/protos/common/Card$Brand;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/common/Card$Brand;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ALIPAY:Lcom/squareup/comms/protos/common/Card$Brand;

.field public static final enum AMERICAN_EXPRESS:Lcom/squareup/comms/protos/common/Card$Brand;

.field public static final enum CASH_APP:Lcom/squareup/comms/protos/common/Card$Brand;

.field public static final Companion:Lcom/squareup/comms/protos/common/Card$Brand$Companion;

.field public static final enum DISCOVER:Lcom/squareup/comms/protos/common/Card$Brand;

.field public static final enum DISCOVER_DINERS:Lcom/squareup/comms/protos/common/Card$Brand;

.field public static final enum EFTPOS:Lcom/squareup/comms/protos/common/Card$Brand;

.field public static final enum FELICA:Lcom/squareup/comms/protos/common/Card$Brand;

.field public static final enum INTERAC:Lcom/squareup/comms/protos/common/Card$Brand;

.field public static final enum JCB:Lcom/squareup/comms/protos/common/Card$Brand;

.field public static final enum MASTER_CARD:Lcom/squareup/comms/protos/common/Card$Brand;

.field public static final enum SQUARE_CAPITAL_CARD:Lcom/squareup/comms/protos/common/Card$Brand;

.field public static final enum SQUARE_GIFT_CARD_V2:Lcom/squareup/comms/protos/common/Card$Brand;

.field public static final enum UNION_PAY:Lcom/squareup/comms/protos/common/Card$Brand;

.field public static final enum UNKNOWN:Lcom/squareup/comms/protos/common/Card$Brand;

.field public static final enum VISA:Lcom/squareup/comms/protos/common/Card$Brand;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0xf

    new-array v0, v0, [Lcom/squareup/comms/protos/common/Card$Brand;

    new-instance v1, Lcom/squareup/comms/protos/common/Card$Brand;

    const/4 v2, 0x0

    const-string v3, "UNKNOWN"

    .line 476
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/Card$Brand;->UNKNOWN:Lcom/squareup/comms/protos/common/Card$Brand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/Card$Brand;

    const/4 v2, 0x1

    const-string v3, "VISA"

    .line 478
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/Card$Brand;->VISA:Lcom/squareup/comms/protos/common/Card$Brand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/Card$Brand;

    const/4 v2, 0x2

    const-string v3, "MASTER_CARD"

    .line 480
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/Card$Brand;->MASTER_CARD:Lcom/squareup/comms/protos/common/Card$Brand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/Card$Brand;

    const/4 v2, 0x3

    const-string v3, "AMERICAN_EXPRESS"

    .line 482
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/comms/protos/common/Card$Brand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/Card$Brand;

    const/4 v2, 0x4

    const-string v3, "DISCOVER"

    .line 484
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/Card$Brand;->DISCOVER:Lcom/squareup/comms/protos/common/Card$Brand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/Card$Brand;

    const/4 v2, 0x5

    const-string v3, "DISCOVER_DINERS"

    .line 486
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/comms/protos/common/Card$Brand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/Card$Brand;

    const/4 v2, 0x6

    const-string v3, "JCB"

    .line 488
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/Card$Brand;->JCB:Lcom/squareup/comms/protos/common/Card$Brand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/Card$Brand;

    const/4 v2, 0x7

    const-string v3, "UNION_PAY"

    .line 490
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/Card$Brand;->UNION_PAY:Lcom/squareup/comms/protos/common/Card$Brand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/Card$Brand;

    const/16 v2, 0x8

    const-string v3, "SQUARE_GIFT_CARD_V2"

    .line 492
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/comms/protos/common/Card$Brand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/Card$Brand;

    const/16 v2, 0x9

    const-string v3, "INTERAC"

    .line 494
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/Card$Brand;->INTERAC:Lcom/squareup/comms/protos/common/Card$Brand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/Card$Brand;

    const/16 v2, 0xa

    const-string v3, "SQUARE_CAPITAL_CARD"

    .line 496
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/comms/protos/common/Card$Brand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/Card$Brand;

    const/16 v2, 0xb

    const-string v3, "EFTPOS"

    .line 498
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/Card$Brand;->EFTPOS:Lcom/squareup/comms/protos/common/Card$Brand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/Card$Brand;

    const/16 v2, 0xc

    const-string v3, "FELICA"

    .line 500
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/Card$Brand;->FELICA:Lcom/squareup/comms/protos/common/Card$Brand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/Card$Brand;

    const/16 v2, 0xd

    const-string v3, "ALIPAY"

    .line 502
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/Card$Brand;->ALIPAY:Lcom/squareup/comms/protos/common/Card$Brand;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/Card$Brand;

    const/16 v2, 0xe

    const-string v3, "CASH_APP"

    .line 504
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/Card$Brand;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/Card$Brand;->CASH_APP:Lcom/squareup/comms/protos/common/Card$Brand;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/comms/protos/common/Card$Brand;->$VALUES:[Lcom/squareup/comms/protos/common/Card$Brand;

    new-instance v0, Lcom/squareup/comms/protos/common/Card$Brand$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/common/Card$Brand$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/common/Card$Brand;->Companion:Lcom/squareup/comms/protos/common/Card$Brand$Companion;

    .line 508
    new-instance v0, Lcom/squareup/comms/protos/common/Card$Brand$Companion$ADAPTER$1;

    .line 509
    const-class v1, Lcom/squareup/comms/protos/common/Card$Brand;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/common/Card$Brand$Companion$ADAPTER$1;-><init>(Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/common/Card$Brand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 473
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/comms/protos/common/Card$Brand;->value:I

    return-void
.end method

.method public static final fromValue(I)Lcom/squareup/comms/protos/common/Card$Brand;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/comms/protos/common/Card$Brand;->Companion:Lcom/squareup/comms/protos/common/Card$Brand$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/comms/protos/common/Card$Brand$Companion;->fromValue(I)Lcom/squareup/comms/protos/common/Card$Brand;

    move-result-object p0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Brand;
    .locals 1

    const-class v0, Lcom/squareup/comms/protos/common/Card$Brand;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/comms/protos/common/Card$Brand;

    return-object p0
.end method

.method public static values()[Lcom/squareup/comms/protos/common/Card$Brand;
    .locals 1

    sget-object v0, Lcom/squareup/comms/protos/common/Card$Brand;->$VALUES:[Lcom/squareup/comms/protos/common/Card$Brand;

    invoke-virtual {v0}, [Lcom/squareup/comms/protos/common/Card$Brand;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/comms/protos/common/Card$Brand;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 474
    iget v0, p0, Lcom/squareup/comms/protos/common/Card$Brand;->value:I

    return v0
.end method
