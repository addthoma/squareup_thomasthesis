.class public final Lcom/squareup/comms/protos/common/Card$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Card.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/common/Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/common/Card;",
        "Lcom/squareup/comms/protos/common/Card$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\n\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0008\u0010\u0015\u001a\u00020\u0002H\u0016J\u0010\u0010\u0006\u001a\u00020\u00002\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u0008\u001a\u00020\u00002\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\t\u001a\u00020\u00002\u0008\u0010\t\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\n\u001a\u00020\u00002\u0008\u0010\n\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u000b\u001a\u00020\u00002\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cJ\u0010\u0010\r\u001a\u00020\u00002\u0008\u0010\r\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u000e\u001a\u00020\u00002\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u000f\u001a\u00020\u00002\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u0010\u001a\u00020\u00002\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u0011\u001a\u00020\u00002\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u0012\u001a\u00020\u00002\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u0013\u001a\u00020\u00002\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u0014\u001a\u00020\u00002\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0007R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u000c8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/comms/protos/common/Card$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/common/Card;",
        "()V",
        "brand",
        "Lcom/squareup/comms/protos/common/Card$Brand;",
        "country_code",
        "",
        "discretionary_data",
        "expiration_month",
        "expiration_year",
        "input_type",
        "Lcom/squareup/comms/protos/common/Card$InputType;",
        "name",
        "pan",
        "pin_verification",
        "postal_code",
        "service_code",
        "title",
        "track_data",
        "verification",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public brand:Lcom/squareup/comms/protos/common/Card$Brand;

.field public country_code:Ljava/lang/String;

.field public discretionary_data:Ljava/lang/String;

.field public expiration_month:Ljava/lang/String;

.field public expiration_year:Ljava/lang/String;

.field public input_type:Lcom/squareup/comms/protos/common/Card$InputType;

.field public name:Ljava/lang/String;

.field public pan:Ljava/lang/String;

.field public pin_verification:Ljava/lang/String;

.field public postal_code:Ljava/lang/String;

.field public service_code:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public track_data:Ljava/lang/String;

.field public verification:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 227
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public final brand(Lcom/squareup/comms/protos/common/Card$Brand;)Lcom/squareup/comms/protos/common/Card$Builder;
    .locals 0

    .line 331
    iput-object p1, p0, Lcom/squareup/comms/protos/common/Card$Builder;->brand:Lcom/squareup/comms/protos/common/Card$Brand;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/common/Card;
    .locals 18

    move-object/from16 v0, p0

    .line 340
    new-instance v17, Lcom/squareup/comms/protos/common/Card;

    .line 341
    iget-object v2, v0, Lcom/squareup/comms/protos/common/Card$Builder;->track_data:Ljava/lang/String;

    .line 342
    iget-object v3, v0, Lcom/squareup/comms/protos/common/Card$Builder;->pan:Ljava/lang/String;

    .line 343
    iget-object v4, v0, Lcom/squareup/comms/protos/common/Card$Builder;->country_code:Ljava/lang/String;

    .line 344
    iget-object v5, v0, Lcom/squareup/comms/protos/common/Card$Builder;->name:Ljava/lang/String;

    .line 345
    iget-object v6, v0, Lcom/squareup/comms/protos/common/Card$Builder;->title:Ljava/lang/String;

    .line 346
    iget-object v7, v0, Lcom/squareup/comms/protos/common/Card$Builder;->expiration_year:Ljava/lang/String;

    .line 347
    iget-object v8, v0, Lcom/squareup/comms/protos/common/Card$Builder;->expiration_month:Ljava/lang/String;

    .line 348
    iget-object v9, v0, Lcom/squareup/comms/protos/common/Card$Builder;->service_code:Ljava/lang/String;

    .line 349
    iget-object v10, v0, Lcom/squareup/comms/protos/common/Card$Builder;->pin_verification:Ljava/lang/String;

    .line 350
    iget-object v11, v0, Lcom/squareup/comms/protos/common/Card$Builder;->discretionary_data:Ljava/lang/String;

    .line 351
    iget-object v12, v0, Lcom/squareup/comms/protos/common/Card$Builder;->verification:Ljava/lang/String;

    .line 352
    iget-object v13, v0, Lcom/squareup/comms/protos/common/Card$Builder;->postal_code:Ljava/lang/String;

    .line 353
    iget-object v14, v0, Lcom/squareup/comms/protos/common/Card$Builder;->brand:Lcom/squareup/comms/protos/common/Card$Brand;

    .line 354
    iget-object v15, v0, Lcom/squareup/comms/protos/common/Card$Builder;->input_type:Lcom/squareup/comms/protos/common/Card$InputType;

    .line 355
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/comms/protos/common/Card$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v16

    move-object/from16 v1, v17

    .line 340
    invoke-direct/range {v1 .. v16}, Lcom/squareup/comms/protos/common/Card;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/comms/protos/common/Card$Brand;Lcom/squareup/comms/protos/common/Card$InputType;Lokio/ByteString;)V

    return-object v17
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 227
    invoke-virtual {p0}, Lcom/squareup/comms/protos/common/Card$Builder;->build()Lcom/squareup/comms/protos/common/Card;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final country_code(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;
    .locals 0

    .line 281
    iput-object p1, p0, Lcom/squareup/comms/protos/common/Card$Builder;->country_code:Ljava/lang/String;

    return-object p0
.end method

.method public final discretionary_data(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;
    .locals 0

    .line 316
    iput-object p1, p0, Lcom/squareup/comms/protos/common/Card$Builder;->discretionary_data:Ljava/lang/String;

    return-object p0
.end method

.method public final expiration_month(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;
    .locals 0

    .line 301
    iput-object p1, p0, Lcom/squareup/comms/protos/common/Card$Builder;->expiration_month:Ljava/lang/String;

    return-object p0
.end method

.method public final expiration_year(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;
    .locals 0

    .line 296
    iput-object p1, p0, Lcom/squareup/comms/protos/common/Card$Builder;->expiration_year:Ljava/lang/String;

    return-object p0
.end method

.method public final input_type(Lcom/squareup/comms/protos/common/Card$InputType;)Lcom/squareup/comms/protos/common/Card$Builder;
    .locals 0

    .line 336
    iput-object p1, p0, Lcom/squareup/comms/protos/common/Card$Builder;->input_type:Lcom/squareup/comms/protos/common/Card$InputType;

    return-object p0
.end method

.method public final name(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;
    .locals 0

    .line 286
    iput-object p1, p0, Lcom/squareup/comms/protos/common/Card$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public final pan(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;
    .locals 0

    .line 276
    iput-object p1, p0, Lcom/squareup/comms/protos/common/Card$Builder;->pan:Ljava/lang/String;

    return-object p0
.end method

.method public final pin_verification(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;
    .locals 0

    .line 311
    iput-object p1, p0, Lcom/squareup/comms/protos/common/Card$Builder;->pin_verification:Ljava/lang/String;

    return-object p0
.end method

.method public final postal_code(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;
    .locals 0

    .line 326
    iput-object p1, p0, Lcom/squareup/comms/protos/common/Card$Builder;->postal_code:Ljava/lang/String;

    return-object p0
.end method

.method public final service_code(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;
    .locals 0

    .line 306
    iput-object p1, p0, Lcom/squareup/comms/protos/common/Card$Builder;->service_code:Ljava/lang/String;

    return-object p0
.end method

.method public final title(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;
    .locals 0

    .line 291
    iput-object p1, p0, Lcom/squareup/comms/protos/common/Card$Builder;->title:Ljava/lang/String;

    return-object p0
.end method

.method public final track_data(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;
    .locals 0

    .line 271
    iput-object p1, p0, Lcom/squareup/comms/protos/common/Card$Builder;->track_data:Ljava/lang/String;

    return-object p0
.end method

.method public final verification(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;
    .locals 0

    .line 321
    iput-object p1, p0, Lcom/squareup/comms/protos/common/Card$Builder;->verification:Ljava/lang/String;

    return-object p0
.end method
