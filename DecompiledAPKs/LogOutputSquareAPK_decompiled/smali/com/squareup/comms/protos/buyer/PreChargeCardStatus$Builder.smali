.class public final Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PreChargeCardStatus.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;",
        "Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\n\u001a\u00020\u0002H\u0016J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u000e\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\u0005J\u0010\u0010\u0008\u001a\u00020\u00002\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tR\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0014\u0010\u0008\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;",
        "()V",
        "chip_card_swiped",
        "",
        "Ljava/lang/Boolean;",
        "inserted",
        "pre_swipe",
        "Lcom/squareup/comms/protos/buyer/PreSwipe;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public chip_card_swiped:Ljava/lang/Boolean;

.field public inserted:Ljava/lang/Boolean;

.field public pre_swipe:Lcom/squareup/comms/protos/buyer/PreSwipe;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 107
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;
    .locals 6

    .line 141
    new-instance v0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;

    .line 142
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;->inserted:Ljava/lang/Boolean;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 143
    iget-object v5, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;->chip_card_swiped:Ljava/lang/Boolean;

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 145
    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;->pre_swipe:Lcom/squareup/comms/protos/buyer/PreSwipe;

    .line 146
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    .line 141
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;-><init>(ZZLcom/squareup/comms/protos/buyer/PreSwipe;Lokio/ByteString;)V

    return-object v0

    :cond_0
    new-array v0, v4, [Ljava/lang/Object;

    aput-object v5, v0, v3

    const-string v1, "chip_card_swiped"

    aput-object v1, v0, v2

    .line 143
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v0, v4, [Ljava/lang/Object;

    aput-object v1, v0, v3

    const-string v1, "inserted"

    aput-object v1, v0, v2

    .line 142
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 107
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;->build()Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final chip_card_swiped(Z)Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;
    .locals 0

    .line 129
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;->chip_card_swiped:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final inserted(Z)Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;
    .locals 0

    .line 121
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;->inserted:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final pre_swipe(Lcom/squareup/comms/protos/buyer/PreSwipe;)Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/PreChargeCardStatus$Builder;->pre_swipe:Lcom/squareup/comms/protos/buyer/PreSwipe;

    return-object p0
.end method
