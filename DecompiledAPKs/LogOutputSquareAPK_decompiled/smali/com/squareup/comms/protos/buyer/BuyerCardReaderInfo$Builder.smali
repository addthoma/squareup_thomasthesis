.class public final Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BuyerCardReaderInfo.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;",
        "Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0008\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0008\u0010\u000e\u001a\u00020\u0002H\u0016J\u000e\u0010\u0006\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007J\u000e\u0010\t\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0007J\u000e\u0010\n\u001a\u00020\u00002\u0006\u0010\n\u001a\u00020\u0005J\u000e\u0010\u000b\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\u0005J\u000e\u0010\u000c\u001a\u00020\u00002\u0006\u0010\u000c\u001a\u00020\u0005J\u0010\u0010\r\u001a\u00020\u00002\u0008\u0010\r\u001a\u0004\u0018\u00010\u0005R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0008R\u0016\u0010\t\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0008R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;",
        "()V",
        "address",
        "",
        "card_reader_id",
        "",
        "Ljava/lang/Integer;",
        "charge_cycle_count",
        "connection_type",
        "firmware_version",
        "hardware_serial_number",
        "reader_name",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public address:Ljava/lang/String;

.field public card_reader_id:Ljava/lang/Integer;

.field public charge_cycle_count:Ljava/lang/Integer;

.field public connection_type:Ljava/lang/String;

.field public firmware_version:Ljava/lang/String;

.field public hardware_serial_number:Ljava/lang/String;

.field public reader_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 138
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public final address(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;->address:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;
    .locals 13

    .line 195
    new-instance v9, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    .line 196
    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;->card_reader_id:Ljava/lang/Integer;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 198
    iget-object v5, p0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;->connection_type:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 200
    iget-object v6, p0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;->firmware_version:Ljava/lang/String;

    if-eqz v6, :cond_2

    .line 202
    iget-object v7, p0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;->hardware_serial_number:Ljava/lang/String;

    if-eqz v7, :cond_1

    .line 204
    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;->charge_cycle_count:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 206
    iget-object v10, p0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;->address:Ljava/lang/String;

    .line 207
    iget-object v11, p0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;->reader_name:Ljava/lang/String;

    .line 208
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v9

    move v1, v4

    move-object v2, v5

    move-object v3, v6

    move-object v4, v7

    move v5, v8

    move-object v6, v10

    move-object v7, v11

    move-object v8, v12

    .line 195
    invoke-direct/range {v0 .. v8}, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v9

    :cond_0
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "charge_cycle_count"

    aput-object v0, v3, v1

    .line 204
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v0, v3, [Ljava/lang/Object;

    aput-object v7, v0, v2

    const-string v2, "hardware_serial_number"

    aput-object v2, v0, v1

    .line 203
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_2
    new-array v0, v3, [Ljava/lang/Object;

    aput-object v6, v0, v2

    const-string v2, "firmware_version"

    aput-object v2, v0, v1

    .line 200
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_3
    new-array v0, v3, [Ljava/lang/Object;

    aput-object v5, v0, v2

    const-string v2, "connection_type"

    aput-object v2, v0, v1

    .line 198
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_4
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "card_reader_id"

    aput-object v0, v3, v1

    .line 196
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 138
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;->build()Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final card_reader_id(I)Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;
    .locals 0

    .line 161
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;->card_reader_id:Ljava/lang/Integer;

    return-object p0
.end method

.method public final charge_cycle_count(I)Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;
    .locals 0

    .line 181
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;->charge_cycle_count:Ljava/lang/Integer;

    return-object p0
.end method

.method public final connection_type(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;
    .locals 1

    const-string v0, "connection_type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;->connection_type:Ljava/lang/String;

    return-object p0
.end method

.method public final firmware_version(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;
    .locals 1

    const-string v0, "firmware_version"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;->firmware_version:Ljava/lang/String;

    return-object p0
.end method

.method public final hardware_serial_number(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;
    .locals 1

    const-string v0, "hardware_serial_number"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;->hardware_serial_number:Ljava/lang/String;

    return-object p0
.end method

.method public final reader_name(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo$Builder;->reader_name:Ljava/lang/String;

    return-object p0
.end method
