.class public final Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TimberLogEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/TimberLogEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/TimberLogEvent;",
        "Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u000c\u001a\u00020\u0002H\u0016J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u000e\u0010\u0006\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007J\u0010\u0010\t\u001a\u00020\u00002\u0008\u0010\t\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\n\u001a\u00020\u00002\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bR\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0008R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/TimberLogEvent;",
        "()V",
        "message",
        "",
        "priority",
        "",
        "Ljava/lang/Integer;",
        "tag",
        "throwable_data",
        "Lcom/squareup/comms/protos/buyer/ThrowableData;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public message:Ljava/lang/String;

.field public priority:Ljava/lang/Integer;

.field public tag:Ljava/lang/String;

.field public throwable_data:Lcom/squareup/comms/protos/buyer/ThrowableData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 103
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/TimberLogEvent;
    .locals 7

    .line 139
    new-instance v6, Lcom/squareup/comms/protos/buyer/TimberLogEvent;

    .line 140
    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;->priority:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 141
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;->tag:Ljava/lang/String;

    .line 142
    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;->message:Ljava/lang/String;

    .line 143
    iget-object v4, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;->throwable_data:Lcom/squareup/comms/protos/buyer/ThrowableData;

    .line 144
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    .line 139
    invoke-direct/range {v0 .. v5}, Lcom/squareup/comms/protos/buyer/TimberLogEvent;-><init>(ILjava/lang/String;Ljava/lang/String;Lcom/squareup/comms/protos/buyer/ThrowableData;Lokio/ByteString;)V

    return-object v6

    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const/4 v0, 0x1

    const-string v2, "priority"

    aput-object v2, v1, v0

    .line 140
    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 103
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;->build()Lcom/squareup/comms/protos/buyer/TimberLogEvent;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final message(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;->message:Ljava/lang/String;

    return-object p0
.end method

.method public final priority(I)Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;
    .locals 0

    .line 117
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;->priority:Ljava/lang/Integer;

    return-object p0
.end method

.method public final tag(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;->tag:Ljava/lang/String;

    return-object p0
.end method

.method public final throwable_data(Lcom/squareup/comms/protos/buyer/ThrowableData;)Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/TimberLogEvent$Builder;->throwable_data:Lcom/squareup/comms/protos/buyer/ThrowableData;

    return-object p0
.end method
