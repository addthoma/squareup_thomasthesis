.class public final Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;
.super Lcom/squareup/wire/AndroidMessage;
.source "LogFirmwareEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/LogFirmwareEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FirmwareEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;,
        Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;",
        "Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLogFirmwareEvent.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LogFirmwareEvent.kt\ncom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent\n*L\n1#1,329:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0006\u0018\u0000 \u00162\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0015\u0016B/\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ8\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\u00082\u0008\u0008\u0002\u0010\n\u001a\u00020\u000bJ\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0096\u0002J\u0008\u0010\u0012\u001a\u00020\u0006H\u0016J\u0008\u0010\u0013\u001a\u00020\u0002H\u0016J\u0008\u0010\u0014\u001a\u00020\u0008H\u0016R\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00020\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;",
        "timestamp",
        "",
        "event",
        "",
        "source",
        "",
        "message",
        "unknownFields",
        "Lokio/ByteString;",
        "(JILjava/lang/String;Ljava/lang/String;Lokio/ByteString;)V",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "newBuilder",
        "toString",
        "Builder",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Companion;


# instance fields
.field public final event:I
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation
.end field

.field public final message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation
.end field

.field public final source:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation
.end field

.field public final timestamp:J
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->Companion:Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Companion;

    .line 277
    new-instance v0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Companion$ADAPTER$1;

    .line 278
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 279
    const-class v2, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 325
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JILjava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    const-string v0, "source"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unknownFields"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    sget-object v0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    iput-wide p1, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->timestamp:J

    iput p3, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->event:I

    iput-object p4, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->source:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->message:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(JILjava/lang/String;Ljava/lang/String;Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p7, p7, 0x10

    if-eqz p7, :cond_0

    .line 181
    sget-object p6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    :cond_0
    move-object v6, p6

    move-object v0, p0

    move-wide v1, p1

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;-><init>(JILjava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;JILjava/lang/String;Ljava/lang/String;Lokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;
    .locals 7

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    .line 226
    iget-wide p1, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->timestamp:J

    :cond_0
    move-wide v1, p1

    and-int/lit8 p1, p7, 0x2

    if-eqz p1, :cond_1

    .line 227
    iget p3, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->event:I

    :cond_1
    move v3, p3

    and-int/lit8 p1, p7, 0x4

    if-eqz p1, :cond_2

    .line 228
    iget-object p4, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->source:Ljava/lang/String;

    :cond_2
    move-object v4, p4

    and-int/lit8 p1, p7, 0x8

    if-eqz p1, :cond_3

    .line 229
    iget-object p5, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->message:Ljava/lang/String;

    :cond_3
    move-object v5, p5

    and-int/lit8 p1, p7, 0x10

    if-eqz p1, :cond_4

    .line 230
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->unknownFields()Lokio/ByteString;

    move-result-object p6

    :cond_4
    move-object v6, p6

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->copy(JILjava/lang/String;Ljava/lang/String;Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(JILjava/lang/String;Ljava/lang/String;Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;
    .locals 8

    const-string v0, "source"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unknownFields"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 231
    new-instance v0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;

    move-object v1, v0

    move-wide v2, p1

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;-><init>(JILjava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    .line 194
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    return v1

    .line 195
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 200
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v3, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->timestamp:J

    iget-wide v5, p1, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->timestamp:J

    cmp-long v0, v3, v5

    if-nez v0, :cond_2

    iget v0, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->event:I

    iget v3, p1, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->event:I

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->source:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->source:Ljava/lang/String;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->message:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->message:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 204
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_0

    .line 206
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 207
    iget-wide v1, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->timestamp:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 208
    iget v1, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->event:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 209
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->source:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 210
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->message:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 211
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_0
    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;
    .locals 3

    .line 184
    new-instance v0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;-><init>()V

    .line 185
    iget-wide v1, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->timestamp:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;->timestamp:Ljava/lang/Long;

    .line 186
    iget v1, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->event:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;->event:Ljava/lang/Integer;

    .line 187
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->source:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;->source:Ljava/lang/String;

    .line 188
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;->message:Ljava/lang/String;

    .line 189
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 152
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->newBuilder()Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .line 217
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 218
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "timestamp="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->timestamp:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 219
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "event="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->event:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 220
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "source="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->source:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 221
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "message="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/LogFirmwareEvent$FirmwareEvent;->message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 222
    move-object v3, v0

    check-cast v3, Ljava/lang/Iterable;

    const-string v0, "FirmwareEvent{"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const-string v0, ", "

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const-string v0, "}"

    move-object v6, v0

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
