.class public final Lcom/squareup/comms/protos/buyer/BranSlideshowProgress$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BranSlideshowProgress.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/BranSlideshowProgress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/BranSlideshowProgress;",
        "Lcom/squareup/comms/protos/buyer/BranSlideshowProgress$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0008\u001a\u00020\u0002H\u0016J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u000e\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\u0005R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/BranSlideshowProgress$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/BranSlideshowProgress;",
        "()V",
        "currentImageIndex",
        "",
        "Ljava/lang/Integer;",
        "totalImageCount",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public currentImageIndex:Ljava/lang/Integer;

.field public totalImageCount:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 82
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/BranSlideshowProgress;
    .locals 6

    .line 99
    new-instance v0, Lcom/squareup/comms/protos/buyer/BranSlideshowProgress;

    .line 100
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/BranSlideshowProgress$Builder;->currentImageIndex:Ljava/lang/Integer;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 102
    iget-object v5, p0, Lcom/squareup/comms/protos/buyer/BranSlideshowProgress$Builder;->totalImageCount:Ljava/lang/Integer;

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 104
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranSlideshowProgress$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    .line 99
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/comms/protos/buyer/BranSlideshowProgress;-><init>(IILokio/ByteString;)V

    return-object v0

    :cond_0
    new-array v0, v4, [Ljava/lang/Object;

    aput-object v5, v0, v3

    const-string v1, "totalImageCount"

    aput-object v1, v0, v2

    .line 102
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v0, v4, [Ljava/lang/Object;

    aput-object v1, v0, v3

    const-string v1, "currentImageIndex"

    aput-object v1, v0, v2

    .line 100
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 82
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranSlideshowProgress$Builder;->build()Lcom/squareup/comms/protos/buyer/BranSlideshowProgress;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final currentImageIndex(I)Lcom/squareup/comms/protos/buyer/BranSlideshowProgress$Builder;
    .locals 0

    .line 90
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranSlideshowProgress$Builder;->currentImageIndex:Ljava/lang/Integer;

    return-object p0
.end method

.method public final totalImageCount(I)Lcom/squareup/comms/protos/buyer/BranSlideshowProgress$Builder;
    .locals 0

    .line 95
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranSlideshowProgress$Builder;->totalImageCount:Ljava/lang/Integer;

    return-object p0
.end method
