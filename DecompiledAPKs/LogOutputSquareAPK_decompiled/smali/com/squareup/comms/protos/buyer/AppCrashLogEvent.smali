.class public final Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;
.super Lcom/squareup/wire/AndroidMessage;
.source "AppCrashLogEvent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/buyer/AppCrashLogEvent$Builder;,
        Lcom/squareup/comms/protos/buyer/AppCrashLogEvent$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;",
        "Lcom/squareup/comms/protos/buyer/AppCrashLogEvent$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAppCrashLogEvent.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AppCrashLogEvent.kt\ncom/squareup/comms/protos/buyer/AppCrashLogEvent\n*L\n1#1,178:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0018\u0000 \u00152\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0014\u0015B+\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ0\u0010\u000b\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u00042\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tJ\u0013\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0096\u0002J\u0008\u0010\u0010\u001a\u00020\u0011H\u0016J\u0008\u0010\u0012\u001a\u00020\u0002H\u0016J\u0008\u0010\u0013\u001a\u00020\u0006H\u0016R\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/buyer/AppCrashLogEvent$Builder;",
        "throwable",
        "Lcom/squareup/comms/protos/buyer/ThrowableData;",
        "crashnado_mini_dump",
        "",
        "id",
        "unknownFields",
        "Lokio/ByteString;",
        "(Lcom/squareup/comms/protos/buyer/ThrowableData;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "newBuilder",
        "toString",
        "Builder",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/buyer/AppCrashLogEvent$Companion;


# instance fields
.field public final crashnado_mini_dump:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation
.end field

.field public final throwable:Lcom/squareup/comms/protos/buyer/ThrowableData;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.buyer.ThrowableData#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->Companion:Lcom/squareup/comms/protos/buyer/AppCrashLogEvent$Companion;

    .line 131
    new-instance v0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent$Companion$ADAPTER$1;

    .line 132
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 133
    const-class v2, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 175
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/comms/protos/buyer/ThrowableData;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    const-string v0, "throwable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "id"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unknownFields"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    sget-object v0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->throwable:Lcom/squareup/comms/protos/buyer/ThrowableData;

    iput-object p2, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->crashnado_mini_dump:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->id:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/comms/protos/buyer/ThrowableData;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    const/4 p2, 0x0

    .line 38
    check-cast p2, Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    .line 46
    sget-object p4, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;-><init>(Lcom/squareup/comms/protos/buyer/ThrowableData;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;Lcom/squareup/comms/protos/buyer/ThrowableData;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    .line 87
    iget-object p1, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->throwable:Lcom/squareup/comms/protos/buyer/ThrowableData;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    .line 88
    iget-object p2, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->crashnado_mini_dump:Ljava/lang/String;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    .line 89
    iget-object p3, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->id:Ljava/lang/String;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    .line 90
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->unknownFields()Lokio/ByteString;

    move-result-object p4

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->copy(Lcom/squareup/comms/protos/buyer/ThrowableData;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(Lcom/squareup/comms/protos/buyer/ThrowableData;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;
    .locals 1

    const-string v0, "throwable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "id"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "unknownFields"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    new-instance v0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;-><init>(Lcom/squareup/comms/protos/buyer/ThrowableData;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 58
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    return v1

    .line 59
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 63
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->throwable:Lcom/squareup/comms/protos/buyer/ThrowableData;

    iget-object v3, p1, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->throwable:Lcom/squareup/comms/protos/buyer/ThrowableData;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->crashnado_mini_dump:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->crashnado_mini_dump:Ljava/lang/String;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->id:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 2

    .line 67
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_1

    .line 69
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 70
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->throwable:Lcom/squareup/comms/protos/buyer/ThrowableData;

    invoke-virtual {v1}, Lcom/squareup/comms/protos/buyer/ThrowableData;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 71
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->crashnado_mini_dump:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 72
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->id:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/buyer/AppCrashLogEvent$Builder;
    .locals 2

    .line 49
    new-instance v0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent$Builder;-><init>()V

    .line 50
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->throwable:Lcom/squareup/comms/protos/buyer/ThrowableData;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent$Builder;->throwable:Lcom/squareup/comms/protos/buyer/ThrowableData;

    .line 51
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->crashnado_mini_dump:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent$Builder;->crashnado_mini_dump:Ljava/lang/String;

    .line 52
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent$Builder;->id:Ljava/lang/String;

    .line 53
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->newBuilder()Lcom/squareup/comms/protos/buyer/AppCrashLogEvent$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 80
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "throwable="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->throwable:Lcom/squareup/comms/protos/buyer/ThrowableData;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 81
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->crashnado_mini_dump:Ljava/lang/String;

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "crashnado_mini_dump="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->crashnado_mini_dump:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 82
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 83
    move-object v3, v0

    check-cast v3, Ljava/lang/Iterable;

    const-string v0, "AppCrashLogEvent{"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const-string v0, ", "

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const-string v0, "}"

    move-object v6, v0

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
