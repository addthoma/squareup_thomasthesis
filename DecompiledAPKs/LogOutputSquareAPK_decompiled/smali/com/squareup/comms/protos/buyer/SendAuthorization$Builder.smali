.class public final Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SendAuthorization.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/SendAuthorization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/SendAuthorization;",
        "Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u000c\u001a\u00020\u0002H\u0016J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u0006\u001a\u00020\u00002\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u0008\u001a\u00020\u00002\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tJ\u0010\u0010\n\u001a\u00020\u00002\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bR\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/SendAuthorization;",
        "()V",
        "buyer_card_info",
        "Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;",
        "card_reader_info",
        "Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;",
        "encrypted_reader_data",
        "Lokio/ByteString;",
        "method",
        "Lcom/squareup/comms/protos/buyer/EntryMethod;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public buyer_card_info:Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

.field public card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

.field public encrypted_reader_data:Lokio/ByteString;

.field public method:Lcom/squareup/comms/protos/buyer/EntryMethod;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 110
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/SendAuthorization;
    .locals 7

    .line 150
    new-instance v6, Lcom/squareup/comms/protos/buyer/SendAuthorization;

    .line 151
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;->encrypted_reader_data:Lokio/ByteString;

    .line 152
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;->method:Lcom/squareup/comms/protos/buyer/EntryMethod;

    .line 153
    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    .line 154
    iget-object v4, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;->buyer_card_info:Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

    .line 155
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    .line 150
    invoke-direct/range {v0 .. v5}, Lcom/squareup/comms/protos/buyer/SendAuthorization;-><init>(Lokio/ByteString;Lcom/squareup/comms/protos/buyer/EntryMethod;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 110
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;->build()Lcom/squareup/comms/protos/buyer/SendAuthorization;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final buyer_card_info(Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;)Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;->buyer_card_info:Lcom/squareup/comms/protos/buyer/SendAuthorization$BuyerCardInfo;

    return-object p0
.end method

.method public final card_reader_info(Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;)Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    return-object p0
.end method

.method public final encrypted_reader_data(Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;->encrypted_reader_data:Lokio/ByteString;

    return-object p0
.end method

.method public final method(Lcom/squareup/comms/protos/buyer/EntryMethod;)Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/SendAuthorization$Builder;->method:Lcom/squareup/comms/protos/buyer/EntryMethod;

    return-object p0
.end method
