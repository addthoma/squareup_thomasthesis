.class public final Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ThrowableData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/ThrowableData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/ThrowableData;",
        "Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u000b\u001a\u00020\u0002H\u0016J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0002J\u0010\u0010\u0005\u001a\u00020\u00002\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u0007\u001a\u00020\u00002\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0006J\u0014\u0010\u0008\u001a\u00020\u00002\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tR\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00028\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/ThrowableData;",
        "()V",
        "cause",
        "message",
        "",
        "original_class_name",
        "stack_trace",
        "",
        "Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public cause:Lcom/squareup/comms/protos/buyer/ThrowableData;

.field public message:Ljava/lang/String;

.field public original_class_name:Ljava/lang/String;

.field public stack_trace:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 102
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 113
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;->stack_trace:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/ThrowableData;
    .locals 7

    .line 136
    new-instance v6, Lcom/squareup/comms/protos/buyer/ThrowableData;

    .line 137
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;->cause:Lcom/squareup/comms/protos/buyer/ThrowableData;

    .line 138
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;->original_class_name:Ljava/lang/String;

    .line 139
    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;->message:Ljava/lang/String;

    .line 140
    iget-object v4, p0, Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;->stack_trace:Ljava/util/List;

    .line 141
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    .line 136
    invoke-direct/range {v0 .. v5}, Lcom/squareup/comms/protos/buyer/ThrowableData;-><init>(Lcom/squareup/comms/protos/buyer/ThrowableData;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 102
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;->build()Lcom/squareup/comms/protos/buyer/ThrowableData;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final cause(Lcom/squareup/comms/protos/buyer/ThrowableData;)Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;->cause:Lcom/squareup/comms/protos/buyer/ThrowableData;

    return-object p0
.end method

.method public final message(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;->message:Ljava/lang/String;

    return-object p0
.end method

.method public final original_class_name(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;->original_class_name:Ljava/lang/String;

    return-object p0
.end method

.method public final stack_trace(Ljava/util/List;)Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;",
            ">;)",
            "Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;"
        }
    .end annotation

    const-string v0, "stack_trace"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 132
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;->stack_trace:Ljava/util/List;

    return-object p0
.end method
