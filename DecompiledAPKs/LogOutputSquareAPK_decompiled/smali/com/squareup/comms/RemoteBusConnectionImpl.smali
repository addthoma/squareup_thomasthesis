.class final Lcom/squareup/comms/RemoteBusConnectionImpl;
.super Ljava/lang/Object;
.source "RemoteBusConnectionImpl.java"

# interfaces
.implements Lcom/squareup/comms/RemoteBusConnection;


# instance fields
.field private final callbackExecutor:Ljava/util/concurrent/Executor;

.field private disconnected:Z

.field private final poster:Lcom/squareup/comms/MessagePoster;

.field private final subject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/squareup/wire/Message;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/comms/MessagePoster;Ljava/util/concurrent/Executor;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/comms/RemoteBusConnectionImpl;->poster:Lcom/squareup/comms/MessagePoster;

    .line 17
    iput-object p2, p0, Lcom/squareup/comms/RemoteBusConnectionImpl;->callbackExecutor:Ljava/util/concurrent/Executor;

    .line 18
    invoke-static {}, Lrx/subjects/PublishSubject;->create()Lrx/subjects/PublishSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/RemoteBusConnectionImpl;->subject:Lrx/subjects/PublishSubject;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onNext$0$RemoteBusConnectionImpl(Lcom/squareup/wire/Message;)V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusConnectionImpl;->subject:Lrx/subjects/PublishSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public observable()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/wire/Message;",
            ">;"
        }
    .end annotation

    .line 44
    iget-boolean v0, p0, Lcom/squareup/comms/RemoteBusConnectionImpl;->disconnected:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Returning empty observable for disconnected bus"

    .line 45
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    invoke-static {}, Lrx/Observable;->empty()Lrx/Observable;

    move-result-object v0

    return-object v0

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusConnectionImpl;->subject:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method onDisconnected()V
    .locals 3

    .line 53
    iget-boolean v0, p0, Lcom/squareup/comms/RemoteBusConnectionImpl;->disconnected:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 54
    iput-boolean v0, p0, Lcom/squareup/comms/RemoteBusConnectionImpl;->disconnected:Z

    .line 55
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusConnectionImpl;->callbackExecutor:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/squareup/comms/RemoteBusConnectionImpl;->subject:Lrx/subjects/PublishSubject;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/comms/-$$Lambda$MnvVk4iAp1b5s_K-YhUt5BrGnHg;

    invoke-direct {v2, v1}, Lcom/squareup/comms/-$$Lambda$MnvVk4iAp1b5s_K-YhUt5BrGnHg;-><init>(Lrx/subjects/PublishSubject;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void

    .line 53
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method onNext(Lcom/squareup/wire/Message;)V
    .locals 2

    .line 59
    iget-boolean v0, p0, Lcom/squareup/comms/RemoteBusConnectionImpl;->disconnected:Z

    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusConnectionImpl;->callbackExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/comms/-$$Lambda$RemoteBusConnectionImpl$4FAGR6BcVKVt3ZkYUCRTTJw9PeI;

    invoke-direct {v1, p0, p1}, Lcom/squareup/comms/-$$Lambda$RemoteBusConnectionImpl$4FAGR6BcVKVt3ZkYUCRTTJw9PeI;-><init>(Lcom/squareup/comms/RemoteBusConnectionImpl;Lcom/squareup/wire/Message;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void

    .line 59
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Not connected."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public post(Lcom/squareup/wire/Message;)V
    .locals 2

    .line 27
    iget-boolean v0, p0, Lcom/squareup/comms/RemoteBusConnectionImpl;->disconnected:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "Ignoring post while disconnected: %s"

    .line 28
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusConnectionImpl;->poster:Lcom/squareup/comms/MessagePoster;

    invoke-interface {v0, p1}, Lcom/squareup/comms/MessagePoster;->post(Lcom/squareup/wire/Message;)V

    return-void
.end method
