.class public Lcom/squareup/comms/buyerapi/BuyerThrowable;
.super Ljava/lang/Throwable;
.source "BuyerThrowable.java"


# direct methods
.method constructor <init>(Lcom/squareup/comms/protos/buyer/ThrowableData;)V
    .locals 3

    .line 11
    invoke-static {p1}, Lcom/squareup/comms/buyerapi/BuyerThrowable;->convertMessage(Lcom/squareup/comms/protos/buyer/ThrowableData;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/comms/protos/buyer/ThrowableData;->cause:Lcom/squareup/comms/protos/buyer/ThrowableData;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/squareup/comms/buyerapi/BuyerThrowable;

    iget-object v2, p1, Lcom/squareup/comms/protos/buyer/ThrowableData;->cause:Lcom/squareup/comms/protos/buyer/ThrowableData;

    invoke-direct {v1, v2}, Lcom/squareup/comms/buyerapi/BuyerThrowable;-><init>(Lcom/squareup/comms/protos/buyer/ThrowableData;)V

    :goto_0
    invoke-direct {p0, v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 12
    iget-object p1, p1, Lcom/squareup/comms/protos/buyer/ThrowableData;->stack_trace:Ljava/util/List;

    invoke-static {p1}, Lcom/squareup/comms/buyerapi/BuyerThrowable;->asStackTrace(Ljava/util/List;)[Ljava/lang/StackTraceElement;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/comms/buyerapi/BuyerThrowable;->setStackTrace([Ljava/lang/StackTraceElement;)V

    return-void
.end method

.method private static asStackTrace(Ljava/util/List;)[Ljava/lang/StackTraceElement;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;",
            ">;)[",
            "Ljava/lang/StackTraceElement;"
        }
    .end annotation

    .line 25
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/StackTraceElement;

    .line 28
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;

    .line 29
    new-instance v3, Ljava/lang/StackTraceElement;

    iget-object v4, v2, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;->declaring_class:Ljava/lang/String;

    iget-object v5, v2, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;->method_name:Ljava/lang/String;

    iget-object v6, v2, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;->file_name:Ljava/lang/String;

    iget-object v2, v2, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;->line_number:Ljava/lang/Integer;

    .line 30
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v3, v4, v5, v6, v2}, Ljava/lang/StackTraceElement;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private static convertMessage(Lcom/squareup/comms/protos/buyer/ThrowableData;)Ljava/lang/String;
    .locals 3

    .line 16
    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/ThrowableData;->message:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/squareup/comms/protos/buyer/ThrowableData;->original_class_name:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/ThrowableData;->original_class_name:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object p0, p0, Lcom/squareup/comms/protos/buyer/ThrowableData;->message:Ljava/lang/String;

    aput-object p0, v0, v1

    const-string p0, "[%s] %s"

    .line 17
    invoke-static {p0, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method


# virtual methods
.method public fillInStackTrace()Ljava/lang/Throwable;
    .locals 0

    return-object p0
.end method
