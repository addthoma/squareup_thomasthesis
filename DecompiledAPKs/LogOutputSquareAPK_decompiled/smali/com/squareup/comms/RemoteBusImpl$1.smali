.class Lcom/squareup/comms/RemoteBusImpl$1;
.super Ljava/lang/Object;
.source "RemoteBusImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/comms/RemoteBusImpl;->stop()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/comms/RemoteBusImpl;


# direct methods
.method constructor <init>(Lcom/squareup/comms/RemoteBusImpl;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/squareup/comms/RemoteBusImpl$1;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "RemoteBusImpl stopping"

    .line 55
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$1;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {v0}, Lcom/squareup/comms/RemoteBusImpl;->access$100(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/HealthChecker;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/comms/HealthChecker;->stop()V

    return-void
.end method
