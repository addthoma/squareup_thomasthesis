.class public final enum Lcom/mattprecious/telescope/ScreenshotMode;
.super Ljava/lang/Enum;
.source "ScreenshotMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mattprecious/telescope/ScreenshotMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mattprecious/telescope/ScreenshotMode;

.field public static final enum CANVAS:Lcom/mattprecious/telescope/ScreenshotMode;

.field public static final enum NONE:Lcom/mattprecious/telescope/ScreenshotMode;

.field public static final enum SYSTEM:Lcom/mattprecious/telescope/ScreenshotMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 21
    new-instance v0, Lcom/mattprecious/telescope/ScreenshotMode;

    const/4 v1, 0x0

    const-string v2, "SYSTEM"

    invoke-direct {v0, v2, v1}, Lcom/mattprecious/telescope/ScreenshotMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mattprecious/telescope/ScreenshotMode;->SYSTEM:Lcom/mattprecious/telescope/ScreenshotMode;

    .line 32
    new-instance v0, Lcom/mattprecious/telescope/ScreenshotMode;

    const/4 v2, 0x1

    const-string v3, "CANVAS"

    invoke-direct {v0, v3, v2}, Lcom/mattprecious/telescope/ScreenshotMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mattprecious/telescope/ScreenshotMode;->CANVAS:Lcom/mattprecious/telescope/ScreenshotMode;

    .line 34
    new-instance v0, Lcom/mattprecious/telescope/ScreenshotMode;

    const/4 v3, 0x2

    const-string v4, "NONE"

    invoke-direct {v0, v4, v3}, Lcom/mattprecious/telescope/ScreenshotMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mattprecious/telescope/ScreenshotMode;->NONE:Lcom/mattprecious/telescope/ScreenshotMode;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mattprecious/telescope/ScreenshotMode;

    .line 4
    sget-object v4, Lcom/mattprecious/telescope/ScreenshotMode;->SYSTEM:Lcom/mattprecious/telescope/ScreenshotMode;

    aput-object v4, v0, v1

    sget-object v1, Lcom/mattprecious/telescope/ScreenshotMode;->CANVAS:Lcom/mattprecious/telescope/ScreenshotMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mattprecious/telescope/ScreenshotMode;->NONE:Lcom/mattprecious/telescope/ScreenshotMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/mattprecious/telescope/ScreenshotMode;->$VALUES:[Lcom/mattprecious/telescope/ScreenshotMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mattprecious/telescope/ScreenshotMode;
    .locals 1

    .line 4
    const-class v0, Lcom/mattprecious/telescope/ScreenshotMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mattprecious/telescope/ScreenshotMode;

    return-object p0
.end method

.method public static values()[Lcom/mattprecious/telescope/ScreenshotMode;
    .locals 1

    .line 4
    sget-object v0, Lcom/mattprecious/telescope/ScreenshotMode;->$VALUES:[Lcom/mattprecious/telescope/ScreenshotMode;

    invoke-virtual {v0}, [Lcom/mattprecious/telescope/ScreenshotMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mattprecious/telescope/ScreenshotMode;

    return-object v0
.end method
