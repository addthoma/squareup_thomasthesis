.class Lcom/mattprecious/telescope/TelescopeLayout$4;
.super Landroid/content/BroadcastReceiver;
.source "TelescopeLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mattprecious/telescope/TelescopeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mattprecious/telescope/TelescopeLayout;


# direct methods
.method constructor <init>(Lcom/mattprecious/telescope/TelescopeLayout;)V
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout$4;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .line 191
    iget-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout$4;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    invoke-static {p1}, Lcom/mattprecious/telescope/TelescopeLayout;->access$300(Lcom/mattprecious/telescope/TelescopeLayout;)V

    const-string p1, "code"

    const/4 v0, 0x0

    .line 193
    invoke-virtual {p2, p1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    const-string v0, "data"

    .line 195
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 197
    iget-object v1, p0, Lcom/mattprecious/telescope/TelescopeLayout$4;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    .line 198
    invoke-static {v1}, Lcom/mattprecious/telescope/TelescopeLayout;->access$400(Lcom/mattprecious/telescope/TelescopeLayout;)Landroid/media/projection/MediaProjectionManager;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/media/projection/MediaProjectionManager;->getMediaProjection(ILandroid/content/Intent;)Landroid/media/projection/MediaProjection;

    move-result-object p1

    if-nez p1, :cond_0

    .line 200
    iget-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout$4;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    invoke-static {p1}, Lcom/mattprecious/telescope/TelescopeLayout;->access$500(Lcom/mattprecious/telescope/TelescopeLayout;)V

    return-void

    :cond_0
    const/4 v0, 0x1

    const-string v1, "prompt-shown"

    .line 204
    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 206
    iget-object p2, p0, Lcom/mattprecious/telescope/TelescopeLayout$4;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    new-instance v0, Lcom/mattprecious/telescope/TelescopeLayout$4$1;

    invoke-direct {v0, p0, p1}, Lcom/mattprecious/telescope/TelescopeLayout$4$1;-><init>(Lcom/mattprecious/telescope/TelescopeLayout$4;Landroid/media/projection/MediaProjection;)V

    const-wide/16 v1, 0x1f4

    invoke-virtual {p2, v0, v1, v2}, Lcom/mattprecious/telescope/TelescopeLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 212
    :cond_1
    iget-object p2, p0, Lcom/mattprecious/telescope/TelescopeLayout$4;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    invoke-static {p2, p1}, Lcom/mattprecious/telescope/TelescopeLayout;->access$600(Lcom/mattprecious/telescope/TelescopeLayout;Landroid/media/projection/MediaProjection;)V

    :goto_0
    return-void
.end method
