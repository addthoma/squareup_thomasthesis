.class public Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;
.super Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;
.source "DoubleHeaderDecoration.java"


# instance fields
.field private mAdapter:Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;

.field private mHeaderCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field private mRenderInline:Z

.field private mSubHeaderCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;)V
    .locals 1

    const/4 v0, 0x0

    .line 42
    invoke-direct {p0, p1, v0}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;-><init>(Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;Z)V

    return-void
.end method

.method public constructor <init>(Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;Z)V
    .locals 0

    .line 48
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;-><init>()V

    .line 49
    iput-object p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;

    .line 51
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mSubHeaderCache:Ljava/util/Map;

    .line 52
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mHeaderCache:Ljava/util/Map;

    .line 53
    iput-boolean p2, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mRenderInline:Z

    return-void
.end method

.method private getAnimatedTop(Landroid/view/View;)I
    .locals 1

    .line 328
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result p1

    float-to-int p1, p1

    add-int/2addr v0, p1

    return v0
.end method

.method private getHeader(Landroidx/recyclerview/widget/RecyclerView;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 5

    .line 134
    iget-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;

    invoke-interface {v0, p2}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;->getHeaderId(I)J

    move-result-wide v0

    .line 136
    iget-object v2, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mHeaderCache:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 137
    iget-object p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mHeaderCache:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1

    .line 139
    :cond_0
    iget-object v2, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;

    invoke-interface {v2, p1}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;->onCreateHeaderHolder(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object v2

    .line 140
    iget-object v3, v2, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 143
    iget-object v4, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;

    invoke-interface {v4, v2, p2}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;->onBindHeaderHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V

    .line 144
    invoke-direct {p0, p1, v3}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->measureView(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;)V

    .line 145
    iget-object p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mHeaderCache:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {p1, p2, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v2
.end method

.method private getHeaderTop(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;Landroid/view/View;Landroid/view/View;II)I
    .locals 7

    .line 282
    invoke-direct {p0, p2}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getAnimatedTop(Landroid/view/View;)I

    move-result p2

    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result v0

    sub-int/2addr p2, v0

    invoke-direct {p0, p4}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getSubHeaderHeightForLayout(Landroid/view/View;)I

    move-result v0

    sub-int/2addr p2, v0

    .line 283
    invoke-direct {p0, p6, p1}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->isFirstValidChild(ILandroidx/recyclerview/widget/RecyclerView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 284
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getChildCount()I

    move-result v0

    .line 285
    iget-object v1, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;

    invoke-interface {v1, p5}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;->getHeaderId(I)J

    move-result-wide v1

    :cond_0
    add-int/lit8 p6, p6, 0x1

    if-ge p6, v0, :cond_1

    .line 289
    invoke-virtual {p1, p6}, Landroidx/recyclerview/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object p5

    .line 290
    invoke-virtual {p1, p5}, Landroidx/recyclerview/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 292
    iget-object v4, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;

    invoke-interface {v4, v3}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;->getHeaderId(I)J

    move-result-wide v4

    cmp-long v6, v4, v1

    if-eqz v6, :cond_0

    .line 294
    invoke-virtual {p3}, Landroid/view/View;->getHeight()I

    move-result p3

    invoke-direct {p0, p1, v3}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getHeader(Landroidx/recyclerview/widget/RecyclerView;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p1

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    add-int/2addr p3, p1

    .line 295
    invoke-direct {p0, p5}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getAnimatedTop(Landroid/view/View;)I

    move-result p1

    sub-int/2addr p1, p3

    invoke-direct {p0, p4}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getSubHeaderHeightForLayout(Landroid/view/View;)I

    move-result p3

    sub-int/2addr p1, p3

    if-gez p1, :cond_1

    return p1

    :cond_1
    const/4 p1, 0x0

    .line 306
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    :cond_2
    return p2
.end method

.method private getSubHeader(Landroidx/recyclerview/widget/RecyclerView;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 5

    .line 82
    iget-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;

    invoke-interface {v0, p2}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;->getSubHeaderId(I)J

    move-result-wide v0

    .line 84
    iget-object v2, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mSubHeaderCache:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 85
    iget-object p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mSubHeaderCache:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1

    .line 87
    :cond_0
    iget-object v2, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;

    invoke-interface {v2, p1}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;->onCreateSubHeaderHolder(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object v2

    .line 88
    iget-object v3, v2, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 91
    iget-object v4, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;

    invoke-interface {v4, v2, p2}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;->onBindSubHeaderHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V

    .line 92
    invoke-direct {p0, p1, v3}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->measureView(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;)V

    .line 93
    iget-object p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mSubHeaderCache:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {p1, p2, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v2
.end method

.method private getSubHeaderHeightForLayout(Landroid/view/View;)I
    .locals 1

    .line 332
    iget-boolean v0, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mRenderInline:Z

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    :goto_0
    return p1
.end method

.method private getSubHeaderTop(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;Landroid/view/View;Landroid/view/View;II)I
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    move/from16 v3, p5

    move-object/from16 v4, p2

    move/from16 v5, p6

    .line 247
    invoke-direct {v0, v4}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getAnimatedTop(Landroid/view/View;)I

    move-result v4

    invoke-direct {v0, v2}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getSubHeaderHeightForLayout(Landroid/view/View;)I

    move-result v6

    sub-int/2addr v4, v6

    .line 248
    invoke-direct {v0, v5, v1}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->isFirstValidChild(ILandroidx/recyclerview/widget/RecyclerView;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 249
    invoke-virtual/range {p1 .. p1}, Landroidx/recyclerview/widget/RecyclerView;->getChildCount()I

    move-result v6

    .line 250
    iget-object v7, v0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;

    invoke-interface {v7, v3}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;->getHeaderId(I)J

    move-result-wide v7

    .line 251
    iget-object v9, v0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;

    invoke-interface {v9, v3}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;->getSubHeaderId(I)J

    move-result-wide v9

    add-int/lit8 v3, v5, 0x1

    :goto_0
    if-ge v3, v6, :cond_2

    .line 255
    invoke-virtual {v1, v3}, Landroidx/recyclerview/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 256
    invoke-virtual {v1, v5}, Landroidx/recyclerview/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v11

    const/4 v12, -0x1

    if-eq v11, v12, :cond_1

    .line 258
    iget-object v12, v0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;

    invoke-interface {v12, v11}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;->getHeaderId(I)J

    move-result-wide v12

    .line 259
    iget-object v14, v0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;

    invoke-interface {v14, v11}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;->getSubHeaderId(I)J

    move-result-wide v14

    cmp-long v16, v14, v9

    if-eqz v16, :cond_1

    .line 262
    invoke-direct {v0, v2}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getSubHeaderHeightForLayout(Landroid/view/View;)I

    move-result v2

    invoke-direct {v0, v1, v11}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getSubHeader(Landroidx/recyclerview/widget/RecyclerView;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object v3

    iget-object v3, v3, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    cmp-long v3, v12, v7

    if-eqz v3, :cond_0

    .line 264
    invoke-direct {v0, v1, v11}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getHeader(Landroidx/recyclerview/widget/RecyclerView;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object v1

    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/2addr v2, v1

    .line 267
    :cond_0
    invoke-direct {v0, v5}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getAnimatedTop(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v1, v2

    .line 268
    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getHeight()I

    move-result v2

    if-ge v1, v2, :cond_2

    return v1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 278
    :cond_2
    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    return v1
.end method

.method private hasHeader(I)Z
    .locals 6

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v1, p1, -0x1

    .line 179
    iget-object v2, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;

    invoke-interface {v2, p1}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;->getHeaderId(I)J

    move-result-wide v2

    iget-object p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;

    invoke-interface {p1, v1}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;->getHeaderId(I)J

    move-result-wide v4

    cmp-long p1, v2, v4

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private hasSubHeader(I)Z
    .locals 6

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v1, p1, -0x1

    .line 170
    iget-object v2, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;

    invoke-interface {v2, p1}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;->getSubHeaderId(I)J

    move-result-wide v2

    iget-object p1, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;

    invoke-interface {p1, v1}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderAdapter;->getSubHeaderId(I)J

    move-result-wide v4

    cmp-long p1, v2, v4

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isFirstValidChild(ILandroidx/recyclerview/widget/RecyclerView;)Z
    .locals 5

    const/4 v0, 0x1

    sub-int/2addr p1, v0

    :goto_0
    const/4 v1, 0x0

    if-ltz p1, :cond_2

    .line 315
    invoke-virtual {p2, p1}, Landroidx/recyclerview/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 316
    invoke-virtual {p2, v2}, Landroidx/recyclerview/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 317
    invoke-direct {p0, v2}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getAnimatedTop(Landroid/view/View;)I

    move-result v3

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    neg-int v2, v2

    if-le v3, v2, :cond_0

    const/4 v2, 0x1

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_1

    const/4 v0, 0x0

    goto :goto_2

    :cond_1
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    :cond_2
    :goto_2
    return v0
.end method

.method private measureView(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;)V
    .locals 5

    .line 152
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getWidth()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 153
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getHeight()I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 156
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 155
    invoke-static {v0, v3, v4}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v0

    .line 158
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getPaddingTop()I

    move-result v3

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getPaddingBottom()I

    move-result p1

    add-int/2addr v3, p1

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    iget p1, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 157
    invoke-static {v1, v3, p1}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result p1

    .line 160
    invoke-virtual {p2, v0, p1}, Landroid/view/View;->measure(II)V

    .line 161
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p2, v2, v2, p1, v0}, Landroid/view/View;->layout(IIII)V

    return-void
.end method


# virtual methods
.method public clearDoubleHeaderCache()V
    .locals 0

    .line 61
    invoke-virtual {p0}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->clearSubHeaderCache()V

    .line 62
    invoke-virtual {p0}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->clearHeaderCache()V

    return-void
.end method

.method public clearHeaderCache()V
    .locals 1

    .line 78
    iget-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mHeaderCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public clearSubHeaderCache()V
    .locals 1

    .line 70
    iget-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mSubHeaderCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public findHeaderViewUnder(FF)Landroid/view/View;
    .locals 5

    .line 100
    iget-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mHeaderCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    .line 101
    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 102
    invoke-static {v1}, Landroidx/core/view/ViewCompat;->getTranslationX(Landroid/view/View;)F

    move-result v2

    .line 103
    invoke-static {v1}, Landroidx/core/view/ViewCompat;->getTranslationY(Landroid/view/View;)F

    move-result v3

    .line 105
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v4, v2

    cmpl-float v4, p1, v4

    if-ltz v4, :cond_0

    .line 106
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v4, v2

    cmpg-float v2, p1, v4

    if-gtz v2, :cond_0

    .line 107
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v3

    cmpl-float v2, p2, v2

    if-ltz v2, :cond_0

    .line 108
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v3

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public findSubHeaderViewUnder(FF)Landroid/view/View;
    .locals 5

    .line 117
    iget-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->mSubHeaderCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    .line 118
    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 119
    invoke-static {v1}, Landroidx/core/view/ViewCompat;->getTranslationX(Landroid/view/View;)F

    move-result v2

    .line 120
    invoke-static {v1}, Landroidx/core/view/ViewCompat;->getTranslationY(Landroid/view/View;)F

    move-result v3

    .line 122
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v4, v2

    cmpl-float v4, p1, v4

    if-ltz v4, :cond_0

    .line 123
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v4, v2

    cmpg-float v2, p1, v4

    if-gtz v2, :cond_0

    .line 124
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v3

    cmpl-float v2, p2, v2

    if-ltz v2, :cond_0

    .line 125
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v3

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V
    .locals 1

    .line 187
    invoke-virtual {p3, p2}, Landroidx/recyclerview/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result p2

    const/4 p4, 0x0

    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 191
    invoke-direct {p0, p2}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->hasSubHeader(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 192
    invoke-direct {p0, p2}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->hasHeader(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    invoke-direct {p0, p3, p2}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getHeader(Landroidx/recyclerview/widget/RecyclerView;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object v0

    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 194
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v0, p4

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 197
    :goto_0
    invoke-direct {p0, p3, p2}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getSubHeader(Landroidx/recyclerview/widget/RecyclerView;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p2

    iget-object p2, p2, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 198
    invoke-direct {p0, p2}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getSubHeaderHeightForLayout(Landroid/view/View;)I

    move-result p2

    add-int/2addr p2, v0

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    .line 201
    :goto_1
    invoke-virtual {p1, p4, p2, p4, p4}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

.method public onDrawOver(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V
    .locals 18

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    .line 209
    invoke-virtual/range {p2 .. p2}, Landroidx/recyclerview/widget/RecyclerView;->getChildCount()I

    move-result v10

    const/4 v12, 0x0

    const/4 v13, 0x0

    :goto_0
    if-ge v12, v10, :cond_5

    .line 213
    invoke-virtual {v9, v12}, Landroidx/recyclerview/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v14

    .line 214
    invoke-direct {v7, v14}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getAnimatedTop(Landroid/view/View;)I

    move-result v0

    invoke-virtual {v14}, Landroid/view/View;->getHeight()I

    move-result v1

    neg-int v1, v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    .line 215
    :goto_1
    invoke-virtual {v9, v14}, Landroidx/recyclerview/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v6

    if-eqz v0, :cond_4

    const/4 v0, -0x1

    if-eq v6, v0, :cond_4

    if-eqz v13, :cond_1

    .line 216
    invoke-direct {v7, v6}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->hasSubHeader(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 218
    :cond_1
    invoke-direct {v7, v9, v6}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getHeader(Landroidx/recyclerview/widget/RecyclerView;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object v0

    iget-object v5, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 219
    invoke-direct {v7, v9, v6}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getSubHeader(Landroidx/recyclerview/widget/RecyclerView;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object v0

    iget-object v4, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 221
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 222
    invoke-virtual {v14}, Landroid/view/View;->getLeft()I

    move-result v3

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object v2, v14

    move v11, v3

    move-object v3, v5

    move-object/from16 v16, v4

    move-object/from16 v17, v5

    move v5, v6

    move v15, v6

    move v6, v12

    .line 223
    invoke-direct/range {v0 .. v6}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getSubHeaderTop(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;Landroid/view/View;Landroid/view/View;II)I

    move-result v0

    int-to-float v1, v11

    int-to-float v0, v0

    .line 224
    invoke-virtual {v8, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 225
    invoke-virtual {v4, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 226
    invoke-virtual {v4, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 227
    invoke-virtual {v4, v8}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 228
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    if-eqz v13, :cond_2

    .line 230
    invoke-direct {v7, v15}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->hasHeader(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 231
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 232
    invoke-virtual {v14}, Landroid/view/View;->getLeft()I

    move-result v11

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object v2, v14

    move-object/from16 v3, v17

    move v5, v15

    move v6, v12

    .line 233
    invoke-direct/range {v0 .. v6}, Lca/barrenechea/widget/recyclerview/decoration/DoubleHeaderDecoration;->getHeaderTop(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;Landroid/view/View;Landroid/view/View;II)I

    move-result v0

    int-to-float v1, v11

    int-to-float v0, v0

    .line 234
    invoke-virtual {v8, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v2, v17

    .line 235
    invoke-virtual {v2, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 236
    invoke-virtual {v2, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 237
    invoke-virtual {v2, v8}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 238
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    :cond_3
    const/4 v13, 0x1

    :cond_4
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    :cond_5
    return-void
.end method
