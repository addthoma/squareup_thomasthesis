.class public final Lsquareup/items/merchant/Request$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Request;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/Request;",
        "Lsquareup/items/merchant/Request$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public delete_request:Lsquareup/items/merchant/DeleteRequest;

.field public get_request:Lsquareup/items/merchant/GetRequest;

.field public put_request:Lsquareup/items/merchant/PutRequest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 107
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 100
    invoke-virtual {p0}, Lsquareup/items/merchant/Request$Builder;->build()Lsquareup/items/merchant/Request;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/Request;
    .locals 5

    .line 127
    new-instance v0, Lsquareup/items/merchant/Request;

    iget-object v1, p0, Lsquareup/items/merchant/Request$Builder;->get_request:Lsquareup/items/merchant/GetRequest;

    iget-object v2, p0, Lsquareup/items/merchant/Request$Builder;->put_request:Lsquareup/items/merchant/PutRequest;

    iget-object v3, p0, Lsquareup/items/merchant/Request$Builder;->delete_request:Lsquareup/items/merchant/DeleteRequest;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lsquareup/items/merchant/Request;-><init>(Lsquareup/items/merchant/GetRequest;Lsquareup/items/merchant/PutRequest;Lsquareup/items/merchant/DeleteRequest;Lokio/ByteString;)V

    return-object v0
.end method

.method public delete_request(Lsquareup/items/merchant/DeleteRequest;)Lsquareup/items/merchant/Request$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lsquareup/items/merchant/Request$Builder;->delete_request:Lsquareup/items/merchant/DeleteRequest;

    return-object p0
.end method

.method public get_request(Lsquareup/items/merchant/GetRequest;)Lsquareup/items/merchant/Request$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lsquareup/items/merchant/Request$Builder;->get_request:Lsquareup/items/merchant/GetRequest;

    return-object p0
.end method

.method public put_request(Lsquareup/items/merchant/PutRequest;)Lsquareup/items/merchant/Request$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lsquareup/items/merchant/Request$Builder;->put_request:Lsquareup/items/merchant/PutRequest;

    return-object p0
.end method
