.class public final Lsquareup/items/merchant/DeleteResponse$DeletedObject$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeleteResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/DeleteResponse$DeletedObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/DeleteResponse$DeletedObject;",
        "Lsquareup/items/merchant/DeleteResponse$DeletedObject$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public token:Ljava/lang/String;

.field public type:Lsquareup/items/merchant/CatalogObjectType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 259
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 254
    invoke-virtual {p0}, Lsquareup/items/merchant/DeleteResponse$DeletedObject$Builder;->build()Lsquareup/items/merchant/DeleteResponse$DeletedObject;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/DeleteResponse$DeletedObject;
    .locals 4

    .line 274
    new-instance v0, Lsquareup/items/merchant/DeleteResponse$DeletedObject;

    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse$DeletedObject$Builder;->type:Lsquareup/items/merchant/CatalogObjectType;

    iget-object v2, p0, Lsquareup/items/merchant/DeleteResponse$DeletedObject$Builder;->token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lsquareup/items/merchant/DeleteResponse$DeletedObject;-><init>(Lsquareup/items/merchant/CatalogObjectType;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public token(Ljava/lang/String;)Lsquareup/items/merchant/DeleteResponse$DeletedObject$Builder;
    .locals 0

    .line 268
    iput-object p1, p0, Lsquareup/items/merchant/DeleteResponse$DeletedObject$Builder;->token:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lsquareup/items/merchant/CatalogObjectType;)Lsquareup/items/merchant/DeleteResponse$DeletedObject$Builder;
    .locals 0

    .line 263
    iput-object p1, p0, Lsquareup/items/merchant/DeleteResponse$DeletedObject$Builder;->type:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0
.end method
