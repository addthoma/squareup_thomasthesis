.class public final Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;
.super Lcom/squareup/wire/Message;
.source "AttributeDefinition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/AttributeDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AttributeVisibility"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$ProtoAdapter_AttributeVisibility;,
        Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;",
        "Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final object_types:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.CatalogObjectType#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 455
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$ProtoAdapter_AttributeVisibility;

    invoke-direct {v0}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$ProtoAdapter_AttributeVisibility;-><init>()V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObjectType;",
            ">;)V"
        }
    .end annotation

    .line 467
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObjectType;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 471
    sget-object v0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p2, "object_types"

    .line 472
    invoke-static {p2, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->object_types:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 486
    :cond_0
    instance-of v1, p1, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 487
    :cond_1
    check-cast p1, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    .line 488
    invoke-virtual {p0}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->object_types:Ljava/util/List;

    iget-object p1, p1, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->object_types:Ljava/util/List;

    .line 489
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 494
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_0

    .line 496
    invoke-virtual {p0}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 497
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->object_types:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 498
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_0
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 454
    invoke-virtual {p0}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->newBuilder()Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;
    .locals 2

    .line 477
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;-><init>()V

    .line 478
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->object_types:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;->object_types:Ljava/util/List;

    .line 479
    invoke-virtual {p0}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 505
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 506
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->object_types:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", object_types="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->object_types:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AttributeVisibility{"

    .line 507
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
