.class public final Lsquareup/items/merchant/Query$AttributeValues$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Query.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Query$AttributeValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/Query$AttributeValues;",
        "Lsquareup/items/merchant/Query$AttributeValues$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attribute_values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 810
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 811
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/Query$AttributeValues$Builder;->attribute_values:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public attribute_values(Ljava/util/List;)Lsquareup/items/merchant/Query$AttributeValues$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;",
            ">;)",
            "Lsquareup/items/merchant/Query$AttributeValues$Builder;"
        }
    .end annotation

    .line 818
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 819
    iput-object p1, p0, Lsquareup/items/merchant/Query$AttributeValues$Builder;->attribute_values:Ljava/util/List;

    return-object p0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 807
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$AttributeValues$Builder;->build()Lsquareup/items/merchant/Query$AttributeValues;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/Query$AttributeValues;
    .locals 3

    .line 825
    new-instance v0, Lsquareup/items/merchant/Query$AttributeValues;

    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues$Builder;->attribute_values:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lsquareup/items/merchant/Query$AttributeValues;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method
