.class public final Lsquareup/items/merchant/Query$Range$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Query.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Query$Range;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/Query$Range;",
        "Lsquareup/items/merchant/Query$Range$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public begin:Ljava/lang/Long;

.field public end:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 496
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public begin(Ljava/lang/Long;)Lsquareup/items/merchant/Query$Range$Builder;
    .locals 0

    .line 500
    iput-object p1, p0, Lsquareup/items/merchant/Query$Range$Builder;->begin:Ljava/lang/Long;

    return-object p0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 491
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$Range$Builder;->build()Lsquareup/items/merchant/Query$Range;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/Query$Range;
    .locals 4

    .line 511
    new-instance v0, Lsquareup/items/merchant/Query$Range;

    iget-object v1, p0, Lsquareup/items/merchant/Query$Range$Builder;->begin:Ljava/lang/Long;

    iget-object v2, p0, Lsquareup/items/merchant/Query$Range$Builder;->end:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lsquareup/items/merchant/Query$Range;-><init>(Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public end(Ljava/lang/Long;)Lsquareup/items/merchant/Query$Range$Builder;
    .locals 0

    .line 505
    iput-object p1, p0, Lsquareup/items/merchant/Query$Range$Builder;->end:Ljava/lang/Long;

    return-object p0
.end method
