.class final Lsquareup/spe/S1DeviceManifest$ProtoAdapter_S1DeviceManifest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "S1DeviceManifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/S1DeviceManifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_S1DeviceManifest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/spe/S1DeviceManifest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 182
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/spe/S1DeviceManifest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 180
    invoke-virtual {p0, p1}, Lsquareup/spe/S1DeviceManifest$ProtoAdapter_S1DeviceManifest;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/spe/S1DeviceManifest;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/spe/S1DeviceManifest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 205
    new-instance v0, Lsquareup/spe/S1DeviceManifest$Builder;

    invoke-direct {v0}, Lsquareup/spe/S1DeviceManifest$Builder;-><init>()V

    .line 206
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 207
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 214
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 212
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lsquareup/spe/S1DeviceManifest$Builder;->pts_version(Ljava/lang/Integer;)Lsquareup/spe/S1DeviceManifest$Builder;

    goto :goto_0

    .line 211
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lsquareup/spe/S1DeviceManifest$Builder;->max_compression_version(Ljava/lang/Integer;)Lsquareup/spe/S1DeviceManifest$Builder;

    goto :goto_0

    .line 210
    :pswitch_2
    sget-object v3, Lsquareup/spe/STM32F2Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/STM32F2Manifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/S1DeviceManifest$Builder;->stm32f2_manifest(Lsquareup/spe/STM32F2Manifest;)Lsquareup/spe/S1DeviceManifest$Builder;

    goto :goto_0

    .line 209
    :pswitch_3
    sget-object v3, Lsquareup/spe/CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/CommsProtocolVersion;

    invoke-virtual {v0, v3}, Lsquareup/spe/S1DeviceManifest$Builder;->comms_protocol_version(Lsquareup/spe/CommsProtocolVersion;)Lsquareup/spe/S1DeviceManifest$Builder;

    goto :goto_0

    .line 218
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/spe/S1DeviceManifest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 219
    invoke-virtual {v0}, Lsquareup/spe/S1DeviceManifest$Builder;->build()Lsquareup/spe/S1DeviceManifest;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 180
    check-cast p2, Lsquareup/spe/S1DeviceManifest;

    invoke-virtual {p0, p1, p2}, Lsquareup/spe/S1DeviceManifest$ProtoAdapter_S1DeviceManifest;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/spe/S1DeviceManifest;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/spe/S1DeviceManifest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 196
    sget-object v0, Lsquareup/spe/CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/S1DeviceManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    const/16 v2, 0x65

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 197
    sget-object v0, Lsquareup/spe/STM32F2Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/S1DeviceManifest;->stm32f2_manifest:Lsquareup/spe/STM32F2Manifest;

    const/16 v2, 0x66

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 198
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/S1DeviceManifest;->max_compression_version:Ljava/lang/Integer;

    const/16 v2, 0x67

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 199
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/S1DeviceManifest;->pts_version:Ljava/lang/Integer;

    const/16 v2, 0x68

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 200
    invoke-virtual {p2}, Lsquareup/spe/S1DeviceManifest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 180
    check-cast p1, Lsquareup/spe/S1DeviceManifest;

    invoke-virtual {p0, p1}, Lsquareup/spe/S1DeviceManifest$ProtoAdapter_S1DeviceManifest;->encodedSize(Lsquareup/spe/S1DeviceManifest;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/spe/S1DeviceManifest;)I
    .locals 4

    .line 187
    sget-object v0, Lsquareup/spe/CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/S1DeviceManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    const/16 v2, 0x65

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lsquareup/spe/STM32F2Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/S1DeviceManifest;->stm32f2_manifest:Lsquareup/spe/STM32F2Manifest;

    const/16 v3, 0x66

    .line 188
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/S1DeviceManifest;->max_compression_version:Ljava/lang/Integer;

    const/16 v3, 0x67

    .line 189
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/S1DeviceManifest;->pts_version:Ljava/lang/Integer;

    const/16 v3, 0x68

    .line 190
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 191
    invoke-virtual {p1}, Lsquareup/spe/S1DeviceManifest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 180
    check-cast p1, Lsquareup/spe/S1DeviceManifest;

    invoke-virtual {p0, p1}, Lsquareup/spe/S1DeviceManifest$ProtoAdapter_S1DeviceManifest;->redact(Lsquareup/spe/S1DeviceManifest;)Lsquareup/spe/S1DeviceManifest;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/spe/S1DeviceManifest;)Lsquareup/spe/S1DeviceManifest;
    .locals 2

    .line 224
    invoke-virtual {p1}, Lsquareup/spe/S1DeviceManifest;->newBuilder()Lsquareup/spe/S1DeviceManifest$Builder;

    move-result-object p1

    .line 225
    iget-object v0, p1, Lsquareup/spe/S1DeviceManifest$Builder;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    if-eqz v0, :cond_0

    sget-object v0, Lsquareup/spe/CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/S1DeviceManifest$Builder;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/CommsProtocolVersion;

    iput-object v0, p1, Lsquareup/spe/S1DeviceManifest$Builder;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    .line 226
    :cond_0
    iget-object v0, p1, Lsquareup/spe/S1DeviceManifest$Builder;->stm32f2_manifest:Lsquareup/spe/STM32F2Manifest;

    if-eqz v0, :cond_1

    sget-object v0, Lsquareup/spe/STM32F2Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/S1DeviceManifest$Builder;->stm32f2_manifest:Lsquareup/spe/STM32F2Manifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/STM32F2Manifest;

    iput-object v0, p1, Lsquareup/spe/S1DeviceManifest$Builder;->stm32f2_manifest:Lsquareup/spe/STM32F2Manifest;

    .line 227
    :cond_1
    invoke-virtual {p1}, Lsquareup/spe/S1DeviceManifest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 228
    invoke-virtual {p1}, Lsquareup/spe/S1DeviceManifest$Builder;->build()Lsquareup/spe/S1DeviceManifest;

    move-result-object p1

    return-object p1
.end method
