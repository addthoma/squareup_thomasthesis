.class public final Lsquareup/spe/FwupAssetGroup$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FwupAssetGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/FwupAssetGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/spe/FwupAssetGroup;",
        "Lsquareup/spe/FwupAssetGroup$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public crq_gt4_fw:Lsquareup/spe/FwupAssetDescriptor;

.field public fpga:Lsquareup/spe/FwupAssetDescriptor;

.field public k21_fw_a:Lsquareup/spe/FwupAssetDescriptor;

.field public k21_fw_b:Lsquareup/spe/FwupAssetDescriptor;

.field public k400_cpu0_fw:Lsquareup/spe/FwupAssetDescriptor;

.field public k400_cpu1_fw:Lsquareup/spe/FwupAssetDescriptor;

.field public k450_cpu0_fw_a:Lsquareup/spe/FwupAssetDescriptor;

.field public k450_cpu0_fw_b:Lsquareup/spe/FwupAssetDescriptor;

.field public k450_cpu1_fw_a:Lsquareup/spe/FwupAssetDescriptor;

.field public k450_cpu1_fw_b:Lsquareup/spe/FwupAssetDescriptor;

.field public tms_capk:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/spe/FwupAssetDescriptor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 221
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 222
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/spe/FwupAssetGroup$Builder;->tms_capk:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 198
    invoke-virtual {p0}, Lsquareup/spe/FwupAssetGroup$Builder;->build()Lsquareup/spe/FwupAssetGroup;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/spe/FwupAssetGroup;
    .locals 14

    .line 283
    new-instance v13, Lsquareup/spe/FwupAssetGroup;

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup$Builder;->k21_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    iget-object v2, p0, Lsquareup/spe/FwupAssetGroup$Builder;->k21_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    iget-object v3, p0, Lsquareup/spe/FwupAssetGroup$Builder;->k400_cpu0_fw:Lsquareup/spe/FwupAssetDescriptor;

    iget-object v4, p0, Lsquareup/spe/FwupAssetGroup$Builder;->k400_cpu1_fw:Lsquareup/spe/FwupAssetDescriptor;

    iget-object v5, p0, Lsquareup/spe/FwupAssetGroup$Builder;->fpga:Lsquareup/spe/FwupAssetDescriptor;

    iget-object v6, p0, Lsquareup/spe/FwupAssetGroup$Builder;->tms_capk:Ljava/util/List;

    iget-object v7, p0, Lsquareup/spe/FwupAssetGroup$Builder;->crq_gt4_fw:Lsquareup/spe/FwupAssetDescriptor;

    iget-object v8, p0, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu0_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    iget-object v9, p0, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu0_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    iget-object v10, p0, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu1_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    iget-object v11, p0, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu1_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lsquareup/spe/FwupAssetGroup;-><init>(Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Ljava/util/List;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lokio/ByteString;)V

    return-object v13
.end method

.method public crq_gt4_fw(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;
    .locals 0

    .line 257
    iput-object p1, p0, Lsquareup/spe/FwupAssetGroup$Builder;->crq_gt4_fw:Lsquareup/spe/FwupAssetDescriptor;

    return-object p0
.end method

.method public fpga(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;
    .locals 0

    .line 246
    iput-object p1, p0, Lsquareup/spe/FwupAssetGroup$Builder;->fpga:Lsquareup/spe/FwupAssetDescriptor;

    return-object p0
.end method

.method public k21_fw_a(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;
    .locals 0

    .line 226
    iput-object p1, p0, Lsquareup/spe/FwupAssetGroup$Builder;->k21_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    return-object p0
.end method

.method public k21_fw_b(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;
    .locals 0

    .line 231
    iput-object p1, p0, Lsquareup/spe/FwupAssetGroup$Builder;->k21_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    return-object p0
.end method

.method public k400_cpu0_fw(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;
    .locals 0

    .line 236
    iput-object p1, p0, Lsquareup/spe/FwupAssetGroup$Builder;->k400_cpu0_fw:Lsquareup/spe/FwupAssetDescriptor;

    return-object p0
.end method

.method public k400_cpu1_fw(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;
    .locals 0

    .line 241
    iput-object p1, p0, Lsquareup/spe/FwupAssetGroup$Builder;->k400_cpu1_fw:Lsquareup/spe/FwupAssetDescriptor;

    return-object p0
.end method

.method public k450_cpu0_fw_a(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;
    .locals 0

    .line 262
    iput-object p1, p0, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu0_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    return-object p0
.end method

.method public k450_cpu0_fw_b(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;
    .locals 0

    .line 267
    iput-object p1, p0, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu0_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    return-object p0
.end method

.method public k450_cpu1_fw_a(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;
    .locals 0

    .line 272
    iput-object p1, p0, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu1_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    return-object p0
.end method

.method public k450_cpu1_fw_b(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;
    .locals 0

    .line 277
    iput-object p1, p0, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu1_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    return-object p0
.end method

.method public tms_capk(Ljava/util/List;)Lsquareup/spe/FwupAssetGroup$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/spe/FwupAssetDescriptor;",
            ">;)",
            "Lsquareup/spe/FwupAssetGroup$Builder;"
        }
    .end annotation

    .line 251
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 252
    iput-object p1, p0, Lsquareup/spe/FwupAssetGroup$Builder;->tms_capk:Ljava/util/List;

    return-object p0
.end method
