.class public final Lsquareup/spe/K450Manifest;
.super Lcom/squareup/wire/Message;
.source "K450Manifest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/spe/K450Manifest$ProtoAdapter_K450Manifest;,
        Lsquareup/spe/K450Manifest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/spe/K450Manifest;",
        "Lsquareup/spe/K450Manifest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/spe/K450Manifest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CHIPID:Lokio/ByteString;

.field public static final DEFAULT_CHIP_REVISION:Lokio/ByteString;

.field public static final DEFAULT_CONFIGURATION:Lsquareup/spe/UnitConfiguration;

.field public static final DEFAULT_CPU0_RUNNING_SLOT:Lsquareup/spe/AssetTypeV2;

.field public static final DEFAULT_CPU1_RUNNING_SLOT:Lsquareup/spe/AssetTypeV2;

.field public static final DEFAULT_RAK_SELECT:Lsquareup/spe/RAKSelect;

.field public static final DEFAULT_READER_AUTHORITY_FINGERPRINT:Lokio/ByteString;

.field public static final DEFAULT_SIGNER_FINGERPRINT:Lokio/ByteString;

.field public static final DEFAULT_TMS_CAPK_RUNNING_SLOT:Lsquareup/spe/AssetTypeV2;

.field private static final serialVersionUID:J


# instance fields
.field public final chip_revision:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0xf
    .end annotation
.end field

.field public final chipid:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x4
    .end annotation
.end field

.field public final configuration:Lsquareup/spe/UnitConfiguration;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.UnitConfiguration#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final cpu0_firmware_a:Lsquareup/spe/AssetManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetManifest#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final cpu0_firmware_b:Lsquareup/spe/AssetManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetManifest#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final cpu0_running_slot:Lsquareup/spe/AssetTypeV2;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetTypeV2#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final cpu1_firmware_a:Lsquareup/spe/AssetManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetManifest#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final cpu1_firmware_b:Lsquareup/spe/AssetManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetManifest#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final cpu1_running_slot:Lsquareup/spe/AssetTypeV2;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetTypeV2#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final rak_select:Lsquareup/spe/RAKSelect;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.RAKSelect#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final reader_authority_fingerprint:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0xd
    .end annotation
.end field

.field public final signer_fingerprint:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x5
    .end annotation
.end field

.field public final tms_capk_a:Lsquareup/spe/AssetManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetManifest#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final tms_capk_b:Lsquareup/spe/AssetManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetManifest#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final tms_capk_running_slot:Lsquareup/spe/AssetTypeV2;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetTypeV2#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lsquareup/spe/K450Manifest$ProtoAdapter_K450Manifest;

    invoke-direct {v0}, Lsquareup/spe/K450Manifest$ProtoAdapter_K450Manifest;-><init>()V

    sput-object v0, Lsquareup/spe/K450Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 24
    sget-object v0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_A:Lsquareup/spe/AssetTypeV2;

    sput-object v0, Lsquareup/spe/K450Manifest;->DEFAULT_CPU0_RUNNING_SLOT:Lsquareup/spe/AssetTypeV2;

    .line 26
    sget-object v0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_A:Lsquareup/spe/AssetTypeV2;

    sput-object v0, Lsquareup/spe/K450Manifest;->DEFAULT_CPU1_RUNNING_SLOT:Lsquareup/spe/AssetTypeV2;

    .line 28
    sget-object v0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_A:Lsquareup/spe/AssetTypeV2;

    sput-object v0, Lsquareup/spe/K450Manifest;->DEFAULT_TMS_CAPK_RUNNING_SLOT:Lsquareup/spe/AssetTypeV2;

    .line 30
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lsquareup/spe/K450Manifest;->DEFAULT_CHIPID:Lokio/ByteString;

    .line 32
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lsquareup/spe/K450Manifest;->DEFAULT_SIGNER_FINGERPRINT:Lokio/ByteString;

    .line 34
    sget-object v0, Lsquareup/spe/UnitConfiguration;->CONFIG_PRODUCTION:Lsquareup/spe/UnitConfiguration;

    sput-object v0, Lsquareup/spe/K450Manifest;->DEFAULT_CONFIGURATION:Lsquareup/spe/UnitConfiguration;

    .line 36
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lsquareup/spe/K450Manifest;->DEFAULT_READER_AUTHORITY_FINGERPRINT:Lokio/ByteString;

    .line 38
    sget-object v0, Lsquareup/spe/RAKSelect;->RAK_HASH_IN_FUSE1:Lsquareup/spe/RAKSelect;

    sput-object v0, Lsquareup/spe/K450Manifest;->DEFAULT_RAK_SELECT:Lsquareup/spe/RAKSelect;

    .line 40
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lsquareup/spe/K450Manifest;->DEFAULT_CHIP_REVISION:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Lsquareup/spe/AssetTypeV2;Lsquareup/spe/AssetTypeV2;Lsquareup/spe/AssetTypeV2;Lokio/ByteString;Lokio/ByteString;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/UnitConfiguration;Lokio/ByteString;Lsquareup/spe/RAKSelect;Lokio/ByteString;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    .line 138
    sget-object v16, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct/range {v0 .. v16}, Lsquareup/spe/K450Manifest;-><init>(Lsquareup/spe/AssetTypeV2;Lsquareup/spe/AssetTypeV2;Lsquareup/spe/AssetTypeV2;Lokio/ByteString;Lokio/ByteString;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/UnitConfiguration;Lokio/ByteString;Lsquareup/spe/RAKSelect;Lokio/ByteString;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lsquareup/spe/AssetTypeV2;Lsquareup/spe/AssetTypeV2;Lsquareup/spe/AssetTypeV2;Lokio/ByteString;Lokio/ByteString;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/UnitConfiguration;Lokio/ByteString;Lsquareup/spe/RAKSelect;Lokio/ByteString;Lokio/ByteString;)V
    .locals 3

    move-object v0, p0

    .line 147
    sget-object v1, Lsquareup/spe/K450Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p16

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 148
    iput-object v1, v0, Lsquareup/spe/K450Manifest;->cpu0_running_slot:Lsquareup/spe/AssetTypeV2;

    move-object v1, p2

    .line 149
    iput-object v1, v0, Lsquareup/spe/K450Manifest;->cpu1_running_slot:Lsquareup/spe/AssetTypeV2;

    move-object v1, p3

    .line 150
    iput-object v1, v0, Lsquareup/spe/K450Manifest;->tms_capk_running_slot:Lsquareup/spe/AssetTypeV2;

    move-object v1, p4

    .line 151
    iput-object v1, v0, Lsquareup/spe/K450Manifest;->chipid:Lokio/ByteString;

    move-object v1, p5

    .line 152
    iput-object v1, v0, Lsquareup/spe/K450Manifest;->signer_fingerprint:Lokio/ByteString;

    move-object v1, p6

    .line 153
    iput-object v1, v0, Lsquareup/spe/K450Manifest;->cpu0_firmware_a:Lsquareup/spe/AssetManifest;

    move-object v1, p7

    .line 154
    iput-object v1, v0, Lsquareup/spe/K450Manifest;->cpu0_firmware_b:Lsquareup/spe/AssetManifest;

    move-object v1, p8

    .line 155
    iput-object v1, v0, Lsquareup/spe/K450Manifest;->cpu1_firmware_a:Lsquareup/spe/AssetManifest;

    move-object v1, p9

    .line 156
    iput-object v1, v0, Lsquareup/spe/K450Manifest;->cpu1_firmware_b:Lsquareup/spe/AssetManifest;

    move-object v1, p10

    .line 157
    iput-object v1, v0, Lsquareup/spe/K450Manifest;->tms_capk_a:Lsquareup/spe/AssetManifest;

    move-object v1, p11

    .line 158
    iput-object v1, v0, Lsquareup/spe/K450Manifest;->tms_capk_b:Lsquareup/spe/AssetManifest;

    move-object v1, p12

    .line 159
    iput-object v1, v0, Lsquareup/spe/K450Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    move-object/from16 v1, p13

    .line 160
    iput-object v1, v0, Lsquareup/spe/K450Manifest;->reader_authority_fingerprint:Lokio/ByteString;

    move-object/from16 v1, p14

    .line 161
    iput-object v1, v0, Lsquareup/spe/K450Manifest;->rak_select:Lsquareup/spe/RAKSelect;

    move-object/from16 v1, p15

    .line 162
    iput-object v1, v0, Lsquareup/spe/K450Manifest;->chip_revision:Lokio/ByteString;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 190
    :cond_0
    instance-of v1, p1, Lsquareup/spe/K450Manifest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 191
    :cond_1
    check-cast p1, Lsquareup/spe/K450Manifest;

    .line 192
    invoke-virtual {p0}, Lsquareup/spe/K450Manifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/spe/K450Manifest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu0_running_slot:Lsquareup/spe/AssetTypeV2;

    iget-object v3, p1, Lsquareup/spe/K450Manifest;->cpu0_running_slot:Lsquareup/spe/AssetTypeV2;

    .line 193
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu1_running_slot:Lsquareup/spe/AssetTypeV2;

    iget-object v3, p1, Lsquareup/spe/K450Manifest;->cpu1_running_slot:Lsquareup/spe/AssetTypeV2;

    .line 194
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->tms_capk_running_slot:Lsquareup/spe/AssetTypeV2;

    iget-object v3, p1, Lsquareup/spe/K450Manifest;->tms_capk_running_slot:Lsquareup/spe/AssetTypeV2;

    .line 195
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->chipid:Lokio/ByteString;

    iget-object v3, p1, Lsquareup/spe/K450Manifest;->chipid:Lokio/ByteString;

    .line 196
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->signer_fingerprint:Lokio/ByteString;

    iget-object v3, p1, Lsquareup/spe/K450Manifest;->signer_fingerprint:Lokio/ByteString;

    .line 197
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu0_firmware_a:Lsquareup/spe/AssetManifest;

    iget-object v3, p1, Lsquareup/spe/K450Manifest;->cpu0_firmware_a:Lsquareup/spe/AssetManifest;

    .line 198
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu0_firmware_b:Lsquareup/spe/AssetManifest;

    iget-object v3, p1, Lsquareup/spe/K450Manifest;->cpu0_firmware_b:Lsquareup/spe/AssetManifest;

    .line 199
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu1_firmware_a:Lsquareup/spe/AssetManifest;

    iget-object v3, p1, Lsquareup/spe/K450Manifest;->cpu1_firmware_a:Lsquareup/spe/AssetManifest;

    .line 200
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu1_firmware_b:Lsquareup/spe/AssetManifest;

    iget-object v3, p1, Lsquareup/spe/K450Manifest;->cpu1_firmware_b:Lsquareup/spe/AssetManifest;

    .line 201
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->tms_capk_a:Lsquareup/spe/AssetManifest;

    iget-object v3, p1, Lsquareup/spe/K450Manifest;->tms_capk_a:Lsquareup/spe/AssetManifest;

    .line 202
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->tms_capk_b:Lsquareup/spe/AssetManifest;

    iget-object v3, p1, Lsquareup/spe/K450Manifest;->tms_capk_b:Lsquareup/spe/AssetManifest;

    .line 203
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    iget-object v3, p1, Lsquareup/spe/K450Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    .line 204
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->reader_authority_fingerprint:Lokio/ByteString;

    iget-object v3, p1, Lsquareup/spe/K450Manifest;->reader_authority_fingerprint:Lokio/ByteString;

    .line 205
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->rak_select:Lsquareup/spe/RAKSelect;

    iget-object v3, p1, Lsquareup/spe/K450Manifest;->rak_select:Lsquareup/spe/RAKSelect;

    .line 206
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->chip_revision:Lokio/ByteString;

    iget-object p1, p1, Lsquareup/spe/K450Manifest;->chip_revision:Lokio/ByteString;

    .line 207
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 212
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_f

    .line 214
    invoke-virtual {p0}, Lsquareup/spe/K450Manifest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 215
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu0_running_slot:Lsquareup/spe/AssetTypeV2;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lsquareup/spe/AssetTypeV2;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 216
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu1_running_slot:Lsquareup/spe/AssetTypeV2;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lsquareup/spe/AssetTypeV2;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 217
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->tms_capk_running_slot:Lsquareup/spe/AssetTypeV2;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lsquareup/spe/AssetTypeV2;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 218
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->chipid:Lokio/ByteString;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 219
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->signer_fingerprint:Lokio/ByteString;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 220
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu0_firmware_a:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lsquareup/spe/AssetManifest;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 221
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu0_firmware_b:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lsquareup/spe/AssetManifest;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 222
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu1_firmware_a:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lsquareup/spe/AssetManifest;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 223
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu1_firmware_b:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lsquareup/spe/AssetManifest;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 224
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->tms_capk_a:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lsquareup/spe/AssetManifest;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 225
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->tms_capk_b:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lsquareup/spe/AssetManifest;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 226
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lsquareup/spe/UnitConfiguration;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 227
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->reader_authority_fingerprint:Lokio/ByteString;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 228
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->rak_select:Lsquareup/spe/RAKSelect;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lsquareup/spe/RAKSelect;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 229
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->chip_revision:Lokio/ByteString;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v2

    :cond_e
    add-int/2addr v0, v2

    .line 230
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_f
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lsquareup/spe/K450Manifest;->newBuilder()Lsquareup/spe/K450Manifest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/spe/K450Manifest$Builder;
    .locals 2

    .line 167
    new-instance v0, Lsquareup/spe/K450Manifest$Builder;

    invoke-direct {v0}, Lsquareup/spe/K450Manifest$Builder;-><init>()V

    .line 168
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu0_running_slot:Lsquareup/spe/AssetTypeV2;

    iput-object v1, v0, Lsquareup/spe/K450Manifest$Builder;->cpu0_running_slot:Lsquareup/spe/AssetTypeV2;

    .line 169
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu1_running_slot:Lsquareup/spe/AssetTypeV2;

    iput-object v1, v0, Lsquareup/spe/K450Manifest$Builder;->cpu1_running_slot:Lsquareup/spe/AssetTypeV2;

    .line 170
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->tms_capk_running_slot:Lsquareup/spe/AssetTypeV2;

    iput-object v1, v0, Lsquareup/spe/K450Manifest$Builder;->tms_capk_running_slot:Lsquareup/spe/AssetTypeV2;

    .line 171
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->chipid:Lokio/ByteString;

    iput-object v1, v0, Lsquareup/spe/K450Manifest$Builder;->chipid:Lokio/ByteString;

    .line 172
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->signer_fingerprint:Lokio/ByteString;

    iput-object v1, v0, Lsquareup/spe/K450Manifest$Builder;->signer_fingerprint:Lokio/ByteString;

    .line 173
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu0_firmware_a:Lsquareup/spe/AssetManifest;

    iput-object v1, v0, Lsquareup/spe/K450Manifest$Builder;->cpu0_firmware_a:Lsquareup/spe/AssetManifest;

    .line 174
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu0_firmware_b:Lsquareup/spe/AssetManifest;

    iput-object v1, v0, Lsquareup/spe/K450Manifest$Builder;->cpu0_firmware_b:Lsquareup/spe/AssetManifest;

    .line 175
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu1_firmware_a:Lsquareup/spe/AssetManifest;

    iput-object v1, v0, Lsquareup/spe/K450Manifest$Builder;->cpu1_firmware_a:Lsquareup/spe/AssetManifest;

    .line 176
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu1_firmware_b:Lsquareup/spe/AssetManifest;

    iput-object v1, v0, Lsquareup/spe/K450Manifest$Builder;->cpu1_firmware_b:Lsquareup/spe/AssetManifest;

    .line 177
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->tms_capk_a:Lsquareup/spe/AssetManifest;

    iput-object v1, v0, Lsquareup/spe/K450Manifest$Builder;->tms_capk_a:Lsquareup/spe/AssetManifest;

    .line 178
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->tms_capk_b:Lsquareup/spe/AssetManifest;

    iput-object v1, v0, Lsquareup/spe/K450Manifest$Builder;->tms_capk_b:Lsquareup/spe/AssetManifest;

    .line 179
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    iput-object v1, v0, Lsquareup/spe/K450Manifest$Builder;->configuration:Lsquareup/spe/UnitConfiguration;

    .line 180
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->reader_authority_fingerprint:Lokio/ByteString;

    iput-object v1, v0, Lsquareup/spe/K450Manifest$Builder;->reader_authority_fingerprint:Lokio/ByteString;

    .line 181
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->rak_select:Lsquareup/spe/RAKSelect;

    iput-object v1, v0, Lsquareup/spe/K450Manifest$Builder;->rak_select:Lsquareup/spe/RAKSelect;

    .line 182
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->chip_revision:Lokio/ByteString;

    iput-object v1, v0, Lsquareup/spe/K450Manifest$Builder;->chip_revision:Lokio/ByteString;

    .line 183
    invoke-virtual {p0}, Lsquareup/spe/K450Manifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/spe/K450Manifest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 237
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 238
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu0_running_slot:Lsquareup/spe/AssetTypeV2;

    if-eqz v1, :cond_0

    const-string v1, ", cpu0_running_slot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu0_running_slot:Lsquareup/spe/AssetTypeV2;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 239
    :cond_0
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu1_running_slot:Lsquareup/spe/AssetTypeV2;

    if-eqz v1, :cond_1

    const-string v1, ", cpu1_running_slot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu1_running_slot:Lsquareup/spe/AssetTypeV2;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 240
    :cond_1
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->tms_capk_running_slot:Lsquareup/spe/AssetTypeV2;

    if-eqz v1, :cond_2

    const-string v1, ", tms_capk_running_slot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->tms_capk_running_slot:Lsquareup/spe/AssetTypeV2;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 241
    :cond_2
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->chipid:Lokio/ByteString;

    if-eqz v1, :cond_3

    const-string v1, ", chipid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->chipid:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 242
    :cond_3
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->signer_fingerprint:Lokio/ByteString;

    if-eqz v1, :cond_4

    const-string v1, ", signer_fingerprint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->signer_fingerprint:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 243
    :cond_4
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu0_firmware_a:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_5

    const-string v1, ", cpu0_firmware_a="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu0_firmware_a:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 244
    :cond_5
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu0_firmware_b:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_6

    const-string v1, ", cpu0_firmware_b="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu0_firmware_b:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 245
    :cond_6
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu1_firmware_a:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_7

    const-string v1, ", cpu1_firmware_a="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu1_firmware_a:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 246
    :cond_7
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu1_firmware_b:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_8

    const-string v1, ", cpu1_firmware_b="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->cpu1_firmware_b:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 247
    :cond_8
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->tms_capk_a:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_9

    const-string v1, ", tms_capk_a="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->tms_capk_a:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 248
    :cond_9
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->tms_capk_b:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_a

    const-string v1, ", tms_capk_b="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->tms_capk_b:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 249
    :cond_a
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    if-eqz v1, :cond_b

    const-string v1, ", configuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 250
    :cond_b
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->reader_authority_fingerprint:Lokio/ByteString;

    if-eqz v1, :cond_c

    const-string v1, ", reader_authority_fingerprint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->reader_authority_fingerprint:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 251
    :cond_c
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->rak_select:Lsquareup/spe/RAKSelect;

    if-eqz v1, :cond_d

    const-string v1, ", rak_select="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->rak_select:Lsquareup/spe/RAKSelect;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 252
    :cond_d
    iget-object v1, p0, Lsquareup/spe/K450Manifest;->chip_revision:Lokio/ByteString;

    if-eqz v1, :cond_e

    const-string v1, ", chip_revision="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/K450Manifest;->chip_revision:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_e
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "K450Manifest{"

    .line 253
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
