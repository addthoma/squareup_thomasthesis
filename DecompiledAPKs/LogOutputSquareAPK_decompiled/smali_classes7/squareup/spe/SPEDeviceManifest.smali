.class public final Lsquareup/spe/SPEDeviceManifest;
.super Lcom/squareup/wire/Message;
.source "SPEDeviceManifest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/spe/SPEDeviceManifest$ProtoAdapter_SPEDeviceManifest;,
        Lsquareup/spe/SPEDeviceManifest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/spe/SPEDeviceManifest;",
        "Lsquareup/spe/SPEDeviceManifest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/spe/SPEDeviceManifest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_HWID:Lokio/ByteString;

.field public static final DEFAULT_MLB_SERIAL_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_SERIAL_NUMBER:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final hwid:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x12d
    .end annotation
.end field

.field public final mlb_serial_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x130
    .end annotation
.end field

.field public final r12c_manifest:Lsquareup/spe/R12CManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.R12CManifest#ADAPTER"
        tag = 0x12f
    .end annotation
.end field

.field public final s1_manifest:Lsquareup/spe/S1DeviceManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.S1DeviceManifest#ADAPTER"
        tag = 0x12b
    .end annotation
.end field

.field public final serial_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x12e
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lsquareup/spe/SPEDeviceManifest$ProtoAdapter_SPEDeviceManifest;

    invoke-direct {v0}, Lsquareup/spe/SPEDeviceManifest$ProtoAdapter_SPEDeviceManifest;-><init>()V

    sput-object v0, Lsquareup/spe/SPEDeviceManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 27
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lsquareup/spe/SPEDeviceManifest;->DEFAULT_HWID:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Ljava/lang/String;Ljava/lang/String;Lsquareup/spe/R12CManifest;Lsquareup/spe/S1DeviceManifest;)V
    .locals 7

    .line 77
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lsquareup/spe/SPEDeviceManifest;-><init>(Lokio/ByteString;Ljava/lang/String;Ljava/lang/String;Lsquareup/spe/R12CManifest;Lsquareup/spe/S1DeviceManifest;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Ljava/lang/String;Ljava/lang/String;Lsquareup/spe/R12CManifest;Lsquareup/spe/S1DeviceManifest;Lokio/ByteString;)V
    .locals 1

    .line 82
    sget-object v0, Lsquareup/spe/SPEDeviceManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 83
    invoke-static {p4, p5}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p6

    const/4 v0, 0x1

    if-gt p6, v0, :cond_0

    .line 86
    iput-object p1, p0, Lsquareup/spe/SPEDeviceManifest;->hwid:Lokio/ByteString;

    .line 87
    iput-object p2, p0, Lsquareup/spe/SPEDeviceManifest;->serial_number:Ljava/lang/String;

    .line 88
    iput-object p3, p0, Lsquareup/spe/SPEDeviceManifest;->mlb_serial_number:Ljava/lang/String;

    .line 89
    iput-object p4, p0, Lsquareup/spe/SPEDeviceManifest;->r12c_manifest:Lsquareup/spe/R12CManifest;

    .line 90
    iput-object p5, p0, Lsquareup/spe/SPEDeviceManifest;->s1_manifest:Lsquareup/spe/S1DeviceManifest;

    return-void

    .line 84
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of r12c_manifest, s1_manifest may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 108
    :cond_0
    instance-of v1, p1, Lsquareup/spe/SPEDeviceManifest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 109
    :cond_1
    check-cast p1, Lsquareup/spe/SPEDeviceManifest;

    .line 110
    invoke-virtual {p0}, Lsquareup/spe/SPEDeviceManifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/spe/SPEDeviceManifest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->hwid:Lokio/ByteString;

    iget-object v3, p1, Lsquareup/spe/SPEDeviceManifest;->hwid:Lokio/ByteString;

    .line 111
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->serial_number:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/spe/SPEDeviceManifest;->serial_number:Ljava/lang/String;

    .line 112
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->mlb_serial_number:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/spe/SPEDeviceManifest;->mlb_serial_number:Ljava/lang/String;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->r12c_manifest:Lsquareup/spe/R12CManifest;

    iget-object v3, p1, Lsquareup/spe/SPEDeviceManifest;->r12c_manifest:Lsquareup/spe/R12CManifest;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->s1_manifest:Lsquareup/spe/S1DeviceManifest;

    iget-object p1, p1, Lsquareup/spe/SPEDeviceManifest;->s1_manifest:Lsquareup/spe/S1DeviceManifest;

    .line 115
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 120
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 122
    invoke-virtual {p0}, Lsquareup/spe/SPEDeviceManifest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->hwid:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->serial_number:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->mlb_serial_number:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->r12c_manifest:Lsquareup/spe/R12CManifest;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lsquareup/spe/R12CManifest;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->s1_manifest:Lsquareup/spe/S1DeviceManifest;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lsquareup/spe/S1DeviceManifest;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 128
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lsquareup/spe/SPEDeviceManifest;->newBuilder()Lsquareup/spe/SPEDeviceManifest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/spe/SPEDeviceManifest$Builder;
    .locals 2

    .line 95
    new-instance v0, Lsquareup/spe/SPEDeviceManifest$Builder;

    invoke-direct {v0}, Lsquareup/spe/SPEDeviceManifest$Builder;-><init>()V

    .line 96
    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->hwid:Lokio/ByteString;

    iput-object v1, v0, Lsquareup/spe/SPEDeviceManifest$Builder;->hwid:Lokio/ByteString;

    .line 97
    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->serial_number:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/spe/SPEDeviceManifest$Builder;->serial_number:Ljava/lang/String;

    .line 98
    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->mlb_serial_number:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/spe/SPEDeviceManifest$Builder;->mlb_serial_number:Ljava/lang/String;

    .line 99
    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->r12c_manifest:Lsquareup/spe/R12CManifest;

    iput-object v1, v0, Lsquareup/spe/SPEDeviceManifest$Builder;->r12c_manifest:Lsquareup/spe/R12CManifest;

    .line 100
    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->s1_manifest:Lsquareup/spe/S1DeviceManifest;

    iput-object v1, v0, Lsquareup/spe/SPEDeviceManifest$Builder;->s1_manifest:Lsquareup/spe/S1DeviceManifest;

    .line 101
    invoke-virtual {p0}, Lsquareup/spe/SPEDeviceManifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/spe/SPEDeviceManifest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 136
    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->hwid:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", hwid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->hwid:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 137
    :cond_0
    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->serial_number:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", serial_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->serial_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    :cond_1
    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->mlb_serial_number:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", mlb_serial_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->mlb_serial_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    :cond_2
    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->r12c_manifest:Lsquareup/spe/R12CManifest;

    if-eqz v1, :cond_3

    const-string v1, ", r12c_manifest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->r12c_manifest:Lsquareup/spe/R12CManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 140
    :cond_3
    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->s1_manifest:Lsquareup/spe/S1DeviceManifest;

    if-eqz v1, :cond_4

    const-string v1, ", s1_manifest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/SPEDeviceManifest;->s1_manifest:Lsquareup/spe/S1DeviceManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SPEDeviceManifest{"

    .line 141
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
