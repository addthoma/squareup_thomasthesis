.class final Lsquareup/spe/K400Manifest$ProtoAdapter_K400Manifest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "K400Manifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/K400Manifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_K400Manifest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/spe/K400Manifest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 293
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/spe/K400Manifest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 291
    invoke-virtual {p0, p1}, Lsquareup/spe/K400Manifest$ProtoAdapter_K400Manifest;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/spe/K400Manifest;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/spe/K400Manifest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 330
    new-instance v0, Lsquareup/spe/K400Manifest$Builder;

    invoke-direct {v0}, Lsquareup/spe/K400Manifest$Builder;-><init>()V

    .line 331
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 332
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 353
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 351
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lsquareup/spe/K400Manifest$Builder;->tms_capk_struct_version_supported_by_cpu0(Ljava/lang/Integer;)Lsquareup/spe/K400Manifest$Builder;

    goto :goto_0

    .line 350
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lsquareup/spe/K400Manifest$Builder;->tms_capk_struct_version(Ljava/lang/Integer;)Lsquareup/spe/K400Manifest$Builder;

    goto :goto_0

    .line 349
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lsquareup/spe/K400Manifest$Builder;->jtag_fuses(Lokio/ByteString;)Lsquareup/spe/K400Manifest$Builder;

    goto :goto_0

    .line 343
    :pswitch_3
    :try_start_0
    sget-object v4, Lsquareup/spe/UnitConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsquareup/spe/UnitConfiguration;

    invoke-virtual {v0, v4}, Lsquareup/spe/K400Manifest$Builder;->configuration(Lsquareup/spe/UnitConfiguration;)Lsquareup/spe/K400Manifest$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 345
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lsquareup/spe/K400Manifest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 340
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lsquareup/spe/K400Manifest$Builder;->signer_fingerprint(Lokio/ByteString;)Lsquareup/spe/K400Manifest$Builder;

    goto :goto_0

    .line 339
    :pswitch_5
    sget-object v3, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/K400Manifest$Builder;->fpga(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K400Manifest$Builder;

    goto :goto_0

    .line 338
    :pswitch_6
    sget-object v3, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/K400Manifest$Builder;->tms_capk(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K400Manifest$Builder;

    goto :goto_0

    .line 337
    :pswitch_7
    sget-object v3, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/K400Manifest$Builder;->cpu1_firmware(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K400Manifest$Builder;

    goto :goto_0

    .line 336
    :pswitch_8
    sget-object v3, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/K400Manifest$Builder;->cpu0_firmware(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K400Manifest$Builder;

    goto/16 :goto_0

    .line 335
    :pswitch_9
    sget-object v3, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/K400Manifest$Builder;->cpu1_bootloader(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K400Manifest$Builder;

    goto/16 :goto_0

    .line 334
    :pswitch_a
    sget-object v3, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/K400Manifest$Builder;->cpu0_bootloader(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K400Manifest$Builder;

    goto/16 :goto_0

    .line 357
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/spe/K400Manifest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 358
    invoke-virtual {v0}, Lsquareup/spe/K400Manifest$Builder;->build()Lsquareup/spe/K400Manifest;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 291
    check-cast p2, Lsquareup/spe/K400Manifest;

    invoke-virtual {p0, p1, p2}, Lsquareup/spe/K400Manifest$ProtoAdapter_K400Manifest;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/spe/K400Manifest;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/spe/K400Manifest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 314
    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K400Manifest;->cpu0_bootloader:Lsquareup/spe/AssetManifest;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 315
    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K400Manifest;->cpu1_bootloader:Lsquareup/spe/AssetManifest;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 316
    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K400Manifest;->cpu0_firmware:Lsquareup/spe/AssetManifest;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 317
    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K400Manifest;->cpu1_firmware:Lsquareup/spe/AssetManifest;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 318
    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K400Manifest;->tms_capk:Lsquareup/spe/AssetManifest;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 319
    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K400Manifest;->fpga:Lsquareup/spe/AssetManifest;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 320
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K400Manifest;->signer_fingerprint:Lokio/ByteString;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 321
    sget-object v0, Lsquareup/spe/UnitConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K400Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 322
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K400Manifest;->jtag_fuses:Lokio/ByteString;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 323
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K400Manifest;->tms_capk_struct_version:Ljava/lang/Integer;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 324
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K400Manifest;->tms_capk_struct_version_supported_by_cpu0:Ljava/lang/Integer;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 325
    invoke-virtual {p2}, Lsquareup/spe/K400Manifest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 291
    check-cast p1, Lsquareup/spe/K400Manifest;

    invoke-virtual {p0, p1}, Lsquareup/spe/K400Manifest$ProtoAdapter_K400Manifest;->encodedSize(Lsquareup/spe/K400Manifest;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/spe/K400Manifest;)I
    .locals 4

    .line 298
    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/K400Manifest;->cpu0_bootloader:Lsquareup/spe/AssetManifest;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K400Manifest;->cpu1_bootloader:Lsquareup/spe/AssetManifest;

    const/4 v3, 0x2

    .line 299
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K400Manifest;->cpu0_firmware:Lsquareup/spe/AssetManifest;

    const/4 v3, 0x3

    .line 300
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K400Manifest;->cpu1_firmware:Lsquareup/spe/AssetManifest;

    const/4 v3, 0x4

    .line 301
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K400Manifest;->tms_capk:Lsquareup/spe/AssetManifest;

    const/4 v3, 0x5

    .line 302
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K400Manifest;->fpga:Lsquareup/spe/AssetManifest;

    const/4 v3, 0x6

    .line 303
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K400Manifest;->signer_fingerprint:Lokio/ByteString;

    const/4 v3, 0x7

    .line 304
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/UnitConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K400Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    const/16 v3, 0x8

    .line 305
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K400Manifest;->jtag_fuses:Lokio/ByteString;

    const/16 v3, 0x9

    .line 306
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K400Manifest;->tms_capk_struct_version:Ljava/lang/Integer;

    const/16 v3, 0xa

    .line 307
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K400Manifest;->tms_capk_struct_version_supported_by_cpu0:Ljava/lang/Integer;

    const/16 v3, 0xb

    .line 308
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 309
    invoke-virtual {p1}, Lsquareup/spe/K400Manifest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 291
    check-cast p1, Lsquareup/spe/K400Manifest;

    invoke-virtual {p0, p1}, Lsquareup/spe/K400Manifest$ProtoAdapter_K400Manifest;->redact(Lsquareup/spe/K400Manifest;)Lsquareup/spe/K400Manifest;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/spe/K400Manifest;)Lsquareup/spe/K400Manifest;
    .locals 2

    .line 363
    invoke-virtual {p1}, Lsquareup/spe/K400Manifest;->newBuilder()Lsquareup/spe/K400Manifest$Builder;

    move-result-object p1

    .line 364
    iget-object v0, p1, Lsquareup/spe/K400Manifest$Builder;->cpu0_bootloader:Lsquareup/spe/AssetManifest;

    if-eqz v0, :cond_0

    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/K400Manifest$Builder;->cpu0_bootloader:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/AssetManifest;

    iput-object v0, p1, Lsquareup/spe/K400Manifest$Builder;->cpu0_bootloader:Lsquareup/spe/AssetManifest;

    .line 365
    :cond_0
    iget-object v0, p1, Lsquareup/spe/K400Manifest$Builder;->cpu1_bootloader:Lsquareup/spe/AssetManifest;

    if-eqz v0, :cond_1

    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/K400Manifest$Builder;->cpu1_bootloader:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/AssetManifest;

    iput-object v0, p1, Lsquareup/spe/K400Manifest$Builder;->cpu1_bootloader:Lsquareup/spe/AssetManifest;

    .line 366
    :cond_1
    iget-object v0, p1, Lsquareup/spe/K400Manifest$Builder;->cpu0_firmware:Lsquareup/spe/AssetManifest;

    if-eqz v0, :cond_2

    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/K400Manifest$Builder;->cpu0_firmware:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/AssetManifest;

    iput-object v0, p1, Lsquareup/spe/K400Manifest$Builder;->cpu0_firmware:Lsquareup/spe/AssetManifest;

    .line 367
    :cond_2
    iget-object v0, p1, Lsquareup/spe/K400Manifest$Builder;->cpu1_firmware:Lsquareup/spe/AssetManifest;

    if-eqz v0, :cond_3

    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/K400Manifest$Builder;->cpu1_firmware:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/AssetManifest;

    iput-object v0, p1, Lsquareup/spe/K400Manifest$Builder;->cpu1_firmware:Lsquareup/spe/AssetManifest;

    .line 368
    :cond_3
    iget-object v0, p1, Lsquareup/spe/K400Manifest$Builder;->tms_capk:Lsquareup/spe/AssetManifest;

    if-eqz v0, :cond_4

    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/K400Manifest$Builder;->tms_capk:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/AssetManifest;

    iput-object v0, p1, Lsquareup/spe/K400Manifest$Builder;->tms_capk:Lsquareup/spe/AssetManifest;

    .line 369
    :cond_4
    iget-object v0, p1, Lsquareup/spe/K400Manifest$Builder;->fpga:Lsquareup/spe/AssetManifest;

    if-eqz v0, :cond_5

    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/K400Manifest$Builder;->fpga:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/AssetManifest;

    iput-object v0, p1, Lsquareup/spe/K400Manifest$Builder;->fpga:Lsquareup/spe/AssetManifest;

    .line 370
    :cond_5
    invoke-virtual {p1}, Lsquareup/spe/K400Manifest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 371
    invoke-virtual {p1}, Lsquareup/spe/K400Manifest$Builder;->build()Lsquareup/spe/K400Manifest;

    move-result-object p1

    return-object p1
.end method
