.class public final Lretrofit2/mock/BehaviorDelegate;
.super Ljava/lang/Object;
.source "BehaviorDelegate.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final behavior:Lretrofit2/mock/NetworkBehavior;

.field private final executor:Ljava/util/concurrent/ExecutorService;

.field final retrofit:Lretrofit2/Retrofit;

.field private final service:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lretrofit2/Retrofit;Lretrofit2/mock/NetworkBehavior;Ljava/util/concurrent/ExecutorService;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/Retrofit;",
            "Lretrofit2/mock/NetworkBehavior;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/lang/Class<",
            "TT;>;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lretrofit2/mock/BehaviorDelegate;->retrofit:Lretrofit2/Retrofit;

    .line 43
    iput-object p2, p0, Lretrofit2/mock/BehaviorDelegate;->behavior:Lretrofit2/mock/NetworkBehavior;

    .line 44
    iput-object p3, p0, Lretrofit2/mock/BehaviorDelegate;->executor:Ljava/util/concurrent/ExecutorService;

    .line 45
    iput-object p4, p0, Lretrofit2/mock/BehaviorDelegate;->service:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public synthetic lambda$returning$0$BehaviorDelegate(Lretrofit2/Call;Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 57
    invoke-virtual {p3}, Ljava/lang/reflect/Method;->getGenericReturnType()Ljava/lang/reflect/Type;

    move-result-object p2

    .line 58
    invoke-virtual {p3}, Ljava/lang/reflect/Method;->getAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object p3

    .line 59
    iget-object p4, p0, Lretrofit2/mock/BehaviorDelegate;->retrofit:Lretrofit2/Retrofit;

    .line 60
    invoke-virtual {p4, p2, p3}, Lretrofit2/Retrofit;->callAdapter(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lretrofit2/CallAdapter;

    move-result-object p2

    .line 61
    invoke-interface {p2, p1}, Lretrofit2/CallAdapter;->adapt(Lretrofit2/Call;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public returning(Lretrofit2/Call;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lretrofit2/Call<",
            "TR;>;)TT;"
        }
    .end annotation

    .line 54
    new-instance v0, Lretrofit2/mock/BehaviorCall;

    iget-object v1, p0, Lretrofit2/mock/BehaviorDelegate;->behavior:Lretrofit2/mock/NetworkBehavior;

    iget-object v2, p0, Lretrofit2/mock/BehaviorDelegate;->executor:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v0, v1, v2, p1}, Lretrofit2/mock/BehaviorCall;-><init>(Lretrofit2/mock/NetworkBehavior;Ljava/util/concurrent/ExecutorService;Lretrofit2/Call;)V

    .line 55
    iget-object p1, p0, Lretrofit2/mock/BehaviorDelegate;->service:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    iget-object v2, p0, Lretrofit2/mock/BehaviorDelegate;->service:Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lretrofit2/mock/-$$Lambda$BehaviorDelegate$1URGFiUdYPD6MPyhpAwAEj6QkUs;

    invoke-direct {v2, p0, v0}, Lretrofit2/mock/-$$Lambda$BehaviorDelegate$1URGFiUdYPD6MPyhpAwAEj6QkUs;-><init>(Lretrofit2/mock/BehaviorDelegate;Lretrofit2/Call;)V

    invoke-static {p1, v1, v2}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public returningResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .line 49
    invoke-static {p1}, Lretrofit2/mock/Calls;->response(Ljava/lang/Object;)Lretrofit2/Call;

    move-result-object p1

    invoke-virtual {p0, p1}, Lretrofit2/mock/BehaviorDelegate;->returning(Lretrofit2/Call;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
