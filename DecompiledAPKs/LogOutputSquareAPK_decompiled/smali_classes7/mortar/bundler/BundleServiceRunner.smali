.class public Lmortar/bundler/BundleServiceRunner;
.super Ljava/lang/Object;
.source "BundleServiceRunner.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmortar/bundler/BundleServiceRunner$State;
    }
.end annotation


# static fields
.field public static final SERVICE_NAME:Ljava/lang/String;


# instance fields
.field rootBundle:Landroid/os/Bundle;

.field private rootScopePath:Ljava/lang/String;

.field final scopedServices:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lmortar/bundler/BundleService;",
            ">;"
        }
    .end annotation
.end field

.field final servicesToBeLoaded:Ljava/util/NavigableSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/NavigableSet<",
            "Lmortar/bundler/BundleService;",
            ">;"
        }
    .end annotation
.end field

.field state:Lmortar/bundler/BundleServiceRunner$State;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    const-class v0, Lmortar/bundler/BundleServiceRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmortar/bundler/BundleServiceRunner;->SERVICE_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lmortar/bundler/BundleServiceRunner;->scopedServices:Ljava/util/Map;

    .line 27
    new-instance v0, Ljava/util/TreeSet;

    new-instance v1, Lmortar/bundler/BundleServiceComparator;

    invoke-direct {v1}, Lmortar/bundler/BundleServiceComparator;-><init>()V

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lmortar/bundler/BundleServiceRunner;->servicesToBeLoaded:Ljava/util/NavigableSet;

    .line 36
    sget-object v0, Lmortar/bundler/BundleServiceRunner$State;->IDLE:Lmortar/bundler/BundleServiceRunner$State;

    iput-object v0, p0, Lmortar/bundler/BundleServiceRunner;->state:Lmortar/bundler/BundleServiceRunner$State;

    return-void
.end method

.method public static getBundleServiceRunner(Landroid/content/Context;)Lmortar/bundler/BundleServiceRunner;
    .locals 1

    .line 19
    sget-object v0, Lmortar/bundler/BundleServiceRunner;->SERVICE_NAME:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lmortar/bundler/BundleServiceRunner;

    return-object p0
.end method

.method public static getBundleServiceRunner(Lmortar/MortarScope;)Lmortar/bundler/BundleServiceRunner;
    .locals 1

    .line 23
    sget-object v0, Lmortar/bundler/BundleServiceRunner;->SERVICE_NAME:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmortar/MortarScope;->getService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lmortar/bundler/BundleServiceRunner;

    return-object p0
.end method


# virtual methods
.method bundleKey(Lmortar/MortarScope;)Ljava/lang/String;
    .locals 3

    .line 116
    iget-object v0, p0, Lmortar/bundler/BundleServiceRunner;->rootScopePath:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 117
    invoke-virtual {p1}, Lmortar/MortarScope;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 118
    iget-object v1, p0, Lmortar/bundler/BundleServiceRunner;->rootScopePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    iget-object p1, p0, Lmortar/bundler/BundleServiceRunner;->rootScopePath:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 119
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 p1, 0x1

    iget-object v2, p0, Lmortar/bundler/BundleServiceRunner;->rootScopePath:Ljava/lang/String;

    aput-object v2, v1, p1

    const-string p1, "\"%s\" is not under \"%s\""

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Was this service not registered?"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method finishLoading()V
    .locals 3

    .line 103
    iget-object v0, p0, Lmortar/bundler/BundleServiceRunner;->state:Lmortar/bundler/BundleServiceRunner$State;

    sget-object v1, Lmortar/bundler/BundleServiceRunner$State;->IDLE:Lmortar/bundler/BundleServiceRunner$State;

    if-ne v0, v1, :cond_2

    .line 104
    sget-object v0, Lmortar/bundler/BundleServiceRunner$State;->LOADING:Lmortar/bundler/BundleServiceRunner$State;

    iput-object v0, p0, Lmortar/bundler/BundleServiceRunner;->state:Lmortar/bundler/BundleServiceRunner$State;

    .line 106
    :cond_0
    :goto_0
    iget-object v0, p0, Lmortar/bundler/BundleServiceRunner;->servicesToBeLoaded:Ljava/util/NavigableSet;

    invoke-interface {v0}, Ljava/util/NavigableSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 107
    iget-object v0, p0, Lmortar/bundler/BundleServiceRunner;->servicesToBeLoaded:Ljava/util/NavigableSet;

    invoke-interface {v0}, Ljava/util/NavigableSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/bundler/BundleService;

    .line 108
    invoke-virtual {v0}, Lmortar/bundler/BundleService;->loadOne()V

    .line 109
    invoke-virtual {v0}, Lmortar/bundler/BundleService;->needsLoading()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lmortar/bundler/BundleServiceRunner;->servicesToBeLoaded:Ljava/util/NavigableSet;

    invoke-interface {v1, v0}, Ljava/util/NavigableSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 112
    :cond_1
    sget-object v0, Lmortar/bundler/BundleServiceRunner$State;->IDLE:Lmortar/bundler/BundleServiceRunner$State;

    iput-object v0, p0, Lmortar/bundler/BundleServiceRunner;->state:Lmortar/bundler/BundleServiceRunner$State;

    return-void

    .line 103
    :cond_2
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lmortar/bundler/BundleServiceRunner;->state:Lmortar/bundler/BundleServiceRunner$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 65
    iput-object p1, p0, Lmortar/bundler/BundleServiceRunner;->rootBundle:Landroid/os/Bundle;

    .line 67
    iget-object p1, p0, Lmortar/bundler/BundleServiceRunner;->scopedServices:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 68
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/bundler/BundleService;

    .line 69
    iget-object v1, p0, Lmortar/bundler/BundleServiceRunner;->rootBundle:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->updateScopedBundleOnCreate(Landroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    iget-object v1, p0, Lmortar/bundler/BundleServiceRunner;->servicesToBeLoaded:Ljava/util/NavigableSet;

    invoke-interface {v1, v0}, Ljava/util/NavigableSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 73
    :cond_1
    invoke-virtual {p0}, Lmortar/bundler/BundleServiceRunner;->finishLoading()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 50
    iget-object v0, p0, Lmortar/bundler/BundleServiceRunner;->rootScopePath:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 51
    invoke-virtual {p1}, Lmortar/MortarScope;->getPath()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lmortar/bundler/BundleServiceRunner;->rootScopePath:Ljava/lang/String;

    return-void

    .line 50
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot double register"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .line 82
    iget-object v0, p0, Lmortar/bundler/BundleServiceRunner;->state:Lmortar/bundler/BundleServiceRunner$State;

    sget-object v1, Lmortar/bundler/BundleServiceRunner$State;->IDLE:Lmortar/bundler/BundleServiceRunner$State;

    if-ne v0, v1, :cond_2

    .line 85
    iput-object p1, p0, Lmortar/bundler/BundleServiceRunner;->rootBundle:Landroid/os/Bundle;

    .line 87
    sget-object p1, Lmortar/bundler/BundleServiceRunner$State;->SAVING:Lmortar/bundler/BundleServiceRunner$State;

    iput-object p1, p0, Lmortar/bundler/BundleServiceRunner;->state:Lmortar/bundler/BundleServiceRunner$State;

    .line 91
    new-instance p1, Ljava/util/ArrayList;

    iget-object v0, p0, Lmortar/bundler/BundleServiceRunner;->scopedServices:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 94
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 95
    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 96
    iget-object v1, p0, Lmortar/bundler/BundleServiceRunner;->scopedServices:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/bundler/BundleService;

    iget-object v1, p0, Lmortar/bundler/BundleServiceRunner;->rootBundle:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->saveToRootBundle(Landroid/os/Bundle;)V

    goto :goto_0

    .line 99
    :cond_1
    sget-object p1, Lmortar/bundler/BundleServiceRunner$State;->IDLE:Lmortar/bundler/BundleServiceRunner$State;

    iput-object p1, p0, Lmortar/bundler/BundleServiceRunner;->state:Lmortar/bundler/BundleServiceRunner$State;

    return-void

    .line 83
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot handle onSaveInstanceState while "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmortar/bundler/BundleServiceRunner;->state:Lmortar/bundler/BundleServiceRunner$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method requireBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;
    .locals 2

    .line 41
    iget-object v0, p0, Lmortar/bundler/BundleServiceRunner;->scopedServices:Ljava/util/Map;

    invoke-virtual {p0, p1}, Lmortar/bundler/BundleServiceRunner;->bundleKey(Lmortar/MortarScope;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/bundler/BundleService;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lmortar/bundler/BundleService;

    invoke-direct {v0, p0, p1}, Lmortar/bundler/BundleService;-><init>(Lmortar/bundler/BundleServiceRunner;Lmortar/MortarScope;)V

    .line 44
    invoke-virtual {v0}, Lmortar/bundler/BundleService;->init()V

    :cond_0
    return-object v0
.end method
