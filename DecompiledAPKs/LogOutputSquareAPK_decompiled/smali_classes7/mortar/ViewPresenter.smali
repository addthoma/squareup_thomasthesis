.class public Lmortar/ViewPresenter;
.super Lmortar/Presenter;
.source "ViewPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Lmortar/Presenter<",
        "TV;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected final extractBundleService(Landroid/view/View;)Lmortar/bundler/BundleService;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Lmortar/bundler/BundleService;"
        }
    .end annotation

    .line 23
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 21
    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lmortar/ViewPresenter;->extractBundleService(Landroid/view/View;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method
