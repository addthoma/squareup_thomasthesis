.class Lmortar/MortarScopeDevHelper$MortarScopeNode;
.super Ljava/lang/Object;
.source "MortarScopeDevHelper.java"

# interfaces
.implements Lmortar/MortarScopeDevHelper$Node;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmortar/MortarScopeDevHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MortarScopeNode"
.end annotation


# instance fields
.field private final mortarScope:Lmortar/MortarScope;


# direct methods
.method constructor <init>(Lmortar/MortarScope;)V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lmortar/MortarScopeDevHelper$MortarScopeNode;->mortarScope:Lmortar/MortarScope;

    return-void
.end method

.method private addScopeChildren(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lmortar/MortarScopeDevHelper$Node;",
            ">;)V"
        }
    .end annotation

    .line 49
    iget-object v0, p0, Lmortar/MortarScopeDevHelper$MortarScopeNode;->mortarScope:Lmortar/MortarScope;

    iget-object v0, v0, Lmortar/MortarScope;->children:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmortar/MortarScope;

    .line 50
    new-instance v2, Lmortar/MortarScopeDevHelper$MortarScopeNode;

    invoke-direct {v2, v1}, Lmortar/MortarScopeDevHelper$MortarScopeNode;-><init>(Lmortar/MortarScope;)V

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public getChildNodes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lmortar/MortarScopeDevHelper$Node;",
            ">;"
        }
    .end annotation

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 44
    invoke-direct {p0, v0}, Lmortar/MortarScopeDevHelper$MortarScopeNode;->addScopeChildren(Ljava/util/List;)V

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SCOPE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmortar/MortarScopeDevHelper$MortarScopeNode;->mortarScope:Lmortar/MortarScope;

    invoke-virtual {v1}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
