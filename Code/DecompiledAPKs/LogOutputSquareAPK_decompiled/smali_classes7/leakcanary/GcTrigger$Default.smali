.class public final Lleakcanary/GcTrigger$Default;
.super Ljava/lang/Object;
.source "GcTrigger.kt"

# interfaces
.implements Lleakcanary/GcTrigger;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lleakcanary/GcTrigger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Default"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0002J\u0008\u0010\u0005\u001a\u00020\u0004H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "Lleakcanary/GcTrigger$Default;",
        "Lleakcanary/GcTrigger;",
        "()V",
        "enqueueReferences",
        "",
        "runGc",
        "leakcanary-object-watcher"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lleakcanary/GcTrigger$Default;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    new-instance v0, Lleakcanary/GcTrigger$Default;

    invoke-direct {v0}, Lleakcanary/GcTrigger$Default;-><init>()V

    sput-object v0, Lleakcanary/GcTrigger$Default;->INSTANCE:Lleakcanary/GcTrigger$Default;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final enqueueReferences()V
    .locals 2

    const-wide/16 v0, 0x64

    .line 50
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    .line 52
    :catch_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public runGc()V
    .locals 1

    .line 40
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Ljava/lang/Runtime;->gc()V

    .line 42
    invoke-direct {p0}, Lleakcanary/GcTrigger$Default;->enqueueReferences()V

    .line 43
    invoke-static {}, Ljava/lang/System;->runFinalization()V

    return-void
.end method
