.class public Lmortar/MortarScope;
.super Ljava/lang/Object;
.source "MortarScope.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmortar/MortarScope$Builder;
    }
.end annotation


# static fields
.field public static final DIVIDER:Ljava/lang/String; = ">>>"

.field private static final MORTAR_SERVICE:Ljava/lang/String;


# instance fields
.field final children:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lmortar/MortarScope;",
            ">;"
        }
    .end annotation
.end field

.field private dead:Z

.field private final name:Ljava/lang/String;

.field final parent:Lmortar/MortarScope;

.field private final services:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final tearDowns:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    const-class v0, Lmortar/MortarScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmortar/MortarScope;->MORTAR_SERVICE:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lmortar/MortarScope;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lmortar/MortarScope;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lmortar/MortarScope;->children:Ljava/util/Map;

    .line 76
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmortar/MortarScope;->tearDowns:Ljava/util/Set;

    .line 82
    iput-object p2, p0, Lmortar/MortarScope;->parent:Lmortar/MortarScope;

    .line 83
    iput-object p1, p0, Lmortar/MortarScope;->name:Ljava/lang/String;

    .line 84
    iput-object p3, p0, Lmortar/MortarScope;->services:Ljava/util/Map;

    return-void
.end method

.method public static buildChild(Landroid/content/Context;)Lmortar/MortarScope$Builder;
    .locals 0

    .line 61
    invoke-static {p0}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object p0

    invoke-virtual {p0}, Lmortar/MortarScope;->buildChild()Lmortar/MortarScope$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static buildRootScope()Lmortar/MortarScope$Builder;
    .locals 2

    .line 69
    new-instance v0, Lmortar/MortarScope$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmortar/MortarScope$Builder;-><init>(Lmortar/MortarScope;)V

    return-object v0
.end method

.method public static findChild(Landroid/content/Context;Ljava/lang/String;)Lmortar/MortarScope;
    .locals 0

    .line 57
    invoke-static {p0}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object p0

    invoke-virtual {p0, p1}, Lmortar/MortarScope;->findChild(Ljava/lang/String;)Lmortar/MortarScope;

    move-result-object p0

    return-object p0
.end method

.method private findService(Ljava/lang/String;Z)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Z)TT;"
        }
    .end annotation

    .line 128
    sget-object v0, Lmortar/MortarScope;->MORTAR_SERVICE:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    :cond_0
    if-eqz p2, :cond_1

    .line 131
    invoke-virtual {p0}, Lmortar/MortarScope;->assertNotDead()V

    .line 134
    :cond_1
    iget-object v0, p0, Lmortar/MortarScope;->services:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    return-object v0

    .line 137
    :cond_2
    iget-object v0, p0, Lmortar/MortarScope;->parent:Lmortar/MortarScope;

    if-eqz v0, :cond_3

    .line 138
    invoke-direct {v0, p1, p2}, Lmortar/MortarScope;->findService(Ljava/lang/String;Z)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_3
    const/4 p1, 0x0

    return-object p1
.end method

.method public static getScope(Landroid/content/Context;)Lmortar/MortarScope;
    .locals 1

    .line 43
    sget-object v0, Lmortar/MortarScope;->MORTAR_SERVICE:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 51
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    sget-object v0, Lmortar/MortarScope;->MORTAR_SERVICE:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 53
    :cond_0
    check-cast v0, Lmortar/MortarScope;

    return-object v0
.end method

.method public static isDestroyed(Landroid/content/Context;)Z
    .locals 0

    .line 65
    invoke-static {p0}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object p0

    invoke-virtual {p0}, Lmortar/MortarScope;->isDestroyed()Z

    move-result p0

    return p0
.end method

.method private searchFromRoot(Lmortar/Scoped;)Lmortar/MortarScope;
    .locals 4

    move-object v0, p0

    .line 151
    :goto_0
    iget-object v1, v0, Lmortar/MortarScope;->parent:Lmortar/MortarScope;

    if-eqz v1, :cond_0

    move-object v0, v1

    goto :goto_0

    .line 156
    :cond_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 157
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 161
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmortar/MortarScope;

    .line 163
    iget-object v3, v2, Lmortar/MortarScope;->tearDowns:Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    return-object v2

    .line 168
    :cond_1
    iget-object v2, v2, Lmortar/MortarScope;->children:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 169
    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method assertNotDead()V
    .locals 3

    .line 264
    invoke-virtual {p0}, Lmortar/MortarScope;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Scope "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " was destroyed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public buildChild()Lmortar/MortarScope$Builder;
    .locals 1

    .line 213
    invoke-virtual {p0}, Lmortar/MortarScope;->assertNotDead()V

    .line 214
    new-instance v0, Lmortar/MortarScope$Builder;

    invoke-direct {v0, p0}, Lmortar/MortarScope$Builder;-><init>(Lmortar/MortarScope;)V

    return-object v0
.end method

.method public createContext(Landroid/content/Context;)Landroid/content/Context;
    .locals 1

    .line 221
    new-instance v0, Lmortar/MortarContextWrapper;

    invoke-direct {v0, p1, p0}, Lmortar/MortarContextWrapper;-><init>(Landroid/content/Context;Lmortar/MortarScope;)V

    return-object v0
.end method

.method public destroy()V
    .locals 4

    .line 235
    iget-boolean v0, p0, Lmortar/MortarScope;->dead:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 236
    iput-boolean v0, p0, Lmortar/MortarScope;->dead:Z

    .line 238
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lmortar/MortarScope;->children:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 239
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmortar/MortarScope;

    .line 240
    invoke-virtual {v1}, Lmortar/MortarScope;->destroy()V

    goto :goto_0

    .line 243
    :cond_1
    iget-object v0, p0, Lmortar/MortarScope;->tearDowns:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmortar/Scoped;

    .line 244
    invoke-interface {v1}, Lmortar/Scoped;->onExitScope()V

    goto :goto_1

    .line 246
    :cond_2
    iget-object v0, p0, Lmortar/MortarScope;->tearDowns:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 248
    iget-object v0, p0, Lmortar/MortarScope;->services:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 249
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 250
    iget-object v2, p0, Lmortar/MortarScope;->services:Ljava/util/Map;

    const-string v3, "Dead service"

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 252
    :cond_3
    iget-object v0, p0, Lmortar/MortarScope;->parent:Lmortar/MortarScope;

    if-eqz v0, :cond_4

    .line 253
    iget-object v0, v0, Lmortar/MortarScope;->children:Ljava/util/Map;

    invoke-virtual {p0}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    return-void
.end method

.method public findChild(Ljava/lang/String;)Lmortar/MortarScope;
    .locals 1

    .line 208
    invoke-virtual {p0}, Lmortar/MortarScope;->assertNotDead()V

    .line 209
    iget-object v0, p0, Lmortar/MortarScope;->children:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lmortar/MortarScope;

    return-object p1
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .line 92
    iget-object v0, p0, Lmortar/MortarScope;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 2

    .line 96
    iget-object v0, p0, Lmortar/MortarScope;->parent:Lmortar/MortarScope;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 97
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lmortar/MortarScope;->parent:Lmortar/MortarScope;

    invoke-virtual {v1}, Lmortar/MortarScope;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ">>>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 116
    invoke-direct {p0, p1, v0}, Lmortar/MortarScope;->findService(Ljava/lang/String;Z)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    return-object v1

    .line 118
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    const-string p1, "No service found named \"%s\""

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public hasService(Ljava/lang/String;)Z
    .locals 2

    .line 105
    sget-object v0, Lmortar/MortarScope;->MORTAR_SERVICE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, p1, v1}, Lmortar/MortarScope;->findService(Ljava/lang/String;Z)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public isDestroyed()Z
    .locals 1

    .line 226
    iget-boolean v0, p0, Lmortar/MortarScope;->dead:Z

    return v0
.end method

.method public register(Lmortar/Scoped;)V
    .locals 4

    .line 186
    invoke-virtual {p0}, Lmortar/MortarScope;->assertNotDead()V

    .line 187
    iget-object v0, p0, Lmortar/MortarScope;->tearDowns:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 192
    :cond_0
    invoke-direct {p0, p1}, Lmortar/MortarScope;->searchFromRoot(Lmortar/Scoped;)Lmortar/MortarScope;

    move-result-object v0

    if-nez v0, :cond_1

    .line 198
    iget-object v0, p0, Lmortar/MortarScope;->tearDowns:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 199
    invoke-interface {p1, p0}, Lmortar/Scoped;->onEnterScope(Lmortar/MortarScope;)V

    return-void

    .line 194
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    aput-object v0, v2, p1

    const-string p1, "\"%s\" is already registered within \"%s\"."

    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 258
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MortarScope@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
