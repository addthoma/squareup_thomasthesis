.class public Lmortar/MortarScopeDevHelper;
.super Ljava/lang/Object;
.source "MortarScopeDevHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmortar/MortarScopeDevHelper$NodeSorter;,
        Lmortar/MortarScopeDevHelper$MortarScopeNode;,
        Lmortar/MortarScopeDevHelper$Node;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is a helper class"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static appendLinePrefix(Ljava/lang/StringBuilder;IJ)V
    .locals 10

    add-int/lit8 v0, p1, -0x1

    const/16 v1, 0xa0

    .line 85
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-gt v2, v0, :cond_5

    const/16 v3, 0x20

    if-lez v2, :cond_0

    .line 88
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v4, 0x1

    shl-int v5, v4, v2

    int-to-long v5, v5

    and-long/2addr v5, p2

    const-wide/16 v7, 0x0

    cmp-long v9, v5, v7

    if-eqz v9, :cond_1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_3

    if-ne v2, v0, :cond_2

    const/16 v3, 0x60

    .line 93
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 95
    :cond_2
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_3
    if-ne v2, v0, :cond_4

    const/16 v3, 0x2b

    .line 99
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_4
    const/16 v3, 0x7c

    .line 101
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    if-lez p1, :cond_6

    const-string p1, "-"

    .line 106
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    return-void
.end method

.method private static getRootScope(Lmortar/MortarScope;)Lmortar/MortarScope;
    .locals 1

    .line 57
    :goto_0
    iget-object v0, p0, Lmortar/MortarScope;->parent:Lmortar/MortarScope;

    if-eqz v0, :cond_0

    .line 58
    iget-object p0, p0, Lmortar/MortarScope;->parent:Lmortar/MortarScope;

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method private static nodeHierarchyToString(Ljava/lang/StringBuilder;IJLmortar/MortarScopeDevHelper$Node;)V
    .locals 6

    .line 65
    invoke-static {p0, p1, p2, p3}, Lmortar/MortarScopeDevHelper;->appendLinePrefix(Ljava/lang/StringBuilder;IJ)V

    .line 66
    invoke-interface {p4}, Lmortar/MortarScopeDevHelper$Node;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 68
    invoke-interface {p4}, Lmortar/MortarScopeDevHelper$Node;->getChildNodes()Ljava/util/List;

    move-result-object p4

    .line 69
    new-instance v0, Lmortar/MortarScopeDevHelper$NodeSorter;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmortar/MortarScopeDevHelper$NodeSorter;-><init>(Lmortar/MortarScopeDevHelper$1;)V

    invoke-static {p4, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 71
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    .line 73
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p4

    const/4 v2, 0x0

    :goto_0
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmortar/MortarScopeDevHelper$Node;

    if-ne v2, v0, :cond_0

    shl-int v4, v1, p1

    int-to-long v4, v4

    or-long/2addr p2, v4

    :cond_0
    add-int/lit8 v4, p1, 0x1

    .line 77
    invoke-static {p0, v4, p2, p3, v3}, Lmortar/MortarScopeDevHelper;->nodeHierarchyToString(Ljava/lang/StringBuilder;IJLmortar/MortarScopeDevHelper$Node;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static scopeHierarchyToString(Lmortar/MortarScope;)Ljava/lang/String;
    .locals 4

    .line 17
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Mortar Hierarchy:\n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 18
    invoke-static {p0}, Lmortar/MortarScopeDevHelper;->getRootScope(Lmortar/MortarScope;)Lmortar/MortarScope;

    move-result-object p0

    .line 19
    new-instance v1, Lmortar/MortarScopeDevHelper$MortarScopeNode;

    invoke-direct {v1, p0}, Lmortar/MortarScopeDevHelper$MortarScopeNode;-><init>(Lmortar/MortarScope;)V

    const/4 p0, 0x0

    const-wide/16 v2, 0x0

    .line 20
    invoke-static {v0, p0, v2, v3, v1}, Lmortar/MortarScopeDevHelper;->nodeHierarchyToString(Ljava/lang/StringBuilder;IJLmortar/MortarScopeDevHelper$Node;)V

    .line 21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
