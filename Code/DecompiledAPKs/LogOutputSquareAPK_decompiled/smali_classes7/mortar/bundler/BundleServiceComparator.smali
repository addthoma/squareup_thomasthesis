.class Lmortar/bundler/BundleServiceComparator;
.super Ljava/lang/Object;
.source "BundleServiceComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lmortar/bundler/BundleService;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 6
    check-cast p1, Lmortar/bundler/BundleService;

    check-cast p2, Lmortar/bundler/BundleService;

    invoke-virtual {p0, p1, p2}, Lmortar/bundler/BundleServiceComparator;->compare(Lmortar/bundler/BundleService;Lmortar/bundler/BundleService;)I

    move-result p1

    return p1
.end method

.method public compare(Lmortar/bundler/BundleService;Lmortar/bundler/BundleService;)I
    .locals 5

    .line 8
    iget-object p1, p1, Lmortar/bundler/BundleService;->scope:Lmortar/MortarScope;

    invoke-virtual {p1}, Lmortar/MortarScope;->getPath()Ljava/lang/String;

    move-result-object p1

    const-string v0, ">>>"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 9
    iget-object p2, p2, Lmortar/bundler/BundleService;->scope:Lmortar/MortarScope;

    invoke-virtual {p2}, Lmortar/MortarScope;->getPath()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p2

    .line 11
    array-length v0, p1

    array-length v1, p2

    if-eq v0, v1, :cond_1

    .line 12
    array-length p1, p1

    array-length p2, p2

    if-ge p1, p2, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    return p1

    .line 15
    :cond_1
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_3

    .line 17
    aget-object v3, p1, v2

    aget-object v4, p2, v2

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_2

    return v3

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    return v1
.end method
