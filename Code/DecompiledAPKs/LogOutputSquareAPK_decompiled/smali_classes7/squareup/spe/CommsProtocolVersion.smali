.class public final Lsquareup/spe/CommsProtocolVersion;
.super Lcom/squareup/wire/Message;
.source "CommsProtocolVersion.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/spe/CommsProtocolVersion$ProtoAdapter_CommsProtocolVersion;,
        Lsquareup/spe/CommsProtocolVersion$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/spe/CommsProtocolVersion;",
        "Lsquareup/spe/CommsProtocolVersion$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/spe/CommsProtocolVersion;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APP:Ljava/lang/Integer;

.field public static final DEFAULT_EP:Ljava/lang/Integer;

.field public static final DEFAULT_TRANSPORT:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final app:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x3
    .end annotation
.end field

.field public final ep:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x1
    .end annotation
.end field

.field public final transport:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lsquareup/spe/CommsProtocolVersion$ProtoAdapter_CommsProtocolVersion;

    invoke-direct {v0}, Lsquareup/spe/CommsProtocolVersion$ProtoAdapter_CommsProtocolVersion;-><init>()V

    sput-object v0, Lsquareup/spe/CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 25
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lsquareup/spe/CommsProtocolVersion;->DEFAULT_EP:Ljava/lang/Integer;

    .line 27
    sput-object v0, Lsquareup/spe/CommsProtocolVersion;->DEFAULT_TRANSPORT:Ljava/lang/Integer;

    .line 29
    sput-object v0, Lsquareup/spe/CommsProtocolVersion;->DEFAULT_APP:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1

    .line 50
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lsquareup/spe/CommsProtocolVersion;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 55
    sget-object v0, Lsquareup/spe/CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 56
    iput-object p1, p0, Lsquareup/spe/CommsProtocolVersion;->ep:Ljava/lang/Integer;

    .line 57
    iput-object p2, p0, Lsquareup/spe/CommsProtocolVersion;->transport:Ljava/lang/Integer;

    .line 58
    iput-object p3, p0, Lsquareup/spe/CommsProtocolVersion;->app:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 74
    :cond_0
    instance-of v1, p1, Lsquareup/spe/CommsProtocolVersion;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 75
    :cond_1
    check-cast p1, Lsquareup/spe/CommsProtocolVersion;

    .line 76
    invoke-virtual {p0}, Lsquareup/spe/CommsProtocolVersion;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/spe/CommsProtocolVersion;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/CommsProtocolVersion;->ep:Ljava/lang/Integer;

    iget-object v3, p1, Lsquareup/spe/CommsProtocolVersion;->ep:Ljava/lang/Integer;

    .line 77
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/CommsProtocolVersion;->transport:Ljava/lang/Integer;

    iget-object v3, p1, Lsquareup/spe/CommsProtocolVersion;->transport:Ljava/lang/Integer;

    .line 78
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/CommsProtocolVersion;->app:Ljava/lang/Integer;

    iget-object p1, p1, Lsquareup/spe/CommsProtocolVersion;->app:Ljava/lang/Integer;

    .line 79
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 84
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 86
    invoke-virtual {p0}, Lsquareup/spe/CommsProtocolVersion;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 87
    iget-object v1, p0, Lsquareup/spe/CommsProtocolVersion;->ep:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 88
    iget-object v1, p0, Lsquareup/spe/CommsProtocolVersion;->transport:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 89
    iget-object v1, p0, Lsquareup/spe/CommsProtocolVersion;->app:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 90
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lsquareup/spe/CommsProtocolVersion;->newBuilder()Lsquareup/spe/CommsProtocolVersion$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/spe/CommsProtocolVersion$Builder;
    .locals 2

    .line 63
    new-instance v0, Lsquareup/spe/CommsProtocolVersion$Builder;

    invoke-direct {v0}, Lsquareup/spe/CommsProtocolVersion$Builder;-><init>()V

    .line 64
    iget-object v1, p0, Lsquareup/spe/CommsProtocolVersion;->ep:Ljava/lang/Integer;

    iput-object v1, v0, Lsquareup/spe/CommsProtocolVersion$Builder;->ep:Ljava/lang/Integer;

    .line 65
    iget-object v1, p0, Lsquareup/spe/CommsProtocolVersion;->transport:Ljava/lang/Integer;

    iput-object v1, v0, Lsquareup/spe/CommsProtocolVersion$Builder;->transport:Ljava/lang/Integer;

    .line 66
    iget-object v1, p0, Lsquareup/spe/CommsProtocolVersion;->app:Ljava/lang/Integer;

    iput-object v1, v0, Lsquareup/spe/CommsProtocolVersion$Builder;->app:Ljava/lang/Integer;

    .line 67
    invoke-virtual {p0}, Lsquareup/spe/CommsProtocolVersion;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/spe/CommsProtocolVersion$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 98
    iget-object v1, p0, Lsquareup/spe/CommsProtocolVersion;->ep:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", ep="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/CommsProtocolVersion;->ep:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 99
    :cond_0
    iget-object v1, p0, Lsquareup/spe/CommsProtocolVersion;->transport:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", transport="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/CommsProtocolVersion;->transport:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 100
    :cond_1
    iget-object v1, p0, Lsquareup/spe/CommsProtocolVersion;->app:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", app="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/CommsProtocolVersion;->app:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CommsProtocolVersion{"

    .line 101
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
