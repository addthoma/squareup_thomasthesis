.class final Lsquareup/spe/STM32F2Manifest$ProtoAdapter_STM32F2Manifest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "STM32F2Manifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/STM32F2Manifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_STM32F2Manifest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/spe/STM32F2Manifest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 258
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/spe/STM32F2Manifest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 256
    invoke-virtual {p0, p1}, Lsquareup/spe/STM32F2Manifest$ProtoAdapter_STM32F2Manifest;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/spe/STM32F2Manifest;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/spe/STM32F2Manifest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 287
    new-instance v0, Lsquareup/spe/STM32F2Manifest$Builder;

    invoke-direct {v0}, Lsquareup/spe/STM32F2Manifest$Builder;-><init>()V

    .line 288
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 289
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 313
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 306
    :pswitch_0
    :try_start_0
    sget-object v4, Lsquareup/spe/UnitConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsquareup/spe/UnitConfiguration;

    invoke-virtual {v0, v4}, Lsquareup/spe/STM32F2Manifest$Builder;->configuration(Lsquareup/spe/UnitConfiguration;)Lsquareup/spe/STM32F2Manifest$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 308
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lsquareup/spe/STM32F2Manifest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 303
    :pswitch_1
    sget-object v3, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/STM32F2Manifest$Builder;->fw_b(Lsquareup/spe/AssetManifest;)Lsquareup/spe/STM32F2Manifest$Builder;

    goto :goto_0

    .line 302
    :pswitch_2
    sget-object v3, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/STM32F2Manifest$Builder;->fw_a(Lsquareup/spe/AssetManifest;)Lsquareup/spe/STM32F2Manifest$Builder;

    goto :goto_0

    .line 301
    :pswitch_3
    sget-object v3, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/STM32F2Manifest$Builder;->bootloader(Lsquareup/spe/AssetManifest;)Lsquareup/spe/STM32F2Manifest$Builder;

    goto :goto_0

    .line 300
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lsquareup/spe/STM32F2Manifest$Builder;->signer_fingerprint(Lokio/ByteString;)Lsquareup/spe/STM32F2Manifest$Builder;

    goto :goto_0

    .line 299
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lsquareup/spe/STM32F2Manifest$Builder;->chipid(Lokio/ByteString;)Lsquareup/spe/STM32F2Manifest$Builder;

    goto :goto_0

    .line 293
    :pswitch_6
    :try_start_1
    sget-object v4, Lsquareup/spe/AssetTypeV2;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsquareup/spe/AssetTypeV2;

    invoke-virtual {v0, v4}, Lsquareup/spe/STM32F2Manifest$Builder;->running_slot(Lsquareup/spe/AssetTypeV2;)Lsquareup/spe/STM32F2Manifest$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 295
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lsquareup/spe/STM32F2Manifest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 317
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/spe/STM32F2Manifest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 318
    invoke-virtual {v0}, Lsquareup/spe/STM32F2Manifest$Builder;->build()Lsquareup/spe/STM32F2Manifest;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 256
    check-cast p2, Lsquareup/spe/STM32F2Manifest;

    invoke-virtual {p0, p1, p2}, Lsquareup/spe/STM32F2Manifest$ProtoAdapter_STM32F2Manifest;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/spe/STM32F2Manifest;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/spe/STM32F2Manifest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 275
    sget-object v0, Lsquareup/spe/AssetTypeV2;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/STM32F2Manifest;->running_slot:Lsquareup/spe/AssetTypeV2;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 276
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/STM32F2Manifest;->chipid:Lokio/ByteString;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 277
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/STM32F2Manifest;->signer_fingerprint:Lokio/ByteString;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 278
    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/STM32F2Manifest;->bootloader:Lsquareup/spe/AssetManifest;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 279
    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/STM32F2Manifest;->fw_a:Lsquareup/spe/AssetManifest;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 280
    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/STM32F2Manifest;->fw_b:Lsquareup/spe/AssetManifest;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 281
    sget-object v0, Lsquareup/spe/UnitConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/STM32F2Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 282
    invoke-virtual {p2}, Lsquareup/spe/STM32F2Manifest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 256
    check-cast p1, Lsquareup/spe/STM32F2Manifest;

    invoke-virtual {p0, p1}, Lsquareup/spe/STM32F2Manifest$ProtoAdapter_STM32F2Manifest;->encodedSize(Lsquareup/spe/STM32F2Manifest;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/spe/STM32F2Manifest;)I
    .locals 4

    .line 263
    sget-object v0, Lsquareup/spe/AssetTypeV2;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/STM32F2Manifest;->running_slot:Lsquareup/spe/AssetTypeV2;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/STM32F2Manifest;->chipid:Lokio/ByteString;

    const/4 v3, 0x2

    .line 264
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/STM32F2Manifest;->signer_fingerprint:Lokio/ByteString;

    const/4 v3, 0x3

    .line 265
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/STM32F2Manifest;->bootloader:Lsquareup/spe/AssetManifest;

    const/4 v3, 0x4

    .line 266
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/STM32F2Manifest;->fw_a:Lsquareup/spe/AssetManifest;

    const/4 v3, 0x5

    .line 267
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/STM32F2Manifest;->fw_b:Lsquareup/spe/AssetManifest;

    const/4 v3, 0x6

    .line 268
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/UnitConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/STM32F2Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    const/4 v3, 0x7

    .line 269
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 270
    invoke-virtual {p1}, Lsquareup/spe/STM32F2Manifest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 256
    check-cast p1, Lsquareup/spe/STM32F2Manifest;

    invoke-virtual {p0, p1}, Lsquareup/spe/STM32F2Manifest$ProtoAdapter_STM32F2Manifest;->redact(Lsquareup/spe/STM32F2Manifest;)Lsquareup/spe/STM32F2Manifest;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/spe/STM32F2Manifest;)Lsquareup/spe/STM32F2Manifest;
    .locals 2

    .line 323
    invoke-virtual {p1}, Lsquareup/spe/STM32F2Manifest;->newBuilder()Lsquareup/spe/STM32F2Manifest$Builder;

    move-result-object p1

    .line 324
    iget-object v0, p1, Lsquareup/spe/STM32F2Manifest$Builder;->bootloader:Lsquareup/spe/AssetManifest;

    if-eqz v0, :cond_0

    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/STM32F2Manifest$Builder;->bootloader:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/AssetManifest;

    iput-object v0, p1, Lsquareup/spe/STM32F2Manifest$Builder;->bootloader:Lsquareup/spe/AssetManifest;

    .line 325
    :cond_0
    iget-object v0, p1, Lsquareup/spe/STM32F2Manifest$Builder;->fw_a:Lsquareup/spe/AssetManifest;

    if-eqz v0, :cond_1

    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/STM32F2Manifest$Builder;->fw_a:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/AssetManifest;

    iput-object v0, p1, Lsquareup/spe/STM32F2Manifest$Builder;->fw_a:Lsquareup/spe/AssetManifest;

    .line 326
    :cond_1
    iget-object v0, p1, Lsquareup/spe/STM32F2Manifest$Builder;->fw_b:Lsquareup/spe/AssetManifest;

    if-eqz v0, :cond_2

    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/STM32F2Manifest$Builder;->fw_b:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/AssetManifest;

    iput-object v0, p1, Lsquareup/spe/STM32F2Manifest$Builder;->fw_b:Lsquareup/spe/AssetManifest;

    .line 327
    :cond_2
    invoke-virtual {p1}, Lsquareup/spe/STM32F2Manifest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 328
    invoke-virtual {p1}, Lsquareup/spe/STM32F2Manifest$Builder;->build()Lsquareup/spe/STM32F2Manifest;

    move-result-object p1

    return-object p1
.end method
