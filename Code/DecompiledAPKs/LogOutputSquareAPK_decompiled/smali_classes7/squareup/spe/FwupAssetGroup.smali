.class public final Lsquareup/spe/FwupAssetGroup;
.super Lcom/squareup/wire/Message;
.source "FwupAssetGroup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/spe/FwupAssetGroup$ProtoAdapter_FwupAssetGroup;,
        Lsquareup/spe/FwupAssetGroup$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/spe/FwupAssetGroup;",
        "Lsquareup/spe/FwupAssetGroup$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/spe/FwupAssetGroup;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final crq_gt4_fw:Lsquareup/spe/FwupAssetDescriptor;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.FwupAssetDescriptor#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final fpga:Lsquareup/spe/FwupAssetDescriptor;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.FwupAssetDescriptor#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final k21_fw_a:Lsquareup/spe/FwupAssetDescriptor;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.FwupAssetDescriptor#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final k21_fw_b:Lsquareup/spe/FwupAssetDescriptor;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.FwupAssetDescriptor#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final k400_cpu0_fw:Lsquareup/spe/FwupAssetDescriptor;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.FwupAssetDescriptor#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final k400_cpu1_fw:Lsquareup/spe/FwupAssetDescriptor;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.FwupAssetDescriptor#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final k450_cpu0_fw_a:Lsquareup/spe/FwupAssetDescriptor;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.FwupAssetDescriptor#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final k450_cpu0_fw_b:Lsquareup/spe/FwupAssetDescriptor;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.FwupAssetDescriptor#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final k450_cpu1_fw_a:Lsquareup/spe/FwupAssetDescriptor;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.FwupAssetDescriptor#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final k450_cpu1_fw_b:Lsquareup/spe/FwupAssetDescriptor;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.FwupAssetDescriptor#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final tms_capk:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.FwupAssetDescriptor#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/spe/FwupAssetDescriptor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lsquareup/spe/FwupAssetGroup$ProtoAdapter_FwupAssetGroup;

    invoke-direct {v0}, Lsquareup/spe/FwupAssetGroup$ProtoAdapter_FwupAssetGroup;-><init>()V

    sput-object v0, Lsquareup/spe/FwupAssetGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Ljava/util/List;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Ljava/util/List<",
            "Lsquareup/spe/FwupAssetDescriptor;",
            ">;",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Lsquareup/spe/FwupAssetDescriptor;",
            ")V"
        }
    .end annotation

    .line 100
    sget-object v12, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, Lsquareup/spe/FwupAssetGroup;-><init>(Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Ljava/util/List;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Ljava/util/List;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lsquareup/spe/FwupAssetDescriptor;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Ljava/util/List<",
            "Lsquareup/spe/FwupAssetDescriptor;",
            ">;",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Lsquareup/spe/FwupAssetDescriptor;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 109
    sget-object v0, Lsquareup/spe/FwupAssetGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p12}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 110
    iput-object p1, p0, Lsquareup/spe/FwupAssetGroup;->k21_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    .line 111
    iput-object p2, p0, Lsquareup/spe/FwupAssetGroup;->k21_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    .line 112
    iput-object p3, p0, Lsquareup/spe/FwupAssetGroup;->k400_cpu0_fw:Lsquareup/spe/FwupAssetDescriptor;

    .line 113
    iput-object p4, p0, Lsquareup/spe/FwupAssetGroup;->k400_cpu1_fw:Lsquareup/spe/FwupAssetDescriptor;

    .line 114
    iput-object p5, p0, Lsquareup/spe/FwupAssetGroup;->fpga:Lsquareup/spe/FwupAssetDescriptor;

    const-string p1, "tms_capk"

    .line 115
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lsquareup/spe/FwupAssetGroup;->tms_capk:Ljava/util/List;

    .line 116
    iput-object p7, p0, Lsquareup/spe/FwupAssetGroup;->crq_gt4_fw:Lsquareup/spe/FwupAssetDescriptor;

    .line 117
    iput-object p8, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu0_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    .line 118
    iput-object p9, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu0_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    .line 119
    iput-object p10, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu1_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    .line 120
    iput-object p11, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu1_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 144
    :cond_0
    instance-of v1, p1, Lsquareup/spe/FwupAssetGroup;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 145
    :cond_1
    check-cast p1, Lsquareup/spe/FwupAssetGroup;

    .line 146
    invoke-virtual {p0}, Lsquareup/spe/FwupAssetGroup;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/spe/FwupAssetGroup;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k21_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    iget-object v3, p1, Lsquareup/spe/FwupAssetGroup;->k21_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    .line 147
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k21_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    iget-object v3, p1, Lsquareup/spe/FwupAssetGroup;->k21_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    .line 148
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k400_cpu0_fw:Lsquareup/spe/FwupAssetDescriptor;

    iget-object v3, p1, Lsquareup/spe/FwupAssetGroup;->k400_cpu0_fw:Lsquareup/spe/FwupAssetDescriptor;

    .line 149
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k400_cpu1_fw:Lsquareup/spe/FwupAssetDescriptor;

    iget-object v3, p1, Lsquareup/spe/FwupAssetGroup;->k400_cpu1_fw:Lsquareup/spe/FwupAssetDescriptor;

    .line 150
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->fpga:Lsquareup/spe/FwupAssetDescriptor;

    iget-object v3, p1, Lsquareup/spe/FwupAssetGroup;->fpga:Lsquareup/spe/FwupAssetDescriptor;

    .line 151
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->tms_capk:Ljava/util/List;

    iget-object v3, p1, Lsquareup/spe/FwupAssetGroup;->tms_capk:Ljava/util/List;

    .line 152
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->crq_gt4_fw:Lsquareup/spe/FwupAssetDescriptor;

    iget-object v3, p1, Lsquareup/spe/FwupAssetGroup;->crq_gt4_fw:Lsquareup/spe/FwupAssetDescriptor;

    .line 153
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu0_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    iget-object v3, p1, Lsquareup/spe/FwupAssetGroup;->k450_cpu0_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    .line 154
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu0_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    iget-object v3, p1, Lsquareup/spe/FwupAssetGroup;->k450_cpu0_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    .line 155
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu1_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    iget-object v3, p1, Lsquareup/spe/FwupAssetGroup;->k450_cpu1_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    .line 156
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu1_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    iget-object p1, p1, Lsquareup/spe/FwupAssetGroup;->k450_cpu1_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    .line 157
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 162
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 164
    invoke-virtual {p0}, Lsquareup/spe/FwupAssetGroup;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 165
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k21_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lsquareup/spe/FwupAssetDescriptor;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 166
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k21_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lsquareup/spe/FwupAssetDescriptor;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 167
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k400_cpu0_fw:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lsquareup/spe/FwupAssetDescriptor;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 168
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k400_cpu1_fw:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lsquareup/spe/FwupAssetDescriptor;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 169
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->fpga:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lsquareup/spe/FwupAssetDescriptor;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 170
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->tms_capk:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 171
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->crq_gt4_fw:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lsquareup/spe/FwupAssetDescriptor;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu0_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lsquareup/spe/FwupAssetDescriptor;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu0_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lsquareup/spe/FwupAssetDescriptor;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu1_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lsquareup/spe/FwupAssetDescriptor;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu1_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lsquareup/spe/FwupAssetDescriptor;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 176
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lsquareup/spe/FwupAssetGroup;->newBuilder()Lsquareup/spe/FwupAssetGroup$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/spe/FwupAssetGroup$Builder;
    .locals 2

    .line 125
    new-instance v0, Lsquareup/spe/FwupAssetGroup$Builder;

    invoke-direct {v0}, Lsquareup/spe/FwupAssetGroup$Builder;-><init>()V

    .line 126
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k21_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    iput-object v1, v0, Lsquareup/spe/FwupAssetGroup$Builder;->k21_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    .line 127
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k21_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    iput-object v1, v0, Lsquareup/spe/FwupAssetGroup$Builder;->k21_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    .line 128
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k400_cpu0_fw:Lsquareup/spe/FwupAssetDescriptor;

    iput-object v1, v0, Lsquareup/spe/FwupAssetGroup$Builder;->k400_cpu0_fw:Lsquareup/spe/FwupAssetDescriptor;

    .line 129
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k400_cpu1_fw:Lsquareup/spe/FwupAssetDescriptor;

    iput-object v1, v0, Lsquareup/spe/FwupAssetGroup$Builder;->k400_cpu1_fw:Lsquareup/spe/FwupAssetDescriptor;

    .line 130
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->fpga:Lsquareup/spe/FwupAssetDescriptor;

    iput-object v1, v0, Lsquareup/spe/FwupAssetGroup$Builder;->fpga:Lsquareup/spe/FwupAssetDescriptor;

    .line 131
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->tms_capk:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lsquareup/spe/FwupAssetGroup$Builder;->tms_capk:Ljava/util/List;

    .line 132
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->crq_gt4_fw:Lsquareup/spe/FwupAssetDescriptor;

    iput-object v1, v0, Lsquareup/spe/FwupAssetGroup$Builder;->crq_gt4_fw:Lsquareup/spe/FwupAssetDescriptor;

    .line 133
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu0_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    iput-object v1, v0, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu0_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    .line 134
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu0_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    iput-object v1, v0, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu0_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    .line 135
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu1_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    iput-object v1, v0, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu1_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    .line 136
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu1_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    iput-object v1, v0, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu1_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    .line 137
    invoke-virtual {p0}, Lsquareup/spe/FwupAssetGroup;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/spe/FwupAssetGroup$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 184
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k21_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_0

    const-string v1, ", k21_fw_a="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k21_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 185
    :cond_0
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k21_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_1

    const-string v1, ", k21_fw_b="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k21_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 186
    :cond_1
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k400_cpu0_fw:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_2

    const-string v1, ", k400_cpu0_fw="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k400_cpu0_fw:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 187
    :cond_2
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k400_cpu1_fw:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_3

    const-string v1, ", k400_cpu1_fw="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k400_cpu1_fw:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 188
    :cond_3
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->fpga:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_4

    const-string v1, ", fpga="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->fpga:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 189
    :cond_4
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->tms_capk:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", tms_capk="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->tms_capk:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 190
    :cond_5
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->crq_gt4_fw:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_6

    const-string v1, ", crq_gt4_fw="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->crq_gt4_fw:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 191
    :cond_6
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu0_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_7

    const-string v1, ", k450_cpu0_fw_a="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu0_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 192
    :cond_7
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu0_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_8

    const-string v1, ", k450_cpu0_fw_b="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu0_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 193
    :cond_8
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu1_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_9

    const-string v1, ", k450_cpu1_fw_a="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu1_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 194
    :cond_9
    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu1_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v1, :cond_a

    const-string v1, ", k450_cpu1_fw_b="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/FwupAssetGroup;->k450_cpu1_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FwupAssetGroup{"

    .line 195
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
