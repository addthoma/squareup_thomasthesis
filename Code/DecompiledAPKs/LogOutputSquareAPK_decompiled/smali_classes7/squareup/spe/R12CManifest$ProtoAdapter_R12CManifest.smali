.class final Lsquareup/spe/R12CManifest$ProtoAdapter_R12CManifest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "R12CManifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/R12CManifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_R12CManifest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/spe/R12CManifest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 192
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/spe/R12CManifest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 190
    invoke-virtual {p0, p1}, Lsquareup/spe/R12CManifest$ProtoAdapter_R12CManifest;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/spe/R12CManifest;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/spe/R12CManifest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 219
    new-instance v0, Lsquareup/spe/R12CManifest$Builder;

    invoke-direct {v0}, Lsquareup/spe/R12CManifest$Builder;-><init>()V

    .line 220
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 221
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 230
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 228
    :pswitch_0
    sget-object v3, Lsquareup/spe/CRQGT4Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/CRQGT4Manifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/R12CManifest$Builder;->crq_gt4_manifest(Lsquareup/spe/CRQGT4Manifest;)Lsquareup/spe/R12CManifest$Builder;

    goto :goto_0

    .line 227
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lsquareup/spe/R12CManifest$Builder;->pts_version(Ljava/lang/Integer;)Lsquareup/spe/R12CManifest$Builder;

    goto :goto_0

    .line 226
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lsquareup/spe/R12CManifest$Builder;->max_compression_version(Ljava/lang/Integer;)Lsquareup/spe/R12CManifest$Builder;

    goto :goto_0

    .line 225
    :pswitch_3
    sget-object v3, Lsquareup/spe/TICC2640Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/TICC2640Manifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/R12CManifest$Builder;->ble_manifest(Lsquareup/spe/TICC2640Manifest;)Lsquareup/spe/R12CManifest$Builder;

    goto :goto_0

    .line 224
    :pswitch_4
    sget-object v3, Lsquareup/spe/K450Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/K450Manifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/R12CManifest$Builder;->k450_manifest(Lsquareup/spe/K450Manifest;)Lsquareup/spe/R12CManifest$Builder;

    goto :goto_0

    .line 223
    :pswitch_5
    sget-object v3, Lsquareup/spe/CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/CommsProtocolVersion;

    invoke-virtual {v0, v3}, Lsquareup/spe/R12CManifest$Builder;->comms_protocol_version(Lsquareup/spe/CommsProtocolVersion;)Lsquareup/spe/R12CManifest$Builder;

    goto :goto_0

    .line 234
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/spe/R12CManifest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 235
    invoke-virtual {v0}, Lsquareup/spe/R12CManifest$Builder;->build()Lsquareup/spe/R12CManifest;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 190
    check-cast p2, Lsquareup/spe/R12CManifest;

    invoke-virtual {p0, p1, p2}, Lsquareup/spe/R12CManifest$ProtoAdapter_R12CManifest;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/spe/R12CManifest;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/spe/R12CManifest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 208
    sget-object v0, Lsquareup/spe/CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/R12CManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    const/16 v2, 0x191

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 209
    sget-object v0, Lsquareup/spe/K450Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/R12CManifest;->k450_manifest:Lsquareup/spe/K450Manifest;

    const/16 v2, 0x192

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 210
    sget-object v0, Lsquareup/spe/TICC2640Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/R12CManifest;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    const/16 v2, 0x193

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 211
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/R12CManifest;->max_compression_version:Ljava/lang/Integer;

    const/16 v2, 0x194

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 212
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/R12CManifest;->pts_version:Ljava/lang/Integer;

    const/16 v2, 0x195

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 213
    sget-object v0, Lsquareup/spe/CRQGT4Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/R12CManifest;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    const/16 v2, 0x196

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 214
    invoke-virtual {p2}, Lsquareup/spe/R12CManifest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 190
    check-cast p1, Lsquareup/spe/R12CManifest;

    invoke-virtual {p0, p1}, Lsquareup/spe/R12CManifest$ProtoAdapter_R12CManifest;->encodedSize(Lsquareup/spe/R12CManifest;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/spe/R12CManifest;)I
    .locals 4

    .line 197
    sget-object v0, Lsquareup/spe/CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/R12CManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    const/16 v2, 0x191

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lsquareup/spe/K450Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/R12CManifest;->k450_manifest:Lsquareup/spe/K450Manifest;

    const/16 v3, 0x192

    .line 198
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/TICC2640Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/R12CManifest;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    const/16 v3, 0x193

    .line 199
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/R12CManifest;->max_compression_version:Ljava/lang/Integer;

    const/16 v3, 0x194

    .line 200
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/R12CManifest;->pts_version:Ljava/lang/Integer;

    const/16 v3, 0x195

    .line 201
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/CRQGT4Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/R12CManifest;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    const/16 v3, 0x196

    .line 202
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 203
    invoke-virtual {p1}, Lsquareup/spe/R12CManifest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 190
    check-cast p1, Lsquareup/spe/R12CManifest;

    invoke-virtual {p0, p1}, Lsquareup/spe/R12CManifest$ProtoAdapter_R12CManifest;->redact(Lsquareup/spe/R12CManifest;)Lsquareup/spe/R12CManifest;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/spe/R12CManifest;)Lsquareup/spe/R12CManifest;
    .locals 2

    .line 240
    invoke-virtual {p1}, Lsquareup/spe/R12CManifest;->newBuilder()Lsquareup/spe/R12CManifest$Builder;

    move-result-object p1

    .line 241
    iget-object v0, p1, Lsquareup/spe/R12CManifest$Builder;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    if-eqz v0, :cond_0

    sget-object v0, Lsquareup/spe/CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/R12CManifest$Builder;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/CommsProtocolVersion;

    iput-object v0, p1, Lsquareup/spe/R12CManifest$Builder;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    .line 242
    :cond_0
    iget-object v0, p1, Lsquareup/spe/R12CManifest$Builder;->k450_manifest:Lsquareup/spe/K450Manifest;

    if-eqz v0, :cond_1

    sget-object v0, Lsquareup/spe/K450Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/R12CManifest$Builder;->k450_manifest:Lsquareup/spe/K450Manifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/K450Manifest;

    iput-object v0, p1, Lsquareup/spe/R12CManifest$Builder;->k450_manifest:Lsquareup/spe/K450Manifest;

    .line 243
    :cond_1
    iget-object v0, p1, Lsquareup/spe/R12CManifest$Builder;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    if-eqz v0, :cond_2

    sget-object v0, Lsquareup/spe/TICC2640Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/R12CManifest$Builder;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/TICC2640Manifest;

    iput-object v0, p1, Lsquareup/spe/R12CManifest$Builder;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    .line 244
    :cond_2
    iget-object v0, p1, Lsquareup/spe/R12CManifest$Builder;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    if-eqz v0, :cond_3

    sget-object v0, Lsquareup/spe/CRQGT4Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/R12CManifest$Builder;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/CRQGT4Manifest;

    iput-object v0, p1, Lsquareup/spe/R12CManifest$Builder;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    .line 245
    :cond_3
    invoke-virtual {p1}, Lsquareup/spe/R12CManifest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 246
    invoke-virtual {p1}, Lsquareup/spe/R12CManifest$Builder;->build()Lsquareup/spe/R12CManifest;

    move-result-object p1

    return-object p1
.end method
