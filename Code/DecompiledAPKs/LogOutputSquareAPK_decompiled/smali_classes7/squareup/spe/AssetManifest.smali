.class public final Lsquareup/spe/AssetManifest;
.super Lcom/squareup/wire/Message;
.source "AssetManifest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/spe/AssetManifest$ProtoAdapter_AssetManifest;,
        Lsquareup/spe/AssetManifest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/spe/AssetManifest;",
        "Lsquareup/spe/AssetManifest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/spe/AssetManifest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_GIT_HASH:Lokio/ByteString;

.field public static final DEFAULT_IMAGE_HASH:Lokio/ByteString;

.field public static final DEFAULT_PERCENT_COMPLETE:Ljava/lang/Integer;

.field public static final DEFAULT_PRODUCT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_VALID:Ljava/lang/Boolean;

.field public static final DEFAULT_VERSION:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final git_hash:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x6
    .end annotation
.end field

.field public final image_hash:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x5
    .end annotation
.end field

.field public final percent_complete:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x4
    .end annotation
.end field

.field public final product_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final valid:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 25
    new-instance v0, Lsquareup/spe/AssetManifest$ProtoAdapter_AssetManifest;

    invoke-direct {v0}, Lsquareup/spe/AssetManifest$ProtoAdapter_AssetManifest;-><init>()V

    sput-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lsquareup/spe/AssetManifest;->DEFAULT_VERSION:Ljava/lang/Integer;

    .line 31
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lsquareup/spe/AssetManifest;->DEFAULT_VALID:Ljava/lang/Boolean;

    .line 35
    sput-object v1, Lsquareup/spe/AssetManifest;->DEFAULT_PERCENT_COMPLETE:Ljava/lang/Integer;

    .line 37
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lsquareup/spe/AssetManifest;->DEFAULT_IMAGE_HASH:Lokio/ByteString;

    .line 39
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lsquareup/spe/AssetManifest;->DEFAULT_GIT_HASH:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;Lokio/ByteString;)V
    .locals 8

    .line 85
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lsquareup/spe/AssetManifest;-><init>(Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;)V
    .locals 1

    .line 90
    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 91
    iput-object p1, p0, Lsquareup/spe/AssetManifest;->version:Ljava/lang/Integer;

    .line 92
    iput-object p2, p0, Lsquareup/spe/AssetManifest;->valid:Ljava/lang/Boolean;

    .line 93
    iput-object p3, p0, Lsquareup/spe/AssetManifest;->product_id:Ljava/lang/String;

    .line 94
    iput-object p4, p0, Lsquareup/spe/AssetManifest;->percent_complete:Ljava/lang/Integer;

    .line 95
    iput-object p5, p0, Lsquareup/spe/AssetManifest;->image_hash:Lokio/ByteString;

    .line 96
    iput-object p6, p0, Lsquareup/spe/AssetManifest;->git_hash:Lokio/ByteString;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 115
    :cond_0
    instance-of v1, p1, Lsquareup/spe/AssetManifest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 116
    :cond_1
    check-cast p1, Lsquareup/spe/AssetManifest;

    .line 117
    invoke-virtual {p0}, Lsquareup/spe/AssetManifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/spe/AssetManifest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/AssetManifest;->version:Ljava/lang/Integer;

    iget-object v3, p1, Lsquareup/spe/AssetManifest;->version:Ljava/lang/Integer;

    .line 118
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/AssetManifest;->valid:Ljava/lang/Boolean;

    iget-object v3, p1, Lsquareup/spe/AssetManifest;->valid:Ljava/lang/Boolean;

    .line 119
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/AssetManifest;->product_id:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/spe/AssetManifest;->product_id:Ljava/lang/String;

    .line 120
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/AssetManifest;->percent_complete:Ljava/lang/Integer;

    iget-object v3, p1, Lsquareup/spe/AssetManifest;->percent_complete:Ljava/lang/Integer;

    .line 121
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/AssetManifest;->image_hash:Lokio/ByteString;

    iget-object v3, p1, Lsquareup/spe/AssetManifest;->image_hash:Lokio/ByteString;

    .line 122
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/AssetManifest;->git_hash:Lokio/ByteString;

    iget-object p1, p1, Lsquareup/spe/AssetManifest;->git_hash:Lokio/ByteString;

    .line 123
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 128
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 130
    invoke-virtual {p0}, Lsquareup/spe/AssetManifest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 131
    iget-object v1, p0, Lsquareup/spe/AssetManifest;->version:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lsquareup/spe/AssetManifest;->valid:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lsquareup/spe/AssetManifest;->product_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 134
    iget-object v1, p0, Lsquareup/spe/AssetManifest;->percent_complete:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 135
    iget-object v1, p0, Lsquareup/spe/AssetManifest;->image_hash:Lokio/ByteString;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 136
    iget-object v1, p0, Lsquareup/spe/AssetManifest;->git_hash:Lokio/ByteString;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 137
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lsquareup/spe/AssetManifest;->newBuilder()Lsquareup/spe/AssetManifest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/spe/AssetManifest$Builder;
    .locals 2

    .line 101
    new-instance v0, Lsquareup/spe/AssetManifest$Builder;

    invoke-direct {v0}, Lsquareup/spe/AssetManifest$Builder;-><init>()V

    .line 102
    iget-object v1, p0, Lsquareup/spe/AssetManifest;->version:Ljava/lang/Integer;

    iput-object v1, v0, Lsquareup/spe/AssetManifest$Builder;->version:Ljava/lang/Integer;

    .line 103
    iget-object v1, p0, Lsquareup/spe/AssetManifest;->valid:Ljava/lang/Boolean;

    iput-object v1, v0, Lsquareup/spe/AssetManifest$Builder;->valid:Ljava/lang/Boolean;

    .line 104
    iget-object v1, p0, Lsquareup/spe/AssetManifest;->product_id:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/spe/AssetManifest$Builder;->product_id:Ljava/lang/String;

    .line 105
    iget-object v1, p0, Lsquareup/spe/AssetManifest;->percent_complete:Ljava/lang/Integer;

    iput-object v1, v0, Lsquareup/spe/AssetManifest$Builder;->percent_complete:Ljava/lang/Integer;

    .line 106
    iget-object v1, p0, Lsquareup/spe/AssetManifest;->image_hash:Lokio/ByteString;

    iput-object v1, v0, Lsquareup/spe/AssetManifest$Builder;->image_hash:Lokio/ByteString;

    .line 107
    iget-object v1, p0, Lsquareup/spe/AssetManifest;->git_hash:Lokio/ByteString;

    iput-object v1, v0, Lsquareup/spe/AssetManifest$Builder;->git_hash:Lokio/ByteString;

    .line 108
    invoke-virtual {p0}, Lsquareup/spe/AssetManifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/spe/AssetManifest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 145
    iget-object v1, p0, Lsquareup/spe/AssetManifest;->version:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/AssetManifest;->version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 146
    :cond_0
    iget-object v1, p0, Lsquareup/spe/AssetManifest;->valid:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", valid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/AssetManifest;->valid:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 147
    :cond_1
    iget-object v1, p0, Lsquareup/spe/AssetManifest;->product_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", product_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/AssetManifest;->product_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    :cond_2
    iget-object v1, p0, Lsquareup/spe/AssetManifest;->percent_complete:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", percent_complete="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/AssetManifest;->percent_complete:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 149
    :cond_3
    iget-object v1, p0, Lsquareup/spe/AssetManifest;->image_hash:Lokio/ByteString;

    if-eqz v1, :cond_4

    const-string v1, ", image_hash="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/AssetManifest;->image_hash:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 150
    :cond_4
    iget-object v1, p0, Lsquareup/spe/AssetManifest;->git_hash:Lokio/ByteString;

    if-eqz v1, :cond_5

    const-string v1, ", git_hash="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/AssetManifest;->git_hash:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AssetManifest{"

    .line 151
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
