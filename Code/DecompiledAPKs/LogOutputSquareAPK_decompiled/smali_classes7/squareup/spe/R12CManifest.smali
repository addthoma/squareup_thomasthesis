.class public final Lsquareup/spe/R12CManifest;
.super Lcom/squareup/wire/Message;
.source "R12CManifest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/spe/R12CManifest$ProtoAdapter_R12CManifest;,
        Lsquareup/spe/R12CManifest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/spe/R12CManifest;",
        "Lsquareup/spe/R12CManifest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/spe/R12CManifest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MAX_COMPRESSION_VERSION:Ljava/lang/Integer;

.field public static final DEFAULT_PTS_VERSION:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final ble_manifest:Lsquareup/spe/TICC2640Manifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.TICC2640Manifest#ADAPTER"
        tag = 0x193
    .end annotation
.end field

.field public final comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.CommsProtocolVersion#ADAPTER"
        tag = 0x191
    .end annotation
.end field

.field public final crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.CRQGT4Manifest#ADAPTER"
        tag = 0x196
    .end annotation
.end field

.field public final k450_manifest:Lsquareup/spe/K450Manifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.K450Manifest#ADAPTER"
        tag = 0x192
    .end annotation
.end field

.field public final max_compression_version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x194
    .end annotation
.end field

.field public final pts_version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x195
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lsquareup/spe/R12CManifest$ProtoAdapter_R12CManifest;

    invoke-direct {v0}, Lsquareup/spe/R12CManifest$ProtoAdapter_R12CManifest;-><init>()V

    sput-object v0, Lsquareup/spe/R12CManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 25
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lsquareup/spe/R12CManifest;->DEFAULT_MAX_COMPRESSION_VERSION:Ljava/lang/Integer;

    .line 27
    sput-object v0, Lsquareup/spe/R12CManifest;->DEFAULT_PTS_VERSION:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lsquareup/spe/CommsProtocolVersion;Lsquareup/spe/K450Manifest;Lsquareup/spe/TICC2640Manifest;Ljava/lang/Integer;Ljava/lang/Integer;Lsquareup/spe/CRQGT4Manifest;)V
    .locals 8

    .line 68
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lsquareup/spe/R12CManifest;-><init>(Lsquareup/spe/CommsProtocolVersion;Lsquareup/spe/K450Manifest;Lsquareup/spe/TICC2640Manifest;Ljava/lang/Integer;Ljava/lang/Integer;Lsquareup/spe/CRQGT4Manifest;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lsquareup/spe/CommsProtocolVersion;Lsquareup/spe/K450Manifest;Lsquareup/spe/TICC2640Manifest;Ljava/lang/Integer;Ljava/lang/Integer;Lsquareup/spe/CRQGT4Manifest;Lokio/ByteString;)V
    .locals 1

    .line 74
    sget-object v0, Lsquareup/spe/R12CManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 75
    iput-object p1, p0, Lsquareup/spe/R12CManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    .line 76
    iput-object p2, p0, Lsquareup/spe/R12CManifest;->k450_manifest:Lsquareup/spe/K450Manifest;

    .line 77
    iput-object p3, p0, Lsquareup/spe/R12CManifest;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    .line 78
    iput-object p4, p0, Lsquareup/spe/R12CManifest;->max_compression_version:Ljava/lang/Integer;

    .line 79
    iput-object p5, p0, Lsquareup/spe/R12CManifest;->pts_version:Ljava/lang/Integer;

    .line 80
    iput-object p6, p0, Lsquareup/spe/R12CManifest;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 99
    :cond_0
    instance-of v1, p1, Lsquareup/spe/R12CManifest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 100
    :cond_1
    check-cast p1, Lsquareup/spe/R12CManifest;

    .line 101
    invoke-virtual {p0}, Lsquareup/spe/R12CManifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/spe/R12CManifest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/R12CManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    iget-object v3, p1, Lsquareup/spe/R12CManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    .line 102
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/R12CManifest;->k450_manifest:Lsquareup/spe/K450Manifest;

    iget-object v3, p1, Lsquareup/spe/R12CManifest;->k450_manifest:Lsquareup/spe/K450Manifest;

    .line 103
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/R12CManifest;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    iget-object v3, p1, Lsquareup/spe/R12CManifest;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    .line 104
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/R12CManifest;->max_compression_version:Ljava/lang/Integer;

    iget-object v3, p1, Lsquareup/spe/R12CManifest;->max_compression_version:Ljava/lang/Integer;

    .line 105
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/R12CManifest;->pts_version:Ljava/lang/Integer;

    iget-object v3, p1, Lsquareup/spe/R12CManifest;->pts_version:Ljava/lang/Integer;

    .line 106
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/R12CManifest;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    iget-object p1, p1, Lsquareup/spe/R12CManifest;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    .line 107
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 112
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 114
    invoke-virtual {p0}, Lsquareup/spe/R12CManifest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 115
    iget-object v1, p0, Lsquareup/spe/R12CManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lsquareup/spe/CommsProtocolVersion;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 116
    iget-object v1, p0, Lsquareup/spe/R12CManifest;->k450_manifest:Lsquareup/spe/K450Manifest;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lsquareup/spe/K450Manifest;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 117
    iget-object v1, p0, Lsquareup/spe/R12CManifest;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lsquareup/spe/TICC2640Manifest;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 118
    iget-object v1, p0, Lsquareup/spe/R12CManifest;->max_compression_version:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 119
    iget-object v1, p0, Lsquareup/spe/R12CManifest;->pts_version:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 120
    iget-object v1, p0, Lsquareup/spe/R12CManifest;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lsquareup/spe/CRQGT4Manifest;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 121
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lsquareup/spe/R12CManifest;->newBuilder()Lsquareup/spe/R12CManifest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/spe/R12CManifest$Builder;
    .locals 2

    .line 85
    new-instance v0, Lsquareup/spe/R12CManifest$Builder;

    invoke-direct {v0}, Lsquareup/spe/R12CManifest$Builder;-><init>()V

    .line 86
    iget-object v1, p0, Lsquareup/spe/R12CManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    iput-object v1, v0, Lsquareup/spe/R12CManifest$Builder;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    .line 87
    iget-object v1, p0, Lsquareup/spe/R12CManifest;->k450_manifest:Lsquareup/spe/K450Manifest;

    iput-object v1, v0, Lsquareup/spe/R12CManifest$Builder;->k450_manifest:Lsquareup/spe/K450Manifest;

    .line 88
    iget-object v1, p0, Lsquareup/spe/R12CManifest;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    iput-object v1, v0, Lsquareup/spe/R12CManifest$Builder;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    .line 89
    iget-object v1, p0, Lsquareup/spe/R12CManifest;->max_compression_version:Ljava/lang/Integer;

    iput-object v1, v0, Lsquareup/spe/R12CManifest$Builder;->max_compression_version:Ljava/lang/Integer;

    .line 90
    iget-object v1, p0, Lsquareup/spe/R12CManifest;->pts_version:Ljava/lang/Integer;

    iput-object v1, v0, Lsquareup/spe/R12CManifest$Builder;->pts_version:Ljava/lang/Integer;

    .line 91
    iget-object v1, p0, Lsquareup/spe/R12CManifest;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    iput-object v1, v0, Lsquareup/spe/R12CManifest$Builder;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    .line 92
    invoke-virtual {p0}, Lsquareup/spe/R12CManifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/spe/R12CManifest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    iget-object v1, p0, Lsquareup/spe/R12CManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    if-eqz v1, :cond_0

    const-string v1, ", comms_protocol_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/R12CManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 130
    :cond_0
    iget-object v1, p0, Lsquareup/spe/R12CManifest;->k450_manifest:Lsquareup/spe/K450Manifest;

    if-eqz v1, :cond_1

    const-string v1, ", k450_manifest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/R12CManifest;->k450_manifest:Lsquareup/spe/K450Manifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 131
    :cond_1
    iget-object v1, p0, Lsquareup/spe/R12CManifest;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    if-eqz v1, :cond_2

    const-string v1, ", ble_manifest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/R12CManifest;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 132
    :cond_2
    iget-object v1, p0, Lsquareup/spe/R12CManifest;->max_compression_version:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", max_compression_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/R12CManifest;->max_compression_version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 133
    :cond_3
    iget-object v1, p0, Lsquareup/spe/R12CManifest;->pts_version:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", pts_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/R12CManifest;->pts_version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 134
    :cond_4
    iget-object v1, p0, Lsquareup/spe/R12CManifest;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    if-eqz v1, :cond_5

    const-string v1, ", crq_gt4_manifest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/R12CManifest;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "R12CManifest{"

    .line 135
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
