.class public final Lsquareup/objc/PropertyOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PropertyOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/objc/PropertyOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/objc/PropertyOptions;",
        "Lsquareup/objc/PropertyOptions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public as_primitive:Ljava/lang/Boolean;

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 104
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public as_primitive(Ljava/lang/Boolean;)Lsquareup/objc/PropertyOptions$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lsquareup/objc/PropertyOptions$Builder;->as_primitive:Ljava/lang/Boolean;

    return-object p0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 99
    invoke-virtual {p0}, Lsquareup/objc/PropertyOptions$Builder;->build()Lsquareup/objc/PropertyOptions;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/objc/PropertyOptions;
    .locals 4

    .line 128
    new-instance v0, Lsquareup/objc/PropertyOptions;

    iget-object v1, p0, Lsquareup/objc/PropertyOptions$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lsquareup/objc/PropertyOptions$Builder;->as_primitive:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lsquareup/objc/PropertyOptions;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public name(Ljava/lang/String;)Lsquareup/objc/PropertyOptions$Builder;
    .locals 0

    .line 112
    iput-object p1, p0, Lsquareup/objc/PropertyOptions$Builder;->name:Ljava/lang/String;

    return-object p0
.end method
