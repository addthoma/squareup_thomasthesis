.class final Lsquareup/items/merchant/Query$NeighborType$ProtoAdapter_NeighborType;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Query.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Query$NeighborType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_NeighborType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/items/merchant/Query$NeighborType;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1177
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/items/merchant/Query$NeighborType;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1175
    invoke-virtual {p0, p1}, Lsquareup/items/merchant/Query$NeighborType$ProtoAdapter_NeighborType;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/Query$NeighborType;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/Query$NeighborType;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1196
    new-instance v0, Lsquareup/items/merchant/Query$NeighborType$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/Query$NeighborType$Builder;-><init>()V

    .line 1197
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1198
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 1210
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1208
    :cond_0
    iget-object v3, v0, Lsquareup/items/merchant/Query$NeighborType$Builder;->attribute_definition_tokens:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1202
    :cond_1
    :try_start_0
    sget-object v4, Lsquareup/items/merchant/CatalogObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsquareup/items/merchant/CatalogObjectType;

    invoke-virtual {v0, v4}, Lsquareup/items/merchant/Query$NeighborType$Builder;->object_type(Lsquareup/items/merchant/CatalogObjectType;)Lsquareup/items/merchant/Query$NeighborType$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 1204
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lsquareup/items/merchant/Query$NeighborType$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 1214
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/items/merchant/Query$NeighborType$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1215
    invoke-virtual {v0}, Lsquareup/items/merchant/Query$NeighborType$Builder;->build()Lsquareup/items/merchant/Query$NeighborType;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1175
    check-cast p2, Lsquareup/items/merchant/Query$NeighborType;

    invoke-virtual {p0, p1, p2}, Lsquareup/items/merchant/Query$NeighborType$ProtoAdapter_NeighborType;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/Query$NeighborType;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/Query$NeighborType;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1189
    sget-object v0, Lsquareup/items/merchant/CatalogObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/Query$NeighborType;->object_type:Lsquareup/items/merchant/CatalogObjectType;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1190
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lsquareup/items/merchant/Query$NeighborType;->attribute_definition_tokens:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1191
    invoke-virtual {p2}, Lsquareup/items/merchant/Query$NeighborType;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1175
    check-cast p1, Lsquareup/items/merchant/Query$NeighborType;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/Query$NeighborType$ProtoAdapter_NeighborType;->encodedSize(Lsquareup/items/merchant/Query$NeighborType;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/items/merchant/Query$NeighborType;)I
    .locals 4

    .line 1182
    sget-object v0, Lsquareup/items/merchant/CatalogObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/Query$NeighborType;->object_type:Lsquareup/items/merchant/CatalogObjectType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 1183
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lsquareup/items/merchant/Query$NeighborType;->attribute_definition_tokens:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1184
    invoke-virtual {p1}, Lsquareup/items/merchant/Query$NeighborType;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1175
    check-cast p1, Lsquareup/items/merchant/Query$NeighborType;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/Query$NeighborType$ProtoAdapter_NeighborType;->redact(Lsquareup/items/merchant/Query$NeighborType;)Lsquareup/items/merchant/Query$NeighborType;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/items/merchant/Query$NeighborType;)Lsquareup/items/merchant/Query$NeighborType;
    .locals 0

    .line 1220
    invoke-virtual {p1}, Lsquareup/items/merchant/Query$NeighborType;->newBuilder()Lsquareup/items/merchant/Query$NeighborType$Builder;

    move-result-object p1

    .line 1221
    invoke-virtual {p1}, Lsquareup/items/merchant/Query$NeighborType$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1222
    invoke-virtual {p1}, Lsquareup/items/merchant/Query$NeighborType$Builder;->build()Lsquareup/items/merchant/Query$NeighborType;

    move-result-object p1

    return-object p1
.end method
