.class final Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$ProtoAdapter_AttributeVisibility;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AttributeDefinition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AttributeVisibility"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 531
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 529
    invoke-virtual {p0, p1}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$ProtoAdapter_AttributeVisibility;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 548
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;-><init>()V

    .line 549
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 550
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 561
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 554
    :cond_0
    :try_start_0
    iget-object v4, v0, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;->object_types:Ljava/util/List;

    sget-object v5, Lsquareup/items/merchant/CatalogObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v5, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 556
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 565
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 566
    invoke-virtual {v0}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;->build()Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 529
    check-cast p2, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    invoke-virtual {p0, p1, p2}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$ProtoAdapter_AttributeVisibility;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 542
    sget-object v0, Lsquareup/items/merchant/CatalogObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->object_types:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 543
    invoke-virtual {p2}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 529
    check-cast p1, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$ProtoAdapter_AttributeVisibility;->encodedSize(Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;)I
    .locals 3

    .line 536
    sget-object v0, Lsquareup/items/merchant/CatalogObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->object_types:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 537
    invoke-virtual {p1}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 529
    check-cast p1, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$ProtoAdapter_AttributeVisibility;->redact(Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;)Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;)Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;
    .locals 0

    .line 571
    invoke-virtual {p1}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->newBuilder()Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;

    move-result-object p1

    .line 572
    invoke-virtual {p1}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 573
    invoke-virtual {p1}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility$Builder;->build()Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    move-result-object p1

    return-object p1
.end method
