.class public final Lsquareup/items/merchant/PutRequest;
.super Lcom/squareup/wire/Message;
.source "PutRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/PutRequest$ProtoAdapter_PutRequest;,
        Lsquareup/items/merchant/PutRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/items/merchant/PutRequest;",
        "Lsquareup/items/merchant/PutRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/PutRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_IDEMPOTENCY_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_LOCK_DURATION_MS:Ljava/lang/Long;

.field public static final DEFAULT_LOCK_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_OPEN_TRANSACTION:Ljava/lang/Boolean;

.field public static final DEFAULT_UNIT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final catalog_object:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.CatalogObject#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObject;",
            ">;"
        }
    .end annotation
.end field

.field public final idempotency_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final lock_duration_ms:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x7
    .end annotation
.end field

.field public final lock_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final open_transaction:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 45
    new-instance v0, Lsquareup/items/merchant/PutRequest$ProtoAdapter_PutRequest;

    invoke-direct {v0}, Lsquareup/items/merchant/PutRequest$ProtoAdapter_PutRequest;-><init>()V

    sput-object v0, Lsquareup/items/merchant/PutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 55
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lsquareup/items/merchant/PutRequest;->DEFAULT_OPEN_TRANSACTION:Ljava/lang/Boolean;

    const-wide/16 v0, 0x0

    .line 59
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lsquareup/items/merchant/PutRequest;->DEFAULT_LOCK_DURATION_MS:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObject;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .line 132
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lsquareup/items/merchant/PutRequest;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObject;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 138
    sget-object v0, Lsquareup/items/merchant/PutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 139
    iput-object p1, p0, Lsquareup/items/merchant/PutRequest;->idempotency_key:Ljava/lang/String;

    const-string p1, "catalog_object"

    .line 140
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lsquareup/items/merchant/PutRequest;->catalog_object:Ljava/util/List;

    .line 141
    iput-object p3, p0, Lsquareup/items/merchant/PutRequest;->merchant_token:Ljava/lang/String;

    .line 142
    iput-object p4, p0, Lsquareup/items/merchant/PutRequest;->unit_token:Ljava/lang/String;

    .line 143
    iput-object p5, p0, Lsquareup/items/merchant/PutRequest;->open_transaction:Ljava/lang/Boolean;

    .line 144
    iput-object p6, p0, Lsquareup/items/merchant/PutRequest;->lock_token:Ljava/lang/String;

    .line 145
    iput-object p7, p0, Lsquareup/items/merchant/PutRequest;->lock_duration_ms:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 165
    :cond_0
    instance-of v1, p1, Lsquareup/items/merchant/PutRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 166
    :cond_1
    check-cast p1, Lsquareup/items/merchant/PutRequest;

    .line 167
    invoke-virtual {p0}, Lsquareup/items/merchant/PutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/items/merchant/PutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->idempotency_key:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/items/merchant/PutRequest;->idempotency_key:Ljava/lang/String;

    .line 168
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->catalog_object:Ljava/util/List;

    iget-object v3, p1, Lsquareup/items/merchant/PutRequest;->catalog_object:Ljava/util/List;

    .line 169
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->merchant_token:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/items/merchant/PutRequest;->merchant_token:Ljava/lang/String;

    .line 170
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->unit_token:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/items/merchant/PutRequest;->unit_token:Ljava/lang/String;

    .line 171
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->open_transaction:Ljava/lang/Boolean;

    iget-object v3, p1, Lsquareup/items/merchant/PutRequest;->open_transaction:Ljava/lang/Boolean;

    .line 172
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->lock_token:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/items/merchant/PutRequest;->lock_token:Ljava/lang/String;

    .line 173
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->lock_duration_ms:Ljava/lang/Long;

    iget-object p1, p1, Lsquareup/items/merchant/PutRequest;->lock_duration_ms:Ljava/lang/Long;

    .line 174
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 179
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 181
    invoke-virtual {p0}, Lsquareup/items/merchant/PutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 182
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->idempotency_key:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 183
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->catalog_object:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 184
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 185
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 186
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->open_transaction:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 187
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->lock_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 188
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->lock_duration_ms:Ljava/lang/Long;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 189
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 44
    invoke-virtual {p0}, Lsquareup/items/merchant/PutRequest;->newBuilder()Lsquareup/items/merchant/PutRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/items/merchant/PutRequest$Builder;
    .locals 2

    .line 150
    new-instance v0, Lsquareup/items/merchant/PutRequest$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/PutRequest$Builder;-><init>()V

    .line 151
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->idempotency_key:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/items/merchant/PutRequest$Builder;->idempotency_key:Ljava/lang/String;

    .line 152
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->catalog_object:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lsquareup/items/merchant/PutRequest$Builder;->catalog_object:Ljava/util/List;

    .line 153
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/items/merchant/PutRequest$Builder;->merchant_token:Ljava/lang/String;

    .line 154
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->unit_token:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/items/merchant/PutRequest$Builder;->unit_token:Ljava/lang/String;

    .line 155
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->open_transaction:Ljava/lang/Boolean;

    iput-object v1, v0, Lsquareup/items/merchant/PutRequest$Builder;->open_transaction:Ljava/lang/Boolean;

    .line 156
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->lock_token:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/items/merchant/PutRequest$Builder;->lock_token:Ljava/lang/String;

    .line 157
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->lock_duration_ms:Ljava/lang/Long;

    iput-object v1, v0, Lsquareup/items/merchant/PutRequest$Builder;->lock_duration_ms:Ljava/lang/Long;

    .line 158
    invoke-virtual {p0}, Lsquareup/items/merchant/PutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/items/merchant/PutRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 196
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 197
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->idempotency_key:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", idempotency_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->idempotency_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    :cond_0
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->catalog_object:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", catalog_object="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->catalog_object:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 199
    :cond_1
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    :cond_2
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    :cond_3
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->open_transaction:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", open_transaction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->open_transaction:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 202
    :cond_4
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->lock_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", lock_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->lock_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    :cond_5
    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->lock_duration_ms:Ljava/lang/Long;

    if-eqz v1, :cond_6

    const-string v1, ", lock_duration_ms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/PutRequest;->lock_duration_ms:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PutRequest{"

    .line 204
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
