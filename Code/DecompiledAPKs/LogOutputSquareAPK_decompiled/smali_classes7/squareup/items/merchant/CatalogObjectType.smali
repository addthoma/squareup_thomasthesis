.class public final enum Lsquareup/items/merchant/CatalogObjectType;
.super Ljava/lang/Enum;
.source "CatalogObjectType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/CatalogObjectType$ProtoAdapter_CatalogObjectType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lsquareup/items/merchant/CatalogObjectType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lsquareup/items/merchant/CatalogObjectType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ADDITIONAL_ITEM_IMAGE:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum ATTRIBUTE_DEFINITION_METADATA:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum CATALOG:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum CATEGORY:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum COMPONENT:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum COMPOSITION:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum CONFIGURATION:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum CONSTRAINT:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum CUSTOM_ATTRIBUTE_DEFINITION:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum CUSTOM_ATTRIBUTE_SELECTION:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum DINING_OPTION:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum DISCOUNT:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum DO_NOT_USE:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum FAVORITES_LIST_POSITION:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum FEE:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum FLOOR_PLAN:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum FLOOR_PLAN_TILE:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum INVENTORY_INFO:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum ITEM:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum ITEM_FEE_MEMBERSHIP:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum ITEM_IMAGE:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum ITEM_MODIFIER_LIST:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum ITEM_MODIFIER_OPTION:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum ITEM_OPTION:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum ITEM_OPTION_VAL:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum ITEM_VARIATION:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum ITEM_VARIATION_VENDOR_INFO:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum MARKET_ITEM_SETTINGS:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum MEASUREMENT_UNIT:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum MENU:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum MENU_GROUP_MEMBERSHIP:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum OBSOLETE_TENDER_FEE:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum PAGE:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum PAGE_TILE:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum PLACEHOLDER:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum PRICING_RULE:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum PRODUCT:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum PRODUCT_FAMILY:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum PRODUCT_SET:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum PROMO:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum QUICK_AMOUNT:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum QUICK_AMOUNTS_SETTINGS:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum RESOURCE:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum SUBSCRIPTION_PLAN:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum SURCHARGE:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum SURCHARGE_FEE_MEMBERSHIP:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum TAG:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum TAX_RULE:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum TICKET_GROUP:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum TICKET_TEMPLATE:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum TIME_PERIOD:Lsquareup/items/merchant/CatalogObjectType;

.field public static final enum VOID_REASON:Lsquareup/items/merchant/CatalogObjectType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 11
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->DO_NOT_USE:Lsquareup/items/merchant/CatalogObjectType;

    .line 17
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const/4 v2, 0x1

    const-string v3, "ITEM"

    invoke-direct {v0, v3, v2, v2}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->ITEM:Lsquareup/items/merchant/CatalogObjectType;

    .line 19
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const/4 v3, 0x2

    const-string v4, "ITEM_IMAGE"

    invoke-direct {v0, v4, v3, v3}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->ITEM_IMAGE:Lsquareup/items/merchant/CatalogObjectType;

    .line 21
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const/4 v4, 0x3

    const-string v5, "PAGE_TILE"

    invoke-direct {v0, v5, v4, v4}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->PAGE_TILE:Lsquareup/items/merchant/CatalogObjectType;

    .line 23
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const/4 v5, 0x4

    const-string v6, "CATEGORY"

    invoke-direct {v0, v6, v5, v5}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->CATEGORY:Lsquareup/items/merchant/CatalogObjectType;

    .line 28
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const/4 v6, 0x5

    const-string v7, "ITEM_VARIATION"

    invoke-direct {v0, v7, v6, v6}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->ITEM_VARIATION:Lsquareup/items/merchant/CatalogObjectType;

    .line 30
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const/4 v7, 0x6

    const-string v8, "FEE"

    invoke-direct {v0, v8, v7, v7}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->FEE:Lsquareup/items/merchant/CatalogObjectType;

    .line 32
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const/4 v8, 0x7

    const-string v9, "PLACEHOLDER"

    invoke-direct {v0, v9, v8, v8}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->PLACEHOLDER:Lsquareup/items/merchant/CatalogObjectType;

    .line 34
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const/16 v9, 0x8

    const-string v10, "DISCOUNT"

    invoke-direct {v0, v10, v9, v9}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->DISCOUNT:Lsquareup/items/merchant/CatalogObjectType;

    .line 36
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const/16 v10, 0x9

    const-string v11, "ITEM_FEE_MEMBERSHIP"

    invoke-direct {v0, v11, v10, v10}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->ITEM_FEE_MEMBERSHIP:Lsquareup/items/merchant/CatalogObjectType;

    .line 38
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const/16 v11, 0xa

    const-string v12, "ITEM_MODIFIER_LIST"

    invoke-direct {v0, v12, v11, v11}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->ITEM_MODIFIER_LIST:Lsquareup/items/merchant/CatalogObjectType;

    .line 40
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const/16 v12, 0xb

    const-string v13, "ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP"

    invoke-direct {v0, v13, v12, v12}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP:Lsquareup/items/merchant/CatalogObjectType;

    .line 42
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const/16 v13, 0xc

    const-string v14, "ITEM_MODIFIER_OPTION"

    invoke-direct {v0, v14, v13, v13}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->ITEM_MODIFIER_OPTION:Lsquareup/items/merchant/CatalogObjectType;

    .line 44
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const/16 v14, 0xd

    const-string v15, "MARKET_ITEM_SETTINGS"

    invoke-direct {v0, v15, v14, v14}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->MARKET_ITEM_SETTINGS:Lsquareup/items/merchant/CatalogObjectType;

    .line 46
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const/16 v15, 0xe

    const-string v14, "ADDITIONAL_ITEM_IMAGE"

    invoke-direct {v0, v14, v15, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->ADDITIONAL_ITEM_IMAGE:Lsquareup/items/merchant/CatalogObjectType;

    .line 48
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v14, "PROMO"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->PROMO:Lsquareup/items/merchant/CatalogObjectType;

    .line 50
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "INVENTORY_INFO"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->INVENTORY_INFO:Lsquareup/items/merchant/CatalogObjectType;

    .line 52
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "OBSOLETE_TENDER_FEE"

    const/16 v14, 0x11

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->OBSOLETE_TENDER_FEE:Lsquareup/items/merchant/CatalogObjectType;

    .line 54
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "DINING_OPTION"

    const/16 v14, 0x12

    const/16 v15, 0x12

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->DINING_OPTION:Lsquareup/items/merchant/CatalogObjectType;

    .line 56
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "TAX_RULE"

    const/16 v14, 0x13

    const/16 v15, 0x13

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->TAX_RULE:Lsquareup/items/merchant/CatalogObjectType;

    .line 58
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "CONFIGURATION"

    const/16 v14, 0x14

    const/16 v15, 0x14

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->CONFIGURATION:Lsquareup/items/merchant/CatalogObjectType;

    .line 60
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "PAGE"

    const/16 v14, 0x15

    const/16 v15, 0x15

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->PAGE:Lsquareup/items/merchant/CatalogObjectType;

    .line 62
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "TICKET_GROUP"

    const/16 v14, 0x16

    const/16 v15, 0x16

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->TICKET_GROUP:Lsquareup/items/merchant/CatalogObjectType;

    .line 64
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "TICKET_TEMPLATE"

    const/16 v14, 0x17

    const/16 v15, 0x17

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->TICKET_TEMPLATE:Lsquareup/items/merchant/CatalogObjectType;

    .line 66
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "VOID_REASON"

    const/16 v14, 0x18

    const/16 v15, 0x18

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->VOID_REASON:Lsquareup/items/merchant/CatalogObjectType;

    .line 68
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "ITEM_VARIATION_VENDOR_INFO"

    const/16 v14, 0x19

    const/16 v15, 0x19

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->ITEM_VARIATION_VENDOR_INFO:Lsquareup/items/merchant/CatalogObjectType;

    .line 70
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "MENU"

    const/16 v14, 0x1a

    const/16 v15, 0x1a

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->MENU:Lsquareup/items/merchant/CatalogObjectType;

    .line 72
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "TAG"

    const/16 v14, 0x1b

    const/16 v15, 0x1b

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->TAG:Lsquareup/items/merchant/CatalogObjectType;

    .line 74
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "FLOOR_PLAN"

    const/16 v14, 0x1c

    const/16 v15, 0x1c

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->FLOOR_PLAN:Lsquareup/items/merchant/CatalogObjectType;

    .line 76
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "FLOOR_PLAN_TILE"

    const/16 v14, 0x1d

    const/16 v15, 0x1d

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->FLOOR_PLAN_TILE:Lsquareup/items/merchant/CatalogObjectType;

    .line 78
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "FAVORITES_LIST_POSITION"

    const/16 v14, 0x1e

    const/16 v15, 0x1e

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->FAVORITES_LIST_POSITION:Lsquareup/items/merchant/CatalogObjectType;

    .line 80
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "MENU_GROUP_MEMBERSHIP"

    const/16 v14, 0x1f

    const/16 v15, 0x1f

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->MENU_GROUP_MEMBERSHIP:Lsquareup/items/merchant/CatalogObjectType;

    .line 82
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "SURCHARGE"

    const/16 v14, 0x20

    const/16 v15, 0x20

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->SURCHARGE:Lsquareup/items/merchant/CatalogObjectType;

    .line 84
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "PRICING_RULE"

    const/16 v14, 0x21

    const/16 v15, 0x21

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->PRICING_RULE:Lsquareup/items/merchant/CatalogObjectType;

    .line 86
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "PRODUCT_SET"

    const/16 v14, 0x22

    const/16 v15, 0x22

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->PRODUCT_SET:Lsquareup/items/merchant/CatalogObjectType;

    .line 88
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "TIME_PERIOD"

    const/16 v14, 0x23

    const/16 v15, 0x23

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->TIME_PERIOD:Lsquareup/items/merchant/CatalogObjectType;

    .line 90
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "SURCHARGE_FEE_MEMBERSHIP"

    const/16 v14, 0x24

    const/16 v15, 0x24

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->SURCHARGE_FEE_MEMBERSHIP:Lsquareup/items/merchant/CatalogObjectType;

    .line 92
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "MEASUREMENT_UNIT"

    const/16 v14, 0x25

    const/16 v15, 0x25

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->MEASUREMENT_UNIT:Lsquareup/items/merchant/CatalogObjectType;

    .line 94
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "SUBSCRIPTION_PLAN"

    const/16 v14, 0x26

    const/16 v15, 0x26

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->SUBSCRIPTION_PLAN:Lsquareup/items/merchant/CatalogObjectType;

    .line 96
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "ITEM_OPTION"

    const/16 v14, 0x27

    const/16 v15, 0x27

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->ITEM_OPTION:Lsquareup/items/merchant/CatalogObjectType;

    .line 98
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "ITEM_OPTION_VAL"

    const/16 v14, 0x28

    const/16 v15, 0x28

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->ITEM_OPTION_VAL:Lsquareup/items/merchant/CatalogObjectType;

    .line 100
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "CUSTOM_ATTRIBUTE_DEFINITION"

    const/16 v14, 0x29

    const/16 v15, 0x29

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->CUSTOM_ATTRIBUTE_DEFINITION:Lsquareup/items/merchant/CatalogObjectType;

    .line 102
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "CUSTOM_ATTRIBUTE_SELECTION"

    const/16 v14, 0x2a

    const/16 v15, 0x2a

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->CUSTOM_ATTRIBUTE_SELECTION:Lsquareup/items/merchant/CatalogObjectType;

    .line 104
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "QUICK_AMOUNTS_SETTINGS"

    const/16 v14, 0x2b

    const/16 v15, 0x2d

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->QUICK_AMOUNTS_SETTINGS:Lsquareup/items/merchant/CatalogObjectType;

    .line 106
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "QUICK_AMOUNT"

    const/16 v14, 0x2c

    const/16 v15, 0x2e

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->QUICK_AMOUNT:Lsquareup/items/merchant/CatalogObjectType;

    .line 108
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "COMPONENT"

    const/16 v14, 0x2d

    const/16 v15, 0x2f

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->COMPONENT:Lsquareup/items/merchant/CatalogObjectType;

    .line 110
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "COMPOSITION"

    const/16 v14, 0x2e

    const/16 v15, 0x30

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->COMPOSITION:Lsquareup/items/merchant/CatalogObjectType;

    .line 112
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "RESOURCE"

    const/16 v14, 0x2f

    const/16 v15, 0x31

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->RESOURCE:Lsquareup/items/merchant/CatalogObjectType;

    .line 122
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "PRODUCT"

    const/16 v14, 0x30

    invoke-direct {v0, v13, v14, v6}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->PRODUCT:Lsquareup/items/merchant/CatalogObjectType;

    .line 127
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "PRODUCT_FAMILY"

    const/16 v14, 0x31

    invoke-direct {v0, v13, v14, v2}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->PRODUCT_FAMILY:Lsquareup/items/merchant/CatalogObjectType;

    .line 133
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "CATALOG"

    const/16 v14, 0x32

    const/16 v15, 0x64

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->CATALOG:Lsquareup/items/merchant/CatalogObjectType;

    .line 138
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "CONSTRAINT"

    const/16 v14, 0x33

    const/16 v15, 0x65

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->CONSTRAINT:Lsquareup/items/merchant/CatalogObjectType;

    .line 143
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType;

    const-string v13, "ATTRIBUTE_DEFINITION_METADATA"

    const/16 v14, 0x34

    const/16 v15, 0xc8

    invoke-direct {v0, v13, v14, v15}, Lsquareup/items/merchant/CatalogObjectType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->ATTRIBUTE_DEFINITION_METADATA:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v0, 0x35

    new-array v0, v0, [Lsquareup/items/merchant/CatalogObjectType;

    .line 10
    sget-object v13, Lsquareup/items/merchant/CatalogObjectType;->DO_NOT_USE:Lsquareup/items/merchant/CatalogObjectType;

    aput-object v13, v0, v1

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->ITEM:Lsquareup/items/merchant/CatalogObjectType;

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->ITEM_IMAGE:Lsquareup/items/merchant/CatalogObjectType;

    aput-object v1, v0, v3

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->PAGE_TILE:Lsquareup/items/merchant/CatalogObjectType;

    aput-object v1, v0, v4

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->CATEGORY:Lsquareup/items/merchant/CatalogObjectType;

    aput-object v1, v0, v5

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->ITEM_VARIATION:Lsquareup/items/merchant/CatalogObjectType;

    aput-object v1, v0, v6

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->FEE:Lsquareup/items/merchant/CatalogObjectType;

    aput-object v1, v0, v7

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->PLACEHOLDER:Lsquareup/items/merchant/CatalogObjectType;

    aput-object v1, v0, v8

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->DISCOUNT:Lsquareup/items/merchant/CatalogObjectType;

    aput-object v1, v0, v9

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->ITEM_FEE_MEMBERSHIP:Lsquareup/items/merchant/CatalogObjectType;

    aput-object v1, v0, v10

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->ITEM_MODIFIER_LIST:Lsquareup/items/merchant/CatalogObjectType;

    aput-object v1, v0, v11

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP:Lsquareup/items/merchant/CatalogObjectType;

    aput-object v1, v0, v12

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->ITEM_MODIFIER_OPTION:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->MARKET_ITEM_SETTINGS:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->ADDITIONAL_ITEM_IMAGE:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->PROMO:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->INVENTORY_INFO:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->OBSOLETE_TENDER_FEE:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->DINING_OPTION:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->TAX_RULE:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->CONFIGURATION:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->PAGE:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->TICKET_GROUP:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->TICKET_TEMPLATE:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->VOID_REASON:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->ITEM_VARIATION_VENDOR_INFO:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->MENU:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->TAG:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->FLOOR_PLAN:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->FLOOR_PLAN_TILE:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->FAVORITES_LIST_POSITION:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->MENU_GROUP_MEMBERSHIP:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->SURCHARGE:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->PRICING_RULE:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->PRODUCT_SET:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->TIME_PERIOD:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->SURCHARGE_FEE_MEMBERSHIP:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->MEASUREMENT_UNIT:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->SUBSCRIPTION_PLAN:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->ITEM_OPTION:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->ITEM_OPTION_VAL:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->CUSTOM_ATTRIBUTE_DEFINITION:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->CUSTOM_ATTRIBUTE_SELECTION:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->QUICK_AMOUNTS_SETTINGS:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->QUICK_AMOUNT:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->COMPONENT:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->COMPOSITION:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->RESOURCE:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->PRODUCT:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->PRODUCT_FAMILY:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->CATALOG:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->CONSTRAINT:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->ATTRIBUTE_DEFINITION_METADATA:Lsquareup/items/merchant/CatalogObjectType;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->$VALUES:[Lsquareup/items/merchant/CatalogObjectType;

    .line 145
    new-instance v0, Lsquareup/items/merchant/CatalogObjectType$ProtoAdapter_CatalogObjectType;

    invoke-direct {v0}, Lsquareup/items/merchant/CatalogObjectType$ProtoAdapter_CatalogObjectType;-><init>()V

    sput-object v0, Lsquareup/items/merchant/CatalogObjectType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 149
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 150
    iput p3, p0, Lsquareup/items/merchant/CatalogObjectType;->value:I

    return-void
.end method

.method public static fromValue(I)Lsquareup/items/merchant/CatalogObjectType;
    .locals 1

    const/16 v0, 0x64

    if-eq p0, v0, :cond_2

    const/16 v0, 0x65

    if-eq p0, v0, :cond_1

    const/16 v0, 0xc8

    if-eq p0, v0, :cond_0

    packed-switch p0, :pswitch_data_0

    packed-switch p0, :pswitch_data_1

    const/4 p0, 0x0

    return-object p0

    .line 205
    :pswitch_0
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->RESOURCE:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 204
    :pswitch_1
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->COMPOSITION:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 203
    :pswitch_2
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->COMPONENT:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 202
    :pswitch_3
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->QUICK_AMOUNT:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 201
    :pswitch_4
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->QUICK_AMOUNTS_SETTINGS:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 200
    :pswitch_5
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->CUSTOM_ATTRIBUTE_SELECTION:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 199
    :pswitch_6
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->CUSTOM_ATTRIBUTE_DEFINITION:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 198
    :pswitch_7
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->ITEM_OPTION_VAL:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 197
    :pswitch_8
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->ITEM_OPTION:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 196
    :pswitch_9
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->SUBSCRIPTION_PLAN:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 195
    :pswitch_a
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->MEASUREMENT_UNIT:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 194
    :pswitch_b
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->SURCHARGE_FEE_MEMBERSHIP:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 193
    :pswitch_c
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->TIME_PERIOD:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 192
    :pswitch_d
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->PRODUCT_SET:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 191
    :pswitch_e
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->PRICING_RULE:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 190
    :pswitch_f
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->SURCHARGE:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 189
    :pswitch_10
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->MENU_GROUP_MEMBERSHIP:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 188
    :pswitch_11
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->FAVORITES_LIST_POSITION:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 187
    :pswitch_12
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->FLOOR_PLAN_TILE:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 186
    :pswitch_13
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->FLOOR_PLAN:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 185
    :pswitch_14
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->TAG:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 184
    :pswitch_15
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->MENU:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 183
    :pswitch_16
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->ITEM_VARIATION_VENDOR_INFO:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 182
    :pswitch_17
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->VOID_REASON:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 181
    :pswitch_18
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->TICKET_TEMPLATE:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 180
    :pswitch_19
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->TICKET_GROUP:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 179
    :pswitch_1a
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->PAGE:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 178
    :pswitch_1b
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->CONFIGURATION:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 177
    :pswitch_1c
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->TAX_RULE:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 176
    :pswitch_1d
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->DINING_OPTION:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 175
    :pswitch_1e
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->OBSOLETE_TENDER_FEE:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 174
    :pswitch_1f
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->INVENTORY_INFO:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 173
    :pswitch_20
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->PROMO:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 172
    :pswitch_21
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->ADDITIONAL_ITEM_IMAGE:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 171
    :pswitch_22
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->MARKET_ITEM_SETTINGS:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 170
    :pswitch_23
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->ITEM_MODIFIER_OPTION:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 169
    :pswitch_24
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 168
    :pswitch_25
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->ITEM_MODIFIER_LIST:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 167
    :pswitch_26
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->ITEM_FEE_MEMBERSHIP:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 166
    :pswitch_27
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->DISCOUNT:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 165
    :pswitch_28
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->PLACEHOLDER:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 164
    :pswitch_29
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->FEE:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 163
    :pswitch_2a
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->ITEM_VARIATION:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 162
    :pswitch_2b
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->CATEGORY:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 161
    :pswitch_2c
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->PAGE_TILE:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 160
    :pswitch_2d
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->ITEM_IMAGE:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 159
    :pswitch_2e
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->ITEM:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 158
    :pswitch_2f
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->DO_NOT_USE:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 208
    :cond_0
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->ATTRIBUTE_DEFINITION_METADATA:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 207
    :cond_1
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->CONSTRAINT:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    .line 206
    :cond_2
    sget-object p0, Lsquareup/items/merchant/CatalogObjectType;->CATALOG:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2d
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lsquareup/items/merchant/CatalogObjectType;
    .locals 1

    .line 10
    const-class v0, Lsquareup/items/merchant/CatalogObjectType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lsquareup/items/merchant/CatalogObjectType;

    return-object p0
.end method

.method public static values()[Lsquareup/items/merchant/CatalogObjectType;
    .locals 1

    .line 10
    sget-object v0, Lsquareup/items/merchant/CatalogObjectType;->$VALUES:[Lsquareup/items/merchant/CatalogObjectType;

    invoke-virtual {v0}, [Lsquareup/items/merchant/CatalogObjectType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsquareup/items/merchant/CatalogObjectType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 215
    iget v0, p0, Lsquareup/items/merchant/CatalogObjectType;->value:I

    return v0
.end method
