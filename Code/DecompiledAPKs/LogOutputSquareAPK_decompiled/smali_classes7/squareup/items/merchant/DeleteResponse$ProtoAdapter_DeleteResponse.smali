.class final Lsquareup/items/merchant/DeleteResponse$ProtoAdapter_DeleteResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "DeleteResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/DeleteResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_DeleteResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/items/merchant/DeleteResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 332
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/items/merchant/DeleteResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 330
    invoke-virtual {p0, p1}, Lsquareup/items/merchant/DeleteResponse$ProtoAdapter_DeleteResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/DeleteResponse;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/DeleteResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 355
    new-instance v0, Lsquareup/items/merchant/DeleteResponse$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/DeleteResponse$Builder;-><init>()V

    .line 356
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 357
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 364
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 362
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/DeleteResponse$Builder;->lock_token(Ljava/lang/String;)Lsquareup/items/merchant/DeleteResponse$Builder;

    goto :goto_0

    .line 361
    :cond_1
    iget-object v3, v0, Lsquareup/items/merchant/DeleteResponse$Builder;->deleted_objects:Ljava/util/List;

    sget-object v4, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 360
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/DeleteResponse$Builder;->modification_timestamp(Ljava/lang/Long;)Lsquareup/items/merchant/DeleteResponse$Builder;

    goto :goto_0

    .line 359
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/DeleteResponse$Builder;->count(Ljava/lang/Long;)Lsquareup/items/merchant/DeleteResponse$Builder;

    goto :goto_0

    .line 368
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/items/merchant/DeleteResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 369
    invoke-virtual {v0}, Lsquareup/items/merchant/DeleteResponse$Builder;->build()Lsquareup/items/merchant/DeleteResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 330
    check-cast p2, Lsquareup/items/merchant/DeleteResponse;

    invoke-virtual {p0, p1, p2}, Lsquareup/items/merchant/DeleteResponse$ProtoAdapter_DeleteResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/DeleteResponse;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/DeleteResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 346
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/DeleteResponse;->count:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 347
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/DeleteResponse;->modification_timestamp:Ljava/lang/Long;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 348
    sget-object v0, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lsquareup/items/merchant/DeleteResponse;->deleted_objects:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 349
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/DeleteResponse;->lock_token:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 350
    invoke-virtual {p2}, Lsquareup/items/merchant/DeleteResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 330
    check-cast p1, Lsquareup/items/merchant/DeleteResponse;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/DeleteResponse$ProtoAdapter_DeleteResponse;->encodedSize(Lsquareup/items/merchant/DeleteResponse;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/items/merchant/DeleteResponse;)I
    .locals 4

    .line 337
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/DeleteResponse;->count:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/DeleteResponse;->modification_timestamp:Ljava/lang/Long;

    const/4 v3, 0x2

    .line 338
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 339
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lsquareup/items/merchant/DeleteResponse;->deleted_objects:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/DeleteResponse;->lock_token:Ljava/lang/String;

    const/4 v3, 0x4

    .line 340
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 341
    invoke-virtual {p1}, Lsquareup/items/merchant/DeleteResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 330
    check-cast p1, Lsquareup/items/merchant/DeleteResponse;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/DeleteResponse$ProtoAdapter_DeleteResponse;->redact(Lsquareup/items/merchant/DeleteResponse;)Lsquareup/items/merchant/DeleteResponse;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/items/merchant/DeleteResponse;)Lsquareup/items/merchant/DeleteResponse;
    .locals 2

    .line 374
    invoke-virtual {p1}, Lsquareup/items/merchant/DeleteResponse;->newBuilder()Lsquareup/items/merchant/DeleteResponse$Builder;

    move-result-object p1

    .line 375
    iget-object v0, p1, Lsquareup/items/merchant/DeleteResponse$Builder;->deleted_objects:Ljava/util/List;

    sget-object v1, Lsquareup/items/merchant/DeleteResponse$DeletedObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 376
    invoke-virtual {p1}, Lsquareup/items/merchant/DeleteResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 377
    invoke-virtual {p1}, Lsquareup/items/merchant/DeleteResponse$Builder;->build()Lsquareup/items/merchant/DeleteResponse;

    move-result-object p1

    return-object p1
.end method
