.class final Lhu/akarnokd/rxjava/interop/SubscriptionV1ToDisposableV2;
.super Ljava/lang/Object;
.source "SubscriptionV1ToDisposableV2.java"

# interfaces
.implements Lio/reactivex/disposables/Disposable;


# instance fields
.field private final subscription:Lrx/Subscription;


# direct methods
.method constructor <init>(Lrx/Subscription;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/SubscriptionV1ToDisposableV2;->subscription:Lrx/Subscription;

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .line 37
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubscriptionV1ToDisposableV2;->subscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .line 42
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SubscriptionV1ToDisposableV2;->subscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->isUnsubscribed()Z

    move-result v0

    return v0
.end method
