.class final Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;
.super Ljava/lang/Object;
.source "ProcessorV2ToSubjectV1.java"

# interfaces
.implements Lrx/Observable$OnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Observable$OnSubscribe<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final processor:Lio/reactivex/processors/FlowableProcessor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/processors/FlowableProcessor<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/processors/FlowableProcessor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/processors/FlowableProcessor<",
            "TT;>;)V"
        }
    .end annotation

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;->processor:Lio/reactivex/processors/FlowableProcessor;

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 58
    check-cast p1, Lrx/Subscriber;

    invoke-virtual {p0, p1}, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;->call(Lrx/Subscriber;)V

    return-void
.end method

.method public call(Lrx/Subscriber;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Subscriber<",
            "-TT;>;)V"
        }
    .end annotation

    .line 69
    new-instance v0, Lhu/akarnokd/rxjava/interop/FlowableV2ToObservableV1$SourceSubscriber;

    invoke-direct {v0, p1}, Lhu/akarnokd/rxjava/interop/FlowableV2ToObservableV1$SourceSubscriber;-><init>(Lrx/Subscriber;)V

    .line 72
    invoke-virtual {p1, v0}, Lrx/Subscriber;->add(Lrx/Subscription;)V

    .line 73
    invoke-virtual {p1, v0}, Lrx/Subscriber;->setProducer(Lrx/Producer;)V

    .line 75
    iget-object p1, p0, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;->processor:Lio/reactivex/processors/FlowableProcessor;

    invoke-virtual {p1, v0}, Lio/reactivex/processors/FlowableProcessor;->subscribe(Lio/reactivex/FlowableSubscriber;)V

    return-void
.end method

.method hasObservers()Z
    .locals 1

    .line 91
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;->processor:Lio/reactivex/processors/FlowableProcessor;

    invoke-virtual {v0}, Lio/reactivex/processors/FlowableProcessor;->hasSubscribers()Z

    move-result v0

    return v0
.end method

.method onCompleted()V
    .locals 1

    .line 87
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;->processor:Lio/reactivex/processors/FlowableProcessor;

    invoke-virtual {v0}, Lio/reactivex/processors/FlowableProcessor;->onComplete()V

    return-void
.end method

.method onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 83
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;->processor:Lio/reactivex/processors/FlowableProcessor;

    invoke-virtual {v0, p1}, Lio/reactivex/processors/FlowableProcessor;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method onNext(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 79
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1$State;->processor:Lio/reactivex/processors/FlowableProcessor;

    invoke-virtual {v0, p1}, Lio/reactivex/processors/FlowableProcessor;->onNext(Ljava/lang/Object;)V

    return-void
.end method
