.class final Lhu/akarnokd/rxjava/interop/CompletableV2ToCompletableV1$SourceCompletableSubscriber;
.super Ljava/lang/Object;
.source "CompletableV2ToCompletableV1.java"

# interfaces
.implements Lio/reactivex/CompletableObserver;
.implements Lrx/Subscription;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhu/akarnokd/rxjava/interop/CompletableV2ToCompletableV1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SourceCompletableSubscriber"
.end annotation


# instance fields
.field d:Lio/reactivex/disposables/Disposable;

.field final observer:Lrx/CompletableSubscriber;


# direct methods
.method constructor <init>(Lrx/CompletableSubscriber;)V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/CompletableV2ToCompletableV1$SourceCompletableSubscriber;->observer:Lrx/CompletableSubscriber;

    return-void
.end method


# virtual methods
.method public isUnsubscribed()Z
    .locals 1

    .line 69
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/CompletableV2ToCompletableV1$SourceCompletableSubscriber;->d:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->isDisposed()Z

    move-result v0

    return v0
.end method

.method public onComplete()V
    .locals 1

    .line 54
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/CompletableV2ToCompletableV1$SourceCompletableSubscriber;->observer:Lrx/CompletableSubscriber;

    invoke-interface {v0}, Lrx/CompletableSubscriber;->onCompleted()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 59
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/CompletableV2ToCompletableV1$SourceCompletableSubscriber;->observer:Lrx/CompletableSubscriber;

    invoke-interface {v0, p1}, Lrx/CompletableSubscriber;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onSubscribe(Lio/reactivex/disposables/Disposable;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/CompletableV2ToCompletableV1$SourceCompletableSubscriber;->d:Lio/reactivex/disposables/Disposable;

    .line 49
    iget-object p1, p0, Lhu/akarnokd/rxjava/interop/CompletableV2ToCompletableV1$SourceCompletableSubscriber;->observer:Lrx/CompletableSubscriber;

    invoke-interface {p1, p0}, Lrx/CompletableSubscriber;->onSubscribe(Lrx/Subscription;)V

    return-void
.end method

.method public unsubscribe()V
    .locals 1

    .line 64
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/CompletableV2ToCompletableV1$SourceCompletableSubscriber;->d:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    return-void
.end method
