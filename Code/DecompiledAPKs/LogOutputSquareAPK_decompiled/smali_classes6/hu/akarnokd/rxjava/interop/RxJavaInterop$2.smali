.class final Lhu/akarnokd/rxjava/interop/RxJavaInterop$2;
.super Ljava/lang/Object;
.source "RxJavaInterop.java"

# interfaces
.implements Lio/reactivex/ObservableTransformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Transformer(Lrx/Observable$Transformer;Lio/reactivex/BackpressureStrategy;)Lio/reactivex/ObservableTransformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableTransformer<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic val$strategy:Lio/reactivex/BackpressureStrategy;

.field final synthetic val$transformer:Lrx/Observable$Transformer;


# direct methods
.method constructor <init>(Lrx/Observable$Transformer;Lio/reactivex/BackpressureStrategy;)V
    .locals 0

    .line 228
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$2;->val$transformer:Lrx/Observable$Transformer;

    iput-object p2, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$2;->val$strategy:Lio/reactivex/BackpressureStrategy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "TT;>;)",
            "Lio/reactivex/ObservableSource<",
            "TR;>;"
        }
    .end annotation

    .line 231
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$2;->val$transformer:Lrx/Observable$Transformer;

    iget-object v1, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$2;->val$strategy:Lio/reactivex/BackpressureStrategy;

    invoke-static {p1, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object p1

    invoke-interface {v0, p1}, Lrx/Observable$Transformer;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lrx/Observable;

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
