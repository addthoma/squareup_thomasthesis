.class final Lhu/akarnokd/rxjava/interop/SingleV1ToMaybeV2$SourceSingleSubscriber;
.super Lrx/SingleSubscriber;
.source "SingleV1ToMaybeV2.java"

# interfaces
.implements Lio/reactivex/disposables/Disposable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lhu/akarnokd/rxjava/interop/SingleV1ToMaybeV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SourceSingleSubscriber"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lrx/SingleSubscriber<",
        "TT;>;",
        "Lio/reactivex/disposables/Disposable;"
    }
.end annotation


# instance fields
.field final observer:Lio/reactivex/MaybeObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/MaybeObserver<",
            "-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/MaybeObserver;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/MaybeObserver<",
            "-TT;>;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Lrx/SingleSubscriber;-><init>()V

    .line 47
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/SingleV1ToMaybeV2$SourceSingleSubscriber;->observer:Lio/reactivex/MaybeObserver;

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 0

    .line 67
    invoke-virtual {p0}, Lhu/akarnokd/rxjava/interop/SingleV1ToMaybeV2$SourceSingleSubscriber;->unsubscribe()V

    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .line 72
    invoke-virtual {p0}, Lhu/akarnokd/rxjava/interop/SingleV1ToMaybeV2$SourceSingleSubscriber;->isUnsubscribed()Z

    move-result v0

    return v0
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 62
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SingleV1ToMaybeV2$SourceSingleSubscriber;->observer:Lio/reactivex/MaybeObserver;

    invoke-interface {v0, p1}, Lio/reactivex/MaybeObserver;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    .line 53
    iget-object p1, p0, Lhu/akarnokd/rxjava/interop/SingleV1ToMaybeV2$SourceSingleSubscriber;->observer:Lio/reactivex/MaybeObserver;

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "The upstream 1.x Single signalled a null value which is not supported in 2.x"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lio/reactivex/MaybeObserver;->onError(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 56
    :cond_0
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/SingleV1ToMaybeV2$SourceSingleSubscriber;->observer:Lio/reactivex/MaybeObserver;

    invoke-interface {v0, p1}, Lio/reactivex/MaybeObserver;->onSuccess(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
