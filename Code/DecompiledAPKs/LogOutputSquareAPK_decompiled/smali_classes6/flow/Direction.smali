.class public final enum Lflow/Direction;
.super Ljava/lang/Enum;
.source "Direction.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lflow/Direction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lflow/Direction;

.field public static final enum BACKWARD:Lflow/Direction;

.field public static final enum FORWARD:Lflow/Direction;

.field public static final enum REPLACE:Lflow/Direction;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 4
    new-instance v0, Lflow/Direction;

    const/4 v1, 0x0

    const-string v2, "FORWARD"

    invoke-direct {v0, v2, v1}, Lflow/Direction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflow/Direction;->FORWARD:Lflow/Direction;

    new-instance v0, Lflow/Direction;

    const/4 v2, 0x1

    const-string v3, "BACKWARD"

    invoke-direct {v0, v3, v2}, Lflow/Direction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflow/Direction;->BACKWARD:Lflow/Direction;

    new-instance v0, Lflow/Direction;

    const/4 v3, 0x2

    const-string v4, "REPLACE"

    invoke-direct {v0, v4, v3}, Lflow/Direction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    const/4 v0, 0x3

    new-array v0, v0, [Lflow/Direction;

    .line 3
    sget-object v4, Lflow/Direction;->FORWARD:Lflow/Direction;

    aput-object v4, v0, v1

    sget-object v1, Lflow/Direction;->BACKWARD:Lflow/Direction;

    aput-object v1, v0, v2

    sget-object v1, Lflow/Direction;->REPLACE:Lflow/Direction;

    aput-object v1, v0, v3

    sput-object v0, Lflow/Direction;->$VALUES:[Lflow/Direction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflow/Direction;
    .locals 1

    .line 3
    const-class v0, Lflow/Direction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lflow/Direction;

    return-object p0
.end method

.method public static values()[Lflow/Direction;
    .locals 1

    .line 3
    sget-object v0, Lflow/Direction;->$VALUES:[Lflow/Direction;

    invoke-virtual {v0}, [Lflow/Direction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflow/Direction;

    return-object v0
.end method
