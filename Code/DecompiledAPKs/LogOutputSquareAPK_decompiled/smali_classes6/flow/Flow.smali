.class public final Lflow/Flow;
.super Ljava/lang/Object;
.source "Flow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lflow/Flow$PendingTraversal;,
        Lflow/Flow$TraversalState;
    }
.end annotation


# static fields
.field public static final FLOW_SERVICE:Ljava/lang/String; = "flow.Flow.FLOW_SERVICE"


# instance fields
.field private dispatcher:Lflow/Dispatcher;

.field private history:Lflow/History;

.field private pendingTraversal:Lflow/Flow$PendingTraversal;

.field private final processor:Lflow/Processor;


# direct methods
.method public constructor <init>(Lflow/History;)V
    .locals 1

    .line 49
    sget-object v0, Lflow/-$$Lambda$Flow$s2yFsPk-kfKSE-YWaNwKTVpJ2Eg;->INSTANCE:Lflow/-$$Lambda$Flow$s2yFsPk-kfKSE-YWaNwKTVpJ2Eg;

    invoke-direct {p0, p1, v0}, Lflow/Flow;-><init>(Lflow/History;Lflow/Processor;)V

    return-void
.end method

.method public constructor <init>(Lflow/History;Lflow/Processor;)V
    .locals 0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {}, Lflow/Preconditions;->assertOnMainThread()V

    .line 54
    iput-object p1, p0, Lflow/Flow;->history:Lflow/History;

    .line 55
    iput-object p2, p0, Lflow/Flow;->processor:Lflow/Processor;

    return-void
.end method

.method static synthetic access$100(Lflow/History;Lflow/History;)Lflow/History;
    .locals 0

    .line 26
    invoke-static {p0, p1}, Lflow/Flow;->preserveEquivalentPrefix(Lflow/History;Lflow/History;)Lflow/History;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200(Lflow/Flow;)Lflow/History;
    .locals 0

    .line 26
    iget-object p0, p0, Lflow/Flow;->history:Lflow/History;

    return-object p0
.end method

.method static synthetic access$202(Lflow/Flow;Lflow/History;)Lflow/History;
    .locals 0

    .line 26
    iput-object p1, p0, Lflow/Flow;->history:Lflow/History;

    return-object p1
.end method

.method static synthetic access$300(Lflow/Flow;)Lflow/Flow$PendingTraversal;
    .locals 0

    .line 26
    iget-object p0, p0, Lflow/Flow;->pendingTraversal:Lflow/Flow$PendingTraversal;

    return-object p0
.end method

.method static synthetic access$302(Lflow/Flow;Lflow/Flow$PendingTraversal;)Lflow/Flow$PendingTraversal;
    .locals 0

    .line 26
    iput-object p1, p0, Lflow/Flow;->pendingTraversal:Lflow/Flow$PendingTraversal;

    return-object p1
.end method

.method static synthetic access$400(Lflow/Flow;)Lflow/Dispatcher;
    .locals 0

    .line 26
    iget-object p0, p0, Lflow/Flow;->dispatcher:Lflow/Dispatcher;

    return-object p0
.end method

.method static synthetic access$500(Lflow/Flow;)Lflow/Processor;
    .locals 0

    .line 26
    iget-object p0, p0, Lflow/Flow;->processor:Lflow/Processor;

    return-object p0
.end method

.method public static get(Landroid/content/Context;)Lflow/Flow;
    .locals 1

    const-string v0, "flow.Flow.FLOW_SERVICE"

    .line 35
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lflow/Flow;

    return-object p0
.end method

.method public static get(Landroid/view/View;)Lflow/Flow;
    .locals 0

    .line 30
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Lflow/Flow;->get(Landroid/content/Context;)Lflow/Flow;

    move-result-object p0

    return-object p0
.end method

.method public static isFlowSystemService(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "flow.Flow.FLOW_SERVICE"

    .line 39
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$new$0(Lflow/Traversal;)Lflow/Traversal;
    .locals 0

    return-object p0
.end method

.method private move(Lflow/Flow$PendingTraversal;)V
    .locals 1

    .line 201
    iget-object v0, p0, Lflow/Flow;->pendingTraversal:Lflow/Flow$PendingTraversal;

    if-nez v0, :cond_0

    .line 202
    iput-object p1, p0, Lflow/Flow;->pendingTraversal:Lflow/Flow$PendingTraversal;

    .line 204
    iget-object v0, p0, Lflow/Flow;->dispatcher:Lflow/Dispatcher;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lflow/Flow$PendingTraversal;->execute()V

    goto :goto_0

    .line 206
    :cond_0
    invoke-virtual {v0, p1}, Lflow/Flow$PendingTraversal;->enqueue(Lflow/Flow$PendingTraversal;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private static preserveEquivalentPrefix(Lflow/History;Lflow/History;)Lflow/History;
    .locals 4

    .line 211
    invoke-virtual {p0}, Lflow/History;->framesFromBottom()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 212
    invoke-virtual {p1}, Lflow/History;->framesFromBottom()Ljava/lang/Iterable;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 214
    invoke-virtual {p0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lflow/History$Builder;->clear()Lflow/History$Builder;

    move-result-object p0

    .line 216
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 217
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 218
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 219
    invoke-virtual {p0, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    goto :goto_1

    .line 222
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 223
    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 224
    invoke-virtual {p0, v2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    goto :goto_0

    .line 226
    :cond_1
    invoke-virtual {p0, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 231
    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 232
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    goto :goto_1

    .line 234
    :cond_3
    invoke-virtual {p0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getHistory()Lflow/History;
    .locals 1

    .line 59
    invoke-static {}, Lflow/Preconditions;->assertOnMainThread()V

    .line 60
    iget-object v0, p0, Lflow/Flow;->history:Lflow/History;

    return-object v0
.end method

.method public goBack()Z
    .locals 3

    .line 180
    invoke-static {}, Lflow/Preconditions;->assertOnMainThread()V

    .line 181
    iget-object v0, p0, Lflow/Flow;->history:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    iget-object v0, p0, Lflow/Flow;->pendingTraversal:Lflow/Flow$PendingTraversal;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lflow/Flow$PendingTraversal;->state:Lflow/Flow$TraversalState;

    sget-object v2, Lflow/Flow$TraversalState;->FINISHED:Lflow/Flow$TraversalState;

    if-eq v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 183
    :cond_1
    :goto_0
    new-instance v0, Lflow/Flow$3;

    invoke-direct {v0, p0}, Lflow/Flow$3;-><init>(Lflow/Flow;)V

    invoke-direct {p0, v0}, Lflow/Flow;->move(Lflow/Flow$PendingTraversal;)V

    return v1
.end method

.method public removeDispatcher(Lflow/Dispatcher;)V
    .locals 2

    .line 99
    invoke-static {}, Lflow/Preconditions;->assertOnMainThread()V

    .line 102
    iget-object v0, p0, Lflow/Flow;->dispatcher:Lflow/Dispatcher;

    const-string v1, "dispatcher"

    invoke-static {p1, v1}, Lflow/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    if-ne v0, p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lflow/Flow;->dispatcher:Lflow/Dispatcher;

    :cond_0
    return-void
.end method

.method public set(Ljava/lang/Object;)V
    .locals 1

    .line 133
    invoke-static {}, Lflow/Preconditions;->assertOnMainThread()V

    .line 134
    new-instance v0, Lflow/Flow$2;

    invoke-direct {v0, p0, p1}, Lflow/Flow$2;-><init>(Lflow/Flow;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lflow/Flow;->move(Lflow/Flow$PendingTraversal;)V

    return-void
.end method

.method public setDispatcher(Lflow/Dispatcher;)V
    .locals 2

    .line 69
    invoke-static {}, Lflow/Preconditions;->assertOnMainThread()V

    const-string v0, "dispatcher"

    .line 70
    invoke-static {p1, v0}, Lflow/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/Dispatcher;

    iput-object p1, p0, Lflow/Flow;->dispatcher:Lflow/Dispatcher;

    .line 72
    iget-object p1, p0, Lflow/Flow;->pendingTraversal:Lflow/Flow$PendingTraversal;

    if-eqz p1, :cond_3

    iget-object p1, p1, Lflow/Flow$PendingTraversal;->state:Lflow/Flow$TraversalState;

    sget-object v0, Lflow/Flow$TraversalState;->DISPATCHED:Lflow/Flow$TraversalState;

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lflow/Flow;->pendingTraversal:Lflow/Flow$PendingTraversal;

    iget-object p1, p1, Lflow/Flow$PendingTraversal;->next:Lflow/Flow$PendingTraversal;

    if-nez p1, :cond_0

    goto :goto_0

    .line 81
    :cond_0
    iget-object p1, p0, Lflow/Flow;->pendingTraversal:Lflow/Flow$PendingTraversal;

    iget-object p1, p1, Lflow/Flow$PendingTraversal;->state:Lflow/Flow$TraversalState;

    sget-object v0, Lflow/Flow$TraversalState;->ENQUEUED:Lflow/Flow$TraversalState;

    if-ne p1, v0, :cond_1

    .line 83
    iget-object p1, p0, Lflow/Flow;->pendingTraversal:Lflow/Flow$PendingTraversal;

    invoke-virtual {p1}, Lflow/Flow$PendingTraversal;->execute()V

    return-void

    .line 87
    :cond_1
    iget-object p1, p0, Lflow/Flow;->pendingTraversal:Lflow/Flow$PendingTraversal;

    iget-object p1, p1, Lflow/Flow$PendingTraversal;->state:Lflow/Flow$TraversalState;

    sget-object v0, Lflow/Flow$TraversalState;->DISPATCHED:Lflow/Flow$TraversalState;

    if-ne p1, v0, :cond_2

    return-void

    .line 88
    :cond_2
    new-instance p1, Ljava/lang/AssertionError;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Hanging traversal in unexpected state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lflow/Flow;->pendingTraversal:Lflow/Flow$PendingTraversal;

    iget-object v1, v1, Lflow/Flow$PendingTraversal;->state:Lflow/Flow$TraversalState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    .line 77
    :cond_3
    :goto_0
    iget-object p1, p0, Lflow/Flow;->history:Lflow/History;

    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-virtual {p0, p1, v0}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    return-void
.end method

.method public setHistory(Lflow/History;Lflow/Direction;)V
    .locals 1

    .line 109
    invoke-static {}, Lflow/Preconditions;->assertOnMainThread()V

    .line 110
    new-instance v0, Lflow/Flow$1;

    invoke-direct {v0, p0, p1, p2}, Lflow/Flow$1;-><init>(Lflow/Flow;Lflow/History;Lflow/Direction;)V

    invoke-direct {p0, v0}, Lflow/Flow;->move(Lflow/Flow$PendingTraversal;)V

    return-void
.end method
