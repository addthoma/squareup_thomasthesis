.class public abstract Lflow/path/Path;
.super Ljava/lang/Object;
.source "Path.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lflow/path/Path$ContextFactory;,
        Lflow/path/Path$Builder;
    }
.end annotation


# static fields
.field static final ROOT:Lflow/path/Path;


# instance fields
.field private transient elements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lflow/path/Path;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lflow/path/Path$1;

    invoke-direct {v0}, Lflow/path/Path$1;-><init>()V

    sput-object v0, Lflow/path/Path;->ROOT:Lflow/path/Path;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static contextFactory()Lflow/path/PathContextFactory;
    .locals 1

    .line 30
    new-instance v0, Lflow/path/Path$ContextFactory;

    invoke-direct {v0}, Lflow/path/Path$ContextFactory;-><init>()V

    return-object v0
.end method

.method public static contextFactory(Lflow/path/PathContextFactory;)Lflow/path/PathContextFactory;
    .locals 1

    .line 34
    new-instance v0, Lflow/path/Path$ContextFactory;

    invoke-direct {v0, p0}, Lflow/path/Path$ContextFactory;-><init>(Lflow/path/PathContextFactory;)V

    return-object v0
.end method

.method public static get(Landroid/content/Context;)Lflow/path/Path;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lflow/path/Path;",
            ">(",
            "Landroid/content/Context;",
            ")TT;"
        }
    .end annotation

    .line 38
    invoke-static {p0}, Lflow/path/FlowPathContextWrapper;->get(Landroid/content/Context;)Lflow/path/FlowPathContextWrapper;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 44
    iget-object p0, p0, Lflow/path/FlowPathContextWrapper;->localScreen:Ljava/lang/Object;

    check-cast p0, Lflow/path/Path;

    return-object p0

    .line 40
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Supplied context has no Path"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method protected build(Lflow/path/Path$Builder;)V
    .locals 0

    return-void
.end method

.method protected final elements()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lflow/path/Path;",
            ">;"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lflow/path/Path;->elements:Ljava/util/List;

    if-nez v0, :cond_1

    .line 52
    new-instance v0, Lflow/path/Path$Builder;

    invoke-direct {v0}, Lflow/path/Path$Builder;-><init>()V

    .line 53
    invoke-virtual {p0, v0}, Lflow/path/Path;->build(Lflow/path/Path$Builder;)V

    .line 55
    invoke-static {v0, p0}, Lflow/path/Path$Builder;->access$000(Lflow/path/Path$Builder;Lflow/path/Path;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    invoke-virtual {v0, p0}, Lflow/path/Path$Builder;->append(Lflow/path/Path;)V

    .line 58
    :cond_0
    invoke-static {v0}, Lflow/path/Path$Builder;->access$100(Lflow/path/Path$Builder;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflow/path/Path;->elements:Ljava/util/List;

    .line 60
    :cond_1
    iget-object v0, p0, Lflow/path/Path;->elements:Ljava/util/List;

    return-object v0
.end method

.method final isRoot()Z
    .locals 1

    .line 64
    sget-object v0, Lflow/path/Path;->ROOT:Lflow/path/Path;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
