.class Lcom/starmicronics/starmgsio/o;
.super Lcom/starmicronics/starmgsio/A;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starmgsio/o$a;
    }
.end annotation


# instance fields
.field private a:[B

.field private b:Lcom/starmicronics/starmgsio/ScaleData$Unit;

.field private c:Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;

.field private d:Lcom/starmicronics/starmgsio/ScaleData$DataType;

.field private e:Lcom/starmicronics/starmgsio/ScaleData$Status;

.field private f:Lcom/starmicronics/starmgsio/K;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/starmicronics/starmgsio/A;-><init>()V

    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/starmicronics/starmgsio/o;->a:[B

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    iput-object v0, p0, Lcom/starmicronics/starmgsio/o;->b:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;

    iput-object v0, p0, Lcom/starmicronics/starmgsio/o;->c:Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    iput-object v0, p0, Lcom/starmicronics/starmgsio/o;->d:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$Status;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$Status;

    iput-object v0, p0, Lcom/starmicronics/starmgsio/o;->e:Lcom/starmicronics/starmgsio/ScaleData$Status;

    new-instance v0, Lcom/starmicronics/starmgsio/K$a;

    invoke-direct {v0}, Lcom/starmicronics/starmgsio/K$a;-><init>()V

    invoke-virtual {v0}, Lcom/starmicronics/starmgsio/K$a;->a()Lcom/starmicronics/starmgsio/K;

    move-result-object v0

    iput-object v0, p0, Lcom/starmicronics/starmgsio/o;->f:Lcom/starmicronics/starmgsio/K;

    return-void
.end method


# virtual methods
.method protected a(Lcom/starmicronics/starmgsio/K;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/o;->f:Lcom/starmicronics/starmgsio/K;

    return-void
.end method

.method protected a(Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/o;->c:Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;

    return-void
.end method

.method protected a(Lcom/starmicronics/starmgsio/ScaleData$DataType;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/o;->d:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    return-void
.end method

.method protected a(Lcom/starmicronics/starmgsio/ScaleData$Status;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/o;->e:Lcom/starmicronics/starmgsio/ScaleData$Status;

    return-void
.end method

.method protected a(Lcom/starmicronics/starmgsio/ScaleData$Unit;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/o;->b:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    return-void
.end method

.method protected a([B)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/o;->a:[B

    return-void
.end method

.method protected a()Z
    .locals 2

    iget-object v0, p0, Lcom/starmicronics/starmgsio/o;->a:[B

    if-eqz v0, :cond_1

    array-length v0, v0

    const/16 v1, 0x1a

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/o;->b:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/o;->c:Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/o;->d:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$DataType;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/o;->e:Lcom/starmicronics/starmgsio/ScaleData$Status;

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Status;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$Status;

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/starmicronics/starmgsio/o;->getNumberOfDecimalPlaces()I

    move-result v0

    if-gez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public getComparatorResult()Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/o;->c:Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;

    return-object v0
.end method

.method public getDataType()Lcom/starmicronics/starmgsio/ScaleData$DataType;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/o;->d:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    return-object v0
.end method

.method public getNumberOfDecimalPlaces()I
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/o;->f:Lcom/starmicronics/starmgsio/K;

    invoke-virtual {v0}, Lcom/starmicronics/starmgsio/K;->a()I

    move-result v0

    return v0
.end method

.method public getRawString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/starmicronics/starmgsio/o;->a:[B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    return-object v0
.end method

.method public getStatus()Lcom/starmicronics/starmgsio/ScaleData$Status;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/o;->e:Lcom/starmicronics/starmgsio/ScaleData$Status;

    return-object v0
.end method

.method public getUnit()Lcom/starmicronics/starmgsio/ScaleData$Unit;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/o;->b:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    return-object v0
.end method

.method public getWeight()D
    .locals 2

    iget-object v0, p0, Lcom/starmicronics/starmgsio/o;->f:Lcom/starmicronics/starmgsio/K;

    invoke-virtual {v0}, Lcom/starmicronics/starmgsio/K;->b()D

    move-result-wide v0

    return-wide v0
.end method
