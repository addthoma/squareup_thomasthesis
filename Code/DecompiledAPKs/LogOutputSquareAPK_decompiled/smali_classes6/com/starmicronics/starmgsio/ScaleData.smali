.class public interface abstract Lcom/starmicronics/starmgsio/ScaleData;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starmgsio/ScaleData$Status;,
        Lcom/starmicronics/starmgsio/ScaleData$DataType;,
        Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;,
        Lcom/starmicronics/starmgsio/ScaleData$Unit;
    }
.end annotation


# virtual methods
.method public abstract getComparatorResult()Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;
.end method

.method public abstract getDataType()Lcom/starmicronics/starmgsio/ScaleData$DataType;
.end method

.method public abstract getNumberOfDecimalPlaces()I
.end method

.method public abstract getRawString()Ljava/lang/String;
.end method

.method public abstract getStatus()Lcom/starmicronics/starmgsio/ScaleData$Status;
.end method

.method public abstract getUnit()Lcom/starmicronics/starmgsio/ScaleData$Unit;
.end method

.method public abstract getWeight()D
.end method
