.class Lcom/starmicronics/starmgsio/m;
.super Lcom/starmicronics/starmgsio/Scale;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starmgsio/m$b;,
        Lcom/starmicronics/starmgsio/m$a;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/os/Handler;

.field private c:Ljava/lang/String;

.field private d:Landroid/bluetooth/BluetoothAdapter;

.field private e:Ljava/util/concurrent/Semaphore;

.field private f:I

.field private g:I

.field private h:Landroid/bluetooth/BluetoothGatt;

.field private i:Landroid/bluetooth/BluetoothGattCharacteristic;

.field private j:Lcom/starmicronics/starmgsio/ScaleCallback;

.field private k:[B

.field private l:[B

.field private final m:Landroid/bluetooth/BluetoothGattCallback;


# direct methods
.method constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Lcom/starmicronics/starmgsio/Scale;-><init>()V

    new-instance v0, Lcom/starmicronics/starmgsio/g;

    invoke-direct {v0, p0}, Lcom/starmicronics/starmgsio/g;-><init>(Lcom/starmicronics/starmgsio/m;)V

    iput-object v0, p0, Lcom/starmicronics/starmgsio/m;->m:Landroid/bluetooth/BluetoothGattCallback;

    iput-object p1, p0, Lcom/starmicronics/starmgsio/m;->c:Ljava/lang/String;

    iput-object p2, p0, Lcom/starmicronics/starmgsio/m;->a:Landroid/content/Context;

    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-direct {p1, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/starmicronics/starmgsio/m;->b:Landroid/os/Handler;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object p1

    iput-object p1, p0, Lcom/starmicronics/starmgsio/m;->d:Landroid/bluetooth/BluetoothAdapter;

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/m;->j:Lcom/starmicronics/starmgsio/ScaleCallback;

    const/4 p1, 0x0

    iput p1, p0, Lcom/starmicronics/starmgsio/m;->f:I

    const/4 p1, 0x2

    iput p1, p0, Lcom/starmicronics/starmgsio/m;->g:I

    return-void
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/m;Landroid/bluetooth/BluetoothGatt;)Landroid/bluetooth/BluetoothGatt;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/m;->h:Landroid/bluetooth/BluetoothGatt;

    return-object p1
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/m;Landroid/bluetooth/BluetoothGattCharacteristic;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/m;->i:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object p1
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/m;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/m;->a:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/m;Ljava/util/concurrent/Semaphore;)Ljava/util/concurrent/Semaphore;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/m;->e:Ljava/util/concurrent/Semaphore;

    return-object p1
.end method

.method private a()V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/m;->h:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/starmgsio/m;->h:Landroid/bluetooth/BluetoothGatt;

    iput-object v0, p0, Lcom/starmicronics/starmgsio/m;->i:Landroid/bluetooth/BluetoothGattCharacteristic;

    const/4 v0, 0x0

    iput v0, p0, Lcom/starmicronics/starmgsio/m;->f:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/starmicronics/starmgsio/m;->g:I

    return-void
.end method

.method private a(I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/m;->j:Lcom/starmicronics/starmgsio/ScaleCallback;

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/m;->b:Landroid/os/Handler;

    new-instance v1, Lcom/starmicronics/starmgsio/h;

    invoke-direct {v1, p0, p0, p1}, Lcom/starmicronics/starmgsio/h;-><init>(Lcom/starmicronics/starmgsio/m;Lcom/starmicronics/starmgsio/Scale;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private a(Lcom/starmicronics/starmgsio/ScaleData;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/m;->j:Lcom/starmicronics/starmgsio/ScaleCallback;

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/m;->b:Landroid/os/Handler;

    new-instance v1, Lcom/starmicronics/starmgsio/j;

    invoke-direct {v1, p0, p0, p1}, Lcom/starmicronics/starmgsio/j;-><init>(Lcom/starmicronics/starmgsio/m;Lcom/starmicronics/starmgsio/Scale;Lcom/starmicronics/starmgsio/ScaleData;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private a(Lcom/starmicronics/starmgsio/ScaleSetting;I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/m;->j:Lcom/starmicronics/starmgsio/ScaleCallback;

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/m;->b:Landroid/os/Handler;

    new-instance v1, Lcom/starmicronics/starmgsio/k;

    invoke-direct {v1, p0, p0, p1, p2}, Lcom/starmicronics/starmgsio/k;-><init>(Lcom/starmicronics/starmgsio/m;Lcom/starmicronics/starmgsio/Scale;Lcom/starmicronics/starmgsio/ScaleSetting;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/m;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/starmicronics/starmgsio/m;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/m;Lcom/starmicronics/starmgsio/ScaleData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/starmicronics/starmgsio/m;->a(Lcom/starmicronics/starmgsio/ScaleData;)V

    return-void
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/m;Lcom/starmicronics/starmgsio/ScaleSetting;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/starmicronics/starmgsio/m;->a(Lcom/starmicronics/starmgsio/ScaleSetting;I)V

    return-void
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/m;[B)[B
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/m;->k:[B

    return-object p1
.end method

.method static synthetic b(Lcom/starmicronics/starmgsio/m;I)I
    .locals 0

    iput p1, p0, Lcom/starmicronics/starmgsio/m;->g:I

    return p1
.end method

.method static synthetic b(Lcom/starmicronics/starmgsio/m;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/m;->b:Landroid/os/Handler;

    return-object p0
.end method

.method private b()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/starmicronics/starmgsio/m;->f:I

    invoke-direct {p0}, Lcom/starmicronics/starmgsio/m;->a()V

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/16 v0, 0x68

    :goto_0
    invoke-direct {p0, v0}, Lcom/starmicronics/starmgsio/m;->b(I)V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/m;->j:Lcom/starmicronics/starmgsio/ScaleCallback;

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/m;->b:Landroid/os/Handler;

    new-instance v1, Lcom/starmicronics/starmgsio/i;

    invoke-direct {v1, p0, p0, p1}, Lcom/starmicronics/starmgsio/i;-><init>(Lcom/starmicronics/starmgsio/m;Lcom/starmicronics/starmgsio/Scale;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method static synthetic b(Lcom/starmicronics/starmgsio/m;[B)[B
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/m;->l:[B

    return-object p1
.end method

.method static synthetic c(Lcom/starmicronics/starmgsio/m;I)I
    .locals 0

    iput p1, p0, Lcom/starmicronics/starmgsio/m;->f:I

    return p1
.end method

.method static synthetic c(Lcom/starmicronics/starmgsio/m;)V
    .locals 0

    invoke-direct {p0}, Lcom/starmicronics/starmgsio/m;->a()V

    return-void
.end method

.method static synthetic d(Lcom/starmicronics/starmgsio/m;)I
    .locals 0

    iget p0, p0, Lcom/starmicronics/starmgsio/m;->g:I

    return p0
.end method

.method static synthetic e(Lcom/starmicronics/starmgsio/m;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/m;->i:Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object p0
.end method

.method static synthetic f(Lcom/starmicronics/starmgsio/m;)V
    .locals 0

    invoke-direct {p0}, Lcom/starmicronics/starmgsio/m;->b()V

    return-void
.end method

.method static synthetic g(Lcom/starmicronics/starmgsio/m;)Lcom/starmicronics/starmgsio/ScaleCallback;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/m;->j:Lcom/starmicronics/starmgsio/ScaleCallback;

    return-object p0
.end method

.method static synthetic h(Lcom/starmicronics/starmgsio/m;)Landroid/bluetooth/BluetoothAdapter;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/m;->d:Landroid/bluetooth/BluetoothAdapter;

    return-object p0
.end method

.method static synthetic i(Lcom/starmicronics/starmgsio/m;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/m;->c:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic j(Lcom/starmicronics/starmgsio/m;)I
    .locals 0

    iget p0, p0, Lcom/starmicronics/starmgsio/m;->f:I

    return p0
.end method

.method static synthetic k(Lcom/starmicronics/starmgsio/m;)[B
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/m;->k:[B

    return-object p0
.end method

.method static synthetic l(Lcom/starmicronics/starmgsio/m;)[B
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/m;->l:[B

    return-object p0
.end method

.method static synthetic m(Lcom/starmicronics/starmgsio/m;)Ljava/util/concurrent/Semaphore;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/m;->e:Ljava/util/concurrent/Semaphore;

    return-object p0
.end method

.method static synthetic n(Lcom/starmicronics/starmgsio/m;)Landroid/bluetooth/BluetoothGatt;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/m;->h:Landroid/bluetooth/BluetoothGatt;

    return-object p0
.end method

.method static synthetic o(Lcom/starmicronics/starmgsio/m;)Landroid/bluetooth/BluetoothGattCallback;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/m;->m:Landroid/bluetooth/BluetoothGattCallback;

    return-object p0
.end method


# virtual methods
.method public connect(Lcom/starmicronics/starmgsio/ScaleCallback;)V
    .locals 2

    iput-object p1, p0, Lcom/starmicronics/starmgsio/m;->j:Lcom/starmicronics/starmgsio/ScaleCallback;

    new-instance p1, Lcom/starmicronics/starmgsio/e;

    invoke-direct {p1, p0}, Lcom/starmicronics/starmgsio/e;-><init>(Lcom/starmicronics/starmgsio/m;)V

    new-instance v0, Lcom/starmicronics/starmgsio/m$a;

    const/16 v1, 0x2710

    invoke-direct {v0, p0, v1, p1}, Lcom/starmicronics/starmgsio/m$a;-><init>(Lcom/starmicronics/starmgsio/m;ILcom/starmicronics/starmgsio/N;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public disconnect()V
    .locals 2

    iget-object v0, p0, Lcom/starmicronics/starmgsio/m;->h:Landroid/bluetooth/BluetoothGatt;

    if-nez v0, :cond_0

    const/16 v0, 0x66

    invoke-direct {p0, v0}, Lcom/starmicronics/starmgsio/m;->b(I)V

    return-void

    :cond_0
    const/4 v1, 0x1

    iput v1, p0, Lcom/starmicronics/starmgsio/m;->f:I

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->disconnect()V

    return-void
.end method

.method public updateSetting(Lcom/starmicronics/starmgsio/ScaleSetting;)V
    .locals 3

    new-instance v0, Lcom/starmicronics/starmgsio/f;

    invoke-direct {v0, p0, p1}, Lcom/starmicronics/starmgsio/f;-><init>(Lcom/starmicronics/starmgsio/m;Lcom/starmicronics/starmgsio/ScaleSetting;)V

    new-instance v1, Lcom/starmicronics/starmgsio/m$b;

    const/16 v2, 0x2710

    invoke-direct {v1, p0, p1, v2, v0}, Lcom/starmicronics/starmgsio/m$b;-><init>(Lcom/starmicronics/starmgsio/m;Lcom/starmicronics/starmgsio/ScaleSetting;ILcom/starmicronics/starmgsio/N;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method
