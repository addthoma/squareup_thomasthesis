.class public final enum Lcom/starmicronics/starmgsio/p$b;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starmgsio/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starmgsio/p$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/starmicronics/starmgsio/p$b;

.field public static final enum b:Lcom/starmicronics/starmgsio/p$b;

.field public static final enum c:Lcom/starmicronics/starmgsio/p$b;

.field public static final enum d:Lcom/starmicronics/starmgsio/p$b;

.field public static final enum e:Lcom/starmicronics/starmgsio/p$b;

.field public static final enum f:Lcom/starmicronics/starmgsio/p$b;

.field public static final enum g:Lcom/starmicronics/starmgsio/p$b;

.field private static final synthetic h:[Lcom/starmicronics/starmgsio/p$b;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    new-instance v0, Lcom/starmicronics/starmgsio/p$b;

    const/4 v1, 0x0

    const-string v2, "RESULT_SUCCESS"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starmgsio/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/p$b;->a:Lcom/starmicronics/starmgsio/p$b;

    new-instance v0, Lcom/starmicronics/starmgsio/p$b;

    const/4 v2, 0x1

    const-string v3, "RESULT_NOW_CONNECTING"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starmgsio/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/p$b;->b:Lcom/starmicronics/starmgsio/p$b;

    new-instance v0, Lcom/starmicronics/starmgsio/p$b;

    const/4 v3, 0x2

    const-string v4, "RESULT_ALREADY_OPENED"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starmgsio/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/p$b;->c:Lcom/starmicronics/starmgsio/p$b;

    new-instance v0, Lcom/starmicronics/starmgsio/p$b;

    const/4 v4, 0x3

    const-string v5, "RESULT_NOT_FIND_DEVICE"

    invoke-direct {v0, v5, v4}, Lcom/starmicronics/starmgsio/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/p$b;->d:Lcom/starmicronics/starmgsio/p$b;

    new-instance v0, Lcom/starmicronics/starmgsio/p$b;

    const/4 v5, 0x4

    const-string v6, "RESULT_FAILED"

    invoke-direct {v0, v6, v5}, Lcom/starmicronics/starmgsio/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/p$b;->e:Lcom/starmicronics/starmgsio/p$b;

    new-instance v0, Lcom/starmicronics/starmgsio/p$b;

    const/4 v6, 0x5

    const-string v7, "RESULT_NOT_GRANTED_PERMISSION"

    invoke-direct {v0, v7, v6}, Lcom/starmicronics/starmgsio/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/p$b;->f:Lcom/starmicronics/starmgsio/p$b;

    new-instance v0, Lcom/starmicronics/starmgsio/p$b;

    const/4 v7, 0x6

    const-string v8, "RESULT_UNKNOWN_ERROR"

    invoke-direct {v0, v8, v7}, Lcom/starmicronics/starmgsio/p$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/p$b;->g:Lcom/starmicronics/starmgsio/p$b;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/starmicronics/starmgsio/p$b;

    sget-object v8, Lcom/starmicronics/starmgsio/p$b;->a:Lcom/starmicronics/starmgsio/p$b;

    aput-object v8, v0, v1

    sget-object v1, Lcom/starmicronics/starmgsio/p$b;->b:Lcom/starmicronics/starmgsio/p$b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starmgsio/p$b;->c:Lcom/starmicronics/starmgsio/p$b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/starmicronics/starmgsio/p$b;->d:Lcom/starmicronics/starmgsio/p$b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/starmicronics/starmgsio/p$b;->e:Lcom/starmicronics/starmgsio/p$b;

    aput-object v1, v0, v5

    sget-object v1, Lcom/starmicronics/starmgsio/p$b;->f:Lcom/starmicronics/starmgsio/p$b;

    aput-object v1, v0, v6

    sget-object v1, Lcom/starmicronics/starmgsio/p$b;->g:Lcom/starmicronics/starmgsio/p$b;

    aput-object v1, v0, v7

    sput-object v0, Lcom/starmicronics/starmgsio/p$b;->h:[Lcom/starmicronics/starmgsio/p$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starmgsio/p$b;
    .locals 1

    const-class v0, Lcom/starmicronics/starmgsio/p$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starmgsio/p$b;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starmgsio/p$b;
    .locals 1

    sget-object v0, Lcom/starmicronics/starmgsio/p$b;->h:[Lcom/starmicronics/starmgsio/p$b;

    invoke-virtual {v0}, [Lcom/starmicronics/starmgsio/p$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starmgsio/p$b;

    return-object v0
.end method
