.class Lcom/starmicronics/stario/TCPPort$a;
.super Ljava/lang/Thread;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/stario/TCPPort;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field a:I

.field final synthetic b:Lcom/starmicronics/stario/TCPPort;


# direct methods
.method constructor <init>(Lcom/starmicronics/stario/TCPPort;I)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/stario/TCPPort$a;->b:Lcom/starmicronics/stario/TCPPort;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput p2, p0, Lcom/starmicronics/stario/TCPPort$a;->a:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort$a;->b:Lcom/starmicronics/stario/TCPPort;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/starmicronics/stario/TCPPort;->a(Lcom/starmicronics/stario/TCPPort;J)J

    :goto_0
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort$a;->b:Lcom/starmicronics/stario/TCPPort;

    invoke-static {v0}, Lcom/starmicronics/stario/TCPPort;->a(Lcom/starmicronics/stario/TCPPort;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x3e8

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    iget-object v0, p0, Lcom/starmicronics/stario/TCPPort$a;->b:Lcom/starmicronics/stario/TCPPort;

    invoke-static {v0}, Lcom/starmicronics/stario/TCPPort;->b(Lcom/starmicronics/stario/TCPPort;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    :try_start_1
    iget v1, p0, Lcom/starmicronics/stario/TCPPort$a;->a:I

    int-to-long v1, v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-object v5, p0, Lcom/starmicronics/stario/TCPPort$a;->b:Lcom/starmicronics/stario/TCPPort;

    invoke-static {v5}, Lcom/starmicronics/stario/TCPPort;->c(Lcom/starmicronics/stario/TCPPort;)J

    move-result-wide v5

    sub-long/2addr v3, v5

    cmp-long v5, v1, v3

    if-gez v5, :cond_0

    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort$a;->b:Lcom/starmicronics/stario/TCPPort;

    iget-object v1, v1, Lcom/starmicronics/stario/TCPPort;->d:Ljava/io/DataOutputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_0

    :try_start_2
    iget-object v1, p0, Lcom/starmicronics/stario/TCPPort$a;->b:Lcom/starmicronics/stario/TCPPort;

    iget-object v1, v1, Lcom/starmicronics/stario/TCPPort;->d:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    monitor-exit v0

    goto :goto_1

    :catch_1
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    :cond_1
    :goto_1
    return-void
.end method
