.class final enum Lcom/starmicronics/stario/StarIOPort$b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/stario/StarIOPort;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/stario/StarIOPort$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/starmicronics/stario/StarIOPort$b;

.field public static final enum b:Lcom/starmicronics/stario/StarIOPort$b;

.field private static final synthetic c:[Lcom/starmicronics/stario/StarIOPort$b;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/starmicronics/stario/StarIOPort$b;

    const/4 v1, 0x0

    const-string v2, "STAR"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/stario/StarIOPort$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/stario/StarIOPort$b;->a:Lcom/starmicronics/stario/StarIOPort$b;

    new-instance v0, Lcom/starmicronics/stario/StarIOPort$b;

    const/4 v2, 0x1

    const-string v3, "ESCPOS"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/stario/StarIOPort$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/stario/StarIOPort$b;->b:Lcom/starmicronics/stario/StarIOPort$b;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/starmicronics/stario/StarIOPort$b;

    sget-object v3, Lcom/starmicronics/stario/StarIOPort$b;->a:Lcom/starmicronics/stario/StarIOPort$b;

    aput-object v3, v0, v1

    sget-object v1, Lcom/starmicronics/stario/StarIOPort$b;->b:Lcom/starmicronics/stario/StarIOPort$b;

    aput-object v1, v0, v2

    sput-object v0, Lcom/starmicronics/stario/StarIOPort$b;->c:[Lcom/starmicronics/stario/StarIOPort$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/stario/StarIOPort$b;
    .locals 1

    const-class v0, Lcom/starmicronics/stario/StarIOPort$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/stario/StarIOPort$b;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/stario/StarIOPort$b;
    .locals 1

    sget-object v0, Lcom/starmicronics/stario/StarIOPort$b;->c:[Lcom/starmicronics/stario/StarIOPort$b;

    invoke-virtual {v0}, [Lcom/starmicronics/stario/StarIOPort$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/stario/StarIOPort$b;

    return-object v0
.end method
