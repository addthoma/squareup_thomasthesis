.class interface abstract Lcom/starmicronics/stario/c;
.super Ljava/lang/Object;


# virtual methods
.method public abstract apply()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation
.end method

.method public abstract close()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation
.end method

.method public abstract getAutoConnect()Z
.end method

.method public abstract getAutoConnectCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
.end method

.method public abstract getBluetoothDeviceName()Ljava/lang/String;
.end method

.method public abstract getBluetoothDeviceNameCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
.end method

.method public abstract getBluetoothDiagnosticMode()Z
.end method

.method public abstract getBluetoothDiagnosticModeCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
.end method

.method public abstract getDiscoveryPermission()Z
.end method

.method public abstract getDiscoveryPermissionCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
.end method

.method public abstract getPairingPermission()Z
.end method

.method public abstract getPairingPermissionCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
.end method

.method public abstract getPinCode()Ljava/lang/String;
.end method

.method public abstract getPinCodeCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
.end method

.method public abstract getSecurityType()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;
.end method

.method public abstract getSecurityTypeCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
.end method

.method public abstract getiOSPortName()Ljava/lang/String;
.end method

.method public abstract getiOSPortNameCapability()Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSettingCapability;
.end method

.method public abstract isOpened()Z
.end method

.method public abstract loadSetting()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation
.end method

.method public abstract open()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation
.end method

.method public abstract setAutoConnect(Z)V
.end method

.method public abstract setBluetoothDeviceName(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation
.end method

.method public abstract setBluetoothDiagnosticMode(Z)V
.end method

.method public abstract setDiscoveryPermission(Z)V
.end method

.method public abstract setPairingPermission(Z)V
.end method

.method public abstract setPinCode(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation
.end method

.method public abstract setSecurityType(Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;)V
.end method

.method public abstract setiOSPortName(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation
.end method
