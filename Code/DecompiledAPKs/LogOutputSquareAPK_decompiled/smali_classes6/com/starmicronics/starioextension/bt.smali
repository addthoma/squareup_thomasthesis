.class Lcom/starmicronics/starioextension/bt;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starioextension/bt$a;,
        Lcom/starmicronics/starioextension/bt$b;
    }
.end annotation


# static fields
.field private static final a:I = 0x2c


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([B)Lcom/starmicronics/starioextension/bt$a;
    .locals 10

    array-length v0, p0

    const-string v1, "Unsupported sound format. Please refer to the SDK manual for supported format."

    const/16 v2, 0x2c

    if-lt v0, v2, :cond_8

    new-instance v0, Lcom/starmicronics/starioextension/bt$a;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/bt$a;-><init>()V

    const/4 v2, 0x0

    aget-byte v3, p0, v2

    const/16 v4, 0x52

    if-ne v3, v4, :cond_7

    const/4 v3, 0x1

    aget-byte v4, p0, v3

    const/16 v5, 0x49

    if-ne v4, v5, :cond_7

    const/4 v4, 0x2

    aget-byte v4, p0, v4

    const/16 v5, 0x46

    if-ne v4, v5, :cond_7

    const/4 v4, 0x3

    aget-byte v4, p0, v4

    if-ne v4, v5, :cond_7

    const/16 v4, 0x8

    aget-byte v5, p0, v4

    const/16 v6, 0x57

    if-ne v5, v6, :cond_6

    const/16 v5, 0x9

    aget-byte v5, p0, v5

    const/16 v6, 0x41

    if-ne v5, v6, :cond_6

    const/16 v5, 0xa

    aget-byte v5, p0, v5

    const/16 v6, 0x56

    if-ne v5, v6, :cond_6

    const/16 v5, 0xb

    aget-byte v5, p0, v5

    const/16 v6, 0x45

    if-ne v5, v6, :cond_6

    const/16 v5, 0xc

    aget-byte v5, p0, v5

    const/16 v6, 0x66

    if-ne v5, v6, :cond_5

    const/16 v5, 0xd

    aget-byte v5, p0, v5

    const/16 v6, 0x6d

    if-ne v5, v6, :cond_5

    const/16 v5, 0xe

    aget-byte v5, p0, v5

    const/16 v6, 0x74

    if-ne v5, v6, :cond_5

    const/16 v5, 0xf

    aget-byte v5, p0, v5

    const/16 v7, 0x20

    if-ne v5, v7, :cond_5

    const/16 v5, 0x10

    const/16 v8, 0x14

    invoke-static {p0, v5, v8}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    sget-object v9, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v5, v9}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/starmicronics/starioextension/bt$a;->b(I)V

    const/16 v5, 0x16

    invoke-static {p0, v8, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v8

    invoke-static {v8}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    sget-object v9, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v8, v9}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v8

    invoke-static {v8}, Lcom/starmicronics/starioextension/bt;->a(I)Lcom/starmicronics/starioextension/bt$b;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/starmicronics/starioextension/bt$a;->a(Lcom/starmicronics/starioextension/bt$b;)V

    const/16 v8, 0x18

    invoke-static {p0, v5, v8}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    sget-object v9, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v5, v9}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v5

    invoke-virtual {v0, v5}, Lcom/starmicronics/starioextension/bt$a;->c(I)V

    const/16 v5, 0x1c

    invoke-static {p0, v8, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v8

    invoke-static {v8}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    sget-object v9, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v8, v9}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v8

    invoke-virtual {v0, v8}, Lcom/starmicronics/starioextension/bt$a;->d(I)V

    invoke-static {p0, v5, v7}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    sget-object v8, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v5, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/starmicronics/starioextension/bt$a;->e(I)V

    const/16 v5, 0x22

    invoke-static {p0, v7, v5}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v7

    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    sget-object v8, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v7

    invoke-virtual {v0, v7}, Lcom/starmicronics/starioextension/bt$a;->g(I)V

    const/16 v7, 0x24

    invoke-static {p0, v5, v7}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    sget-object v7, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v5

    invoke-virtual {v0, v5}, Lcom/starmicronics/starioextension/bt$a;->g(I)V

    const/16 v7, 0x24

    :goto_0
    array-length v8, p0

    add-int/lit8 v8, v8, -0x4

    if-ge v7, v8, :cond_1

    aget-byte v8, p0, v7

    const/16 v9, 0x64

    if-ne v8, v9, :cond_0

    add-int/lit8 v8, v7, 0x1

    aget-byte v8, p0, v8

    const/16 v9, 0x61

    if-ne v8, v9, :cond_0

    add-int/lit8 v8, v7, 0x2

    aget-byte v8, p0, v8

    if-ne v8, v6, :cond_0

    add-int/lit8 v8, v7, 0x3

    aget-byte v8, p0, v8

    const/16 v9, 0x61

    if-ne v8, v9, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_4

    add-int/lit8 v7, v7, 0x4

    add-int/lit8 v1, v7, 0x4

    invoke-static {p0, v7, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v3, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/starmicronics/starioextension/bt$a;->h(I)V

    invoke-virtual {v0}, Lcom/starmicronics/starioextension/bt$a;->i()I

    move-result v3

    add-int/2addr v3, v1

    invoke-static {p0, v1, v3}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object p0

    if-ne v5, v4, :cond_3

    :goto_2
    array-length v1, p0

    if-ge v2, v1, :cond_3

    aget-byte v1, p0, v2

    if-lez v1, :cond_2

    add-int/lit8 v1, v1, -0x80

    int-to-byte v1, v1

    aput-byte v1, p0, v2

    goto :goto_3

    :cond_2
    add-int/lit16 v1, v1, 0x80

    int-to-byte v1, v1

    aput-byte v1, p0, v2

    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    invoke-virtual {v0, p0}, Lcom/starmicronics/starioextension/bt$a;->a([B)V

    return-object v0

    :cond_4
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_5
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_6
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_7
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_8
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static a(I)Lcom/starmicronics/starioextension/bt$b;
    .locals 2

    sget-object v0, Lcom/starmicronics/starioextension/bt$b;->a:Lcom/starmicronics/starioextension/bt$b;

    const/4 v1, 0x1

    if-eq p0, v1, :cond_4

    const/4 v1, 0x2

    if-eq p0, v1, :cond_3

    const/4 v1, 0x5

    if-eq p0, v1, :cond_2

    const/4 v1, 0x6

    if-eq p0, v1, :cond_1

    const/4 v1, 0x7

    if-eq p0, v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/starmicronics/starioextension/bt$b;->f:Lcom/starmicronics/starioextension/bt$b;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/starmicronics/starioextension/bt$b;->e:Lcom/starmicronics/starioextension/bt$b;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/starmicronics/starioextension/bt$b;->d:Lcom/starmicronics/starioextension/bt$b;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/starmicronics/starioextension/bt$b;->c:Lcom/starmicronics/starioextension/bt$b;

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/starmicronics/starioextension/bt$b;->b:Lcom/starmicronics/starioextension/bt$b;

    :goto_0
    return-object v0
.end method
