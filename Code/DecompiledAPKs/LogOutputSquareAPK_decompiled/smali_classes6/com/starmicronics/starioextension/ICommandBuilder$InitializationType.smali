.class public final enum Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/ICommandBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InitializationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;

.field public static final enum Command:Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;

    const/4 v1, 0x0

    const-string v2, "Command"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;->Command:Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;

    sget-object v2, Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;->Command:Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;->$VALUES:[Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;->$VALUES:[Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/ICommandBuilder$InitializationType;

    return-object v0
.end method
