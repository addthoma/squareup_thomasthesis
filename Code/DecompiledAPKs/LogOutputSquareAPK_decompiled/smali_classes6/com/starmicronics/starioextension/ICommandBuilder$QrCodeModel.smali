.class public final enum Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/ICommandBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QrCodeModel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;

.field public static final enum No1:Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;

.field public static final enum No2:Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;

    const/4 v1, 0x0

    const-string v2, "No1"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;->No1:Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;

    const/4 v2, 0x1

    const-string v3, "No2"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;->No2:Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;

    sget-object v3, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;->No1:Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;

    aput-object v3, v0, v1

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;->No2:Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;

    aput-object v1, v0, v2

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;->$VALUES:[Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;->$VALUES:[Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/ICommandBuilder$QrCodeModel;

    return-object v0
.end method
