.class final enum Lcom/starmicronics/starioextension/ah$b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/ah;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/ah$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/starmicronics/starioextension/ah$b;

.field public static final enum b:Lcom/starmicronics/starioextension/ah$b;

.field public static final enum c:Lcom/starmicronics/starioextension/ah$b;

.field private static final synthetic d:[Lcom/starmicronics/starioextension/ah$b;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/starmicronics/starioextension/ah$b;

    const/4 v1, 0x0

    const-string v2, "Invalid"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/ah$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ah$b;->a:Lcom/starmicronics/starioextension/ah$b;

    new-instance v0, Lcom/starmicronics/starioextension/ah$b;

    const/4 v2, 0x1

    const-string v3, "No"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starioextension/ah$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ah$b;->b:Lcom/starmicronics/starioextension/ah$b;

    new-instance v0, Lcom/starmicronics/starioextension/ah$b;

    const/4 v3, 0x2

    const-string v4, "Yes"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starioextension/ah$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ah$b;->c:Lcom/starmicronics/starioextension/ah$b;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/starmicronics/starioextension/ah$b;

    sget-object v4, Lcom/starmicronics/starioextension/ah$b;->a:Lcom/starmicronics/starioextension/ah$b;

    aput-object v4, v0, v1

    sget-object v1, Lcom/starmicronics/starioextension/ah$b;->b:Lcom/starmicronics/starioextension/ah$b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ah$b;->c:Lcom/starmicronics/starioextension/ah$b;

    aput-object v1, v0, v3

    sput-object v0, Lcom/starmicronics/starioextension/ah$b;->d:[Lcom/starmicronics/starioextension/ah$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/ah$b;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/ah$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/ah$b;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/ah$b;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/ah$b;->d:[Lcom/starmicronics/starioextension/ah$b;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/ah$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/ah$b;

    return-object v0
.end method
