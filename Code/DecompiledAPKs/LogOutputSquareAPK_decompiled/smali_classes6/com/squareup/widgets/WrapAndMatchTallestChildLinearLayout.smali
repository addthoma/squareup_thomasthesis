.class public Lcom/squareup/widgets/WrapAndMatchTallestChildLinearLayout;
.super Landroid/widget/LinearLayout;
.source "WrapAndMatchTallestChildLinearLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 25
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/WrapAndMatchTallestChildLinearLayout;->setOrientation(I)V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 2

    .line 30
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    const/4 p1, 0x0

    const/high16 p2, -0x80000000

    const/4 p2, 0x0

    const/high16 v0, -0x80000000

    .line 33
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/widgets/WrapAndMatchTallestChildLinearLayout;->getChildCount()I

    move-result v1

    if-ge p2, v1, :cond_0

    .line 34
    invoke-virtual {p0, p2}, Lcom/squareup/widgets/WrapAndMatchTallestChildLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 35
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 39
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/widgets/WrapAndMatchTallestChildLinearLayout;->getChildCount()I

    move-result p2

    if-ge p1, p2, :cond_1

    .line 40
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/WrapAndMatchTallestChildLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object p2

    .line 41
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 42
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 44
    invoke-virtual {p2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method
