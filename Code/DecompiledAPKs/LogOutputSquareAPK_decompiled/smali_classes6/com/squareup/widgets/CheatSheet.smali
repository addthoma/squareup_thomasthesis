.class public Lcom/squareup/widgets/CheatSheet;
.super Ljava/lang/Object;
.source "CheatSheet.java"


# static fields
.field private static final ESTIMATED_TOAST_HEIGHT_DIPS:I = 0x30


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/view/View;Ljava/lang/CharSequence;)Z
    .locals 0

    .line 35
    invoke-static {p0, p1}, Lcom/squareup/widgets/CheatSheet;->showCheatSheet(Landroid/view/View;Ljava/lang/CharSequence;)Z

    move-result p0

    return p0
.end method

.method public static remove(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    .line 102
    invoke-virtual {p0, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-void
.end method

.method public static setup(Landroid/view/View;)V
    .locals 1

    .line 51
    new-instance v0, Lcom/squareup/widgets/CheatSheet$1;

    invoke-direct {v0}, Lcom/squareup/widgets/CheatSheet$1;-><init>()V

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-void
.end method

.method public static setup(Landroid/view/View;I)V
    .locals 1

    .line 69
    new-instance v0, Lcom/squareup/widgets/CheatSheet$2;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/CheatSheet$2;-><init>(I)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-void
.end method

.method public static setup(Landroid/view/View;Ljava/lang/CharSequence;)V
    .locals 1

    .line 87
    new-instance v0, Lcom/squareup/widgets/CheatSheet$3;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/CheatSheet$3;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-void
.end method

.method private static showCheatSheet(Landroid/view/View;Ljava/lang/CharSequence;)Z
    .locals 9

    .line 109
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x2

    new-array v2, v0, [I

    .line 114
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 115
    invoke-virtual {p0, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 116
    invoke-virtual {p0, v3}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 118
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 119
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    .line 120
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p0

    .line 121
    aget v6, v2, v1

    div-int/2addr v5, v0

    add-int/2addr v6, v5

    .line 122
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    const/high16 v7, 0x42400000    # 48.0f

    .line 124
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    mul-float v8, v8, v7

    float-to-int v7, v8

    .line 126
    invoke-static {v4, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    const/4 v4, 0x1

    .line 127
    aget v8, v2, v4

    if-ge v8, v7, :cond_1

    const/4 v1, 0x1

    :cond_1
    if-eqz v1, :cond_2

    const/16 v1, 0x31

    .line 131
    div-int/2addr v5, v0

    sub-int/2addr v6, v5

    aget v0, v2, v4

    iget v2, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v2

    add-int/2addr v0, p0

    invoke-virtual {p1, v1, v6, v0}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_0

    :cond_2
    const/16 p0, 0x51

    .line 137
    div-int/2addr v5, v0

    sub-int/2addr v6, v5

    iget v0, v3, Landroid/graphics/Rect;->bottom:I

    aget v1, v2, v4

    sub-int/2addr v0, v1

    invoke-virtual {p1, p0, v6, v0}, Landroid/widget/Toast;->setGravity(III)V

    .line 142
    :goto_0
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return v4
.end method
