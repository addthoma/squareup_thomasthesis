.class public Lcom/squareup/widgets/ScalingTextView;
.super Lcom/squareup/marketfont/MarketTextView;
.source "ScalingTextView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/ScalingTextView$SavedState;
    }
.end annotation


# instance fields
.field private lastMeasuredWidth:I

.field private maxTextSize:F

.field private minTextSize:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 34
    invoke-direct {p0, p1, v0}, Lcom/squareup/widgets/ScalingTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x1010084

    .line 38
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/widgets/ScalingTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    sget-object v0, Lcom/squareup/widgets/R$styleable;->ScalingTextView:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 44
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->getTextSize()F

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/ScalingTextView;->maxTextSize:F

    .line 45
    sget p2, Lcom/squareup/widgets/R$styleable;->ScalingTextView_minTextSize:I

    .line 46
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v0, Lcom/squareup/widgets/R$dimen;->scaling_text_view_minimum_font_size:I

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    int-to-float p3, p3

    .line 45
    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/ScalingTextView;->minTextSize:F

    .line 47
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 48
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->getGravity()I

    move-result p1

    or-int/lit8 p1, p1, 0x10

    invoke-virtual {p0, p1}, Lcom/squareup/widgets/ScalingTextView;->setGravity(I)V

    .line 49
    sget-object p1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, p1}, Lcom/squareup/widgets/ScalingTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    const/4 p1, 0x1

    .line 50
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/ScalingTextView;->setMaxLines(I)V

    .line 51
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->setSingleLine()V

    return-void
.end method

.method private getAvailableWidth(I)I
    .locals 3

    .line 78
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    .line 79
    aget-object v2, v0, v1

    if-eqz v2, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->getCompoundPaddingLeft()I

    move-result v2

    sub-int/2addr p1, v2

    .line 81
    aget-object v1, v0, v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sub-int/2addr p1, v1

    :cond_0
    const/4 v1, 0x2

    .line 83
    aget-object v2, v0, v1

    if-eqz v2, :cond_1

    .line 84
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->getCompoundPaddingRight()I

    move-result v2

    sub-int/2addr p1, v2

    .line 85
    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    sub-int/2addr p1, v0

    .line 89
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    sub-int/2addr p1, v0

    return p1
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 5

    .line 59
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 62
    iget v1, p0, Lcom/squareup/widgets/ScalingTextView;->lastMeasuredWidth:I

    if-eq v0, v1, :cond_0

    .line 63
    invoke-direct {p0, v0}, Lcom/squareup/widgets/ScalingTextView;->getAvailableWidth(I)I

    move-result v0

    .line 64
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    iget v3, p0, Lcom/squareup/widgets/ScalingTextView;->minTextSize:F

    iget v4, p0, Lcom/squareup/widgets/ScalingTextView;->maxTextSize:F

    invoke-static {v1, v2, v0, v3, v4}, Lcom/squareup/text/Fonts;->getFittedTextSize(Landroid/text/TextPaint;Ljava/lang/CharSequence;IFF)I

    move-result v0

    const/4 v1, 0x0

    int-to-float v0, v0

    .line 67
    invoke-super {p0, v1, v0}, Lcom/squareup/marketfont/MarketTextView;->setTextSize(IF)V

    .line 71
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/squareup/marketfont/MarketTextView;->onMeasure(II)V

    .line 73
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->getMeasuredWidth()I

    move-result p1

    iput p1, p0, Lcom/squareup/widgets/ScalingTextView;->lastMeasuredWidth:I

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 139
    check-cast p1, Lcom/squareup/widgets/ScalingTextView$SavedState;

    .line 140
    invoke-virtual {p1}, Lcom/squareup/widgets/ScalingTextView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/squareup/marketfont/MarketTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 141
    invoke-static {p1}, Lcom/squareup/widgets/ScalingTextView$SavedState;->access$100(Lcom/squareup/widgets/ScalingTextView$SavedState;)F

    move-result v0

    iput v0, p0, Lcom/squareup/widgets/ScalingTextView;->minTextSize:F

    .line 142
    invoke-static {p1}, Lcom/squareup/widgets/ScalingTextView$SavedState;->access$200(Lcom/squareup/widgets/ScalingTextView$SavedState;)F

    move-result p1

    iput p1, p0, Lcom/squareup/widgets/ScalingTextView;->maxTextSize:F

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 5

    .line 135
    new-instance v0, Lcom/squareup/widgets/ScalingTextView$SavedState;

    invoke-super {p0}, Lcom/squareup/marketfont/MarketTextView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    iget v2, p0, Lcom/squareup/widgets/ScalingTextView;->minTextSize:F

    iget v3, p0, Lcom/squareup/widgets/ScalingTextView;->maxTextSize:F

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/widgets/ScalingTextView$SavedState;-><init>(Landroid/os/Parcelable;FFLcom/squareup/widgets/ScalingTextView$1;)V

    return-object v0
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 96
    invoke-super {p0, p1, p2, p3, p4}, Lcom/squareup/marketfont/MarketTextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 97
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->requestLayout()V

    return-void
.end method

.method public setGravity(I)V
    .locals 0

    .line 121
    invoke-super {p0, p1}, Lcom/squareup/marketfont/MarketTextView;->setGravity(I)V

    .line 122
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->requestLayout()V

    return-void
.end method

.method public setMinTextSize(F)V
    .locals 0

    .line 131
    iput p1, p0, Lcom/squareup/widgets/ScalingTextView;->minTextSize:F

    return-void
.end method

.method public setTextAppearance(Landroid/content/Context;I)V
    .locals 0

    .line 115
    invoke-super {p0, p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 116
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->getTextSize()F

    move-result p1

    iput p1, p0, Lcom/squareup/widgets/ScalingTextView;->maxTextSize:F

    .line 117
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->requestLayout()V

    return-void
.end method

.method public setTextSize(F)V
    .locals 0

    .line 108
    invoke-super {p0, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextSize(F)V

    .line 109
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->getTextSize()F

    move-result p1

    iput p1, p0, Lcom/squareup/widgets/ScalingTextView;->maxTextSize:F

    const/4 p1, 0x0

    .line 110
    iput p1, p0, Lcom/squareup/widgets/ScalingTextView;->lastMeasuredWidth:I

    .line 111
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->requestLayout()V

    return-void
.end method

.method public setTextSize(IF)V
    .locals 0

    .line 101
    invoke-super {p0, p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setTextSize(IF)V

    .line 102
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->getTextSize()F

    move-result p1

    iput p1, p0, Lcom/squareup/widgets/ScalingTextView;->maxTextSize:F

    const/4 p1, 0x0

    .line 103
    iput p1, p0, Lcom/squareup/widgets/ScalingTextView;->lastMeasuredWidth:I

    .line 104
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->requestLayout()V

    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .locals 0

    .line 126
    invoke-super {p0, p1}, Lcom/squareup/marketfont/MarketTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 127
    invoke-virtual {p0}, Lcom/squareup/widgets/ScalingTextView;->requestLayout()V

    return-void
.end method
