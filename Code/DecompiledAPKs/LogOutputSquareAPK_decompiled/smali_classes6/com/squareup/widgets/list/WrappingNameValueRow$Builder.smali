.class public Lcom/squareup/widgets/list/WrappingNameValueRow$Builder;
.super Ljava/lang/Object;
.source "WrappingNameValueRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/list/WrappingNameValueRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private name:Ljava/lang/CharSequence;

.field private showPercent:Z

.field private value:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/widgets/list/WrappingNameValueRow$Builder;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/widgets/list/WrappingNameValueRow;
    .locals 2

    .line 53
    new-instance v0, Lcom/squareup/widgets/list/WrappingNameValueRow;

    iget-object v1, p0, Lcom/squareup/widgets/list/WrappingNameValueRow$Builder;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/squareup/widgets/list/WrappingNameValueRow;-><init>(Landroid/content/Context;)V

    .line 54
    iget-object v1, p0, Lcom/squareup/widgets/list/WrappingNameValueRow$Builder;->name:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/WrappingNameValueRow;->setName(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v1, p0, Lcom/squareup/widgets/list/WrappingNameValueRow$Builder;->value:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/WrappingNameValueRow;->setValue(Ljava/lang/CharSequence;)V

    .line 56
    iget-boolean v1, p0, Lcom/squareup/widgets/list/WrappingNameValueRow$Builder;->showPercent:Z

    invoke-static {v0, v1}, Lcom/squareup/widgets/list/WrappingNameValueRow;->access$000(Lcom/squareup/widgets/list/WrappingNameValueRow;Z)V

    return-object v0
.end method

.method public name(I)Lcom/squareup/widgets/list/WrappingNameValueRow$Builder;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/widgets/list/WrappingNameValueRow$Builder;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/widgets/list/WrappingNameValueRow$Builder;->name(Ljava/lang/CharSequence;)Lcom/squareup/widgets/list/WrappingNameValueRow$Builder;

    move-result-object p1

    return-object p1
.end method

.method public name(Ljava/lang/CharSequence;)Lcom/squareup/widgets/list/WrappingNameValueRow$Builder;
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/widgets/list/WrappingNameValueRow$Builder;->name:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public percent()Lcom/squareup/widgets/list/WrappingNameValueRow$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 48
    iput-boolean v0, p0, Lcom/squareup/widgets/list/WrappingNameValueRow$Builder;->showPercent:Z

    return-object p0
.end method

.method public value(Ljava/lang/CharSequence;)Lcom/squareup/widgets/list/WrappingNameValueRow$Builder;
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/widgets/list/WrappingNameValueRow$Builder;->value:Ljava/lang/CharSequence;

    return-object p0
.end method
