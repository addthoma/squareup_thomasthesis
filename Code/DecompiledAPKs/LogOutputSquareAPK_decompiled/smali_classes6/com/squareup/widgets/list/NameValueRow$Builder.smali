.class public Lcom/squareup/widgets/list/NameValueRow$Builder;
.super Ljava/lang/Object;
.source "NameValueRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/list/NameValueRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private name:Ljava/lang/CharSequence;

.field private showPercent:Z

.field private value:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/widgets/list/NameValueRow$Builder;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/widgets/list/NameValueRow;
    .locals 4

    .line 52
    new-instance v0, Lcom/squareup/widgets/list/NameValueRow;

    iget-object v1, p0, Lcom/squareup/widgets/list/NameValueRow$Builder;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/squareup/widgets/list/NameValueRow;-><init>(Landroid/content/Context;)V

    .line 53
    iget-object v1, p0, Lcom/squareup/widgets/list/NameValueRow$Builder;->name:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/squareup/widgets/list/NameValueRow$Builder;->value:Ljava/lang/CharSequence;

    iget-boolean v3, p0, Lcom/squareup/widgets/list/NameValueRow$Builder;->showPercent:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/widgets/list/NameValueRow;->update(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    return-object v0
.end method

.method public name(I)Lcom/squareup/widgets/list/NameValueRow$Builder;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueRow$Builder;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/widgets/list/NameValueRow$Builder;->name(Ljava/lang/CharSequence;)Lcom/squareup/widgets/list/NameValueRow$Builder;

    move-result-object p1

    return-object p1
.end method

.method public name(Ljava/lang/CharSequence;)Lcom/squareup/widgets/list/NameValueRow$Builder;
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/squareup/widgets/list/NameValueRow$Builder;->name:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public percent()Lcom/squareup/widgets/list/NameValueRow$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 47
    iput-boolean v0, p0, Lcom/squareup/widgets/list/NameValueRow$Builder;->showPercent:Z

    return-object p0
.end method

.method public value(Ljava/lang/CharSequence;)Lcom/squareup/widgets/list/NameValueRow$Builder;
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/squareup/widgets/list/NameValueRow$Builder;->value:Ljava/lang/CharSequence;

    return-object p0
.end method
