.class Lcom/squareup/widgets/list/ToggleButtonRow$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "ToggleButtonRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/list/ToggleButtonRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/widgets/list/ToggleButtonRow$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final buttonState:Landroid/os/Parcelable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 316
    new-instance v0, Lcom/squareup/widgets/list/ToggleButtonRow$SavedState$1;

    invoke-direct {v0}, Lcom/squareup/widgets/list/ToggleButtonRow$SavedState$1;-><init>()V

    sput-object v0, Lcom/squareup/widgets/list/ToggleButtonRow$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 333
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    const/4 v0, 0x0

    .line 334
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/widgets/list/ToggleButtonRow$SavedState;->buttonState:Landroid/os/Parcelable;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/squareup/widgets/list/ToggleButtonRow$1;)V
    .locals 0

    .line 315
    invoke-direct {p0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcelable;Landroid/os/Parcelable;)V
    .locals 0

    .line 328
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 329
    iput-object p2, p0, Lcom/squareup/widgets/list/ToggleButtonRow$SavedState;->buttonState:Landroid/os/Parcelable;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcelable;Landroid/os/Parcelable;Lcom/squareup/widgets/list/ToggleButtonRow$1;)V
    .locals 0

    .line 315
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/list/ToggleButtonRow$SavedState;-><init>(Landroid/os/Parcelable;Landroid/os/Parcelable;)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/widgets/list/ToggleButtonRow$SavedState;)Landroid/os/Parcelable;
    .locals 0

    .line 315
    iget-object p0, p0, Lcom/squareup/widgets/list/ToggleButtonRow$SavedState;->buttonState:Landroid/os/Parcelable;

    return-object p0
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 338
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 339
    iget-object p2, p0, Lcom/squareup/widgets/list/ToggleButtonRow$SavedState;->buttonState:Landroid/os/Parcelable;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
