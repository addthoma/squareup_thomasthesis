.class public Lcom/squareup/widgets/DigitInputView;
.super Landroid/widget/LinearLayout;
.source "DigitInputView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/DigitInputView$SavedState;,
        Lcom/squareup/widgets/DigitInputView$OnDigitsEnteredListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_NUMBER_OF_DIGITS:I = 0x6

.field private static final moveCursorToEnd:Landroid/view/View$OnFocusChangeListener;


# instance fields
.field private final digitCount:I

.field private final digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

.field private final digitLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

.field private final digitTextSize:F

.field private onDigitsEnteredListener:Lcom/squareup/widgets/DigitInputView$OnDigitsEnteredListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 226
    sget-object v0, Lcom/squareup/widgets/-$$Lambda$DigitInputView$gBI0FAvS5NQZihpQ_zwqFj9Lok4;->INSTANCE:Lcom/squareup/widgets/-$$Lambda$DigitInputView$gBI0FAvS5NQZihpQ_zwqFj9Lok4;

    sput-object v0, Lcom/squareup/widgets/DigitInputView;->moveCursorToEnd:Landroid/view/View$OnFocusChangeListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 52
    invoke-direct {p0, p1, v0}, Lcom/squareup/widgets/DigitInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 56
    sget v0, Lcom/squareup/widgets/R$attr;->digitInputViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/widgets/DigitInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .line 60
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/squareup/widgets/DigitInputView;->digitLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 61
    invoke-virtual {p0, v1}, Lcom/squareup/widgets/DigitInputView;->setOrientation(I)V

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/squareup/widgets/R$drawable;->digit_divider_clear_small_gap:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 62
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/DigitInputView;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x2

    .line 64
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/DigitInputView;->setShowDividers(I)V

    .line 66
    sget-object v0, Lcom/squareup/widgets/R$styleable;->DigitInputView:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 68
    sget p3, Lcom/squareup/widgets/R$styleable;->DigitInputView_digitCount:I

    const/4 v0, 0x6

    invoke-virtual {p2, p3, v0}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p3

    iput p3, p0, Lcom/squareup/widgets/DigitInputView;->digitCount:I

    .line 69
    sget p3, Lcom/squareup/widgets/R$styleable;->DigitInputView_android_textSize:I

    .line 70
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/squareup/widgets/R$dimen;->default_digit_input_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 69
    invoke-virtual {p2, p3, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p3

    iput p3, p0, Lcom/squareup/widgets/DigitInputView;->digitTextSize:F

    .line 71
    iget-object p3, p0, Lcom/squareup/widgets/DigitInputView;->digitLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p3, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 72
    iget p3, p0, Lcom/squareup/widgets/DigitInputView;->digitCount:I

    new-array p3, p3, [Lcom/squareup/widgets/OnDeleteEditText;

    iput-object p3, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    .line 74
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    const/4 p2, 0x0

    .line 76
    :goto_0
    iget p3, p0, Lcom/squareup/widgets/DigitInputView;->digitCount:I

    if-ge p2, p3, :cond_1

    add-int/lit8 p3, p3, -0x1

    if-ne p2, p3, :cond_0

    const/4 p3, 0x1

    goto :goto_1

    :cond_0
    const/4 p3, 0x0

    .line 80
    :goto_1
    iget-object v0, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    new-instance v2, Lcom/squareup/widgets/OnDeleteEditText;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v3}, Lcom/squareup/widgets/OnDeleteEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    aput-object v2, v0, p2

    .line 81
    iget-object v0, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    aget-object v0, v0, p2

    invoke-direct {p0, v0}, Lcom/squareup/widgets/DigitInputView;->configureDigit(Landroid/widget/EditText;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    aget-object v0, v0, p2

    new-instance v2, Lcom/squareup/widgets/DigitInputView$1;

    invoke-direct {v2, p0, p3, p2}, Lcom/squareup/widgets/DigitInputView$1;-><init>(Lcom/squareup/widgets/DigitInputView;ZI)V

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/OnDeleteEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 102
    iget-object p3, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    aget-object p3, p3, p2

    new-instance v0, Lcom/squareup/widgets/-$$Lambda$DigitInputView$rh2aqz5I6LzUh43IJnU3rbTpu5o;

    invoke-direct {v0, p0, p2}, Lcom/squareup/widgets/-$$Lambda$DigitInputView$rh2aqz5I6LzUh43IJnU3rbTpu5o;-><init>(Lcom/squareup/widgets/DigitInputView;I)V

    invoke-virtual {p3, v0}, Lcom/squareup/widgets/OnDeleteEditText;->setOnDeleteKeyListener(Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;)V

    .line 114
    iget-object p3, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    aget-object p3, p3, p2

    sget-object v0, Lcom/squareup/widgets/DigitInputView;->moveCursorToEnd:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {p3, v0}, Lcom/squareup/widgets/OnDeleteEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 115
    iget-object p3, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    aget-object p3, p3, p2

    invoke-virtual {p0, p3}, Lcom/squareup/widgets/DigitInputView;->addView(Landroid/view/View;)V

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic access$000(Lcom/squareup/widgets/DigitInputView;)Lcom/squareup/widgets/DigitInputView$OnDigitsEnteredListener;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/widgets/DigitInputView;->onDigitsEnteredListener:Lcom/squareup/widgets/DigitInputView$OnDigitsEnteredListener;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/widgets/DigitInputView;)[Lcom/squareup/widgets/OnDeleteEditText;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    return-object p0
.end method

.method private configureDigit(Landroid/widget/EditText;)V
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/squareup/widgets/DigitInputView;->digitLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/high16 v0, 0x2000000

    .line 147
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 148
    iget v0, p0, Lcom/squareup/widgets/DigitInputView;->digitTextSize:F

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Landroid/widget/EditText;->setTextSize(IF)V

    const/4 v0, 0x2

    .line 149
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setInputType(I)V

    .line 150
    invoke-virtual {p1, v1}, Landroid/widget/EditText;->setCursorVisible(Z)V

    const/16 v0, 0x11

    .line 151
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setGravity(I)V

    return-void
.end method

.method static synthetic lambda$static$4(Landroid/view/View;Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 228
    check-cast p0, Landroid/widget/EditText;

    .line 229
    new-instance p1, Lcom/squareup/widgets/DigitInputView$3;

    invoke-direct {p1, p0}, Lcom/squareup/widgets/DigitInputView$3;-><init>(Landroid/widget/EditText;)V

    invoke-virtual {p0, p1}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public appendDigit(C)V
    .locals 5

    .line 218
    iget-object v0, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 219
    invoke-virtual {v3}, Landroid/widget/EditText;->hasFocus()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 220
    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public clearDigits()V
    .locals 3

    const/4 v0, 0x0

    .line 205
    :goto_0
    iget v1, p0, Lcom/squareup/widgets/DigitInputView;->digitCount:I

    if-ge v0, v1, :cond_0

    .line 206
    iget-object v1, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    aget-object v1, v1, v0

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/OnDeleteEditText;->setText(Ljava/lang/CharSequence;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public digits()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 180
    new-instance v0, Lcom/squareup/widgets/-$$Lambda$DigitInputView$rJ1VC2ZgNrFHG9jeTKvYZvv1Smo;

    invoke-direct {v0, p0}, Lcom/squareup/widgets/-$$Lambda$DigitInputView$rJ1VC2ZgNrFHG9jeTKvYZvv1Smo;-><init>(Lcom/squareup/widgets/DigitInputView;)V

    invoke-static {v0}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object v0

    .line 201
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getDigitCount()I
    .locals 1

    .line 142
    iget v0, p0, Lcom/squareup/widgets/DigitInputView;->digitCount:I

    return v0
.end method

.method public getDigits()Ljava/lang/String;
    .locals 3

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    .line 157
    :goto_0
    iget v2, p0, Lcom/squareup/widgets/DigitInputView;->digitCount:I

    if-ge v1, v2, :cond_0

    .line 158
    iget-object v2, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/squareup/widgets/OnDeleteEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 160
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$digits$3$DigitInputView(Lio/reactivex/ObservableEmitter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 182
    iget-object v0, p0, Lcom/squareup/widgets/DigitInputView;->onDigitsEnteredListener:Lcom/squareup/widgets/DigitInputView$OnDigitsEnteredListener;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 184
    new-instance v0, Lcom/squareup/widgets/DigitInputView$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/widgets/DigitInputView$2;-><init>(Lcom/squareup/widgets/DigitInputView;Lio/reactivex/ObservableEmitter;)V

    iput-object v0, p0, Lcom/squareup/widgets/DigitInputView;->onDigitsEnteredListener:Lcom/squareup/widgets/DigitInputView$OnDigitsEnteredListener;

    .line 194
    new-instance v0, Lcom/squareup/widgets/-$$Lambda$DigitInputView$mci19ZPe_bKI5ktgWAFjH8BjYg4;

    invoke-direct {v0, p0}, Lcom/squareup/widgets/-$$Lambda$DigitInputView$mci19ZPe_bKI5ktgWAFjH8BjYg4;-><init>(Lcom/squareup/widgets/DigitInputView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnMainThread(Lio/reactivex/ObservableEmitter;Lkotlin/jvm/functions/Function0;)V

    .line 199
    invoke-virtual {p0}, Lcom/squareup/widgets/DigitInputView;->getDigits()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$new$0$DigitInputView(ILandroid/view/View;)V
    .locals 1

    .line 103
    iget-object p2, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    aget-object p2, p2, p1

    invoke-virtual {p2}, Lcom/squareup/widgets/OnDeleteEditText;->getText()Landroid/text/Editable;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result p2

    const-string v0, ""

    if-nez p2, :cond_0

    .line 105
    iget-object p2, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    aget-object p1, p2, p1

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/OnDeleteEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    if-lez p1, :cond_1

    .line 109
    iget-object p2, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    add-int/lit8 p1, p1, -0x1

    aget-object p2, p2, p1

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/OnDeleteEditText;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object p2, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    aget-object p1, p2, p1

    invoke-virtual {p1}, Lcom/squareup/widgets/OnDeleteEditText;->requestFocus()Z

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$null$2$DigitInputView()Lkotlin/Unit;
    .locals 1

    const/4 v0, 0x0

    .line 195
    iput-object v0, p0, Lcom/squareup/widgets/DigitInputView;->onDigitsEnteredListener:Lcom/squareup/widgets/DigitInputView$OnDigitsEnteredListener;

    .line 196
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public synthetic lambda$showKeyboard$1$DigitInputView()V
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    .line 120
    check-cast p1, Lcom/squareup/widgets/DigitInputView$SavedState;

    .line 121
    invoke-virtual {p1}, Lcom/squareup/widgets/DigitInputView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const/4 v0, 0x0

    .line 122
    :goto_0
    iget-object v1, p1, Lcom/squareup/widgets/DigitInputView$SavedState;->digitsState:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 123
    iget-object v1, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    aget-object v1, v1, v0

    iget-object v2, p1, Lcom/squareup/widgets/DigitInputView$SavedState;->digitsState:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/OnDeleteEditText;->setText(Ljava/lang/CharSequence;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 128
    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 129
    new-instance v1, Lcom/squareup/widgets/DigitInputView$SavedState;

    invoke-direct {v1, v0}, Lcom/squareup/widgets/DigitInputView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 130
    invoke-virtual {p0}, Lcom/squareup/widgets/DigitInputView;->getDigits()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/squareup/widgets/DigitInputView$SavedState;->digitsState:Ljava/lang/String;

    return-object v1
.end method

.method public setAlphaNumeric()V
    .locals 5

    .line 211
    iget-object v0, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    const/4 v4, 0x1

    .line 212
    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setInputType(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    .line 135
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    const/4 v0, 0x0

    .line 136
    :goto_0
    iget v1, p0, Lcom/squareup/widgets/DigitInputView;->digitCount:I

    if-ge v0, v1, :cond_0

    .line 137
    iget-object v1, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/squareup/widgets/OnDeleteEditText;->setEnabled(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setOnDigitsEnteredListener(Lcom/squareup/widgets/DigitInputView$OnDigitsEnteredListener;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 172
    iput-object p1, p0, Lcom/squareup/widgets/DigitInputView;->onDigitsEnteredListener:Lcom/squareup/widgets/DigitInputView$OnDigitsEnteredListener;

    return-void
.end method

.method public showKeyboard()V
    .locals 2

    .line 164
    iget-object v0, p0, Lcom/squareup/widgets/DigitInputView;->digitEditTexts:[Lcom/squareup/widgets/OnDeleteEditText;

    array-length v1, v0

    if-nez v1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    .line 165
    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/squareup/widgets/OnDeleteEditText;->requestFocus()Z

    .line 166
    new-instance v0, Lcom/squareup/widgets/-$$Lambda$DigitInputView$kDi-EbS8VISjbMTszXn10rEq2Is;

    invoke-direct {v0, p0}, Lcom/squareup/widgets/-$$Lambda$DigitInputView$kDi-EbS8VISjbMTszXn10rEq2Is;-><init>(Lcom/squareup/widgets/DigitInputView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/widgets/DigitInputView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
