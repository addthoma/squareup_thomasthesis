.class public final Lcom/squareup/widgets/pos/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/pos/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final about_device_setting_layout:I = 0x7f0d001d

.field public static final attribute_pair_row:I = 0x7f0d0066

.field public static final card_editor:I = 0x7f0d00b6

.field public static final card_expiration_cvv_editor:I = 0x7f0d00b7

.field public static final check_box_list_row:I = 0x7f0d00df

.field public static final coupon_ribbon:I = 0x7f0d010b

.field public static final cvv_editor:I = 0x7f0d01af

.field public static final date_picker_layout:I = 0x7f0d01b3

.field public static final date_picker_weekday:I = 0x7f0d01b4

.field public static final date_picker_weekday_cell:I = 0x7f0d01b5

.field public static final duration_picker_view:I = 0x7f0d01f0

.field public static final edit_catalog_object_label:I = 0x7f0d01f1

.field public static final edit_quantity_row:I = 0x7f0d0220

.field public static final empty_view:I = 0x7f0d0251

.field public static final expiration_editor:I = 0x7f0d0267

.field public static final favorite_empty_tile:I = 0x7f0d0269

.field public static final favorite_item_icon_tile:I = 0x7f0d026a

.field public static final favorite_text_tile:I = 0x7f0d026e

.field public static final glyph_button_auto_complete_edit_text:I = 0x7f0d0280

.field public static final glyph_button_edit_text:I = 0x7f0d0281

.field public static final glyph_radio_row:I = 0x7f0d0282

.field public static final help_row:I = 0x7f0d028b

.field public static final hud:I = 0x7f0d02ca

.field public static final info_bar_inline_button:I = 0x7f0d02ce

.field public static final info_bar_right_aligned_button:I = 0x7f0d02cf

.field public static final items_applet_modifiers_list_row:I = 0x7f0d0308

.field public static final keypad_entry_money:I = 0x7f0d0319

.field public static final line_row:I = 0x7f0d0330

.field public static final load_more_row:I = 0x7f0d0334

.field public static final month_pager_view:I = 0x7f0d035f

.field public static final multi_line_row:I = 0x7f0d037d

.field public static final noho_date_picker_dialog:I = 0x7f0d0390

.field public static final noho_date_picker_dialog_optional_year:I = 0x7f0d0391

.field public static final noho_time_picker_dialog:I = 0x7f0d0399

.field public static final reader_status_row:I = 0x7f0d046a

.field public static final settings_applet_payment_types_list_row:I = 0x7f0d04c7

.field public static final single_choice_list_item:I = 0x7f0d04d5

.field public static final single_picker_view:I = 0x7f0d04d6

.field public static final smart_line_row:I = 0x7f0d04d7

.field public static final smart_line_row_contents:I = 0x7f0d04d8

.field public static final smart_line_row_listitem:I = 0x7f0d04d9

.field public static final smoked_glass_dialog:I = 0x7f0d04da

.field public static final xable_auto_complete_edit_text:I = 0x7f0d0594

.field public static final xable_edit_text:I = 0x7f0d0595


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
