.class public Lcom/squareup/widgets/PairLayout;
.super Landroid/view/ViewGroup;
.source "PairLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/PairLayout$Child;
    }
.end annotation


# static fields
.field private static final UNSET:I = -0x1


# instance fields
.field private childDimen:I

.field private childPercentage:F

.field private firstChild:Landroid/view/View;

.field private final isLandscape:Z

.field private primaryChild:Lcom/squareup/widgets/PairLayout$Child;

.field private secondChild:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 99
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 100
    sget-object v0, Lcom/squareup/widgets/R$styleable;->PairLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 101
    sget p2, Lcom/squareup/widgets/R$styleable;->PairLayout_android_orientation:I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/squareup/widgets/PairLayout;->isLandscape:Z

    .line 102
    sget p2, Lcom/squareup/widgets/R$styleable;->PairLayout_primaryChild:I

    invoke-static {}, Lcom/squareup/widgets/PairLayout$Child;->values()[Lcom/squareup/widgets/PairLayout$Child;

    move-result-object v0

    sget-object v1, Lcom/squareup/widgets/PairLayout$Child;->FIRST:Lcom/squareup/widgets/PairLayout$Child;

    invoke-static {p1, p2, v0, v1}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;I[Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p2

    check-cast p2, Lcom/squareup/widgets/PairLayout$Child;

    iput-object p2, p0, Lcom/squareup/widgets/PairLayout;->primaryChild:Lcom/squareup/widgets/PairLayout$Child;

    .line 103
    sget p2, Lcom/squareup/widgets/R$styleable;->PairLayout_childPercentage:I

    const/high16 v0, -0x40800000    # -1.0f

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/PairLayout;->childPercentage:F

    .line 104
    sget p2, Lcom/squareup/widgets/R$styleable;->PairLayout_childDimen:I

    const/4 v0, -0x1

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/PairLayout;->childDimen:I

    .line 105
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 3

    .line 109
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 110
    invoke-virtual {p0}, Lcom/squareup/widgets/PairLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 115
    iget v0, p0, Lcom/squareup/widgets/PairLayout;->childDimen:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/squareup/widgets/PairLayout;->childPercentage:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    .line 116
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must set either childDimen or childPercentage."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 119
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/PairLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/widgets/PairLayout;->firstChild:Landroid/view/View;

    const/4 v0, 0x1

    .line 120
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/PairLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/widgets/PairLayout;->secondChild:Landroid/view/View;

    return-void

    .line 111
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PairLayout requires exactly two children, but currently has "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    invoke-virtual {p0}, Lcom/squareup/widgets/PairLayout;->getChildCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .line 187
    invoke-virtual {p0}, Lcom/squareup/widgets/PairLayout;->getMeasuredWidth()I

    move-result p1

    .line 188
    invoke-virtual {p0}, Lcom/squareup/widgets/PairLayout;->getMeasuredHeight()I

    move-result p2

    .line 190
    iget-object p3, p0, Lcom/squareup/widgets/PairLayout;->firstChild:Landroid/view/View;

    invoke-virtual {p3}, Landroid/view/View;->getVisibility()I

    move-result p3

    const/4 p4, 0x1

    const/16 p5, 0x8

    const/4 v0, 0x0

    if-eq p3, p5, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    .line 191
    :goto_0
    iget-object v1, p0, Lcom/squareup/widgets/PairLayout;->secondChild:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, p5, :cond_1

    goto :goto_1

    :cond_1
    const/4 p4, 0x0

    :goto_1
    if-eqz p3, :cond_3

    if-eqz p4, :cond_3

    .line 194
    iget-boolean p3, p0, Lcom/squareup/widgets/PairLayout;->isLandscape:Z

    if-eqz p3, :cond_2

    .line 196
    iget-object p3, p0, Lcom/squareup/widgets/PairLayout;->firstChild:Landroid/view/View;

    invoke-virtual {p3}, Landroid/view/View;->getMeasuredWidth()I

    move-result p4

    invoke-virtual {p3, v0, v0, p4, p2}, Landroid/view/View;->layout(IIII)V

    .line 199
    iget-object p3, p0, Lcom/squareup/widgets/PairLayout;->secondChild:Landroid/view/View;

    iget-object p4, p0, Lcom/squareup/widgets/PairLayout;->firstChild:Landroid/view/View;

    invoke-virtual {p4}, Landroid/view/View;->getMeasuredWidth()I

    move-result p4

    invoke-virtual {p3, p4, v0, p1, p2}, Landroid/view/View;->layout(IIII)V

    goto :goto_3

    .line 202
    :cond_2
    iget-object p3, p0, Lcom/squareup/widgets/PairLayout;->firstChild:Landroid/view/View;

    invoke-virtual {p3}, Landroid/view/View;->getMeasuredHeight()I

    move-result p4

    invoke-virtual {p3, v0, v0, p1, p4}, Landroid/view/View;->layout(IIII)V

    .line 205
    iget-object p3, p0, Lcom/squareup/widgets/PairLayout;->secondChild:Landroid/view/View;

    iget-object p4, p0, Lcom/squareup/widgets/PairLayout;->firstChild:Landroid/view/View;

    invoke-virtual {p4}, Landroid/view/View;->getMeasuredHeight()I

    move-result p4

    invoke-virtual {p3, v0, p4, p1, p2}, Landroid/view/View;->layout(IIII)V

    goto :goto_3

    :cond_3
    if-eqz p3, :cond_4

    .line 210
    iget-object p3, p0, Lcom/squareup/widgets/PairLayout;->firstChild:Landroid/view/View;

    goto :goto_2

    :cond_4
    iget-object p3, p0, Lcom/squareup/widgets/PairLayout;->secondChild:Landroid/view/View;

    .line 214
    :goto_2
    invoke-virtual {p3, v0, v0, p1, p2}, Landroid/view/View;->layout(IIII)V

    :goto_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10

    .line 124
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    .line 125
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    const/high16 v0, 0x40000000    # 2.0f

    .line 126
    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 127
    invoke-static {p1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 130
    iget-boolean v3, p0, Lcom/squareup/widgets/PairLayout;->isLandscape:Z

    if-eqz v3, :cond_0

    move v3, p1

    goto :goto_0

    :cond_0
    move v3, p2

    .line 134
    :goto_0
    iget-object v4, p0, Lcom/squareup/widgets/PairLayout;->primaryChild:Lcom/squareup/widgets/PairLayout$Child;

    sget-object v5, Lcom/squareup/widgets/PairLayout$Child;->FIRST:Lcom/squareup/widgets/PairLayout$Child;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/16 v8, 0x8

    if-ne v4, v5, :cond_2

    .line 135
    iget-object v4, p0, Lcom/squareup/widgets/PairLayout;->firstChild:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eq v4, v8, :cond_1

    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    .line 137
    :cond_2
    iget-object v4, p0, Lcom/squareup/widgets/PairLayout;->secondChild:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eq v4, v8, :cond_1

    :goto_1
    const/4 v4, -0x1

    if-nez v6, :cond_3

    .line 143
    invoke-static {v3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    const/4 v5, -0x1

    goto :goto_2

    .line 144
    :cond_3
    iget v5, p0, Lcom/squareup/widgets/PairLayout;->childPercentage:F

    const/high16 v6, -0x40800000    # -1.0f

    cmpl-float v6, v5, v6

    if-eqz v6, :cond_4

    int-to-float v3, v3

    mul-float v5, v5, v3

    .line 150
    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    const/high16 v6, 0x3f800000    # 1.0f

    .line 151
    iget v7, p0, Lcom/squareup/widgets/PairLayout;->childPercentage:F

    sub-float/2addr v6, v7

    mul-float v3, v3, v6

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_2

    .line 155
    :cond_4
    iget v5, p0, Lcom/squareup/widgets/PairLayout;->childDimen:I

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 156
    iget v6, p0, Lcom/squareup/widgets/PairLayout;->childDimen:I

    sub-int/2addr v3, v6

    invoke-static {v3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 160
    :goto_2
    iget-object v3, p0, Lcom/squareup/widgets/PairLayout;->primaryChild:Lcom/squareup/widgets/PairLayout$Child;

    sget-object v6, Lcom/squareup/widgets/PairLayout$Child;->FIRST:Lcom/squareup/widgets/PairLayout$Child;

    if-ne v3, v6, :cond_5

    move v9, v5

    move v5, v0

    move v0, v9

    .line 168
    :cond_5
    iget-boolean v3, p0, Lcom/squareup/widgets/PairLayout;->isLandscape:Z

    if-eqz v3, :cond_7

    if-eq v0, v4, :cond_6

    .line 170
    iget-object v2, p0, Lcom/squareup/widgets/PairLayout;->firstChild:Landroid/view/View;

    invoke-virtual {v2, v0, v1}, Landroid/view/View;->measure(II)V

    :cond_6
    if-eq v5, v4, :cond_9

    .line 173
    iget-object v0, p0, Lcom/squareup/widgets/PairLayout;->secondChild:Landroid/view/View;

    invoke-virtual {v0, v5, v1}, Landroid/view/View;->measure(II)V

    goto :goto_3

    :cond_7
    if-eq v0, v4, :cond_8

    .line 176
    iget-object v1, p0, Lcom/squareup/widgets/PairLayout;->firstChild:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Landroid/view/View;->measure(II)V

    :cond_8
    if-eq v5, v4, :cond_9

    .line 179
    iget-object v0, p0, Lcom/squareup/widgets/PairLayout;->secondChild:Landroid/view/View;

    invoke-virtual {v0, v2, v5}, Landroid/view/View;->measure(II)V

    .line 183
    :cond_9
    :goto_3
    invoke-virtual {p0, p1, p2}, Lcom/squareup/widgets/PairLayout;->setMeasuredDimension(II)V

    return-void
.end method

.method public setChildDimen(I)V
    .locals 0

    .line 229
    iput p1, p0, Lcom/squareup/widgets/PairLayout;->childDimen:I

    .line 230
    invoke-virtual {p0}, Lcom/squareup/widgets/PairLayout;->requestLayout()V

    return-void
.end method

.method public setChildPercentage(F)V
    .locals 0

    .line 224
    iput p1, p0, Lcom/squareup/widgets/PairLayout;->childPercentage:F

    .line 225
    invoke-virtual {p0}, Lcom/squareup/widgets/PairLayout;->requestLayout()V

    return-void
.end method

.method public setPrimaryChild(Lcom/squareup/widgets/PairLayout$Child;)V
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/widgets/PairLayout;->primaryChild:Lcom/squareup/widgets/PairLayout$Child;

    .line 220
    invoke-virtual {p0}, Lcom/squareup/widgets/PairLayout;->requestLayout()V

    return-void
.end method
