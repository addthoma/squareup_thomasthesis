.class public interface abstract Lcom/squareup/widgets/DigitInputView$OnDigitsEnteredListener;
.super Ljava/lang/Object;
.source "DigitInputView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/DigitInputView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnDigitsEnteredListener"
.end annotation


# virtual methods
.method public abstract onDigitEntered()V
.end method

.method public abstract onDigitsEntered(Ljava/lang/String;)V
.end method
