.class Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter$EmailDomains;
.super Ljava/lang/Object;
.source "EmailAutoCompleteFilterAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EmailDomains"
.end annotation


# instance fields
.field private final emails:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;)V
    .locals 3

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter$EmailDomains;->emails:Ljava/util/ArrayList;

    const/4 v0, 0x6

    new-array v0, v0, [I

    .line 129
    sget v1, Lcom/squareup/widgets/R$string;->email_domain_1:I

    const/4 v2, 0x0

    aput v1, v0, v2

    sget v1, Lcom/squareup/widgets/R$string;->email_domain_2:I

    const/4 v2, 0x1

    aput v1, v0, v2

    sget v1, Lcom/squareup/widgets/R$string;->email_domain_3:I

    const/4 v2, 0x2

    aput v1, v0, v2

    sget v1, Lcom/squareup/widgets/R$string;->email_domain_4:I

    const/4 v2, 0x3

    aput v1, v0, v2

    sget v1, Lcom/squareup/widgets/R$string;->email_domain_5:I

    const/4 v2, 0x4

    aput v1, v0, v2

    sget v1, Lcom/squareup/widgets/R$string;->email_domain_6:I

    const/4 v2, 0x5

    aput v1, v0, v2

    invoke-direct {p0, p1, v0}, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter$EmailDomains;->buildEmails(Lcom/squareup/util/Res;[I)V

    return-void
.end method

.method private varargs buildEmails(Lcom/squareup/util/Res;[I)V
    .locals 3

    const/4 v0, 0x0

    .line 138
    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 139
    iget-object v1, p0, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter$EmailDomains;->emails:Ljava/util/ArrayList;

    aget v2, p2, v0

    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public getDomains()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 134
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter$EmailDomains;->emails:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method
