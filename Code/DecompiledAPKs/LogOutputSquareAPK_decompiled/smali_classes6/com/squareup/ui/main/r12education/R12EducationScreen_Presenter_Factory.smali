.class public final Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "R12EducationScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final r12BlockingUpdateScreenLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ContentLauncher<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation
.end field

.field private final r12EducationDoneRedirectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/R12EducationDoneRedirector;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ContentLauncher<",
            "Ljava/lang/Void;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/R12EducationDoneRedirector;",
            ">;)V"
        }
    .end annotation

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p2, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p3, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p4, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->r12BlockingUpdateScreenLauncherProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p5, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p6, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p7, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p8, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p9, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->r12EducationDoneRedirectorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ContentLauncher<",
            "Ljava/lang/Void;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/R12EducationDoneRedirector;",
            ">;)",
            "Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;"
        }
    .end annotation

    .line 75
    new-instance v10, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/ui/main/ContentLauncher;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lflow/Flow;Lcom/squareup/ui/help/R12EducationDoneRedirector;)Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/tender/TenderStarter;",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            "Lcom/squareup/ui/main/ContentLauncher<",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/Features;",
            "Lflow/Flow;",
            "Lcom/squareup/ui/help/R12EducationDoneRedirector;",
            ")",
            "Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;"
        }
    .end annotation

    .line 83
    new-instance v10, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/ui/main/ContentLauncher;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lflow/Flow;Lcom/squareup/ui/help/R12EducationDoneRedirector;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;
    .locals 10

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/tender/TenderStarter;

    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->r12BlockingUpdateScreenLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/main/ContentLauncher;

    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->r12EducationDoneRedirectorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/help/R12EducationDoneRedirector;

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/ui/main/ContentLauncher;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lflow/Flow;Lcom/squareup/ui/help/R12EducationDoneRedirector;)Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/main/r12education/R12EducationScreen_Presenter_Factory;->get()Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
