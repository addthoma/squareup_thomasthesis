.class public final Lcom/squareup/ui/main/TopScreenChecker_Factory;
.super Ljava/lang/Object;
.source "TopScreenChecker_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/TopScreenChecker;",
        ">;"
    }
.end annotation


# instance fields
.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/ui/main/TopScreenChecker_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/main/TopScreenChecker_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)",
            "Lcom/squareup/ui/main/TopScreenChecker_Factory;"
        }
    .end annotation

    .line 24
    new-instance v0, Lcom/squareup/ui/main/TopScreenChecker_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/TopScreenChecker_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/ui/main/TopScreenChecker;
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/ui/main/TopScreenChecker;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/TopScreenChecker;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/TopScreenChecker;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/ui/main/TopScreenChecker_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    invoke-static {v0}, Lcom/squareup/ui/main/TopScreenChecker_Factory;->newInstance(Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/ui/main/TopScreenChecker;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/main/TopScreenChecker_Factory;->get()Lcom/squareup/ui/main/TopScreenChecker;

    move-result-object v0

    return-object v0
.end method
