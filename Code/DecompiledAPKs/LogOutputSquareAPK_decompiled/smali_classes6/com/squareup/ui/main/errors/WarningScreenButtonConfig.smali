.class public final Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;
.super Ljava/lang/Object;
.source "WarningScreenButtonConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final buttonBehaviorType:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

.field final enabled:Z

.field final localizedText:Ljava/lang/String;

.field final textId:I

.field final visible:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 86
    new-instance v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$1;

    invoke-direct {v0}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 6

    .line 41
    sget-object v1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->DO_NOTHING:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;-><init>(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;ZZLjava/lang/String;I)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;I)V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v5, p2

    .line 45
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;-><init>(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;ZZLjava/lang/String;I)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;ZZLjava/lang/String;I)V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->buttonBehaviorType:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    .line 51
    iput-boolean p2, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->enabled:Z

    .line 52
    iput-boolean p3, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->visible:Z

    .line 53
    iput-object p4, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->localizedText:Ljava/lang/String;

    .line 54
    iput p5, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->textId:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 62
    instance-of v0, p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 63
    check-cast p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->buttonBehaviorType:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    iget-object v2, p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->buttonBehaviorType:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    if-ne v0, v2, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->enabled:Z

    iget-boolean v2, p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->enabled:Z

    if-ne v0, v2, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->visible:Z

    iget-boolean v2, p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->visible:Z

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->localizedText:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->localizedText:Ljava/lang/String;

    .line 67
    invoke-static {v0, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->textId:I

    iget p1, p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->textId:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    .line 75
    iget-object v1, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->buttonBehaviorType:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->enabled:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->visible:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->localizedText:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->textId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 79
    iget-object p2, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->buttonBehaviorType:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    invoke-virtual {p2}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 80
    iget-boolean p2, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->enabled:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 81
    iget-boolean p2, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->visible:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 82
    iget-object p2, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->localizedText:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 83
    iget p2, p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->textId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
