.class public final Lcom/squareup/ui/main/errors/WarningScreenData;
.super Ljava/lang/Object;
.source "WarningScreenData.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/errors/WarningScreenData$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/main/errors/WarningScreenData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final cancellable:Z

.field public final defaultButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

.field public final glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field final localizedMessage:Ljava/lang/String;

.field final localizedTitle:Ljava/lang/String;

.field final messageId:Ljava/lang/Integer;

.field final timeout:Ljava/lang/Long;

.field final timeoutBehavior:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

.field final titleId:I

.field final topAlternativeButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

.field public final vectorId:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 81
    new-instance v0, Lcom/squareup/ui/main/errors/WarningScreenData$1;

    invoke-direct {v0}, Lcom/squareup/ui/main/errors/WarningScreenData$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/errors/WarningScreenData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)V
    .locals 1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->access$000(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->vectorId:Ljava/lang/Integer;

    .line 29
    invoke-static {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->access$100(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->defaultButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    .line 30
    invoke-static {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->access$200(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->topAlternativeButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    .line 31
    invoke-static {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->access$300(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 32
    invoke-static {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->access$400(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->localizedMessage:Ljava/lang/String;

    .line 33
    invoke-static {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->access$500(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->localizedTitle:Ljava/lang/String;

    .line 34
    invoke-static {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->access$600(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->messageId:Ljava/lang/Integer;

    .line 35
    invoke-static {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->access$700(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->titleId:I

    .line 36
    invoke-static {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->access$800(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->cancellable:Z

    .line 37
    iget-object v0, p1, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->timeout:Ljava/lang/Long;

    iput-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->timeout:Ljava/lang/Long;

    .line 38
    iget-object p1, p1, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->timeoutBehavior:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    iput-object p1, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->timeoutBehavior:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;Lcom/squareup/ui/main/errors/WarningScreenData$1;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/errors/WarningScreenData;-><init>(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 46
    instance-of v0, p1, Lcom/squareup/ui/main/errors/WarningScreenData;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 50
    :cond_0
    check-cast p1, Lcom/squareup/ui/main/errors/WarningScreenData;

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->defaultButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    iget-object v2, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->defaultButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    invoke-static {v0, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->topAlternativeButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    iget-object v2, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->topAlternativeButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    .line 52
    invoke-static {v0, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->localizedMessage:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->localizedMessage:Ljava/lang/String;

    .line 54
    invoke-static {v0, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->localizedTitle:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->localizedTitle:Ljava/lang/String;

    .line 55
    invoke-static {v0, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->messageId:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->messageId:Ljava/lang/Integer;

    .line 56
    invoke-static {v0, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->titleId:I

    iget v2, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->titleId:I

    if-ne v0, v2, :cond_1

    iget-boolean v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->cancellable:Z

    .line 58
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-boolean v2, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->cancellable:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->timeout:Ljava/lang/Long;

    iget-object v2, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->timeout:Ljava/lang/Long;

    .line 59
    invoke-static {v0, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->timeoutBehavior:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    iget-object p1, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->timeoutBehavior:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    .line 60
    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    .line 64
    iget-object v1, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->defaultButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->topAlternativeButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->localizedMessage:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->localizedTitle:Ljava/lang/String;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->messageId:Ljava/lang/Integer;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->titleId:I

    .line 65
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->cancellable:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x7

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->timeout:Ljava/lang/Long;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->timeoutBehavior:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    .line 64
    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->defaultButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->localizedMessage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->localizedTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->messageId:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 74
    iget v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->titleId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->topAlternativeButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 76
    iget-boolean p2, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->cancellable:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 77
    iget-object p2, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->timeout:Ljava/lang/Long;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 78
    iget-object p2, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->timeoutBehavior:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    invoke-virtual {p2}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
