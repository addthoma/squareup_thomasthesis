.class public final enum Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;
.super Ljava/lang/Enum;
.source "WarningScreenButtonConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ButtonBehaviorType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

.field public static final enum CONTACT_SUPPORT:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

.field public static final enum DISMISS:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

.field public static final enum DO_NOTHING:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

.field public static final enum EMV_SESSION_CANCEL_PAYMENT:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

.field public static final enum KILL_TENDER_INITIATED_BY_HUMAN:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

.field public static final enum KILL_TENDER_INITIATED_BY_READER:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

.field public static final enum KILL_TENDER_INITIATED_BY_READER_AND_TURN_OFF_NFC_FIELD:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

.field public static final enum RESUME_CHECKOUT_FLOW_AT_PAYMENT_PROMPT:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

.field public static final enum START_ACTIVATION:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 20
    new-instance v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    const/4 v1, 0x0

    const-string v2, "DO_NOTHING"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->DO_NOTHING:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    .line 21
    new-instance v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    const/4 v2, 0x1

    const-string v3, "KILL_TENDER_INITIATED_BY_READER"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->KILL_TENDER_INITIATED_BY_READER:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    .line 22
    new-instance v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    const/4 v3, 0x2

    const-string v4, "KILL_TENDER_INITIATED_BY_HUMAN"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->KILL_TENDER_INITIATED_BY_HUMAN:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    .line 23
    new-instance v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    const/4 v4, 0x3

    const-string v5, "KILL_TENDER_INITIATED_BY_READER_AND_TURN_OFF_NFC_FIELD"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->KILL_TENDER_INITIATED_BY_READER_AND_TURN_OFF_NFC_FIELD:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    .line 24
    new-instance v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    const/4 v5, 0x4

    const-string v6, "EMV_SESSION_CANCEL_PAYMENT"

    invoke-direct {v0, v6, v5}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->EMV_SESSION_CANCEL_PAYMENT:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    .line 25
    new-instance v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    const/4 v6, 0x5

    const-string v7, "CONTACT_SUPPORT"

    invoke-direct {v0, v7, v6}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->CONTACT_SUPPORT:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    .line 26
    new-instance v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    const/4 v7, 0x6

    const-string v8, "START_ACTIVATION"

    invoke-direct {v0, v8, v7}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->START_ACTIVATION:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    .line 27
    new-instance v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    const/4 v8, 0x7

    const-string v9, "DISMISS"

    invoke-direct {v0, v9, v8}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->DISMISS:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    .line 28
    new-instance v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    const/16 v9, 0x8

    const-string v10, "RESUME_CHECKOUT_FLOW_AT_PAYMENT_PROMPT"

    invoke-direct {v0, v10, v9}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->RESUME_CHECKOUT_FLOW_AT_PAYMENT_PROMPT:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    .line 19
    sget-object v10, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->DO_NOTHING:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->KILL_TENDER_INITIATED_BY_READER:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->KILL_TENDER_INITIATED_BY_HUMAN:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->KILL_TENDER_INITIATED_BY_READER_AND_TURN_OFF_NFC_FIELD:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->EMV_SESSION_CANCEL_PAYMENT:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->CONTACT_SUPPORT:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->START_ACTIVATION:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->DISMISS:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->RESUME_CHECKOUT_FLOW_AT_PAYMENT_PROMPT:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->$VALUES:[Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;
    .locals 1

    .line 19
    const-class v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->$VALUES:[Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    invoke-virtual {v0}, [Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    return-object v0
.end method
