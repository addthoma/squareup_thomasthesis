.class public Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule;
.super Ljava/lang/Object;
.source "NoAdditionalContainerLayerSetupModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideNoAdditionalContainerLayerSetup()Lcom/squareup/container/AdditionalContainerLayerSetup;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 12
    sget-object v0, Lcom/squareup/container/AdditionalContainerLayerSetup$None;->INSTANCE:Lcom/squareup/container/AdditionalContainerLayerSetup$None;

    return-object v0
.end method
