.class public Lcom/squareup/ui/main/CheckoutEntryHandler;
.super Ljava/lang/Object;
.source "CheckoutEntryHandler.java"

# interfaces
.implements Lcom/squareup/librarylist/LibraryListEntryHandler;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final discountEntryScreenRunner:Lcom/squareup/ui/library/DiscountEntryScreenRunner;

.field private final editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final eventSink:Lcom/squareup/badbus/BadEventSink;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final lazyCogs:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final priceEntryScreenRunner:Lcom/squareup/ui/library/PriceEntryScreenRunner;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

.field private final transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/library/DiscountEntryScreenRunner;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/payment/Transaction;Ldagger/Lazy;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/library/PriceEntryScreenRunner;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/items/EditItemGateway;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/ui/library/DiscountEntryScreenRunner;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/payment/Transaction;",
            "Ldagger/Lazy<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/library/PriceEntryScreenRunner;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/analytics/Analytics;",
            "Lflow/Flow;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/items/EditItemGateway;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 116
    iput-object v1, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->res:Lcom/squareup/util/Res;

    move-object v1, p2

    .line 117
    iput-object v1, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    move-object v1, p3

    .line 118
    iput-object v1, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->discountEntryScreenRunner:Lcom/squareup/ui/library/DiscountEntryScreenRunner;

    move-object v1, p4

    .line 119
    iput-object v1, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    move-object v1, p5

    .line 120
    iput-object v1, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p6

    .line 121
    iput-object v1, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->lazyCogs:Ldagger/Lazy;

    move-object v1, p7

    .line 122
    iput-object v1, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p8

    .line 123
    iput-object v1, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->priceEntryScreenRunner:Lcom/squareup/ui/library/PriceEntryScreenRunner;

    move-object v1, p9

    .line 124
    iput-object v1, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->eventSink:Lcom/squareup/badbus/BadEventSink;

    move-object v1, p10

    .line 125
    iput-object v1, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    move-object v1, p11

    .line 126
    iput-object v1, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    move-object v1, p12

    .line 127
    iput-object v1, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    move-object v1, p13

    .line 128
    iput-object v1, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    move-object/from16 v1, p14

    .line 129
    iput-object v1, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->analytics:Lcom/squareup/analytics/Analytics;

    move-object/from16 v1, p15

    .line 130
    iput-object v1, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->flow:Lflow/Flow;

    move-object/from16 v1, p16

    .line 131
    iput-object v1, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v1, p17

    .line 132
    iput-object v1, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/main/CheckoutEntryHandler;)Lcom/squareup/ui/items/EditItemGateway;
    .locals 0

    .line 88
    iget-object p0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/main/CheckoutEntryHandler;)Lflow/Flow;
    .locals 0

    .line 88
    iget-object p0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->flow:Lflow/Flow;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/main/CheckoutEntryHandler;)Lcom/squareup/log/cart/TransactionInteractionsLogger;
    .locals 0

    .line 88
    iget-object p0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/main/CheckoutEntryHandler;)Lcom/squareup/util/Res;
    .locals 0

    .line 88
    iget-object p0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/shared/catalog/models/CatalogDiscount;Landroid/view/View;ZLjava/lang/Class;Lcom/squareup/protos/client/Employee;)V
    .locals 0

    .line 88
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/main/CheckoutEntryHandler;->addDiscount(Lcom/squareup/shared/catalog/models/CatalogDiscount;Landroid/view/View;ZLjava/lang/Class;Lcom/squareup/protos/client/Employee;)V

    return-void
.end method

.method private addDiscount(Lcom/squareup/shared/catalog/models/CatalogDiscount;Landroid/view/View;ZLjava/lang/Class;Lcom/squareup/protos/client/Employee;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            "Landroid/view/View;",
            "Z",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;",
            "Lcom/squareup/protos/client/Employee;",
            ")V"
        }
    .end annotation

    .line 186
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getDiscountType()Lcom/squareup/api/items/Discount$DiscountType;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 187
    sget-object v1, Lcom/squareup/api/items/Discount$DiscountType;->FIXED:Lcom/squareup/api/items/Discount$DiscountType;

    if-eq v0, v1, :cond_1

    .line 189
    new-instance p4, Lcom/squareup/configure/item/WorkingDiscount;

    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {p4, p1, v0}, Lcom/squareup/configure/item/WorkingDiscount;-><init>(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 190
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {p1, p2, p3}, Lcom/squareup/orderentry/OrderEntryScreenState;->enqueueFlyByAnimationForDiscount(Landroid/view/View;Z)V

    .line 191
    invoke-virtual {p4}, Lcom/squareup/configure/item/WorkingDiscount;->isPercentage()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 192
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->discountEntryScreenRunner:Lcom/squareup/ui/library/DiscountEntryScreenRunner;

    invoke-virtual {p1, p4, p5}, Lcom/squareup/ui/library/DiscountEntryScreenRunner;->goToDiscountEntryPercent(Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/protos/client/Employee;)V

    goto :goto_0

    .line 194
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->discountEntryScreenRunner:Lcom/squareup/ui/library/DiscountEntryScreenRunner;

    invoke-virtual {p1, p4, p5}, Lcom/squareup/ui/library/DiscountEntryScreenRunner;->goToDiscountEntryMoney(Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/protos/client/Employee;)V

    goto :goto_0

    .line 198
    :cond_1
    sget-object v0, Lcom/squareup/checkout/Discount$Scope;->CART:Lcom/squareup/checkout/Discount$Scope;

    iget-object v1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    .line 199
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    .line 198
    invoke-static {p1, v0, v1}, Lcom/squareup/checkout/Discounts;->toAppliedDiscount(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/checkout/Discount$Scope;Z)Lcom/squareup/checkout/Discount;

    move-result-object v0

    .line 200
    iget-object v1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1, v0, p5}, Lcom/squareup/payment/Transaction;->addDiscount(Lcom/squareup/checkout/Discount;Lcom/squareup/protos/client/Employee;)V

    if-eqz p4, :cond_2

    .line 203
    iget-object p5, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {p5, p2, p3}, Lcom/squareup/orderentry/OrderEntryScreenState;->enqueueFlyByAnimationForDiscount(Landroid/view/View;Z)V

    .line 204
    iget-object p2, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    invoke-virtual {p2, p1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->setCurrentCatalogDiscount(Lcom/squareup/shared/catalog/models/CatalogDiscount;)V

    .line 205
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->flow:Lflow/Flow;

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Class;

    const/4 p3, 0x0

    aput-object p4, p2, p3

    invoke-static {p1, p2}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    .line 207
    :cond_2
    new-instance p4, Lcom/squareup/ui/main/-$$Lambda$CheckoutEntryHandler$cK3xBKqmr2EMvy0Tr8E79vkdC7I;

    invoke-direct {p4, p0, p1}, Lcom/squareup/ui/main/-$$Lambda$CheckoutEntryHandler$cK3xBKqmr2EMvy0Tr8E79vkdC7I;-><init>(Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/shared/catalog/models/CatalogDiscount;)V

    invoke-direct {p0, p2, p3, p4}, Lcom/squareup/ui/main/CheckoutEntryHandler;->startFlyByAnimation(Landroid/view/View;ZLcom/squareup/orderentry/FlyByListener;)V

    :goto_0
    return-void
.end method

.method private addItemToCart(Ljava/lang/String;Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;",
            ">;)V"
        }
    .end annotation

    .line 580
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0, p2}, Lcom/squareup/orderentry/OrderEntryScreenState;->setLastItemEntryMethod(Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;)V

    .line 583
    iget-object p2, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    invoke-interface {p2}, Lcom/squareup/ui/main/TransactionMetrics;->beginTransaction()V

    .line 585
    :cond_0
    new-instance p2, Lcom/squareup/ui/main/-$$Lambda$CheckoutEntryHandler$fANMy0DVzgNRjCwC6F8XtRmeREE;

    invoke-direct {p2, p1}, Lcom/squareup/ui/main/-$$Lambda$CheckoutEntryHandler$fANMy0DVzgNRjCwC6F8XtRmeREE;-><init>(Ljava/lang/String;)V

    .line 587
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->lazyCogs:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cogs/Cogs;

    invoke-interface {p1, p2, p3}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method private addOrderItemAndStartFlyByAnimation(Lcom/squareup/configure/item/WorkingItem;Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/configure/item/WorkingItem;",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 565
    iget-object v0, p1, Lcom/squareup/configure/item/WorkingItem;->quantity:Ljava/math/BigDecimal;

    invoke-static {v0}, Lcom/squareup/util/BigDecimals;->asIntOr1(Ljava/math/BigDecimal;)I

    move-result v0

    .line 566
    iget-object v1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->getFlyByAnimationData()Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->setQuantity(I)V

    .line 567
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/configure/item/WorkingItem;->finishWithOnlyFixedPrice()Lcom/squareup/checkout/CartItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->addOrderItem(Lcom/squareup/checkout/CartItem;)V

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 569
    iget-object v1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v1, p3, v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->enqueueFlyByAnimationForDiscount(Landroid/view/View;Z)V

    .line 570
    iget-object p3, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    invoke-virtual {p3, p1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->setCurrentWorkingItem(Lcom/squareup/configure/item/WorkingItem;)V

    .line 571
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->flow:Lflow/Flow;

    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/Class;

    aput-object p2, p3, v0

    invoke-static {p1, p3}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    :cond_0
    if-eqz p3, :cond_1

    .line 573
    new-instance p2, Lcom/squareup/ui/main/-$$Lambda$CheckoutEntryHandler$qniBCc7WwwLfGFdfD8V4ZJKvm3E;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/main/-$$Lambda$CheckoutEntryHandler$qniBCc7WwwLfGFdfD8V4ZJKvm3E;-><init>(Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/configure/item/WorkingItem;)V

    invoke-direct {p0, p3, v0, p2}, Lcom/squareup/ui/main/CheckoutEntryHandler;->startFlyByAnimation(Landroid/view/View;ZLcom/squareup/orderentry/FlyByListener;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private assertNotInEditMode()V
    .locals 2

    .line 666
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-eq v0, v1, :cond_0

    return-void

    .line 667
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Edit mode is not supported."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private attemptToPreSelectVariation(Ljava/lang/String;Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;Lcom/squareup/configure/item/WorkingItem;)Z
    .locals 2

    .line 500
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/main/CheckoutEntryHandler;->shouldAttemptToPreSelectVariation(Ljava/lang/String;Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 504
    invoke-virtual {p2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/main/CheckoutEntryHandler;->getVariationFromCollectionThatMatchesSKU(Ljava/util/List;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 507
    invoke-virtual {p2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    .line 511
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isVariablePricing()Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    .line 512
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {p3, p1}, Lcom/squareup/configure/item/WorkingItem;->setZeroCurrentAmount(Lcom/squareup/protos/common/CurrencyCode;)V

    const/4 v1, 0x1

    .line 516
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p3, p1}, Lcom/squareup/configure/item/WorkingItem;->selectVariation(I)V

    .line 517
    invoke-direct {p0, p3, v1}, Lcom/squareup/ui/main/CheckoutEntryHandler;->goToConfigureWorkingItem(Lcom/squareup/configure/item/WorkingItem;Z)V

    return v0

    :cond_1
    return v1
.end method

.method private createWorkingItem(Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;)Lcom/squareup/configure/item/WorkingItem;
    .locals 20

    move-object/from16 v0, p0

    .line 639
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getItemImageOrNull()Lcom/squareup/shared/catalog/models/CatalogItemImage;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    .line 641
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getItemImageOrNull()Lcom/squareup/shared/catalog/models/CatalogItemImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemImage;->getUrl()Ljava/lang/String;

    move-result-object v1

    :goto_0
    move-object v4, v1

    .line 643
    iget-object v1, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v1}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v1

    .line 645
    new-instance v19, Lcom/squareup/configure/item/WorkingItem;

    move-object/from16 v2, v19

    .line 646
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getItem()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v3

    .line 648
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getBackingDetails()Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    move-result-object v5

    .line 649
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getCategory()Lcom/squareup/api/items/MenuCategory;

    move-result-object v6

    .line 650
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getTaxes()Ljava/util/List;

    move-result-object v7

    iget-object v8, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transaction:Lcom/squareup/payment/Transaction;

    .line 651
    invoke-virtual {v8}, Lcom/squareup/payment/Transaction;->getAvailableTaxRules()Ljava/util/List;

    move-result-object v8

    iget-object v9, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transaction:Lcom/squareup/payment/Transaction;

    .line 652
    invoke-virtual {v9}, Lcom/squareup/payment/Transaction;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v9

    iget-object v10, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transaction:Lcom/squareup/payment/Transaction;

    .line 653
    invoke-virtual {v10}, Lcom/squareup/payment/Transaction;->getAddedCouponsAndCartScopeDiscountEvents()Ljava/util/Map;

    move-result-object v10

    .line 654
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object v11

    .line 655
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getModifierLists()Ljava/util/Map;

    move-result-object v12

    .line 656
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getModifierOverrides()Ljava/util/Map;

    move-result-object v13

    iget-object v14, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transaction:Lcom/squareup/payment/Transaction;

    .line 657
    invoke-virtual {v14}, Lcom/squareup/payment/Transaction;->getCurrentDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v14

    iget-object v15, v0, Lcom/squareup/ui/main/CheckoutEntryHandler;->res:Lcom/squareup/util/Res;

    move-object/from16 v16, v15

    move-object/from16 v0, v16

    .line 659
    invoke-virtual {v1, v0}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeProto(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;

    move-result-object v16

    .line 660
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getMeasurementUnits()Ljava/util/Map;

    move-result-object v17

    .line 661
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getItemOptions()Ljava/util/List;

    move-result-object v18

    invoke-direct/range {v2 .. v18}, Lcom/squareup/configure/item/WorkingItem;-><init>(Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization$BackingDetails;Lcom/squareup/api/items/MenuCategory;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/checkout/DiningOption;Lcom/squareup/util/Res;Lcom/squareup/protos/client/Employee;Ljava/util/Map;Ljava/util/List;)V

    return-object v19
.end method

.method private getVariationFromCollectionThatMatchesSKU(Ljava/util/List;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;"
        }
    .end annotation

    .line 681
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 682
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getSku()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private goToConfigureWorkingItem(Lcom/squareup/configure/item/WorkingItem;Z)V
    .locals 3

    .line 627
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/ui/main/-$$Lambda$CheckoutEntryHandler$8oXCuc1P_xINKyFVwebbeORkrkE;

    invoke-direct {v2, p1, p2}, Lcom/squareup/ui/main/-$$Lambda$CheckoutEntryHandler$8oXCuc1P_xINKyFVwebbeORkrkE;-><init>(Lcom/squareup/configure/item/WorkingItem;Z)V

    invoke-direct {v1, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private goToPriceEntryScreen(Lcom/squareup/configure/item/WorkingItem;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    .line 557
    invoke-virtual {p1, v0}, Lcom/squareup/configure/item/WorkingItem;->selectVariation(I)V

    .line 558
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {p1, v0}, Lcom/squareup/configure/item/WorkingItem;->setZeroCurrentAmount(Lcom/squareup/protos/common/CurrencyCode;)V

    .line 559
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0, p2, p3, p4}, Lcom/squareup/orderentry/OrderEntryScreenState;->enqueueFlyByAnimationForItem(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    .line 560
    iget-object p2, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->priceEntryScreenRunner:Lcom/squareup/ui/library/PriceEntryScreenRunner;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/library/PriceEntryScreenRunner;->goToPriceEntry(Lcom/squareup/configure/item/WorkingItem;)V

    return-void
.end method

.method static synthetic lambda$addItemToCart$7(Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;
    .locals 0

    .line 585
    invoke-static {p0, p1}, Lcom/squareup/librarylist/CogsSelectedEntryDataLocator;->getEntryDataForItem(Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$discountClicked$0(Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/models/CatalogDiscount;
    .locals 1

    .line 161
    const-class v0, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    invoke-interface {p1, v0, p0}, Lcom/squareup/shared/catalog/Catalog$Local;->findById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    return-object p0
.end method

.method static synthetic lambda$goToConfigureWorkingItem$8(Lcom/squareup/configure/item/WorkingItem;ZLflow/History;)Lcom/squareup/container/Command;
    .locals 2

    .line 629
    invoke-virtual {p2}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p2

    new-instance v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen;

    sget-object v1, Lcom/squareup/ui/seller/SellerScope;->INSTANCE:Lcom/squareup/ui/seller/SellerScope;

    invoke-direct {v0, v1, p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/configure/item/WorkingItem;)V

    .line 630
    invoke-virtual {p2, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p2

    if-eqz p1, :cond_0

    .line 632
    new-instance p1, Lcom/squareup/configure/item/ConfigureItemPriceScreen;

    sget-object v0, Lcom/squareup/ui/seller/SellerScope;->INSTANCE:Lcom/squareup/ui/seller/SellerScope;

    invoke-direct {p1, v0, p0}, Lcom/squareup/configure/item/ConfigureItemPriceScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/configure/item/WorkingItem;)V

    invoke-virtual {p2, p1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 634
    :cond_0
    invoke-virtual {p2}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p0

    sget-object p1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {p0, p1}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0
.end method

.method private shouldAttemptToPreSelectVariation(Ljava/lang/String;Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;)Z
    .locals 3

    .line 673
    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    const/4 v0, 0x1

    xor-int/2addr p1, v0

    .line 674
    invoke-virtual {p2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getItem()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v1

    sget-object v2, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    invoke-virtual {v1, v2}, Lcom/squareup/api/items/Item$Type;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 675
    invoke-virtual {p2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    const/4 v2, 0x0

    if-le p2, v0, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p1, :cond_1

    if-eqz v1, :cond_1

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method private startFlyByAnimation(Landroid/view/View;ZLcom/squareup/orderentry/FlyByListener;)V
    .locals 2

    .line 622
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance v1, Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter$EntryFlyBy;-><init>(Landroid/view/View;ZLcom/squareup/orderentry/FlyByListener;)V

    invoke-interface {v0, v1}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public createNewLibraryItemClicked()V
    .locals 3

    .line 611
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_CREATE_NEW:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 612
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->EDIT_ITEMS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/main/CheckoutEntryHandler$3;

    invoke-direct {v2, p0}, Lcom/squareup/ui/main/CheckoutEntryHandler$3;-><init>(Lcom/squareup/ui/main/CheckoutEntryHandler;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public customAmountClicked()V
    .locals 2

    .line 136
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot enter custom amount from Library in Checkout Applet"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public discountClicked(Landroid/view/View;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Z)V
    .locals 0

    .line 143
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/main/CheckoutEntryHandler;->discountClicked(Landroid/view/View;Ljava/lang/String;Z)V

    return-void
.end method

.method public discountClicked(Landroid/view/View;Ljava/lang/String;Z)V
    .locals 1

    const/4 v0, 0x0

    .line 147
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/squareup/ui/main/CheckoutEntryHandler;->discountClicked(Landroid/view/View;Ljava/lang/String;ZLjava/lang/Class;)V

    return-void
.end method

.method public discountClicked(Landroid/view/View;Ljava/lang/String;ZLjava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;)V"
        }
    .end annotation

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne v0, v1, :cond_0

    return-void

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transactionMetrics:Lcom/squareup/ui/main/TransactionMetrics;

    invoke-interface {v0}, Lcom/squareup/ui/main/TransactionMetrics;->beginTransaction()V

    .line 160
    :cond_1
    new-instance v0, Lcom/squareup/ui/main/-$$Lambda$CheckoutEntryHandler$uWqNCBYqX0YwWsQXreH8WxBEnlg;

    invoke-direct {v0, p2}, Lcom/squareup/ui/main/-$$Lambda$CheckoutEntryHandler$uWqNCBYqX0YwWsQXreH8WxBEnlg;-><init>(Ljava/lang/String;)V

    .line 162
    new-instance p2, Lcom/squareup/ui/main/-$$Lambda$CheckoutEntryHandler$RweCS9ciTYb6mG16BR9HV1AS630;

    invoke-direct {p2, p0, p1, p3, p4}, Lcom/squareup/ui/main/-$$Lambda$CheckoutEntryHandler$RweCS9ciTYb6mG16BR9HV1AS630;-><init>(Lcom/squareup/ui/main/CheckoutEntryHandler;Landroid/view/View;ZLjava/lang/Class;)V

    .line 180
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->lazyCogs:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cogs/Cogs;

    invoke-interface {p1, v0, p2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public isEntryEnabled(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;)Z
    .locals 1

    .line 591
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {p1}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object p1

    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->SALE:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne p1, v0, :cond_0

    .line 592
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1, p2}, Lcom/squareup/payment/Transaction;->catalogDiscountMayApplyAtCartScope(Ljava/lang/String;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method public itemClicked(Landroid/view/View;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;)V
    .locals 1

    const/4 v0, 0x0

    .line 371
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/squareup/ui/main/CheckoutEntryHandler;->itemClicked(Landroid/view/View;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;)V

    return-void
.end method

.method public itemClicked(Landroid/view/View;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 376
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/main/CheckoutEntryHandler;->itemClicked(Landroid/view/View;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/Class;)V

    return-void
.end method

.method public itemClicked(Landroid/view/View;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/Item$Type;",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;)V"
        }
    .end annotation

    .line 381
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/squareup/api/items/Item$Type;

    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 385
    invoke-direct {p0}, Lcom/squareup/ui/main/CheckoutEntryHandler;->assertNotInEditMode()V

    .line 388
    sget-object v0, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    if-ne p3, v0, :cond_0

    iget-object p3, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p3}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/settings/server/GiftCardSettings;->canUseGiftCards()Z

    move-result p3

    if-nez p3, :cond_0

    .line 389
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance p2, Lcom/squareup/orderentry/GiftCardWarning;

    invoke-direct {p2}, Lcom/squareup/orderentry/GiftCardWarning;-><init>()V

    invoke-interface {p1, p2}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    return-void

    .line 393
    :cond_0
    new-instance p3, Lcom/squareup/ui/main/-$$Lambda$CheckoutEntryHandler$YTuzXT8tPdStYZ3H_AjvPpPrL6w;

    invoke-direct {p3, p0, p4, p1, p5}, Lcom/squareup/ui/main/-$$Lambda$CheckoutEntryHandler$YTuzXT8tPdStYZ3H_AjvPpPrL6w;-><init>(Lcom/squareup/ui/main/CheckoutEntryHandler;Ljava/lang/String;Landroid/view/View;Ljava/lang/Class;)V

    .line 494
    sget-object p1, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;->CLICKED:Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    invoke-direct {p0, p2, p1, p3}, Lcom/squareup/ui/main/CheckoutEntryHandler;->addItemToCart(Ljava/lang/String;Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void

    .line 382
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p3, " is not supported."

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public itemClicked(Landroid/widget/ImageView;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Ljava/lang/String;)V
    .locals 1

    .line 366
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object p2

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/squareup/ui/main/CheckoutEntryHandler;->itemClicked(Landroid/view/View;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;)V

    return-void
.end method

.method public itemLongClicked(Landroid/view/View;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;)V
    .locals 1

    const/4 v0, 0x0

    .line 310
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/squareup/ui/main/CheckoutEntryHandler;->itemLongClicked(Landroid/view/View;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;)V

    return-void
.end method

.method public itemLongClicked(Landroid/view/View;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;)V
    .locals 2

    .line 319
    sget-object v0, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    if-eq p3, v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/squareup/api/items/Item$Type;

    .line 320
    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    invoke-direct {p0}, Lcom/squareup/ui/main/CheckoutEntryHandler;->assertNotInEditMode()V

    .line 326
    new-instance p3, Lcom/squareup/ui/main/-$$Lambda$CheckoutEntryHandler$NkXvsFYM1elNmqTr7a3lGStt_jw;

    invoke-direct {p3, p0, p1, p4}, Lcom/squareup/ui/main/-$$Lambda$CheckoutEntryHandler$NkXvsFYM1elNmqTr7a3lGStt_jw;-><init>(Lcom/squareup/ui/main/CheckoutEntryHandler;Landroid/view/View;Ljava/lang/String;)V

    .line 360
    sget-object p1, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;->LONG_CLICKED:Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    invoke-direct {p0, p2, p1, p3}, Lcom/squareup/ui/main/CheckoutEntryHandler;->addItemToCart(Ljava/lang/String;Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void

    .line 321
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p3, " is not supported for long clicking."

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public itemLongClicked(Landroid/widget/ImageView;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Ljava/lang/String;)V
    .locals 1

    .line 301
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object p2

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/squareup/ui/main/CheckoutEntryHandler;->itemLongClicked(Landroid/view/View;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;)V

    return-void
.end method

.method public itemNotFound(Ljava/lang/String;)V
    .locals 2

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/BarcodeNotFoundScreen;

    invoke-direct {v1, p1}, Lcom/squareup/ui/BarcodeNotFoundScreen;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public itemScanned(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 218
    new-instance v0, Lcom/squareup/ui/main/-$$Lambda$CheckoutEntryHandler$MIhTJ8tnr3EFZI1PY4vtZlBcBU4;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/main/-$$Lambda$CheckoutEntryHandler$MIhTJ8tnr3EFZI1PY4vtZlBcBU4;-><init>(Lcom/squareup/ui/main/CheckoutEntryHandler;Ljava/lang/String;)V

    .line 295
    sget-object p2, Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;->SCANNED:Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/main/CheckoutEntryHandler;->addItemToCart(Ljava/lang/String;Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public itemSuggestionClicked(Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;)V
    .locals 3

    .line 599
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ORDER_ENTRY_ITEM_SUGGESTION:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 600
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->EDIT_ITEMS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/main/CheckoutEntryHandler$2;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/main/CheckoutEntryHandler$2;-><init>(Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public synthetic lambda$addDiscount$2$CheckoutEntryHandler(Lcom/squareup/shared/catalog/models/CatalogDiscount;)V
    .locals 2

    .line 208
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_MAIN:Lcom/squareup/analytics/RegisterViewName;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logDiscountEvent(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/shared/catalog/models/CatalogDiscount;)V

    return-void
.end method

.method public synthetic lambda$addOrderItemAndStartFlyByAnimation$6$CheckoutEntryHandler(Lcom/squareup/configure/item/WorkingItem;)V
    .locals 2

    .line 574
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_MAIN:Lcom/squareup/analytics/RegisterViewName;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logItemEvent(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/WorkingItem;)V

    return-void
.end method

.method public synthetic lambda$discountClicked$1$CheckoutEntryHandler(Landroid/view/View;ZLjava/lang/Class;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 9

    .line 163
    invoke-interface {p4}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 164
    invoke-virtual {p4}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->getPasscodeRequired()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    iget-object v6, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v7, Lcom/squareup/permissions/Permission;->USE_PASSCODE_RESTRICTED_DISCOUNT:Lcom/squareup/permissions/Permission;

    new-instance v8, Lcom/squareup/ui/main/CheckoutEntryHandler$1;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/main/CheckoutEntryHandler$1;-><init>(Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/shared/catalog/models/CatalogDiscount;Landroid/view/View;ZLjava/lang/Class;)V

    invoke-virtual {v6, v7, v8}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    .line 173
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object p2, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_PERMISSION_PASSCODE_POPUP:Lcom/squareup/analytics/RegisterViewName;

    invoke-virtual {p1, p2, p4}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logDiscountEvent(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/shared/catalog/models/CatalogDiscount;)V

    goto :goto_0

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 177
    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeProtoNonNull(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;

    move-result-object v5

    move-object v0, p0

    move-object v1, p4

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    .line 176
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/main/CheckoutEntryHandler;->addDiscount(Lcom/squareup/shared/catalog/models/CatalogDiscount;Landroid/view/View;ZLjava/lang/Class;Lcom/squareup/protos/client/Employee;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$itemClicked$5$CheckoutEntryHandler(Ljava/lang/String;Landroid/view/View;Ljava/lang/Class;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 8

    .line 395
    invoke-interface {p4}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;

    .line 397
    invoke-virtual {p4}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getItem()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v0

    .line 398
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getAbbreviationOrAbbreviatedName()Ljava/lang/String;

    move-result-object v4

    .line 399
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getColor()Ljava/lang/String;

    move-result-object v5

    .line 401
    invoke-direct {p0, p4}, Lcom/squareup/ui/main/CheckoutEntryHandler;->createWorkingItem(Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;)Lcom/squareup/configure/item/WorkingItem;

    move-result-object v3

    .line 404
    invoke-direct {p0, p1, p4, v3}, Lcom/squareup/ui/main/CheckoutEntryHandler;->attemptToPreSelectVariation(Ljava/lang/String;Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;Lcom/squareup/configure/item/WorkingItem;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 410
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem;->isGiftCard()Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    .line 411
    invoke-virtual {p4}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p1

    const-string p3, "Gift cards variations"

    invoke-static {p1, v0, p3}, Lcom/squareup/util/Preconditions;->enforceSize(Ljava/util/Collection;ILjava/lang/String;)Ljava/util/Collection;

    .line 412
    invoke-virtual {v3}, Lcom/squareup/configure/item/WorkingItem;->selectOnlyVariation()V

    .line 413
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {p1, v4, v5, p2}, Lcom/squareup/orderentry/OrderEntryScreenState;->enqueueFlyByAnimationForItem(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    .line 414
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->flow:Lflow/Flow;

    new-instance p2, Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen;

    invoke-direct {p2, v3}, Lcom/squareup/ui/library/giftcard/GiftCardActivationScreen;-><init>(Lcom/squareup/configure/item/WorkingItem;)V

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 422
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->isSkipModifierDetailScreenEnabled()Z

    move-result p1

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 423
    invoke-virtual {v3}, Lcom/squareup/configure/item/WorkingItem;->skipsConfigureItemDetailScreen()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 425
    invoke-virtual {p4}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isVariablePricing()Z

    move-result v2

    move-object v1, p0

    move-object v6, p2

    move-object v7, p3

    .line 426
    invoke-virtual/range {v1 .. v7}, Lcom/squareup/ui/main/CheckoutEntryHandler;->skipConfigureScreenForClickedItem(ZLcom/squareup/configure/item/WorkingItem;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;Ljava/lang/Class;)V

    return-void

    .line 430
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_SKIP_ITEM_MODAL_STILL_SHOWN:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v2}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 437
    :cond_3
    invoke-virtual {p4}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-ne p1, v0, :cond_4

    .line 438
    invoke-virtual {p4}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isVariablePricing()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 439
    invoke-virtual {p4}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getModifierLists()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result p1

    if-lez p1, :cond_4

    .line 442
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v3, p1}, Lcom/squareup/configure/item/WorkingItem;->setZeroCurrentAmount(Lcom/squareup/protos/common/CurrencyCode;)V

    .line 443
    invoke-virtual {v3, v1}, Lcom/squareup/configure/item/WorkingItem;->selectVariation(I)V

    .line 444
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {p1, v4, v5, p2}, Lcom/squareup/orderentry/OrderEntryScreenState;->enqueueFlyByAnimationForItem(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    .line 445
    invoke-direct {p0, v3, v0}, Lcom/squareup/ui/main/CheckoutEntryHandler;->goToConfigureWorkingItem(Lcom/squareup/configure/item/WorkingItem;Z)V

    return-void

    .line 450
    :cond_4
    invoke-virtual {p4}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-gt p1, v0, :cond_9

    invoke-virtual {p4}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getModifierLists()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result p1

    if-lez p1, :cond_5

    goto :goto_0

    .line 463
    :cond_5
    invoke-virtual {p4}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-ne p1, v0, :cond_7

    iget-object p1, v3, Lcom/squareup/configure/item/WorkingItem;->variations:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 464
    invoke-virtual {p4}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isVariablePricing()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 467
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v3, p1}, Lcom/squareup/configure/item/WorkingItem;->setZeroCurrentAmount(Lcom/squareup/protos/common/CurrencyCode;)V

    .line 468
    invoke-virtual {v3}, Lcom/squareup/configure/item/WorkingItem;->selectOnlyVariation()V

    .line 469
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {p1, v4, v5, p2}, Lcom/squareup/orderentry/OrderEntryScreenState;->enqueueFlyByAnimationForItem(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    .line 470
    invoke-direct {p0, v3, v0}, Lcom/squareup/ui/main/CheckoutEntryHandler;->goToConfigureWorkingItem(Lcom/squareup/configure/item/WorkingItem;Z)V

    return-void

    .line 475
    :cond_6
    invoke-virtual {v3}, Lcom/squareup/configure/item/WorkingItem;->selectOnlyVariation()V

    .line 476
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {p1, v4, v5, p2}, Lcom/squareup/orderentry/OrderEntryScreenState;->enqueueFlyByAnimationForItem(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    .line 477
    invoke-direct {p0, v3, v1}, Lcom/squareup/ui/main/CheckoutEntryHandler;->goToConfigureWorkingItem(Lcom/squareup/configure/item/WorkingItem;Z)V

    return-void

    .line 484
    :cond_7
    invoke-virtual {p4}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-ne p1, v0, :cond_8

    invoke-virtual {p4}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isVariablePricing()Z

    move-result p1

    if-nez p1, :cond_8

    .line 485
    invoke-direct {p0, v3, p3, p2}, Lcom/squareup/ui/main/CheckoutEntryHandler;->addOrderItemAndStartFlyByAnimation(Lcom/squareup/configure/item/WorkingItem;Ljava/lang/Class;Landroid/view/View;)V

    return-void

    .line 491
    :cond_8
    invoke-direct {p0, v3, v4, v5, p2}, Lcom/squareup/ui/main/CheckoutEntryHandler;->goToPriceEntryScreen(Lcom/squareup/configure/item/WorkingItem;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    return-void

    .line 451
    :cond_9
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->canEditItemWithItemOptions()Z

    move-result p1

    if-eqz p1, :cond_a

    .line 452
    invoke-virtual {p4}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getItemOptions()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_a

    goto :goto_1

    :cond_a
    const/4 v0, 0x0

    .line 454
    :goto_1
    invoke-virtual {p4}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isVariablePricing()Z

    move-result p1

    if-nez p1, :cond_b

    if-nez v0, :cond_b

    .line 455
    invoke-virtual {v3, v1}, Lcom/squareup/configure/item/WorkingItem;->selectVariation(I)V

    .line 457
    :cond_b
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {p1, v4, v5, p2}, Lcom/squareup/orderentry/OrderEntryScreenState;->enqueueFlyByAnimationForItem(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    .line 458
    invoke-direct {p0, v3, v1}, Lcom/squareup/ui/main/CheckoutEntryHandler;->goToConfigureWorkingItem(Lcom/squareup/configure/item/WorkingItem;Z)V

    return-void
.end method

.method public synthetic lambda$itemLongClicked$4$CheckoutEntryHandler(Landroid/view/View;Ljava/lang/String;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 4

    .line 327
    invoke-interface {p3}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;

    .line 328
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->isPasscodeLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 334
    :cond_0
    invoke-direct {p0, p3}, Lcom/squareup/ui/main/CheckoutEntryHandler;->createWorkingItem(Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;)Lcom/squareup/configure/item/WorkingItem;

    move-result-object v0

    .line 335
    iget-object v1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 336
    invoke-virtual {p3}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getItem()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getAbbreviationOrAbbreviatedName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getItem()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItem;->getColor()Ljava/lang/String;

    move-result-object v3

    .line 335
    invoke-virtual {v1, v2, v3, p1}, Lcom/squareup/orderentry/OrderEntryScreenState;->enqueueFlyByAnimationForItem(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    .line 339
    invoke-direct {p0, p2, p3, v0}, Lcom/squareup/ui/main/CheckoutEntryHandler;->attemptToPreSelectVariation(Ljava/lang/String;Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;Lcom/squareup/configure/item/WorkingItem;)Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    .line 344
    :cond_1
    invoke-virtual {p3}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 p2, 0x1

    const/4 v1, 0x0

    if-ne p1, p2, :cond_2

    .line 345
    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/WorkingItem;->selectVariation(I)V

    .line 347
    invoke-virtual {p3}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isVariablePricing()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 348
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/WorkingItem;->setZeroCurrentAmount(Lcom/squareup/protos/common/CurrencyCode;)V

    .line 349
    invoke-direct {p0, v0, p2}, Lcom/squareup/ui/main/CheckoutEntryHandler;->goToConfigureWorkingItem(Lcom/squareup/configure/item/WorkingItem;Z)V

    return-void

    .line 355
    :cond_2
    invoke-virtual {p3}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isVariablePricing()Z

    move-result p1

    if-nez p1, :cond_3

    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/WorkingItem;->selectVariation(I)V

    .line 357
    :cond_3
    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/main/CheckoutEntryHandler;->goToConfigureWorkingItem(Lcom/squareup/configure/item/WorkingItem;Z)V

    return-void
.end method

.method public synthetic lambda$itemScanned$3$CheckoutEntryHandler(Ljava/lang/String;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 5

    .line 219
    invoke-interface {p2}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->isPasscodeLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 226
    :cond_0
    invoke-direct {p0, p2}, Lcom/squareup/ui/main/CheckoutEntryHandler;->createWorkingItem(Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;)Lcom/squareup/configure/item/WorkingItem;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 229
    :goto_0
    iget-object v4, v0, Lcom/squareup/configure/item/WorkingItem;->variations:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 230
    iget-object v4, v0, Lcom/squareup/configure/item/WorkingItem;->variations:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v4}, Lcom/squareup/checkout/OrderVariation;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 231
    invoke-virtual {v0, v3}, Lcom/squareup/configure/item/WorkingItem;->selectVariation(I)V

    move v1, v3

    goto :goto_1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 241
    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->isSkipModifierDetailScreenEnabled()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 242
    invoke-virtual {v0}, Lcom/squareup/configure/item/WorkingItem;->skipsConfigureItemDetailScreen()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 243
    invoke-virtual {p2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isVariablePricing()Z

    move-result p1

    .line 244
    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/main/CheckoutEntryHandler;->skipConfigureScreenForScannedItem(ZLcom/squareup/configure/item/WorkingItem;)V

    return-void

    .line 247
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v3, Lcom/squareup/analytics/RegisterActionName;->ADVANCED_MODIFIER_SKIP_ITEM_MODAL_STILL_SHOWN:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v3}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 253
    :cond_4
    invoke-virtual {p2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isVariablePricing()Z

    move-result p1

    const/4 v3, 0x1

    if-eqz p1, :cond_5

    .line 254
    invoke-virtual {p2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getModifierLists()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result p1

    if-lez p1, :cond_5

    .line 257
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/WorkingItem;->setZeroCurrentAmount(Lcom/squareup/protos/common/CurrencyCode;)V

    .line 258
    invoke-direct {p0, v0, v3}, Lcom/squareup/ui/main/CheckoutEntryHandler;->goToConfigureWorkingItem(Lcom/squareup/configure/item/WorkingItem;Z)V

    return-void

    .line 263
    :cond_5
    invoke-virtual {p2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getModifierLists()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result p1

    if-lez p1, :cond_6

    .line 264
    invoke-direct {p0, v0, v2}, Lcom/squareup/ui/main/CheckoutEntryHandler;->goToConfigureWorkingItem(Lcom/squareup/configure/item/WorkingItem;Z)V

    return-void

    .line 269
    :cond_6
    iget-object p1, v0, Lcom/squareup/configure/item/WorkingItem;->variations:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result p1

    if-eqz p1, :cond_8

    .line 270
    invoke-virtual {p2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isVariablePricing()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 273
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/WorkingItem;->setZeroCurrentAmount(Lcom/squareup/protos/common/CurrencyCode;)V

    .line 274
    invoke-direct {p0, v0, v3}, Lcom/squareup/ui/main/CheckoutEntryHandler;->goToConfigureWorkingItem(Lcom/squareup/configure/item/WorkingItem;Z)V

    return-void

    .line 278
    :cond_7
    invoke-direct {p0, v0, v2}, Lcom/squareup/ui/main/CheckoutEntryHandler;->goToConfigureWorkingItem(Lcom/squareup/configure/item/WorkingItem;Z)V

    return-void

    .line 284
    :cond_8
    invoke-virtual {p2}, Lcom/squareup/librarylist/LibraryListEntryHandler$CogsSelectedEntryData;->getVariations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isVariablePricing()Z

    move-result p1

    if-nez p1, :cond_9

    .line 285
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/configure/item/WorkingItem;->finish()Lcom/squareup/checkout/CartItem;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/payment/Transaction;->addOrderItem(Lcom/squareup/checkout/CartItem;)V

    return-void

    .line 291
    :cond_9
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/WorkingItem;->setZeroCurrentAmount(Lcom/squareup/protos/common/CurrencyCode;)V

    .line 292
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->priceEntryScreenRunner:Lcom/squareup/ui/library/PriceEntryScreenRunner;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/library/PriceEntryScreenRunner;->goToPriceEntry(Lcom/squareup/configure/item/WorkingItem;)V

    return-void
.end method

.method skipConfigureScreenForClickedItem(ZLcom/squareup/configure/item/WorkingItem;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/configure/item/WorkingItem;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 547
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/squareup/ui/main/CheckoutEntryHandler;->goToPriceEntryScreen(Lcom/squareup/configure/item/WorkingItem;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    return-void

    .line 552
    :cond_0
    invoke-direct {p0, p2, p6, p5}, Lcom/squareup/ui/main/CheckoutEntryHandler;->addOrderItemAndStartFlyByAnimation(Lcom/squareup/configure/item/WorkingItem;Ljava/lang/Class;Landroid/view/View;)V

    return-void
.end method

.method skipConfigureScreenForScannedItem(ZLcom/squareup/configure/item/WorkingItem;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 531
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {p2, p1}, Lcom/squareup/configure/item/WorkingItem;->setZeroCurrentAmount(Lcom/squareup/protos/common/CurrencyCode;)V

    .line 532
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->priceEntryScreenRunner:Lcom/squareup/ui/library/PriceEntryScreenRunner;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/library/PriceEntryScreenRunner;->goToPriceEntry(Lcom/squareup/configure/item/WorkingItem;)V

    return-void

    .line 537
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/configure/item/WorkingItem;->finish()Lcom/squareup/checkout/CartItem;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/payment/Transaction;->addOrderItem(Lcom/squareup/checkout/CartItem;)V

    return-void
.end method
