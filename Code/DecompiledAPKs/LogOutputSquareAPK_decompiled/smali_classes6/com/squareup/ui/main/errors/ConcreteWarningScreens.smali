.class public interface abstract Lcom/squareup/ui/main/errors/ConcreteWarningScreens;
.super Ljava/lang/Object;
.source "ConcreteWarningScreens.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/errors/ConcreteWarningScreens$InvalidTmsErrorScreen;,
        Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DarkTamperErrorScreen;,
        Lcom/squareup/ui/main/errors/ConcreteWarningScreens$CardNotRead;,
        Lcom/squareup/ui/main/errors/ConcreteWarningScreens$SecureSessionFlipperDenied;,
        Lcom/squareup/ui/main/errors/ConcreteWarningScreens$PaymentTimedOut;,
        Lcom/squareup/ui/main/errors/ConcreteWarningScreens$DeadReaderBattery;,
        Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcRequestTapCard;,
        Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcContactlessLimitExceededInsertCard;,
        Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcContactlessLimitExceededTryAnotherCard;,
        Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcCollisionDetected;,
        Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcTapAgain;,
        Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcCardDeclined;,
        Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcProcessingError;,
        Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcUnlockDevice;,
        Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcActionRequired;,
        Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcCallYourBank;,
        Lcom/squareup/ui/main/errors/ConcreteWarningScreens$NfcInterfaceUnavailable;
    }
.end annotation


# static fields
.field public static final UNLOCK_PHONE_SCREEN_TIMEOUT_MILLIS:J = 0xfa0L
