.class public Lcom/squareup/ui/main/errors/WarningWorkflow;
.super Ljava/lang/Object;
.source "WarningWorkflow.java"

# interfaces
.implements Lcom/squareup/ui/main/errors/WarningScreenController;


# instance fields
.field private final buttonFlowStarter:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

.field private final screenData:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/main/errors/WarningScreenData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/ui/main/errors/WarningScreenData;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/errors/WarningWorkflow;->screenData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 17
    iput-object p1, p0, Lcom/squareup/ui/main/errors/WarningWorkflow;->buttonFlowStarter:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    .line 19
    iget-object p1, p0, Lcom/squareup/ui/main/errors/WarningWorkflow;->screenData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected handleButtonPressed(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V
    .locals 3

    .line 52
    sget-object v0, Lcom/squareup/ui/main/errors/WarningWorkflow$1;->$SwitchMap$com$squareup$ui$main$errors$WarningScreenButtonConfig$ButtonBehaviorType:[I

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 72
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string p1, "Unrecognized ButtonBehaviorType %s"

    .line 73
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :pswitch_0
    iget-object p1, p0, Lcom/squareup/ui/main/errors/WarningWorkflow;->buttonFlowStarter:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->resumePaymentFlowAtPaymentPrompt()V

    goto :goto_0

    .line 66
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/ui/main/errors/WarningWorkflow;->buttonFlowStarter:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->dismiss()V

    goto :goto_0

    .line 63
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/ui/main/errors/WarningWorkflow;->buttonFlowStarter:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->killTenderHumanInitiatedAndTurnOffReader()V

    :goto_0
    return-void

    .line 60
    :pswitch_3
    iget-object p1, p0, Lcom/squareup/ui/main/errors/WarningWorkflow;->buttonFlowStarter:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->killTenderReaderInitiated()V

    return-void

    .line 57
    :pswitch_4
    iget-object p1, p0, Lcom/squareup/ui/main/errors/WarningWorkflow;->buttonFlowStarter:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->startActivation()V

    return-void

    .line 54
    :pswitch_5
    iget-object p1, p0, Lcom/squareup/ui/main/errors/WarningWorkflow;->buttonFlowStarter:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->contactSupport()V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningWorkflow;->screenData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/errors/WarningScreenData;

    iget-object v0, v0, Lcom/squareup/ui/main/errors/WarningScreenData;->defaultButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    iget-object v0, v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->buttonBehaviorType:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/main/errors/WarningWorkflow;->handleButtonPressed(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V

    return-void
.end method

.method public onCancelPressed()V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningWorkflow;->screenData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/errors/WarningScreenData;

    iget-object v0, v0, Lcom/squareup/ui/main/errors/WarningScreenData;->defaultButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    iget-object v0, v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->buttonBehaviorType:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/main/errors/WarningWorkflow;->handleButtonPressed(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V

    return-void
.end method

.method public onDefaultButtonPressed(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V
    .locals 0

    .line 40
    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/errors/WarningWorkflow;->handleButtonPressed(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V

    return-void
.end method

.method public onTimeout(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V
    .locals 0

    .line 32
    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/errors/WarningWorkflow;->handleButtonPressed(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V

    return-void
.end method

.method public onTopAlternativeButtonPressed(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V
    .locals 0

    .line 44
    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/errors/WarningWorkflow;->handleButtonPressed(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V

    return-void
.end method

.method public screenData()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/main/errors/WarningScreenData;",
            ">;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningWorkflow;->screenData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method
