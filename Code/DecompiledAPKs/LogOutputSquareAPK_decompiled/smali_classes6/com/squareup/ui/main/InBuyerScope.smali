.class public abstract Lcom/squareup/ui/main/InBuyerScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "InBuyerScope.java"

# interfaces
.implements Lcom/squareup/ui/buyer/InBuyerFlow;


# instance fields
.field public final buyerPath:Lcom/squareup/ui/buyer/BuyerScope;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/squareup/ui/main/InBuyerScope;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    return-void
.end method


# virtual methods
.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/main/InBuyerScope;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    return-object v0
.end method
