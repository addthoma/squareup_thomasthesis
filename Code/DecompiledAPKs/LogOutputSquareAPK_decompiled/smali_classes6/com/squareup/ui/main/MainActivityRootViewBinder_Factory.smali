.class public final Lcom/squareup/ui/main/MainActivityRootViewBinder_Factory;
.super Ljava/lang/Object;
.source "MainActivityRootViewBinder_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/MainActivityRootViewBinder;",
        ">;"
    }
.end annotation


# instance fields
.field private final impersonatingBannerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ImpersonatingBanner;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/MainActivityContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final printErrorPopupViewBinderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PrintErrorPopupViewBinder;",
            ">;"
        }
    .end annotation
.end field

.field private final storedPaymentRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ImpersonatingBanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PrintErrorPopupViewBinder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/MainActivityContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder_Factory;->impersonatingBannerProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder_Factory;->printErrorPopupViewBinderProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder_Factory;->storedPaymentRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/MainActivityRootViewBinder_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ImpersonatingBanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PrintErrorPopupViewBinder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/MainActivityContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;",
            ">;)",
            "Lcom/squareup/ui/main/MainActivityRootViewBinder_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/ui/main/MainActivityRootViewBinder_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/main/MainActivityRootViewBinder_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljava/lang/Object;Lcom/squareup/ui/main/PrintErrorPopupViewBinder;Lcom/squareup/ui/main/MainActivityContainer;Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;)Lcom/squareup/ui/main/MainActivityRootViewBinder;
    .locals 1

    .line 52
    new-instance v0, Lcom/squareup/ui/main/MainActivityRootViewBinder;

    check-cast p0, Lcom/squareup/ui/main/ImpersonatingBanner;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/main/MainActivityRootViewBinder;-><init>(Lcom/squareup/ui/main/ImpersonatingBanner;Lcom/squareup/ui/main/PrintErrorPopupViewBinder;Lcom/squareup/ui/main/MainActivityContainer;Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/MainActivityRootViewBinder;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder_Factory;->impersonatingBannerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder_Factory;->printErrorPopupViewBinderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/main/PrintErrorPopupViewBinder;

    iget-object v2, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/main/MainActivityContainer;

    iget-object v3, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder_Factory;->storedPaymentRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/main/MainActivityRootViewBinder_Factory;->newInstance(Ljava/lang/Object;Lcom/squareup/ui/main/PrintErrorPopupViewBinder;Lcom/squareup/ui/main/MainActivityContainer;Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;)Lcom/squareup/ui/main/MainActivityRootViewBinder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivityRootViewBinder_Factory;->get()Lcom/squareup/ui/main/MainActivityRootViewBinder;

    move-result-object v0

    return-object v0
.end method
