.class public Lcom/squareup/ui/main/R6VideoLauncher;
.super Lcom/squareup/ui/main/ReaderTutorialLauncher;
.source "R6VideoLauncher.java"

# interfaces
.implements Lcom/squareup/ui/main/R6ForceableContentLauncher;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/main/ReaderTutorialLauncher<",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/squareup/ui/main/R6ForceableContentLauncher;"
    }
.end annotation


# instance fields
.field private final flow:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;


# direct methods
.method constructor <init>(Ldagger/Lazy;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/main/PosContainer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/ui/main/PosContainer;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0, p2}, Lcom/squareup/ui/main/ReaderTutorialLauncher;-><init>(Lcom/squareup/settings/LocalSetting;)V

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/main/R6VideoLauncher;->flow:Ldagger/Lazy;

    .line 20
    iput-object p3, p0, Lcom/squareup/ui/main/R6VideoLauncher;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    return-void
.end method


# virtual methods
.method protected canShowTutorial()Z
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/main/R6VideoLauncher;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    const-class v1, Lcom/squareup/register/tutorial/R6VideoLauncherAllowed;

    invoke-interface {v0, v1}, Lcom/squareup/ui/main/PosContainer;->currentPathIncludes(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public isContentShowing()Z
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/main/R6VideoLauncher;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    const-class v1, Lcom/squareup/ui/main/R6FirstTimeVideoScreen;

    invoke-interface {v0, v1}, Lcom/squareup/ui/main/PosContainer;->currentPathIncludes(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic showContent(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/R6VideoLauncher;->showContent(Ljava/lang/Void;)V

    return-void
.end method

.method public showContent(Ljava/lang/Void;)V
    .locals 1

    .line 24
    iget-object p1, p0, Lcom/squareup/ui/main/R6VideoLauncher;->flow:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/main/R6FirstTimeVideoScreen;->INSTANCE:Lcom/squareup/ui/main/R6FirstTimeVideoScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
