.class final Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$7;
.super Lkotlin/jvm/internal/Lambda;
.source "MainActivityScopeRegistrar.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/MainActivityScopeRegistrar;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/connectivity/InternetState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/connectivity/InternetState;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/MainActivityScopeRegistrar;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/MainActivityScopeRegistrar;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$7;->this$0:Lcom/squareup/ui/main/MainActivityScopeRegistrar;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/connectivity/InternetState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$7;->invoke(Lcom/squareup/connectivity/InternetState;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/connectivity/InternetState;)V
    .locals 3

    .line 163
    iget-object p1, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$7;->this$0:Lcom/squareup/ui/main/MainActivityScopeRegistrar;

    invoke-static {p1}, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->access$getCardReaderHub$p(Lcom/squareup/ui/main/MainActivityScopeRegistrar;)Lcom/squareup/cardreader/CardReaderHub;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReader;

    const-string v1, "cardReader"

    .line 164
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    const-string v2, "cardReader.cardReaderInfo"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->getSecureSessionState()Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;

    move-result-object v1

    sget-object v2, Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;->ABORTED:Lcom/squareup/cardreader/CardReaderInfo$SecureSessionState;

    if-ne v1, v2, :cond_0

    .line 165
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->reinitializeSecureSession()V

    goto :goto_0

    :cond_1
    return-void
.end method
