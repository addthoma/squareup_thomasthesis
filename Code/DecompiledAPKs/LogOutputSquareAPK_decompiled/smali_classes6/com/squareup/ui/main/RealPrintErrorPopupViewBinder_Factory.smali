.class public final Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder_Factory;
.super Ljava/lang/Object;
.source "RealPrintErrorPopupViewBinder_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder;",
        ">;"
    }
.end annotation


# instance fields
.field private final printErrorPopupPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/print/PrintErrorPopupPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/print/PrintErrorPopupPresenter;",
            ">;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder_Factory;->printErrorPopupPresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/print/PrintErrorPopupPresenter;",
            ">;)",
            "Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder_Factory;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/print/PrintErrorPopupPresenter;)Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder;
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder;-><init>(Lcom/squareup/ui/print/PrintErrorPopupPresenter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder_Factory;->printErrorPopupPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    invoke-static {v0}, Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder_Factory;->newInstance(Lcom/squareup/ui/print/PrintErrorPopupPresenter;)Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder_Factory;->get()Lcom/squareup/ui/main/RealPrintErrorPopupViewBinder;

    move-result-object v0

    return-object v0
.end method
