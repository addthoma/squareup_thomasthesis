.class public Lcom/squareup/ui/main/Es1TransactionMetrics;
.super Ljava/lang/Object;
.source "Es1TransactionMetrics.java"

# interfaces
.implements Lcom/squareup/ui/main/TransactionMetrics;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/Es1TransactionMetrics$TransactionTimingEvent;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private buyerFacing:Z

.field private buyerFacingTime:J

.field private final clock:Lcom/squareup/util/Clock;

.field private inTransaction:Z

.field private merchantFacingTime:J

.field private screenStartTime:J

.field private transactionStartTime:J


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method private accumulateScreenTime(J)V
    .locals 2

    .line 73
    iget-wide v0, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->screenStartTime:J

    sub-long/2addr p1, v0

    .line 74
    iget-boolean v0, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->buyerFacing:Z

    if-eqz v0, :cond_0

    .line 75
    iget-wide v0, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->buyerFacingTime:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->buyerFacingTime:J

    goto :goto_0

    .line 77
    :cond_0
    iget-wide v0, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->merchantFacingTime:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->merchantFacingTime:J

    :goto_0
    return-void
.end method

.method private log(JLjava/lang/String;)V
    .locals 10

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 82
    iget-wide v1, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->merchantFacingTime:J

    .line 83
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-wide v1, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->buyerFacingTime:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "Transaction time: %s ms merchant facing, %s ms customer facing, %s ms total"

    .line 82
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v9, Lcom/squareup/ui/main/Es1TransactionMetrics$TransactionTimingEvent;

    iget-wide v5, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->buyerFacingTime:J

    iget-wide v7, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->merchantFacingTime:J

    move-object v1, v9

    move-object v2, p3

    move-wide v3, p1

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/main/Es1TransactionMetrics$TransactionTimingEvent;-><init>(Ljava/lang/String;JJJ)V

    invoke-interface {v0, v9}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private onScreenChange(Z)V
    .locals 3

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    .line 49
    iget-boolean v2, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->inTransaction:Z

    if-nez v2, :cond_0

    return-void

    .line 54
    :cond_0
    iget-boolean v2, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->buyerFacing:Z

    if-eq p1, v2, :cond_1

    .line 55
    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/main/Es1TransactionMetrics;->accumulateScreenTime(J)V

    .line 56
    iput-wide v0, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->screenStartTime:J

    .line 57
    iput-boolean p1, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->buyerFacing:Z

    :cond_1
    return-void
.end method


# virtual methods
.method public beginTransaction()V
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    const/4 v2, 0x0

    .line 36
    iput-boolean v2, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->buyerFacing:Z

    const/4 v2, 0x1

    .line 37
    iput-boolean v2, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->inTransaction:Z

    .line 38
    iput-wide v0, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->screenStartTime:J

    iput-wide v0, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->transactionStartTime:J

    const-wide/16 v0, 0x0

    .line 39
    iput-wide v0, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->buyerFacingTime:J

    iput-wide v0, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->merchantFacingTime:J

    return-void
.end method

.method public endTransaction(Ljava/lang/String;)V
    .locals 4

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    .line 63
    iget-boolean v2, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->inTransaction:Z

    if-nez v2, :cond_0

    return-void

    :cond_0
    const/4 v2, 0x0

    .line 66
    iput-boolean v2, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->inTransaction:Z

    .line 68
    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/main/Es1TransactionMetrics;->accumulateScreenTime(J)V

    .line 69
    iget-wide v2, p0, Lcom/squareup/ui/main/Es1TransactionMetrics;->transactionStartTime:J

    sub-long/2addr v0, v2

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/ui/main/Es1TransactionMetrics;->log(JLjava/lang/String;)V

    return-void
.end method

.method public onScreenChange(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1

    .line 43
    const-class v0, Lcom/squareup/ui/buyer/InBuyerFlow;

    invoke-virtual {p1, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p1

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/Es1TransactionMetrics;->onScreenChange(Z)V

    return-void
.end method
