.class final Lcom/squareup/ui/main/MainActivityContainer$buildRedirectPipeline$2;
.super Ljava/lang/Object;
.source "MainActivityContainer.kt"

# interfaces
.implements Lcom/squareup/container/RedirectStep;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/MainActivityContainer;->buildRedirectPipeline()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0001\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "traversal",
        "Lflow/Traversal;",
        "kotlin.jvm.PlatformType",
        "maybeRedirect"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/MainActivityContainer;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/MainActivityContainer;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/main/MainActivityContainer$buildRedirectPipeline$2;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic maybeRedirect(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 0

    .line 77
    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/MainActivityContainer$buildRedirectPipeline$2;->maybeRedirect(Lflow/Traversal;)Ljava/lang/Void;

    move-result-object p1

    check-cast p1, Lcom/squareup/container/RedirectStep$Result;

    return-object p1
.end method

.method public final maybeRedirect(Lflow/Traversal;)Ljava/lang/Void;
    .locals 1

    .line 336
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$buildRedirectPipeline$2;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-static {v0}, Lcom/squareup/ui/main/MainActivityContainer;->access$getTransactionMetrics$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/squareup/ui/main/TransactionMetrics;

    move-result-object v0

    iget-object p1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {p1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    invoke-interface {v0, p1}, Lcom/squareup/ui/main/TransactionMetrics;->onScreenChange(Lcom/squareup/container/ContainerTreeKey;)V

    const/4 p1, 0x0

    return-object p1
.end method
