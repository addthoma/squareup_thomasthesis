.class public final Lcom/squareup/ui/main/ApiTransactionFactory_Parser_Factory;
.super Ljava/lang/Object;
.source "ApiTransactionFactory_Parser_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/ApiTransactionFactory$Parser;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiTransactionControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;"
        }
    .end annotation
.end field

.field private final apiTransactionStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/main/ApiTransactionFactory_Parser_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/main/ApiTransactionFactory_Parser_Factory;->apiTransactionControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/ApiTransactionFactory_Parser_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;)",
            "Lcom/squareup/ui/main/ApiTransactionFactory_Parser_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/ui/main/ApiTransactionFactory_Parser_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/main/ApiTransactionFactory_Parser_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/api/ApiTransactionController;)Lcom/squareup/ui/main/ApiTransactionFactory$Parser;
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/ui/main/ApiTransactionFactory$Parser;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/main/ApiTransactionFactory$Parser;-><init>(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/api/ApiTransactionController;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/ApiTransactionFactory$Parser;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/main/ApiTransactionFactory_Parser_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ApiTransactionState;

    iget-object v1, p0, Lcom/squareup/ui/main/ApiTransactionFactory_Parser_Factory;->apiTransactionControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/ApiTransactionController;

    invoke-static {v0, v1}, Lcom/squareup/ui/main/ApiTransactionFactory_Parser_Factory;->newInstance(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/api/ApiTransactionController;)Lcom/squareup/ui/main/ApiTransactionFactory$Parser;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/main/ApiTransactionFactory_Parser_Factory;->get()Lcom/squareup/ui/main/ApiTransactionFactory$Parser;

    move-result-object v0

    return-object v0
.end method
