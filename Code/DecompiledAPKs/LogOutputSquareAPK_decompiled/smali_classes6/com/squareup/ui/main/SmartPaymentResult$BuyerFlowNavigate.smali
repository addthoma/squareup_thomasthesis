.class public final Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowNavigate;
.super Ljava/lang/Object;
.source "SmartPaymentResult.java"

# interfaces
.implements Lcom/squareup/ui/main/SmartPaymentResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/SmartPaymentResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BuyerFlowNavigate"
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowNavigate;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowNavigate;

    invoke-direct {v0}, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowNavigate;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowNavigate;->INSTANCE:Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowNavigate;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatch(Lcom/squareup/ui/main/SmartPaymentResult$SmartPaymentResultHandler;)V
    .locals 0

    .line 37
    invoke-interface {p1, p0}, Lcom/squareup/ui/main/SmartPaymentResult$SmartPaymentResultHandler;->onResult(Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowNavigate;)V

    return-void
.end method
