.class public interface abstract Lcom/squareup/ui/main/AddNoteScreen$Component;
.super Ljava/lang/Object;
.source "AddNoteScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/main/AddNoteScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/AddNoteScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/main/AddNoteView;)V
.end method
