.class public final Lcom/squareup/ui/main/LibraryCreateNewItemDialog;
.super Lcom/squareup/ui/seller/InSellerScope;
.source "LibraryCreateNewItemDialog.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/main/LibraryCreateNewItemDialog$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/LibraryCreateNewItemDialog$ParentComponent;,
        Lcom/squareup/ui/main/LibraryCreateNewItemDialog$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLibraryCreateNewItemDialog.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LibraryCreateNewItemDialog.kt\ncom/squareup/ui/main/LibraryCreateNewItemDialog\n+ 2 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,57:1\n24#2,4:58\n*E\n*S KotlinDebug\n*F\n+ 1 LibraryCreateNewItemDialog.kt\ncom/squareup/ui/main/LibraryCreateNewItemDialog\n*L\n55#1,4:58\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c7\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0002\u0006\u0007B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003R\u0016\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/ui/main/LibraryCreateNewItemDialog;",
        "Lcom/squareup/ui/seller/InSellerScope;",
        "Landroid/os/Parcelable;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "Factory",
        "ParentComponent",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/main/LibraryCreateNewItemDialog;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/main/LibraryCreateNewItemDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/ui/main/LibraryCreateNewItemDialog;

    invoke-direct {v0}, Lcom/squareup/ui/main/LibraryCreateNewItemDialog;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/LibraryCreateNewItemDialog;->INSTANCE:Lcom/squareup/ui/main/LibraryCreateNewItemDialog;

    .line 58
    new-instance v0, Lcom/squareup/ui/main/LibraryCreateNewItemDialog$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/ui/main/LibraryCreateNewItemDialog$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 61
    sput-object v0, Lcom/squareup/ui/main/LibraryCreateNewItemDialog;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/seller/InSellerScope;-><init>()V

    return-void
.end method
