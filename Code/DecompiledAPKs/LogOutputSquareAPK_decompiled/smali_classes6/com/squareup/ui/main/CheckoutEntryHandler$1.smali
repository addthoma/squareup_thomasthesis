.class Lcom/squareup/ui/main/CheckoutEntryHandler$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "CheckoutEntryHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/CheckoutEntryHandler;->lambda$discountClicked$1(Landroid/view/View;ZLjava/lang/Class;Lcom/squareup/shared/catalog/CatalogResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/CheckoutEntryHandler;

.field final synthetic val$discount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

.field final synthetic val$screenShowing:Ljava/lang/Class;

.field final synthetic val$sourceView:Landroid/view/View;

.field final synthetic val$useDiscountDrawable:Z


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/shared/catalog/models/CatalogDiscount;Landroid/view/View;ZLjava/lang/Class;)V
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler$1;->this$0:Lcom/squareup/ui/main/CheckoutEntryHandler;

    iput-object p2, p0, Lcom/squareup/ui/main/CheckoutEntryHandler$1;->val$discount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    iput-object p3, p0, Lcom/squareup/ui/main/CheckoutEntryHandler$1;->val$sourceView:Landroid/view/View;

    iput-boolean p4, p0, Lcom/squareup/ui/main/CheckoutEntryHandler$1;->val$useDiscountDrawable:Z

    iput-object p5, p0, Lcom/squareup/ui/main/CheckoutEntryHandler$1;->val$screenShowing:Ljava/lang/Class;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 8

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler$1;->this$0:Lcom/squareup/ui/main/CheckoutEntryHandler;

    invoke-static {v0}, Lcom/squareup/ui/main/CheckoutEntryHandler;->access$200(Lcom/squareup/ui/main/CheckoutEntryHandler;)Lcom/squareup/log/cart/TransactionInteractionsLogger;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_PERMISSION_PASSCODE_POPUP:Lcom/squareup/analytics/RegisterViewName;

    invoke-virtual {v0, v1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;)V

    .line 169
    iget-object v2, p0, Lcom/squareup/ui/main/CheckoutEntryHandler$1;->this$0:Lcom/squareup/ui/main/CheckoutEntryHandler;

    iget-object v3, p0, Lcom/squareup/ui/main/CheckoutEntryHandler$1;->val$discount:Lcom/squareup/shared/catalog/models/CatalogDiscount;

    iget-object v4, p0, Lcom/squareup/ui/main/CheckoutEntryHandler$1;->val$sourceView:Landroid/view/View;

    iget-boolean v5, p0, Lcom/squareup/ui/main/CheckoutEntryHandler$1;->val$useDiscountDrawable:Z

    iget-object v6, p0, Lcom/squareup/ui/main/CheckoutEntryHandler$1;->val$screenShowing:Ljava/lang/Class;

    .line 170
    invoke-virtual {p0}, Lcom/squareup/ui/main/CheckoutEntryHandler$1;->getAuthorizedEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeInfo(Lcom/squareup/permissions/Employee;)Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler$1;->this$0:Lcom/squareup/ui/main/CheckoutEntryHandler;

    invoke-static {v1}, Lcom/squareup/ui/main/CheckoutEntryHandler;->access$300(Lcom/squareup/ui/main/CheckoutEntryHandler;)Lcom/squareup/util/Res;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeProtoNonNull(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;

    move-result-object v7

    .line 169
    invoke-static/range {v2 .. v7}, Lcom/squareup/ui/main/CheckoutEntryHandler;->access$400(Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/shared/catalog/models/CatalogDiscount;Landroid/view/View;ZLjava/lang/Class;Lcom/squareup/protos/client/Employee;)V

    return-void
.end method
