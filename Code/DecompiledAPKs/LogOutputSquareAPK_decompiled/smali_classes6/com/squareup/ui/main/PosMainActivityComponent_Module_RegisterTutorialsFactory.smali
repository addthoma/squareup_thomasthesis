.class public final Lcom/squareup/ui/main/PosMainActivityComponent_Module_RegisterTutorialsFactory;
.super Ljava/lang/Object;
.source "PosMainActivityComponent_Module_RegisterTutorialsFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/List<",
        "Lcom/squareup/register/tutorial/RegisterTutorial;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final cardTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final cashTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final newInvoiceFeaturesTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/main/PosMainActivityComponent_Module_RegisterTutorialsFactory;->cardTutorialProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/main/PosMainActivityComponent_Module_RegisterTutorialsFactory;->cashTutorialProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/main/PosMainActivityComponent_Module_RegisterTutorialsFactory;->invoiceTutorialProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/ui/main/PosMainActivityComponent_Module_RegisterTutorialsFactory;->newInvoiceFeaturesTutorialProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/PosMainActivityComponent_Module_RegisterTutorialsFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;",
            ">;)",
            "Lcom/squareup/ui/main/PosMainActivityComponent_Module_RegisterTutorialsFactory;"
        }
    .end annotation

    .line 52
    new-instance v0, Lcom/squareup/ui/main/PosMainActivityComponent_Module_RegisterTutorialsFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/main/PosMainActivityComponent_Module_RegisterTutorialsFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static registerTutorials(Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;",
            "Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;",
            "Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;",
            "Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/register/tutorial/RegisterTutorial;",
            ">;"
        }
    .end annotation

    .line 58
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/ui/main/PosMainActivityComponent$Module;->registerTutorials(Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;)Ljava/util/List;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/main/PosMainActivityComponent_Module_RegisterTutorialsFactory;->get()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/register/tutorial/RegisterTutorial;",
            ">;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/main/PosMainActivityComponent_Module_RegisterTutorialsFactory;->cardTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;

    iget-object v1, p0, Lcom/squareup/ui/main/PosMainActivityComponent_Module_RegisterTutorialsFactory;->cashTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;

    iget-object v2, p0, Lcom/squareup/ui/main/PosMainActivityComponent_Module_RegisterTutorialsFactory;->invoiceTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    iget-object v3, p0, Lcom/squareup/ui/main/PosMainActivityComponent_Module_RegisterTutorialsFactory;->newInvoiceFeaturesTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/main/PosMainActivityComponent_Module_RegisterTutorialsFactory;->registerTutorials(Lcom/squareup/register/tutorial/FirstPaymentCardTutorial;Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
