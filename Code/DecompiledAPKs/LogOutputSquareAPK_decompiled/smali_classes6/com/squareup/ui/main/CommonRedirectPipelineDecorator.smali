.class public Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;
.super Ljava/lang/Object;
.source "CommonRedirectPipelineDecorator.java"

# interfaces
.implements Lcom/squareup/ui/main/MainActivityContainer$RedirectPipelineDecorator;


# instance fields
.field private final appIdling:Lcom/squareup/ui/main/AppIdling;

.field private final applets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/applet/Applet;",
            ">;"
        }
    .end annotation
.end field

.field private final device:Lcom/squareup/util/Device;

.field private final employeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final home:Lcom/squareup/ui/main/Home;

.field private final homeScreenSelector:Lcom/squareup/ui/main/HomeScreenSelector;

.field private final remoteLogger:Lcom/squareup/logging/RemoteLogger;

.field private final squareDeviceTour:Lcom/squareup/SquareDeviceTour;

.field private final whatsNewUi:Lcom/squareup/tour/WhatsNewUi;


# direct methods
.method public constructor <init>(Lcom/squareup/SquareDeviceTour;Lcom/squareup/tour/WhatsNewUi;Lcom/squareup/applet/Applets;Lcom/squareup/util/Device;Lcom/squareup/ui/main/HomeScreenSelector;Lcom/squareup/ui/main/Home;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/logging/RemoteLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->squareDeviceTour:Lcom/squareup/SquareDeviceTour;

    .line 57
    iput-object p2, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->whatsNewUi:Lcom/squareup/tour/WhatsNewUi;

    .line 58
    invoke-interface {p3}, Lcom/squareup/applet/Applets;->getApplets()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->applets:Ljava/util/List;

    .line 59
    iput-object p4, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->device:Lcom/squareup/util/Device;

    .line 60
    iput-object p5, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->homeScreenSelector:Lcom/squareup/ui/main/HomeScreenSelector;

    .line 61
    iput-object p6, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->home:Lcom/squareup/ui/main/Home;

    .line 62
    iput-object p7, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->appIdling:Lcom/squareup/ui/main/AppIdling;

    .line 63
    iput-object p8, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->employeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 64
    iput-object p9, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->remoteLogger:Lcom/squareup/logging/RemoteLogger;

    return-void
.end method

.method private getEnteringAppletEntryPoint(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/applet/AppletEntryPoint;
    .locals 2

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->applets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/applet/Applet;

    .line 155
    invoke-virtual {v1, p1, p2}, Lcom/squareup/applet/Applet;->getEntryPoint(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/applet/AppletEntryPoint;

    move-result-object v1

    if-eqz v1, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public static synthetic lambda$8pV0nMIfMiyCE0FDC-moTo5Z4VA(Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->validateEmployeeToken(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic lambda$OqxL67P87Oe6-jXTXkFCx8FbSRg(Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->maybeSetAppIdling(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic lambda$dLoCrqNt6uOoVeQ8hhBGwa6AxD0(Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->redirectForExitFullScreen(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic lambda$yWKxkrdTZ9M0xdr17RCO22HxZzw(Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->redirectForAppletEntry(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;

    move-result-object p0

    return-object p0
.end method

.method private maybeSetAppIdling(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->home:Lcom/squareup/ui/main/Home;

    iget-object p1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-static {v0, p1}, Lcom/squareup/ui/main/HomeKt;->isHome(Lcom/squareup/ui/main/Home;Lflow/History;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 99
    iget-object p1, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->appIdling:Lcom/squareup/ui/main/AppIdling;

    invoke-virtual {p1}, Lcom/squareup/ui/main/AppIdling;->setIdle()V

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private redirectForAppletEntry(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 5

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->getCurrentScreenSize()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPhone()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 115
    :cond_0
    iget-object v0, p1, Lflow/Traversal;->origin:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    .line 116
    iget-object v2, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v2}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/container/ContainerTreeKey;

    .line 117
    invoke-direct {p0, v0, v2}, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->getEnteringAppletEntryPoint(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/applet/AppletEntryPoint;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 123
    instance-of v0, v0, Lcom/squareup/ui/permissions/PermissionDeniedScreen;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lflow/Traversal;->direction:Lflow/Direction;

    sget-object v4, Lflow/Direction;->BACKWARD:Lflow/Direction;

    if-eq v0, v4, :cond_2

    .line 124
    :cond_1
    invoke-virtual {v3, v2}, Lcom/squareup/applet/AppletEntryPoint;->getRedirect(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 126
    iget-object v1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v1

    .line 127
    invoke-virtual {v1}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    .line 128
    invoke-virtual {v1, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 129
    new-instance v0, Lcom/squareup/container/RedirectStep$Result;

    .line 130
    invoke-virtual {v1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object v1

    iget-object p1, p1, Lflow/Traversal;->direction:Lflow/Direction;

    invoke-static {v1, p1}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    const-string v1, "Redirect for applet entry"

    invoke-direct {v0, v1, p1}, Lcom/squareup/container/RedirectStep$Result;-><init>(Ljava/lang/String;Lcom/squareup/container/Command;)V

    return-object v0

    .line 138
    :cond_2
    invoke-static {v2}, Lcom/squareup/container/Masters;->getSection(Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Class;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 140
    const-class v0, Lcom/squareup/applet/AppletSection;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 146
    invoke-virtual {v3, p1}, Lcom/squareup/applet/AppletEntryPoint;->setSelectedSection(Ljava/lang/Class;)V

    goto :goto_0

    .line 141
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/squareup/applet/AppletSection;

    .line 142
    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 143
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    const-string p1, "%s subclass required, found %s"

    .line 142
    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    :goto_0
    return-object v1
.end method

.method private redirectForExitFullScreen(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 3

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->home:Lcom/squareup/ui/main/Home;

    iget-object v1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/main/HomeKt;->contains(Lcom/squareup/ui/main/Home;Ljava/lang/Object;Lflow/History;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p1, Lflow/Traversal;->origin:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    .line 175
    const-class v1, Lcom/squareup/container/spot/ModalBodyScreen;

    invoke-virtual {v0, v1}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lflow/Traversal;->origin:Lflow/History;

    .line 176
    invoke-static {v0}, Lcom/squareup/container/layer/HidesMasterKt;->hidesMaster(Lflow/History;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 178
    iget-object v0, p1, Lflow/Traversal;->direction:Lflow/Direction;

    sget-object v1, Lflow/Direction;->BACKWARD:Lflow/Direction;

    if-eq v0, v1, :cond_1

    .line 179
    new-instance v0, Lcom/squareup/container/RedirectStep$Result;

    iget-object p1, p1, Lflow/Traversal;->destination:Lflow/History;

    sget-object v1, Lflow/Direction;->BACKWARD:Lflow/Direction;

    .line 180
    invoke-static {p1, v1}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    const-string v1, "Redirect for exit full screen"

    invoke-direct {v0, v1, p1}, Lcom/squareup/container/RedirectStep$Result;-><init>(Ljava/lang/String;Lcom/squareup/container/Command;)V

    return-object v0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private validateEmployeeToken(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 2

    .line 188
    iget-object v0, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    .line 189
    iget-object p1, p1, Lflow/Traversal;->origin:Lflow/History;

    invoke-virtual {p1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;

    if-eqz p1, :cond_0

    instance-of p1, v0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;

    if-nez p1, :cond_0

    instance-of p1, v0, Lcom/squareup/ui/timecards/ClockInOutScreen;

    if-nez p1, :cond_0

    instance-of p1, v0, Lcom/squareup/ui/main/SessionExpiredDialog;

    if-nez p1, :cond_0

    .line 193
    iget-object p1, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->employeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isUnlocked()Z

    move-result p1

    if-nez p1, :cond_0

    .line 194
    iget-object p1, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->remoteLogger:Lcom/squareup/logging/RemoteLogger;

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Expected employee management to be unlocked when navigating away from the EnterPasscodeToUnlockScreen."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/logging/RemoteLogger;->w(Ljava/lang/Throwable;)V

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method public updatePipeline(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/container/RedirectStep;",
            ">;)V"
        }
    .end annotation

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 73
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 75
    iget-object v2, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->squareDeviceTour:Lcom/squareup/SquareDeviceTour;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/main/-$$Lambda$WIKiGme-bEeKAORBdOBjc_KLtas;

    invoke-direct {v3, v2}, Lcom/squareup/ui/main/-$$Lambda$WIKiGme-bEeKAORBdOBjc_KLtas;-><init>(Lcom/squareup/SquareDeviceTour;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    iget-object v2, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->whatsNewUi:Lcom/squareup/tour/WhatsNewUi;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/main/-$$Lambda$wgAretNosPJjE2s6RtD-bh-eyFs;

    invoke-direct {v3, v2}, Lcom/squareup/ui/main/-$$Lambda$wgAretNosPJjE2s6RtD-bh-eyFs;-><init>(Lcom/squareup/tour/WhatsNewUi;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    new-instance v2, Lcom/squareup/ui/main/-$$Lambda$CommonRedirectPipelineDecorator$yWKxkrdTZ9M0xdr17RCO22HxZzw;

    invoke-direct {v2, p0}, Lcom/squareup/ui/main/-$$Lambda$CommonRedirectPipelineDecorator$yWKxkrdTZ9M0xdr17RCO22HxZzw;-><init>(Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    new-instance v2, Lcom/squareup/ui/main/-$$Lambda$CommonRedirectPipelineDecorator$OqxL67P87Oe6-jXTXkFCx8FbSRg;

    invoke-direct {v2, p0}, Lcom/squareup/ui/main/-$$Lambda$CommonRedirectPipelineDecorator$OqxL67P87Oe6-jXTXkFCx8FbSRg;-><init>(Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    iget-object v2, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;->homeScreenSelector:Lcom/squareup/ui/main/HomeScreenSelector;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/main/-$$Lambda$8bAhg6CFce73EFzL3BJ9qFoJ-Rw;

    invoke-direct {v3, v2}, Lcom/squareup/ui/main/-$$Lambda$8bAhg6CFce73EFzL3BJ9qFoJ-Rw;-><init>(Lcom/squareup/ui/main/HomeScreenSelector;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    new-instance v2, Lcom/squareup/ui/main/-$$Lambda$CommonRedirectPipelineDecorator$dLoCrqNt6uOoVeQ8hhBGwa6AxD0;

    invoke-direct {v2, p0}, Lcom/squareup/ui/main/-$$Lambda$CommonRedirectPipelineDecorator$dLoCrqNt6uOoVeQ8hhBGwa6AxD0;-><init>(Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    new-instance v2, Lcom/squareup/ui/main/-$$Lambda$CommonRedirectPipelineDecorator$8pV0nMIfMiyCE0FDC-moTo5Z4VA;

    invoke-direct {v2, p0}, Lcom/squareup/ui/main/-$$Lambda$CommonRedirectPipelineDecorator$8pV0nMIfMiyCE0FDC-moTo5Z4VA;-><init>(Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x0

    .line 92
    invoke-interface {p1, v2, v0}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 93
    invoke-interface {p1, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method
