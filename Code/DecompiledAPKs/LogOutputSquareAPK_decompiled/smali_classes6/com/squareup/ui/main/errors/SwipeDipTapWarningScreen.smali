.class public abstract Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "SwipeDipTapWarningScreen.java"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/ModalBodyScreen;
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Component;,
        Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$ParentComponent;,
        Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Module;,
        Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$ComponentFactory;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getHideMaster()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected abstract getInitialScreenData(Lcom/squareup/util/Res;)Lcom/squareup/ui/main/errors/WarningScreenData;
.end method

.method public bridge synthetic provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 0

    .line 30
    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen;->provideCoordinator(Landroid/view/View;)Lcom/squareup/ui/main/errors/WarningCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/ui/main/errors/WarningCoordinator;
    .locals 2

    .line 42
    const-class v0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Component;

    .line 43
    new-instance v0, Lcom/squareup/ui/main/errors/WarningCoordinator;

    invoke-interface {p1}, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Component;->workflow()Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;

    move-result-object v1

    invoke-interface {p1}, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Component;->mainScheduler()Lrx/Scheduler;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/main/errors/WarningCoordinator;-><init>(Lcom/squareup/ui/main/errors/WarningWorkflow;Lrx/Scheduler;)V

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 47
    const-class v0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Component;

    invoke-interface {v0}, Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$Component;->workflow()Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->register(Lmortar/MortarScope;)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 34
    sget v0, Lcom/squareup/cardreader/ui/R$layout;->warning_view:I

    return v0
.end method
