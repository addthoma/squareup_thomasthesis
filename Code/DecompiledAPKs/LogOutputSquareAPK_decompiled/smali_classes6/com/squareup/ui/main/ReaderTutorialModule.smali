.class public abstract Lcom/squareup/ui/main/ReaderTutorialModule;
.super Ljava/lang/Object;
.source "ReaderTutorialModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideR12FirstTimeTutorialLauncher(Lcom/squareup/ui/main/R12TutorialLauncher;)Lcom/squareup/ui/main/R12ForceableContentLauncher;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideR6FirstTimeVideoLauncher(Lcom/squareup/ui/main/R6VideoLauncher;)Lcom/squareup/ui/main/R6ForceableContentLauncher;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideReaderTutorialWorkflowRunner(Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;)Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideSpeedTestWorkflow(Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;)Lcom/squareup/ui/main/r12education/workflow/SpeedTestWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
