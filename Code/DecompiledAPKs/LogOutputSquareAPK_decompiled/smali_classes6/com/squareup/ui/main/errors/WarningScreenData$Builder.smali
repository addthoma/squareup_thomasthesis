.class public final Lcom/squareup/ui/main/errors/WarningScreenData$Builder;
.super Ljava/lang/Object;
.source "WarningScreenData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/WarningScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private cancellable:Z

.field private defaultButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

.field private glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private localizedMessage:Ljava/lang/String;

.field private localizedTitle:Ljava/lang/String;

.field private messageId:Ljava/lang/Integer;

.field timeout:Ljava/lang/Long;

.field timeoutBehavior:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

.field private titleId:I

.field private topAlternativeButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

.field private vectorId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 131
    iput-boolean v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->cancellable:Z

    .line 133
    sget-object v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->KILL_TENDER_INITIATED_BY_HUMAN:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    iput-object v0, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->timeoutBehavior:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    .line 136
    new-instance v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    invoke-direct {v0}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;-><init>()V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->defaultButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)I
    .locals 0

    .line 118
    iget p0, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->vectorId:I

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;
    .locals 0

    .line 118
    iget-object p0, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->defaultButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;
    .locals 0

    .line 118
    iget-object p0, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->topAlternativeButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 0

    .line 118
    iget-object p0, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)Ljava/lang/String;
    .locals 0

    .line 118
    iget-object p0, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->localizedMessage:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)Ljava/lang/String;
    .locals 0

    .line 118
    iget-object p0, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->localizedTitle:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)Ljava/lang/Integer;
    .locals 0

    .line 118
    iget-object p0, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->messageId:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)I
    .locals 0

    .line 118
    iget p0, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->titleId:I

    return p0
.end method

.method static synthetic access$800(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;)Z
    .locals 0

    .line 118
    iget-boolean p0, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->cancellable:Z

    return p0
.end method


# virtual methods
.method public build()Lcom/squareup/ui/main/errors/WarningScreenData;
    .locals 2

    .line 204
    new-instance v0, Lcom/squareup/ui/main/errors/WarningScreenData;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/main/errors/WarningScreenData;-><init>(Lcom/squareup/ui/main/errors/WarningScreenData$Builder;Lcom/squareup/ui/main/errors/WarningScreenData$1;)V

    return-object v0
.end method

.method public cancellable(Z)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;
    .locals 0

    .line 189
    iput-boolean p1, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->cancellable:Z

    return-object p0
.end method

.method public defaultButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;
    .locals 1

    .line 145
    new-instance v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;-><init>(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;I)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->defaultButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    return-object p1
.end method

.method public defaultButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->defaultButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    return-object p0
.end method

.method public glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 164
    iput-object p1, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public localizedMessage(Ljava/lang/String;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->localizedMessage:Ljava/lang/String;

    return-object p0
.end method

.method public localizedTitle(Ljava/lang/String;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->localizedTitle:Ljava/lang/String;

    return-object p0
.end method

.method public messageId(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;
    .locals 0

    .line 179
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->messageId:Ljava/lang/Integer;

    return-object p0
.end method

.method public timeout(Ljava/lang/Long;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->timeout:Ljava/lang/Long;

    return-object p0
.end method

.method public timeoutBehavior(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->timeoutBehavior:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    return-object p0
.end method

.method public titleId(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;
    .locals 0

    .line 184
    iput p1, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->titleId:I

    return-object p0
.end method

.method public topAlternativeButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;
    .locals 1

    .line 155
    new-instance v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;-><init>(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;I)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->topAlternativeButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    return-object p1
.end method

.method public topAlternativeButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->topAlternativeButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    return-object p0
.end method

.method public vector(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;
    .locals 0

    .line 159
    iput p1, p0, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->vectorId:I

    return-object p0
.end method
