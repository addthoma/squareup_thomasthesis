.class public Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;
.super Lcom/squareup/ui/main/errors/RootScreenHandler;
.source "RootScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/RootScreenHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LibraryLoadingErrorHandler"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final REFRESH_DELAY_MS:I = 0x3e8


# instance fields
.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final pendingPaymentsCounter:Lcom/squareup/payment/NonForwardedPendingTransactionsCounter;

.field private final queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

.field private final refreshRunnable:Ljava/lang/Runnable;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/payment/NonForwardedPendingTransactionsCounter;Lcom/squareup/util/Res;Lcom/squareup/queue/QueueServiceStarter;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/AndroidNfcState;)V
    .locals 14
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v13, p0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p11

    move-object/from16 v8, p12

    move-object/from16 v9, p13

    move-object/from16 v10, p14

    move-object/from16 v11, p15

    move-object/from16 v12, p16

    .line 519
    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/main/errors/RootScreenHandler;-><init>(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/AndroidNfcState;)V

    .line 505
    new-instance v0, Lcom/squareup/ui/main/errors/-$$Lambda$RootScreenHandler$LibraryLoadingErrorHandler$lQ90Wb1k2HkUCvCa4kEm2CH-FN0;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/errors/-$$Lambda$RootScreenHandler$LibraryLoadingErrorHandler$lQ90Wb1k2HkUCvCa4kEm2CH-FN0;-><init>(Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;)V

    iput-object v0, v13, Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;->refreshRunnable:Ljava/lang/Runnable;

    move-object/from16 v0, p7

    .line 522
    iput-object v0, v13, Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    move-object/from16 v0, p8

    .line 523
    iput-object v0, v13, Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;->pendingPaymentsCounter:Lcom/squareup/payment/NonForwardedPendingTransactionsCounter;

    move-object/from16 v0, p9

    .line 524
    iput-object v0, v13, Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;->res:Lcom/squareup/util/Res;

    move-object/from16 v0, p10

    .line 525
    iput-object v0, v13, Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    return-void
.end method

.method static synthetic lambda$from$1()Ljava/lang/Integer;
    .locals 1

    const/4 v0, 0x0

    .line 537
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private scheduleRefresh()V
    .locals 4

    .line 568
    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;->refreshRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method


# virtual methods
.method public from(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;
    .locals 3

    .line 529
    new-instance p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 530
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->native_library_load_error_title:I

    .line 531
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->native_library_load_error_msg:I

    .line 532
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->ok:I

    .line 533
    invoke-virtual {p0, v0}, Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;->goHomeButton(I)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->defaultButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 535
    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;->pendingPaymentsCounter:Lcom/squareup/payment/NonForwardedPendingTransactionsCounter;

    .line 536
    invoke-interface {v0}, Lcom/squareup/payment/NonForwardedPendingTransactionsCounter;->nonForwardedPendingTransactionsCount()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/main/errors/-$$Lambda$RootScreenHandler$LibraryLoadingErrorHandler$a4ke3JWNKGqvPI792gj-8QZC34o;->INSTANCE:Lcom/squareup/ui/main/errors/-$$Lambda$RootScreenHandler$LibraryLoadingErrorHandler$a4ke3JWNKGqvPI792gj-8QZC34o;

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2BlockingSupport;->getValueOrProvideDefault(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 539
    sget v0, Lcom/squareup/cardreader/ui/R$string;->go_to_google_play:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;->goToPlayStore(I)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->secondButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    goto :goto_1

    .line 541
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;->res:Lcom/squareup/util/Res;

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    sget v2, Lcom/squareup/cardreader/ui/R$string;->processing_payments_one:I

    goto :goto_0

    :cond_1
    sget v2, Lcom/squareup/cardreader/ui/R$string;->processing_payments_many:I

    :goto_0
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "count"

    .line 543
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 544
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 545
    invoke-static {v0}, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;->forDisabledButton(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->secondButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/cardreader/ui/R$string;->native_library_load_error_msg_blocked:I

    .line 546
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->importantMessageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    .line 549
    :goto_1
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$new$0$RootScreenHandler$LibraryLoadingErrorHandler()V
    .locals 1

    .line 506
    invoke-virtual {p0}, Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;->getPresenter()Lcom/squareup/ui/main/errors/ReaderWarningPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->refreshParameters()V

    .line 507
    invoke-direct {p0}, Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;->scheduleRefresh()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 553
    invoke-super {p0, p1}, Lcom/squareup/ui/main/errors/RootScreenHandler;->onEnterScope(Lmortar/MortarScope;)V

    .line 556
    iget-object p1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    const-string v0, "Flush payments before reinstalling app."

    invoke-interface {p1, v0}, Lcom/squareup/queue/QueueServiceStarter;->start(Ljava/lang/String;)V

    .line 560
    invoke-direct {p0}, Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;->scheduleRefresh()V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 564
    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;->refreshRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method
