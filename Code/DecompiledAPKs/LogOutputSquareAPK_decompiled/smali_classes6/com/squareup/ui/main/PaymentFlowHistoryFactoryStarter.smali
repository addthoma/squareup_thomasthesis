.class public final Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;
.super Ljava/lang/Object;
.source "PaymentFlowHistoryFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\u0018\u00002\u00020\u0001B\u001d\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010J\u0014\u0010\r\u001a\u00020\u000e2\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0011R\u001b\u0010\u0008\u001a\u00020\u00068BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u000b\u0010\u000c\u001a\u0004\u0008\t\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;",
        "",
        "paymentFlowHistoryFactory",
        "Lcom/squareup/ui/main/PaymentFlowHistoryFactory;",
        "lazyFlow",
        "Ldagger/Lazy;",
        "Lflow/Flow;",
        "(Lcom/squareup/ui/main/PaymentFlowHistoryFactory;Ldagger/Lazy;)V",
        "flow",
        "getFlow",
        "()Lflow/Flow;",
        "flow$delegate",
        "Ldagger/Lazy;",
        "showOverPaymentFlowBackground",
        "",
        "stack",
        "Lcom/squareup/container/ContainerTreeKey;",
        "",
        "pos-container_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final flow$delegate:Ldagger/Lazy;

.field private final paymentFlowHistoryFactory:Lcom/squareup/ui/main/PaymentFlowHistoryFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    const-class v2, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "flow"

    const-string v4, "getFlow()Lflow/Flow;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/PaymentFlowHistoryFactory;Ldagger/Lazy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/PaymentFlowHistoryFactory;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "paymentFlowHistoryFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyFlow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;->paymentFlowHistoryFactory:Lcom/squareup/ui/main/PaymentFlowHistoryFactory;

    .line 34
    iput-object p2, p0, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;->flow$delegate:Ldagger/Lazy;

    return-void
.end method

.method public static final synthetic access$getPaymentFlowHistoryFactory$p(Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;)Lcom/squareup/ui/main/PaymentFlowHistoryFactory;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;->paymentFlowHistoryFactory:Lcom/squareup/ui/main/PaymentFlowHistoryFactory;

    return-object p0
.end method

.method private final getFlow()Lflow/Flow;
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;->flow$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    return-object v0
.end method


# virtual methods
.method public final showOverPaymentFlowBackground(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1

    const-string v0, "stack"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    const-string v0, "singletonList(stack)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;->showOverPaymentFlowBackground(Ljava/util/List;)V

    return-void
.end method

.method public final showOverPaymentFlowBackground(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;)V"
        }
    .end annotation

    const-string v0, "stack"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;->getFlow()Lflow/Flow;

    move-result-object v0

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter$showOverPaymentFlowBackground$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter$showOverPaymentFlowBackground$1;-><init>(Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;Ljava/util/List;)V

    check-cast v2, Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;

    invoke-direct {v1, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
