.class final Lcom/squareup/ui/main/RealPaymentCompletionMonitor$showTransactionIncompleteNotification$1;
.super Ljava/lang/Object;
.source "PaymentCompletionMonitor.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/RealPaymentCompletionMonitor;-><init>(Lcom/squareup/payment/AutoVoid;Lcom/squareup/badbus/BadBus;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/notifications/PaymentIncompleteNotifier;Lcom/squareup/payment/Transaction;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/RealPaymentCompletionMonitor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/RealPaymentCompletionMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor$showTransactionIncompleteNotification$1;->this$0:Lcom/squareup/ui/main/RealPaymentCompletionMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor$showTransactionIncompleteNotification$1;->this$0:Lcom/squareup/ui/main/RealPaymentCompletionMonitor;

    invoke-static {v0}, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;->access$getPaymentIncompleteNotifier$p(Lcom/squareup/ui/main/RealPaymentCompletionMonitor;)Lcom/squareup/notifications/PaymentIncompleteNotifier;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/notifications/PaymentIncompleteNotifier;->showNotification()V

    return-void
.end method
