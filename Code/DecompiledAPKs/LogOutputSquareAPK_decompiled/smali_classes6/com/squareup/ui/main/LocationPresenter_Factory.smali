.class public final Lcom/squareup/ui/main/LocationPresenter_Factory;
.super Ljava/lang/Object;
.source "LocationPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/LocationPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final locationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/main/LocationPresenter_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/main/LocationPresenter_Factory;->locationProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/main/LocationPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/LocationPresenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/main/LocationPresenter_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/ui/main/LocationPresenter_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/main/LocationPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Clock;Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/main/LocationPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Clock;",
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/ui/main/LocationPresenter;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/ui/main/LocationPresenter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/main/LocationPresenter;-><init>(Lcom/squareup/util/Clock;Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/LocationPresenter;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/main/LocationPresenter_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Clock;

    iget-object v1, p0, Lcom/squareup/ui/main/LocationPresenter_Factory;->locationProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/ui/main/LocationPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/main/LocationPresenter_Factory;->newInstance(Lcom/squareup/util/Clock;Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/main/LocationPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/main/LocationPresenter_Factory;->get()Lcom/squareup/ui/main/LocationPresenter;

    move-result-object v0

    return-object v0
.end method
