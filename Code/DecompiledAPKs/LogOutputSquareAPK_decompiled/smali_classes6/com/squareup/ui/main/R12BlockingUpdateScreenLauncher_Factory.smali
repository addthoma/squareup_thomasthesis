.class public final Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher_Factory;
.super Ljava/lang/Object;
.source "R12BlockingUpdateScreenLauncher_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher;",
        ">;"
    }
.end annotation


# instance fields
.field private final buyerFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final r12BlockingUpdateDayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher_Factory;->r12BlockingUpdateDayProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher_Factory;->buyerFlowStarterProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;)",
            "Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher_Factory;"
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;)Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ")",
            "Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher;"
        }
    .end annotation

    .line 56
    new-instance v0, Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher;-><init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher;
    .locals 4

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher_Factory;->r12BlockingUpdateDayProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    iget-object v1, p0, Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    iget-object v2, p0, Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher_Factory;->buyerFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/buyer/BuyerFlowStarter;

    iget-object v3, p0, Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher_Factory;->newInstance(Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;)Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher_Factory;->get()Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher;

    move-result-object v0

    return-object v0
.end method
