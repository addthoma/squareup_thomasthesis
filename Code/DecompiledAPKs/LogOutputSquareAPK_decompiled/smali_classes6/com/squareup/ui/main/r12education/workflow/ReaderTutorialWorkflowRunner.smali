.class public interface abstract Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner;
.super Ljava/lang/Object;
.source "ReaderTutorialWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner$ParentComponent;,
        Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/container/PosWorkflowRunner<",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0008f\u0018\u0000 \u00072\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0007\u0008J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner;",
        "Lcom/squareup/container/PosWorkflowRunner;",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput;",
        "start",
        "",
        "useUpdatingMessaging",
        "",
        "Companion",
        "ParentComponent",
        "reader-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner$Companion;->$$INSTANCE:Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner$Companion;

    sput-object v0, Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner;->Companion:Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner$Companion;

    return-void
.end method


# virtual methods
.method public abstract start(Z)V
.end method
