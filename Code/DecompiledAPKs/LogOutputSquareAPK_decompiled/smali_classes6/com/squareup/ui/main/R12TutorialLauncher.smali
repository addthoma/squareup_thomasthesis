.class public Lcom/squareup/ui/main/R12TutorialLauncher;
.super Lcom/squareup/ui/main/ReaderTutorialLauncher;
.source "R12TutorialLauncher.java"

# interfaces
.implements Lcom/squareup/ui/main/R12ForceableContentLauncher;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/main/ReaderTutorialLauncher<",
        "Ljava/lang/Boolean;",
        ">;",
        "Lcom/squareup/ui/main/R12ForceableContentLauncher;"
    }
.end annotation


# instance fields
.field private final apiRequestController:Lcom/squareup/api/ApiRequestController;

.field private final buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

.field private final cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/settings/LocalSetting;Ldagger/Lazy;Lcom/squareup/api/ApiRequestController;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/CardReaderHubUtils;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/api/ApiRequestController;",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0, p2}, Lcom/squareup/ui/main/ReaderTutorialLauncher;-><init>(Lcom/squareup/settings/LocalSetting;)V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/main/R12TutorialLauncher;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/main/R12TutorialLauncher;->flow:Ldagger/Lazy;

    .line 31
    iput-object p4, p0, Lcom/squareup/ui/main/R12TutorialLauncher;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    .line 32
    iput-object p5, p0, Lcom/squareup/ui/main/R12TutorialLauncher;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    .line 33
    iput-object p6, p0, Lcom/squareup/ui/main/R12TutorialLauncher;->features:Lcom/squareup/settings/server/Features;

    .line 34
    iput-object p7, p0, Lcom/squareup/ui/main/R12TutorialLauncher;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    return-void
.end method


# virtual methods
.method protected canShowTutorial()Z
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/main/R12TutorialLauncher;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    invoke-interface {v0}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/main/R12TutorialLauncher;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->isApiRequest()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isContentShowing()Z
    .locals 2

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/main/R12TutorialLauncher;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    const-class v1, Lcom/squareup/ui/main/r12education/R12EducationScreen;

    invoke-interface {v0, v1}, Lcom/squareup/ui/main/PosContainer;->currentPathIncludes(Ljava/lang/Class;)Z

    move-result v0

    return v0
.end method

.method public showContent(Ljava/lang/Boolean;)V
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/main/R12TutorialLauncher;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->EMONEY_SPEED_TEST:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/main/R12TutorialLauncher;->flow:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/main/r12education/workflow/R12ReaderTutorialBootstrapScreen;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {v1, p1}, Lcom/squareup/ui/main/r12education/workflow/R12ReaderTutorialBootstrapScreen;-><init>(Z)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/R12TutorialLauncher;->flow:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/main/r12education/R12EducationScreen;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {v1, p1}, Lcom/squareup/ui/main/r12education/R12EducationScreen;-><init>(Z)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public bridge synthetic showContent(Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/R12TutorialLauncher;->showContent(Ljava/lang/Boolean;)V

    return-void
.end method
