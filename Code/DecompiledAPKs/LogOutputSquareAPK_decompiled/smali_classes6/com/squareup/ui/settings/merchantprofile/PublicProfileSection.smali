.class public Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;
.super Lcom/squareup/applet/AppletSection;
.source "PublicProfileSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ReceiptInfoGranularPermissionAccess;,
        Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$Access;,
        Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;
    }
.end annotation


# static fields
.field public static TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    sget v0, Lcom/squareup/settingsapplet/R$string;->merchant_profile_title:I

    sput v0, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_SETTINGS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p2, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p2

    if-eqz p2, :cond_0

    new-instance p2, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ReceiptInfoGranularPermissionAccess;

    invoke-direct {p2, p1}, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ReceiptInfoGranularPermissionAccess;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;)V

    goto :goto_0

    :cond_0
    new-instance p2, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$Access;

    invoke-direct {p2, p1}, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$Access;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;)V

    :goto_0
    invoke-direct {p0, p2}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileScreen;->INSTANCE:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileScreen;

    return-object v0
.end method
