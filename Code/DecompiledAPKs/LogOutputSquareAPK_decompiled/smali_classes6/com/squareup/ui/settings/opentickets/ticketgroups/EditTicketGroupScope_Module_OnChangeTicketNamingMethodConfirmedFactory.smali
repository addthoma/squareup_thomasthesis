.class public final Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope_Module_OnChangeTicketNamingMethodConfirmedFactory;
.super Ljava/lang/Object;
.source "EditTicketGroupScope_Module_OnChangeTicketNamingMethodConfirmedFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope_Module_OnChangeTicketNamingMethodConfirmedFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lrx/Observable<",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope_Module_OnChangeTicketNamingMethodConfirmedFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope_Module_OnChangeTicketNamingMethodConfirmedFactory$InstanceHolder;->access$000()Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope_Module_OnChangeTicketNamingMethodConfirmedFactory;

    move-result-object v0

    return-object v0
.end method

.method public static onChangeTicketNamingMethodConfirmed()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 27
    invoke-static {}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope$Module;->onChangeTicketNamingMethodConfirmed()Lrx/Observable;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Observable;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope_Module_OnChangeTicketNamingMethodConfirmedFactory;->get()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public get()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 19
    invoke-static {}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope_Module_OnChangeTicketNamingMethodConfirmedFactory;->onChangeTicketNamingMethodConfirmed()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method
