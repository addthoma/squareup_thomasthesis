.class Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsCardPresenter;
.source "MerchantProfileEditLogoScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsCardPresenter<",
        "Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;",
        ">;"
    }
.end annotation


# instance fields
.field private final fileThreadExecutor:Ljava/util/concurrent/Executor;

.field private imageStatus:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final merchantProfileState:Lcom/squareup/merchantprofile/MerchantProfileState;

.field private final res:Lcom/squareup/util/Res;

.field private final showsImageSaveProgress:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            ">;"
        }
    .end annotation
.end field

.field private final tempPhotoDir:Ljava/io/File;

.field private uri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/merchantprofile/MerchantProfileState;Lcom/squareup/util/Res;Ljava/io/File;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p7    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 73
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/SettingsCardPresenter;-><init>(Lcom/squareup/util/Device;Lflow/Flow;)V

    .line 58
    new-instance p1, Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-direct {p1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->showsImageSaveProgress:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    .line 68
    sget-object p1, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;->LOADING:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->imageStatus:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    .line 74
    iput-object p3, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 75
    iput-object p4, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->merchantProfileState:Lcom/squareup/merchantprofile/MerchantProfileState;

    .line 76
    iput-object p5, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 77
    iput-object p6, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->tempPhotoDir:Ljava/io/File;

    .line 78
    iput-object p7, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method private saveImage()V
    .locals 4

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->showsImageSaveProgress:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v1, Lcom/squareup/caller/ProgressPopup$Progress;

    iget-object v2, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/settingsapplet/R$string;->merchant_profile_saving_image:I

    .line 140
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/caller/ProgressPopup$Progress;-><init>(Ljava/lang/String;)V

    .line 139
    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 141
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->createBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 143
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/logo/-$$Lambda$MerchantProfileEditLogoScreen$Presenter$Cn84YqAbE1qqo8g00748HrUGAGI;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/settings/merchantprofile/logo/-$$Lambda$MerchantProfileEditLogoScreen$Presenter$Cn84YqAbE1qqo8g00748HrUGAGI;-><init>(Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;Landroid/graphics/Bitmap;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method bitmapLoadFailed()V
    .locals 2

    .line 134
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 135
    sget-object v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;->FAILED:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    iput-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->imageStatus:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    return-void
.end method

.method bitmapLoaded()V
    .locals 2

    .line 129
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 130
    sget-object v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;->LOADED:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    iput-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->imageStatus:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    return-void
.end method

.method public closeCard()V
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->showsImageSaveProgress:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dismiss()V

    .line 158
    invoke-super {p0}, Lcom/squareup/ui/settings/SettingsCardPresenter;->closeCard()V

    return-void
.end method

.method public dropView(Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;)V
    .locals 2

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->showsImageSaveProgress:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->getProgressPopup()Lcom/squareup/caller/ProgressPopup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 111
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsCardPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->dropView(Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;)V

    return-void
.end method

.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->merchant_profile_add_photo:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$1$MerchantProfileEditLogoScreen$Presenter(Ljava/io/File;Landroid/graphics/Bitmap;)V
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->merchantProfileState:Lcom/squareup/merchantprofile/MerchantProfileState;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/merchantprofile/MerchantProfileState;->setLogo(Ljava/io/File;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public synthetic lambda$onLoad$0$MerchantProfileEditLogoScreen$Presenter()V
    .locals 0

    .line 85
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->onSaveClicked()Z

    .line 86
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->closeCard()V

    return-void
.end method

.method public synthetic lambda$saveImage$2$MerchantProfileEditLogoScreen$Presenter(Landroid/graphics/Bitmap;)V
    .locals 3

    :try_start_0
    const-string v0, "merchant-logo"

    const/4 v1, 0x0

    .line 146
    iget-object v2, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->tempPhotoDir:Ljava/io/File;

    invoke-static {v0, v1, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    .line 147
    invoke-static {p1, v0}, Lcom/squareup/util/Images;->writeToFile(Landroid/graphics/Bitmap;Ljava/io/File;)V

    .line 149
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/logo/-$$Lambda$MerchantProfileEditLogoScreen$Presenter$e2Zv7wznOZwq4VA_IZw5Yv0Gfzs;

    invoke-direct {v2, p0, v0, p1}, Lcom/squareup/ui/settings/merchantprofile/logo/-$$Lambda$MerchantProfileEditLogoScreen$Presenter$e2Zv7wznOZwq4VA_IZw5Yv0Gfzs;-><init>(Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;Ljava/io/File;Landroid/graphics/Bitmap;)V

    invoke-interface {v1, v2}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "Failed to store merchant logo file."

    .line 151
    invoke-static {p1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 96
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsCardPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 97
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen;

    .line 98
    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen;->access$000(Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->uri:Landroid/net/Uri;

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 82
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->buildActionbarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->save:I

    .line 83
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/logo/-$$Lambda$MerchantProfileEditLogoScreen$Presenter$eYJNNEjZWcI2tNr8l3RRBf8X1rA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/merchantprofile/logo/-$$Lambda$MerchantProfileEditLogoScreen$Presenter$eYJNNEjZWcI2tNr8l3RRBf8X1rA;-><init>(Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;)V

    .line 84
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 88
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 82
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 91
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;

    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->uri:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->loadPhoto(Landroid/net/Uri;)V

    .line 92
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->showsImageSaveProgress:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoView;->getProgressPopup()Lcom/squareup/caller/ProgressPopup;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method onSaveClicked()Z
    .locals 3

    .line 115
    sget-object v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$1;->$SwitchMap$com$squareup$ui$settings$merchantprofile$logo$MerchantProfileEditLogoScreen$Presenter$ImageStatus:[I

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->imageStatus:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    .line 121
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->saveImage()V

    return v1

    .line 124
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized image status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter;->imageStatus:Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Presenter$ImageStatus;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 102
    const-class v0, Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen;

    return-object v0
.end method
