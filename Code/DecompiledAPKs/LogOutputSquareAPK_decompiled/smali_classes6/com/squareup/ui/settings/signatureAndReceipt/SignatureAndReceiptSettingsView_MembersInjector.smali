.class public final Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView_MembersInjector;
.super Ljava/lang/Object;
.source "SignatureAndReceiptSettingsView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;",
            ">;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectFeatures(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->presenter:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    return-void
.end method

.method public static injectRes(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;Lcom/squareup/util/Res;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView_MembersInjector;->injectPresenter(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView_MembersInjector;->injectRes(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;Lcom/squareup/util/Res;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView_MembersInjector;->injectFeatures(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;Lcom/squareup/settings/server/Features;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView_MembersInjector;->injectMembers(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)V

    return-void
.end method
