.class public abstract Lcom/squareup/ui/settings/SettingsCardPresenter;
.super Lmortar/ViewPresenter;
.source "SettingsCardPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/ViewGroup;",
        ":",
        "Lcom/squareup/ui/HasActionBar;",
        ">",
        "Lmortar/ViewPresenter<",
        "TT;>;"
    }
.end annotation


# instance fields
.field protected final device:Lcom/squareup/util/Device;

.field protected final flow:Lflow/Flow;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lflow/Flow;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/settings/SettingsCardPresenter;->device:Lcom/squareup/util/Device;

    .line 23
    iput-object p2, p0, Lcom/squareup/ui/settings/SettingsCardPresenter;->flow:Lflow/Flow;

    return-void
.end method

.method private buildCardConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    .line 54
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 55
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideSecondaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public buildActionbarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 3

    .line 31
    invoke-direct {p0}, Lcom/squareup/ui/settings/SettingsCardPresenter;->buildCardConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 32
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsCardPresenter;->getActionbarText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 33
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/-$$Lambda$SettingsCardPresenter$fmj64eLGWsJGNts92c4-273azLo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/-$$Lambda$SettingsCardPresenter$fmj64eLGWsJGNts92c4-273azLo;-><init>(Lcom/squareup/ui/settings/SettingsCardPresenter;)V

    .line 34
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    return-object v0
.end method

.method public closeCard()V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsCardPresenter;->flow:Lflow/Flow;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsCardPresenter;->screenForAssertion()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->closeCard(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public abstract getActionbarText()Ljava/lang/String;
.end method

.method public synthetic lambda$buildActionbarConfig$0$SettingsCardPresenter()V
    .locals 0

    .line 35
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsCardPresenter;->onUpClicked()V

    .line 36
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsCardPresenter;->closeCard()V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsCardPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    check-cast p1, Lcom/squareup/ui/HasActionBar;

    invoke-interface {p1}, Lcom/squareup/ui/HasActionBar;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsCardPresenter;->buildActionbarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method public onUpClicked()V
    .locals 0

    return-void
.end method

.method public abstract screenForAssertion()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation
.end method
